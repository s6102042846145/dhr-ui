#region Using

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using ESS.ANNOUNCEMENT.ABSTRACT;
using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
//using ESS.SHAREDATASERVICE.DATACLASS;

#endregion Using

namespace ESS.ANNOUNCEMENT.DB
{
    public class AnnouncementServiceImpl : AbstractAnnouncementService
    {
        #region Constructor
        public AnnouncementServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.ANNOUNCEMENT.DB";

        private string BaseConnStr 
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region Override Function

        public override List<AnnouncementData> GetAnnoncementList()
        {
            List<AnnouncementData> returnValue = new List<AnnouncementData>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AnnouncementGetUptodate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("Announcement");
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                AnnouncementData data = new AnnouncementData();
                data.ParseToObject(dr);
                returnValue.Add(data);
            }
            oConnection.Close();



            return returnValue;
        }
        

        public override void acceptAnnouncement(string employeeID, string announcementID, string statusType)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_AnnouncementSeeByUpdate", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.AddWithValue("@p_EmployeeID", employeeID);
            oCommand.Parameters.AddWithValue("@p_AnnouncementID", Convert.ToInt32(announcementID));
            oCommand.Parameters.AddWithValue("@p_Status", statusType);

            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        public override void announcementSave(AnnouncementData announcement)
        {
            try
            {
                oSqlManage["ProcedureName"] = "sp_AnnouncementSave";
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_ID"] = announcement.ID;
                oParamRequest["@p_Name"] = announcement.Name;
                oParamRequest["@p_NameEN"] = announcement.NameEN;
                oParamRequest["@p_Content"] = announcement.Content;
                oParamRequest["@p_ContentEN"] = announcement.ContentEN;
                oParamRequest["@p_Image"] = announcement.Image;
                oParamRequest["@p_Priority"] = announcement.Priority;
                oParamRequest["@p_BeginDate"] = announcement.BeginDate;
                oParamRequest["@p_EndDate"] = announcement.EndDate;
                oParamRequest["@p_NewFlagDate"] = announcement.NewFlagDate;
                oParamRequest["@p_Type"] = announcement.Type;
                oParamRequest["@p_RequestNo"] = announcement.RequestNo;
                oParamRequest["@p_Acceptable"] = announcement.Acceptable;

                DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            }
            catch(Exception ex)
            {
                throw new Exception("Save announcement error", ex);
            }
            
        }

        public override void announcementDelete(int announcementID)
        {
            try
            {
                oSqlManage["ProcedureName"] = "sp_AnnouncementDelete";
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_ID"] = announcementID;

                DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            }
            catch (Exception ex)
            {
                throw new Exception("Delete announcement error", ex);
            }

        }

        public override void announcementUpdateState(int oID, string oState)
        {
            try
            {
                oSqlManage["ProcedureName"] = "sp_AnnouncementUpdateState";
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_ID"] = oID;
                oParamRequest["@p_State"] = oState;

                DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            }
            catch (Exception ex)
            {
                throw new Exception("Update state announcement error", ex);
            }
        }

        public override void announcementDeleteByRequestNo(string RequestNo)
        {
            try
            {
                oSqlManage["ProcedureName"] = "sp_AnnouncementDeleteByRequestNo";
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_RequestNo"] = RequestNo;

                DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            }
            catch (Exception ex)
            {
                throw new Exception("Delete announcement By Request No. error", ex);
            }

        }
        #endregion Override Function
    }
}