﻿using DHR.HR.API;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using DESS.JOB;
using ESS.DATA;
using ESS.DATA.EXCEPTION;
using ESS.HR.PA.ABSTRACT;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace ESS.HR.PA.DHR
{
    public class HRPADataServiceImpl : AbstractHRPADataService
    {
        private CultureInfo thCL = new CultureInfo("th-TH");
        private CultureInfo enCL = new CultureInfo("en-US");
        #region Constructor
        public HRPADataServiceImpl(string oComparyCode)
        {
            CompanyCode = oComparyCode;
        }
        #endregion Constructor

        #region Member
        //private static string ModuleID = "ESS.HR.PA.DHR";
        private string CompanyCode { get; set; }
        CultureInfo oCL = new CultureInfo("en-US");

        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        #region GET
        private string DefaultDateGet
        {
            get
            {
                //ddMMyyyy:00:00:00
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATGET");
            }
        }
        private string DefaultEndDateGet
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }

        #endregion GET

        #region POST
        private string DefaultDatePost
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATPOST");
            }
        }
        private string DefaultEndDatePost
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATPOST");
            }
        }
        #endregion POST


        public string PeriodStartDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "PERIODSTARTDATE");

            }
        }

        #endregion Member

        #region "GetPersonalAddress"

        public override List<PersonalAddressCenter> GetPersonalAddress(string EmployeeID, DateTime CheckDate)
        {
            List<PersonalAddressCenter> personalAddrList = new List<PersonalAddressCenter>();
            ResponseModel<PAAddressInf> responseModel = new ResponseModel<PAAddressInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAddressInfByEmpCode(CheckDate.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                List<PAAddressInf> paAddress = responseModel.Data;
                foreach (PAAddressInf item in paAddress)
                {
                    PersonalAddressCenter personalAddr = new PersonalAddressCenter();
                    personalAddr.CompanyCode = CompanyCode;
                    personalAddr.EmployeeID = EmployeeID;
                    personalAddr.AddressType = string.IsNullOrEmpty(item.addressTypeValue) ? "" : item.addressTypeValue;
                    personalAddr.AddressNo = string.IsNullOrEmpty(item.addressNo) ? "" : item.addressNo;
                    personalAddr.Moo = string.IsNullOrEmpty(item.moo) ? "" : item.moo;
                    personalAddr.Street = string.IsNullOrEmpty(item.street) ? "" : item.street;
                    personalAddr.Soi = string.IsNullOrEmpty(item.soi) ? "" : item.soi;
                    personalAddr.District = string.IsNullOrEmpty(item.districtCode) ? "" : item.districtCode;
                    personalAddr.Subdistrict = string.IsNullOrEmpty(item.subdistrictCode) ? "" : item.subdistrictCode;
                    personalAddr.Province = string.IsNullOrEmpty(item.provinceCode) ? "" : item.provinceCode;
                    personalAddr.Country = string.IsNullOrEmpty(item.countryValue) ? "" : item.countryValue;
                    personalAddr.Postcode = string.IsNullOrEmpty(item.postCode) ? "" : item.postCode;
                    //personalAddr.BeginDate = DateTime.ParseExact(item.startDate, DefaultReturnDateFormat, oCL);
                    personalAddr.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                    //personalAddr.EndDate = DateTime.ParseExact(item.endDate, DefaultReturnDateFormat, oCL);
                    personalAddr.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                    personalAddr.AddressTypeCode = string.IsNullOrEmpty(item.addressTypeCode) ? "" : item.addressTypeCode;
                    personalAddr.AddressTypeTextTH = string.IsNullOrEmpty(item.addressTypeTextTH) ? "" : item.addressTypeTextTH;
                    personalAddr.AddressTypeTextEN = string.IsNullOrEmpty(item.addressTypeTextEN) ? "" : item.addressTypeTextEN;
                    personalAddr.SubdistrictTextTH = string.IsNullOrEmpty(item.subdistrictTextTH) ? "" : item.subdistrictTextTH;
                    personalAddr.SubdistrictTextEN = string.IsNullOrEmpty(item.subdistrictTextEN) ? "" : item.subdistrictTextEN;
                    personalAddr.DistrictTextTH = string.IsNullOrEmpty(item.districtTextTH) ? "" : item.districtTextTH;
                    personalAddr.DistrictTextEN = string.IsNullOrEmpty(item.districtTextEN) ? "" : item.districtTextEN;
                    personalAddr.ProvinceTextTH = string.IsNullOrEmpty(item.provinceTextTH) ? "" : item.provinceTextTH;
                    personalAddr.ProvinceTextEN = string.IsNullOrEmpty(item.provinceTextEN) ? "" : item.provinceTextEN;
                    personalAddr.CountryCode = string.IsNullOrEmpty(item.countryCode) ? "" : item.countryCode;
                    personalAddr.CountryTextTH = string.IsNullOrEmpty(item.countryTextTH) ? "" : item.countryTextTH;
                    personalAddr.CountryTextEN = string.IsNullOrEmpty(item.countryTextEN) ? "" : item.countryTextEN;
                    personalAddr.Building = string.IsNullOrEmpty(item.building) ? "" : item.building;
                    personalAddr.BuildingEn = string.IsNullOrEmpty(item.buildingEn) ? "" : item.buildingEn;
                    personalAddrList.Add(personalAddr);
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetPersonalAddress Error", responseModel.message);
            }

            return PopulateMissingAddress(EmployeeID, personalAddrList);
        }
        private List<PersonalAddressCenter> PopulateMissingAddress(string EmployeeID, List<PersonalAddressCenter> addresses)
        {
            Dictionary<string, int> addressdict = new Dictionary<string, int>();
            foreach (PersonalAddressCenter item in addresses)
            {
                addressdict[item.AddressType] = 1;
            }

            // populate the missing address type
            if (!addressdict.ContainsKey("001"))
            {

                PersonalAddressCenter regisAddress = new PersonalAddressCenter();
                regisAddress.AddressType = "001";
                regisAddress.EmployeeID = EmployeeID;
                addresses.Add(regisAddress);
            }
            if (!addressdict.ContainsKey("002"))
            {

                PersonalAddressCenter currentAddress = new PersonalAddressCenter();
                currentAddress.AddressType = "002";
                currentAddress.EmployeeID = EmployeeID;
                addresses.Add(currentAddress);
            }
            return addresses.OrderBy(a => a.AddressType).ToList();

        }

        public override List<MTValidateDataCodeValue> GetMTValidateDataCodeValue(string dataCode, string dataValue)
        {
            List<MTValidateDataCodeValue> ValidateDataCodeVule = new List<MTValidateDataCodeValue>();
            ResponseModel<MTValidateDataCodeValue> responseModel = new ResponseModel<MTValidateDataCodeValue>();
            if (!string.IsNullOrEmpty(dataCode) && !string.IsNullOrEmpty(dataValue))
            {
                try
                {
                    responseModel = APIMasterDataManagement.CreateInstance(CompanyCode).GetMTValidateDataCodeValue(dataCode, dataValue);
                    List<MTValidateDataCodeValue> paDataCodeValue = responseModel.Data;

                    foreach (MTValidateDataCodeValue item in paDataCodeValue)
                    {
                        MTValidateDataCodeValue data = new MTValidateDataCodeValue();
                        data.dataCode = item.dataCode;
                        data.valueCode = item.valueCode;
                        data.valueName = item.valueName;
                        data.valueNameEn = item.valueNameEn;
                        data.mapDataCode = item.mapDataCode;
                        data.mapValueCode = item.mapValueCode;
                        data.mapValueName = item.mapValueName;
                        data.mapValueNameEn = item.mapValueNameEn;
                        ValidateDataCodeVule.Add(data);
                    }

                }
                catch (Exception ex)
                {
                    throw new DataServiceException("HRPA", "GetMTValidateDataCodeValues Error", responseModel.message);

                }

            }

            return ValidateDataCodeVule;
        }
        public override List<MTValidateTitleName> GetMTValidateTitleName(string companyCode, string language, string iname_code = null, string iname_value = null, string iname1_code = null, string iname1_value = null,
            string iname2_code = null, string iname2_value = null, string iname3_code = null, string iname3_value = null, string iname4_code = null, string iname4_value = null, string iname5_code = null, string iname5_value = null)
        {
            List<MTValidateTitleName> ValidateTitleName = new List<MTValidateTitleName>();
            ResponseModel<MTValidateTitleName> responseModel = new ResponseModel<MTValidateTitleName>();

            try
            {
                responseModel = APIMasterDataManagement.CreateInstance(CompanyCode).GetMTValidateTitleName(companyCode, language, iname_code, iname_value, iname1_code, iname1_value,
           iname2_code, iname2_value, iname3_code, iname3_value, iname4_code, iname4_value, iname5_code, iname5_value);
                List<MTValidateTitleName> paDataCodeValue = responseModel.Data;
                foreach (MTValidateTitleName item in paDataCodeValue)
                {
                    MTValidateTitleName data = new MTValidateTitleName();
                    data.companyCode = !string.IsNullOrEmpty(item.companyCode) ? item.companyCode : string.Empty;
                    data.empCode = !string.IsNullOrEmpty(item.empCode) ? item.empCode : string.Empty;
                    data.startDate = !string.IsNullOrEmpty(item.startDate) ? item.startDate : string.Empty;
                    data.endDate = !string.IsNullOrEmpty(item.endDate) ? item.endDate : string.Empty;
                    data.title = !string.IsNullOrEmpty(item.title) ? item.title : string.Empty;
                    ValidateTitleName.Add(data);
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetMTValidateTitleName Error", responseModel.message);
            }

            return ValidateTitleName;
        }
        public override List<DistrictProvice> GetDistrictProvices(string provinceCode, string districtCode = null, string subdistrictCode = null, string postcode = null)
        {
            List<DistrictProvice> DistrictProvince = new List<DistrictProvice>();
            ResponseModel<DistrictProvice> responseModel = new ResponseModel<DistrictProvice>();
            try
            {
                responseModel = APIMasterDataManagement.CreateInstance(CompanyCode).GetDHRDistrictProvince(provinceCode, districtCode, subdistrictCode, postcode);
                List<DistrictProvice> paAddress = responseModel.Data;
                foreach (DistrictProvice item in paAddress)
                {
                    DistrictProvice DistrictProve = new DistrictProvice();
                    DistrictProve.provinceCode = item.provinceCode;
                    DistrictProve.provinceName = item.provinceName;
                    DistrictProve.provinceNameEn = item.provinceNameEn;
                    DistrictProve.districtCode = item.districtCode;
                    DistrictProve.districtName = item.districtName;
                    DistrictProve.districtNameEn = item.districtNameEn;
                    DistrictProve.subdistrictCode = item.subdistrictCode;
                    DistrictProve.subdistrictName = item.subdistrictName;
                    DistrictProve.subdistrictNameEn = item.subdistrictNameEn;
                    DistrictProve.postcode = item.postcode;
                    if (paAddress != null)
                    {
                        DistrictProvince.Add(DistrictProve);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetDISTRICTPROVICE Error", responseModel.message);
            }

            return DistrictProvince;
        }

        public override void SavePersonalAddress(List<PersonalAddressCenter> data)
        {

            PostResult postResult = null;

            try
            {
                foreach (PersonalAddressCenter item in data)
                {
                    PAAddressInfPost pAddressInfo = new PAAddressInfPost();
                    pAddressInfo.companyCode = CompanyCode;
                    pAddressInfo.empCode = item.EmployeeID;
                    pAddressInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                    pAddressInfo.endDate = DefaultEndDatePost;
                    pAddressInfo.addressTypeCode = string.IsNullOrEmpty(item.AddressType) ? "" : "ADDRESS_TYPE_CODE";
                    pAddressInfo.addressTypeValue = item.AddressType;

                    pAddressInfo.addressNo = string.IsNullOrEmpty(item.AddressNo) ? "" : item.AddressNo;
                    pAddressInfo.moo = string.IsNullOrEmpty(item.Moo) ? "" : item.Moo;
                    pAddressInfo.building = string.IsNullOrEmpty(item.Building) ? "" : item.Building;
                    pAddressInfo.soi = string.IsNullOrEmpty(item.Soi) ? "" : item.Soi;
                    pAddressInfo.street = string.IsNullOrEmpty(item.Street) ? "" : item.Street;
                    pAddressInfo.subdistrictCode = string.IsNullOrEmpty(item.Subdistrict) ? "" : item.Subdistrict;
                    pAddressInfo.districtCode = string.IsNullOrEmpty(item.District) ? "" : item.District;
                    pAddressInfo.provinceCode = string.IsNullOrEmpty(item.Province) ? "" : item.Province;
                    pAddressInfo.postCode = string.IsNullOrEmpty(item.Postcode) ? "" : item.Postcode;
                    pAddressInfo.countryCode = string.IsNullOrEmpty(item.CountryCode) ? "COUNTRY_CODE" : item.CountryCode;
                    pAddressInfo.countryValue = string.IsNullOrEmpty(item.Country) ? "" : item.Country;
                    pAddressInfo.buildingEn = string.IsNullOrEmpty(item.BuildingEn) ? "" : item.BuildingEn;

                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAAddressInf(pAddressInfo);
                    if (postResult.returnCode != 101)
                    {
                        var DataDetail = JsonConvert.SerializeObject(pAddressInfo);
                        throw new DataServiceException("HRPA", "SAVE_PERSONALADDRESS", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "SAVE_PERSONALADDRESS", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
            }
        }

        #endregion "GetPersonalAddress"        

        public override List<PersonalFamilyCenter> GetPersonalFamilyData(string EmployeeID)
        {
            List<PersonalFamilyCenter> returnValue = new List<PersonalFamilyCenter>();
            ResponseModel<PAFamilyInf> responseModel = new ResponseModel<PAFamilyInf>();
            ResponseModel<PAAddressFamilyInf> responseModelFAddress = new ResponseModel<PAAddressFamilyInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAFamilyInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                if (responseModel.Data.Count > 0)
                {
                    responseModelFAddress = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAddressFamilyInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);

                    var resModel = responseModel.Data.OrderBy(r => r.relateValue);

                    foreach (var item in resModel)
                    {
                        PAAddressFamilyInf fAddress = null;
                        if (responseModelFAddress != null && responseModelFAddress.Data.Count > 0)
                        {
                            fAddress = responseModelFAddress.Data.Find(a => a.familyId == item.familyId);
                        }

                        PersonalFamilyCenter personalFamily = new PersonalFamilyCenter();
                        personalFamily.CompanyCode = item.companyCode;
                        personalFamily.EmployeeID = item.empCode;
                        personalFamily.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        personalFamily.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        personalFamily.FamilyId = item.familyId;
                        personalFamily.BirthDate = DateTimeService.ConvertDateTime(CompanyCode, item.birthDate);
                        personalFamily.ChildNo = item.familyId.ToString(); //เพื่อ ดึงข้อมูลมา edit ได้ถูกต้อง ในหน้า html ส่ง parameter familymember,childno เนื่องจาก SAP ส่งเป็น childno DHR เลยต้อง assign familyid ใส่ childno 
                        if (!item.relateValue.StartsWith("2"))
                        {
                            personalFamily.FamilyMember = item.relateValue;

                            if (item.relateValue == "01")//มีคู่สมรส
                            {
                                var responsePersonalModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPersonalInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                                PAPersonalInf personalInf = new PAPersonalInf();
                                personalInf = responsePersonalModel.Data[0];
                                personalFamily.MaritalStatus = personalInf.maritalStatusValue;
                                personalFamily.SpouseID = item.idCard;
                                if (!string.IsNullOrEmpty(item.maritalDate))
                                {
                                    DateTime maritalDate = DateTimeService.ConvertDateTime(CompanyCode, item.maritalDate);
                                    personalFamily.MaritalDate = maritalDate != DateTime.MinValue ? maritalDate : (DateTime?)null;
                                }
                                else
                                {
                                    personalFamily.MaritalDate = (DateTime?)null;
                                }
                                if (!string.IsNullOrEmpty(item.divorceDate))
                                {
                                    DateTime divorceDate = DateTimeService.ConvertDateTime(CompanyCode, item.divorceDate);
                                    personalFamily.DivorceDate = divorceDate != DateTime.MinValue ? divorceDate : (DateTime?)null;
                                }
                                else
                                {
                                    personalFamily.DivorceDate = (DateTime?)null;
                                }
                                if (!string.IsNullOrEmpty(item.passAwayDate))
                                {
                                    DateTime passAwayDate = DateTimeService.ConvertDateTime(CompanyCode, item.passAwayDate);
                                    personalFamily.PassAwayDate = passAwayDate != DateTime.MinValue ? passAwayDate : (DateTime?)null;
                                }
                                else
                                {
                                    personalFamily.PassAwayDate = (DateTime?)null;
                                }
                            }
                            else if (item.relateValue == "11")
                            {
                                personalFamily.FatherID = item.idCard;
                            }
                            else if (item.relateValue == "12")
                            {
                                personalFamily.MotherID = item.idCard;
                            }

                        }
                        else
                        {
                            //personalFamily.FamilyMember = "2";
                            personalFamily.FamilyMember = item.relateValue;
                            //  personalFamily.ChildNo = item.relateValue.Substring(item.relateValue.Length - 1, 1);
                            personalFamily.ChildID = item.idCard;
                        }

                        personalFamily.TitleName = item.inameValue;
                        personalFamily.TitleRank = "";
                        personalFamily.Name = item.fname;
                        personalFamily.Surname = item.lname;


                        personalFamily.Sex = item.sexValue;
                        personalFamily.BirthPlace = string.IsNullOrEmpty(item.provinceOfBirth) ? "" : item.provinceOfBirth;
                        personalFamily.CityOfBirth = string.IsNullOrEmpty(item.countryBirthValue) ? "" : item.countryBirthValue;
                        personalFamily.Nationality = string.IsNullOrEmpty(item.nationalityValue) ? "" : item.nationalityValue;
                        personalFamily.MotherSpouseID = string.IsNullOrEmpty(item.idCardMotherSpouse) ? "" : item.idCardMotherSpouse;
                        personalFamily.FatherSpouseID = string.IsNullOrEmpty(item.idCardFatherSpouse) ? "" : item.idCardFatherSpouse;

                        personalFamily.Age = personalFamily.BirthDate != null ? CalculateAge((DateTime)personalFamily.BirthDate) : (int?)null;

                        //SAP("":มีชีวิต,"X":เสียชีวิต)|DHR("0":มีชีวิต,"1":เสียชีวิต)
                        personalFamily.Dead = string.IsNullOrEmpty(item.passAwayF) ? "" : "X";
                        personalFamily.Dead = item.passAwayF == "0" ? "" : "X";
                        if (item.passAwayF == "1")
                        {
                            personalFamily.PassAwayDate = DateTimeService.ConvertDateTime(CompanyCode, item.passAwayDate);
                        }
                        else
                        {
                            personalFamily.PassAwayDate = null;
                        }

                        personalFamily.RelateCode = item.relateCode;
                        personalFamily.RelateCodeTextTH = item.relateCodeTextTH;
                        personalFamily.RelateCodeTextEN = item.relateCodeTextEN;
                        personalFamily.InameCode = item.inameCode;
                        personalFamily.InameCodeTextTH = item.inameCodeTextTH;
                        personalFamily.InameCodeTextEN = item.inameCodeTextEN;
                        personalFamily.FullNameText = item.fullNameText;
                        personalFamily.SexCode = item.sexCode;
                        personalFamily.SexCodeTextTH = item.sexCodeTextTH;
                        personalFamily.SexCodeTextEN = item.sexCodeTextEN;
                        personalFamily.CountryBirthCode = item.countryBirthCode;
                        personalFamily.CountryBirthCodeTextTH = item.countryBirthCodeTextTH;
                        personalFamily.CountryBirthCodeTextEN = item.countryBirthCodeTextEN;
                        personalFamily.NationalityCode = item.nationalityCode;
                        personalFamily.NationalityCodeTextTH = item.nationalityCodeTextTH;
                        personalFamily.NationalityCodeTextEN = item.nationalityCodeTextEN;
                        personalFamily.FamEmpCode = item.famEmpCode;
                        personalFamily.TelephoneNo = item.telNumber;
                        if (fAddress != null)
                        {
                            personalFamily.AddressTypeValue = fAddress.addressTypeValue;
                            personalFamily.Address = string.IsNullOrEmpty(fAddress.addressNo) ? "" : fAddress.addressNo;
                            personalFamily.Street = string.IsNullOrEmpty(fAddress.street) ? "" : fAddress.street;
                            personalFamily.District = string.IsNullOrEmpty(fAddress.districtCode) ? "" : fAddress.districtCode;
                            personalFamily.City = string.IsNullOrEmpty(fAddress.provinceCode) ? "" : fAddress.provinceCode;
                            personalFamily.Postcode = string.IsNullOrEmpty(fAddress.postcode) ? "" : fAddress.postcode;
                            personalFamily.Country = string.IsNullOrEmpty(fAddress.countryValue) ? "" : fAddress.countryValue;
                            personalFamily.Moo = fAddress.moo == null ? 0 : fAddress.moo;
                            personalFamily.City = string.IsNullOrEmpty(fAddress.provinceCode) ? "" : fAddress.provinceCode;
                            personalFamily.Postcode = string.IsNullOrEmpty(fAddress.postcode) ? "" : fAddress.postcode;
                            personalFamily.Building = string.IsNullOrEmpty(fAddress.building) ? "" : fAddress.building;
                            personalFamily.BuildingEn = string.IsNullOrEmpty(fAddress.buildingEn) ? "" : fAddress.buildingEn;
                            personalFamily.Soi = string.IsNullOrEmpty(fAddress.soi) ? "" : fAddress.soi;
                            personalFamily.SubdistrictCode = string.IsNullOrEmpty(fAddress.subdistrictCode) ? "" : fAddress.subdistrictCode;

                            personalFamily.AddressTypeCode = fAddress.addressTypeCode;
                            personalFamily.AddressTypeTextTH = fAddress.addressTypeTextTH;
                            personalFamily.AddressTypeTextEN = fAddress.addressTypeTextEN;
                            personalFamily.SubdistrictTextTH = fAddress.subdistrictTextTH;
                            personalFamily.SubdistrictTextEN = fAddress.subdistrictTextEN;
                            personalFamily.DistrictTextTH = fAddress.districtTextTH;
                            personalFamily.DistrictTextEN = fAddress.districtTextEN;
                            personalFamily.ProvinceTextTH = fAddress.provinceTextTH;
                            personalFamily.ProvinceTextEN = fAddress.provinceTextEN;
                            personalFamily.CountryCode = fAddress.countryCode;
                            personalFamily.CountryTextTH = fAddress.countryTextTH;
                            personalFamily.CountryTextEN = fAddress.countryTextEN;

                        }
                        returnValue.Add(personalFamily);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetPersonalFamilyData Error", responseModel.message);
            }

            return returnValue;
        }

        private static int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }

        public override PersonalFamilyCenter GetPersonalFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        {

            List<PersonalFamilyCenter> returnValue = new List<PersonalFamilyCenter>();

            returnValue = GetPersonalFamilyData(EmployeeID);
            PersonalFamilyCenter returnData = new PersonalFamilyCenter();
            returnData = returnValue.Find(a => a.FamilyMember == FamilyMember && a.FamilyId.ToString() == ChildNo);
            return returnData;
        }

        public override void SavePersonalFamilyData(List<PersonalFamilyCenter> data)
        {
            PostResult postResult = null;
            try
            {
                foreach (PersonalFamilyCenter item in data)
                {
                    PAFamilyInfPost pFamilyInfo = new PAFamilyInfPost();
                    pFamilyInfo.companyCode = CompanyCode;
                    pFamilyInfo.empCode = item.EmployeeID;
                    pFamilyInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                    pFamilyInfo.endDate = DefaultEndDatePost;
                    if (item.FamilyId != 0)
                    {
                        pFamilyInfo.familyId = item.FamilyId;
                    }
                    else
                    {
                        pFamilyInfo.familyId = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAFamilyInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, data[0].EmployeeID).Data.Count + 1;
                    }

                    pFamilyInfo.relateCode = string.IsNullOrEmpty(item.FamilyMember) ? "" : "RELATE_CODE"; //"RELATE_CODE";
                    pFamilyInfo.relateValue = item.FamilyMember;

                    pFamilyInfo.inameCode = string.IsNullOrEmpty(item.TitleName) ? "" : "INAME_CODE"; //"INAME_CODE";
                    pFamilyInfo.inameValue = string.IsNullOrEmpty(item.TitleName) ? "" : item.TitleName;
                    pFamilyInfo.fname = string.IsNullOrEmpty(item.Name) ? "" : item.Name;
                    pFamilyInfo.lname = string.IsNullOrEmpty(item.Surname) ? "" : item.Surname;
                    pFamilyInfo.birthDate = string.IsNullOrEmpty(item.BirthDate.Value.ToString(DefaultDatePost)) ? "" : item.BirthDate.Value.ToString(DefaultDatePost);

                    pFamilyInfo.sexCode = string.IsNullOrEmpty(item.Sex) ? "" : "SEX_CODE";  //"SEX_CODE";
                    pFamilyInfo.sexValue = string.IsNullOrEmpty(item.Sex) ? "" : item.Sex;

                    pFamilyInfo.provinceOfBirth = string.IsNullOrEmpty(item.BirthPlace) ? "" : item.BirthPlace;

                    pFamilyInfo.countryBirthCode = string.IsNullOrEmpty(item.CityOfBirth) ? "" : "COUNTRY_CODE";// "COUNTRY_CODE";
                    pFamilyInfo.countryBirthValue = string.IsNullOrEmpty(item.CityOfBirth) ? "" : item.CityOfBirth;//ประเทศที่เกิด

                    pFamilyInfo.nationalityCode = string.IsNullOrEmpty(item.Nationality) ? "" : "NATIONALITY_CODE";//"NATIONALITY_CODE";
                    pFamilyInfo.nationalityValue = string.IsNullOrEmpty(item.Nationality) ? "" : item.Nationality;

                    pFamilyInfo.idCard = "";
                    pFamilyInfo.passAwayF = string.IsNullOrEmpty(item.Dead) ? "0" : "1"; //SAP("":มีชีวิต,"X":เสียชีวิต)|DHR("0":มีชีวิต,"1":เสียชีวิต)
                    if (pFamilyInfo.passAwayF == "0")
                    {
                        pFamilyInfo.passAwayDate = string.Empty;
                    }
                    else
                    {
                        if (item.PassAwayDate != DateTime.MinValue)
                        {
                            pFamilyInfo.passAwayDate = string.IsNullOrEmpty(item.Dead) ? "" : item.PassAwayDate.Value.ToString(DefaultDatePost);
                        }
                    }
                    pFamilyInfo.maritalDate = "";
                    pFamilyInfo.divorceDate = "";
                    if (item.FamilyMember == "01")
                    {
                        pFamilyInfo.idCard = string.IsNullOrEmpty(item.SpouseID) ? "" : item.SpouseID;
                        if (item.MaritalDate != DateTime.MinValue)
                        {
                            pFamilyInfo.maritalDate = string.IsNullOrEmpty(item.MaritalDate.ToString()) ? "" : item.MaritalDate.Value.ToString(DefaultDatePost);
                        }
                        if (item.DivorceDate != DateTime.MinValue)
                        {
                            pFamilyInfo.divorceDate = string.IsNullOrEmpty(item.DivorceDate.ToString()) ? "" : item.DivorceDate.Value.ToString(DefaultDatePost);
                            // pFamilyInfo.endDate = DateTime.Now.ToString(DefaultDatePost);
                        }
                        if (item.PassAwayDate != DateTime.MinValue)
                        {
                            pFamilyInfo.passAwayDate = string.IsNullOrEmpty(item.Dead) ? "" : item.PassAwayDate.Value.ToString(DefaultDatePost);
                            // pFamilyInfo.endDate = DateTime.Now.ToString(DefaultDatePost);
                        }

                    }
                    else if (item.FamilyMember == "11")
                    {
                        pFamilyInfo.idCard = string.IsNullOrEmpty(item.FatherID) ? "" : item.FatherID;
                    }
                    else if (item.FamilyMember == "12")
                    {
                        pFamilyInfo.idCard = string.IsNullOrEmpty(item.MotherID) ? "" : item.MotherID;
                    }
                    else if (item.FamilyMember.StartsWith("2"))
                    {
                        if ((pFamilyInfo.relateValue + pFamilyInfo.familyId) == (item.FamilyMember + item.ChildNo))
                        {
                            pFamilyInfo.idCard = string.IsNullOrEmpty(item.ChildID) ? "" : item.ChildID;
                        }
                    }

                    pFamilyInfo.telNumber = string.IsNullOrEmpty(item.TelephoneNo) ? "" : item.TelephoneNo;
                    pFamilyInfo.famEmpCode = string.IsNullOrEmpty(item.FamEmpCode) ? "" : item.FamEmpCode; //"FAM_EMP_CODE";

                    pFamilyInfo.idCardFatherSpouse = string.IsNullOrEmpty(item.FatherSpouseID) ? "" : item.FatherSpouseID;
                    pFamilyInfo.idCardMotherSpouse = string.IsNullOrEmpty(item.MotherSpouseID) ? "" : item.MotherSpouseID;

                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAFamilyInf(pFamilyInfo);

                    if (postResult.returnCode != 101)
                    {
                        var DataDetail = JsonConvert.SerializeObject(pFamilyInfo);
                        throw new DataServiceException("HRPA", "SAVE_PERSONALFAMILYDATA", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.Address) || !string.IsNullOrEmpty(item.Building)
                            || !string.IsNullOrEmpty(item.BuildingEn) || !string.IsNullOrEmpty(item.Soi)
                            || !string.IsNullOrEmpty(item.Street) || !string.IsNullOrEmpty(item.SubdistrictCode)
                            || !string.IsNullOrEmpty(item.District) || !string.IsNullOrEmpty(item.City)
                            || !string.IsNullOrEmpty(item.Postcode) || !string.IsNullOrEmpty(item.CountryCode)
                            || !string.IsNullOrEmpty(item.Country))
                        {
                            PAAddressFamilyInfPost addrFam = new PAAddressFamilyInfPost();
                            addrFam.companyCode = CompanyCode;
                            addrFam.empCode = item.EmployeeID;
                            addrFam.startDate = DateTime.Now.ToString(DefaultDatePost);
                            addrFam.endDate = DefaultEndDatePost;
                            addrFam.familyId = pFamilyInfo.familyId;
                            addrFam.relateCode = string.IsNullOrEmpty(pFamilyInfo.relateValue) ? "" : "RELATE_CODE";
                            addrFam.relateValue = string.IsNullOrEmpty(pFamilyInfo.relateValue) ? "" : pFamilyInfo.relateValue;
                            addrFam.addressTypeCode = "ADDRESS_TYPE_CODE";
                            addrFam.addressTypeValue = "002";
                            addrFam.addressNo = string.IsNullOrEmpty(item.Address) ? "" : item.Address;
                            addrFam.moo = item.Moo == null ? 0 : item.Moo;
                            addrFam.building = string.IsNullOrEmpty(item.Building) ? "" : item.Building;
                            addrFam.buildingEn = string.IsNullOrEmpty(item.BuildingEn) ? "" : item.BuildingEn;
                            addrFam.soi = string.IsNullOrEmpty(item.Soi) ? "" : item.Soi;
                            addrFam.street = string.IsNullOrEmpty(item.Street) ? "" : item.Street;
                            addrFam.subdistrictCode = string.IsNullOrEmpty(item.SubdistrictCode) ? "" : item.SubdistrictCode;
                            addrFam.districtCode = string.IsNullOrEmpty(item.District) ? "" : item.District;
                            addrFam.provinceCode = string.IsNullOrEmpty(item.City) ? "" : item.City;
                            addrFam.postcode = string.IsNullOrEmpty(item.Postcode) ? "" : item.Postcode;
                            addrFam.countryCode = string.IsNullOrEmpty(item.Country) ? "" : "COUNTRY_CODE"; //"COUNTRY_CODE";
                            addrFam.countryValue = string.IsNullOrEmpty(item.Country) ? "" : item.Country;

                            postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAAddressFamilyInf(addrFam);
                            if (postResult.returnCode != 101)
                            {
                                var DataDetail = JsonConvert.SerializeObject(addrFam);
                                throw new DataServiceException("HRPA", "SAVE_PERSONALFAMILYADDRDATA", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "SAVE_PERSONALFAMILYDATA", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);

            }
        }

        public override List<EvaluateDataCenter> GetEvaluateEmployee(string oEmployeeID)
        {

            List<EvaluateDataCenter> returnValue = new List<EvaluateDataCenter>();
            ResponseModel<PAEvaluationInf> responseModel = new ResponseModel<PAEvaluationInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAEvaluationInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, oEmployeeID);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {
                        EvaluateDataCenter temp = new EvaluateDataCenter();

                        temp.CompanyCode = item.companyCode;
                        temp.EmpCode = item.empCode;
                        temp.EndDate = item.endDate;
                        temp.StartDate = item.startDate;
                        temp.EvaluationTime = item.evaluationTime;
                        temp.GradeCode = item.gradeCode;
                        temp.GradeTextEN = item.gradeTextEN;
                        temp.GradeTextTH = item.gradeTextTH;
                        temp.GradeValue = item.gradeValue;
                        temp.Period = item.period;
                        temp.UpSalaryPercentage = item.upSalaryPercentage;
                        temp.VariableBonus = item.variableBonus;
                        temp.Average = item.average;
                        temp.Score = item.score;
                        temp.increaseSalary = item.increaseSalary;
                        temp.actualBonus = item.compenAmount;
                        returnValue.Add(temp);
                    }

                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetEvaluateEmployee Error", responseModel.message);
            }

            return returnValue;
        }


        public override List<EmployeeDataCenter> GetEmployeeList(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode, string UnitSelected, string LevelSelected, string PositionSelected)
        {
            List<EmployeeDataCenter> returnValue = new List<EmployeeDataCenter>();
            ResponseModel<PAEmployeeInf> responseModel = new ResponseModel<PAEmployeeInf>();
            try
            {
                string StringBeginDate =  BeginDate.ToString("ddMMyyyy");
                string StringEndDate = BeginDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAEmployeeInfByEmpCode(StringBeginDate, StringEndDate, CompanyCode, EmpCode, UnitSelected, LevelSelected, PositionSelected);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {
                        EmployeeDataCenter personalEmployee = new EmployeeDataCenter();
                        personalEmployee.CompanyCode = item.companyCode;
                        personalEmployee.StartDate = item.startDate;
                        personalEmployee.EndDate = item.endDate;
                        personalEmployee.EmpCode = item.empCode;
                        personalEmployee.EmpNameTH = item.empNameTH;
                        personalEmployee.EmpNameEN = item.empNameEN;
                        personalEmployee.PosCode = item.posCode;
                        personalEmployee.PositionTextTH = item.positionTextTH;
                        personalEmployee.PositionTextEN = item.positionTextEN;
                        personalEmployee.UnitCode = item.unitCode;
                        personalEmployee.UnitTextTh = item.unitTextTh;
                        personalEmployee.UnitTextEN = item.unitTextEN;
                        personalEmployee.UnitLevelValue = item.unitLevelValue;
                        personalEmployee.UnitLevelTextTH = item.unitLevelTextTH;
                        personalEmployee.UnitLevelTextEN = item.unitLevelTextTH;
                        personalEmployee.BandValue = item.bandValue;
                        personalEmployee.BandTextTH = item.bandTextTH;
                        personalEmployee.BandTextEN = item.bandTextEN;


                        returnValue.Add(personalEmployee);
                    }

                    //var distinctResult = returnValue.GroupBy(i => new { i.EducationLevelCode, i.BeginDate, i.EndDate }).Select(g => g.First()).ToList();
                    //   returnValue = distinctResult.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetEmployeeData Error", responseModel.message);
            }

            return returnValue;
        }



      

        public override List<PersonalPositionHisCenter> GetPositionHisByEmpCode(string EmpCode)
        {
            List<PersonalPositionHisCenter> returnValue = new List<PersonalPositionHisCenter>();
            ResponseModel<PAPositionHis> responseModel = new ResponseModel<PAPositionHis>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPositionHisByEmpCode(DefaultBeginDate, DateTime.Now.ToString(DefaultDateGet), EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {

                        DateTime GetDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        PersonalPositionHisCenter personalPositionHis = new PersonalPositionHisCenter();
                        personalPositionHis.CompanyCode = item.companyCode;
                        personalPositionHis.EmpCode = item.empCode;
                        personalPositionHis.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        personalPositionHis.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        personalPositionHis.PosStartDate = item.posStartDate;
                        personalPositionHis.ActionCode = string.IsNullOrEmpty(item.actionCode) ? "" : item.actionCode;
                        personalPositionHis.ActionValue = string.IsNullOrEmpty(item.actionValue) ? "" : item.actionValue;
                        personalPositionHis.ActionTextTH = string.IsNullOrEmpty(item.actionTextTH) ? "" : item.actionTextTH;
                        personalPositionHis.ActionTextEN = string.IsNullOrEmpty(item.actionTextEN) ? "" : item.actionTextEN; ;
                        personalPositionHis.ReasonCode = string.IsNullOrEmpty(item.reasonCode) ? "" : item.reasonCode;
                        personalPositionHis.ReasonValue = string.IsNullOrEmpty(item.reasonValue) ? "" : item.reasonValue;
                        personalPositionHis.ReasonTextTH = string.IsNullOrEmpty(item.reasonTextTH) ? "" : item.reasonTextTH;
                        personalPositionHis.ReasonTextEN = string.IsNullOrEmpty(item.reasonTextEN) ? "" : item.reasonTextEN;
                        personalPositionHis.CommandNo = string.IsNullOrEmpty(item.commandNo) ? "" : item.commandNo;
                        personalPositionHis.CmdTypeCode = string.IsNullOrEmpty(item.cmdTypeCode) ? "" : item.cmdTypeCode;
                        personalPositionHis.CmdTypeValue = string.IsNullOrEmpty(item.cmdTypeValue) ? "" : item.cmdTypeValue;
                        personalPositionHis.CmdTypeTextTH = string.IsNullOrEmpty(item.cmdTypeTextTH) ? "" : item.cmdTypeTextTH;
                        personalPositionHis.CmdTypeTextEN = string.IsNullOrEmpty(item.cmdTypeTextEN) ? "" : item.cmdTypeTextEN;
                        personalPositionHis.CmdName = string.IsNullOrEmpty(item.cmdName) ? "" : item.cmdName;
                        personalPositionHis.CmdNameEn = string.IsNullOrEmpty(item.cmdNameEn) ? "" : item.cmdNameEn;
                       
                        returnValue.Add(personalPositionHis);
                    }

                    //var distinctResult = returnValue.GroupBy(i => new { i.EducationLevelCode, i.BeginDate, i.EndDate }).Select(g => g.First()).ToList();
                    //   returnValue = distinctResult.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetAllPersonalEducationHistory Error", responseModel.message);
            }

            return returnValue;
        }

        public override List<PersonalActingPositionCenter> GetActingPositionByEmpCode(string EmpCode)
        {
            List<PersonalActingPositionCenter> returnValue = new List<PersonalActingPositionCenter>();
            ResponseModel<PAActingPosition> responseModel = new ResponseModel<PAActingPosition>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAActingPositionByEmpCode(DefaultBeginDate, DateTime.Now.ToString(DefaultDateGet), EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {
                        DateTime GetDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        PersonalActingPositionCenter personalActingPositionHis = new PersonalActingPositionCenter();
                        personalActingPositionHis.CompanyCode = item.companyCode;
                        personalActingPositionHis.EmpCode = item.empCode;
                        personalActingPositionHis.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        personalActingPositionHis.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        personalActingPositionHis.PaAcId = item.paAcId;
                        personalActingPositionHis.ActingPosCode = string.IsNullOrEmpty(item.actingPosCode) ? "" : item.actingPosCode;
                        personalActingPositionHis.ActingPosTextTH = string.IsNullOrEmpty(item.actingPosTextTH) ? "" : item.actingPosTextTH;
                        personalActingPositionHis.ActingPosTextEN = string.IsNullOrEmpty(item.actingPosTextEN) ? "" : item.actingPosTextEN;
                        personalActingPositionHis.ActingUnitTextTH =  string.IsNullOrEmpty(item.actingUnitTextTH) ? "" : item.actingUnitTextTH;
                        personalActingPositionHis.ActingUnitTextEN = string.IsNullOrEmpty(item.actingUnitTextEN) ? "" : item.actingUnitTextEN;
                        personalActingPositionHis.ActingBandTextTH = string.IsNullOrEmpty(item.actingBandTextTH) ? "" : item.actingBandTextTH;
                        personalActingPositionHis.ActingBandTextEN = string.IsNullOrEmpty(item.actingBandTextEN) ? "" : item.actingBandTextEN;
                        personalActingPositionHis.ActingHeadOfUnit = string.IsNullOrEmpty(item.actingHeadOfUnit) ? "" : item.actingHeadOfUnit;
                        personalActingPositionHis.PosTextTH = string.IsNullOrEmpty(item.posTextTH) ? "" : item.posTextTH;
                        personalActingPositionHis.PosTextEN = string.IsNullOrEmpty(item.posTextEN) ? "" : item.posTextEN;
                        personalActingPositionHis.UnitTextTH = string.IsNullOrEmpty(item.unitTextTH) ? "" : item.unitTextTH;
                        personalActingPositionHis.UnitTextEN = string.IsNullOrEmpty(item.unitTextEN) ? "" : item.unitTextEN;
                        personalActingPositionHis.BandTextTH = string.IsNullOrEmpty(item.bandTextTH) ? "" : item.bandTextTH;
                        personalActingPositionHis.BandTextEN = string.IsNullOrEmpty(item.bandTextEN) ? "" : item.bandTextEN;
                        personalActingPositionHis.HeadOfUnit = string.IsNullOrEmpty(item.headOfUnit) ? "" : item.headOfUnit;
                        returnValue.Add(personalActingPositionHis);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetAllPersonalEducationHistory Error", responseModel.message);
            }

            return returnValue;
        }

       
        public override List<PersonalImportanceDateCenter> GetImportanceDateByEmpCode(string EmpCode)
        {
            List<PersonalImportanceDateCenter> returnValue = new List<PersonalImportanceDateCenter>();
            ResponseModel<PAImportanceDateInf> responseModel = new ResponseModel<PAImportanceDateInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAImportanceDateInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {
                        
                        DateTime GetDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        PersonalImportanceDateCenter personalImportanceDate = new PersonalImportanceDateCenter();
                        personalImportanceDate.CompanyCode = item.companyCode;
                        personalImportanceDate.EmpCode = item.empCode;
                        personalImportanceDate.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        personalImportanceDate.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        personalImportanceDate.StartWorkDate = DateTimeService.ConvertDateTime(CompanyCode, item.startWorkDate);
                        personalImportanceDate.EmployedDate = DateTimeService.ConvertDateTime(CompanyCode, item.employedDate);
                        personalImportanceDate.CountLeaveAgeDate = DateTimeService.ConvertDateTime(CompanyCode, item.countLeaveAgeDate);
                        personalImportanceDate.RegistFundDate = DateTimeService.ConvertDateTime(CompanyCode, item.registFundDate);
                        personalImportanceDate.CountFundAgeDate = DateTimeService.ConvertDateTime(CompanyCode, item.countFundAgeDate);
                        personalImportanceDate.StartWorkGroupDate = DateTimeService.ConvertDateTime(CompanyCode, item.startWorkGroupDate);
                        personalImportanceDate.LeavePfFundDate1 = DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate1);
                        personalImportanceDate.LeavePfFundDate2 = DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate2);
                        personalImportanceDate.LeavePfFundDate3 = DateTimeService.ConvertDateTime(CompanyCode, item.leavePfFundDate3);
                        personalImportanceDate.RetireDate = DateTimeService.ConvertDateTime(CompanyCode, item.retireDate);
                        personalImportanceDate.CountLevelAgeDate = DateTimeService.ConvertDateTime(CompanyCode, item.countLevelAgeDate);
                        personalImportanceDate.CountLevelAgeGroupDate = DateTimeService.ConvertDateTime(CompanyCode, item.countLevelAgeGroupDate);
                        personalImportanceDate.CountPosAgeDate = DateTimeService.ConvertDateTime(CompanyCode, item.countPosAgeDate);
                        personalImportanceDate.CountUnitAgeDate = DateTimeService.ConvertDateTime(CompanyCode, item.countUnitAgeDate);
                        personalImportanceDate.CountRootJobAgeDate = DateTimeService.ConvertDateTime(CompanyCode, item.countRootJobAgeDate);
                        personalImportanceDate.Id = string.IsNullOrEmpty(item.id) ? "" : item.id;
                        returnValue.Add(personalImportanceDate);
                    }

                    //var distinctResult = returnValue.GroupBy(i => new { i.EducationLevelCode, i.BeginDate, i.EndDate }).Select(g => g.First()).ToList();
                    //   returnValue = distinctResult.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetImportanceDateByEmpCode Error", responseModel.message);
            }

            return returnValue;
        }


        

        public override List<PersonalEducationCenter> GetAllPersonalEducationHistory(string EmployeeID)
        {
            List<PersonalEducationCenter> returnValue = new List<PersonalEducationCenter>();
            ResponseModel<PAEducationInf> responseModel = new ResponseModel<PAEducationInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAEducationInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {

                       DateTime GetDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        PersonalEducationCenter personalEducation = new PersonalEducationCenter();
                        personalEducation.CompanyCode = item.companyCode;
                        personalEducation.EmployeeID = EmployeeID;
                        personalEducation.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        personalEducation.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        //personalEducation.BeginDate =  item.startDate;
                        //personalEducation.EndDate =  item.endDate;
                        personalEducation.PAEduID = item.paEduID;
                        personalEducation.EducationLevelCode = string.IsNullOrEmpty(item.eduLevelValue) ? "" : item.eduLevelValue;
                        personalEducation.InstituteName = string.IsNullOrEmpty(item.instituteValue) ? "" : item.instituteValue;
                        personalEducation.CountryCode = string.IsNullOrEmpty(item.countryValue) ? "" : item.countryValue;
                        personalEducation.VocationCode = "";
                        personalEducation.CertificateCode = string.IsNullOrEmpty(item.certificateValue) ? "" : item.certificateValue;
                        personalEducation.Branch1 = string.IsNullOrEmpty(item.majorValue) ? "" : item.majorValue;
                        personalEducation.Branch2 = string.IsNullOrEmpty(item.minorValue) ? "" : item.minorValue;
                        personalEducation.HonorCode = string.IsNullOrEmpty(item.specialValue) ? "" : item.specialValue;
                        personalEducation.GraduateYear = GetDate.Year.ToString();
                        personalEducation.InstituteTextTH = string.IsNullOrEmpty(item.instituteTextTH) ? "" : item.instituteTextTH;
                        personalEducation.EducationLevelTextTH = string.IsNullOrEmpty(item.eduLevelTextTH) ? "" : item.eduLevelTextTH;
                        personalEducation.CountryTextTH = string.IsNullOrEmpty(item.countryTextTH) ? "" : item.countryTextTH;
                        personalEducation.CertificateTextTH = string.IsNullOrEmpty(item.certificateTextTH) ? "" : item.certificateTextTH;
                        personalEducation.Branch1TextTH = string.IsNullOrEmpty(item.majorTextTH) ? "" : item.majorTextTH;
                        personalEducation.Branch2TextTH = string.IsNullOrEmpty(item.minorTextTH) ? "" : item.minorTextTH;
                        personalEducation.Branch1TextEN = string.IsNullOrEmpty(item.majorTextEN) ? "" : item.majorTextEN;
                        personalEducation.Branch2TextEN = string.IsNullOrEmpty(item.minorTextEN) ? "" : item.minorTextEN;
                        personalEducation.HonorTextTH = string.IsNullOrEmpty(item.specialTextTH) ? "" : item.specialTextTH;
                        personalEducation.HonorTextEN = string.IsNullOrEmpty(item.specialTextEN) ? "" : item.specialTextEN;
                        personalEducation.InstituteTextEN = string.IsNullOrEmpty(item.instituteTextEN) ? "" : item.instituteTextEN;
                        personalEducation.EducationLevelTextEN = string.IsNullOrEmpty(item.eduLevelTextEN) ? "" : item.eduLevelTextEN;
                        personalEducation.CountryTextEN = string.IsNullOrEmpty(item.countryTextEN) ? "" : item.countryTextEN;
                        personalEducation.CertificateTextEN = string.IsNullOrEmpty(item.certificateTextEN) ? "" : item.certificateTextEN;

                       
                      
                       
                      
                       
                     
                        //EduGroupCode = string.Empty;
                        //EduGroupValue = string.Empty;
                        //EduGroupTextTH = string.Empty;
                        //EduGroupTextEN = string.Empty;




                        decimal bufferDecimal;
                        decimal.TryParse(item.grade, out bufferDecimal);
                        personalEducation.Grade = bufferDecimal;
                        personalEducation.GradeText = "";
                        personalEducation.InstituteCode = string.IsNullOrEmpty(item.instituteValue) ? "" : item.instituteValue;
                        personalEducation.EducationGroupCode = string.IsNullOrEmpty(item.eduGroupValue) ? "" : item.eduGroupValue;
                        personalEducation.Unit = "";
                        returnValue.Add(personalEducation);
                    }

                    //var distinctResult = returnValue.GroupBy(i => new { i.EducationLevelCode, i.BeginDate, i.EndDate }).Select(g => g.First()).ToList();
                    //   returnValue = distinctResult.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetAllPersonalEducationHistory Error", responseModel.message);
            }

            return returnValue;
        }


        public override List<WorkHistoryCenter> GetWorkHistory(string EmployeeID)
        {
            List<WorkHistoryCenter> returnValue = new List<WorkHistoryCenter>();
            ResponseModel<PAWorkingHisInf> responseModel = new ResponseModel<PAWorkingHisInf>();

            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAWorkingHisInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {
                        WorkHistoryCenter personalWorkHistory = new WorkHistoryCenter();

                        personalWorkHistory.PaWoId = item.paWoId;
                        personalWorkHistory.CompanyCode = item.companyCode;
                        personalWorkHistory.EmpCode = item.empCode;
                        personalWorkHistory.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        personalWorkHistory.EndDate =  DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        personalWorkHistory.EmployerName =  string.IsNullOrEmpty(item.employerName) ? "" : item.employerName;
                        personalWorkHistory.Position = string.IsNullOrEmpty(item.position) ? "" : item.position;
                        personalWorkHistory.WorkCountryCode = string.IsNullOrEmpty(item.workCountryCode) ? "" : item.workCountryCode;
                        personalWorkHistory.WorkCountryValue = string.IsNullOrEmpty(item.workCountryValue) ? "" : item.workCountryValue;
                        personalWorkHistory.WorkCountryTextTH = string.IsNullOrEmpty(item.workCountryTextTH) ? "" : item.workCountryTextTH;
                        personalWorkHistory.WorkCountryTextEN = string.IsNullOrEmpty(item.workCountryTextEN) ? "" : item.workCountryTextEN;
                        personalWorkHistory.BuCode = string.IsNullOrEmpty(item.buCode) ? "" : item.buCode;
                        personalWorkHistory.BuValue = string.IsNullOrEmpty(item.buValue) ? "" : item.buValue;
                        personalWorkHistory.BuTextTH = string.IsNullOrEmpty(item.buTextTH) ? "" : item.buTextTH;
                        personalWorkHistory.BuTextEN = string.IsNullOrEmpty(item.buTextEN) ? "" : item.buTextEN;
                        personalWorkHistory.JobGrpCode = string.IsNullOrEmpty(item.jobGrpCode) ? "" : item.jobGrpCode;
                        personalWorkHistory.JobGrpValue =  string.IsNullOrEmpty(item.jobGrpValue) ? "" : item.jobGrpValue;
                        personalWorkHistory.JobGrpTextTH = string.IsNullOrEmpty(item.jobGrpTextTH) ? "" : item.jobGrpTextTH;
                        personalWorkHistory.JobGrpTextEN = string.IsNullOrEmpty(item.jobGrpTextEN) ? "" : item.jobGrpTextEN;
                        personalWorkHistory.ActionType = null;
                        personalWorkHistory.ServiceType = null;
                        personalWorkHistory.CreateDate =  string.IsNullOrEmpty(item.createDate) ? "" : item.createDate;
                        personalWorkHistory.CreateBy = string.IsNullOrEmpty(item.createBy) ? "" : item.createBy;
                        personalWorkHistory.UpdateDate = string.IsNullOrEmpty(item.updateDate) ? "" : item.updateDate;
                        personalWorkHistory.UpdateBy =  string.IsNullOrEmpty(item.updateDate) ? "" : item.updateDate;


                        returnValue.Add(personalWorkHistory);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return returnValue;
        }


        public override List<PersonalOrganizationCenter> GeOrganizationByEmpCode(string EmployeeID)
        {
            List<PersonalOrganizationCenter> returnValue = new List<PersonalOrganizationCenter>();
           // ResponseModel<PAWorkingHisInf> responseModel = new ResponseModel<PAWorkingHisInf>();
            ResponseModel<PAOrganizationInf> responseModel = new ResponseModel<PAOrganizationInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAOrganizationInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);
               
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {
                        PersonalOrganizationCenter personalOrganize = new PersonalOrganizationCenter();

                        personalOrganize.CompanyCode = item.companyCode;
                        personalOrganize.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        personalOrganize.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        personalOrganize.EmpCode = item.empCode;
                        personalOrganize.EmpStatusCode = string.IsNullOrEmpty(item.empStatusCode) ? "" : item.empStatusCode;
                        personalOrganize.EmpStatusValue = string.IsNullOrEmpty(item.empStatusValue) ? "" : item.empStatusValue;
                        personalOrganize.EmpStatusTextTH = string.IsNullOrEmpty(item.empStatusTextTH) ? "" : item.empStatusTextTH;
                        personalOrganize.EmpStatusTextEN = string.IsNullOrEmpty(item.empStatusTextEN) ? "" : item.empStatusTextEN;
                        personalOrganize.EmpTypeCode = string.IsNullOrEmpty(item.empTypeCode) ? "" : item.empTypeCode;
                        personalOrganize.EmpTypeValue = string.IsNullOrEmpty(item.empTypeValue) ? "" : item.empTypeValue;
                        personalOrganize.EmpTypeTextTH = string.IsNullOrEmpty(item.empTypeTextTH) ? "" : item.empTypeTextTH;
                        personalOrganize.EmpTypeTextEN = string.IsNullOrEmpty(item.empTypeTextEN) ? "" : item.empTypeTextEN;
                        personalOrganize.PosCode = string.IsNullOrEmpty(item.posCode) ? "" : item.posCode;
                        personalOrganize.PosTextTH = string.IsNullOrEmpty(item.posTextTH) ? "" : item.posTextTH;
                        personalOrganize.PosTextEN = string.IsNullOrEmpty(item.posTextEN) ? "" : item.posTextEN;
                        personalOrganize.SpcPosName = string.IsNullOrEmpty(item.spcPosName) ? "" : item.spcPosName;
                        personalOrganize.UnitCode = string.IsNullOrEmpty(item.unitCode) ? "" : item.unitCode;
                        personalOrganize.UnitTextTH = string.IsNullOrEmpty(item.unitTextTH) ? "" : item.unitTextTH;
                        personalOrganize.UnitTextEN = string.IsNullOrEmpty(item.unitTextEN) ? "" : item.unitTextEN;
                        personalOrganize.CostCenterCode = string.IsNullOrEmpty(item.costCenterCode) ? "" : item.costCenterCode;
                        personalOrganize.CostCenterValue = string.IsNullOrEmpty(item.costCenterValue) ? "" : item.costCenterValue;
                        personalOrganize.LevelCode = string.IsNullOrEmpty(item.levelCode) ? "" : item.levelCode;
                        personalOrganize.LevelValue = string.IsNullOrEmpty(item.levelValue) ? "" : item.levelValue;
                        personalOrganize.LevelText = string.IsNullOrEmpty(item.levelText) ? "" : item.levelText;
                        personalOrganize.ActingLevelCode = string.IsNullOrEmpty(item.actingLevelCode) ? "" : item.actingLevelCode;
                        personalOrganize.ActingLevelValue = string.IsNullOrEmpty(item.actingLevelValue) ? "" : item.actingLevelValue;
                        personalOrganize.ActingLevelText = string.IsNullOrEmpty(item.actingLevelText) ? "" : item.actingLevelText;
                        personalOrganize.BandCode = string.IsNullOrEmpty(item.bandCode) ? "" : item.bandCode;
                        personalOrganize.BandValue = string.IsNullOrEmpty(item.bandValue) ? "" : item.bandValue;
                        personalOrganize.BandTextTH = string.IsNullOrEmpty(item.bandTextTH) ? "" : item.bandTextTH;
                        personalOrganize.BandTextEN = string.IsNullOrEmpty(item.bandTextEN) ? "" : item.bandTextEN;
                        personalOrganize.ActingBandCode = string.IsNullOrEmpty(item.actingBandCode) ? "" : item.actingBandCode;
                        personalOrganize.ActingBandValue = string.IsNullOrEmpty(item.actingBandValue) ? "" : item.actingBandValue;
                        personalOrganize.ActingBandTextTH = string.IsNullOrEmpty(item.actingBandTextTH) ? "" : item.actingBandTextTH;
                        personalOrganize.ActingBandTextEN = string.IsNullOrEmpty(item.actingBandTextEN) ? "" : item.actingBandTextEN;
                        personalOrganize.CountryWorkCode = string.IsNullOrEmpty(item.countryWorkCode) ? "" : item.countryWorkCode;
                        personalOrganize.CountryWorkValue = string.IsNullOrEmpty(item.countryWorkValue) ? "" : item.countryWorkValue;
                        personalOrganize.CountryWorkTextTH = string.IsNullOrEmpty(item.countryWorkTextTH) ? "" : item.countryWorkTextTH;
                        personalOrganize.CountryWorkTextEN = string.IsNullOrEmpty(item.countryWorkTextEN) ? "" : item.countryWorkTextEN;
                        personalOrganize.WorkPlaceCode = string.IsNullOrEmpty(item.workPlaceCode) ? "" : item.workPlaceCode;
                        personalOrganize.WorkPlaceValue = string.IsNullOrEmpty(item.workPlaceValue) ? "" : item.workPlaceValue;
                        personalOrganize.WorkPlaceTextTH = string.IsNullOrEmpty(item.workPlaceTextTH) ? "" : item.workPlaceTextTH;
                        personalOrganize.WorkPlaceTextEN = string.IsNullOrEmpty(item.workPlaceTextEN) ? "" : item.workPlaceTextEN;
                        personalOrganize.WorkAreaCode = string.IsNullOrEmpty(item.workAreaCode) ? "" : item.workAreaCode;
                        personalOrganize.WorkAreaValue = string.IsNullOrEmpty(item.workAreaValue) ? "" : item.workAreaValue;
                        personalOrganize.WorkAreaTextTH = string.IsNullOrEmpty(item.workAreaTextTH) ? "" : item.workAreaTextTH;
                        personalOrganize.WorkAreaTextEN = string.IsNullOrEmpty(item.workAreaTextEN) ? "" : item.workAreaTextEN;
                        personalOrganize.WorkTimeCode = string.IsNullOrEmpty(item.workTimeCode) ? "" : item.workTimeCode;
                        personalOrganize.WorkTimeValue = string.IsNullOrEmpty(item.workTimeValue) ? "" : item.workTimeValue;
                        personalOrganize.WorkTimeTextTH = string.IsNullOrEmpty(item.workTimeTextTH) ? "" : item.workTimeTextTH;
                        personalOrganize.WorkTimeTextEN = string.IsNullOrEmpty(item.workTimeTextEN) ? "" : item.workTimeTextEN;
                        personalOrganize.ReportToPosCode = string.IsNullOrEmpty(item.reportToPosCode) ? "" : item.reportToPosCode;
                        personalOrganize.ReportToPosTextTH = string.IsNullOrEmpty(item.reportToPosTextTH) ? "" : item.reportToPosTextTH;
                        personalOrganize.ReportToPosTextEN = string.IsNullOrEmpty(item.reportToPosTextEN) ? "" : item.reportToPosTextEN;
                        personalOrganize.ReportToBandCode = string.IsNullOrEmpty(item.reportToBandCode) ? "" : item.reportToBandCode;
                        personalOrganize.ReportToBandValue = string.IsNullOrEmpty(item.reportToBandValue) ? "" : item.reportToBandValue;
                        personalOrganize.ReportToBandTextTH = string.IsNullOrEmpty(item.reportToBandTextTH) ? "" : item.reportToBandTextTH;
                        personalOrganize.ReportToBandTextEN = string.IsNullOrEmpty(item.reportToBandTextEN) ? "" : item.reportToBandTextEN;
                        personalOrganize.SpcPosNameEn = string.IsNullOrEmpty(item.spcPosNameEn) ? "" : item.spcPosNameEn;
                        personalOrganize.CostCenterText = string.IsNullOrEmpty(item.costCenterText) ? "" : item.costCenterText;
                        personalOrganize.LevelTextTH = string.IsNullOrEmpty(item.levelTextTH) ? "" : item.levelTextTH;
                        personalOrganize.LevelTextEN = string.IsNullOrEmpty(item.levelTextEN) ? "" : item.levelTextEN;
                        personalOrganize.ActingLevelTextTH = string.IsNullOrEmpty(item.actingLevelTextTH) ? "" : item.actingLevelTextTH;
                        personalOrganize.ActingLevelTextEN = string.IsNullOrEmpty(item.actingLevelTextEN) ? "" : item.actingLevelTextEN;
                        personalOrganize.HrAdminCode = string.IsNullOrEmpty(item.hrAdminCode) ? "" : item.hrAdminCode;
                        personalOrganize.HrAdminValue = string.IsNullOrEmpty(item.hrAdminValue) ? "" : item.hrAdminValue;
                        personalOrganize.HrAdminTextTH = string.IsNullOrEmpty(item.hrAdminTextTH) ? "" : item.hrAdminTextTH;
                        personalOrganize.HrAdminTextEN = string.IsNullOrEmpty(item.hrAdminTextEN) ? "" : item.hrAdminTextEN;
                        personalOrganize.ActionType = string.IsNullOrEmpty(item.actionType) ? "" : item.actionType;
                        personalOrganize.ServiceType = string.IsNullOrEmpty(item.serviceType) ? "" : item.serviceType;
                        personalOrganize.Id = string.IsNullOrEmpty(item.id) ? "" : item.id;
                        returnValue.Add(personalOrganize);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return returnValue;
        }



        public override void SavePersonalEducation(List<PersonalEducationCenter> data)
        {
            PostResult postResult = null;
            try
            {
                foreach (PersonalEducationCenter item in data)
                {
                    PAEducationInfPost pEducationInfo = new PAEducationInfPost();
                    pEducationInfo.companyCode = CompanyCode;
                    pEducationInfo.empCode = item.EmployeeID;
                    pEducationInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pEducationInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pEducationInfo.paEduID = item.PAEduID;
                    pEducationInfo.eduLevelCode = string.IsNullOrEmpty(item.EducationLevelCode) ? "" : "EDU_LEVEL_CODE";
                    pEducationInfo.eduLevelValue = string.IsNullOrEmpty(item.EducationLevelCode) ? "" : item.EducationLevelCode;
                    pEducationInfo.instituteCode = string.IsNullOrEmpty(item.InstituteCode) ? "" : "INSTITUTE_CODE";
                    pEducationInfo.instituteValue = string.IsNullOrEmpty(item.InstituteCode) ? "" : item.InstituteCode;
                    pEducationInfo.countryCode = string.IsNullOrEmpty(item.CountryCode) ? "" : "COUNTRY_CODE";
                    pEducationInfo.countryValue = string.IsNullOrEmpty(item.CountryCode) ? "" : item.CountryCode;
                    pEducationInfo.certificateCode = string.IsNullOrEmpty(item.CertificateCode) ? "" : "CERTIFICATE_EDU_CODE";
                    pEducationInfo.certificateValue = string.IsNullOrEmpty(item.CertificateCode) ? "" : item.CertificateCode;
                    pEducationInfo.grade = item.Grade.ToString();
                    pEducationInfo.majorCode = string.IsNullOrEmpty(item.Branch1) ? "" : "MAJOR_CODE";
                    pEducationInfo.majorValue = string.IsNullOrEmpty(item.Branch1) ? "" : item.Branch1;
                    pEducationInfo.minorCode = string.IsNullOrEmpty(item.Branch2) ? "" : "MAJOR_CODE";
                    pEducationInfo.minorValue = string.IsNullOrEmpty(item.Branch2) ? "" : item.Branch2;
                    pEducationInfo.specialCode = string.IsNullOrEmpty(item.HonorCode) ? "" : "SPECIAL_CODE";
                    pEducationInfo.specialValue = string.IsNullOrEmpty(item.HonorCode) ? "" : item.HonorCode;
                    pEducationInfo.eduGroupCode = string.IsNullOrEmpty(item.EducationGroupCode) ? "" : "EDU_GROUP_CODE";
                    pEducationInfo.eduGroupValue = string.IsNullOrEmpty(item.EducationGroupCode) ? "" : item.EducationGroupCode;
                    //pEducationInfo.actionType = !string.IsNullOrEmpty(item.PAEduID) ? "U" : "I";
                    pEducationInfo.actionType = item.ActionType;
                    pEducationInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAEducationInf(pEducationInfo);
                    if (postResult.returnCode == 101)
                    {
                        //Do Nothing
                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override string ValidateEducation(List<PersonalEducationCenter> data)
        {
            string oMessage = "SUCCESS";
            PostResult postResult = null;
            try
            {
                foreach (PersonalEducationCenter item in data)
                {
                    PAEducationInfPost pEducationInfo = new PAEducationInfPost();
                    pEducationInfo.companyCode = CompanyCode;
                    pEducationInfo.empCode = item.EmployeeID;
                    pEducationInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pEducationInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pEducationInfo.paEduID = item.PAEduID;
                    pEducationInfo.eduLevelCode = string.IsNullOrEmpty(item.EducationLevelCode) ? "" : "EDU_LEVEL_CODE";
                    pEducationInfo.eduLevelValue = string.IsNullOrEmpty(item.EducationLevelCode) ? "" : item.EducationLevelCode;
                    pEducationInfo.instituteCode = string.IsNullOrEmpty(item.InstituteCode) ? "" : "INSTITUTE_CODE";
                    pEducationInfo.instituteValue = string.IsNullOrEmpty(item.InstituteCode) ? "" : item.InstituteCode;
                    pEducationInfo.countryCode = string.IsNullOrEmpty(item.CountryCode) ? "" : "COUNTRY_CODE";
                    pEducationInfo.countryValue = string.IsNullOrEmpty(item.CountryCode) ? "" : item.CountryCode;
                    pEducationInfo.certificateCode = string.IsNullOrEmpty(item.CertificateCode) ? "" : "CERTIFICATE_EDU_CODE";
                    pEducationInfo.certificateValue = string.IsNullOrEmpty(item.CertificateCode) ? "" : item.CertificateCode;
                    pEducationInfo.grade = item.Grade.ToString();
                    pEducationInfo.majorCode = string.IsNullOrEmpty(item.Branch1) ? "" : "MAJOR_CODE";
                    pEducationInfo.majorValue = string.IsNullOrEmpty(item.Branch1) ? "" : item.Branch1;
                    pEducationInfo.minorCode = string.IsNullOrEmpty(item.Branch2) ? "" : "MAJOR_CODE";
                    pEducationInfo.minorValue = string.IsNullOrEmpty(item.Branch2) ? "" : item.Branch2;
                    pEducationInfo.specialCode = string.IsNullOrEmpty(item.HonorCode) ? "" : "SPECIAL_CODE";
                    pEducationInfo.specialValue = string.IsNullOrEmpty(item.HonorCode) ? "" : item.HonorCode;
                    pEducationInfo.eduGroupCode = string.IsNullOrEmpty(item.EducationGroupCode) ? "" : "EDU_GROUP_CODE";
                    pEducationInfo.eduGroupValue = string.IsNullOrEmpty(item.EducationGroupCode) ? "" : item.EducationGroupCode;
                    //pEducationInfo.actionType = !string.IsNullOrEmpty(item.PAEduID) ? "U" : "I";
                    pEducationInfo.actionType = item.ActionType;
                    pEducationInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAEducationInf(pEducationInfo);
                    if (postResult.returnCode == 101)
                    {
                        oMessage = "SUCCESS";

                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                    //if (postResult.returnCode != 101)
                    //{
                    //    var DataDetail = JsonConvert.SerializeObject(pEducationInfo);
                    //    throw new DataServiceException("HRPA", "SAVE_PERSONALEDUCATION", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                    //}
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                oMessage = "PROCESS ERROR : " + ex.Message;
            }
            return oMessage;
        }
        

         public override string ValidateAcademicWorkMember(List<PersonalAcademicMemberCenter> data)
         {
            string oMessage = "SUCCESS";
            PostResult postResult = null;
            try
            {
                foreach (PersonalAcademicMemberCenter item in data)
                {
                    PAAcademicMemberInfPost pAcademicMemberInfo = new PAAcademicMemberInfPost();
                    pAcademicMemberInfo.companyCode = CompanyCode;
                    pAcademicMemberInfo.empCode = item.EmpCode;
                    pAcademicMemberInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pAcademicMemberInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pAcademicMemberInfo.paReMemId = item.PAReMemID;
                    pAcademicMemberInfo.paReId = item.PAReID;
                    pAcademicMemberInfo.roleCode = string.IsNullOrEmpty(item.RoleValue) ? "" : "ROLE_CODE";
                    pAcademicMemberInfo.roleValue = string.IsNullOrEmpty(item.RoleValue) ? "" : item.RoleValue;


                    //pEducationInfo.actionType = !string.IsNullOrEmpty(item.PAEduID) ? "U" : "I";
                    pAcademicMemberInfo.actionType = item.ActionType;
                    pAcademicMemberInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAAcademicMemberInf(pAcademicMemberInfo);
                    if (postResult.returnCode == 101)
                    {
                        oMessage = "SUCCESS";

                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                oMessage = "PROCESS ERROR : " + ex.Message;
            }
            return oMessage;
        }

        public override void SavePersonalAcademicWorkMember(List<PersonalAcademicMemberCenter> data)
        {
            
            PostResult postResult = null;
            try
            {
                foreach (PersonalAcademicMemberCenter item in data)
                {
                    PAAcademicMemberInfPost pAcademicMemberInfo = new PAAcademicMemberInfPost();
                    pAcademicMemberInfo.companyCode = CompanyCode;
                    pAcademicMemberInfo.empCode = item.EmpCode;
                    pAcademicMemberInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pAcademicMemberInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pAcademicMemberInfo.paReMemId = item.PAReMemID;
                    pAcademicMemberInfo.paReId = item.PAReID;
                    pAcademicMemberInfo.roleCode = string.IsNullOrEmpty(item.RoleValue) ? "" : "ROLE_CODE";
                    pAcademicMemberInfo.roleValue = string.IsNullOrEmpty(item.RoleValue) ? "" : item.RoleValue;


                    //pEducationInfo.actionType = !string.IsNullOrEmpty(item.PAEduID) ? "U" : "I";
                    pAcademicMemberInfo.actionType = item.ActionType;
                    pAcademicMemberInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAAcademicMemberInf(pAcademicMemberInfo);
                    if (postResult.returnCode == 101)
                    {
                        //oMessage = "SUCCESS";

                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



       
        public override string ValidateWorkHistory(List<WorkHistoryCenter> data)
        {

            string oMessage = "SUCCESS";
            PostResult postResult = null;
            try
            {
                foreach (WorkHistoryCenter item in data)
                {
                    PAWorkHistoryInfPost pWorkingHistoryInfo = new PAWorkHistoryInfPost();
                    pWorkingHistoryInfo.companyCode = CompanyCode;
                    pWorkingHistoryInfo.empCode = item.EmpCode;
                    pWorkingHistoryInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pWorkingHistoryInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pWorkingHistoryInfo.paWoId = item.PaWoId;
                    pWorkingHistoryInfo.employerName = string.IsNullOrEmpty(item.EmployerName) ? "" : item.EmployerName;
                    pWorkingHistoryInfo.position = string.IsNullOrEmpty(item.Position) ? "" : item.Position;
                    pWorkingHistoryInfo.workCountryCode = string.IsNullOrEmpty(item.WorkCountryValue) ? "" : "COUNTRY_CODE";
                    pWorkingHistoryInfo.workCountryValue = string.IsNullOrEmpty(item.WorkCountryValue) ? "" : item.WorkCountryValue;
                    pWorkingHistoryInfo.buCode = string.IsNullOrEmpty(item.BuValue) ? "" : "BU_CODE";
                    pWorkingHistoryInfo.buValue = string.IsNullOrEmpty(item.BuValue) ? "" : item.BuValue;
                    pWorkingHistoryInfo.jobGrpCode = null;
                    pWorkingHistoryInfo.jobGrpValue = null;
                    pWorkingHistoryInfo.actionType = item.ActionType;
                    pWorkingHistoryInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAWorkHisotryInf(pWorkingHistoryInfo);
                    if (postResult.returnCode == 101)
                    {
                        oMessage = "SUCCESS";

                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                oMessage = "PROCESS ERROR : " + ex.Message;
            }
            return oMessage;
            //catch (Exception ex)
            //{
            //    throw new DataServiceException("HRPA", "SAVE_PERSONALEDUCATION", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
            //}
        }



        public override void SaveWorkHistory(List<WorkHistoryCenter> data)
        {
            PostResult postResult = null;
            try
            {
                foreach (WorkHistoryCenter item in data)
                {
                    PAWorkHistoryInfPost pWorkingHistoryInfo = new PAWorkHistoryInfPost();
                    pWorkingHistoryInfo.companyCode = CompanyCode;
                    pWorkingHistoryInfo.empCode = item.EmpCode;
                    pWorkingHistoryInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pWorkingHistoryInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pWorkingHistoryInfo.paWoId = item.PaWoId;
                    pWorkingHistoryInfo.employerName = string.IsNullOrEmpty(item.EmployerName) ? "" : item.EmployerName;
                    pWorkingHistoryInfo.position = string.IsNullOrEmpty(item.Position) ? "" : item.Position;
                    pWorkingHistoryInfo.workCountryCode = string.IsNullOrEmpty(item.WorkCountryValue) ? "" : "COUNTRY_CODE";
                    pWorkingHistoryInfo.workCountryValue = string.IsNullOrEmpty(item.WorkCountryValue) ? "" : item.WorkCountryValue;
                    pWorkingHistoryInfo.buCode = string.IsNullOrEmpty(item.BuValue) ? "" : "BU_CODE";
                    pWorkingHistoryInfo.buValue = string.IsNullOrEmpty(item.BuValue) ? "" : item.BuValue;
                    pWorkingHistoryInfo.jobGrpCode = null;
                    pWorkingHistoryInfo.jobGrpValue = null;
                    pWorkingHistoryInfo.actionType = item.ActionType;
                    pWorkingHistoryInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAWorkHisotryInf(pWorkingHistoryInfo);
                    if (postResult.returnCode == 101)
                    {
                        //Do Nothing
                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        

        public override string ValidateTranning(List<PersonalTrainingCenter> data)
        {
            string oMessage = "SUCCESS";
            PostResult postResult = null;
            try
            {
                foreach (PersonalTrainingCenter item in data)
                {
                    PATrainingHis pTrainningHistoryInfo = new PATrainingHis();
                    pTrainningHistoryInfo.companyCode = CompanyCode;
                    pTrainningHistoryInfo.empCode = item.EmployeeID;
                    pTrainningHistoryInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pTrainningHistoryInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pTrainningHistoryInfo.paTrId = item.PaTrId;
                    pTrainningHistoryInfo.courseName = string.IsNullOrEmpty(item.CourseName) ? "" : item.CourseName;
                    pTrainningHistoryInfo.actionType = item.ActionType;
                    pTrainningHistoryInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPATrainningHisotryInf(pTrainningHistoryInfo);
                    if (postResult.returnCode == 101)
                    {
                        oMessage = "SUCCESS";

                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                oMessage = "PROCESS ERROR : " + ex.Message;
            }
            return oMessage;
        }



        public override void SaveTrainningHistory(List<PersonalTrainingCenter> data)
        {
            PostResult postResult = null;
            try
            {
                foreach (PersonalTrainingCenter item in data)
                {
                    PATrainingHis pTrainningHistoryInfo = new PATrainingHis();
                    pTrainningHistoryInfo.companyCode = CompanyCode;
                    pTrainningHistoryInfo.empCode = item.EmployeeID;
                    pTrainningHistoryInfo.startDate = item.BeginDate.ToString(DefaultDatePost);
                    pTrainningHistoryInfo.endDate = item.EndDate.ToString(DefaultDatePost);
                    pTrainningHistoryInfo.paTrId = item.PaTrId;
                    pTrainningHistoryInfo.courseName = string.IsNullOrEmpty(item.CourseName) ? "" : item.CourseName;
                    pTrainningHistoryInfo.actionType = item.ActionType;
                    pTrainningHistoryInfo.serviceType = item.ServiceType;
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPATrainningHisotryInf(pTrainningHistoryInfo);
                    if (postResult.returnCode == 101)
                    {
                        //oMessage = "SUCCESS";

                    }
                    else if (postResult.returnCode == 501 || postResult.returnCode == 601) // Warning | Comfirmation alert
                    {
                        // Do Something                    
                        //oMessage = "ERROR" + " : " + oPost.returnDesc;
                    }
                    else
                    {
                        var oArgs = String.IsNullOrEmpty(postResult.returnParam) ? new String[0] : postResult.returnParam.Split('|');
                        if (!String.IsNullOrEmpty(postResult.returnBusinessCode))
                            throw new DataServiceException(String.Empty, postResult.returnBusinessCode, oArgs);
                        else
                        {
                            throw new Exception(postResult.returnDesc);
                        }
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #region Medical Check up

        public override List<PAMedicalCheckup> MedicalCheckUP_Year_EmpID(string Year, string EmployeeId)
        {
            List<PAMedicalCheckup> Gethealth = new List<PAMedicalCheckup>();
            ResponseModel<PAMedicalCheckup> responseModel = new ResponseModel<PAMedicalCheckup>();

            DateTime Startdate = DateTimeService.ConvertDateTime(CompanyCode, PeriodStartDate + Year);//new DateTime(int.Parse(Year), 1, 1).ToString(DefaultDatePost);
            DateTime Enddate = Startdate.AddYears(1).AddDays(-1); //new DateTime(int.Parse(Year), 12, 31).ToString(DefaultDatePost);
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAMedicalCheckupByEmpCode(Startdate.ToString(DefaultDateGet), Enddate.ToString(DefaultDateGet), EmployeeId);
                List<PAMedicalCheckup> paMedicalCheckup = responseModel.Data;
                List<PAMedicalCheckup> Gethealth3 = new List<PAMedicalCheckup>();


                foreach (PAMedicalCheckup item in paMedicalCheckup)
                {

                    PAMedicalCheckup personalMedicalCheckup = new PAMedicalCheckup();
                    personalMedicalCheckup.companyCode = CompanyCode;
                    personalMedicalCheckup.empCode = EmployeeId;
                    personalMedicalCheckup.startDate = item.startDate;
                    personalMedicalCheckup.endDate = item.endDate;
                    personalMedicalCheckup.sequence = item.sequence;
                    personalMedicalCheckup.checkupCode = item.checkupCode;
                    personalMedicalCheckup.checkupValue = item.checkupValue;
                    personalMedicalCheckup.checkupTextTH = item.checkupTextTH;
                    personalMedicalCheckup.checkupTextEN = item.checkupTextEN;
                    personalMedicalCheckup.normCheckResult = item.normCheckResult;
                    personalMedicalCheckup.abnormCheckResult = item.abnormCheckResult;
                    personalMedicalCheckup.doctorComment = item.doctorComment;

                    Gethealth.Add(personalMedicalCheckup);
                }

            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "MedicalCheckUP Error", responseModel.message);
            }

            return Gethealth;

        }
        #endregion Medical Check up

        #region " PreviousEmployers "
        public override List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        {
            List<INFOTYPE0023> returnValue = new List<INFOTYPE0023>();
            ResponseModel<PAWorkingHisInf> responseModel = new ResponseModel<PAWorkingHisInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAWorkingHisInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);
                foreach (var item in responseModel.Data)
                {
                    INFOTYPE0023 dataItem = new INFOTYPE0023();
                    dataItem.EmployeeID = EmployeeID;
                    //DateTime bufferDate = DateTime.MinValue;
                    //DateTime.TryParseExact(item.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None, out bufferDate);
                    dataItem.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                    //DateTime.TryParseExact(item.endDate, DefaultReturnDateFormat, null, DateTimeStyles.None, out bufferDate);
                    dataItem.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                    dataItem.Employer = item.employerName;
                    dataItem.Position = item.buTextTH + " " + item.jobGrpTextTH;
                    dataItem.CountryCode = item.workCountryValue;
                    dataItem.CompanyBusinessCode = item.buValue;
                    dataItem.JobCode = item.jobGrpValue;

                    returnValue.Add(dataItem);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return returnValue;
        }
        #endregion " PreviousEmployers "

        public void SearchINFOTYPE0002(FilterData filter)
        {
            throw new NotImplementedException();
        }

        public override List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        {
            List<INFOTYPE9002> returnValue = new List<INFOTYPE9002>();
            ResponseModel<PAWorkingHisInf> responseModel = new ResponseModel<PAWorkingHisInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAWorkingHisInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);

                foreach (var item in responseModel.Data)
                {
                    INFOTYPE9002 dataItem = new INFOTYPE9002();
                    //dataItem.dhrWorkingHisInf = item;
                    dataItem.EmployeeID = item.empCode;

                    //DateTime bufferDate = DateTime.MinValue;
                    //DateTime.TryParseExact(item.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None, out bufferDate);
                    dataItem.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                    dataItem.PositionName = "";
                    dataItem.UNIT = item.jobGrpValue;
                    returnValue.Add(dataItem);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return returnValue;
        }

        public List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        {
            List<INFOTYPE9001> returnValue = new List<INFOTYPE9001>();

            return returnValue;
        }

        public override List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        {
            List<INFOTYPE0008> returnValue = new List<INFOTYPE0008>();
            ResponseModel<PYWageInf> responseModel = new ResponseModel<PYWageInf>();
            try
            {
                responseModel = APIPayrollManagement.CreateInstance(CompanyCode).GetDHRPYWageInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);

                if (responseModel.Data.Count > 0)
                {
                    foreach (PYWageInf row in responseModel.Data)
                    {
                        INFOTYPE0008 oTemp = new INFOTYPE0008();
                        //oTemp.BeginDate = DateTime.ParseExact(row.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None);
                        oTemp.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, row.startDate);
                        //oTemp.EndDate = DateTime.ParseExact(row.endDate, DefaultReturnDateFormat, null, DateTimeStyles.None);
                        oTemp.EndDate = DateTimeService.ConvertDateTime(CompanyCode, row.endDate);
                        oTemp.EmployeeID = EmployeeID;

                        //PYWageInf oPYWageInf = new PYWageInf();
                        Type oType = row.GetType();
                        PropertyInfo oWageTypeCode;
                        PropertyInfo oAmount;


                        for (int i = 1; i <= 5; i++)
                        {
                            string index = i.ToString("00");
                            oWageTypeCode = oType.GetProperty(string.Format("wageTypeCode{0}", index));
                            oAmount = oType.GetProperty(string.Format("amount{0}", index));

                            string oWT = (string)oWageTypeCode.GetValue(row, null);

                            if (oWT == "1000")
                            {
                                oTemp.Salary = Convert.ToDecimal(oAmount.GetValue(row, null));
                                break;
                            }
                        }

                        returnValue.Add(oTemp);
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            return returnValue;
        }

        public override List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate)
        {
            List<INFOTYPE0008> returnValue = new List<INFOTYPE0008>();

            return returnValue;
        }

        public override List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        {
            List<INFOTYPE9003> returnValue = new List<INFOTYPE9003>();
            ResponseModel<PAEvaluationInf> responseModel = new ResponseModel<PAEvaluationInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAEvaluationInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);

                foreach (var item in responseModel.Data)
                {
                    INFOTYPE9003 dataItem = new INFOTYPE9003();
                    dataItem.EmployeeID = EmployeeID;

                    //DateTime dt = DateTime.MinValue;
                    //DateTime.TryParseExact(item.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None, out dt);
                    dataItem.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                    dataItem.EvaluationResult = "";
                    dataItem.CompanyAdjustRate = item.upSalaryPercentage;
                    dataItem.PersonalAdjustRate = 0;
                    dataItem.BaseSalaryAdjust = 0;
                    dataItem.AmountSalaryAdjust = 0;
                    dataItem.SpecailSalaryAdjust = 0;
                    dataItem.Bonus1 = item.variableBonus;
                    dataItem.SpecialBonus = 0;
                    dataItem.Remark = "";
                    returnValue.Add(dataItem);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return returnValue;
        }

        public override List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        {
            List<INFOTYPE9004> returnValue = new List<INFOTYPE9004>();
            ResponseModel<PAEvaluationInf> responseModel = new ResponseModel<PAEvaluationInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAEvaluationInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);
                foreach (var item in responseModel.Data)
                {
                    INFOTYPE9004 dataItem = new INFOTYPE9004();
                    dataItem.EmployeeID = EmployeeID;
                    //DateTime dt = DateTime.MinValue;
                    //DateTime.TryParseExact(item.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None, out dt);
                    dataItem.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                    //DateTime.TryParseExact(item.endDate, DefaultReturnDateFormat, null, DateTimeStyles.None, out dt);
                    dataItem.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                    dataItem.Level = item.gradeValue;
                    returnValue.Add(dataItem);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return returnValue;
        }

        public override List<INFOTYPE0022> GetAllCertificateHistory(string EmployeeID)
        {
            List<INFOTYPE0022> returnValue = new List<INFOTYPE0022>();
            ResponseModel<PACertificateInf> responseModel = new ResponseModel<PACertificateInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPACertificateInfByEmpCode(DefaultBeginDate, DefaultEndDateGet, EmployeeID);
                foreach (var item in responseModel.Data)
                {
                    INFOTYPE0022 dataItem = new INFOTYPE0022();
                    //DateTime bufferDate = DateTime.MinValue;
                    //DateTime.TryParseExact(item.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None, out bufferDate);
                    dataItem.EmployeeID = EmployeeID;
                    dataItem.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                    dataItem.CertificateCode = item.certificateValue;
                    dataItem.Grade = item.score;
                    dataItem.Remark = "";
                    returnValue.Add(dataItem);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return returnValue;
        }

        public void DeleteEducation(List<INFOTYPE0022> data)
        {
            throw new NotImplementedException();
        }

        public void DeleteFamilyData(List<INFOTYPE0021> data)
        {
            throw new NotImplementedException();
        }

        public INFOTYPE0008 GetInfotype0008(string EmployeeID, DateTime CheckDate)
        {
            throw new NotImplementedException();
        }

        //get location name such as Rayong Office...
        public override List<WorkPlaceData> GetWorkPlace()
        {
            List<WorkPlaceData> returnValue = new List<WorkPlaceData>();
            ResponseModel<PAOrganizationInf> responseModel = new ResponseModel<PAOrganizationInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAOrganizationInf(DefaultBeginDate, DefaultEndDateGet);
                foreach (var item in responseModel.Data)
                {
                    WorkPlaceData obj = new WorkPlaceData();
                    obj.WorkPlaceCode = item.workPlaceValue;
                    returnValue.Add(obj);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return returnValue;
        }


        public override PersonalCommunicationCenter GetPersonalCommunicationData(string EmployeeID)
        {

            PersonalCommunicationCenter comData = new PersonalCommunicationCenter();
            ResponseModel<PAContactInf> responseModel = new ResponseModel<PAContactInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAContactInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                if (responseModel.Data.Count > 0)
                {
                    comData.CompanyCode = responseModel.Data[0].companyCode;
                    comData.EmployeeID = responseModel.Data[0].empCode;
                    foreach (var item in responseModel.Data)
                    {
                        switch (item.contactValue)
                        {
                            case "001":
                                comData.HomePhone = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "002":
                                comData.WorkPlace = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "003":
                                comData.MobilePhone = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "004":
                                comData.LineID = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "005":
                                comData.Email = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "006":
                                comData.PrivateEmail = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "008":
                                comData.PersonOfEmergency = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "009":
                                comData.PhoneOfEmergency = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "010":
                                comData.PrimaryWebsite = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            case "011":
                                comData.SecondaryWebsite = string.IsNullOrEmpty(item.contactData) ? string.Empty : item.contactData;
                                break;
                            default:
                                break;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetPersonalCommunicationData Error", responseModel.message + "|" + ex.Message);
            }

            return comData;
        }

        public override List<PersonalDocumentCenter> GetPersonalDocumentData(string EmployeeID)
        {
            List<PersonalDocumentCenter> docData = new List<PersonalDocumentCenter>();
            ResponseModel<PADocumentInf> responseModel = new ResponseModel<PADocumentInf>();


            List<PADocumentInf> pDocument = new List<PADocumentInf>();
            pDocument = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPADocumentInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID).Data;
            if (pDocument != null && pDocument.Count > 0)
            {
                PADocumentInf pLicenceDoc = new PADocumentInf();
                //01 = Licence
                //02 = Passport
                //03 = Workpermit
                //51 = Visa
                string[] excdoc = ShareDataManagement.LookupCache(CompanyCode, "ESS.HR.PA", "EXCEPTION_DOCUMENTTYPE").Split(',');

                foreach (var item in pDocument)
                {
                    PersonalDocumentCenter data = new PersonalDocumentCenter();
                    if (excdoc.Where(a => a.ToString().ToLower() == item.documentValue).Count() == 0)
                    {
                        data.CompanyCode = CompanyCode;
                        data.DocumentNo = item.documentId;
                        data.DocumentType = item.documentValue;
                        data.IssueDate = !string.IsNullOrEmpty(item.dateOfIssue) ? DateTimeService.ConvertDateTime(CompanyCode, item.dateOfIssue) : (DateTime?)null;
                        data.ExpiredDate = !string.IsNullOrEmpty(item.expireDate) ? DateTimeService.ConvertDateTime(CompanyCode, item.expireDate) : (DateTime?)null;
                        data.Nationality = item.countryOfIssueValue;
                        data.ValidityOfEntry = item.validityValue;
                        docData.Add(data);
                    }


                }
                //   pLicenceDoc = pDocument.Find(a => a.documentValue == "01");
                if (pLicenceDoc != null)
                {
                    //data.LicenseID = pLicenceDoc.documentId;

                    // data.LicenceIssueDate = !string.IsNullOrEmpty(pLicenceDoc.dateOfIssue) ? DateTimeService.ConvertDateTime(CompanyCode, pLicenceDoc.dateOfIssue) : (DateTime?)null;
                    // data.LicenceExpiredDate = !string.IsNullOrEmpty(pLicenceDoc.expireDate) ? DateTimeService.ConvertDateTime(CompanyCode, pLicenceDoc.expireDate) : (DateTime?)null;
                }
            }


            return docData;

        }
        public override void SavePersonalCommunicationData(PersonalCommunicationCenter data, PersonalCommunicationCenter olddata = null)
        {
            PostResult postResult = null;
            List<PAContactInfPost> pContactList = new List<PAContactInfPost>();

            try
            {
                Type type = typeof(PersonalCommunicationCenter);
                PropertyInfo[] properties = type.GetProperties();
                foreach (PropertyInfo item in properties)
                {
                    PAContactInfPost pContactInfo = new PAContactInfPost();
                    pContactInfo.companyCode = CompanyCode;
                    pContactInfo.empCode = data.EmployeeID;
                    pContactInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                    pContactInfo.endDate = DefaultEndDatePost;
                    pContactInfo.contactCode = "CONTACT_CODE";
                    pContactInfo.actionType = "U";
                    switch (item.Name)
                    {
                        //"001"
                        case "HomePhone":
                            pContactInfo.contactValue = "001";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;

                        //"002"
                        case "WorkPlace":
                            pContactInfo.contactValue = "002";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;

                        //"003"
                        case "MobilePhone":
                            pContactInfo.contactValue = "003";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                        //"004"
                        case "LineID":
                            pContactInfo.contactValue = "004";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                        // "005"
                        case "Email":
                            pContactInfo.contactValue = "005";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                        //"006"
                        case "PrivateEmail":
                            pContactInfo.contactValue = "006";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                        //"008"
                        case "PersonOfEmergency":
                            pContactInfo.contactValue = "008";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                        //"009"
                        case "PhoneOfEmergency":
                            pContactInfo.contactValue = "009";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                        case "PrimaryWebsite":
                            pContactInfo.contactValue = "010";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                        case "SecondaryWebsite":
                            pContactInfo.contactValue = "011";
                            pContactInfo.contactData = item.GetValue(data, null).ToString();
                            if (!string.IsNullOrEmpty(pContactInfo.contactData) && pContactInfo.contactData != "-")
                            {
                                pContactList.Add(pContactInfo);
                            }
                            break;
                            //default:
                            //    pContactInfo.contactCode = string.Empty;
                            //    pContactInfo.contactValue = string.Empty;
                            //    pContactInfo.contactData = string.Empty;
                            //    break;
                    }
                }

                foreach (var item in pContactList)
                {
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAContactInf(item);
                    if (postResult.returnCode != 101)
                    {
                        var DataDetail = JsonConvert.SerializeObject(item);
                        throw new DataServiceException("HRPA", "SAVE_PERSONALCOMMUNICATION", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "SAVE_PERSONALCOMMUNICATION", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
            }
        }
        public override List<PersonalAcademicCenter> GetPersonalAcademicDataByParameter(string oSubtypeCode = null, string oSubtypeValue = null, string oDOI = null)
        {
            List<PersonalAcademicCenter> acaData = new List<PersonalAcademicCenter>();
            ResponseModel<PAAcademicInf> responseModel = new ResponseModel<PAAcademicInf>();
            DateTime oCheckDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd", enCL);
            try
            {
                responseModel = responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAcademicInf(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, oSubtypeCode, oSubtypeValue, oDOI);
                foreach (var item in responseModel.Data)
                {
                    PersonalAcademicCenter dataItem = new PersonalAcademicCenter();
                    dataItem.PaReId = item.paReId;
                    dataItem.SubTypeCode = item.subtypeCode;
                    dataItem.SubTypeValue = item.subtypeValue;
                    dataItem.SubTypeNameTH = item.subtypeNameTH;
                    dataItem.SubTypeNameEN = item.subtypeNameEN;
                    dataItem.Title = item.title;
                    dataItem.YearOfPublic = !string.IsNullOrEmpty(item.yearOfPublic) ? item.yearOfPublic : string.Empty;
                    dataItem.Journal = !string.IsNullOrEmpty(item.journal) ? item.journal : string.Empty;
                    dataItem.DOI = item.doi;
                    dataItem.ImpactFactor = !string.IsNullOrEmpty(item.impactFactor) ? item.impactFactor : string.Empty;
                    dataItem.SubmittedDate = DateTimeService.ConvertDateTime(CompanyCode, item.submittedDate);
                    dataItem.GrantedDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                    dataItem.CategoryCode = item.categoryCode;
                    dataItem.CategoryValue = item.categoryValue;
                    dataItem.CategoryNameTH = item.categoryNameTH;
                    dataItem.CategoryNameEN = item.categoryNameEN;
                    dataItem.Remark = !string.IsNullOrEmpty(item.remark) ? item.remark : string.Empty;
                    dataItem.AttachmentId = !string.IsNullOrEmpty(item.attachmentId) ? item.attachmentId : string.Empty;
                    dataItem.RefAuthor = !string.IsNullOrEmpty(item.refAuthors) ? item.refAuthors : string.Empty;
                    var memberlist = GetPersonalAcademicMemberDataByAcademicID(item.paReId.ToString());
                    foreach (var mem in memberlist)
                    {
                        var personalInfo = HRPAManagement.CreateInstance(CompanyCode).GetPersonalInfo(mem.EmployeeID, oCheckDate);
                        if (!string.IsNullOrEmpty(personalInfo.EmployeeID))
                        {
                            PersonalAcademicMemberCenter data = new PersonalAcademicMemberCenter();
                            data.EmployeeID = mem.EmployeeID;
                            data.CompanyCode = mem.CompanyCode;
                            data.PAReID = mem.PAReID;
                            data.PAReMemID = mem.PAReMemID;
                            data.FullNameEN = personalInfo.FullNameEN;
                            data.FullNameTH = personalInfo.FullName;
                            data.RoleCode = mem.RoleCode;
                            data.RoleValue = mem.RoleValue;
                            data.RoleNameTH = mem.RoleNameTH;
                            data.RoleNameEN = mem.RoleNameEN;
                            dataItem.AcademicMembers.Add(data);
                        }
                    }
                    acaData.Add(dataItem);
                }

            }
            catch (Exception)
            {

                throw;
            }


            return acaData;

        }
        public override List<PersonalAcademicCenter> GetPersonalAcademicDataByEmpCode(string oEmployeeID)
        {
            List<PersonalAcademicCenter> acaData = new List<PersonalAcademicCenter>();
            ResponseModel<PAAcademicInf> responseModel = new ResponseModel<PAAcademicInf>();
            DateTime oCheckDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd", enCL);
            try
            {
                responseModel = responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAcademicInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, oEmployeeID);
                if (responseModel.Data == null)
                {
                    return acaData;
                }
                foreach (var item in responseModel.Data)
                {
                    PersonalAcademicCenter dataItem = new PersonalAcademicCenter();
                    dataItem.PaReId = item.paReId;
                    dataItem.SubTypeCode = item.subtypeCode;
                    dataItem.SubTypeValue = item.subtypeValue;
                    dataItem.SubTypeNameTH = item.subtypeNameTH;
                    dataItem.SubTypeNameEN = item.subtypeNameEN;
                    dataItem.Title = !string.IsNullOrEmpty(item.title) ? item.title : string.Empty;
                    dataItem.YearOfPublic = !string.IsNullOrEmpty(item.yearOfPublic) ? item.yearOfPublic : string.Empty;
                    dataItem.Journal = !string.IsNullOrEmpty(item.journal) ? item.journal : string.Empty;
                    dataItem.DOI = item.doi;
                    dataItem.ImpactFactor = !string.IsNullOrEmpty(item.impactFactor) ? item.impactFactor : string.Empty;
                    dataItem.SubmittedDate = DateTimeService.ConvertDateTime(CompanyCode, item.submittedDate);
                    dataItem.GrantedDate = DateTimeService.ConvertDateTime(CompanyCode, item.grantedDate);
                    dataItem.CategoryCode = item.categoryCode;
                    dataItem.CategoryValue = item.categoryValue;
                    dataItem.CategoryNameTH = item.categoryNameTH;
                    dataItem.CategoryNameEN = item.categoryNameEN;
                    dataItem.Remark = !string.IsNullOrEmpty(item.remark) ? item.remark : string.Empty;
                    dataItem.AttachmentId = !string.IsNullOrEmpty(item.attachmentId) ? item.attachmentId : string.Empty;
                    dataItem.RefAuthor = !string.IsNullOrEmpty(item.refAuthors) ? item.refAuthors : string.Empty;
                    var memberlist = GetPersonalAcademicMemberDataByAcademicID(item.paReId.ToString());
                    foreach (var mem in memberlist)
                    {
                        var personalInfo = HRPAManagement.CreateInstance(CompanyCode).GetPersonalInfo(mem.EmployeeID, oCheckDate);
                        if (!string.IsNullOrEmpty(personalInfo.EmployeeID))
                        {
                            PersonalAcademicMemberCenter data = new PersonalAcademicMemberCenter();
                            data.EmployeeID = mem.EmployeeID;
                            data.CompanyCode = mem.CompanyCode;
                            data.PAReID = mem.PAReID;
                            data.PAReMemID = mem.PAReMemID;
                            data.FullNameEN = personalInfo.FullNameEN;
                            data.FullNameTH = personalInfo.FullName;
                            data.RoleCode = mem.RoleCode;
                            data.RoleValue = mem.RoleValue;
                            data.RoleNameTH = mem.RoleNameTH;
                            data.RoleNameEN = mem.RoleNameEN;
                            dataItem.AcademicMembers.Add(data);
                        }
                    }
                    acaData.Add(dataItem);
                }

            }
            catch (Exception)
            {

                throw;
            }


            return acaData;

        }
        public override List<PersonalAcademicMemberCenter> GetPersonalAcademicMemberDataByAcademicID(string AcademicID)
        {
            List<PersonalAcademicMemberCenter> acaMemData = new List<PersonalAcademicMemberCenter>();
            ResponseModel<PAAcademicMemberInf> responseModel = new ResponseModel<PAAcademicMemberInf>();

            try
            {
                responseModel = responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAcademicMemberInfByAcademicID(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, AcademicID);
                foreach (var item in responseModel.Data)
                {
                    PersonalAcademicMemberCenter dataItem = new PersonalAcademicMemberCenter();
                    dataItem.EmployeeID = item.empCode;
                    dataItem.StartDate = item.startDate;
                    dataItem.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                    dataItem.PAReID = item.paReId;
                    dataItem.PAReMemID = item.paReMemId;
                    dataItem.RoleCode = item.roleCode;
                    dataItem.RoleValue = item.roleValue;
                    dataItem.RoleNameTH = item.roleNameTH;
                    dataItem.RoleNameEN = item.roleNameEN;
                    acaMemData.Add(dataItem);
                }

            }
            catch (Exception)
            {

                throw;
            }


            return acaMemData;

        }


        #region " WorkHistory Ext "
        public override List<WorkPeriod> GetCurrentWorkHistory(string EmployeeID)
        {
            List<WorkPeriod> oList = new List<WorkPeriod>();

            //Data in responseModelPos is according with responseModelOrg
            ResponseModel<PAPositionHis> responseModelPos = new ResponseModel<PAPositionHis>();
            ResponseModel<PAOrganizationInf> responseModelOrg = new ResponseModel<PAOrganizationInf>();

            try
            {
                responseModelPos = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPositionHisByEmpCode(DefaultBeginDate, DateTime.Now.ToString(DefaultDateGet), EmployeeID);
                responseModelOrg = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAOrganizationInfByEmpCode(DefaultBeginDate, DateTime.Now.ToString(DefaultDateGet), EmployeeID);


                if (responseModelPos.Data.Count > 0 && responseModelOrg.Data.Count > 0)
                {
                    for (int i = 0; i < responseModelOrg.Data.Count; i++)
                    {
                        PAPositionHis oPos = responseModelPos.Data[i];
                        PAOrganizationInf oOrg = responseModelOrg.Data[i];
                        WorkPeriod oTemp = new WorkPeriod();

                        oTemp.EMPLOYEEID = EmployeeID;
                        //oTemp.STARTDATE = DateTime.ParseExact(oPos.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None).ToString("dd/MM/yyyy");
                        oTemp.STARTDATE = DateTimeService.ConvertDateTime(CompanyCode, oPos.startDate).ToString("dd/MM/yyyy");
                        //oTemp.ENDDATE = DateTime.ParseExact(oPos.endDate, DefaultReturnDateFormat, null, DateTimeStyles.None).ToString("dd/MM/yyyy");
                        if (i == responseModelOrg.Data.Count - 1)
                            oTemp.ENDDATE = DateTimeService.ConvertDateTime(CompanyCode, DateTime.Now.ToString(DefaultDateGet)).ToString("dd/MM/yyyy");
                        else
                            oTemp.ENDDATE = DateTimeService.ConvertDateTime(CompanyCode, oPos.endDate).ToString("dd/MM/yyyy");
                        oTemp.ORG_ID = oOrg.unitCode;
                        oTemp.ORG_NAME = oOrg.unitTextTH;
                        oTemp.ORG_NAME_EN = oOrg.unitTextEN;
                        oTemp.POS_ID = oOrg.posCode;
                        oTemp.POS_NAME = oOrg.posTextTH;
                        oTemp.POS_NAME_EN = oOrg.posTextEN;
                        // oTemp.ACTION = oPos.actionValue;
                        oTemp.ACTION = oPos.actionTextTH;
                        oList.Add(oTemp);
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return oList;
        }

        public override RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate)
        {
            RelatedAge oReturn = new RelatedAge();
            string oCurrentPos, oCurrentOrg;
            int oIndexStartCurrentPosition = 0, oIndexStartOrganization = 0;
            ResponseModel<PAOrganizationInf> responseModel = new ResponseModel<PAOrganizationInf>();
            ResponseModel<PAImportanceDateInf> oImportanceDate = new ResponseModel<PAImportanceDateInf>();

            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAOrganizationInfByEmpCode(DefaultBeginDate, CheckDate.ToString(DefaultDateGet), EmployeeID);
                oImportanceDate = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAImportanceDateInfByEmpCode(CheckDate.ToString(DefaultDateGet), CheckDate.ToString(DefaultDateGet), EmployeeID);

                if (responseModel.Data.Count > 0)
                {
                    oCurrentPos = responseModel.Data[responseModel.Data.Count - 1].posCode;
                    oCurrentOrg = responseModel.Data[responseModel.Data.Count - 1].unitCode;
                    oIndexStartCurrentPosition = responseModel.Data.Count - 1;
                    oIndexStartOrganization = responseModel.Data.Count - 1;

                    for (int i = responseModel.Data.Count; i > 0; i--)
                    {
                        PAOrganizationInf oTemp = responseModel.Data[i - 1];
                        if (oTemp.posCode == oCurrentPos)
                        {
                            oIndexStartCurrentPosition = i - 1;
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                    for (int i = responseModel.Data.Count; i > 0; i--)
                    {
                        PAOrganizationInf oTemp = responseModel.Data[i - 1];
                        if (oTemp.unitCode == oCurrentOrg)
                        {
                            oIndexStartOrganization = i - 1;
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }

                }

                DateTime CurrentDateTime = DateTime.Now;

                DateTime FirstDateOfCompany = DateTimeService.ConvertDateTime(CompanyCode, oImportanceDate.Data.Count == 0 ? DefaultBeginDate : oImportanceDate.Data[0].startWorkDate);
                DateTime FirstDateOfOrg = DateTimeService.ConvertDateTime(CompanyCode, responseModel.Data[oIndexStartOrganization].startDate);
                DateTime FirstdateOfPosition = DateTimeService.ConvertDateTime(CompanyCode, responseModel.Data[oIndexStartCurrentPosition].startDate);

                oReturn.EmployeeID = EmployeeID;
                oReturn.ageOfOrg = CalculateDatetimeData(FirstDateOfOrg, CurrentDateTime);
                //oReturn.ageOfLevel = CalculateDatetimeData(FirstDateOfCompany, CurrentDateTime);
                oReturn.ageOfPosition = CalculateDatetimeData(FirstdateOfPosition, CurrentDateTime);
                oReturn.ageOfCompany = CalculateDatetimeData(FirstDateOfCompany, CurrentDateTime);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return oReturn;

        }

        public DateTimeData CalculateDatetimeData(DateTime startDate, DateTime endDate)
        {
            DateTimeData oReturn = new DateTimeData();

            int[] monthDay = { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            int year, month, day;

            //Day calculate
            int increment = 0;
            if (startDate.Day > endDate.Day)
            {
                increment = monthDay[startDate.Month - 1];
            }

            if (increment == -1)
            {
                if (DateTime.IsLeapYear(startDate.Year))
                {
                    increment = 29;
                }
                else
                {
                    increment = 28;
                }
            }

            if (increment != 0)
            {
                day = (endDate.Day + increment) - startDate.Day;
                increment = 1;
            }
            else
            {
                day = endDate.Day - startDate.Day;
            }

            //month calculate
            if ((startDate.Month + increment) > endDate.Month)
            {
                month = (endDate.Month + 12) - (startDate.Month + increment);
                increment = 1;
            }
            else
            {
                month = (endDate.Month) - (startDate.Month + increment);
                increment = 0;
            }

            //Year calculate
            year = endDate.Year - (startDate.Year + increment);

            oReturn.Zday = day.ToString("00");
            oReturn.Zmonth = month.ToString("00");
            oReturn.Zyear = year.ToString("00");

            return oReturn;
        }

        #endregion " WorkHistory Ext "

        public List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup)
        {
            throw new NotImplementedException();
        }


        public override List<string> GetIDCardData(string EmployeeID)
        {
            List<string> returnValue = new List<string>();
            ResponseModel<PAPrivateInf> responseModel = new ResponseModel<PAPrivateInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPrivateInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                if (responseModel.Data.Count > 0)
                {
                    returnValue.Add(responseModel.Data[0].idCard);
                }

            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetIDCardData Error", responseModel.message);
            }

            return returnValue;
        }

        #region GetPersonalInfo
        public override PersonalInfoCenter GetPersonalInfo(string EmployeeID, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            PersonalInfoCenter data = new PersonalInfoCenter();
            ResponseModel<PAPersonalInf> responseModel = new ResponseModel<PAPersonalInf>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPersonalInfByEmpCode(CheckDate.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                PAPersonalInf personalInf = new PAPersonalInf();
                if (responseModel.Data.Count() > 0)
                {
                    personalInf = responseModel.Data[0];
                    if (personalInf != null)
                    {

                        ResponseModel<PAPrivateInf> modelPrivate = new ResponseModel<PAPrivateInf>();
                        modelPrivate = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPrivateInfByEmpCode(CheckDate.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                        data.CompanyCode = personalInf.companyCode;
                        data.EmployeeID = personalInf.empCode;

                        data.TitleID = string.IsNullOrEmpty(personalInf.inameValue) ? "" : personalInf.inameValue;
                        data.TitleIDEn = string.IsNullOrEmpty(personalInf.inameEnValue) ? "" : personalInf.inameEnValue;
                        data.Prefix = string.IsNullOrEmpty(personalInf.iname1Value) ? "" : personalInf.iname1Value;//ราชทินนาม,ฐานันดรศักดิ์
                        data.MilitaryTitle = string.IsNullOrEmpty(personalInf.iname2Value) ? "" : personalInf.iname2Value; //ยศทางทหาร
                        data.SecondTitle = string.IsNullOrEmpty(personalInf.iname3Value) ? "" : personalInf.iname3Value; //ดร.
                        data.AcademicTitle = string.IsNullOrEmpty(personalInf.iname4Value) ? "" : personalInf.iname4Value;//ยศทางวิชาการ                    
                        data.MedicalTitle = string.IsNullOrEmpty(personalInf.iname5Value) ? "" : personalInf.iname5Value;//ยศทางการแพทย์
                        data.FirstName = personalInf.fname;
                        data.MiddleName = personalInf.mname;
                        data.LastName = personalInf.lname;

                        data.FullName = string.IsNullOrEmpty(personalInf.fullName) ? "" : personalInf.fullName.Replace("  ", " ");
                        data.FullNameEN = string.IsNullOrEmpty(personalInf.fullNameEnText) ? "" : personalInf.fullNameEnText.Replace("  ", " ");
                        data.InameEnValue = string.IsNullOrEmpty(personalInf.inameEnValue) ? "" : personalInf.inameEnValue;
                        data.FirstNameEn = string.IsNullOrEmpty(personalInf.fnameEn) ? "" : personalInf.fnameEn;
                        data.MiddleNameEn = string.IsNullOrEmpty(personalInf.mnameEn) ? "" : personalInf.mnameEn;
                        data.LastNameEn = string.IsNullOrEmpty(personalInf.lnameEn) ? "" : personalInf.lnameEn;
                        data.NickName = string.IsNullOrEmpty(personalInf.nickName) ? "" : personalInf.nickName;
                        if (modelPrivate.Data.Count > 0)
                        {
                            data.IDCard = string.IsNullOrEmpty(modelPrivate.Data[0].idCard) ? "" : modelPrivate.Data[0].idCard;
                            data.BloodType = string.IsNullOrEmpty(modelPrivate.Data[0].bloodGrpTextEN) ? "" : modelPrivate.Data[0].bloodGrpTextEN;
                            data.DOB = DateTimeService.ConvertDateTime(CompanyCode, modelPrivate.Data[0].birthDate);
                        }
                        else
                        {
                            data.IDCard = string.Empty;
                            data.DOB = DateTime.MinValue;
                            data.BloodType = string.Empty;
                        }

                        data.Gender = string.IsNullOrEmpty(personalInf.sexValue) ? "" : personalInf.sexValue;
                        data.BirthPlace = string.IsNullOrEmpty(personalInf.provinceBirthCode) ? "" : personalInf.provinceBirthCode;
                        data.BirthCity = string.IsNullOrEmpty(personalInf.countryBirthValue) ? "" : personalInf.countryBirthValue;
                        data.Nationality = string.IsNullOrEmpty(personalInf.nationalityValue) ? "" : personalInf.nationalityValue;
                        data.Religion = string.IsNullOrEmpty(personalInf.religionValue) ? "" : personalInf.religionValue;
                        data.MaritalStatus = string.IsNullOrEmpty(personalInf.maritalStatusValue) ? "" : personalInf.maritalStatusValue;


                        if (data.MaritalStatus != "0")
                        {
                            ResponseModel<PAFamilyInf> familyModel = new ResponseModel<PAFamilyInf>();
                            familyModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAFamilyInfByEmpCode(CheckDate.ToString(DefaultDateGet), DefaultEndDateGet, EmployeeID);
                            if (familyModel.Data != null)
                            {
                                PAFamilyInf familyInf = familyModel.Data.Where(r => r.relateValue == "01").FirstOrDefault();
                                //DateTime bufferDate;
                                //001-โสด,002-สมรส,003-หย่า
                                if (familyInf != null && familyInf.relateValue == "01")
                                {
                                    switch (data.MaritalStatus)
                                    {
                                        case "001":
                                            break;
                                        case "002":
                                            data.MaritalDate = DateTimeService.ConvertDateTime(CompanyCode, familyInf.maritalDate);
                                            break;
                                        case "003":
                                            data.DivorceDate = DateTimeService.ConvertDateTime(CompanyCode, familyInf.divorceDate);
                                            break;
                                        case "004":
                                            data.PassAwayDate = DateTimeService.ConvertDateTime(CompanyCode, familyInf.divorceDate);
                                            break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            data.MaritalDate = DateTime.MinValue;
                        }


                    }
                    else
                    {
                        return null;
                    }
                }



            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetPersonalInfo Error", responseModel.message + " " + ex.Message);
            }

            return data;
        }
        #endregion GetPersonalInfo
        public override void SavePersonalInfo(PersonalInfoCenter data)
        {
            PostResult postResult = null;
            PAPersonalInfPost personalInfo = new PAPersonalInfPost();
            personalInfo = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPersonalInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, data.EmployeeID).Data[0];
            try
            {
                personalInfo.companyCode = CompanyCode;
                personalInfo.empCode = data.EmployeeID;
                personalInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                personalInfo.endDate = DefaultEndDatePost;
                #region INAME_CODE - TitleID
                if (!string.IsNullOrEmpty(data.TitleID))
                {
                    personalInfo.inameCode = "INAME_CODE";
                    personalInfo.inameValue = data.TitleID;
                }
                else
                {
                    personalInfo.inameCode = string.Empty;
                    personalInfo.inameValue = string.Empty;
                }
                #endregion
                #region INAME_CODE - TitleIDEn
                if (!string.IsNullOrEmpty(data.TitleIDEn))
                {
                    personalInfo.inameEnCode = "INAME_EN_CODE";
                    personalInfo.inameEnValue = data.TitleIDEn;
                }
                else
                {
                    personalInfo.inameEnCode = string.Empty;
                    personalInfo.inameEnValue = string.Empty;
                }
                #endregion
                #region INAME1_CODE-Prefix ราชทินนาม,ฐานันดรศักดิ์
                if (!string.IsNullOrEmpty(data.Prefix))
                {
                    personalInfo.iname1Code = "INAME1_CODE";
                    personalInfo.iname1Value = data.Prefix;
                }
                else
                {
                    personalInfo.iname1Code = string.Empty;
                    personalInfo.iname1Value = string.Empty;
                }
                #endregion
                #region INAME2_CODE-MilitaryTitle ยศทางทหาร
                if (!string.IsNullOrEmpty(data.MilitaryTitle))
                {
                    personalInfo.iname2Code = "INAME2_CODE";
                    personalInfo.iname2Value = data.MilitaryTitle;
                }
                else
                {
                    personalInfo.iname2Code = string.Empty;
                    personalInfo.iname2Value = string.Empty;
                }
                #endregion
                #region INAME3_CODE-SecondTitle ดร.
                if (!string.IsNullOrEmpty(data.SecondTitle))
                {
                    personalInfo.iname3Code = "INAME3_CODE";
                    personalInfo.iname3Value = data.SecondTitle;
                }
                else
                {
                    personalInfo.iname3Code = string.Empty;
                    personalInfo.iname3Value = string.Empty;
                }
                #endregion
                #region INAME4_CODE-AcademicTitle ยศทางวิชาการ
                if (!string.IsNullOrEmpty(data.AcademicTitle))
                {
                    personalInfo.iname4Code = "INAME4_CODE";
                    personalInfo.iname4Value = data.AcademicTitle;
                }
                else
                {
                    personalInfo.iname4Code = string.Empty;
                    personalInfo.iname4Value = string.Empty;
                }
                #endregion
                #region INAME5_CODE-Prefix
                if (!string.IsNullOrEmpty(data.MedicalTitle))
                {
                    personalInfo.iname5Code = "INAME5_CODE";
                    personalInfo.iname5Value = data.MedicalTitle;
                }
                else
                {
                    personalInfo.iname5Code = string.Empty;
                    personalInfo.iname5Value = string.Empty;
                }
                #endregion
                #region FullName,FirstName,MiddleName,LastName,NickName
                personalInfo.fullName = string.IsNullOrEmpty(data.FullName) ? "" : data.FullName;
                personalInfo.fname = string.IsNullOrEmpty(data.FirstName) ? "" : data.FirstName;
                personalInfo.mname = string.IsNullOrEmpty(data.MiddleName) ? "" : data.MiddleName;
                personalInfo.lname = string.IsNullOrEmpty(data.LastName) ? "" : data.LastName;
                personalInfo.nickName = string.IsNullOrEmpty(data.NickName) ? "" : data.NickName;
                #endregion
                #region FirstNameEn,MiddleNameEn,LastNameEn
                personalInfo.fnameEn = string.IsNullOrEmpty(data.FirstNameEn) ? "" : data.FirstNameEn;
                personalInfo.mnameEn = string.IsNullOrEmpty(data.MiddleNameEn) ? "" : data.MiddleNameEn;
                personalInfo.lnameEn = string.IsNullOrEmpty(data.LastNameEn) ? "" : data.LastNameEn;
                #endregion
                #region COUNTRY_CODE-BirthCity (Country)
                if (!string.IsNullOrEmpty(data.BirthCity))
                {
                    personalInfo.countryBirthCode = "COUNTRY_CODE";
                    personalInfo.countryBirthValue = data.BirthCity;
                }
                else
                {
                    personalInfo.countryBirthCode = string.Empty;
                    personalInfo.countryBirthValue = string.Empty;
                }
                #endregion
                #region BirthPlace (Province)
                if (!string.IsNullOrEmpty(data.BirthPlace))
                {
                    personalInfo.provinceBirthCode = data.BirthPlace;
                }
                else
                {
                    personalInfo.provinceBirthCode = string.Empty;
                }
                #endregion
                #region RACE_CODE-RaceValue
                if (!string.IsNullOrEmpty(data.RaceValue))
                {
                    personalInfo.raceCode = "RACE_CODE";
                    personalInfo.raceValue = data.RaceValue;
                }
                else
                {
                    personalInfo.raceCode = string.Empty;
                    personalInfo.raceValue = string.Empty;
                }
                #endregion
                #region NATIONALITY_CODE-Nationality
                if (!string.IsNullOrEmpty(data.Nationality))
                {
                    personalInfo.nationalityCode = "NATIONALITY_CODE";
                    personalInfo.nationalityValue = data.Nationality;
                }
                else
                {
                    personalInfo.nationalityCode = string.Empty;
                    personalInfo.nationalityValue = string.Empty;
                }
                #endregion
                #region RELIGION_CODE-Religion
                if (!string.IsNullOrEmpty(data.Religion))
                {
                    personalInfo.religionCode = "RELIGION_CODE";
                    personalInfo.religionValue = data.Religion;
                }
                else
                {
                    personalInfo.religionCode = string.Empty;
                    personalInfo.religionValue = string.Empty;
                }
                #endregion
                #region SEX_CODE-Gender
                if (!string.IsNullOrEmpty(data.Gender))
                {
                    personalInfo.sexCode = "SEX_CODE";
                    personalInfo.sexValue = data.Gender;
                }
                else
                {
                    personalInfo.sexCode = string.Empty;
                    personalInfo.sexValue = string.Empty;
                }
                #endregion
                #region MARITAL_STATUS_CODE-MaritalStatus
                if (!string.IsNullOrEmpty(data.MaritalStatus))
                {
                    personalInfo.maritalStatusCode = "MARITAL_STATUS_CODE";
                    personalInfo.maritalStatusValue = data.MaritalStatus;
                }
                else
                {
                    personalInfo.maritalStatusCode = string.Empty;
                    personalInfo.maritalStatusValue = string.Empty;
                }
                #endregion

                PAFamilyInf familyInf = null;
                if (personalInfo.maritalStatusValue != Convert.ToInt32(data.MaritalStatus).ToString("00#"))
                {
                    personalInfo.maritalStatusValue = Convert.ToInt32(data.MaritalStatus).ToString("00#");
                    //Pending กรณีที่แก้ไขข้อมูลส่วนตัวเป็นหย่า/หม้าย หากมีข้อมูลคู่สมรสต้องไปอัพเดท effective date ให้เป็นหย่า/หม้าย ตามวันที่เลือกด้วย
                    //Save privateInf
                    //List<PAFamilyInf> familyInfList = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAFamilyInfByEmpCode(DateTime.Now.ToString(DefaultDateFormat), DefaultEndDate, data.EmployeeID).Data;
                    //if (familyInfList != null && familyInfList.Count > 0)
                    //{
                    //    familyInf = familyInfList.Find(a => a.relateValue == "01");
                    //    switch (data.MaritalStatus)
                    //    {
                    //        case "001":
                    //            break;
                    //        case "002":
                    //            familyInf.maritalDate = data.MaritalDate.ToString();
                    //            break;
                    //        case "003":
                    //            familyInf.divorceDate = data.DivorceDate.ToString();
                    //            break;
                    //        case "004":
                    //            familyInf.passAwayDate = data.PassAwayDate.ToString();
                    //            break;
                    //    }
                    //    familyInf.startDate = DateTime.Now.ToString(DefaultDateFormat);
                    //}
                }

                if (data.ModifyType == "PersonalInfo" || string.IsNullOrEmpty(data.ModifyType))
                {
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAPersonalInf(personalInfo);
                    if (postResult.returnCode != 101)
                    {

                        data.Remark = postResult.returnDesc;
                        var DataDetail = JsonConvert.SerializeObject(personalInfo);
                        throw new DataServiceException("HRPA", "SAVE_PERSONALINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                    }
                    else
                    {

                        if (familyInf != null)
                        {
                            postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAFamilyInf(familyInf);
                            if (postResult.returnCode != 101)
                            {
                                throw new DataServiceException("HRPA", "SAVE_PERSONALINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
                            }
                        }
                        //save document

                    }
                }
                else if (data.ModifyType == "DocumentInfo")
                {
                    //SavePersonalDocumentInfo(data);
                }
                else if (data.ModifyType == "AllInfo")
                {
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAPersonalInf(personalInfo);
                    if (postResult.returnCode != 101)
                    {

                        data.Remark = postResult.returnDesc;
                        var DataDetail = JsonConvert.SerializeObject(personalInfo);
                        throw new DataServiceException("HRPA", "SAVE_PERSONALINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                    }
                    else
                    {

                        if (familyInf != null)
                        {
                            postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAFamilyInf(familyInf);
                            if (postResult.returnCode != 101)
                            {
                                throw new DataServiceException("HRPA", "SAVE_PERSONALINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
                            }
                        }
                        //save document
                        //  SavePersonalDocumentInfo(data);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "SAVE_PERSONALINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
            }
        }

        public override void SavePersonalDocumentInfo(PersonalDocumentCenter data)
        {
            PostResult postResult = null;

            //   List<PADocumentInf> pDocument = new List<PADocumentInf>();
            PADocumentInf pDocument = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPADocumentInfByEmpCode(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, data.EmployeeID).Data.Where(a => a.documentValue == data.DocumentType).SingleOrDefault();
            try
            {
                if (pDocument != null && !string.IsNullOrEmpty(pDocument.empCode))
                {
                    PADocumentInfPost personalDocInfo = new PADocumentInfPost();
                    personalDocInfo.companyCode = CompanyCode;
                    personalDocInfo.empCode = data.EmployeeID;
                    personalDocInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                    personalDocInfo.endDate = DefaultEndDatePost;
                    personalDocInfo.documentCode = !string.IsNullOrEmpty(data.DocumentType) ? "DOCUMENT_CODE" : string.Empty;
                    personalDocInfo.documentValue = data.DocumentType;
                    personalDocInfo.documentId = data.DocumentNo;
                    personalDocInfo.issuingAuthority = pDocument.issuingAuthority;
                    switch (pDocument.documentValue)
                    {
                        case "51":
                            personalDocInfo.validityCode = !string.IsNullOrEmpty(data.ValidityOfEntry) ? "VALIDITY_CODE" : string.Empty;
                            personalDocInfo.validityValue = data.ValidityOfEntry;
                            break;
                    }

                    personalDocInfo.dateOfIssue = string.IsNullOrEmpty(data.IssueDate.ToString()) ? "" : data.IssueDate.Value.ToString(DefaultDatePost);
                    personalDocInfo.effectiveDate = string.IsNullOrEmpty(data.IssueDate.ToString()) ? "" : data.IssueDate.Value.ToString(DefaultDatePost);
                    personalDocInfo.expireDate = string.IsNullOrEmpty(data.ExpiredDate.ToString()) ? "" : data.ExpiredDate.Value.ToString(DefaultDatePost);
                    personalDocInfo.issuingAuthority = pDocument.issuingAuthority;
                    personalDocInfo.placeOfIssue = !string.IsNullOrEmpty(pDocument.placeOfIssue) ? pDocument.placeOfIssue : string.Empty;
                    personalDocInfo.countryOfIssueCode = pDocument.countryOfIssueCode;
                    personalDocInfo.countryOfIssueValue = pDocument.countryOfIssueValue;
                    personalDocInfo.actionType = "U";
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPADocumentInf(personalDocInfo);
                    if (postResult.returnCode != 101)
                    {
                        // data.Remark = postResult.returnDesc;
                        var DataDetail = JsonConvert.SerializeObject(personalDocInfo);
                        throw new DataServiceException("HRPA", "SAVE_PERSONALDOCUMENTINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
                    }
                }
                else
                {
                    PADocumentInfPost personalDocInfo = new PADocumentInfPost();
                    personalDocInfo.companyCode = CompanyCode;
                    personalDocInfo.empCode = data.EmployeeID;
                    personalDocInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                    personalDocInfo.endDate = DefaultEndDatePost;
                    personalDocInfo.documentCode = !string.IsNullOrEmpty(data.DocumentType) ? "DOCUMENT_CODE" : string.Empty;
                    personalDocInfo.documentValue = data.DocumentType;
                    personalDocInfo.documentId = data.DocumentNo;
                    personalDocInfo.dateOfIssue = string.IsNullOrEmpty(data.IssueDate.ToString()) ? "" : data.IssueDate.Value.ToString(DefaultDatePost);
                    personalDocInfo.effectiveDate = string.IsNullOrEmpty(data.IssueDate.ToString()) ? "" : data.IssueDate.Value.ToString(DefaultDatePost);
                    personalDocInfo.expireDate = string.IsNullOrEmpty(data.ExpiredDate.ToString()) ? "" : data.ExpiredDate.Value.ToString(DefaultDatePost);
                    personalDocInfo.issuingAuthority = string.Empty;
                    personalDocInfo.placeOfIssue = string.Empty;
                    personalDocInfo.countryOfIssueCode = string.Empty;
                    personalDocInfo.countryOfIssueValue = string.Empty;
                    if (data.DocumentType == "51")//VISA
                    {
                        personalDocInfo.validityCode = !string.IsNullOrEmpty(data.ValidityOfEntry) ? "VALIDITY_CODE" : string.Empty;
                        personalDocInfo.validityValue = data.ValidityOfEntry;
                    }
                    else
                    {
                        personalDocInfo.validityCode = string.Empty;
                        personalDocInfo.validityValue = string.Empty;
                    }

                    personalDocInfo.actionType = "I";
                    postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPADocumentInf(personalDocInfo);
                    if (postResult.returnCode != 101)
                    {
                        //data.Remark = postResult.returnDesc;
                        var DataDetail = JsonConvert.SerializeObject(personalDocInfo);
                        throw new DataServiceException("HRPA", "SAVE_PERSONALDOCUMENTINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
                    }
                }
            }
            catch (Exception ex)
            {

                throw new DataServiceException("HRPA", "SAVE_PERSONALDOCUMENTINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
            }
        }

        public override void SavePersonalAcademicInfo(PersonalAcademicCenter data)
        {
            PostResult postResult = null;
            PAAcademicInfPost academicInfo = new PAAcademicInfPost();
            academicInfo = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAcademicInf(DateTime.Now.ToString(DefaultDateGet), DefaultEndDateGet, string.Empty, string.Empty, data.DOI).Data.FirstOrDefault();
            if (academicInfo == null)
            {
                academicInfo = new PAAcademicInfPost();
                academicInfo.actionType = "I";
                academicInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                academicInfo.endDate = DefaultEndDatePost;
                academicInfo.attachmentId = DateTime.Now.ToString("yyyyMMddHHmmss");
            }
            else
            {
                academicInfo.startDate = academicInfo.startDate;
                academicInfo.endDate = academicInfo.endDate;
                academicInfo.attachmentId = data.AttachmentId;
                academicInfo.actionType = "U";
            }

            try
            {
                academicInfo.impactFactor = !string.IsNullOrEmpty(data.ImpactFactor) ? data.ImpactFactor.ToString() : null;
                academicInfo.companyCode = CompanyCode;
                academicInfo.empCode = data.EmployeeID;
                academicInfo.title = data.Title;
                academicInfo.doi = data.DOI;

                academicInfo.yearOfPublic = data.YearOfPublic;
                academicInfo.journal = data.Journal;
                academicInfo.submittedDate = data.SubmittedDate == DateTime.MinValue ? "" : data.SubmittedDate.Value.ToString(DefaultDatePost);
                academicInfo.grantedDate = data.GrantedDate == DateTime.MinValue ? "" : data.GrantedDate.Value.ToString(DefaultDatePost);
                academicInfo.subtypeCode = string.IsNullOrEmpty(data.SubTypeValue) ? "" : "SUBTYPE_CODE";
                academicInfo.subtypeValue = string.IsNullOrEmpty(data.SubTypeValue) ? "" : data.SubTypeValue;
                academicInfo.categoryCode = string.IsNullOrEmpty(data.CategoryValue) ? "" : "CATEGORY_CODE";
                academicInfo.categoryValue = string.IsNullOrEmpty(data.CategoryValue) ? "" : data.CategoryValue;
                academicInfo.remark = data.Remark;
                academicInfo.refAuthors = data.RefAuthor;

                postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAAcademicInf(academicInfo);
                if (postResult.returnCode != 101)
                {

                    data.Remark = postResult.returnDesc;
                    var DataDetail = JsonConvert.SerializeObject(academicInfo);
                    throw new DataServiceException("HRPA", "SAVE_PERSONALINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "SAVE_ACADEMICINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc);
            }

        }

        public override void SavePersonalAcademicMemberInfo(PersonalAcademicMemberCenter data)
        {
            PostResult postResult = null;
            PAAcademicMemberInfPost academicMemberInfo = new PAAcademicMemberInfPost();
            var members = GetPersonalAcademicMemberDataByAcademicID(data.PAReID).ToList();
            var member = members.Where(a => a.EmployeeID == data.EmployeeID).FirstOrDefault();
            try
            {
                academicMemberInfo.companyCode = CompanyCode;
                academicMemberInfo.empCode = data.EmployeeID;
                academicMemberInfo.paReId = data.PAReID;

                academicMemberInfo.roleCode = string.IsNullOrEmpty(data.RoleValue) ? "" : "ROLE_CODE";
                academicMemberInfo.roleValue = string.IsNullOrEmpty(data.RoleValue) ? "" : data.RoleValue;
                if (members.Where(a => a.EmployeeID == data.EmployeeID).Count() > 0)//มีข้อมูล
                {
                    academicMemberInfo.actionType = "U";
                    academicMemberInfo.paReMemId = member.PAReMemID;
                    academicMemberInfo.startDate = member.StartDate;
                    academicMemberInfo.endDate = member.EndDate.ToString(DefaultDatePost);
                }
                else
                {//new
                    academicMemberInfo.startDate = DateTime.Now.ToString(DefaultDatePost);
                    academicMemberInfo.endDate = DefaultEndDatePost;
                    //  academicMemberInfo.paReMemId = null;
                    //  academicMemberInfo.paReMemId = (members.Count() + 1).ToString();
                    academicMemberInfo.actionType = "I";
                }
                postResult = APIPersonalManagement.CreateInstance(CompanyCode).PostDHRPAAcademicMemberInf(academicMemberInfo);
                if (postResult.returnCode != 101)
                {

                    data.Remark = postResult.returnDesc;
                    var DataDetail = JsonConvert.SerializeObject(academicMemberInfo);
                    throw new DataServiceException("HRPA", "SAVE_PERSONALINFO", "RefNo : " + postResult.refNo + " ReturnCode : " + postResult.returnCode + " ReturnDesc :  " + postResult.returnDesc + " DataDetail : " + DataDetail);
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "SAVE_ACADEMICINFO", ex.Message);
            }
        }

        public override List<PersonalTrainingCenter> getTrainingHistory(string oEmployeeID)
        {
            List<PersonalTrainingCenter> oTrainingHistory = new List<PersonalTrainingCenter>();
            ResponseModel<PATrainingHis> responseModel = new ResponseModel<PATrainingHis>();
            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPATrainingHisByEmpCode(DefaultBeginDate, DateTime.Now.ToString(DefaultDateGet), oEmployeeID);
                foreach (PATrainingHis row in responseModel.Data)
                {
                    PersonalTrainingCenter oTemp = new PersonalTrainingCenter();
                    oTemp.PaTrId = row.paTrId;
                    oTemp.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, row.startDate); 
                    oTemp.EndDate = DateTimeService.ConvertDateTime(CompanyCode, row.endDate);
                    oTemp.EmployeeID = oEmployeeID;
                    oTemp.CourseName = row.courseName;
                    oTemp.CompanyCode = row.companyCode;
                    oTrainingHistory.Add(oTemp);
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetTrainingHistory Error", responseModel.message);
            }

            return oTrainingHistory;
        }

        public override List<PersonalAcademicMemberCenter> GetAcedemicworkmember(string oEmployeeID)
        {
            List<PersonalAcademicMemberCenter> oTrainingHistory = new List<PersonalAcademicMemberCenter>();
            ResponseModel<PAAcademicMemberInf> responseModel = new ResponseModel<PAAcademicMemberInf>();
            try
            {
  

                 responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAcedemicworkmemberByEmpCode(DefaultBeginDate, DateTime.Now.ToString(DefaultDateGet), oEmployeeID);
                foreach (PAAcademicMemberInf row in responseModel.Data)
                {
                    PersonalAcademicMemberCenter oTemp = new PersonalAcademicMemberCenter();
                    oTemp.CompanyCode = CompanyCode;
                    oTemp.EmpCode = oEmployeeID;
                    oTemp.BeginDate = Convert.ToDateTime(row.startDate);
                    oTemp.EndDate = Convert.ToDateTime(row.endDate); 
                    oTemp.PAReMemID = row.paReMemId;
                    oTemp.PAReID = row.paReId;
                    oTemp.Title = row.title;
                    oTemp.SubtypeValue = row.subtypeValue;
                    oTemp.SubtypeTextTH = row.subtypeTextTH;
                    oTemp.SubtypeTextEN = row.subtypeTextEN;
                    oTemp.RoleCode = row.roleCode;
                    oTemp.RoleValue = row.roleValue;
                    oTemp.RoleNameTH = row.roleNameTH;
                    oTemp.RoleNameEN = row.roleNameEN;
                    oTrainingHistory.Add(oTemp);


                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetAcedemicworkmember Error", responseModel.message);
            }

            return oTrainingHistory;
        }

       


        public override List<PersonalPunishmentCenter> GetPunishmentList(string oEmployeeID)
        {
            List<PersonalPunishmentCenter> oPunishmentList = new List<PersonalPunishmentCenter>();
            ResponseModel<PAPunishmentInf> responseModel = new ResponseModel<PAPunishmentInf>();

            try
            {
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPunishmentInfByEmpCode(DefaultBeginDate, DateTime.Now.ToString(DefaultDateGet), oEmployeeID);
                foreach (PAPunishmentInf row in responseModel.Data)
                {
                    PersonalPunishmentCenter oTemp = new PersonalPunishmentCenter();
                    //oTemp.Begindate = DateTime.ParseExact(row.startDate, DefaultReturnDateFormat, null, DateTimeStyles.None);
                    oTemp.Begindate = DateTimeService.ConvertDateTime(CompanyCode, row.startDate);
                    //oTemp.Enddate = DateTime.ParseExact(row.endDate, DefaultReturnDateFormat, null, DateTimeStyles.None);
                    oTemp.Enddate = DateTimeService.ConvertDateTime(CompanyCode, row.endDate);
                    oTemp.EmployeeID = oEmployeeID;
                    oTemp.CompanyCode = row.companyCode;
                    oTemp.CommandNo = row.commandNo;
                    oTemp.Detail = row.detail;
                    oTemp.EvidenceCode = row.evidenceCode;
                    oTemp.EvidenceValue = row.evidenceValue;
                    oTemp.PunishmentCode = row.punishmentCode;
                    oTemp.PunishmentValue = row.punishmentValue;
                    oTemp.StartCode = row.startCode;
                    oTemp.StartValue = row.startValue;
                    oTemp.StatusCode = row.statusCode;
                    oTemp.StatusValue = row.statusValue;
                    oTemp.EvidenceDesc = row.evidenceTextTH;
                    oTemp.PunishmentDesc = row.punishmentTextTH;
                    oTemp.StartValueDesc = row.startTextTH;
                    oTemp.StatusDesc = row.statusTextTH;
                    oPunishmentList.Add(oTemp);
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "Get Punishment list Error", responseModel.message);
            }

            return oPunishmentList;
        }

        public override void UpdatePersonalDataAfterApprovedImmediatly(string oEmployeeID)
        {
            // Process's concept clon from job of personaldata but only 1 employee for criteria emp1 , emp2
            APIJobPersonalManagement oApiJobPersonalData = APIJobPersonalManagement.CreateInstance(CompanyCode);
            ESS.EMPLOYEE.EmployeeManagement oEmpMng = ESS.EMPLOYEE.EmployeeManagement.CreateInstance(CompanyCode);
            string oErrMsg = string.Empty;
            try
            {

                #region GetDataFromDHRToTempOnDB
                List<PAInfoPersonal> infoPersonal = oApiJobPersonalData.GetAllInfoPersonal(DefaultBeginDate, DefaultEndDateGet).Data; // get from DHR
                List<PAContactInf> contactinf = oApiJobPersonalData.GetAllContactInf(DefaultBeginDate, DefaultEndDateGet).Data;
                List<PAPersonalInf> personalInf = oApiJobPersonalData.GetAllPersonalInf(DefaultBeginDate, DefaultEndDateGet).Data;

                List<PAInfoPersonal> infoPersonalFiltered = new List<PAInfoPersonal>();
                List<PAContactInf> contactinfFiltered = new List<PAContactInf>();
                List<PAPersonalInf> personalInfFiltered = new List<PAPersonalInf>();

                foreach (PAInfoPersonal row in infoPersonal)
                {
                    if (row.employeeID == oEmployeeID)
                        infoPersonalFiltered.Add(row);
                }

                foreach (PAContactInf row in contactinf)
                {
                    if (row.empCode == oEmployeeID)
                        contactinfFiltered.Add(row);
                }

                var emp = personalInf.Where(a => a.empCode == oEmployeeID).ToList();
                foreach (PAPersonalInf item in emp)
                {
                    //item.fullNameEnText = item.fullNameEnText+ item.fnameEn + " " + item.lnameEn;
                    personalInfFiltered.Add(item);
                }
                //foreach (PAPersonalInf row in personalInf)
                //{

                //    if (row.empCode == oEmployeeID)
                //    {
                //        //add here pariyaporn
                //        row.fullNameEnText = TitleNameFormatEN(row) + row.fnameEn + " " + row.lnameEn;
                //        personalInfFiltered.Add(row);
                //    }

                //}

                if (infoPersonalFiltered.Count > 0)
                {
                    oErrMsg += oApiJobPersonalData.SaveAllInfoPersonal(infoPersonalFiltered); //ลง [DHR_PA_InfoPersonal]
                    oEmpMng.JobALL_DHR_TO_INFOTYPE0001(oEmployeeID, oEmployeeID, "DHR"); //จาก [DHR_PA_InfoPersonal]
                }
                if (contactinfFiltered.Count > 0)
                {
                    oErrMsg += oApiJobPersonalData.SaveAllContactInf(contactinfFiltered); // ลง [DHR_PA_ContactInf]
                    oEmpMng.JobALL_DHR_TO_INFOTYPE0105(oEmployeeID, oEmployeeID, "DHR"); //จาก [DHR_PA_ContactInf] 
                }
                if (personalInfFiltered.Count > 0)
                {
                    oErrMsg += oApiJobPersonalData.SaveAllPersonalInf(personalInfFiltered); //ลง [DHR_PA_PersonalInf]
                    oEmpMng.JobALL_DHR_TO_INFOTYPE0002(oEmployeeID, oEmployeeID, "DHR"); //จาก [DHR_PA_PersonalInf]
                    oEmpMng.JobALL_DHR_TO_INFOTYPE0182(oEmployeeID, oEmployeeID, "DHR"); //จาก [DHR_PA_PersonalInf]
                }

                #endregion

                //ยังไม่ได้ใช้ตอนนี้
                //oEmpMng.JobALL_DHR_TO_INFOTYPE0007(oEmployeeID, oEmployeeID, "DHR");
                //oEmpMng.JobALL_DHR_TO_INFOTYPE0027(oEmployeeID, oEmployeeID, "DHR");
                //oEmpMng.JobALL_DHR_TO_INFOTYPE0030(oEmployeeID, oEmployeeID, "DHR");
            }
            catch (Exception ex)
            {
                throw new Exception("Error UpdatePersonalDataAfterApprovedImmediatly" + ex.Message);
            }
        }

        public override List<PersonalInfoCenter> GetPersonalInfoData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PersonalInfoCenter> returnValue = new List<PersonalInfoCenter>();
            ResponseModel<PAPrivateInf> responseModel = new ResponseModel<PAPrivateInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPrivateInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data)
                    {
                        PersonalInfoCenter list = new PersonalInfoCenter();
                        list.BeginDate = DateTimeService.ConvertDateTime(CompanyCode, item.startDate);
                        list.EndDate = DateTimeService.ConvertDateTime(CompanyCode, item.endDate);
                        list.BirthDate = DateTimeService.ConvertDateTime(CompanyCode, item.birthDate);
                        list.BloodType = item.bloodGrpTextTH;
                        list.IDCard = item.idCard;
                        


                        returnValue.Add(list);
                    }

                    //var distinctResult = returnValue.GroupBy(i => new { i.EducationLevelCode, i.BeginDate, i.EndDate }).Select(g => g.First()).ToList();
                    //   returnValue = distinctResult.ToList();
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HRPA", "GetPersonalInfo Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAPersonalInf> GetPersonalInfoDataDetail(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PAPersonalInf> returnValue = new List<PAPersonalInf>();
            ResponseModel<PAPersonalInf> responseModel = new ResponseModel<PAPersonalInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPersonalInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetPersonalInfo Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAContactInf> GetContractData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PAContactInf> returnValue = new List<PAContactInf>();
            ResponseModel<PAContactInf> responseModel = new ResponseModel<PAContactInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAContactInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetContractData Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PADocumentInf> GetDocumentData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PADocumentInf> returnValue = new List<PADocumentInf>();
            ResponseModel<PADocumentInf> responseModel = new ResponseModel<PADocumentInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPADocumentInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetContractData Error", responseModel.message);
            }

            return returnValue;
        }
        
        public override List<PAClothingSize> GetClothingSizeData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PAClothingSize> returnValue = new List<PAClothingSize>();
            ResponseModel<PAClothingSize> responseModel = new ResponseModel<PAClothingSize>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAClothingSizeByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetContractData Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAEvaluationInf> GetEvaluationInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PAEvaluationInf> returnValue = new List<PAEvaluationInf>();
            ResponseModel<PAEvaluationInf> responseModel = new ResponseModel<PAEvaluationInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAEvaluationInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetContractData Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAMedicalCheckup> GetMedicalCheckupData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PAMedicalCheckup> returnValue = new List<PAMedicalCheckup>();
            ResponseModel<PAMedicalCheckup> responseModel = new ResponseModel<PAMedicalCheckup>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAMedicalCheckupByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetContractData Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAPunishmentInf> GetPunishmentInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            List<PAPunishmentInf> returnValue = new List<PAPunishmentInf>();
            ResponseModel<PAPunishmentInf> responseModel = new ResponseModel<PAPunishmentInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAPunishmentInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetContractData Error", responseModel.message);
            }

            return returnValue;
        }



        //no use
        public override List<PAFamilyInf> GetDHRPAFamilyInf(DateTime BeginDate, DateTime EndDate)
        {
            List<PAFamilyInf> returnValue = new List<PAFamilyInf>();
            ResponseModel<PAFamilyInf> responseModel = new ResponseModel<PAFamilyInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAFamilyInf(StringBeginDate, StringEndDate);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }

            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetDHRPAFamilyInf Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAFamilyInf> GetRelationList(DateTime BeginDate, DateTime EndDate)
        {
            List<PAFamilyInf> returnValue = new List<PAFamilyInf>();
            ResponseModel<PAFamilyInf> responseModel = new ResponseModel<PAFamilyInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAFamilyInf(StringBeginDate, StringEndDate);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    var data = from p in responseModel.Data
                               group p by new { p.relateValue, p.relateCodeTextTH, p.relateCodeTextEN } into g
                               orderby g.Key.relateValue
                               select new
                               {
                                   relateValue = g.Key.relateValue,
                                   relateCodeTextTH = g.Key.relateCodeTextTH,
                                   relateCodeTextEN = g.Key.relateCodeTextEN
                               };
                    //returnValue = responseModel.Data;
                    foreach (var item in data)
                    {
                        PAFamilyInf list = new PAFamilyInf();
                        list.relateValue = item.relateValue;
                        list.relateCodeTextTH = item.relateCodeTextTH;
                        list.relateCodeTextEN = item.relateCodeTextTH;

                        returnValue.Add(list);

                        //returnValue
                    }
                }

            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetDHRPAFamilyInf Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAFamilyInf> GetDHRPAFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate, string EmpCode)
        {
            List<PAFamilyInf> returnValue = new List<PAFamilyInf>();
            ResponseModel<PAFamilyInf> responseModel = new ResponseModel<PAFamilyInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAFamilyInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetDHRPAFamilyInfByEmpCode Error", responseModel.message);
            }

            return returnValue;
        }
        public override List<PAAddressFamilyInf> GetDHRPAAddressFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate, string EmpCode)
        {
            List<PAAddressFamilyInf> returnValue = new List<PAAddressFamilyInf>();
            ResponseModel<PAAddressFamilyInf> responseModel = new ResponseModel<PAAddressFamilyInf>();
            try
            {
                string StringBeginDate = BeginDate.ToString("ddMMyyyy");
                string StringEndDate = EndDate.ToString("ddMMyyyy");
                responseModel = APIPersonalManagement.CreateInstance(CompanyCode).GetDHRPAAddressFamilyInfByEmpCode(StringBeginDate, StringEndDate, EmpCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    returnValue = responseModel.Data;
                }
            }
            catch (Exception)
            {
                throw new DataServiceException("HRPA", "GetDHRPAAddressFamilyInfByEmpCode Error", responseModel.message);
            }

            return returnValue;
        }
    }
}