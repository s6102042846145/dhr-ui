﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using DHR.HR.API.Interfaces;
using DHR.HR.API.Model;

using ESS.SHAREDATASERVICE;


namespace DHR.HR.API
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework

        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "DHR.HR";

        public string CompanyCode { get; set; }

        public string BaseUrl { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
              
            };
            return oServiceManager;
        }

        #endregion MultiCompany  Framework

        #region "privateData"
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"{ModuleID}.{Mode.ToUpper()}");
            string typeName = string.Format($"{ModuleID}.{Mode.ToUpper()}.{ClassName}");
            oAssembly = Assembly.Load(assemblyName); // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);   // Load class

            return oReturn;
        }

        #endregion "privateData"

        //internal IAuthentication GetAuthentication(string Mode)
        //{
        //    Type oType = GetService(Mode, "Authentication");
        //    if(oType == null)
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        return (IAuthentication)Activator.CreateInstance(oType, CompanyCode);
        //    }
        //}

        private string MODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MODE");
            }
        }

        

        public IAuthentication API
        {
            get
            {
                Type otype = GetService(MODE, "Authentication");
                if(otype == null)
                {
                    return null;
                }
                else
                {
                    return (IAuthentication)Activator.CreateInstance(otype, CompanyCode);
                }
            }
        }
    }
}
