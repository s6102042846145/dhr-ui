﻿using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.HR.API
{
    public class PaymentRoute
    {
        public PaymentRoute()
        {

        }
        private static string ModuleID = "DESS.ROUTE.PY";
        private string CompanyCode { get; set; }

        public static PaymentRoute Route (string oCompanyCode)
        {
            PaymentRoute oPaymentRoute = new PaymentRoute()
            {
                CompanyCode = oCompanyCode
            };
            return oPaymentRoute;
        }
        
        #region example variable
        public string PaymentRoute_WsSentPYTaxAllowance
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYTAXALLOWANCE");
            }
        }

        public string PaymentRoute_WsSentPYGetProvidentfund
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPYGETPROVIDENTFUND");
            }
        }
        
        #endregion


    }
}
