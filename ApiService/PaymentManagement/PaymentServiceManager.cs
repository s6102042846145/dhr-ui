﻿using DESS.HR.API.Interfaces;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DESS.HR.API
{
    public class PaymentServiceManager
    {
        #region Constructor
        public PaymentServiceManager()
        {

        }
        #endregion Constructor
        #region Multicompany Framework

        private static string ModuleID = "DESS.HR";

        private string CompanyCode { get; set; }

        private string BaseUrl { get; set; }

        private string Gateway { get; set; }

        public static PaymentServiceManager CreateInstance(string oCompanyCode,string oBaseUrl ,string oGateway)
        {
            PaymentServiceManager oPaymentServiceManager = new PaymentServiceManager()
            {
                CompanyCode = oCompanyCode,
                BaseUrl = oBaseUrl,
                Gateway = oGateway
            };
            return oPaymentServiceManager;
        }
        #endregion Multicompany Framework

        #region Private Data
        private Type GetService (string Mode,string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"{ModuleID}.{Mode.ToUpper()}");
            string typeName = string.Format($"{ModuleID}.{Mode.ToUpper()}.{ClassName}");

            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);

            return oReturn;
        }
        #endregion Private Data

        private string MODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MODE");
            }
        }

        public IPayment API
        {
            get
            {
                Type oType = GetService(MODE, "Payment");
                if(oType == null)
                {
                    return null;
                }
                else
                {
                    return (IPayment)Activator.CreateInstance(oType, CompanyCode, BaseUrl, Gateway);
                }
            }
        }
    }
}
