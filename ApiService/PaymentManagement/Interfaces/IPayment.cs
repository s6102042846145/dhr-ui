﻿using DESS.HR.API.Model;
using DESS.HR.API.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.HR.API.Interfaces
{
    public interface IPayment
    {
        #region GET ALL

        ResponseModel<PYProvidentfundInf> GetDHRPYPaymentGetProvidentFund(string startDate, string endDate, string ApiKey, string EmpCode);

        #endregion GET ALL

        #region GET BY EMPCODE

        ResponseModel<PYTaxAllowanceInf> GetDHRPYPaymentTaxAllowanceByEmpcode(string startDate, string endDate, string ApiKey, string EmpCode);
        
        #endregion GET BY EMPCODE
    }
}
