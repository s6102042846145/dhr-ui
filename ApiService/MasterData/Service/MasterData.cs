﻿using DHR.HR.API.Interfaces;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace DHR.HR.API
{
    public class MasterData : IMasterData
    {
        #region  "Constructor"

        public MasterData(string oCompanyCode, string oBaseUrl, string oGateway)
        {
            CompanyCode = oCompanyCode;
            BaseUrl = oBaseUrl;
            Gateway = oGateway;
        }
        #endregion "Constructor"

        #region "Member"
        private string ModuleID = "DHR.HR";
        private string CompanyCode { get; set; }
        private string BaseUrl { get; set; }
        private string Gateway { get; set; }
        private string ContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONTENTTYPE");
            }

        }

        private string Scheme
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SCHEME");
            }
        }

        private string HeaderContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "HEADERCONTENTTYPE");
            }
        }

        private int MaxReconnectLimit
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAXRECONNECTIONATTEMPTLIMIT"));
            }
        }

        #endregion
        public ResponseModel<MasterDataCode> GetDHRDataCode(string startUpdateDate, string endUpdateDate, string ApiKey)
        {
            ResponseModel<MasterDataCode> responseModel = new ResponseModel<MasterDataCode>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = MasterDataRoute.Route(CompanyCode).MasterDataRoute_WsSentDataCode;
                    string strParams = startUpdateDate + "/" + endUpdateDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<MasterDataCode>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<MasterDataValue> GetDHRDataValue(string startUpdateDate, string endUpdateDate, string ApiKey)
        {
            ResponseModel<MasterDataValue> responseModel = new ResponseModel<MasterDataValue>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = MasterDataRoute.Route(CompanyCode).MasterDataRoute_WsSentDataValue;
                    string strParams = startUpdateDate + "/" + endUpdateDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<MasterDataValue>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }

                }
                connnectionTime++;
            }

            return responseModel;

        }
        public ResponseModel<MTValidateDataCodeValue> GetMTValidateDataCodeValue(string dataCode, string dataValue, string ApiKey)
        {
            ResponseModel<MTValidateDataCodeValue> responseModel = new ResponseModel<MTValidateDataCodeValue>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);
                    //?companyCode=1000
                    string oRoute = MasterDataRoute.Route(CompanyCode).MasterDataRoute_WsSentMTValidateCodeValue;
                    string strParams = dataCode + "/" + dataValue+ "?companyCode="+CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<MTValidateDataCodeValue>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }

                }
                connnectionTime++;
            }

            return responseModel;

        }
        public ResponseModel<MTValidateTitleName> GetMTValidateTitleName(string companyCode, string language,
            string iname_code = null, string iname_value = null,
            string iname1_code = null, string iname1_value = null,
            string iname2_code = null, string iname2_value = null,
            string iname3_code = null, string iname3_value = null,
            string iname4_code = null, string iname4_value = null,
            string iname5_code = null, string iname5_value = null, string ApiKey = null)
        {
            ResponseModel<MTValidateTitleName> responseModel = new ResponseModel<MTValidateTitleName>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = MasterDataRoute.Route(CompanyCode).MasterDataRoute_WsSentMTValidateTitle;
                    string strParams = string.Empty;

                    if (!string.IsNullOrEmpty(iname_code) && !string.IsNullOrEmpty(iname_value))
                    {
                        strParams = strParams + "inameCode=" + iname_code + "&inameValue=" + iname_value;
                    }

                    if (!string.IsNullOrEmpty(iname1_code) && !string.IsNullOrEmpty(iname1_value))
                    {
                        if (!string.IsNullOrEmpty(strParams)) strParams = strParams + "&";
                        strParams = strParams + "iname1Code=" + iname1_code + "&iname1Value=" + iname1_value;
                    }

                    if (!string.IsNullOrEmpty(iname2_code) && !string.IsNullOrEmpty(iname2_value))
                    {
                        if (!string.IsNullOrEmpty(strParams)) strParams = strParams + "&";
                        strParams = strParams + "iname2Code=" + iname2_code + "&iname2Value=" + iname2_value;
                    }

                    if (!string.IsNullOrEmpty(iname3_code) && !string.IsNullOrEmpty(iname3_value))
                    {
                        if (!string.IsNullOrEmpty(strParams)) strParams = strParams + "&";
                        strParams = strParams + "iname3Code=" + iname3_code + "&iname3Value=" + iname3_value;
                    }

                    if (!string.IsNullOrEmpty(iname4_code) && !string.IsNullOrEmpty(iname4_value))
                    {
                        if (!string.IsNullOrEmpty(strParams)) strParams = strParams + "&";
                        strParams = strParams + "iname4Code=" + iname4_code + "&iname4Value=" + iname4_value;
                    }

                    if (!string.IsNullOrEmpty(iname5_code) && !string.IsNullOrEmpty(iname5_value))
                    {
                        if (!string.IsNullOrEmpty(strParams)) strParams = strParams + "&";
                        strParams = strParams + "iname5Code=" + iname5_code + "&iname5Value=" + iname5_value;
                    }
                    //1000/TH?
                    strParams = companyCode + "/" + language + "?" + strParams;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<MTValidateTitleName>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }

                }
                connnectionTime++;
            }

            return responseModel;

        }
        public ResponseModel<DistrictProvice> GetDHRDistrictProvince(string provinceCode, string districtCode, string subdistrictCode, string postcode, string ApiKey)
        {
            ResponseModel<DistrictProvice> responseModel = new ResponseModel<DistrictProvice>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string strParams = string.Empty;

                    if (!string.IsNullOrEmpty(provinceCode))
                    {
                        strParams = strParams + "provinceCode=" + provinceCode;
                    }

                    if (!string.IsNullOrEmpty(districtCode))
                    {
                        if (!string.IsNullOrEmpty(strParams))
                        {
                            strParams = strParams + "&";
                        }
                        strParams = strParams + "districtCode=" + districtCode;
                    }

                    if (!string.IsNullOrEmpty(subdistrictCode))
                    {
                        if (!string.IsNullOrEmpty(strParams))
                        {
                            strParams = strParams + "&";
                        }
                        strParams = strParams + "subdistrictCode=" + subdistrictCode;
                    }

                    if (!string.IsNullOrEmpty(postcode))
                    {
                        if (!string.IsNullOrEmpty(strParams))
                        {
                            strParams = strParams + "&";
                        }
                        strParams = strParams + "postcode=" + postcode;
                    }

                    string oRoute = MasterDataRoute.Route(CompanyCode).MasterDataRoute_WsSentDistrictProvince;
                    string ApiGateWay = string.Empty;
                    if (!string.IsNullOrEmpty(Gateway))
                    {
                        ApiGateWay = Gateway + "/";
                    }
                    ApiGateWay = ApiGateWay + oRoute + strParams;
                    //string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<DistrictProvice>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }

                }
                connnectionTime++;
            }

            return responseModel;
        }
    }
}
