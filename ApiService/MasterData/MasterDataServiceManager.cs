﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;
using DHR.HR.API.Interfaces;
using System.Reflection;

namespace DHR.HR.API
{
    public class MasterDataServiceManager
    {
        #region "Constructor"
        public MasterDataServiceManager()
        {

        }
        #endregion "Constructor"

        #region "Multicompany Framework"

        private static string ModuleID = "DHR.HR";

        public string CompanyCode { get; set; }

        public string BaseUrl { get; set; }

        public string Gateway { get; set; }

        public static MasterDataServiceManager CreateInstance(string oCompanyCode , string oBaseUrl , string oGateway)
        {
            MasterDataServiceManager oMasterDataServiceManager = new MasterDataServiceManager()
            {
                CompanyCode = oCompanyCode,
                BaseUrl = oBaseUrl,
                Gateway = oGateway
            };
            return oMasterDataServiceManager;
        }

        #endregion "Multicompany Framework"


        #region "private Data"
        private Type GetService (string Mode,string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"{ModuleID}.{Mode.ToUpper()}");
            string typeName = string.Format($"{ModuleID}.{Mode.ToUpper()}.{ClassName}");
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);

            return oReturn;
        }

        private string MODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MODE");
            }
        }
        #endregion "private Data"

        public IMasterData API
        {
            get
            {
                Type oType = GetService(MODE, "MasterData");
                if (oType == null)
                {
                    return null;
                }
                else
                    return (IMasterData)Activator.CreateInstance(oType, CompanyCode, BaseUrl, Gateway);
            }
        }

    }
}
