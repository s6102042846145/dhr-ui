﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;

namespace DHR.HR.API.Interfaces
{
    public interface IMasterData
    {
        ResponseModel<MasterDataCode> GetDHRDataCode(string startUpdateDate, string endUpdateDate, string ApiKey);

        ResponseModel<MasterDataValue> GetDHRDataValue(string startUpdateDate, string endUpdateDate, string ApiKey);

        ResponseModel<DistrictProvice> GetDHRDistrictProvince(string provinceCode,string districtCode, string subdistrictCode,string postcode, string ApiKey);

        ResponseModel<MTValidateDataCodeValue> GetMTValidateDataCodeValue(string dataCode,string dataValue, string ApiKey);

        ResponseModel<MTValidateTitleName> GetMTValidateTitleName(string companyCode, string language, string iname_code, string iname_value, string iname1_code, string iname1_value,
            string iname2_code, string iname2_value, string iname3_code, string iname3_value, string iname4_code, string iname4_value, string iname5_code, string iname5_value, string ApiKey);
    }
}
