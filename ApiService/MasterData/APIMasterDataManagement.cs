﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Web;

namespace DHR.HR.API
{
    public class APIMasterDataManagement
    {
        #region "Constructor"
        public APIMasterDataManagement()
        {

        }
        #endregion

        #region "Multicompany Framework"
        private Dictionary<string, APIMasterDataManagement> cache = new Dictionary<string, APIMasterDataManagement>();

        private static string ModuleID = "DHR.HR";

        public string CompanyCode { get; set; }

        private string _ApiKey = string.Empty;

        private string _empCode = string.Empty;

        public static APIMasterDataManagement CreateInstance(string oCompanyCode)
        {
            APIMasterDataManagement oAPIMasterDataManagement = new APIMasterDataManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIMasterDataManagement;
        }

        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");
            }
        }

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }

        private string ApiUser
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIUSER");
            }
        }

        private string ApiPassword
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIPASSWORD");
            }
        }

        public string ApiKey
        {
            get
            {
                string ApiKeyRequest = string.Format("ApiKeyRequest_{0}_{1}", CompanyCode, _empCode);
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                    {
                        _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                    }
                    else

                    {
                        _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                        HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                    }

                }
                else
                {
                    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                }
                return _ApiKey;

            }
            set
            {
                _ApiKey = value;
            }
        }
        public int Period_Time_Expired
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PERIODTIMEEXPIRED"));
            }
        }
        //private string ApiKey
        //{
        //    get
        //    {
        //        return APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
        //    }
        //}
        #endregion "Multicompany Framework"

        public ResponseModel<MasterDataCode> GetDHRMasterDataCode(string startUpdateDate, string endUpdateDate)
        {
            return MasterDataServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRDataCode(startUpdateDate, endUpdateDate, ApiKey);
        }

        public ResponseModel<MasterDataValue> GetDHRMasterDataValue(string startUpdateDate, string endUpdateDate)
        {
            return MasterDataServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRDataValue(startUpdateDate, endUpdateDate, ApiKey);
        }
        public ResponseModel<MTValidateDataCodeValue> GetMTValidateDataCodeValue(string dataCode, string dataValue)
        {
            return MasterDataServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetMTValidateDataCodeValue(dataCode, dataValue, ApiKey);
        }
        public ResponseModel<MTValidateTitleName> GetMTValidateTitleName(string companyCode, string language, string iname_code, string iname_value, string iname1_code, string iname1_value,
            string iname2_code, string iname2_value, string iname3_code, string iname3_value, string iname4_code, string iname4_value, string iname5_code, string iname5_value)
        {
            return MasterDataServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetMTValidateTitleName(companyCode, language, iname_code, iname_value, iname1_code, iname1_value,
           iname2_code, iname2_value, iname3_code, iname3_value, iname4_code, iname4_value, iname5_code, iname5_value, ApiKey);
        }
        public ResponseModel<DistrictProvice> GetDHRDistrictProvince(string provinceCode, string districtCode, string subdistrictCode, string postcode)
        {
            return MasterDataServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRDistrictProvince(provinceCode, districtCode, subdistrictCode, postcode, ApiKey);
        }
    }
}
