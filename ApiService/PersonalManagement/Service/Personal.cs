﻿using DHR.HR.API.Interfaces;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;
using ESS.EMPLOYEE;

namespace DHR.HR.API
{
    public class Personal : IPersonal
    {
        #region Constructor
        public Personal(string oCompanyCode, string oBaseUrl, string oGateway)
        {
            CompanyCode = oCompanyCode;
            BaseUrl = oBaseUrl;
            Gateway = oGateway;
        }
        #endregion Constructor

        #region Member
        private string ModuleID = "DHR.HR";
        private string CompanyCode { get; set; }
        private string BaseUrl { get; set; }
        private string Gateway { get; set; }
        private string ContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONTENTTYPE");
            }

        }

        private string Scheme
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SCHEME");
            }
        }

        private string HeaderContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "HEADERCONTENTTYPE");
            }
        }

        private int MaxReconnectLimit
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAXRECONNECTIONATTEMPTLIMIT"));
            }
        }

        #endregion Member

        #region GET

        public ResponseModel<PAActingPosition> GetDHRPAActingPosition(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAActingPosition> responseModel = new ResponseModel<PAActingPosition>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAActingPosition;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAActingPosition>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAActingPosition> GetDHRPAActingPositionByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAActingPosition> responseModel = new ResponseModel<PAActingPosition>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAActingPosition;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAActingPosition>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAddressInf> GetDHRPAAddressInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAAddressInf> responseModel = new ResponseModel<PAAddressInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAAddressInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAddressInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAddressInf> GetDHRPAAddressInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAAddressInf> responseModel = new ResponseModel<PAAddressInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAAddressInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAddressInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PACertificateInf> GetDHRPACertificateInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PACertificateInf> responseModel = new ResponseModel<PACertificateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPACertificateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PACertificateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PACertificateInf> GetDHRPACertificateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PACertificateInf> responseModel = new ResponseModel<PACertificateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPACertificateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PACertificateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PACommandInf> GetDHRPACommandInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PACommandInf> responseModel = new ResponseModel<PACommandInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPACommandInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PACommandInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }

                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PACommandInf> GetDHRPACommandInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PACommandInf> responseModel = new ResponseModel<PACommandInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPACommandInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PACommandInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAContactInf> GetDHRPAContactInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAContactInf> responseModel = new ResponseModel<PAContactInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAContactInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAContactInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAContactInf> GetDHRPAContactInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAContactInf> responseModel = new ResponseModel<PAContactInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAContactInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAContactInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PADiseaseInf> GetDHRPADiseaseInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PADiseaseInf> responseModel = new ResponseModel<PADiseaseInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPADiseaseInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PADiseaseInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PADiseaseInf> GetDHRPADiseaseInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PADiseaseInf> responseModel = new ResponseModel<PADiseaseInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPADiseaseInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PADiseaseInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAEducationInf> GetDHRPAEducationInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAEducationInf> responseModel = new ResponseModel<PAEducationInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAEducationInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAEducationInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAEducationInf> GetDHRPAEducationInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAEducationInf> responseModel = new ResponseModel<PAEducationInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";
            endDate = "31129999";
     
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);
                    //client.DefaultRequestHeaders.Add("ActionBy", "UI");
                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAEducationInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAEducationInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAEmployeeInf> GetDHRPAEmployeeInfByEmpCode(string startDate, string endDate, string ApiKey, string CompanyCode, string EmpCode, string UnitSelected, string LevelSelected, string PositionSelected)
        {
            ResponseModel<PAEmployeeInf> responseModel = new ResponseModel<PAEmployeeInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");
                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_UiGetPAEmployoeeInf;
                    string strParams = null;
                    if (!string.IsNullOrEmpty(EmpCode))
                    {
                        strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode + "?unitCode=" + UnitSelected + "&unitLevelValue=" + LevelSelected + "&posCode=" + PositionSelected;
                    }
                    if (string.IsNullOrEmpty(EmpCode))
                    {
                        strParams = startDate + "/" + endDate + "/" + CompanyCode + "?unitCode=" + UnitSelected + "&unitLevelValue=" + LevelSelected + "&posCode=" + PositionSelected;
                    }
                     //?unitCode = 10003000 & unitLevelValue = 030 & posCode = 10000001
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAEmployeeInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        




        public ResponseModel<PAEvaluationInf> GetDHRPAEvaluationInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAEvaluationInf> responseModel = new ResponseModel<PAEvaluationInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAEvaluationInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAEvaluationInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }

                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAEvaluationInf> GetDHRPAEvaluationInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAEvaluationInf> responseModel = new ResponseModel<PAEvaluationInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAEvaluationInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAEvaluationInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAFamilyInf> GetDHRPAFamilyInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAFamilyInf> responseModel = new ResponseModel<PAFamilyInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAFamilyInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAFamilyInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAFamilyInf> GetDHRPAFamilyInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAFamilyInf> responseModel = new ResponseModel<PAFamilyInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAFamilyInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAFamilyInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                        string oWorkAction = string.Format($"Error GetDHRPAFamilyInfByEmpCode : {EmpCode} ");
                        oWorkAction = oWorkAction + " " + responseModel.message;
                        EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, ApiGateWay, oWorkAction, true);

                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAHiringConditionInf> GetDHRPAHiringConditionInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAHiringConditionInf> responseModel = new ResponseModel<PAHiringConditionInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAHiringConditionInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAHiringConditionInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAHiringConditionInf> GetDHRPAHiringConditionInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAHiringConditionInf> responseModel = new ResponseModel<PAHiringConditionInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAHiringConditionInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAHiringConditionInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAImportanceDateInf> GetDHRPAImportanceDateInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAImportanceDateInf> responseModel = new ResponseModel<PAImportanceDateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAImportanceDateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAImportanceDateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAImportanceDateInf> GetDHRPAImportanceDateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAImportanceDateInf> responseModel = new ResponseModel<PAImportanceDateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAImportanceDateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAImportanceDateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PALanguageTestInf> GetDHRPALanguageTestInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PALanguageTestInf> responseModel = new ResponseModel<PALanguageTestInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPALanguageTestInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PALanguageTestInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PALanguageTestInf> GetDHRPALanguageTestInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PALanguageTestInf> responseModel = new ResponseModel<PALanguageTestInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPALanguageTestInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PALanguageTestInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAOrganizationInf> GetDHRPAOrganizationInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAOrganizationInf> responseModel = new ResponseModel<PAOrganizationInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAOrganizationInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAOrganizationInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAOrganizationInf> GetDHRPAOrganizationInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAOrganizationInf> responseModel = new ResponseModel<PAOrganizationInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAOrganizationInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAOrganizationInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPersonalInf> GetDHRPAPersonalInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAPersonalInf> responseModel = new ResponseModel<PAPersonalInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPersonalInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    //HttpResponseMessage response = new HttpResponseMessage();
                    //response.Content.Headers.ContentType.MediaType = ContentType;
                    //response = client.GetAsync(ApiGateWay).Result;
                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPersonalInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPersonalInf> GetDHRPAPersonalInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAPersonalInf> responseModel = new ResponseModel<PAPersonalInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPersonalInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPersonalInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPosition> GetDHRPAPosition(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAPosition> responseModel = new ResponseModel<PAPosition>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPosition;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPosition>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPosition> GetDHRPAPositionByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAPosition> responseModel = new ResponseModel<PAPosition>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPosition;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPosition>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPositionHis> GetDHRPAPositionHis(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAPositionHis> responseModel = new ResponseModel<PAPositionHis>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPositionHis;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPositionHis>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPositionHis> GetDHRPAPositionHisByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAPositionHis> responseModel = new ResponseModel<PAPositionHis>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPositionHis;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPositionHis>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPrivateInf> GetDHRPAPrivateInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAPrivateInf> responseModel = new ResponseModel<PAPrivateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPrivateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPrivateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPrivateInf> GetDHRPAPrivateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAPrivateInf> responseModel = new ResponseModel<PAPrivateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPrivateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPrivateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPunishmentInf> GetDHRPAPunishmentInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAPunishmentInf> responseModel = new ResponseModel<PAPunishmentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPunishmentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPunishmentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPunishmentInf> GetDHRPAPunishmentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAPunishmentInf> responseModel = new ResponseModel<PAPunishmentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPunishmentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPunishmentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PARelateEmpInf> GetDHRPARelateEmpInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PARelateEmpInf> responseModel = new ResponseModel<PARelateEmpInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPARelateEmpInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PARelateEmpInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PARelateEmpInf> GetDHRPARelateEmpInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PARelateEmpInf> responseModel = new ResponseModel<PARelateEmpInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPARelateEmpInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PARelateEmpInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PASecondmentInf> GetDHRPASecondmentInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PASecondmentInf> responseModel = new ResponseModel<PASecondmentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPASecondmentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PASecondmentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PASecondmentInf> GetDHRPASecondmentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PASecondmentInf> responseModel = new ResponseModel<PASecondmentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPASecondmentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PASecondmentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PASecondmentOutInf> GetDHRPASecondmentOutInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PASecondmentOutInf> responseModel = new ResponseModel<PASecondmentOutInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPASecondmentOutInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PASecondmentOutInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PASecondmentOutInf> GetDHRPASecondmentOutInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PASecondmentOutInf> responseModel = new ResponseModel<PASecondmentOutInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPASecondmentOutInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PASecondmentOutInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PATalentInf> GetDHRPATalentInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PATalentInf> responseModel = new ResponseModel<PATalentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPATalentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PATalentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PATalentInf> GetDHRPATalentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PATalentInf> responseModel = new ResponseModel<PATalentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPATalentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PATalentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PATrainingHis> GetDHRPATrainingHis(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PATrainingHis> responseModel = new ResponseModel<PATrainingHis>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";
            endDate = "31129999";
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPATrainingHis;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PATrainingHis>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PATrainingHis> GetDHRPATrainingHisByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PATrainingHis> responseModel = new ResponseModel<PATrainingHis>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";
            endDate = "31129999";
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPATrainingHis;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PATrainingHis>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAcademicMemberInf> GetDHRPAAcedemicworkmemberByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAAcademicMemberInf> responseModel = new ResponseModel<PAAcademicMemberInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";
            endDate = "31129999";
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_UIGetPAResearchMember;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAcademicMemberInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }



       




        #region NOT USE

        //เลิกใช้แล้ว Pariyaporn 20200327
        //public ResponseModel<PAWorkCalendar> GetDHRPAWorkCalendar(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<PAWorkCalendar> responseModel = new ResponseModel<PAWorkCalendar>();
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(BaseUrl);
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

        //        string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAWorkCalendar;
        //        string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //        //test
        //        //string strParams = startDate + "/" + endDate + "/" + "1000";
        //        string ApiGateWay = Gateway + "/" + oRoute + "/" + strParams;
        //        HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //        if(response.IsSuccessStatusCode)
        //        {
        //            var content = response.Content.ReadAsStringAsync().Result;
        //            responseModel = JsonConvert.DeserializeObject<ResponseModel<PAWorkCalendar>>(content);
        //        }
        //        else
        //        {
        //            responseModel.message = response.ReasonPhrase;
        //        }
        //    }
        //    return responseModel;
        //}
        //เลิกใช้แล้ว Pariyaporn 20200327
        //public ResponseModel<PAWorkCalendar> GetDHRPAWorkCalendarByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        //{
        //    ResponseModel<PAWorkCalendar> responseModel = new ResponseModel<PAWorkCalendar>();
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(BaseUrl);
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

        //        string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAWorkCalendar;
        //        string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode; 
        //        //test
        //        //string strParams = startDate + "/" + endDate + "/" + "1000" + "/" + EmpCode;
        //        string ApiGateWay = Gateway + "/" + oRoute + "/" + strParams;

        //        HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //        if(response.IsSuccessStatusCode)
        //        {
        //            var content = response.Content.ReadAsStringAsync().Result;
        //            responseModel = JsonConvert.DeserializeObject<ResponseModel<PAWorkCalendar>>(content);
        //        }
        //        else
        //        {
        //            responseModel.message = response.ReasonPhrase;
        //        }
        //    }
        //    return responseModel;
        //}

        #endregion  NOT USE

        public ResponseModel<PAWorkingHisInf> GetDHRPAWorkingHisInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAWorkingHisInf> responseModel = new ResponseModel<PAWorkingHisInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAWorkingHisInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAWorkingHisInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAWorkingHisInf> GetDHRPAWorkingHisInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAWorkingHisInf> responseModel = new ResponseModel<PAWorkingHisInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            startDate = "01012000";
            endDate = "31129999";
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);
                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAWorkingHisInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAWorkingHisInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPropationUpdateInf> GetDHRPAPropationUpdateInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAPropationUpdateInf> responseModel = new ResponseModel<PAPropationUpdateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPropationUpdateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPropationUpdateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAPropationUpdateInf> GetDHRPAPropationUpdateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAPropationUpdateInf> responseModel = new ResponseModel<PAPropationUpdateInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAPropationUpdateInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAPropationUpdateInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAMedicalCheckup> GetDHRPAMedicalCheckup(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAMedicalCheckup> responseModel = new ResponseModel<PAMedicalCheckup>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAMedicalCheckup;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAMedicalCheckup>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAMedicalCheckup> GetDHRPAMedicalCheckupByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAMedicalCheckup> responseModel = new ResponseModel<PAMedicalCheckup>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAMedicalCheckup;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAMedicalCheckup>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAddressFamilyInf> GetDHRPAAddressFamilyInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAAddressFamilyInf> responseModel = new ResponseModel<PAAddressFamilyInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAAddressFamilyInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAddressFamilyInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAddressFamilyInf> GetDHRPAAddressFamilyInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAAddressFamilyInf> responseModel = new ResponseModel<PAAddressFamilyInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAAddressFamilyInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAddressFamilyInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PADocumentInf> GetDHRPADocumentInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PADocumentInf> responseModel = new ResponseModel<PADocumentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPADocumentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PADocumentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAClothingSize> GetDHRPAClothingSizeByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAClothingSize> responseModel = new ResponseModel<PAClothingSize>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAClothingSize;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAClothingSize>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }


        public ResponseModel<PADocumentInf> GetDHRPADocumentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PADocumentInf> responseModel = new ResponseModel<PADocumentInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPADocumentInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PADocumentInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAcademicInf> GetDHRPAAcademicInf(string startDate, string endDate, string ApiKey,string SubtypeCode=null,string SubtypeValue=null,string DOI=null)
        {
            ResponseModel<PAAcademicInf> responseModel = new ResponseModel<PAAcademicInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
        
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAResearch;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ostrParams = string.Empty;
                 
                    if (!string.IsNullOrEmpty(SubtypeCode))
                    {
                        ostrParams += "subtypeCode=" + SubtypeCode + "&";
                    }
                    if (!string.IsNullOrEmpty(SubtypeValue))
                    {
                        ostrParams += "subtypeValue=" + SubtypeValue + "&";
                    }
                    if (!string.IsNullOrEmpty(DOI))
                    {
                        ostrParams += "doi=" + DOI + "&";
                    }
                    if (!string.IsNullOrEmpty(ostrParams))
                    {
                        strParams = strParams + "?" + ostrParams;
                    }
                    //?subtypeCode=2&subtypeValue=3&doi=4
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAcademicInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAcademicInf> GetDHRPAAcademicInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAAcademicInf> responseModel = new ResponseModel<PAAcademicInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAResearch;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAcademicInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAcademicMemberInf> GetDHRPAAcademicMemberInf(string startDate, string endDate, string ApiKey, string academicID = null)
        {
            ResponseModel<PAAcademicMemberInf> responseModel = new ResponseModel<PAAcademicMemberInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAResearchMember;
                    //?paReId=1
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    if (!string.IsNullOrEmpty(academicID))
                    {
                        strParams += "?paReId=" + academicID;
                    }
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAcademicMemberInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAAcademicMemberInf> GetDHRPAAcademicMemberInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAAcademicMemberInf> responseModel = new ResponseModel<PAAcademicMemberInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAResearchMember;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAAcademicMemberInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }


        #region INFOTYPE0001
        public ResponseModel<PAInfoPersonal> GetDHRPAInfoPersonal(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<PAInfoPersonal> responseModel = new ResponseModel<PAInfoPersonal>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAInfoPersonal;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAInfoPersonal>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<PAInfoPersonal> GetDHRPAInfoPersonalByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<PAInfoPersonal> responseModel = new ResponseModel<PAInfoPersonal>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsSentPAInfoPersonal;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PAInfoPersonal>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        #endregion INFOTYPE0001

        #endregion GET

        #region POST
        public PostResult PostDHRPAPersonalInf(PAPersonalInfPost personalInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAPersonalInf;
                    string dataPost = JsonConvert.SerializeObject(personalInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, personalInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAPrivateInf(PAPrivateInfPost privateInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAPrivateInf;
                    string dataPost = JsonConvert.SerializeObject(privateInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, privateInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAFamilyInf(PAFamilyInfPost familyInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAFamilyInf;
                    string dataPost = JsonConvert.SerializeObject(familyInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, familyInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPARelateEmpInf(PARelateEmpInfPost relateEmpInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePARelateEmpInf;
                    string dataPost = JsonConvert.SerializeObject(relateEmpInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, relateEmpInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAAddressInf(PAAddressInfPost addressInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAAddressInf;
                    string dataPost = JsonConvert.SerializeObject(addressInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, addressInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAContactInf(PAContactInfPost contactInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAContactInf;
                    string dataPost = JsonConvert.SerializeObject(contactInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, contactInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAEducationInf(PAEducationInfPost educationInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAEducationInf;
                    string dataPost = JsonConvert.SerializeObject(educationInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, educationInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }


        public PostResult PostDHRPAWorkHisotryInf(PAWorkHistoryInfPost workhistoryInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");


                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAWorkinghisInf;
                    string dataPost = JsonConvert.SerializeObject(workhistoryInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, workhistoryInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }


        public PostResult PostDHRPATrainningHisotryInf(PATrainingHis TrainninghistoryInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePATrainninghisInf;
                    string dataPost = JsonConvert.SerializeObject(TrainninghistoryInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, TrainninghistoryInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }







        public PostResult PostDHRPACertificateInf(PACertificateInfPost certificateInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePACertificateInf;
                    string dataPost = JsonConvert.SerializeObject(certificateInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, certificateInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPALanguageTestInf(PALanguageTestInf languageTestInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePALanguageTestInf;
                    string dataPost = JsonConvert.SerializeObject(languageTestInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, languageTestInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPADiseaseInf(PADiseaseInfPost diseaseInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePADiseaseInf;
                    string dataPost = JsonConvert.SerializeObject(diseaseInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, diseaseInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPATalentInf(PATalentInfPost talentInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePATalentInf;
                    string dataPost = JsonConvert.SerializeObject(talentInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, talentInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAAddressFamilyInf(PAAddressFamilyInfPost addressFamilyInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAAddressFamilyInf;
                    string dataPost = JsonConvert.SerializeObject(addressFamilyInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, addressFamilyInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPADocumentInf(PADocumentInfPost documentInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePADocumentInf;
                    string dataPost = JsonConvert.SerializeObject(documentInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, documentInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAAcademicInf(PAAcademicInfPost AcademicInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAResearch;
                    string dataPost = JsonConvert.SerializeObject(AcademicInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, AcademicInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRPAAcademicMemberInf(PAAcademicMemberInfPost AcademicMemberInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;


            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");

                    string oRoute = PersonalRoute.Route(CompanyCode).PersonalRoute_WsReceivePAResearchMember;
                    string dataPost = JsonConvert.SerializeObject(AcademicMemberInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, AcademicMemberInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }
        #endregion POST        
    }
}
