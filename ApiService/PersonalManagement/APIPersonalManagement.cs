﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DHR.HR.API
{
    public class APIPersonalManagement
    {
        #region Constructor
        public APIPersonalManagement()
        {

        }
        #endregion Constructor
        #region Multicompany Framework
        // private Dictionary<string, APIPersonalManagement> cache = new Dictionary<string, APIPersonalManagement>();

        private string ModuleID = "DHR.HR";

        private string CompanyCode { get; set; }

        private string _ApiKey = string.Empty;

        private string _empCode = string.Empty;

        public static APIPersonalManagement CreateInstance(string oCompanyCode)
        {
            APIPersonalManagement oAPIPersonalManagement = new APIPersonalManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIPersonalManagement;
        }

        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");
            }
        }

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }

        public string ApiKey
        {
            get
            {
                string ApiKeyRequest = string.Format("ApiKeyRequest_{0}_{1}", CompanyCode, _empCode);
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                    {
                        _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                    }
                    else

                    {
                        _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                        HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                    }
                }
                else
                {
                    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                }
                //if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                //{
                //    _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                //}
                //else

                //{
                //    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                //    HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                //}

                return _ApiKey;

            }
            set
            {
                _ApiKey = value;
            }
        }

        public int Period_Time_Expired
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PERIODTIMEEXPIRED"));
            }
        }

        //private string ApiKey
        //{
        //    get
        //    {
        //        return APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
        //    }
        //}

        #endregion Multicompany Framework

        #region GET ALL DATA
        //เลิกใช้แล้ว Pariyaporn 20200327
        //public ResponseModel<PAWorkCalendar> GetDHRPAWorkCalendar(string startDate, string endDate)
        //{
        //    return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAWorkCalendar(startDate, endDate, ApiKey);
        //}

        public ResponseModel<PAPersonalInf> GetDHRPAPersonalInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPersonalInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAPrivateInf> GetDHRPAPrivateInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPrivateInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAOrganizationInf> GetDHRPAOrganizationInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAOrganizationInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAFamilyInf> GetDHRPAFamilyInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAFamilyInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PARelateEmpInf> GetDHRPARelateEmpInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPARelateEmpInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAImportanceDateInf> GetDHRPAImportanceDateInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAImportanceDateInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAAddressInf> GetDHRPAAddressInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAddressInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAPosition> GetDHRPAPosition(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPosition(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAActingPosition> GetDHRPAActingPosition(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAActingPosition(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAPositionHis> GetDHRPAPositionHis(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPositionHis(startDate, endDate, ApiKey);
        }

        public ResponseModel<PASecondmentInf> GetDHRPASecondmentInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPASecondmentInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PASecondmentOutInf> GetDHRPASecondmentOutInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPASecondmentOutInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAContactInf> GetDHRPAContactInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAContactInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAEducationInf> GetDHRPAEducationInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAEducationInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAHiringConditionInf> GetDHRPAHiringConditionInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAHiringConditionInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAEvaluationInf> GetDHRPAEvaluationInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAEvaluationInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAWorkingHisInf> GetDHRPAWorkingHisInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAWorkingHisInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PACertificateInf> GetDHRPACertificateInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPACertificateInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PALanguageTestInf> GetDHRPALanguageTestInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPALanguageTestInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PADiseaseInf> GetDHRPADiseaseInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPADiseaseInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PATalentInf> GetDHRPATalentInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPATalentInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAPunishmentInf> GetDHRPAPunishmentInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPunishmentInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAPropationUpdateInf> GetDHRPAPropationUpdateInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPropationUpdateInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PACommandInf> GetDHRPACommandInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPACommandInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PATrainingHis> GetDHRPATrainingHis(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPATrainingHis(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAMedicalCheckup> GetDHRPAMedicalCheckup(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAMedicalCheckup(startDate, endDate, ApiKey);
        }

        ResponseModel<PAAddressFamilyInf> GetDHRPAAddressFamilyInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAddressFamilyInf(startDate, endDate, ApiKey);
        }

        ResponseModel<PADocumentInf> GetDHRPADocumentInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPADocumentInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAInfoPersonal> GetDHRPAInfoPersonal(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAInfoPersonal(startDate, endDate, ApiKey);
        }
        public ResponseModel<PAAcademicInf> GetDHRPAAcademicInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAcademicInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAAcademicInf> GetDHRPAAcademicInf(string startDate, string endDate,  string SubtypeCode = null, string SubtypeValue = null, string DOI = null)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAcademicInf(startDate, endDate, ApiKey,  SubtypeCode, SubtypeValue, DOI);
        }
        public ResponseModel<PAAcademicMemberInf> GetDHRPAAcademicMemberInf(string startDate, string endDate)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAcademicMemberInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PAAcademicMemberInf> GetDHRPAAcademicMemberInfByAcademicID(string startDate, string endDate, string academicID)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAcademicMemberInf(startDate, endDate, ApiKey, academicID);
        }
        #endregion GET ALL DATA

        #region GET BY EMPCODE

        public ResponseModel<PAPersonalInf> GetDHRPAPersonalInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPersonalInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAPrivateInf> GetDHRPAPrivateInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPrivateInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAOrganizationInf> GetDHRPAOrganizationInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAOrganizationInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAFamilyInf> GetDHRPAFamilyInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAFamilyInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PARelateEmpInf> GetDHRPARelateEmpInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPARelateEmpInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAImportanceDateInf> GetDHRPAImportanceDateInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAImportanceDateInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAAddressInf> GetDHRPAAddressInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAddressInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAPosition> GetDHRPAPositionByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPositionByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAActingPosition> GetDHRPAActingPositionByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAActingPositionByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAPositionHis> GetDHRPAPositionHisByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPositionHisByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PASecondmentInf> GetDHRPASecondmentInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPASecondmentInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PASecondmentOutInf> GetDHRPASecondmentOutInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPASecondmentOutInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAContactInf> GetDHRPAContactInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAContactInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAEducationInf> GetDHRPAEducationInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAEducationInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }
       
        public ResponseModel<PAEmployeeInf> GetDHRPAEmployeeInfByEmpCode(string startDate, string endDate,string CompanyCode, string EmpCode, string UnitSelected, string LevelSelected, string PositionSelected)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAEmployeeInfByEmpCode(startDate, endDate, ApiKey, CompanyCode, EmpCode, UnitSelected, LevelSelected,  PositionSelected);
        }



        public ResponseModel<PAHiringConditionInf> GetDHRPAHiringConditionInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAHiringConditionInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAEvaluationInf> GetDHRPAEvaluationInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAEvaluationInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAWorkingHisInf> GetDHRPAWorkingHisInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAWorkingHisInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PACertificateInf> GetDHRPACertificateInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPACertificateInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PALanguageTestInf> GetDHRPALanguageTestInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPALanguageTestInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PADiseaseInf> GetDHRPADiseaseInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPADiseaseInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PATalentInf> GetDHRPATalentInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPATalentInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAPunishmentInf> GetDHRPAPunishmentInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPunishmentInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAPropationUpdateInf> GetDHRPAPropationUpdateInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAPropationUpdateInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PACommandInf> GetDHRPACommandInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPACommandInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PATrainingHis> GetDHRPATrainingHisByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPATrainingHisByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAAcademicMemberInf> GetDHRPAAcedemicworkmemberByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAcedemicworkmemberByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        

        public ResponseModel<PAMedicalCheckup> GetDHRPAMedicalCheckupByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAMedicalCheckupByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAAddressFamilyInf> GetDHRPAAddressFamilyInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAddressFamilyInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PADocumentInf> GetDHRPADocumentInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPADocumentInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAClothingSize> GetDHRPAClothingSizeByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAClothingSizeByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }



        public ResponseModel<PAAcademicInf> GetDHRPAAcademicInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAcademicInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAAcademicMemberInf> GetDHRPAAcademicMemberInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAAcademicMemberInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PAInfoPersonal> GetDHRPAInfoPersonalByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPAInfoPersonalByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }


        #endregion GET BY EMPCODE

        #region POST DATA
        public PostResult PostDHRPAPersonalInf(PAPersonalInfPost personalInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAPersonalInf(personalInf, ApiKey);
        }

        public PostResult PostDHRPAPrivateInf(PAPrivateInfPost privateInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAPrivateInf(privateInf, ApiKey);
        }

        public PostResult PostDHRPAFamilyInf(PAFamilyInfPost familyInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAFamilyInf(familyInf, ApiKey);
        }

        public PostResult PostDHRPARelateEmpInf(PARelateEmpInfPost relateEmpInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPARelateEmpInf(relateEmpInf, ApiKey);
        }

        public PostResult PostDHRPAAddressInf(PAAddressInfPost addressInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAAddressInf(addressInf, ApiKey);
        }

        public PostResult PostDHRPAContactInf(PAContactInfPost contactInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAContactInf(contactInf, ApiKey);
        }

        public PostResult PostDHRPAEducationInf(PAEducationInfPost educationInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAEducationInf(educationInf, ApiKey);
        }

        public PostResult PostDHRPAWorkHisotryInf(PAWorkHistoryInfPost workhistoryInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAWorkHisotryInf(workhistoryInf, ApiKey);
        }

        public PostResult PostDHRPATrainningHisotryInf(PATrainingHis trainninghistoryInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPATrainningHisotryInf(trainninghistoryInf, ApiKey);
        }

        public PostResult PostDHRPACertificateInf(PACertificateInfPost certificateInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPACertificateInf(certificateInf, ApiKey);
        }

        public PostResult PostDHRPALanguageTestInf(PALanguageTestInf languageTestInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPALanguageTestInf(languageTestInf, ApiKey);
        }

        public PostResult PostDHRPADiseaseInf(PADiseaseInfPost diseaseInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPADiseaseInf(diseaseInf, ApiKey);
        }

        public PostResult PostDHRPATalentInf(PATalentInfPost talentInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPATalentInf(talentInf, ApiKey);
        }


        public PostResult PostDHRPAAddressFamilyInf(PAAddressFamilyInfPost addressFamilyInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAAddressFamilyInf(addressFamilyInf, ApiKey);
        }

        public PostResult PostDHRPADocumentInf(PADocumentInfPost documentInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPADocumentInf(documentInf, ApiKey);
        }

        public PostResult PostDHRPAAcademicInf(PAAcademicInfPost academicInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAAcademicInf(academicInf, ApiKey);
        }

        public PostResult PostDHRPAAcademicMemberInf(PAAcademicMemberInfPost academicMemberInf)
        {
            return PersonalServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAAcademicMemberInf(academicMemberInf, ApiKey);
        }
        #endregion
    }
}
