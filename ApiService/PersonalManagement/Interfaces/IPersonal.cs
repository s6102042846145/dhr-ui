﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Interfaces
{
    public interface IPersonal
    {
        #region GET ALL
        ResponseModel<PAPersonalInf> GetDHRPAPersonalInf(string startDate, string endDate, string ApiKey); //For INFOTYPE0001 oLD VERSION
        ResponseModel<PAPrivateInf> GetDHRPAPrivateInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAOrganizationInf> GetDHRPAOrganizationInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAFamilyInf> GetDHRPAFamilyInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PARelateEmpInf> GetDHRPARelateEmpInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAImportanceDateInf> GetDHRPAImportanceDateInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAAddressInf> GetDHRPAAddressInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAPosition> GetDHRPAPosition(string startDate, string endDate, string ApiKey);
        ResponseModel<PAActingPosition> GetDHRPAActingPosition(string startDate, string endDate, string ApiKey);
        ResponseModel<PAPositionHis> GetDHRPAPositionHis(string startDate, string endDate, string ApiKey);
        ResponseModel<PASecondmentInf> GetDHRPASecondmentInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PASecondmentOutInf> GetDHRPASecondmentOutInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAContactInf> GetDHRPAContactInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAEducationInf> GetDHRPAEducationInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAHiringConditionInf> GetDHRPAHiringConditionInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAEvaluationInf> GetDHRPAEvaluationInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAWorkingHisInf> GetDHRPAWorkingHisInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PACertificateInf> GetDHRPACertificateInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PALanguageTestInf> GetDHRPALanguageTestInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PADiseaseInf> GetDHRPADiseaseInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PATalentInf> GetDHRPATalentInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAPunishmentInf> GetDHRPAPunishmentInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAPropationUpdateInf> GetDHRPAPropationUpdateInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PACommandInf> GetDHRPACommandInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PATrainingHis> GetDHRPATrainingHis(string startDate, string endDate, string ApiKey);
        ResponseModel<PAMedicalCheckup> GetDHRPAMedicalCheckup(string startDate, string endDate, string ApiKey);
        ResponseModel<PAAddressFamilyInf> GetDHRPAAddressFamilyInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PADocumentInf> GetDHRPADocumentInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PAAcademicInf> GetDHRPAAcademicInf(string startDate, string endDate, string ApiKey, string SubtypeCode = null, string SubtypeValue = null, string DOI = null);
        ResponseModel<PAAcademicInf> GetDHRPAAcademicInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAAcademicMemberInf> GetDHRPAAcademicMemberInf(string startDate, string endDate, string ApiKey, string academicID = null);

        ResponseModel<PAAcademicMemberInf> GetDHRPAAcademicMemberInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAInfoPersonal> GetDHRPAInfoPersonal(string startDate, string endDate, string ApiKey);  //For INFOTYPE0001 NEW
        #endregion GET ALL

        #region GET BY EMPCODE
        ResponseModel<PAPersonalInf> GetDHRPAPersonalInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode); //For INFOTYPE0001 oLD VERSION
        ResponseModel<PAPrivateInf> GetDHRPAPrivateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAOrganizationInf> GetDHRPAOrganizationInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAFamilyInf> GetDHRPAFamilyInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PARelateEmpInf> GetDHRPARelateEmpInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAImportanceDateInf> GetDHRPAImportanceDateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAAddressInf> GetDHRPAAddressInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAPosition> GetDHRPAPositionByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAActingPosition> GetDHRPAActingPositionByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAPositionHis> GetDHRPAPositionHisByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PASecondmentInf> GetDHRPASecondmentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PASecondmentOutInf> GetDHRPASecondmentOutInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAContactInf> GetDHRPAContactInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAEducationInf> GetDHRPAEducationInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);

        ResponseModel<PAEmployeeInf> GetDHRPAEmployeeInfByEmpCode(string startDate, string endDate, string ApiKey, string CompanyCode, string EmpCode, string UnitSelected, string LevelSelected, string PositionSelected);

        ResponseModel<PAHiringConditionInf> GetDHRPAHiringConditionInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAEvaluationInf> GetDHRPAEvaluationInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAWorkingHisInf> GetDHRPAWorkingHisInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PACertificateInf> GetDHRPACertificateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PALanguageTestInf> GetDHRPALanguageTestInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PADiseaseInf> GetDHRPADiseaseInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PATalentInf> GetDHRPATalentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAPunishmentInf> GetDHRPAPunishmentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAPropationUpdateInf> GetDHRPAPropationUpdateInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PACommandInf> GetDHRPACommandInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PATrainingHis> GetDHRPATrainingHisByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAAcademicMemberInf> GetDHRPAAcedemicworkmemberByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);

        

        ResponseModel<PAMedicalCheckup> GetDHRPAMedicalCheckupByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAAddressFamilyInf> GetDHRPAAddressFamilyInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PADocumentInf> GetDHRPADocumentInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PAClothingSize> GetDHRPAClothingSizeByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        
        ResponseModel<PAInfoPersonal> GetDHRPAInfoPersonalByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);  //For INFOTYPE0001 NEW
        #endregion GET BY EMPCODE

        #region POST DATA

        PostResult PostDHRPAPersonalInf(PAPersonalInfPost personalInf, string ApiKey);
        PostResult PostDHRPAPrivateInf(PAPrivateInfPost privateInf, string ApiKey);
        PostResult PostDHRPAFamilyInf(PAFamilyInfPost familyInf, string ApiKey);
        PostResult PostDHRPARelateEmpInf(PARelateEmpInfPost relateEmpInf, string ApiKey);
        PostResult PostDHRPAAddressInf(PAAddressInfPost addressInf, string ApiKey);
        PostResult PostDHRPAContactInf(PAContactInfPost contactInf, string ApiKey);
        PostResult PostDHRPAEducationInf(PAEducationInfPost educationInf, string ApiKey);
        PostResult PostDHRPAWorkHisotryInf(PAWorkHistoryInfPost workhistoryInf, string ApiKey);
        PostResult PostDHRPATrainningHisotryInf(PATrainingHis trainninghistoryInf, string ApiKey);
        PostResult PostDHRPACertificateInf(PACertificateInfPost certificateInf, string ApiKey);
        PostResult PostDHRPALanguageTestInf(PALanguageTestInf languageTestInf, string ApiKey);
        PostResult PostDHRPADiseaseInf(PADiseaseInfPost diseaseInf, string ApiKey);
        PostResult PostDHRPATalentInf(PATalentInfPost talentInf, string ApiKey);
        PostResult PostDHRPAAddressFamilyInf(PAAddressFamilyInfPost addressFamilyInf, string ApiKey);
        PostResult PostDHRPADocumentInf(PADocumentInfPost documentInf, string ApiKey);
        PostResult PostDHRPAAcademicInf(PAAcademicInfPost academicInf, string ApiKey);
        PostResult PostDHRPAAcademicMemberInf(PAAcademicMemberInfPost academicMembertInf, string ApiKey);
        #endregion POST DATA
    }
}
