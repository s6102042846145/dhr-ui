﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAEducationInf : PAEducationInfPost
    {
        public string paEduID { get; set; }
        public string eduLevelTextTH { get; set; }
        public string eduLevelTextEN { get; set; }
        public string instituteTextTH { get; set; }
        public string instituteTextEN { get; set; }
        public string countryTextTH { get; set; }
        public string countryTextEN { get; set; }
        public string certificateTextTH { get; set; }
        public string certificateTextEN { get; set; }
        public string majorTextTH { get; set; }
        public string majorTextEN { get; set; }
        public string minorTextTH { get; set; }
        public string minorTextEN { get; set; }
        public string specialTextTH { get; set; }
        public string specialTextEN { get; set; }
        public string eduGroupTextTH { get; set; }
        public string eduGroupTextEN { get; set; }
    }
}
