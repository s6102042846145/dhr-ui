﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PARelateEmpInf : PARelateEmpInfPost
    {
        public string relateTextTH { get; set; }
        public string relateTextEN { get; set; }
        public string inameTextTH { get; set; }
        public string inameTextEN { get; set; }
        public string sexTextTH { get; set; }
        public string sexTextEN { get; set; }
        public string addRelateTextTH { get; set; }
        public string addRelateTextEN { get; set; }
        
       
    }
}
   