﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PALanguageTestInf
    {
        public string paLaId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string testCode { get; set; }
        public string testValue { get; set; }
        public string testStartDate { get; set; }
        public string testEndDate { get; set; }
        public int testTime { get; set; }
        public int score { get; set; }
        public string actionType { get; set; }
    }
}
 
