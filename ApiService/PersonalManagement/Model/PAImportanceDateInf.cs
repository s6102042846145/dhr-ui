﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAImportanceDateInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string startWorkDate { get; set; }
        public string employedDate { get; set; }
        public string countLeaveAgeDate { get; set; }
        public string registFundDate { get; set; }
        public string countFundAgeDate { get; set; }
        public string startWorkGroupDate { get; set; }
        public string leavePfFundDate1 { get; set; }
        public string leavePfFundDate2 { get; set; }
        public string leavePfFundDate3 { get; set; }
        public string retireDate { get; set; }
        public string countLevelAgeDate { get; set; }
        public string countLevelAgeGroupDate { get; set; }
        public string countPosAgeDate { get; set; }
        public string countUnitAgeDate { get; set; }
        public string countRootJobAgeDate { get; set; }
        public string actionType { get; set; }
        public string id { get; set; }

         
    }
}
