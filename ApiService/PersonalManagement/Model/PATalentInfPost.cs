﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PATalentInfPost
    {
        public string paTaId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string talentCode { get; set; }
        public string talentValue { get; set; }
        public string detail { get; set; }
        public string actionType { get; set; }

        public PATalentInfPost()
        {
            paTaId = string.Empty;
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            talentCode = string.Empty;
            talentValue = string.Empty;
            detail = string.Empty;
            actionType = string.Empty;
        }
    }
}
 