﻿namespace DHR.HR.API.Model
{
    public class PAAddressFamilyInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int familyId { get; set; }
        public string relateCode { get; set; }
        public string relateValue { get; set; }
        public string addressTypeCode { get; set; }
        public string addressTypeValue { get; set; }
        public string addressNo { get; set; }
        public int? moo { get; set; }
        public string building { get; set; }
        public string buildingEn { get; set; }
        public string soi { get; set; }
        public string street { get; set; }
        public string subdistrictCode { get; set; }
        public string districtCode { get; set; }
        public string provinceCode { get; set; }
        public string postcode { get; set; }
        public string countryCode { get; set; }
        public string countryValue { get; set; }
        public string actionType { get; set; }

        public PAAddressFamilyInfPost()
        {
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            familyId = default(int);
            relateCode = string.Empty;
            relateValue = string.Empty;
            addressTypeCode = string.Empty;
            addressTypeValue = string.Empty;
            addressNo = string.Empty;
            moo = null;
            building = string.Empty;
            buildingEn = string.Empty;
            soi = string.Empty;
            street = string.Empty;
            subdistrictCode = string.Empty;
            districtCode = string.Empty;
            provinceCode = string.Empty;
            postcode = string.Empty;
            countryCode = string.Empty;
            countryValue = string.Empty;
            actionType = string.Empty;
        }
    }
}
