﻿namespace DHR.HR.API.Model
{
    public class PAPersonalInf : PAPersonalInfPost
    {
        public string inameTextTH { get; set; }
        public string inameTextEN { get; set; }
        public string iname1TextTH { get; set; }
        public string iname1TextEN { get; set; }
        public string iname2TextTH { get; set; }
        public string iname2TextEN { get; set; }
        public string iname3TextTH { get; set; }
        public string iname3TextEN { get; set; }
        public string iname4TextTH { get; set; }
        public string iname4TextEN { get; set; }
        public string iname5TextTH { get; set; }
        public string iname5TextEN { get; set; }
        public string inameEnText { get; set; }
        public string fullNameEnText { get; set; }
        public string countryBirthTextTH { get; set; }
        public string countryBirthTextEN { get; set; }
        public string provinceBirthTextTH { get; set; }
        public string provinceBirthTextEN { get; set; }
        public string raceTextTH { get; set; }
        public string raceTextEN { get; set; }
        public string nationalityTextTH { get; set; }
        public string nationalityTextEN { get; set; }
        public string religionTextTH { get; set; }
        public string religionTextEN { get; set; }
        public string maritalStatusTextTH { get; set; }
        public string maritalStatusTextEN { get; set; }
        public string sexTextTH { get; set; }
        public string sexTextEN { get; set; }

        public  PAPersonalInf()
        {
             inameTextTH= string.Empty;
             inameTextEN= string.Empty;
             iname1TextTH= string.Empty;
             iname1TextEN= string.Empty;
             iname2TextTH= string.Empty;
             iname2TextEN= string.Empty;
             iname3TextTH= string.Empty;
             iname3TextEN= string.Empty;
             iname4TextTH= string.Empty;
             iname4TextEN= string.Empty;
             iname5TextTH= string.Empty;
             iname5TextEN= string.Empty;
             inameEnText= string.Empty;
             fullNameEnText= string.Empty;
             countryBirthTextTH= string.Empty;
             countryBirthTextEN= string.Empty;
             provinceBirthTextTH= string.Empty;
             provinceBirthTextEN= string.Empty;
             raceTextTH= string.Empty;
             raceTextEN= string.Empty;
             nationalityTextTH= string.Empty;
             nationalityTextEN= string.Empty;
             religionTextTH= string.Empty;
             religionTextEN= string.Empty;
             maritalStatusTextTH= string.Empty;
             maritalStatusTextEN= string.Empty;
             sexTextTH= string.Empty;
             sexTextEN= string.Empty;

        }
    }

}
