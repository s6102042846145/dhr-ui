﻿namespace DHR.HR.API.Model
{
    public class PAWorkHistoryInfPost
    {
        public string paWoId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string employerName { get; set; }
        public string position { get; set; }
        public string workCountryCode { get; set; }
        public string workCountryValue { get; set; }
        public string buCode { get; set; }
        public string buValue { get; set; }
        public string jobGrpCode { get; set; }
        public string jobGrpValue { get; set; }
        public object actionType { get; set; }
        public object serviceType { get; set; }


        public PAWorkHistoryInfPost()
        {
            paWoId = string.Empty;
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            employerName = string.Empty;
            position = string.Empty;
            workCountryCode = string.Empty;
            workCountryValue = string.Empty;
            buCode = string.Empty;
            buValue = string.Empty;
            jobGrpCode = string.Empty;
            jobGrpValue = string.Empty;
            actionType = string.Empty;
            serviceType = string.Empty;

        }

    }
}
