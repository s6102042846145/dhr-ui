﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PADocumentInf : PADocumentInfPost
    {
        public string documentTextTH { get; set; }
        public string documentTextEN { get; set; }
        public string countryOfIssueTextTH { get; set; }
        public string countryOfIssueTextEN { get; set; }
        public string validityTextTH { get; set; }
        public string validityTextEN { get; set; }

        public PADocumentInf()
        {
            documentTextTH = string.Empty;
            documentTextEN = string.Empty;
            countryOfIssueTextTH = string.Empty;
            countryOfIssueTextEN = string.Empty;
            validityTextTH = string.Empty;
            validityTextEN = string.Empty;
        }
    }
}