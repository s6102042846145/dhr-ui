﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAPrivateInf : PAPrivateInfPost
    {
        public string bloodGrpTextTH { get; set; }
        public string bloodGrpTextEN { get; set; }

        public PAPrivateInf()
        {
            bloodGrpTextTH = string.Empty;
            bloodGrpTextEN = string.Empty;
        }
    }
    
   
}