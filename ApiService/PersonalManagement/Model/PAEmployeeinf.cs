﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAEmployeeInf : PAEmployeeInfPost
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string empCode { get; set; }
        public string empNameTH { get; set; }
        public string empNameEN { get; set; }
        public string posCode { get; set; }
        public string positionTextTH { get; set; }
        public string positionTextEN { get; set; }
        public string unitCode { get; set; }
        public string unitTextTh { get; set; }
        public string unitTextEN { get; set; }
        public string unitLevelValue { get; set; }
        public string unitLevelTextTH { get; set; }
        public string unitLevelTextEN { get; set; }
        public string bandValue { get; set; }
        public string bandTextTH { get; set; }
        public string bandTextEN { get; set; }
    }
}
