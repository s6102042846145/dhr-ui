﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAHiringConditionInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string eduLevelCode { get; set; }
        public string eduLevelValue { get; set; }
        public string eduBkCode { get; set; }
        public string eduBkValue { get; set; }
        public int experience { get; set; }
        public string employedTypeCode { get; set; }
        public string employedTypeValue { get; set; }
        public string actionType { get; set; }
    }
}
