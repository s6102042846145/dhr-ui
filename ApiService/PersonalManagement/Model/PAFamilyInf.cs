﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAFamilyInf : PAFamilyInfPost
    {
        public string relateCodeTextTH { get; set; }
        public string relateCodeTextEN { get; set; }
        public string inameCodeTextTH { get; set; }
        public string inameCodeTextEN { get; set; }
        public string fullNameText { get; set; }
        public string birthDate { get; set; }
        public string sexCodeTextTH { get; set; }
        public string sexCodeTextEN { get; set; }
        public string countryBirthCodeTextTH { get; set; }
        public string countryBirthCodeTextEN { get; set; }
        public string nationalityCodeTextTH { get; set; }
        public string nationalityCodeTextEN { get; set; }
        
       
       
     
    }
}
