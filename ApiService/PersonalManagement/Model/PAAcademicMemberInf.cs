﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
  public  class PAAcademicMemberInf
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string paReMemId { get; set; }
        public string paReId { get; set; }
        public string title { get; set; }
        public string subtypeValue { get; set; }
        public string subtypeTextTH { get; set; }
        public string subtypeTextEN { get; set; }
        public string empCode { get; set; }
        public string roleCode { get; set; }
        public string roleValue { get; set; }
        public string roleNameTH { get; set; }
        public string roleNameEN { get; set; }

    
    }
}
