﻿namespace DHR.HR.API.Model
{
    public class PAWorkingHisInfPost
    {
        public string paWoId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string employerName { get; set; }
        public string position { get; set; }
        public string workCountryCode { get; set; }
        public string workCountryValue { get; set; }
        public string workCountryTextTH { get; set; }
        public string workCountryTextEN { get; set; }
        public string buCode { get; set; }
        public string buValue { get; set; }
        public string buTextTH { get; set; }
        public string buTextEN { get; set; }
        public string jobGrpCode { get; set; }
        public string jobGrpValue { get; set; }
        public string jobGrpTextTH { get; set; }
        public string jobGrpTextEN { get; set; }
        public object actionType { get; set; }
        public object serviceType { get; set; }
        public string createDate { get; set; }
        public string createBy { get; set; }
        public string updateDate { get; set; }
        public string updateBy { get; set; }

        public PAWorkingHisInfPost()
        {
            paWoId = string.Empty;
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            employerName = string.Empty;
            position = string.Empty;
            workCountryCode = string.Empty;
            workCountryValue = string.Empty;
            workCountryTextTH = string.Empty;
            workCountryTextEN = string.Empty;
            buCode = string.Empty;
            buValue = string.Empty;
            buTextTH = string.Empty;
            buTextEN = string.Empty;
            jobGrpCode = string.Empty;
            jobGrpValue = string.Empty;
            jobGrpTextTH = string.Empty;
            jobGrpTextEN = string.Empty;
            actionType = string.Empty;
            serviceType = string.Empty;
            createDate = string.Empty;
            createBy = string.Empty;
            updateDate = string.Empty;
            updateBy = string.Empty;
        }

    }
}
