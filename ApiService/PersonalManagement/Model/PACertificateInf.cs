﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PACertificateInf : PACertificateInfPost
    {
        public string certificateTextTH { get; set; }
        public string certificateTextEN { get; set; }
    }
}
