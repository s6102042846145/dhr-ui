﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PACommandInf
    {
        public string paCoId { get; set; }
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string commandCode { get; set; }
        public string cmdName { get; set; }
        public string cmdNameEn { get; set; }
        public string cmdTypeCode { get; set; }
        public string cmdDate { get; set; }
        public string cmdBy { get; set; }
        public string cmdTypeValue { get; set; }
        public string actionType { get; set; }
    }
}
     