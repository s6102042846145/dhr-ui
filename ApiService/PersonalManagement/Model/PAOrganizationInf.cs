﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAOrganizationInf
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }     
        public string empCode { get; set; }
        public string empStatusCode { get; set; }
        public string empStatusValue { get; set; }
        public string empStatusTextTH { get; set; }
        public string empStatusTextEN { get; set; }
        public string empTypeCode { get; set; }
        public string empTypeValue { get; set; }
        public string empTypeTextTH { get; set; }
        public string empTypeTextEN { get; set; }
        public string posCode { get; set; }
        public string posTextTH { get; set; }
        public string posTextEN { get; set; }
        public string spcPosName { get; set; }
        public string unitCode { get; set; }
        public string unitTextTH { get; set; }
        public string unitTextEN { get; set; }
        public string costCenterCode { get; set; }
        public string costCenterValue { get; set; }
        public string levelCode { get; set; }
        public string levelValue { get; set; }
        public string levelText { get; set; }
        public string actingLevelCode { get; set; }
        public string actingLevelValue { get; set; }
        public string actingLevelText { get; set; }
        public string bandCode { get; set; }
        public string bandValue { get; set; }
        public string bandTextTH { get; set; }
        public string bandTextEN { get; set; }
        public string actingBandCode { get; set; }
        public string actingBandValue { get; set; }
        public string actingBandTextTH { get; set; }
        public string actingBandTextEN { get; set; }
        public string countryWorkCode { get; set; }
        public string countryWorkValue { get; set; }
        public string countryWorkTextTH { get; set; }
        public string countryWorkTextEN { get; set; }
        public string workPlaceCode { get; set; }
        public string workPlaceValue { get; set; }
        public string workPlaceTextTH { get; set; }
        public string workPlaceTextEN { get; set; }
        public string workAreaCode { get; set; }
        public string workAreaValue { get; set; }
        public string workAreaTextTH { get; set; }
        public string workAreaTextEN { get; set; }
        public string workTimeCode { get; set; }
        public string workTimeValue { get; set; }
        public string workTimeTextTH { get; set; }
        public string workTimeTextEN { get; set; }
        public string reportToPosCode { get; set; }
        public string reportToPosTextTH { get; set; }
        public string reportToPosTextEN { get; set; }
        public string reportToBandCode { get; set; }
        public string reportToBandValue { get; set; }
        public string reportToBandTextTH { get; set; }
        public string reportToBandTextEN { get; set; }

        public string costCenterTextTH { get; set; }
        public string costCenterTextEN { get; set; }

        public string hrAdmin { get; set; }
       

        public string spcPosNameEn { get; set; }

        public string costCenterText { get; set; }
        public string levelTextTH { get; set; }
        public string levelTextEN { get; set; }

        public string actingLevelTextTH { get; set; }
        public string actingLevelTextEN { get; set; }

        public string hrAdminCode { get; set; }
        public string hrAdminValue { get; set; }
        public string hrAdminTextTH { get; set; }
        public string hrAdminTextEN { get; set; }

        public string actionType { get; set; }
        public string serviceType { get; set; }
        public string id { get; set; }


    }
}
