﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
   public class PAAcademicMemberInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string paReMemId { get; set; }
        public string paReId { get; set; }
        public string roleCode { get; set; }
        public string roleValue { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }
        
        
        public PAAcademicMemberInfPost()
        {
            companyCode = string.Empty;
            empCode = string.Empty;
            startDate = string.Empty;
            endDate = string.Empty;
            paReMemId = string.Empty;
            paReId = string.Empty;
            roleCode = string.Empty;
            roleValue = string.Empty;
            actionType = string.Empty;
            serviceType = string.Empty;
        }
    }
}


