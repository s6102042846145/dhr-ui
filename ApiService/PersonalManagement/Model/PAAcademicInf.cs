﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
  public  class PAAcademicInf :PAAcademicInfPost
    {
        public string categoryNameTH { get; set; }
        public string categoryNameEN { get; set; }
        public string subtypeNameTH { get; set; }
        public string subtypeNameEN { get; set; }
    }
}
