﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAActingPosition
    {
        public string paAcId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string actingPosCode { get; set; }
        public string actingPosTextTH { get; set; }
        public string actingPosTextEN { get; set; }
        public string actionType { get; set; }
        public string actingUnitTextTH { get; set; }
        public string actingUnitTextEN { get; set; }
        public string actingBandTextTH { get; set; }
        public string actingBandTextEN { get; set; }
        public string actingHeadOfUnit { get; set; }
        public string posTextTH { get; set; }
        public string posTextEN { get; set; }
        public string unitTextTH { get; set; }
        public string unitTextEN { get; set; }
        public string bandTextTH { get; set; }
        public string bandTextEN { get; set; }
        public string headOfUnit { get; set; }
    }
}
