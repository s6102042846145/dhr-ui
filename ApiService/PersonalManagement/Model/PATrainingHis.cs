﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PATrainingHis
    {
        public string paTrId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string courseName { get; set; }
        public string actionType { get; set; }
        public string serviceType { get; set; }
        
    }
}
