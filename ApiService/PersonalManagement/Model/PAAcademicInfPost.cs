﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAAcademicInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int paReId { get; set; }
        public string subtypeCode { get; set; }
        public string subtypeValue { get; set; }
        public string title { get; set; }
        public string yearOfPublic { get; set; }
        public string journal { get; set; }
        public string doi { get; set; }
        public string impactFactor { get; set; }
        public string submittedDate { get; set; }
        public string grantedDate { get; set; }
        public string categoryCode { get; set; }
        public string categoryValue { get; set; }
        public string remark { get; set; }
        public string attachmentId { get; set; }
        public string refAuthors { get; set; }
        public string actionType { get; set; }

    }
}

