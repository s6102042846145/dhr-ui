﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PAEvaluationInf
    {
        public string paEvId { get; set; }
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string period { get; set; }
        public string evaluationTime { get; set; }
        public string gradeCode { get; set; }
        public string gradeValue { get; set; }
        public string gradeTextTH { get; set; }
        public string gradeTextEN { get; set; }
        public decimal? upSalaryPercentage { get; set; }
        public decimal? variableBonus { get; set; }
        public decimal? score { get; set; }
        public decimal? average { get; set; }
        public decimal? increaseSalary { get; set; }
        public decimal? compenAmount { get; set; }
        public string actionType { get; set; }
    }
}
 