﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PARelateEmpInfPost
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string relateCode { get; set; }
        public string relateValue { get; set; }
        public string effectiveDate { get; set; }
        public string inameCode { get; set; }
        public string inameValue { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string fnameEn { get; set; }
        public string lnameEN { get; set; }
        public string sexCode { get; set; }
        public string sexValue { get; set; }
        public string addRelateCode { get; set; }
        public string addRelateValue { get; set; }
        public string phoneNumber { get; set; }
        public int benefitRatio { get; set; }
        public string relateEmpCode { get; set; }
        public string actionType { get; set; }
    }
}
