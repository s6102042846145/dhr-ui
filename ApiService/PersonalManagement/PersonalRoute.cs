﻿using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class PersonalRoute
    {
        public PersonalRoute()
        {

        }
        private static string ModuleID = "DHR.ROUTE.PA";
        private string CompanyCode { get; set; }

        public static PersonalRoute Route (string oCompanyCode)
        {
            PersonalRoute oPersonalRoute = new PersonalRoute()
            {
                CompanyCode = oCompanyCode
            };
            return oPersonalRoute;
        }
        //เลิกใช้แล้ว Pariyaporn 20200327
        //public string PersonalRoute_WsSentPAWorkCalendar
        //{
        //    get
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAWORKCALENDAR");
        //    }
        //}

        public string PersonalRoute_WsSentPAPersonalInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAPERSONALINF");
            }
        }

        public string PersonalRoute_WsSentPAPrivateInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAPRIVATEINF");
            }
        }

        public string PersonalRoute_WsSentPAOrganizationInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAORGANIZATIONINF");
            }
        }

        public string PersonalRoute_WsSentPAFamilyInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAFAMILYINF");
            }
        }

        public string PersonalRoute_WsSentPARelateEmpInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPARELATEEMPINF");
            }
        }

        public string PersonalRoute_WsSentPAImportanceDateInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAIMPORTANCEDATEINF");
            }
        }

        public string PersonalRoute_WsSentPAAddressInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAADDRESSINF");
            }
        }

        public string PersonalRoute_WsSentPAPosition
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAPOSITION");
            }
        }

        public string PersonalRoute_WsSentPAActingPosition
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAACTINGPOSITION");
            }
        }

        public string PersonalRoute_WsSentPAPositionHis
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAPOSITIONHIS");
            }
        }

        public string PersonalRoute_WsSentPASecondmentInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPASECONDMENTINF");
            }
        }

        public string PersonalRoute_WsSentPASecondmentOutInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPASECONDMENTOUTINF");
            }
        }

        public string PersonalRoute_WsSentPAContactInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPACONTACTINF");
            }
        }

        public string PersonalRoute_WsSentPAEducationInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAEDUCATIONINF");
            }
        }

        public string PersonalRoute_UiGetPAEmployoeeInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UIGETPAEMPLOYEE");
            }
        }

        public string PersonalRoute_WsSentPAHiringConditionInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAHIRINGCONDITIONINF");
            }
        }

        public string PersonalRoute_WsSentPAEvaluationInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAEVALUATIONINF");
            }
        }

        public string PersonalRoute_WsSentPAWorkingHisInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAWORKINGHISINF");
            }
        }

        public string PersonalRoute_WsSentPACertificateInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPACERTIFICATEINF");
            }
        }

        public string PersonalRoute_WsSentPALanguageTestInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPALANGUAGETESTINF");
            }
        }

        public string PersonalRoute_WsSentPADiseaseInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPADISEASEINF");
            }
        }

        public string PersonalRoute_WsSentPATalentInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPATALENTINF");
            }
        }

        public string PersonalRoute_WsSentPAPunishmentInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAPUNISHMENTINF");
            }
        }
        public string PersonalRoute_WsSentPAPropationUpdateInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAPROPATIONUPDATEINF");
            }
        }

        public string PersonalRoute_WsSentPACommandInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPACOMMANDINF");
            }
        }

        public string PersonalRoute_WsSentPATrainingHis
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPATRAININGHIS");
            }
        }

        public string PersonalRoute_UIGetPAResearchMember
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UIGETPARESEARCHMEMBER");
            }
        }

        

        public string PersonalRoute_WsSentPAMedicalCheckup
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAMEDICALCHECKUP");
            }
        }

        public string PersonalRoute_WsSentPAAddressFamilyInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAADDRESSFAMILYINF");
            }
        }

        public string PersonalRoute_WsSentPADocumentInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPADOCUMENTINF");
            }
        }

        public string PersonalRoute_WsSentPAInfoPersonal
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPAINFOPERSONAL");
            }
        }

        public string PersonalRoute_WsSentPAResearch
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPARESEARCH");
            }
        }

        public string PersonalRoute_WsSentPAResearchMember
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPARESEARCHMEMBER");
            }
        }
        public string PersonalRoute_WsSentPAClothingSize
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSSENTPACLOTHINGSIZE");
            }
        }

        






        #region POST DATA

        public string PersonalRoute_WsReceivePAPersonalInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPAPERSONALINF");
            }
        }

        public string PersonalRoute_WsReceivePAPrivateInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPAPRIVATEINF");
            }
        }

        public string PersonalRoute_WsReceivePAFamilyInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPAFAMILYINF");
            }
        }
        
        public string PersonalRoute_WsReceivePARelateEmpInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPARELATEEMPINF");
            }
        }
        
        public string PersonalRoute_WsReceivePAAddressInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPAADDRESSINF");
            }
        }
        
        public string PersonalRoute_WsReceivePAContactInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPACONTACTINF");
            }
        }
        
        public string PersonalRoute_WsReceivePAEducationInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPAEDUCATIONINF");
            }
        }


        public string PersonalRoute_WsReceivePAWorkinghisInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPAWORKINGHISINF");
            }
        }

        public string PersonalRoute_WsReceivePATrainninghisInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPATRAINNINGINF");
            }
        }

        public string PersonalRoute_WsReceivePACertificateInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPACERTIFICATEINF");
            }
        }
        
        public string PersonalRoute_WsReceivePALanguageTestInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPALANGUAGETESTINF");
            }
        }
       
        public string PersonalRoute_WsReceivePADiseaseInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPADISEASEINF");
            }
        }
        
        public string PersonalRoute_WsReceivePATalentInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPATALENTINF");
            }
        }

        public string PersonalRoute_WsReceivePAAddressFamilyInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPAADDRESSFAMILYINF");
            }
        }
        
        public string PersonalRoute_WsReceivePADocumentInf
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPADOCUMENTINF");
            }
        }

        public string PersonalRoute_WsReceivePAResearch
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPARESEARCH");
            }
        }

        public string PersonalRoute_WsReceivePAResearchMember
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "WSRECEIVEPARESEARCHMEMBER");
            }
        }
        #endregion
    }
}
