﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Interfaces
{
    public interface IPayroll
    {
        #region GET ALL

        ResponseModel<PYPayslipInf> GetDHRPYPayslipInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYWageInf> GetDHRPYWageInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYRegIncDed> GetDHRPYRegIncDed(string startDate, string endDate, string ApiKey);
        ResponseModel<PYNonRegIncDed> GetDHRPYNonRegIncDed(string startDate, string endDate, string ApiKey);
        ResponseModel<PYSpecialIncDed> GetDHRPYSpecialIncDed(string startDate, string endDate, string ApiKey);
        ResponseModel<PYSsoInf> GetDHRPYSsoInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYAccountInf> GetDHRPYAccountInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYPndInf> GetDHRPYPndInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYTaxDedInf> GetDHRPYTaxDedInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYPersonalArea> GetDHRPYPersonalArea(string startDate, string endDate, string ApiKey);
        ResponseModel<PYTaxInf> GetDHRPYTaxInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYWageType> GetDHRPYWageType(string startDate, string endDate, string ApiKey);
        ResponseModel<PYCalOt> GetDHRPYCalOt(string startDate, string endDate, string ApiKey);
        ResponseModel<PYCalProrate> GetDHRPYCalProrate(string startDate, string endDate, string ApiKey);
        ResponseModel<PYSsoRate> GetDHRPYSsoRate(string startDate, string endDate, string ApiKey);
        ResponseModel<PYCalTax> GetDHRPYCalTax(string startDate, string endDate, string ApiKey);
        ResponseModel<PYCalPf> GetDHRPYCalPf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYPfInf> GetDHRPYPfInf(string startDate, string endDate, string ApiKey);
        ResponseModel<PYPayslipWt> GetDHRPYPayslipWt(string startDate, string endDate, string ApiKey);
        ResponseModel<PYPayslipGroup> GetDHRPYPayslipGroup(string startDate, string endDate, string ApiKey);
        ResponseModel<PYPayslipHeader> GetDHRPYPayslipHeader(string startDate, string endDate, string ApiKey);
        ResponseModel<PYBankMaster> GetDHRPYBankMaster(string startDate, string endDate, string ApiKey);
        ResponseModel<PYPayStructure> GetDHRPYPayStructure(string startDate, string endDate, string ApiKey);

        #endregion GET ALL

        #region GET BY EMPCODE

        //ResponseModel<PYProvidentfundInf> GetDHRPYPaymentGetProvidentFund(string startDate, string endDate, string ApiKey, string EmpCode);
        //ResponseModel<PYTaxAllowanceInf> GetDHRPYPaymentTaxAllowanceByEmpcode(string startDate, string endDate, string ApiKey, string EmpCode);


        ResponseModel<PYPayslipInf> GetDHRPYPayslipInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYWageInf> GetDHRPYWageInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYRegIncDed> GetDHRPYRegIncDedByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYNonRegIncDed> GetDHRPYNonRegIncDedByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYSpecialIncDed> GetDHRPYSpecialIncDedByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYSsoInf> GetDHRPYSsoInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYAccountInf> GetDHRPYAccountInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYPndInf> GetDHRPYPndInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYTaxDedInf> GetDHRPYTaxDedInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYPfInf> GetDHRPYPfInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);
        ResponseModel<PYPayslipHeader> GetDHRPYPayslipHeaderByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode);

        #endregion GET BY EMPCODE


        #region POST DATA

        PostResult PostDHRPAPYTaxDedInf(PYTaxDedInf taxDedInf, string ApiKey);
        PostResult PostDHRPYNonregIncDed(PYNonRegIncDed oNonRegIncDed, string ApiKey);
        PostResult PostDHRPYAccountInf(PYAccountPost oAccountInf, string ApiKey);
        #endregion POST DATA
    }
}















