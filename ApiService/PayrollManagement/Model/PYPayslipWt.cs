﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYPayslipWt
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string wageTypeGroup { get; set; }
        public string wageTypeGroupOrder { get; set; }
        public string wageTypeCode { get; set; }
        public string wageTypeSign { get; set; }
    }
}
