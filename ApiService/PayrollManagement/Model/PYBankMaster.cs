﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYBankMaster
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string bankKey { get; set; }
        public string bankCode { get; set; }
        public string bankValue { get; set; }
        public string bankBranch { get; set; }
        public string street { get; set; }
        public string subdistrictCode { get; set; }
        public string districtCode { get; set; }
        public string provinceCode { get; set; }
        public string postcode { get; set; }
        public string currencyCode { get; set; }
        public string currencyValue { get; set; }
    }
}
