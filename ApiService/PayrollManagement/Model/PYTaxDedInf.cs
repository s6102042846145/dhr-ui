﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYTaxDedInf
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string disabled { get; set; }
        public string spouseNoIncome { get; set; }
        public int? numOfChild { get; set; }
        public double? adoptFather { get; set; }
        public double? adoptMother { get; set; }
        public double? adoptFatherSpouse { get; set; }
        public double? adoptMotherSpouse { get; set; }
        public double? adoptDisabled { get; set; }
        public double? insuranceFather { get; set; }
        public double? insuranceMother { get; set; }
        public double? insuranceFatherSpo { get; set; }
        public double? insuranceMotherSpo { get; set; }
        public double? lifeInsurance { get; set; }
        public double? lifeInsuranceSpo { get; set; }
        public double? healthInsurance { get; set; }
        public double? healthInsuranceSpo { get; set; }
        public double? nationalSavingFund { get; set; }
        public double? rmf { get; set; }
        public double? ssf { get; set; }
        public double? interest { get; set; }
        public double? paidForRealEstate { get; set; }
        public double? childbirthFee { get; set; }
        public double? politicalDonation { get; set; }
        public double? donation { get; set; }
        public double? educationDonation { get; set; }
        public double? pensionFund { get; set; }
        public double? lifeinsurancePension { get; set; }
        public int? numOfChildExtra { get; set; }

        public double? ssfx { get; set; }
        public double? reserved1 { get; set; }
        public double? reserved2 { get; set; }
        public double? reserved3 { get; set; }
        public double? reserved4 { get; set; }
        public double? reserved5{ get; set; }

        public int? reserved6 { get; set; }
        public int? reserved7 { get; set; }
        public int? reserved8 { get; set; }
 
    }
}

