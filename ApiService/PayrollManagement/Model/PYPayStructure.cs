﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYPayStructure
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string majorSalary { get; set; }
        public string minorSalary { get; set; }
        public string majorSalaryBase { get; set; }
        public string levelWage { get; set; }
        public double minSalary { get; set; }
        public double maxSalary { get; set; }
        public double midpoint { get; set; }
        public string currencyCode { get; set; }
        public string currencyValue { get; set; }
    }
}
