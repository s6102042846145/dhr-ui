﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PYCalProrate
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string prorateCode { get; set; }
        public string prorateValue { get; set; }
        public string dayBaseCal { get; set; }
    }
}
