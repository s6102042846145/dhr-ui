﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DHR.HR.API
{
    public class APIPayrollManagement
    {
        #region Constructor
        public APIPayrollManagement()
        {

        }
        #endregion Constructor
        #region Multicompany Framework
       // private Dictionary<string, APIPersonalManagement> cache = new Dictionary<string, APIPersonalManagement>();

        private string ModuleID = "DHR.HR";

        private string CompanyCode { get; set; }

        private string _ApiKey = string.Empty;

        private string _empCode = string.Empty;

        public static APIPayrollManagement CreateInstance(string oCompanyCode)
        {
            APIPayrollManagement oAPIPaymentManagement = new APIPayrollManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIPaymentManagement;
        }

        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");
            }
        }

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }

        private string ApiUser
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIUSER");
            }
        }

        private string ApiPassword
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APIPASSWORD");
            }
        }

        public string ApiKey
        {
            get
            {
                string ApiKeyRequest = string.Format("ApiKeyRequest_{0}_{1}", CompanyCode, _empCode);
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                    {
                        _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                    }
                    else

                    {
                        _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                        HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                    }
                }
                else
                {
                    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                }
                //if (HttpContext.Current.Cache.Get(ApiKeyRequest) != null)
                //{
                //    _ApiKey = HttpContext.Current.Cache.Get(ApiKeyRequest).ToString();
                //}
                //else

                //{
                //    _ApiKey = APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
                //    HttpContext.Current.Cache.Insert(ApiKeyRequest, _ApiKey, null, DateTime.Now.AddMinutes(Period_Time_Expired), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                //}

                return _ApiKey;

            }
            set
            {
                _ApiKey = value;
            }
        }

        public int Period_Time_Expired
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PERIODTIMEEXPIRED"));
            }
        }

        //private string ApiKey
        //{
        //    get
        //    {
        //        return APIAuthenManagement.CreateInstance(CompanyCode).RequestApiToken();
        //    }
        //}

        #endregion Multicompany Framework

        #region GET ALL DATA

        public ResponseModel<PYPayslipInf> GetDHRPYPayslipInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPayslipInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYWageInf> GetDHRPYWageInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYWageInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYRegIncDed> GetDHRPYRegIncDed(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYRegIncDed(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYNonRegIncDed> GetDHRPYNonRegIncDed(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYNonRegIncDed(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYSpecialIncDed> GetDHRPYSpecialIncDed(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYSpecialIncDed(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYSsoInf> GetDHRPYSsoInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYSsoInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYAccountInf> GetDHRPYAccountInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYAccountInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYPndInf> GetDHRPYPndInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPndInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYTaxDedInf> GetDHRPYTaxDedInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYTaxDedInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYPersonalArea> GetDHRPYPersonalArea(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPersonalArea(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYTaxInf> GetDHRPYTaxInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYTaxInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYWageType> GetDHRPYWageType(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYWageType(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYCalOt> GetDHRPYCalOt(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYCalOt(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYCalProrate> GetDHRPYCalProrate(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYCalProrate(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYSsoRate> GetDHRPYSsoRate(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYSsoRate(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYCalTax> GetDHRPYCalTax(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYCalTax(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYCalPf> GetDHRPYCalPf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYCalPf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYPfInf> GetDHRPYPfInf(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPfInf(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYPayslipWt> GetDHRPYPayslipWt(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPayslipWt(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYPayslipGroup> GetDHRPYPayslipGroup(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPayslipGroup(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYPayslipHeader> GetDHRPYPayslipHeader(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPayslipHeader(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYBankMaster> GetDHRPYBankMaster(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYBankMaster(startDate, endDate, ApiKey);
        }

        public ResponseModel<PYPayStructure> GetDHRPYPayStructure(string startDate, string endDate)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPayStructure(startDate, endDate, ApiKey);
        }

        #endregion GET ALL DATA

        #region TESTING
        
        //public ResponseModel<PYProvidentfundInf> GetDHRPYPaymentGetProvidentFund(string startDate, string endDate, string EmpCode)
        //{
        //    return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPaymentGetProvidentFund(startDate, endDate, ApiKey, EmpCode);
        //}

        //public ResponseModel<PYTaxAllowanceInf> GetDHRPYPaymentTaxAllowanceByEmpcode(string startDate, string endDate, string EmpCode)
        //{
        //    return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPaymentTaxAllowanceByEmpcode(startDate, endDate, ApiKey, EmpCode);
        //}

        //public ResponseModel<PYProvidentfundInf> GetDHRPYPaymentGetProvidentFund(string startDate, string endDate, string ApiKey, string EmpCode)
        //{
        //    throw new NotImplementedException();
        //}

        //public ResponseModel<PYTaxAllowanceInf> GetDHRPYPaymentTaxAllowanceByEmpcode(string startDate, string endDate, string ApiKey, string EmpCode)
        //{
        //    throw new NotImplementedException();
        //}
        #endregion TESTING

        #region GET BY EMPCODE
        public ResponseModel<PYPayslipInf> GetDHRPYPayslipInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPayslipInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYWageInf> GetDHRPYWageInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYWageInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYRegIncDed> GetDHRPYRegIncDedByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYRegIncDedByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYNonRegIncDed> GetDHRPYNonRegIncDedByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYNonRegIncDedByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYSpecialIncDed> GetDHRPYSpecialIncDedByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYSpecialIncDedByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYSsoInf> GetDHRPYSsoInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYSsoInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYAccountInf> GetDHRPYAccountInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYAccountInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYPndInf> GetDHRPYPndInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPndInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYTaxDedInf> GetDHRPYTaxDedInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYTaxDedInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYPfInf> GetDHRPYPfInfByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPfInfByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        public ResponseModel<PYPayslipHeader> GetDHRPYPayslipHeaderByEmpCode(string startDate, string endDate, string EmpCode)
        {
            _empCode = EmpCode;
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.GetDHRPYPayslipHeaderByEmpCode(startDate, endDate, ApiKey, EmpCode);
        }

        #endregion GET BY EMPCODE


        #region POST DATA

        public PostResult PostDHRPAPYTaxDedInf(PYTaxDedInf taxDedInf)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPAPYTaxDedInf(taxDedInf, ApiKey);
        }

        public PostResult PostDHRPYNonregIncDed(PYNonRegIncDed oNonRegIncDed)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPYNonregIncDed(oNonRegIncDed, ApiKey);
        }

        public PostResult PostDHRPYAccount(PYAccountPost oAccountInf)
        {
            return PayrollServiceManager.CreateInstance(CompanyCode, BaseUrl, Gateway).API.PostDHRPYAccountInf(oAccountInf, ApiKey);
        }

        #endregion
    }
}
