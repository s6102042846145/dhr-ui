﻿using DHR.HR.API.Interfaces;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class PayrollServiceManager
    {
        #region Constructor
        public PayrollServiceManager()
        {

        }
        #endregion Constructor
        #region Multicompany Framework

        private static string ModuleID = "DHR.HR";

        private string CompanyCode { get; set; }

        private string BaseUrl { get; set; }

        private string Gateway { get; set; }

        public static PayrollServiceManager CreateInstance(string oCompanyCode,string oBaseUrl ,string oGateway)
        {
            PayrollServiceManager oPaymentServiceManager = new PayrollServiceManager()
            {
                CompanyCode = oCompanyCode,
                BaseUrl = oBaseUrl,
                Gateway = oGateway
            };
            return oPaymentServiceManager;
        }
        #endregion Multicompany Framework

        #region Private Data
        private Type GetService (string Mode,string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"{ModuleID}.{Mode.ToUpper()}");
            string typeName = string.Format($"{ModuleID}.{Mode.ToUpper()}.{ClassName}");

            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);

            return oReturn;
        }
        #endregion Private Data

        private string MODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MODE");
            }
        }

        public IPayroll API
        {
            get
            {
                Type oType = GetService(MODE, "Payroll");
                if(oType == null)
                {
                    return null;
                }
                else
                {
                    return (IPayroll)Activator.CreateInstance(oType, CompanyCode, BaseUrl, Gateway);
                }
            }
        }
    }
}
