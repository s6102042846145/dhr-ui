﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Shared
{
    public class ResponseModel<T> where T: class
    {
        public string message { get; set; }
        public string referenceCode { get; set; }
        public bool isSuccess { get; set; }
        public int returnCode { get; set; }
        public string messageException { get; set; }
        public string requestId { get; set; }
        public List<errors> Errors { get; set; }
        public List<T> Data { get; set; }
    }

    public class errors
    {
        public string errorCode { get; set; }
        public string filedName { get; set; }
        public string value { get; set; }
    }
}
