﻿using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class APIServices
    {
        public APIServices()
        {

        }

        private static string ModuleID = "DHR.HR";
        private string CompanyCode { get; set; }

        public static APIServices SVC(string oCompanyCode)
        {
            APIServices oAPIServices = new APIServices()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIServices;
        }

        public int Period_Time_Expired
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PERIODTIMEEXPIRED"));
            }
        }

        public string MultipleFormatDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MULTIPLEFORMATDATE");
            }
        }
    }
}
