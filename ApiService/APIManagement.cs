﻿using DHR.HR.API.Model;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class APIManagement
    {
        #region Constructor
        public APIManagement()
        {

        }
        #endregion

        #region MultiCompany  Framework
        private static Dictionary<string, APIManagement> Cache = new Dictionary<string, APIManagement>();

        private static string ModuleID = "DHR.HR";

        public string CompanyCode { get; set; }

        public static APIManagement CreateInstance(string oCompanyCode)
        {
            APIManagement oAPIManagement = new APIManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAPIManagement;
        }

        #endregion MultiCompany  Framework

        private string Gateway
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GATEWAY");
            }
        }

        private string BaseUrl
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASEURL");
            }
        }

        //public string RequestToken(TokenRequest user)
        //{
        //    return AuthenServiceManager.CreateInstance(CompanyCode).API.RequestToken(user, BaseUrl, Gateway);
        //}
    }
}
