﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class PositionModel
    {
        public string id { get; set; }
        public string companyCode { get; set; }
        public string posCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string posShortname { get; set; }
        public string posName { get; set; }
        public string posShortnameEn { get; set; }
        public string posNameEn { get; set; }
        public string headOfUnitF { get; set; }
        public string bandCode { get; set; }
        public string bandValue { get; set; }
        public string bandValueTextTH { get; set; }
        public string bandValueTextEN { get; set; }
        public string bandTextTH { get; set; }
        public string bandTextEN { get; set; }
        public string unitCode { get; set; }
        public string unitTextTH { get; set; }
        public string unitTextEN { get; set; }
        public string empPositionTH { get; set; }
        public string empPositionEN { get; set; }
        public string positionTextTH { get; set; }
        public string positionTextEN { get; set; }
        public string createDate { get; set; }
        public string createBy { get; set; }
        public string updateDate { get; set; }
        public string updateBy { get; set; }
    }
}

