﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class ComSecondment
    {
        public string companyCode { get; set; }
        public string secondmentComCode { get; set; }
        public string secondmentShortname { get; set; }
        public string secondmentName { get; set; }
        public string secondmentShortnameEn { get; set; }
        public string secondmentNameEn { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
