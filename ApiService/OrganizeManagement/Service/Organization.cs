﻿using DHR.HR.API.Interfaces;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API
{
    public class Organization : IOrganization
    {
        #region Constructor
        public Organization(string oCompanyCode, string oBaseUrl, string oGateway)
        {
            CompanyCode = oCompanyCode;
            BaseUrl = oBaseUrl;
            Gateway = oGateway;
        }
        #endregion Constructor
        #region Member
        private string ModuleID = "DHR.HR";
        private string CompanyCode { get; set; }
        private string BaseUrl { get; set; }
        private string Gateway { get; set; }
        private string ContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONTENTTYPE");
            }

        }

        private string Scheme
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SCHEME");
            }
        }

        private string HeaderContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "HEADERCONTENTTYPE");
            }
        }

        private int MaxReconnectLimit
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAXRECONNECTIONATTEMPTLIMIT"));
            }
        }

        #endregion Member

        #region API Unused
        //public ResponseModel<BelongTo> GetDHRBelongTo(string startDate, string endDate,string ApiKey)
        //{
        //    ResponseModel<BelongTo> responseModel = new ResponseModel<BelongTo>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentBelongTo;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<BelongTo>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;
        //}

        //public ResponseModel<ComSecondment> GetDHRComSecondment(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<ComSecondment> responseModel = new ResponseModel<ComSecondment>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentComSecondment;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<ComSecondment>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }

        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;
        //}
        //public ResponseModel<Company> GetDHRCompany(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<Company> responseModel = new ResponseModel<Company>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentCompany;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<Company>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;  
        //}

        //public ResponseModel<FiscalYear> GetDHRFiscalYear(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<FiscalYear> responseModel = new ResponseModel<FiscalYear>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentFiscalYear;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<FiscalYear>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;   
        //}

        //public ResponseModel<Position> GetDHRPosition(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<Position> responseModel = new ResponseModel<Position>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentPosition;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<Position>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;
        //}

        //public ResponseModel<ReportTo> GetDHRReportTo(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<ReportTo> responseModel = new ResponseModel<ReportTo>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentReportTo;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<ReportTo>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;
        //}

        //public ResponseModel<Unit> GetDHRUnit(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<Unit> responseModel = new ResponseModel<Unit>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentUnit;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<Unit>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;
        //}

        //public ResponseModel<SolidLine> GetDHRSolidLine(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<SolidLine> responseModel = new ResponseModel<SolidLine>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentSolidLine;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<SolidLine>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;
        //}

        //public ResponseModel<DotedLine> GetDHRDotedLine(string startDate, string endDate, string ApiKey)
        //{
        //    ResponseModel<DotedLine> responseModel = new ResponseModel<DotedLine>();
        //    bool isSuccess = false;
        //    int connnectionTime = 0;
        //    while (!isSuccess && (connnectionTime < MaxReconnectLimit))
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(BaseUrl);
        //            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
        //            client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

        //            string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentDotedLine;
        //            string strParams = startDate + "/" + endDate + "/" + CompanyCode;
        //            string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

        //            HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var content = response.Content.ReadAsStringAsync().Result;
        //                responseModel = JsonConvert.DeserializeObject<ResponseModel<DotedLine>>(content);
        //                isSuccess = response.IsSuccessStatusCode;
        //            }
        //            else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
        //            {
        //                ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
        //            }
        //            else
        //            {
        //                responseModel.message = responseModel.message + " " + response.ReasonPhrase;
        //            }
        //        }
        //        connnectionTime++;
        //    }

        //    return responseModel;
        //}
        #endregion
        #region OM POSITION
        public ResponseModel<PositionModel> GetDHRPosition(string ApiKey, string startDate, string endDate, string unitCode = null, string unitLevelValue = null, string posCode = null)
        {
            ResponseModel<PositionModel> responseModel = new ResponseModel<PositionModel>();

            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");

                    string strUrlPamarm = !string.IsNullOrEmpty(unitCode) ? "unitCode=" + unitCode + "&" : !string.IsNullOrEmpty(unitLevelValue) ? "unitLevelValue=" + unitLevelValue + "&" : !string.IsNullOrEmpty(posCode) ? "posCode=" + posCode + "&" : string.Empty;

                    string oRoute = OrganizationRoute.Route(CompanyCode).OM_PositionRoute_UIGETOMPOSITION;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;

                    if (!string.IsNullOrEmpty(strUrlPamarm))
                    {
                        strParams = strParams + "?" + strUrlPamarm;
                    }

                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<PositionModel>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }
        public PostResult PostDHROMSolidLine_UI(SolidLineModel oSolidData, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            object oRequiredData = new
            {
                id = oSolidData.id,
                companyCode = oSolidData.companyCode,
                startDate = oSolidData.startDate,
                endDate = oSolidData.endDate,
                posCode = !string.IsNullOrEmpty(oSolidData.posCode)?oSolidData.posCode:string.Empty,
                posShortname = oSolidData.posShortname,
                posName = oSolidData.posName,
                posShortnameEn = oSolidData.posShortnameEn,
                posNameEn = oSolidData.posNameEn,
                bandCode = oSolidData.bandCode,
                bandValue = oSolidData.bandValue,
                headOfUnitF = oSolidData.headOfUnitF,
                unitCode = oSolidData.unitCode,
                headCode = oSolidData.headCode,
                actionType = oSolidData.actionType,
                serviceType = oSolidData.serviceType,
            };

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                   // Comment by Chanakrit.t 2021/09/03
                   // client.DefaultRequestHeaders.Add("ActionBy", "UI");

                    string oRoute = OrganizationRoute.Route(CompanyCode).OM_PositionRoute_UIPOSTOMPOSITION;
                    string dataPost = JsonConvert.SerializeObject(oRequiredData);//oOMData
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "OMPOSITION", ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }
        public PostResult PostDHROMDotedLine_UI(DotedLineModel oDotedData, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            object oRequiredData = new
            {
                id = oDotedData.id,
                companyCode = oDotedData.companyCode,
                startDate = oDotedData.startDate,
                endDate = oDotedData.endDate,
                posCode = oDotedData.posCode,
                headPosition = oDotedData.headPosition,
                actionType = oDotedData.actionType,
                serviceType = oDotedData.serviceType
            };

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");

                    string oRoute = OrganizationRoute.Route(CompanyCode).OM_PositionRoute_UIPOSTOMDOTEDLINE;
                    string dataPost = JsonConvert.SerializeObject(oRequiredData);//oOMData
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "OMUNIT", ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }
        #endregion

        public ResponseModel<Unit> GetDHRUnit(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<Unit> responseModel = new ResponseModel<Unit>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentUnit;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<Unit>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<OrganizationModel> GetDHROMUnit_UI(string startDate, string endDate, string unitCode, string unitLevel, string ApiKey)
        {
            ResponseModel<OrganizationModel> responseModel = new ResponseModel<OrganizationModel>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsSentOMUnit_UI;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string listOfParam = "?"
                    + "unitCode" + "=" + unitCode + "&"
                    + "unitLevelValue" + "=" + unitLevel;

                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams + listOfParam);
                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<OrganizationModel>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public PostResult PostDHROMUnit_UI(OrganizationModel oOMData, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            object oRequiredData = new
            {
               id = oOMData.id,
               companyCode = oOMData.companyCode,
               unitCode = oOMData.unitCode,
               unitShortname = oOMData.unitShortname,
               unitName = oOMData.unitName,
               unitShortnameEn = oOMData.unitShortnameEn,
               unitNameEn = oOMData.unitNameEn,
               unitLevelCode = String.IsNullOrEmpty(oOMData.unitLevelValue) ? "" : oOMData.unitLevelCode,
               unitLevelValue = Convert.ToString(oOMData.unitLevelValue),
               costCenterCode = String.IsNullOrEmpty(oOMData.costCenterValue) ? ""  :  oOMData.costCenterCode,
               costCenterValue = Convert.ToString(oOMData.costCenterValue),
               upperUnitCode = Convert.ToString(oOMData.upperUnitCode),
               startDate = oOMData.startDate,
               endDate = oOMData.endDate,
               priority = oOMData.priority,
               actiontype = oOMData.actiontype,
               servicetype = oOMData.servicetype
            };

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.Add("ActionBy", "UI");

                    string oRoute = OrganizationRoute.Route(CompanyCode).OrganizationRoute_WsReceiveOMUnitInf;
                    string dataPost = JsonConvert.SerializeObject(oRequiredData);//oOMData
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "OMUNIT", ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase + " " + ApiGateWay + " " + dataPost;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }
    }
}
