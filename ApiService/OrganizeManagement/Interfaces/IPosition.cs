﻿using DHR.HR.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Interfaces
{
    public interface IPosition
    {
        List<PositionModel> GetDHRPosition(string ApiKey, string startDate, string endDate, string unitCode = null, string unitLevelValue = null, string posCode = null);
    }
}
