﻿using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Interfaces
{
    public interface IOrganization
    {
        #region Interface Unused
        //ResponseModel<BelongTo> GetDHRBelongTo(string startDate, string endDate, string ApiKey);
        //ResponseModel<ComSecondment> GetDHRComSecondment(string startDate, string endDate, string ApiKey);
        //ResponseModel<Company> GetDHRCompany(string startDate, string endDate, string ApiKey);
        //ResponseModel<FiscalYear> GetDHRFiscalYear(string startDate, string endDate, string ApiKey);
        //ResponseModel<Position> GetDHRPosition(string startDate, string endDate, string ApiKey);
        //ResponseModel<ReportTo> GetDHRReportTo(string startDate, string endDate, string ApiKey);
        //ResponseModel<Unit> GetDHRUnit(string startDate, string endDate, string ApiKey);
        //ResponseModel<SolidLine> GetDHRSolidLine(string startDate, string endDate, string ApiKey);
        //ResponseModel<DotedLine> GetDHRDotedLine(string startDate, string endDate, string ApiKey);
        #endregion
        #region Posotion
        ResponseModel<PositionModel> GetDHRPosition(string ApiKey, string startDate, string endDate, string unitCode = null, string unitLevelValue = null, string posCode = null);
        PostResult PostDHROMSolidLine_UI(SolidLineModel oSolidData, string ApiKey);
        PostResult PostDHROMDotedLine_UI(DotedLineModel oDotedData, string ApiKey);
        #endregion
        ResponseModel<Unit> GetDHRUnit(string startDate, string endDate, string ApiKey);
        ResponseModel<OrganizationModel> GetDHROMUnit_UI(string startDate, string endDate, string unitCode, string unitLevel, string ApiKey);
        PostResult PostDHROMUnit_UI(OrganizationModel oOMData, string ApiKey);
    }
}
