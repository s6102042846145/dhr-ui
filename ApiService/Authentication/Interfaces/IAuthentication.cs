﻿using DHR.HR.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Interfaces
{
    public interface IAuthentication
    {
       string RequestApiToken(string username , string password);
      
       string RequestApiTokenRefresh(string token);
    }
}
