﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API.Interfaces;
using ESS.SHAREDATASERVICE;

namespace DHR.HR.API
{
    public class AuthenServiceManager
    {
        #region Constructor
        public AuthenServiceManager()
        {

        }
        #endregion Constructor

        #region MultiCompany Framework
        private static Dictionary<string, AuthenServiceManager> Cache = new Dictionary<string, AuthenServiceManager>();

        private static string ModuleID = "DHR.HR";

        private string CompanyCode { get; set; }

        private string BaseUrl { get; set; }

        private string Gateway { get; set; }

        public static AuthenServiceManager CreateInstance(string oCompanyCode, string oBaseUrl, string oGateway)
        {
            AuthenServiceManager oAuthenServiceManager = new AuthenServiceManager()
            {
                CompanyCode = oCompanyCode,
                BaseUrl = oBaseUrl,
                Gateway = oGateway
            };
            return oAuthenServiceManager;
        }
        #endregion Multicompany Framework

        #region "private Data"

        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"{ModuleID}.{Mode.ToUpper()}");
            string typeName = string.Format($"{ModuleID}.{Mode.ToUpper()}.{ClassName}");
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            return oReturn;

        }
        #endregion "private Data"

        private string MODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MODE");
            }
        }

        public IAuthentication API
        {
            get
            {
                Type otype = GetService(MODE, "Authentication");
                if (otype == null)
                {
                    return null;
                }
                else
                {
                    return (IAuthentication)Activator.CreateInstance(otype, CompanyCode, BaseUrl, Gateway);

                }
            }
        }
    }

}
