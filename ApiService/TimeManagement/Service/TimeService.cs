﻿using DHR.HR.API.Interfaces;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using ESS.SHAREDATASERVICE;
using System.Net.Http.Headers;

namespace DHR.HR.API
{
    public class TimeService : ITimeService
    {
        #region Constructor

        public TimeService(string oCompanyCode, string oBaseUrl, string oGateWay)
        {
            CompanyCode = oCompanyCode;
            BaseUrl = oBaseUrl;
            Gateway = oGateWay;
        }
        #endregion Constructor

        #region Member
        private string ModuleID = "DHR.HR";
        private string CompanyCode { get; set; }
        private string BaseUrl { get; set; }
        private string Gateway { get; set; }
        private string ContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CONTENTTYPE");
            }
        }

        private string Scheme
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SCHEME");
            }
        }

        private string HeaderContentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "HEADERCONTENTTYPE");
            }
        }

        private int MaxReconnectLimit
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAXRECONNECTIONATTEMPTLIMIT"));
            }
        }
        #endregion
        public ResponseModel<TMBreakSchedule> GetDHRTMBreakSchedule(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMBreakSchedule> responseModel = new ResponseModel<TMBreakSchedule>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMBreakSchedule;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMBreakSchedule>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMCalenHoliday> GetDHRTMCalenHoliday(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMCalenHoliday> responseModel = new ResponseModel<TMCalenHoliday>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMCalenHoliday;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMCalenHoliday>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMChaWorkSch> GetDHRTMChaWorkSch(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMChaWorkSch> responseModel = new ResponseModel<TMChaWorkSch>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMChaWorkSch;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMChaWorkSch>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMCollisionCheck> GetDHRTMCollisionCheck(string startDate, string endDate,  string leaveStartTime, string leaveEndTime, string ApiKey,string empCode)
        {
            ResponseModel<TMCollisionCheck> responseModel = new ResponseModel<TMCollisionCheck>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMCollisionCheck;
                    string listOfParam = "?";
                    if (!string.IsNullOrEmpty(empCode))
                        listOfParam = listOfParam + "empCode=" + empCode + "&";
                    if (!string.IsNullOrEmpty(leaveStartTime))
                        listOfParam = listOfParam + "leaveStartTime=" + leaveStartTime + "&";
                    if (!string.IsNullOrEmpty(leaveEndTime))
                        listOfParam = listOfParam + "leaveEndTime=" + leaveEndTime;

                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + listOfParam;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMCollisionCheck>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
            return responseModel;
        }
        public ResponseModel<TMChaWorkSch> GetDHRTMChaWorkSchByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<TMChaWorkSch> responseModel = new ResponseModel<TMChaWorkSch>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMChaWorkSch;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMChaWorkSch>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMDailyWorkSchedule> GetDHRTMDailyWorkSchedule(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMDailyWorkSchedule> responseModel = new ResponseModel<TMDailyWorkSchedule>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMDailyWorkSchedule;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMDailyWorkSchedule>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMEmpGroup> GetDHRTMEmpGroup(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMEmpGroup> responseModel = new ResponseModel<TMEmpGroup>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMEmpGroup;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMEmpGroup>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMLeaveInf> GetDHRTMLeaveInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMLeaveInf> responseModel = new ResponseModel<TMLeaveInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMLeaveInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMLeaveInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMLeaveInf> GetDHRTMLeaveInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<TMLeaveInf> responseModel = new ResponseModel<TMLeaveInf>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMLeaveInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMLeaveInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMLeaveQuota> GetDHRTMLeaveQuota(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMLeaveQuota> responseModel = new ResponseModel<TMLeaveQuota>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMLeaveQuota;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMLeaveQuota>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMLeaveQuota> GetDHRTMLeaveQuotaByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<TMLeaveQuota> responseModel = new ResponseModel<TMLeaveQuota>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMLeaveQuota;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    var check = response.Content.Headers.ContentType.MediaType;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMLeaveQuota>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMLeaveQuotaType> GetDHRTMLeaveQuotaType(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMLeaveQuotaType> responseModel = new ResponseModel<TMLeaveQuotaType>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMLeaveQuotaType;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMLeaveQuotaType>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMLeaveType> GetDHRTMLeaveType(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMLeaveType> responseModel = new ResponseModel<TMLeaveType>();
            bool isSuccess = false;
            int connnectionTime = 0;
            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMLeaveType;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMLeaveType>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }


            return responseModel;
        }

        public ResponseModel<TMOTType> GetDHRTMOTType(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMOTType> responseModel = new ResponseModel<TMOTType>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMOTType;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMOTType>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
            return responseModel;
        }

        public ResponseModel<TMOTTypeInf> GetDHRTMOTTypeInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMOTTypeInf> responseModel = new ResponseModel<TMOTTypeInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMOTTypeInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMOTTypeInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMOTTypeInf> GetDHRTMOTTypeInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<TMOTTypeInf> responseModel = new ResponseModel<TMOTTypeInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMOTTypeInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMOTTypeInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMPeriodWorkSchedule> GetDHRTMPeriodWorkSchedule(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMPeriodWorkSchedule> responseModel = new ResponseModel<TMPeriodWorkSchedule>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMPeriodWorkSchedule;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMPeriodWorkSchedule>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }
            return responseModel;
        }

        public ResponseModel<TMCalculateLeaveDataReceive> GetDHRTMCalculateLeaveData(string startDate, string endDate, string ApiKey, string EmpCode, TMCalculateLeaveDataInf oData)
        {
            ResponseModel<TMCalculateLeaveDataReceive> responseModel = new ResponseModel<TMCalculateLeaveDataReceive>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMCalculateLeaveData;
                    string listOfParam = "?"
                        + "empCode" + "=" + oData.empCode + "&"
                        + "leaveTypeCode" + "=" + oData.leaveTypeCode + "&"
                        + "checkQuota" + "=" + oData.checkQuota + "&"
                        + "leaveStartTime" + "=" + oData.leaveStartTime + "&"
                        + "leaveEndTime" + "=" + oData.leaveEndTime + "&"
                        + "skipDayoff" + "=" + oData.skipDayoff;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + listOfParam;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMCalculateLeaveDataReceive>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }
        public ResponseModel<TMTimeAttendCalculateDataReceive> GetDHRTMCalculateAttendanceData(string startDate, string endDate, string ApiKey, string EmpCode, TMTimeAttendCalculateDataInf oData)
        {
            ResponseModel<TMTimeAttendCalculateDataReceive> responseModel = new ResponseModel<TMTimeAttendCalculateDataReceive>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMCalculateAttendData;
                    string listOfParam = "?"
                        + "empCode" + "=" + oData.empCode + "&"
                        + "workingTypeCode" + "=" + oData.attendanceTypeCode + "&"
                        + "startTime" + "=" + oData.startTime + "&"
                        + "endTime" + "=" + oData.endTime + "&"
                        + "skipDayoff" + "=" + oData.skipDayoff;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + listOfParam;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeAttendCalculateDataReceive>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMPersonalArea> GetDHRTMPersonalArea(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMPersonalArea> responseModel = new ResponseModel<TMPersonalArea>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMPersonalArea;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMPersonalArea>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMPlanWorkTime> GetDHRTMPlanWorkTime(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMPlanWorkTime> responseModel = new ResponseModel<TMPlanWorkTime>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMPlanWorkTime;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMPlanWorkTime>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMSetupHoliday> GetDHRTMSetupHoliday(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMSetupHoliday> responseModel = new ResponseModel<TMSetupHoliday>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMSetupHoliday;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMSetupHoliday>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMSubsititutionType> GetDHRTMSubsititutionType(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMSubsititutionType> responseModel = new ResponseModel<TMSubsititutionType>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMSubsititutionType;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMSubsititutionType>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMTimeAttend> GetDHRTMTimeAttend(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMTimeAttend> responseModel = new ResponseModel<TMTimeAttend>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMTimeAttend;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeAttend>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMTimeAttend> GetDHRTMTimeAttendByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<TMTimeAttend> responseModel = new ResponseModel<TMTimeAttend>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMTimeAttend;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeAttend>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMTimeRecInf> GetDHRTMTimeRecInf(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMTimeRecInf> responseModel = new ResponseModel<TMTimeRecInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMTimeRecInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeRecInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMTimeRecInf> GetDHRTMTimeRecInfByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<TMTimeRecInf> responseModel = new ResponseModel<TMTimeRecInf>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMTimeRecInf;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeRecInf>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMTimeRecInOut> GetDHRTMTimeRecInOut(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMTimeRecInOut> responseModel = new ResponseModel<TMTimeRecInOut>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMTimeRecInOut;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeRecInOut>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMTimeRecInOut> GetDHRTMTimeRecInOutByEmpCode(string startDate, string endDate, string ApiKey, string EmpCode)
        {
            ResponseModel<TMTimeRecInOut> responseModel = new ResponseModel<TMTimeRecInOut>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMTimeRecInOut;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode + "/" + EmpCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeRecInOut>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, EmpCode, ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMTimeRecType> GetDHRTMTimeRecType(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMTimeRecType> responseModel = new ResponseModel<TMTimeRecType>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMTimeRecType;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMTimeRecType>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMWorkingType> GetDHRTMWorkingType(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMWorkingType> responseModel = new ResponseModel<TMWorkingType>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMWorkingType;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMWorkingType>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMWorkSchedule> GetDHRTMWorkSchedule(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMWorkSchedule> responseModel = new ResponseModel<TMWorkSchedule>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMWorkSchedule;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMWorkSchedule>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public ResponseModel<TMWorkScheduleRule> GetDHRTMWorkScheduleRule(string startDate, string endDate, string ApiKey)
        {
            ResponseModel<TMWorkScheduleRule> responseModel = new ResponseModel<TMWorkScheduleRule>();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));
                    client.DefaultRequestHeaders.TryAddWithoutValidation(HeaderContentType, ContentType + "; charset=" + Encoding.UTF8);

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsSentTMWorkScheduleRule;
                    string strParams = startDate + "/" + endDate + "/" + CompanyCode;
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute, strParams);

                    HttpResponseMessage response = client.GetAsync(ApiGateWay).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        responseModel = JsonConvert.DeserializeObject<ResponseModel<TMWorkScheduleRule>>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        responseModel.message = responseModel.message + " " + response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return responseModel;
        }

        public PostResult PostDHRTMChaWorkSch(TMChaWorkSch chaWorkSch, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsReceiveTMChaWorkSch;
                    string dataPost = JsonConvert.SerializeObject(chaWorkSch);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, chaWorkSch.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRTMLeaveInf(TMLeaveInf leaveInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsReceiveTMLeaveInf;
                    string dataPost = JsonConvert.SerializeObject(leaveInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, leaveInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRTMOTTypeInf(TMOTTypeInf otTypeInf, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsReceiveTMOTTypeInf;
                    string dataPost = JsonConvert.SerializeObject(otTypeInf);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, otTypeInf.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRTMTimeAttend(TMTimeAttend timeAttend, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsReceiveTMTimeAttend;
                    string dataPost = JsonConvert.SerializeObject(timeAttend);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, timeAttend.empCode, ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }

        public PostResult PostDHRTMTimeRecInOut(TMTimeRecInOutPost timeRecInOut, string ApiKey)
        {
            PostResult postResult = new PostResult();
            bool isSuccess = false;
            int connnectionTime = 0;

            while (!isSuccess && (connnectionTime < MaxReconnectLimit))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, ApiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(ContentType));

                    string oRoute = TimeRoute.Route(CompanyCode).TimeRoute_WsReceiveTMTimeRecInOut;
                    string dataPost = JsonConvert.SerializeObject(timeRecInOut);
                    var contentPost = new StringContent(dataPost, Encoding.UTF8, ContentType);
                    string ApiGateWay = ApiGatewayService.ApiGateWay(Gateway, oRoute);

                    HttpResponseMessage response = client.PostAsync(ApiGateWay, contentPost).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content.ReadAsStringAsync().Result;
                        postResult = JsonConvert.DeserializeObject<PostResult>(content);
                        isSuccess = response.IsSuccessStatusCode;
                    }
                    else if (ApiKeyService.CheckTokenIsExpired(response) && (connnectionTime + 1) < MaxReconnectLimit)
                    {
                        ApiKey = ApiKeyService.ApiKeyRefreshManageMent(CompanyCode, "", ApiKey);
                    }
                    else
                    {
                        postResult.returnDesc = response.ReasonPhrase;
                    }
                }
                connnectionTime++;
            }

            return postResult;
        }
    }
}
