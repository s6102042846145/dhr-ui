﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMSetupHoliday
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string year { get; set; }
        public string holidayCode { get; set; }
        public string holidayName { get; set; }
        public string holidayNameEN { get; set; }
        public string date { get; set; }
    }
}
