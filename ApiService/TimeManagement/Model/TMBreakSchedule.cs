﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMBreakSchedule
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string breakSchCode { get; set; }
        public string breakSchName { get; set; }
        public string startBreakTime { get; set; }
        public string endBreakTime { get; set; }
        public string paidHour { get; set; }
        public string unpaidHour { get; set; }
    }
}
