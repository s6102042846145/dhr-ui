﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMWorkSchedule
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string empGrpGrouping { get; set; }
        public string psnAreaGrouping { get; set; }
        public string holiCalenCode { get; set; }
        public string workSchCode { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public string day01 { get; set; }
        public string day02 { get; set; }
        public string day03 { get; set; }
        public string day04 { get; set; }
        public string day05 { get; set; }
        public string day06 { get; set; }
        public string day07 { get; set; }
        public string day08 { get; set; }
        public string day09 { get; set; }
        public string day10 { get; set; }
        public string day11 { get; set; }
        public string day12 { get; set; }
        public string day13 { get; set; }
        public string day14 { get; set; }
        public string day15 { get; set; }
        public string day16 { get; set; }
        public string day17 { get; set; }
        public string day18 { get; set; }
        public string day19 { get; set; }
        public string day20 { get; set; }
        public string day21 { get; set; }
        public string day22 { get; set; }
        public string day23 { get; set; }
        public string day24 { get; set; }
        public string day25 { get; set; }
        public string day26 { get; set; }
        public string day27 { get; set; }
        public string day28 { get; set; }
        public string day29 { get; set; }
        public string day30 { get; set; }
        public string day31 { get; set; }

        public string day01_H { get; set; }
        public string day02_H { get; set; }
        public string day03_H { get; set; }
        public string day04_H { get; set; }
        public string day05_H { get; set; }
        public string day06_H { get; set; }
        public string day07_H { get; set; }
        public string day08_H { get; set; }
        public string day09_H { get; set; }
        public string day10_H { get; set; }
        public string day11_H { get; set; }
        public string day12_H { get; set; }
        public string day13_H { get; set; }
        public string day14_H { get; set; }
        public string day15_H { get; set; }
        public string day16_H { get; set; }
        public string day17_H { get; set; }
        public string day18_H { get; set; }
        public string day19_H { get; set; }
        public string day20_H { get; set; }
        public string day21_H { get; set; }
        public string day22_H { get; set; }
        public string day23_H { get; set; }
        public string day24_H { get; set; }
        public string day25_H { get; set; }
        public string day26_H { get; set; }
        public string day27_H { get; set; }
        public string day28_H { get; set; }
        public string day29_H { get; set; }
        public string day30_H { get; set; }
        public string day31_H { get; set; }
    }
}
