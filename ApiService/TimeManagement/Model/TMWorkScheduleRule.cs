﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMWorkScheduleRule
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string psnAreaGrouping { get; set; }
        public string workSchCode { get; set; }
        public string workSchName { get; set; }
        public int dailyWorkHour { get; set; }
        public int weekWorkHour { get; set; }
        public int weekWorkDay { get; set; }
        public int monthWorkHour { get; set; }
        public string pwsCode { get; set; }
    }
}
