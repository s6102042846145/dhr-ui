﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMTimeAttendCalculateDataInf
    {

        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string attendanceTypeCode { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string skipDayoff { get; set; }
    }
}
