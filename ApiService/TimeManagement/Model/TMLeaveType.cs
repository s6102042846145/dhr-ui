﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMLeaveType
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string empGrpGrouping { get; set; }
        public string psnAreaGrouping { get; set; }
        public string leaveTypeCode { get; set; }
        public string leaveTypeName { get; set; }
        public string firstDateDayoffFlag { get; set; }
        public string endDateDayoffFlag { get; set; }
        public string nonWorkPeriodFlag { get; set; }
        public string dayTypeFlag { get; set; }
        public string countHoliFlag { get; set; }
        public string leaveQuotaCode { get; set; }
    }
}
