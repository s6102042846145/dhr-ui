﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMTimeAttend
    {
        public string companyCode { get; set; }
        public string timeAttendID { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string workingTypeCode { get; set; }
        public string timeStart { get; set; }
        public string endTime { get; set; }
        public double? hour { get; set; }
        public double? day { get; set; }
        public string prevDay { get; set; }
        public string fullDay { get; set; }
        public string actionType { get; set; }
    }
}
