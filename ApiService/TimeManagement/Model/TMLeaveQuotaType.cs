﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMLeaveQuotaType
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string empGrpGrouping { get; set; }
        public string psnAreaGrouping { get; set; }
        public string leaveQoutaCode { get; set; }
        public string leaveQoutaName { get; set; }
        public string unitCode { get; set; }
        public string unitValue { get; set; }
        public int timeConst { get; set; }
    }
}
