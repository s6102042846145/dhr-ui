﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMCollisionCheck
    {
        public string companyCode { get; set; }
        public string empCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string leaveStartTime { get; set; }
        public string leaveEndTime { get; set; }
        public string status { get; set; }
        public string collisionType { get; set; }
        public string collisionSubType { get; set; }
        public int collisionID { get; set; }
        public string collisionStartDate { get; set; }
        public string collisionEndDate { get; set; }
        public string collisionStartTime { get; set; }
        public string collisionEndTime { get; set; }

    }
}


