﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMWorkingType
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string workingTypeCode { get; set; }
        public string workingTypeName { get; set; }
        public int minDuration { get; set; }
        public int maxDuration { get; set; }
        public string dayTypeFlag { get; set; }
    }
}
