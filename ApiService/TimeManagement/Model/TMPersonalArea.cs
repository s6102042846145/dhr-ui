﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMPersonalArea
    {
        public string companyCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string psnArea { get; set; }
        public string psnSubArea { get; set; }
        public string psnAreaGrouping { get; set; }
    }
}
