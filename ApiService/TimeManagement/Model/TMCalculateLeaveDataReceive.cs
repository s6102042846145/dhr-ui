﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHR.HR.API.Model
{
    public class TMCalculateLeaveDataReceive
    {

      public string companyCode { get; set; }
      public string empCode { get; set; }
      public string startDate { get; set; }
      public string endDate { get; set; }
      public string startTime { get; set; }
      public string endTime { get; set; }
      public string absenceDays { get; set; }
      public string absenceHours { get; set; }
      public string calendarDays { get; set; }
      public string payrollDays { get; set; }
      public string fullDayFlag { get; set; }
      public string previousDayFlag { get; set; }
      public string checkQuotaResult { get; set; }
      public string calculateLeaveInfResult { get; set; }


    }
}
