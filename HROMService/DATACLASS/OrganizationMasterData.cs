﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.DATACLASS
{
    public class OrganizationMasterData
    {
        private OrganizationCenter _organizationData = new OrganizationCenter();

        public OrganizationCenter OrganizationData
        {
            get
            {
                return _organizationData;
            }
            set
            {
                _organizationData = value;
            }
        }

        private OrganizationCenter _organizationDataOLD = new OrganizationCenter();
        public OrganizationCenter OrganizationDataOLD
        {
            get
            {
                return _organizationDataOLD;
            }
            set
            {
                _organizationDataOLD = value;
            }
        }

        private string _actionType = String.Empty;

    }
}
