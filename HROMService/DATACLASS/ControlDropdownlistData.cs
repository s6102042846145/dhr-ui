﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.DATACLASS
{
    public class ControlDropdownlistData : AbstractObject
    {
        public string DLL_DATA { get; set; }
        public string DLL_VALUE { get; set; }

    }
}
