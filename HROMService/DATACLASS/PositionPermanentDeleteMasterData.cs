﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.DATACLASS
{
    class PositionPermanentDeleteMasterData
    {
        private List<PositionCenter> _positionData = new List<PositionCenter>();
        public List<PositionCenter> PositionData
        {
            get
            {
                return _positionData;
            }
            set
            {
                _positionData = value;
            }
        }

        private List<PositionCenter> _positionData_OLD = new List<PositionCenter>();
        public List<PositionCenter> PositionData_OLD
        {
            get
            {
                return _positionData_OLD;
            }
            set
            {
                _positionData_OLD = value;
            }
        }
    }
}
