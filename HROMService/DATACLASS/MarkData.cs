﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.DATACLASS
{
    public class MarkData : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string DataCategory { get; set; }
        public string RequestNo { get; set; }
        #region Optional
        public int RequestTypeID {get; set;}
        #endregion
    }
}
