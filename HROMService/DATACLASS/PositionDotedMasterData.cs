﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM.DATACLASS
{
    public class PositionDotedMasterData
    {
        private PositionDotedCenter _positionDotedData = new PositionDotedCenter();
        public PositionDotedCenter PositionDotedData
        {
            get
            {
                return _positionDotedData;
            }
            set
            {
                _positionDotedData = value;
            }
        }

        private PositionDotedCenter _positionDotedData_OLD = new PositionDotedCenter();
        public PositionDotedCenter PositionDotedData_OLD
        {
            get
            {
                return _positionDotedData_OLD;
            }
            set
            {
                _positionDotedData_OLD = value;
            }
        }
    }
}
