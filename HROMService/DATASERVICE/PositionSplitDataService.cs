﻿
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using System.Reflection;

namespace ESS.HR.OM.DATASERVICE
{
    class PositionSplitDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PositionMasterData PositionData = new PositionMasterData();
            PositionCenter tmp = new PositionCenter();
            if (!string.IsNullOrEmpty(CreateParam))
            {   //request_id = 1205
                //pos_code|start_date|end_date ,  pos_code|start_date|end_date
                string[] param = CreateParam.Split('|');
                string poscode = string.Empty;
                DateTime startDate = new DateTime(DateTime.Now.Date.Year, 1, 1);
                DateTime endDate = new DateTime(9999, 12, 31);
                if (param.Count() > 0)
                {
                    poscode = param[0];
                    startDate = DateTimeService.ConvertDateTimeURL(Requestor.CompanyCode, param[1]);
                    endDate = DateTimeService.ConvertDateTimeURL(Requestor.CompanyCode, param[2]);
                }
                if (!string.IsNullOrEmpty(poscode))
                {
                    //select วันที่สุดท้าย
                    tmp = HROMManagement.CreateInstance(Requestor.CompanyCode).GetPositionByPosCode(poscode, startDate, endDate).OrderByDescending(a => a.EndDate).FirstOrDefault();
                    //new startdata > old startdate
                    tmp.StartDate = tmp.StartDate.AddDays(1);
                    tmp.ActionType = "S";
                }
            }
            PositionData.PositionData = tmp;
            PositionData.PositionData_OLD = tmp;
            return PositionData;
        }
        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {

        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //validate org not modify
            string oExceptionError = string.Empty;
            string oErrorTextCode = string.Empty;
            string oErrorParam = string.Empty;
            PositionMasterData PositionData = JsonConvert.DeserializeObject<PositionMasterData>(newData.ToString());
            string dataCategory = string.Format("HROMPOSITION_{0}", PositionData.PositionData.PosCode); //HROMPOSITION_POSCODE

            #region Check MarkData
            string oRequestNo = RequestNo == "DUMMY" ? "" : RequestNo;
            string oCondition = "=";
            //Check Org
            string oDataCategory = String.Format("HROMORGANIZATION_{0}", PositionData.PositionData.UnitCode);
            oCondition = "%";
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition)
                && oDataCategory != "HROMORGANIZATION_")
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_UNITCODE");


            //check PosCode
            oDataCategory = String.Format("HROMPOSITION_{0}", PositionData.PositionData.PosCode);
            oCondition = "%";
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition)
                && oDataCategory != "HROMPOSITION_")
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_POSCODE");
            #endregion

            if (RequestNo.ToUpper() == "DUMMY")
            {
                if (HROMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(dataCategory))
                {
                    throw new DataServiceException("HROM", "REQUEST_DUPLICATE", "Request duplicate");
                }
            }
            else
            {
                if (HROMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(dataCategory, RequestNo))
                {
                    throw new DataServiceException("HROM", "REQUEST_DUPLICATE", "Request duplicate");
                }
            }
            PositionData.PositionData.ActionType = "S";
            bool result = ServiceManager.CreateInstance(Requestor.CompanyCode, "DHR").DataService.SaveWithValidateSolidLineData(PositionData.PositionData, "V", out oExceptionError, out oErrorTextCode, out oErrorParam);

            if (!result)
            {
                if (!string.IsNullOrEmpty(oErrorTextCode))
                {
                    string[] param = !string.IsNullOrEmpty(oErrorParam) ? oErrorParam.Split('|') : null;
                    throw new DataServiceException("VALIDATION", oErrorTextCode, param);
                }
                else if (!string.IsNullOrEmpty(oExceptionError))
                {
                    throw new DataServiceException("HROM_EXCEPTION", "POSITION_VALIDATE_OTHER", oExceptionError);
                }
                else
                {
                    throw new DataServiceException("HROM_EXCEPTION", "POSITION_EXCEPTION");
                }

            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Do Something
            PositionMasterData PositionData = JsonConvert.DeserializeObject<PositionMasterData>(Data.ToString());
            string oExceptionError = string.Empty;
            string oErrorTextCode = string.Empty;
            string oErrorParam = string.Empty;
            string dataCategory = string.Format("HROMPOSITION_{0}", PositionData.PositionData.PosCode); //HROMPOSITION_DOTED_HEADPOSCODE_POSCODE
            HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    // save something to dhr
                    PositionData.PositionData.ActionType = "S";
                    bool result = ServiceManager.CreateInstance(Requestor.CompanyCode, "DHR").DataService.SaveWithValidateSolidLineData(PositionData.PositionData, "S", out oExceptionError, out oErrorTextCode, out oErrorParam);

                    if (!result)
                    {
                        if (!string.IsNullOrEmpty(oErrorTextCode))
                        {
                            string[] param = !string.IsNullOrEmpty(oErrorParam) ? oErrorParam.Split('|') : null;
                            throw new DataServiceException("VALIDATION", oErrorTextCode, param);
                        }
                        else if (!string.IsNullOrEmpty(oExceptionError))
                        {
                            throw new DataServiceException("HROM_EXCEPTION", "POSITION_VALIDATE_OTHER", oExceptionError);
                        }
                        else
                        {
                            throw new DataServiceException("HROM_EXCEPTION", "POSITION_EXCEPTION");
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw new DataServiceException("HROM_EXCEPTION", "SAVE_EXTERNAL_ERROR " + ex.Message);
                }
            }
        }

    }
}
