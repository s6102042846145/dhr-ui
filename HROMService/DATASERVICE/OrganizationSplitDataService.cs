﻿
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.HR.OM;
using ESS.HR.OM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;
using System.Linq;
using Newtonsoft.Json;

namespace ESS.HR.OM.DATASERVICE
{
   class OrganizationSplitDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            OrganizationMasterData oReturn = new OrganizationMasterData();
            if (!String.IsNullOrEmpty(CreateParam))
            {
                string oOrgID = CreateParam.Split('|')[0];
                //oReturn = HROMManagement.CreateInstance(Requestor.CompanyCode).getOrganizationDetail(oOrgID, oBegDate.Date, oEndDate.Date);
                var orgStructure = HROMManagement.CreateInstance(Requestor.CompanyCode).GetOrganizationDataContent("", oOrgID, new DateTime(1900,1,1), new DateTime(9999,12,31)).LastOrDefault();
                orgStructure.BeginDate = orgStructure.BeginDate.AddDays(1);
                oReturn.OrganizationData = orgStructure;
                oReturn.OrganizationDataOLD = orgStructure;
            }
            return oReturn;
        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {
            
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            OrganizationMasterData Organization = JsonConvert.DeserializeObject<OrganizationMasterData>(newData.ToString());
            OrganizationCenter additional = Organization.OrganizationData;
            string oActionType = "S", // for Insert
                oServiceType = "V", // type Validate
                DataSource = HROMServices.SVC(Requestor.CompanyCode).GET_OMVALIDATE_SOURCE;

            #region Check MarkData
            string oRequestNo = RequestNo == "DUMMY" ? "" : RequestNo;
            string oCondition = "=";
            //Check Org
            string oDataCategory = String.Format("HROMORGANIZATION_{0}", additional.OrganizationID);
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORG");

            //check Pos
            oDataCategory = String.Format("HROMPOSITION_{0}", additional.OrganizationID);
            oCondition = "%";
            if (ServiceManager.CreateInstance(Requestor.CompanyCode, "DB").DataService.CheckMarkWithWhereCause(oDataCategory, oRequestNo, oCondition))
                throw new DataServiceException("HROM_EXCEPTION", "MARKDATA_DUPPLICATE_ORGPOS");
            #endregion

            try
            {
                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSource).DataService.ValidateOrganizationData(additional, oActionType, oServiceType);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //DataServiceException("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            OrganizationMasterData Organization = JsonConvert.DeserializeObject<OrganizationMasterData>(Data.ToString());
            string dataCategoryOrg = string.Format("HROMORGANIZATION_{0}", Organization.OrganizationDataOLD.OrganizationID);

            if (State == "COMPLETED")
            {
                try
                {
                    HROMManagement.CreateInstance(Requestor.CompanyCode).SaveOrganizationData(Organization.OrganizationData, "S", "S");
                    HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //DataServiceException("HROM_EXCEPTION", "OM_SPLIT_SAVE");
                }
            }
            else
            {
                HROMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategoryOrg, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            }
        }

    }
}
