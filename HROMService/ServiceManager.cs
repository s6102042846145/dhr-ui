using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.HR.OM.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.HR.OM
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.HR.OM";

        public string CompanyCode { get; set; }
        private string DataSource { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {

            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }

        public static ServiceManager CreateInstance(string oCompanyCode, string oDataSource)
        {

            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode,
                DataSource = oDataSource
            };
            return oServiceManager;
        }

        #endregion MultiCompany  Framework

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "


        internal IHROMConfigService GetMirrorConfig(string Mode)
        {
            Type oType = GetService(Mode, "HROMConfigServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHROMConfigService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        internal IHROMDataService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "HROMDataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHROMDataService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        #region Multi Source Data Service
        public IHROMDataService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HROMDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHROMDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHROMConfigService ESSConfig
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HROMConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHROMConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHROMDataService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HROMDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHROMDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IHROMConfigService ERPConfig
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HROMConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHROMConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }


        public IHROMDataService DataService
        {
            get
            {
                Type oType = GetService(DataSource, "HROMDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHROMDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHROMConfigService ConfigService
        {
            get
            {
                Type oType = GetService(DataSource, "HROMConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHROMConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IAuthenService AuthenService
        {
            get
            {
                Type oType = GetService(DataSource, "AuthenServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IAuthenService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
        #endregion Multi Source Data Service

        public string ProvidentFundHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ProvidentFundHeir");
            }
        }
        public string LifeInsuranceHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "LifeInsuranceHeir");
            }
        }
        public string AccidentInsuranceHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "AccidentInsuranceHeir");
            }
        }
        public string Surety
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "Surety");
            }
        }

    }
}