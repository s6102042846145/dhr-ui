﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM
{
   public class PositionDetailCenter
    {
        public string UnitCode { get; set; }
        public string LevelText { get; set; }
        public string CostCenterText { get; set; }
        public string SupervisorName { get; set; }
    }
}
