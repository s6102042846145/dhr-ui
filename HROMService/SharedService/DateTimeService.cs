﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using ESS.SHAREDATASERVICE;

namespace ESS.HR.OM
{
    public class DateTimeService
    {
        public static DateTime ConvertDateTime(string CompanyCode , string strDateTime)
        {
            string formats = ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "MULTIPLEFORMATDATEGET");
            var Dateformats = formats.Split('|');
            DateTime parsedDate;
            DateTime.TryParseExact(strDateTime, Dateformats, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate);
            return parsedDate;
        }
        /// <summary>
        /// ddMMyyyy HH:mm:ss|ddMMyyyy
        /// </summary>
        /// <param name="CompanyCode"></param>
        /// <param name="strDateTime"></param>
        /// <returns></returns>
        public static DateTime ConvertDateTimeURL(string CompanyCode, string strDateTime)
        {
            string formats = ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATURL");
            var Dateformats = formats.Split('|');
            DateTime parsedDate;
            DateTime.TryParseExact(strDateTime, Dateformats, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate);
            return parsedDate;
        }
    }
}
