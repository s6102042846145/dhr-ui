﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM
{
    public class PositionDotedDetailCenter
    {

        public string Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UnitCode { get; set; }
        public string HeadPosCode { get; set; }
        public string PosCode { get; set; }

        public string PositionOfSubordinate { get; set; }
        public string Unit { get; set; }
        public string EmployeeID { get; set; }
        public string HeadOfPosition { get; set; }
        public string HeadOfOrganizationName { get; set; }
        public string ActionType { get; set; }
    }
}
