﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.OM
{
    public class OrganizationCenter
    {
        public OrganizationCenter()
        {
            SuggestOrganizationID = String.Empty;
            BeginDate = DateTime.Now.Date;
            EndDate = DateTime.ParseExact("9999-12-31", "yyyy-MM-dd", null);
            LastActiveDate = DateTime.Now;
            OrderNo = 0;
        }
        public int? ID { get; set; }
        public string OrganizationID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ShortNameEN { get; set; }
        public string ShortNameTH { get; set; }
        public string FullNameEN { get; set; }
        public string FullNameTH { get; set; }
        public string OrganizationLevelID { get; set; }
        public string OrganizationLevelName { get; set; }
        public string OrganizationLevelNameEN { get; set; }
        public string CostCenterID { get; set; }
        public string CostCenterName { get; set; }
        public string CostCenterNameEN { get; set; }
        public string HeadOfOrganizationID { get; set; }
        public string HeadOfOrganizationName { get; set; }
        public string HeadOfOrganizationNameEN { get; set; }
        public string HeadOfPositionName { get; set; }
        public string HeadOfPositionNameEN { get; set; }
        public int OrderNo { get; set; }
        public string CompanyCode { get; set; }
        public DateTime LastActiveDate { get; set; }
        public string SuggestOrganizationID { get; set; } //When Create new OM and UNITID is blank DHR send suggest Unit from service  

    }
}
