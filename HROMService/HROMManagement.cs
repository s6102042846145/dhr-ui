﻿
using ESS.PORTALENGINE;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Data;
using ESS.HR.OM.DATACLASS;
using System.Data.SqlClient;

namespace ESS.HR.OM
{
    public class HROMManagement
    {
        #region Constructor
        private HROMManagement()
        {
        }

        #endregion

        #region MultiCompany  Framework
        private static Dictionary<string, HROMManagement> Cache = new Dictionary<string, HROMManagement>();

        private static string ModuleID = "ESS.HR.OM";
        public string CompanyCode { get; set; }

        public static HROMManagement CreateInstance(string oCompanyCode)
        {
            HROMManagement oHROMManagement = new HROMManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oHROMManagement;
        }
        #endregion MultiCompany  Framework

        #region ORGANIZATION
        public List<OrganizationCenter> getOMUnitByPeriodDate(DateTime oBegdate, DateTime oEndDate)
        {
            string DataSource = HROMServices.SVC(CompanyCode).GET_OMUNIT_BY_PERIODDATE;// "DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.getOMUnitByPeriodDate(oBegdate, oEndDate);
        }
        public List<OrganizationCenter> getOrganizationByOrgID(string oOrgID, string oLang)
        {
            string DataSource = HROMServices.SVC(CompanyCode).GET_ORGANIZATION_BY_ORGID;//"DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getOrganizationByOrgID(oOrgID, oLang);
        }
        public OrganizationCenter getOrganizationDetail(string oOrgID, DateTime oBegDate, DateTime oEndDate)
        {
            string DataSource = HROMServices.SVC(CompanyCode).GET_ORGANIZATIONDATA_BY_ID_BEGDA_ENDDA;//"DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getOrganizationDetail(oOrgID, oBegDate, oEndDate);

        }

        public void SaveOrganizationData(OrganizationCenter Organization, string actionType, string serviceType)
        {
            string DataSource = HROMServices.SVC(CompanyCode).SAVE_ORGANIZATIONDATA;//"DHR";
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SaveOrganizationData(Organization, actionType, serviceType);
        }

        public List<string> getOrganizationChartByHeadOfOrg(string unitCode, string levelCode, string HeadCode, DateTime BeginDate, DateTime EndDate, string Language)
        {
            string DataSource = HROMServices.SVC(CompanyCode).GET_ORGANIZATION_CHART_BY_HEAD_ORG;//"DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getOrganizationChartByHeadOfOrg(unitCode, levelCode, HeadCode, BeginDate, EndDate, Language);
        }
        public DataTable getPriorityListByOrganizationID(string unitCode, DateTime BeginDate, DateTime EndDate, string Language)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getPriorityListByOrganizationID(unitCode, BeginDate, EndDate, Language);
        }
        public string getRootOrganizationUnit()
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getRootOrganizationUnit();
        }
        public OrganizationChief getLeaderByHeadOfOrganization(string oHeadOfOrg, DateTime oBeginDate, DateTime oEndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getLeaderByHeadOfOrganization(oHeadOfOrg, oBeginDate, oEndDate);
        }

        public string getExampleOMUnitID(OrganizationCenter Organization, string oActionType, string oServiceType)
        {
            string DataSource = "DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getExampleOMUnitID(Organization, oActionType, oServiceType);
        }
        #endregion


        #region Get Organization Leve,Unit,Position, Cost center, Head unit
        public List<ControlDropdownlistData> GetOrgLevelDDL(string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetOrgLevelDDL(Langugae, BeginDate, EndDate);
        }

        public List<ControlDropdownlistData> GetOrgUnitDDL(string LevelCode, string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetOrgUnitByOrgLevelDDL(LevelCode, Langugae, BeginDate, EndDate);
        }

        public List<ControlDropdownlistData> GetPositionDDL(string UnitCode, string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPositionByOrgUnitDDL(UnitCode, Langugae, BeginDate, EndDate);
        }
        public List<ControlDropdownlistData> GetPositionUnderSupervisorDDL(string HeadPosCode, string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPositionUnderSupervisorDDL(HeadPosCode, Langugae, BeginDate, EndDate);
        }

        public List<ControlDropdownlistData> GetPositionUnderSupervisorNewDDL(string PosCode, string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPositionUnderSupervisorNewDDL(Langugae, BeginDate, EndDate);
        }
        public List<ControlDropdownlistData> GetCostCenterDDL(string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCostCenterDDL(Langugae, BeginDate, EndDate);
        }
        public List<ControlDropdownlistData> GetHeadUnitDDL(string oOrgLevel, string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetHeadOfOrgUnitDDL(oOrgLevel, Langugae, BeginDate, EndDate);
        }

        public List<ControlDropdownlistData> GetBandDDL(string Langugae, DateTime BeginDate, DateTime EndDate)
        {
            // string DataSource = "DB";
            string DataSource = HROMServices.SVC(CompanyCode).GET_BAND_DLL;//"DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetBandDDL(Langugae, BeginDate, EndDate);
        }
        #endregion

        #region Get Postion Master Data
        public List<PositionCenter> GetPositionDataContent(DateTime StartDate, DateTime EndDate,string LevelCode = null, string UnitCode = null,  string PosCode = null)
        {
            string DataSource = HROMServices.SVC(CompanyCode).UI_GET_OMPOSITION;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPositionDataContent(StartDate, EndDate, LevelCode, UnitCode, PosCode);
        }
        public PositionDetailCenter GetPositionDataDetail(string UnitCode, string Language, DateTime StartDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPositionDataDetail(UnitCode, Language, StartDate, EndDate);
        }
        public List<PositionDotedDetailCenter> GetPositionDotedDetail(string HeadPosition,string PosCode, string Language, DateTime StartDate, DateTime EndDate)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPositionDotedDetail(HeadPosition,PosCode, Language, StartDate, EndDate);
        }
        public List<OrganizationCenter> GetOrganizationDataContent(string LevelCode, string UnitCode, DateTime StartDate, DateTime EndDate)
        {
            string DataSource = HROMServices.SVC(CompanyCode).GET_OM_DATACONTENT; //"DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetOrganizationDataContent(LevelCode, UnitCode, StartDate, EndDate);
        }

        public List<PositionCenter> GetPositionByPosCode(string PosCode, DateTime StartDate, DateTime EndDate)
        {
            string DataSource = HROMServices.SVC(CompanyCode).UI_GET_OMPOSITION;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPositionByPosCode(StartDate, EndDate, PosCode);
        }
        #endregion

        #region CheckMark
        public void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            string DataSource = "DB";
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.MarkUpdate(EmployeeID, DataCategory, RequestNo, isMarkIn);
        }
        public bool CheckMark(string DataCategory)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.CheckMark(DataCategory);
        }
        public bool CheckMark(string DataCategory, string ReqNo)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.CheckMark(DataCategory, ReqNo);
        }

        public List<MarkData> GetMarkDataAll()
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetMarkDataAll();
        }

        public List<MarkData> GetMarkDataWithRequestType()
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetMarkDataWithRequestType();
        }
        #endregion

        #region Get Document
        public DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string language)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetCreatedDocByEmployeeID(EmployeeID, RequestType, language);
        }
        public DataTable GetCreatedDocAll(int RequestTypeID, string language)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetCreatedDocAllByRequestTypeID(RequestTypeID, language);
        }
        public DataTable GetRequestDocumentData(string oRequestNo)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetRequestDocumentData(oRequestNo);
        }
        #endregion

    }
}