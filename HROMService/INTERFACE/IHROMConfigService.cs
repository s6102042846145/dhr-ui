using System;
using System.Collections.Generic;
using System.Data;
using ESS.HR.OM.DATACLASS;

namespace ESS.HR.OM.INTERFACE
{
    public interface IHROMConfigService
    {
        List<OrganizationCenter> getOMUnitByPeriodDate(DateTime oBegdate, DateTime oEndDate);
        List<ControlDropdownlistData> GetSearchContentDDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1 = null);
        List<ControlDropdownlistData> GetOrgLevelDDL(string LanguageCode, DateTime BeginDate, DateTime EndDate);
        List<ControlDropdownlistData> GetOrgUnitByOrgLevelDDL(string LevelCode, string LanguageCode, DateTime BeginDate, DateTime EndDate);
        List<ControlDropdownlistData> GetPositionByOrgUnitDDL(string UnitCode, string LanguageCode, DateTime BeginDate, DateTime EndDate);
        List<ControlDropdownlistData> GetPositionUnderSupervisorDDL(string PosCode, string LanguageCode, DateTime StartDate, DateTime EndDate);
        List<ControlDropdownlistData> GetPositionUnderSupervisorNewDDL(string LanguageCode, DateTime StartDate, DateTime EndDate);
        List<ControlDropdownlistData> GetCostCenterDDL(string LanguageCode, DateTime StartDate, DateTime EndDate);
        List<ControlDropdownlistData> GetHeadOfOrgUnitDDL(string oOrgLevel, string LanguageCode, DateTime StartDate, DateTime EndDate);
        List<ControlDropdownlistData> GetBandDDL(string LanguageCode, DateTime StartDate, DateTime EndDate);
    }
}