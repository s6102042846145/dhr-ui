using System;
using System.Collections.Generic;
using System.Data;
using ESS.HR.OM.DATACLASS;

namespace ESS.HR.OM.INTERFACE
{
    public interface IHROMDataService
    {
        List<OrganizationCenter> getOrganizationByOrgID(string oOrgID, string oLang);
        OrganizationCenter getOrganizationDetail(string oOrgID, DateTime oBegDate, DateTime oEndDate);
        void SaveOrganizationData(OrganizationCenter Organization, string actionType, string serviceType);
        List<string> getOrganizationChartByHeadOfOrg(string unitCode, string levelCode, string HeadCode, DateTime BeginDate, DateTime EndDate, string Language);
        string ValidateOrganizationData(OrganizationCenter Organization, string oActionType, string oServiceType);
        DataTable getPriorityListByOrganizationID(string unitCode, DateTime BeginDate, DateTime EndDate, string Language);
        string getRootOrganizationUnit();
        OrganizationChief getLeaderByHeadOfOrganization(string oHeadOfOrg, DateTime oBeginDate, DateTime oEndDate);
        string getExampleOMUnitID(OrganizationCenter Organization, string oActionType, string oServiceType);
        #region Position
        List<OrganizationCenter> GetOrganizationDataContent(string LevelCode, string UnitCode, DateTime StartDate, DateTime EndDate);
        List<PositionCenter> GetPositionByPosCode(DateTime StartDate, DateTime EndDate, string PosCode);
        List<PositionCenter> GetPositionDataContent(DateTime StartDate, DateTime EndDate, string UnitCode = null, string LevelCode = null, string PosCode = null);
        PositionDetailCenter GetPositionDataDetail(string UnitCode, string Language, DateTime StartDate, DateTime EndDate);
        List<PositionDotedDetailCenter> GetPositionDotedDetail(string HeadPosition, string PosCode, string Language, DateTime StartDate, DateTime EndDate);
        bool SaveWithValidateSolidLineData(PositionCenter oSolidData, string ServiceType, out string oExceptionError, out string oErrorTextCode, out string oErrorParam);
        bool SaveWithValidateDotedLineData(PositionDotedDetailCenter oDotedData, string ServiceType, out string oExceptionError, out string oErrorTextCode, out string oErrorParam);
        #endregion

        #region Mark Data
        List<MarkData> GetMarkDataAll();
        List<MarkData> GetMarkDataWithRequestType();
        void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn);
        bool CheckMark(string DataCategory);
        bool CheckMark(string DataCategory, string ReqNo);
        bool CheckMarkWithWhereCause(string DataCategory, string ReqNo, string oWhereCause);
        #endregion

        #region Document data
        DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string Language);
        DataTable GetCreatedDocAllByRequestTypeID(int RequestTypeID, string Language);
        DataTable GetRequestDocumentData(string oRequestNo);
        #endregion
    }
}