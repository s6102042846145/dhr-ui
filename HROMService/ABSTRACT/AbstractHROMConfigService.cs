using System;
using System.Collections.Generic;
using System.Data;
using ESS.HR.OM.INTERFACE;
using ESS.HR.OM.DATACLASS;

namespace ESS.HR.OM.ABSTRACT
{
    public abstract class AbstractHROMConfigService : IHROMConfigService
    {
        public virtual List<OrganizationCenter> getOMUnitByPeriodDate(DateTime oBegdate, DateTime oEndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<ControlDropdownlistData> GetSearchContentDDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1 = null)
        {
            throw new NotImplementedException();
        }

        public virtual List<ControlDropdownlistData> GetOrgLevelDDL(string LanguageCode, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<ControlDropdownlistData> GetOrgUnitByOrgLevelDDL(string LevelCode, string LanguageCode, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<ControlDropdownlistData> GetPositionByOrgUnitDDL(string UnitCode, string LanguageCode, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }
        public virtual List<ControlDropdownlistData> GetPositionUnderSupervisorDDL(string PosCode, string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }
        public virtual List<ControlDropdownlistData> GetPositionUnderSupervisorNewDDL(string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }
        public virtual List<ControlDropdownlistData> GetCostCenterDDL(string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<ControlDropdownlistData> GetHeadOfOrgUnitDDL(string oOrgLevel, string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<ControlDropdownlistData> GetBandDDL(string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

    }
}