using ESS.DATA;
using ESS.HR.OM.INTERFACE;
using System;
using System.Collections.Generic;
using System.Data;
using ESS.HR.OM.DATACLASS;

namespace ESS.HR.OM.ABSTRACT
{
    public abstract class AbstractHROMDataService : IHROMDataService
    {
        public virtual List<OrganizationCenter> getOrganizationByOrgID(string oOrgID, string oLang)
        {
            throw new NotImplementedException();
        }
        public virtual OrganizationCenter getOrganizationDetail(string oOrgID, DateTime oBegDate, DateTime oEndDate)
        {
            throw new NotImplementedException();
        }
        public virtual void SaveOrganizationData(OrganizationCenter Organization, string actionType, string serviceType)
        {
            throw new NotImplementedException();
        }
        public virtual string getExampleOMUnitID(OrganizationCenter Organization, string oActionType, string oServiceType)
        {
            throw new NotImplementedException();
        }

        #region Position
        public virtual List<PositionCenter> GetPositionByPosCode(DateTime StartDate, DateTime EndDate, string PosCode)
        {
            throw new NotImplementedException();
        }

        public virtual List<PositionCenter> GetPositionDataContent(DateTime StartDate, DateTime EndDate, string LevelCode = null, string UnitCode = null, string PosCode = null)
        {
            throw new NotImplementedException();
        }

        public virtual PositionDetailCenter GetPositionDataDetail(string UnitCode, string Language, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<PositionDotedDetailCenter> GetPositionDotedDetail(string HeadPosition, string PosCode, string Language, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }
        public virtual bool SaveWithValidateSolidLineData(PositionCenter oSolidData, string ServiceType, out string oExceptionError, out string oErrorTextCode, out string oErrorParam)
        {
            throw new NotImplementedException();
        }
        public virtual bool SaveWithValidateDotedLineData(PositionDotedDetailCenter oDotedData, string ServiceType, out string oExceptionError, out string oErrorTextCode, out string oErrorParam)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Organization

        public virtual List<OrganizationCenter> GetOrganizationDataContent(string LevelCode, string UnitCode, DateTime StartDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }
        public virtual List<string> getOrganizationChartByHeadOfOrg(string unitCode, string levelCode, string HeadCode, DateTime BeginDate, DateTime EndDate, string Language)
        {
            throw new NotImplementedException();
        }
        public virtual string ValidateOrganizationData(OrganizationCenter Organization, string oActionType, string oServiceType)
        {
            throw new NotImplementedException();
        }
        public virtual DataTable getPriorityListByOrganizationID(string unitCode, DateTime BeginDate, DateTime EndDate, string Language)
        {
            throw new NotImplementedException();
        }
        public virtual string getRootOrganizationUnit()
        {
            throw new NotImplementedException();
        }
        public virtual OrganizationChief getLeaderByHeadOfOrganization(string oHeadOfOrg, DateTime oBeginDate, DateTime oEndDate)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Mark Data
        public virtual List<MarkData> GetMarkDataAll()
        {
            throw new NotImplementedException();
        }
        public virtual List<MarkData> GetMarkDataWithRequestType()
        {
            throw new NotImplementedException();
        }
        public virtual void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckMark(string DataCategory)
        {
            throw new NotImplementedException();
        }
        public virtual bool CheckMark(string DataCategory, string ReqNo)
        {
            throw new NotImplementedException();
        }
        public virtual bool CheckMarkWithWhereCause(string DataCategory, string ReqNo, string oWhereCause)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Document Data
        public virtual DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string Language)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetCreatedDocAllByRequestTypeID(int RequestTypeID, string Language)
        {
            throw new NotImplementedException();
        }
        public virtual DataTable GetRequestDocumentData(string oRequestNo)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}