﻿using DHR.HR.API.Model;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DESS.JOB
{
    public class JobOrgServiceImpl : IJobOrgService
    {
        #region Constructor
        public JobOrgServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = this.BaseConnStr;
        }
        #endregion Constructor

        #region Property

        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        public string CompanyCode { get; set; }
        private static string ModuleID = "DHR.JOB.DB";
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        #endregion Property
        public void SaveAllBelongTo(List<BelongTo> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            try
            {
                foreach (BelongTo item in data)
                {
                    oCommand = new SqlCommand("sp_SaveDHRDataToBelongTo", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_posCode", item.posCode);
                    oCommand.Parameters.AddWithValue("@p_startDate", item.startDate);
                    oCommand.Parameters.AddWithValue("@p_endDate", item.endDate);
                    //oCommand.Parameters.AddWithValue("@p_startDate", DateTime.ParseExact(item.startDate, "ddMMyyyy:HH:mm:ss", null));
                    //oCommand.Parameters.AddWithValue("@p_endDate", DateTime.ParseExact(item.endDate, "ddMMyyyy:HH:mm:ss", null));
                    oCommand.Parameters.AddWithValue("@p_unitCode", item.unitCode);
                    oCommand.ExecuteNonQuery();
                    oCommand.Dispose();
                }
                oTransaction.Commit();
            }
            catch(Exception ex)
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                throw ex;
            }
            oConnection.Close();
        }
    }
}
