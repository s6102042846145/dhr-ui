﻿using DHR.HR.API.Model;
using DHR.HR.API;
using ESS.EMPLOYEE;
using ESS.JOB.INTERFACE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;

namespace DESS.JOB
{
    public class TaskCopyOrganizeData : ITaskWorker
    {
      
        public TaskCopyOrganizeData()
        {

        }
        public string CompanyCode { get; set ; }
        public int TaskID { get ; set ; }

        public string ConfigName { get; set; }

        public string SourceMode { get; set; }


        public string SourceProfile { get; set; }

        public string TargetMode { get; set; }

        public string TargetProfile { get; set; }


        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        private string DefaultEndDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }

        public void Run()
        {
            if(string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            string oWorkAction = string.Format($"Try to copy D-HR data {ConfigName} to DB");
            Console.WriteLine(oWorkAction);
            APIJobOrgManagement oApiJobOrg = APIJobOrgManagement.CreateInstance(CompanyCode);

            switch (ConfigName.ToUpper())
            {
                
                //case "BELONGTO":
                //    var tmpBelongTo = oApiJobOrg.GetAllBelongTo(DefaultBeginDate, DefaultEndDate);
                //    List<BelongTo> belongto = tmpBelongTo == null ? new List<BelongTo>() : tmpBelongTo.Data;
                //    if (belongto != null && belongto.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {belongto.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllBelongTo(belongto, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {belongto.Count()} recode(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        } 
                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

                //case "COMSECONDMENT" :
                //    var tmpComsecondment = oApiJobOrg.GetAllComSecondment(DefaultBeginDate, DefaultEndDate);
                //    List<ComSecondment> comsecondment = tmpComsecondment == null ? new List<ComSecondment>() : tmpComsecondment.Data;
                //    if (comsecondment != null && comsecondment.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {comsecondment.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllComSecondment(comsecondment, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {comsecondment.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        }
                        
                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

                //case "COMPANY":
                //    List<Company> company = oApiJobOrg.GetAllCompany(DefaultBeginDate, DefaultEndDate).Data;
                //    if (company != null && company.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {company.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllCompany(company, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if(string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {company.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);

                //        }  
                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }

                //    break;

                //case "FISCALYEAR":
                //    var tmpFiscalYear = oApiJobOrg.GetAllFiscalYear(DefaultBeginDate, DefaultEndDate);
                //    List<FiscalYear> fiscalyear = tmpFiscalYear == null ? new List<FiscalYear>() : tmpFiscalYear.Data;
                   
                //    if (fiscalyear != null && fiscalyear.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {fiscalyear.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllFiscalYear(fiscalyear, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {fiscalyear.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        }
                        
                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

                //case "POSITION":
                //    var tmpPosition = oApiJobOrg.GetAllPosition(DefaultBeginDate, DefaultEndDate);
                //    List<Position> position = tmpPosition == null ? new List<Position>() : tmpPosition.Data;
                    
                //    if (position != null && position.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {position.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllPosition(position, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {position.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        }  
                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

                //case "REPORTTO":
                //    var tmpReportTo = oApiJobOrg.GetAllReportTo(DefaultBeginDate, DefaultEndDate);
                //    List<ReportTo> reportto = tmpReportTo == null ? new List<ReportTo>() : tmpReportTo.Data;
                //    if (reportto != null && reportto.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {reportto.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllReportTo(reportto, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {reportto.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        }
                        
                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

                //case "UNIT":
                //    var tempUnit = oApiJobOrg.GetAllUnit(DefaultBeginDate, DefaultEndDate);
                //    List<Unit> unit = tempUnit == null ? new List<Unit>() : tempUnit.Data;
                //    if (unit != null && unit.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {unit.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllUnit(unit, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {unit.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        }
                        
                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

                //case "SOLIDLINE":
                //    var tmpSolidLine = oApiJobOrg.GetAllSolidLine(DefaultBeginDate, DefaultEndDate);
                //    List<SolidLine> solidLine = tmpSolidLine == null ? new List<SolidLine>() : tmpSolidLine.Data;
                //    if (solidLine != null && solidLine.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {solidLine.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllSolidLine(solidLine, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {solidLine.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        }

                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

                //case "DOTEDLINE":
                //    var tmpDotedLine = oApiJobOrg.GetAllDotedLine(DefaultBeginDate, DefaultEndDate);
                //    List<DotedLine> dotedLine = tmpDotedLine == null ? new List<DotedLine>() : tmpDotedLine.Data;

                //    if (dotedLine != null && dotedLine.Count() > 0)
                //    {
                //        string ErrMsg = string.Empty;
                //        Console.WriteLine($"Load {dotedLine.Count()} record(s) completed.");
                //        ErrMsg = oApiJobOrg.SaveAllDotedLine(dotedLine, "");
                //        oWorkAction = oWorkAction + " " + ErrMsg;
                //        if (string.IsNullOrEmpty(ErrMsg))
                //        {
                //            Console.WriteLine($"Save {dotedLine.Count()} record(s) completed.");
                //        }
                //        else
                //        {
                //            Console.WriteLine($"Found error {ErrMsg} please check!!");
                //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ErrMsg, ConfigName);
                //        }

                //    }
                //    else
                //    {
                //        Console.WriteLine($"No record To Save");
                //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", ConfigName);
                //    }
                //    break;

              
            }
        }
    }
}
