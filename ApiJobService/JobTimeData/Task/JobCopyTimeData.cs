﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.WORKFLOW;
using ESS.JOB.INTERFACE;
using ESS.JOB.DATACLASS;

namespace DESS.JOB
{
    public class JobCopyTimeData : IJobWorker
    {
        public JobCopyTimeData()
        {

        }
        public List<JobParam> Params { get ; set ; }
        public string CompanyCode { get; set; }

        public List<ITaskWorker> LoadTasks()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            int iTaskid = 1;
            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyTimeData task;

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "CalenHoliday";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "SetupHoliday";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "PlanWorkTime";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "TimeRecType";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "LeaveType";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "WorkingType";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "LeaveQuotaType";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "EmpGroup";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "PersonalArea";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "BreakSchedule";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "DailyWorkSchedule";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "PeriodWorkSchedule";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "WorkSchedule";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "WorkScheduleRule";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "SubsititutionType";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "Leave";
            returnValue.Add(task);

            task = new TaskCopyTimeData();
            task.TaskID = iTaskid++;
            task.ConfigName = "Attendance";
            returnValue.Add(task);

            return returnValue;
        }
    }
}
