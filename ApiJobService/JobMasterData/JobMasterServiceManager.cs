﻿using DESS.API.DB;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class JobMasterServiceManager
    {
        #region Constructor
        public JobMasterServiceManager()
        {

        }
        #endregion Constructor

        #region MultiCompany Framework
        private string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "DHR.JOB";
            }
        }

        public static JobMasterServiceManager CreateInstance(string oCompanyCode)
        {
            JobMasterServiceManager oJobMasterServiceManager = new JobMasterServiceManager()
            {
                CompanyCode = oCompanyCode
             };

            return oJobMasterServiceManager;
        }

        #endregion MultoCompany Framework

        public string TARGETMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TARGETMODE");
            }
        }
        private Type GetService(string Mode , string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format($"DESS.API.{Mode.ToUpper()}");
            string typeName = string.Format($"DESS.API.{Mode.ToUpper()}.{ClassName}");
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);

            return oReturn;
        }

        internal IMasterDataService JobMasterDataService
        {
            get
            {
                Type otype = GetService(TARGETMODE, "MasterDataServiceImpl");
                if (otype == null)
                {
                    return null;
                }
                else
                    return (IMasterDataService)Activator.CreateInstance(otype, CompanyCode);
            }
        }
    }
}
