﻿using DHR.HR.API.Shared;
using DHR.HR.API.Model;
using DHR.HR.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class APIJobMasterDataManagement
    {
        #region Constructor
        public APIJobMasterDataManagement()
        {

        }
        #endregion Constructor
        #region MultiCompany Framework
        private static string ModuleID = "DHR.JOB";
        private string CompanyCode { get; set; }

        public static APIJobMasterDataManagement CreateInstance (string oCompanyCode)
        {
            APIJobMasterDataManagement oAPIJobMasterDataManagement = new APIJobMasterDataManagement()
            {
                CompanyCode = oCompanyCode
            };

            return oAPIJobMasterDataManagement;
        }

        #endregion MultiCompany Framework

        public ResponseModel<MasterDataCode> GetAllMasterDataCode(string startUpdateDate, string endUpdateDate)
        {
            return APIMasterDataManagement.CreateInstance(CompanyCode).GetDHRMasterDataCode(startUpdateDate, endUpdateDate);
        }

        public string SaveAllMasterDataCode(List<MasterDataCode> data)
        {
            return JobMasterServiceManager.CreateInstance(CompanyCode).JobMasterDataService.SaveAllMasterDataCode(data);
        }

        public ResponseModel<MasterDataValue> GetAllMasterDataValue (string startUpdateDate,string endUpdateDate)
        {
            return APIMasterDataManagement.CreateInstance(CompanyCode).GetDHRMasterDataValue(startUpdateDate, endUpdateDate);
        }

        public string SaveAllMasterDataValue(List<MasterDataValue> data)
        {
            return JobMasterServiceManager.CreateInstance(CompanyCode).JobMasterDataService.SaveAllMasterDataValue(data);
        }
    }
}
