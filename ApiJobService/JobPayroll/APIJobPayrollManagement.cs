﻿using DHR.HR.API.Shared;
using DHR.HR.API.Model;
using DHR.HR.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.JOB
{
    public class APIJobPayrollManagement
    {
        #region Constructor
        public APIJobPayrollManagement()
        {

        }
        #endregion Constructor

        #region MultiCompany Framework
        private static string ModuleID = "DHR.JOB";
        private string CompanyCode { get; set; }

        public static APIJobPayrollManagement CreateInstance(string oCompanyCode)
        {
            APIJobPayrollManagement oAPIJobPayrollManagement = new APIJobPayrollManagement()
            {
                CompanyCode = oCompanyCode
            };

            return oAPIJobPayrollManagement;
        }

        #endregion MultiCompany Framework

        public ResponseModel<PYBankMaster> GetAllBankMaster(string startUpdateDate, string endUpdateDate)
        {
            return APIPayrollManagement.CreateInstance(CompanyCode).GetDHRPYBankMaster(startUpdateDate, endUpdateDate);
        }

        public string SaveAllBankMaster(List<PYBankMaster> data)
        {
            return JobPayrollServiceManager.CreateInstance(CompanyCode).JobPayrollDataService.SaveAllBankMaster(data);
        }
    }
}
