using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.HR.PA.ABSTRACT;
using ESS.HR.PA.CONFIG;
using ESS.SHAREDATASERVICE;
using SAP.Connector;
using SAPInterface;

namespace ESS.HR.PA.SAP
{
    public class HRPAConfigServiceImpl : AbstractHRPAConfigService
    {
        #region Constructor
        public HRPAConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.HR.PA.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");
                //if (config == null || config.AppSettings.Settings["SAP_VERSION"] == null)
                //{
                //    return "6";
                //}
                //else
                //{
                //    return config.AppSettings.Settings["SAP_VERSION"].Value;
                //}
            }
        }
        #endregion Member

   


        #region IHRPAConfig Members

        #region " Get "

        #region " GetTitleList "

        public override List<TitleName> GetTitleList(string LanguageCode)
        {
            List<TitleName> oReturn = new List<TitleName>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANRED";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ATEXT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANRLT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = 2");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T522T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                TitleName Item = new TitleName();
                Item.Key = arrTemp[0].Trim();
                Item.Abbreviation = arrTemp[1].Trim();
                Item.Description = arrTemp[2].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetTitleList "

        #region " GetPrefixNameList "

        public override List<PrefixName> GetPrefixNameList()
        {
            List<PrefixName> oReturn = new List<PrefixName>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TTOUT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TITEL";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            if (Sap_Version == "6")
            {
                opt.Text = string.Format("ART = 'Z'");
            }
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T535N", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            // add blank item
            PrefixName Item;
            Item = new PrefixName();
            Item.Key = "";
            Item.Description = "-";
            oReturn.Add(Item);
            Dictionary<string, string> dict = new Dictionary<string, string>();
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = arrTemp[0].Trim();
                string description = arrTemp[1].Trim();

                if (!dict.ContainsKey(key))
                {
                    dict[key] = description;
                    Item = new PrefixName();
                    Item.Key = key;
                    Item.Description = description;
                    oReturn.Add(Item);
                }
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetPrefixNameList "

        #region " GetNameFormatList "

        public override List<NameFormat> GetNameFormatList()
        {
            List<NameFormat> oReturn = new List<NameFormat>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KNZNM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LFDNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FNAME";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("MOLGA = '26' AND FORMT = '01' AND PREFX = '0002'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T522N", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            NameFormat Item = null;
            Dictionary<string, NameFormat> Dic = new Dictionary<string, NameFormat>();
            SortedList<int, string> mapfields = new SortedList<int, string>();
            Dictionary<string, string> Mapping = new Dictionary<string, string>();
            Mapping.Add("ANRED", "Title");
            Mapping.Add("VORNA", "FirstName");
            Mapping.Add("NACHN", "LastName");
            Mapping.Add("TITEL", "Prefix");
            Mapping.Add("NAMZU", "Prefix");
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                if (Dic.ContainsKey(arrTemp[0]))
                {
                    Item = Dic[arrTemp[0]];
                }
                else
                {
                    if (Item != null)
                    {
                        Item.Fields.AddRange(mapfields.Values);
                    }
                    mapfields = new SortedList<int, string>();
                    Item = new NameFormat();
                    oReturn.Add(Item);
                    Item.Key = arrTemp[0];
                    Dic.Add(Item.Key, Item);
                }
                if (Mapping.ContainsKey(arrTemp[2]))
                    mapfields.Add(int.Parse(arrTemp[1]), Mapping[arrTemp[2]]);
            }
            if (Item != null)
            {
                Item.Fields.AddRange(mapfields.Values);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetNameFormatList "

        #region " GetGenderList "

        public override List<Gender> GetGenderList()
        {
            List<Gender> oReturn = new List<Gender>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DDTEXT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DOMVALUE_L";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("DOMNAME = 'GESCH' AND DDLANGUAGE = 'E'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND DOMVALUE_L NE '' ");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "DD07T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            // add blank item
            Gender Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new Gender();
                Item.Key = arrTemp[1].Trim();
                Item.Description = arrTemp[0].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetGenderList "

        #region " GetCountryList "

        public override List<Country> GetCountryList()
        {
            List<Country> oReturn = new List<Country>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LANDX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATIO";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRAS = 2");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T005T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            // add blank item
            Country Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new Country();
                Item.CountryCode = arrTemp[0].Trim();
                Item.CountryName = arrTemp[1].Trim();
                Item.Nationality = arrTemp[2].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetCountryList "

        #region " GetMaritalStatusList "

        public override List<MaritalStatus> GetMaritalStatusList(string LanguageCode)
        {
            List<MaritalStatus> oReturn = new List<MaritalStatus>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMST";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FTEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = 'E'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T502T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            // add blank item
            MaritalStatus Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new MaritalStatus();
                Item.Key = arrTemp[0].Trim();
                Item.Description = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetMaritalStatusList "

        #region " GetReligionList "

        public override List<Religion> GetReligionList(string LanguageCode)
        {
            List<Religion> oReturn = new List<Religion>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KONFE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KTEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = '2'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND KONFE >= '90'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T516T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            // add blank item
            Religion Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new Religion();
                Item.Key = arrTemp[0].Trim();
                Item.Description = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetReligionList "

        #region " GetLanguageList "

        public override List<Language> GetLanguageList()
        {
            List<Language> oReturn = new List<Language>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " T002 "

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPRAS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAISO";
            Fields.Add(fld);

            #endregion " Fields "

            oFunction.Rfc_Read_Table("^", "", "T002", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            Language Item;
            Dictionary<string, Language> oDic = new Dictionary<string, Language>();
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                if (!oDic.ContainsKey(arrTemp[0].Trim()))
                {
                    Item = new Language();
                    Item.Key = arrTemp[0].Trim();
                    Item.Code = arrTemp[1].Trim();
                    oDic.Add(Item.Key, Item);
                    oReturn.Add(Item);
                }
            }

            #endregion " ParseObject "

            #endregion " T002 "

            #region " T002T "

            Data = new TAB512Table();
            Fields = new RFC_DB_FLDTable();
            Options = new RFC_DB_OPTTable();

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPRSL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPTXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRAS = 'E'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T002T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = oDic[arrTemp[0].Trim()];
                Item.Description = arrTemp[1].Trim();
            }

            #endregion " ParseObject "

            #endregion " T002T "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetLanguageList "

        #region " GetAddressTypeList "

        public override List<AddressType> GetAddressTypeList(string LanguageCode)
        {
            List<AddressType> oReturn = new List<AddressType>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = 'E'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND INFTY = '0006'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T591S", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                AddressType Item = new AddressType();
                Item.Key = arrTemp[0].Trim();
                Item.Description = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAddressTypeList "

        #region " GetBankList "

        public override List<Bank> GetBankList(string LanguageCode)
        {
            List<Bank> oReturn = new List<Bank>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BANKL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BANKA";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("BANKS = 'TH'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "BNKA", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            // add blank item
            Bank Item;
            List<string> lstKey = new List<string>();
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                if (!lstKey.Contains(arrTemp[0].Trim()))
                {
                    Item = new Bank();
                    Item.Key = arrTemp[0].Trim();
                    Item.Description = arrTemp[1].Trim();
                    oReturn.Add(Item);
                    lstKey.Add(Item.Key);
                }
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetBankList "

        #region " GetFamilyMemberList "

        public override List<FamilyMember> GetFamilyMemberList()
        {
            List<FamilyMember> returnValue = new List<FamilyMember>();
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            TAB512Table data2 = new TAB512Table();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;
            //key
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            //description
            fld = new RFC_DB_FLD();
            fld.Fieldname = "STEXT";
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields2.Add(fld);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("INFTY = '0021'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = '2' AND INFTY = '0021'");
            Options2.Add(opt);

            oFunction.Rfc_Read_Table("^", "", "T591A", 0, 0, ref data, ref Fields, ref Options);
            oFunction.Rfc_Read_Table("^", "", "T591S", 0, 0, ref data2, ref Fields2, ref Options2);
            FamilyMember fMember;

            Dictionary<string, string> descriptionList = new Dictionary<string, string>();
            foreach (TAB512 item2 in data2)
            {
                string[] buffer2 = item2.Wa.Split('^');
                descriptionList.Add(buffer2[1], item2.Wa);//subty -> t591s
            }

            foreach (TAB512 item in data)
            {
                string[] buffer = item.Wa.Split('^');
                fMember = new FamilyMember();
                fMember.Key = buffer[0].Trim(); //subty -> t591a
                if (descriptionList.ContainsKey(fMember.Key))
                {
                    string[] buffer2 = descriptionList[fMember.Key].Split('^');
                    fMember.Description = buffer2[0].Trim(); //stext -> t591s
                }
                returnValue.Add(fMember);
            }
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return returnValue;
        }

        #endregion " GetFamilyMemberList "

        #region " GetAllEducationLevel "

        public override List<EducationLevel> GetAllEducationLevel()
        {
            List<EducationLevel> oReturn = new List<EducationLevel>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLART";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = 2 AND SLART LIKE 'Z%'"); // PTT used Z%
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T517T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            EducationLevel Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new EducationLevel();
                Item.EducationLevelCode = arrTemp[0].Trim();
                Item.EducationLevelText = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllEducationLevel "

        #region " GetAllCertificate "

        public override List<Certificate> GetAllCertificate()
        {
            List<Certificate> oReturn = new List<Certificate>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLABS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = 2");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T519T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            Certificate Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new Certificate();
                Item.CertificateCode = arrTemp[0].Trim();
                Item.CertificateDescription = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllCertificate "

        #region " GetAllVocation "

        public override List<Vocation> GetAllVocation()
        {
            List<Vocation> oReturn = new List<Vocation>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "AUSBI";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ATEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("LANGU = 2");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T518B", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            Vocation Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new Vocation();
                Item.VocationCode = arrTemp[0].Trim();
                Item.VocationText = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllVocation "

        #region " GetAllInstitute "

        public override List<Institute> GetAllInstitute()
        {
            List<Institute> oReturn = new List<Institute>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            //TAB512Table Data1 = new TAB512Table();
            //RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
            //RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "AUSBI";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ATEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("LANGU = 2 AND AUSBI >= 9000000");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T518B", 0, 0, ref Data, ref Fields, ref Options);
            //oFunction.Zrfc_Read_Table("^", "", "T5J65T", 0, 0, ref Data1, ref Fields1, ref Options1);

            #region " ParseObject "

            Institute Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = string.Format("{0}#{1}", arrTemp[0].Trim(), arrTemp[1].Trim());
                Item = new Institute();
                Item.EducationLevelCode = "*";
                Item.InstituteCode = arrTemp[0].Trim();
                Item.InstituteText = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllInstitute "

        #region " GetAllHonor "

        public override List<Honor> GetAllHonor()
        {
            List<Honor> oReturn = new List<Honor>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FACCD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FACUL";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRSL = 2");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T5J68T", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            Honor Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new Honor();
                Item.HonorCode = arrTemp[0].Trim();
                Item.HonorText = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllHonor "

        #region " GetAllBranch "

        public override List<Branch> GetAllBranch()
        {
            List<Branch> oReturn = new List<Branch>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            TAB512Table Data1 = new TAB512Table();
            RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLART";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAART";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAART";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FTEXT";
            Fields1.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("LANGU = 2");
            Options1.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T517Z", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "T517X", 0, 0, ref Data1, ref Fields1, ref Options1);

            Dictionary<string, string> mapping = new Dictionary<string, string>();
            foreach (TAB512 data in Data1)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = string.Format("{0}", arrTemp[0].Trim());
                mapping.Add(key, arrTemp[1].Trim());
            }

            #region " ParseObject "

            Branch Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = string.Format("{0}", arrTemp[1].Trim());
                Item = new Branch();
                Item.EducationLevelCode = arrTemp[0].Trim();
                Item.BranchCode = arrTemp[1].Trim();
                if (mapping.ContainsKey(key))
                {
                    Item.BranchText = mapping[key];
                }
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllBranch "

        #region " GetAllCompanyBusiness "

        public override List<CompanyBusiness> GetAllCompanyBusiness()
        {
            List<CompanyBusiness> oReturn = new List<CompanyBusiness>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            TAB512Table Data1 = new TAB512Table();
            RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BRSCH";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BRSCH";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BRTXT";
            Fields1.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRAS = 2");
            Options1.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T016", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "T016T", 0, 0, ref Data1, ref Fields1, ref Options1);

            Dictionary<string, string> mapping = new Dictionary<string, string>();
            foreach (TAB512 data in Data1)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = string.Format("{0}", arrTemp[0].Trim());
                mapping.Add(key, arrTemp[1].Trim());
            }

            #region " ParseObject "

            CompanyBusiness Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = string.Format("{0}", arrTemp[0].Trim());
                Item = new CompanyBusiness();
                Item.BusinessCode = arrTemp[0].Trim();
                if (mapping.ContainsKey(key))
                {
                    Item.BusinessText = mapping[key];
                }
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllCompanyBusiness "

        #region " GetAllJobType "

        public override List<JobType> GetAllJobType()
        {
            List<JobType> oReturn = new List<JobType>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TAETE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LTEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SPRAS = 2");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "T513C", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            JobType Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new JobType();
                Item.JobCode = arrTemp[0].Trim();
                Item.JobText = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllJobType "

        #region " GetAllEducationLevel "

        public override List<Relationship> GetAllRelationship()
        {
            List<Relationship> oReturn = new List<Relationship>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLART";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ABART";
            Fields.Add(fld);

            #endregion " Fields "

            oFunction.Rfc_Read_Table("^", "", "T517A", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            Relationship Item;
            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                Item = new Relationship();
                Item.EducationLevel = arrTemp[0].Trim();
                Item.CertificateCode = arrTemp[1].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetAllEducationLevel "

        #endregion " Get "

        #endregion IHRPAConfig Members
    }
}