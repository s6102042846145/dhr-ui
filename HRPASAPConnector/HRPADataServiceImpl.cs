using ESS.DATA;
using ESS.HR.PA.ABSTRACT;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using SAP.Connector;
using SAPInterface;
using SAPInterfaceExt;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ESS.HR.PA.SAP
{
    public class HRPADataServiceImpl : AbstractHRPADataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        #region Constructor
        public HRPADataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.HR.PA.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");

                //if (config == null || config.AppSettings.Settings["SAP_VERSION"] == null)
                //{
                //    return "6";
                //}
                //else
                //{
                //    return config.AppSettings.Settings["SAP_VERSION"].Value;
                //}
            }
        }

        #endregion Member


        #region " PersonalData "

        #region " GetPersonalData / PersonalInfoCenter  "
        //public override INFOTYPE0002 GetINFOTYPE0002(string EmployeeID, DateTime CheckDate)
        public override PersonalInfoCenter GetPersonalInfo(string EmployeeID, DateTime CheckDate)
        {
            List<PersonalInfoCenter> list = this.GetPersonalInfo(EmployeeID, EmployeeID, CheckDate);
            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }

        #endregion " GetPersonalData "

        #region " GetINFOTYPE0002/PersonalInfoCenter "
        //private List<INFOTYPE0002> GetINFOTYPE0002(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        private List<PersonalInfoCenter> GetPersonalInfo(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<PersonalInfoCenter> oReturn = new List<PersonalInfoCenter>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANRED";
            Fields.Add(fld);

            if (Sap_Version == "6")
            {
                fld = new RFC_DB_FLD();
                fld.Fieldname = "NAMZU";
                Fields.Add(fld);
            }
            else
            {
                fld = new RFC_DB_FLD();
                fld.Fieldname = "TITEL";
                Fields.Add(fld);
            }

            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORNA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NACHN";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "INITS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "RUFNM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KNZNM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBDAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GESCH";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBORT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBLND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATIO";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMST";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZKD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KONFE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPRSL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBDEP";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBORT";

            //Add By Kiattiwat 13-02-2012
            //22
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TITEL";
            Fields.Add(fld);

            //23
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TITL2";
            Fields.Add(fld);

            //24
            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORSW";
            Fields.Add(fld);

            //25
            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORS2";
            Fields.Add(fld);

            //26
            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATI2";
            Fields.Add(fld);

            //27
            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATI3";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            #endregion " Options "
            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0002", 0, 0, ref Data, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }


            #region " Parse Object "

            PersonalInfoCenter data;
            foreach (TAB512 item in Data)
            {
                data = new PersonalInfoCenter();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "ANRED";
                data.TitleID = arrTemp[4].Trim();
                //fld.Fieldname = "TITEL";
                data.Prefix = arrTemp[5].Trim();
                //fld.Fieldname = "VORNA";
                data.FirstName = arrTemp[6].Trim();
                //fld.Fieldname = "NACHN";
                data.LastName = arrTemp[7].Trim();
                //fld.Fieldname = "INITS";
                data.Initial = arrTemp[8].Trim();
                //fld.Fieldname = "RUFNM";
                data.NickName = arrTemp[9].Trim();
                //fld.Fieldname = "KNZNM";
                data.NameFormat = arrTemp[10].Trim();
                //fld.Fieldname = "GBDAT";
                data.DOB = DateTime.ParseExact(arrTemp[11], "yyyyMMdd", oCL);
                //fld.Fieldname = "GESCH";
                data.Gender = arrTemp[12].Trim();
                //fld.Fieldname = "GBORT";
                data.BirthPlace = arrTemp[13].Trim();
                //fld.Fieldname = "GBLND";
                data.BirthCity = arrTemp[14].Trim();
                //fld.Fieldname = "NATIO";
                data.Nationality = arrTemp[15].Trim();
                //fld.Fieldname = "FAMST";
                data.MaritalStatus = arrTemp[16].Trim();
                //fld.Fieldname = "FAMDT";
                if (arrTemp[17].Trim() == "00000000")
                {
                    data.MaritalDate = DateTime.MinValue;
                }
                else
                {
                    data.MaritalDate = DateTime.ParseExact(arrTemp[17], "yyyyMMdd", oCL);
                }
                //fld.Fieldname = "ANZKD";
                data.NoOfChild = int.Parse(arrTemp[18].Trim());
                //fld.Fieldname = "KONFE";
                data.Religion = arrTemp[19].Trim();
                //fld.Fieldname = "SPRSL";
                data.Language = arrTemp[20].Trim();
                //Add By Kiattiwat 13-02-2012
                //fld.Fieldname = "TITEL";
                data.MilitaryTitle = arrTemp[22].Trim();
                //fld.Fieldname = "TITL2";
                data.SecondTitle = arrTemp[23].Trim();
                //fld.Fieldname = "VORSW";
                data.AcademicTitle = arrTemp[24].Trim();
                //fld.Fieldname = "VORS2";
                data.MedicalTitle = arrTemp[25].Trim();
                //fld.Fieldname = "NATI2";
                data.SecondNationality = arrTemp[26].Trim();
                //fld.Fieldname = "NATI3";
                data.ThirdNationality = arrTemp[27].Trim();
                oReturn.Add(data);
            }

            #endregion " Parse Object "

            return oReturn;
        }

        #endregion " GetINFOTYPE0002 "

        #region " GetINFOTYPE0002List "

        //public override List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2)
        //{
        //    return GetINFOTYPE0002(EmployeeID1, EmployeeID2, DateTime.Now);
        //}

        #endregion " GetINFOTYPE0002List "

        #region " SaveINFOTYPE0002 "

        //public override void SaveINFOTYPE0002(INFOTYPE0002 data)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
        //    oFunction.Connection = oConnection;

        //    ZHRHRS001Table Updatetab = new ZHRHRS001Table();

        //    ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

        //    ZHRHRS001 item = new ZHRHRS001();
        //    item.Begda = data.BeginDate.ToString("yyyyMMdd", oCL);
        //    item.Endda = data.EndDate.ToString("yyyyMMdd", oCL);
        //    item.Infty = data.InfoType;
        //    item.Msg = "";
        //    item.Msgtype = "";
        //    item.Objps = "";
        //    item.Operation = "INS";
        //    item.Pernr = data.EmployeeID;
        //    item.Reqnr = Guid.NewGuid().ToString();
        //    item.Seqnr = "";
        //    item.Status = "N";
        //    item.Subty = data.SubType;
        //    item.T01 = data.TitleID;
        //    item.T02 = data.Prefix;
        //    item.T03 = data.FirstName;
        //    item.T04 = data.LastName;
        //    item.T05 = data.Initial;
        //    item.T06 = data.NickName;
        //    item.T07 = data.NameFormat;
        //    item.T08 = data.DOB.ToString("yyyyMMdd", oCL);
        //    item.T09 = data.Gender;
        //    item.T10 = data.BirthPlace;
        //    item.T11 = data.BirthCity;
        //    item.T12 = data.Nationality;
        //    item.T13 = data.MaritalStatus;
        //    if (data.MaritalEffectiveDate == DateTime.MinValue)
        //    {
        //        item.T14 = "00000000";
        //    }
        //    else
        //    {
        //        item.T14 = data.MaritalEffectiveDate.ToString("yyyyMMdd", oCL);
        //    }
        //    item.T15 = data.NoOfChild.ToString();
        //    item.T16 = data.Religion;
        //    item.T17 = data.Language;
        //    //Add By Kiattiwat 13-02-2012
        //    item.T18 = data.MilitaryTitle;
        //    item.T19 = data.NamePrefix1;
        //    item.T20 = data.NamePrefix2;
        //    item.T21 = data.SecondTitle;
        //    item.T22 = data.SecondNationality;
        //    item.T23 = data.ThirdNationality;
        //    Updatetab.Add(item);

        //    oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

        //    Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    bool lError = false;
        //    ZHRHRS001 result;
        //    if (Updatetab_Ret.Count > 0)
        //    {
        //        result = Updatetab_Ret[0];
        //        if (result.Msgtype == "E")
        //        {
        //            data.Remark = result.Msg;
        //            lError = true;
        //        }
        //    }
        //    if (lError)
        //    {
        //        throw new Exception("Post data error");
        //    }
        //}

        #endregion " SavePersonalData "

        #endregion " PersonalData "

        #region " PersonalAddress "

        #region " GetInfotype0006Data "

        private List<PersonalAddressCenter> GetInfotype0006Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<PersonalAddressCenter> oReturn = new List<PersonalAddressCenter>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TELNR";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", CheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND SUBTY <> '9'");
            Options.Add(opt);

            #endregion " Options "
            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0006", 0, 0, ref Data, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            #region " Parse Object "

            PersonalAddressCenter data;
            foreach (TAB512 item in Data)
            {
                data = new PersonalAddressCenter();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "STRAS";
                data.AddressNo = arrTemp[4].Trim();
                //fld.Fieldname = "LOCAT";
                data.Street = arrTemp[5].Trim();
                //fld.Fieldname = "ORT02";
                data.District = arrTemp[6].Trim();
                //fld.Fieldname = "ORT01";
                data.Province = arrTemp[7].Trim();
                //fld.Fieldname = "PSTLZ";
                data.Postcode = arrTemp[8].Trim();
                //fld.Fieldname = "LAND1";
                data.Country = arrTemp[9].Trim();
                //fld.Fieldname = "TELNR";
                data.Telephone = arrTemp[10].Trim();
                oReturn.Add(data);
            }

            #endregion " Parse Object "

            return oReturn;
        }

        #endregion " GetInfotype0006Data "

        #region " GetPersonalAddress "

        public override List<PersonalAddressCenter> GetPersonalAddress(string EmployeeID, DateTime CheckDate)
        {
            List<PersonalAddressCenter> list = this.GetInfotype0006Data(EmployeeID, EmployeeID, CheckDate);
            return PopulateMissingAddress(EmployeeID, list);
        }

        private List<PersonalAddressCenter> PopulateMissingAddress(string EmployeeID, List<PersonalAddressCenter> addresses)
        {
            Dictionary<string, int> addressdict = new Dictionary<string, int>();
            foreach (PersonalAddressCenter item in addresses)
            {
                addressdict[item.AddressType] = 1;
            }

            // populate the missing address type
            if (!addressdict.ContainsKey("1"))
            {
                // populate �������������¹��ҹ
                PersonalAddressCenter regisAddress = new PersonalAddressCenter();
                regisAddress.AddressType = "1";
                regisAddress.EmployeeID = EmployeeID;
                addresses.Add(regisAddress);
            }
            if (!addressdict.ContainsKey("3"))
            {
                // populate �������Ѩ�غѹ
                PersonalAddressCenter currentAddress = new PersonalAddressCenter();
                currentAddress.AddressType = "3";
                currentAddress.EmployeeID = EmployeeID;
                addresses.Add(currentAddress);
            }
            return addresses;
        }

        #endregion " GetPersonalAddress "

        #region " SavePersonalAddress "

        public override void SavePersonalAddress(List<PersonalAddressCenter> dataList)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "

            foreach (PersonalAddressCenter data in dataList)
            {
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = data.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = data.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = data.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                if (data.IsDelete)
                {
                    item.Operation = "DEL";
                }
                else
                {
                    item.Operation = "INS";
                }
                item.Pernr = data.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = data.SubType;
                // STRAS
                item.T01 = data.AddressNo;
                item.T02 = "";
                //if (data.AddressNo.Length > 40)
                //{
                //    item.T01 = data.AddressNo.Substring(0, 40);
                //    //item.T02 = data.AddressNo.Substring(40);
                //}
                //else
                //{
                //    item.T01 = data.AddressNo;
                //    //item.T02 = "";
                //}
                //LOCAT
                item.T03 = data.Street;
                //ORT2
                item.T04 = data.District;
                //ORT1
                item.T05 = data.Province;
                //PSTLZ
                if (data.Postcode != "")
                {
                    item.T06 = data.Postcode.Trim().PadRight(5, '0');
                }
                //LAND1;
                item.T07 = data.Country;
                //TELNR;
                item.T08 = data.Telephone;
                Updatetab.Add(item);
            }

            #endregion " set item "

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    dataList[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        #endregion " SavePersonalAddress "

        #endregion " PersonalAddress "

        #region " BankDetail "

        #region " GetInfotype0009Data "

        private List<INFOTYPE0009> GetInfotype0009Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0009> oReturn = new List<INFOTYPE0009>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BANKL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BANKN";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "EMFTX";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", CheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0009", 0, 0, ref Data, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            #region " Parse Object "

            INFOTYPE0009 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE0009();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "BANKL";
                data.Bank = arrTemp[4].Trim();
                //fld.Fieldname = "BANKN";
                data.AccountNo = arrTemp[5].Trim();
                //fld.Fieldname = "EMFTX";
                data.AccountName = arrTemp[6].Trim();
                if (data.AccountName == "")
                {
                    EMPLOYEE.EmployeeData oEmp = new EMPLOYEE.EmployeeData(data.EmployeeID, "", CompanyCode, DateTime.Now);
                    data.AccountName = oEmp.Name;
                }
                oReturn.Add(data);
            }

            #endregion " Parse Object "

            return oReturn;
        }

        //private List<PersonalAccountCenter> GetPersonalBankDetailData(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<PersonalAccountCenter> oReturn = new List<PersonalAccountCenter>();

        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    RFCREADTABLE6 oFunction = new RFCREADTABLE6();
        //    oFunction.Connection = oConnection;

        //    TAB512Table Data = new TAB512Table();
        //    RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

        //    RFC_DB_FLD fld;
        //    RFC_DB_OPT opt;

        //    #region " Fields "

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "PERNR";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "SUBTY";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "BEGDA";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "ENDDA";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "BANKL";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "BANKN";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "EMFTX";
        //    Fields.Add(fld);

        //    #endregion " Fields "

        //    #region " Options "

        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
        //    Options.Add(opt);
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", CheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
        //    Options.Add(opt);

        //    #endregion " Options "

        //    try
        //    {
        //        oFunction.Zrfc_Read_Table("^", "", "PA0009", 0, 0, ref Data, ref Fields, ref Options);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    finally
        //    {
        //        Connection.ReturnConnection(oConnection);
        //        oFunction.Dispose();
        //    }

        //    #region " Parse Object "

        //    PersonalAccountCenter data;
        //    foreach (TAB512 item in Data)
        //    {
        //        data = new PersonalAccountCenter();
        //        string[] arrTemp = item.Wa.Split('^');
        //        //fld.Fieldname = "PERNR";
        //        data.EmployeeID = arrTemp[0].Trim();
        //        //fld.Fieldname = "SUBTY";
        //        data.SubType = arrTemp[1].Trim();
        //        //fld.Fieldname = "BEGDA";
        //        data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
        //        //fld.Fieldname = "ENDDA";
        //        data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
        //        //fld.Fieldname = "BANKL";
        //        data.Bank = arrTemp[4].Trim();
        //        //fld.Fieldname = "BANKN";
        //        data.AccountNo = arrTemp[5].Trim();
        //        //fld.Fieldname = "EMFTX";
        //        data.AccountName = arrTemp[6].Trim();
        //        if (data.AccountName == "")
        //        {
        //            EMPLOYEE.EmployeeData oEmp = new EMPLOYEE.EmployeeData(data.EmployeeID, "", CompanyCode, DateTime.Now);
        //            data.AccountName = oEmp.Name;
        //        }
        //        oReturn.Add(data);
        //    }

        //    #endregion " Parse Object "

        //    return oReturn;
        //}

        #endregion " GetInfotype0009Data "

        #region " GetBankDetail "

        //public override INFOTYPE0009 GetBankDetail(string EmployeeID, DateTime CheckDate)
        //{
        //    List<INFOTYPE0009> list = this.GetInfotype0009Data(EmployeeID, EmployeeID, CheckDate);
        //    if (list.Count > 0)
        //    {
        //        return list[0];
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
        //public override PersonalAccountCenter GetPersonalBankDetail(string EmployeeID, DateTime CheckDate)
        //{
        //    List<PersonalAccountCenter> list = this.GetPersonalBankDetailData(EmployeeID, EmployeeID, CheckDate);
        //    if (list.Count > 0)
        //    {
        //        return list[0];
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
        #endregion " GetBankDetail "

        #region " SaveBankDetail "

        //public override void SaveBankDetail(INFOTYPE0009 data)
        //public override void SavePersonalBankDetail(PersonalAccountCenter data)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();

        //    oFunction.Connection = oConnection;

        //    ZHRHRS001Table Updatetab = new ZHRHRS001Table();

        //    ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

        //    #region " set item "

        //    ZHRHRS001 item = new ZHRHRS001();

        //    item.Begda = data.BeginDate.ToString("yyyyMMdd", oCL);
        //    item.Endda = data.EndDate.ToString("yyyyMMdd", oCL);
        //    item.Infty = data.InfoType;
        //    item.Msg = "";
        //    item.Msgtype = "";
        //    item.Objps = "";
        //    item.Operation = "INS";
        //    item.Pernr = data.EmployeeID;
        //    item.Reqnr = Guid.NewGuid().ToString();
        //    item.Seqnr = "";
        //    item.Status = "N";
        //    item.Subty = "0"; // FIX : MAIN BANK
        //    //BANK COUNTRY : banks
        //    item.T01 = "TH"; // FIX : Thai bank
        //    //BANK KEY : bankl
        //    item.T02 = data.Bank;
        //    //ACCOUNT NO : bankn
        //    item.T03 = data.AccountNo;
        //    //ACCOUNT NAME : emftx
        //    EMPLOYEE.EmployeeData oEmp = new EMPLOYEE.EmployeeData(data.EmployeeID, "", CompanyCode, DateTime.Now);
        //    if (data.AccountName.Trim() == oEmp.Name.Trim())
        //    {
        //        item.T04 = "";
        //    }
        //    else
        //    {
        //        item.T04 = data.AccountName;
        //    }
        //    //Payment method : zlsch
        //    item.T05 = "Y"; // FIX : HR: Transfer
        //    Updatetab.Add(item);

        //    #endregion " set item "

        //    oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

        //    Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    bool lError = false;
        //    ZHRHRS001 result;
        //    if (Updatetab_Ret.Count > 0)
        //    {
        //        result = Updatetab_Ret[0];
        //        if (result.Msgtype == "E")
        //        {
        //            data.Remark = result.Msg;
        //            lError = true;
        //        }
        //    }

        //    if (lError)
        //    {
        //        throw new Exception("Post data error");
        //    }
        //}

        #endregion " SaveBankDetail "

        #endregion " BankDetail "

        #region " Family "

        public override void DeleteFamilyData(List<INFOTYPE0021> data)
        {
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            foreach (INFOTYPE0021 info21 in data)
            {
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = info21.BeginDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Endda = info21.EndDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Infty = info21.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = info21.ChildNo;
                item.Operation = "MOD"; // MOD
                item.Pernr = info21.EmployeeID;
                //�繡������Ţ����ͧ�Ҥӹǳ�������ͧ�ѹ�����ū�ӡѹ ����ѹ�� gen key �������շҧ��ӡѹ�����
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = info21.FamilyMember;
                //T ��ҧ � ����Ҩҡ function �������� sap se37
                item.T01 = info21.FamilyMember;
                item.T02 = info21.TitleRank;
                item.T03 = info21.Name;
                item.T04 = info21.Surname;
                if (info21.BirthDate == DateTime.MinValue)
                {
                    item.T05 = "00000000";
                }
                else
                {
                    item.T05 = info21.BirthDate.ToString("yyyyMMdd", DefaultCultureInfo);
                }
                item.T06 = info21.Sex;
                item.T07 = info21.BirthPlace;
                item.T08 = info21.CityOfBirth;
                item.T09 = info21.Nationality;
                item.T10 = info21.FatherID;
                item.T11 = info21.MotherID;
                item.T12 = info21.SpouseID;
                item.T13 = info21.FatherSpouseID;
                item.T14 = info21.MotherSpouseID;
                item.T15 = info21.TitleName;
                item.T16 = info21.Employer;
                item.T17 = info21.JobTitle;
                item.T18 = info21.Dead;
                item.T19 = info21.Address;
                item.T20 = info21.Street;
                item.T21 = info21.District;
                item.T22 = info21.City;
                item.T23 = info21.Postcode;
                item.T24 = info21.Country;
                item.T25 = info21.TelephoneNo;
                Updatetab.Add(item);
            }
            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }
        //public override List<INFOTYPE0021> GetFamilyData(string EmployeeID)
        public override List<PersonalFamilyCenter> GetPersonalFamilyData(string EmployeeID)
        {
            List<PersonalFamilyCenter> returnValue = new List<PersonalFamilyCenter>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table oTable2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt, opt2;
            RFC_DB_FLD fld, fld2;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMSA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FNMZU";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAVOR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FASEX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBOT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBLD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            //Get Begda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //Get Endda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            #region INF0187

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "FTXID"; //Identity Number for Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "MTXID"; //Identity Number for Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "STXID"; //Identity Number for Spouse
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SFXID"; //Identity Number for Spouse's Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SMXID"; //Identity Number for Spouse's Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "ANRED"; //Form-of-Address Key
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "EFAML"; //Employer of Family Member (TH)
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "JOBTL"; //Job Title
            Fields2.Add(fld2);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY"; //Subtype
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS"; //Object Identification
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALDEC"; //Deceased Indicator
            Fields2.Add(fld);

            //Addby:Nuttapon.a 11.02.2013 adress
            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS"; // Address Number, Soi Trok (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT"; //Street/Road, Tambol
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02"; // District (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01"; // Province
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ"; // Postal Code
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1"; // Country Key
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TELNR"; // Telephone Number
            Fields2.Add(fld);

            #endregion INF0187

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND SUBTY <> '10'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt2);
            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt2);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0021", 0, 0, ref oTable, ref Fields, ref Options);
                oFunction.Zrfc_Read_Table("^", "", "PA0187", 0, 0, ref oTable2, ref Fields2, ref Options2);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }

            foreach (TAB512 item in oTable)
            {
                string tempSub1, temp1;
                PersonalFamilyCenter IF21 = new PersonalFamilyCenter();
                string[] buffer = item.Wa.Split('^');
                IF21.EmployeeID = EmployeeID;
                IF21.FamilyMember = buffer[0].Trim();
                IF21.ChildNo = buffer[1].Trim();
                IF21.TitleRank = buffer[2].Trim();
                IF21.Name = buffer[3].Trim();
                IF21.Surname = buffer[4].Trim();
                DateTime bufferDate;
                DateTime.TryParseExact(buffer[5].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                IF21.BirthDate = bufferDate;
                IF21.Sex = buffer[6].Trim();
                IF21.BirthPlace = buffer[7].Trim();
                IF21.CityOfBirth = buffer[8].Trim();
                IF21.Nationality = buffer[9].Trim();
                IF21.SubType = buffer[10].Trim();
                IF21.Age = CalculateAge(bufferDate);
                //Addby:Nuttapon.a 07.02.2013
                DateTime oTempDatetime;
                DateTime.TryParseExact(buffer[12].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                IF21.BeginDate = oTempDatetime;
                DateTime.TryParseExact(buffer[13].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                IF21.EndDate = oTempDatetime;

                tempSub1 = buffer[10].Trim();
                temp1 = buffer[11].Trim();
                string tempSub2, temp2;
                foreach (TAB512 item2 in oTable2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    tempSub2 = buffer2[8].Trim();
                    temp2 = buffer2[9].Trim();
                    if (tempSub1 == tempSub2 && temp1 == temp2)
                    {
                        IF21.FatherID = buffer2[0].Trim();
                        IF21.MotherID = buffer2[1].Trim();
                        IF21.SpouseID = buffer2[2].Trim();
                        IF21.FatherSpouseID = buffer2[3].Trim();
                        IF21.MotherSpouseID = buffer2[4].Trim();
                        IF21.TitleName = buffer2[5].Trim();
                        IF21.Employer = buffer2[6].Trim();
                        IF21.JobTitle = buffer2[7].Trim();
                        //AddBY: Ratchatawan W. (2011-10-14)
                        IF21.Dead = buffer2[10].Trim();
                        //AddBy: Nuttapon.a 11.02.2013
                        IF21.Address = buffer2[11].Trim();
                        IF21.Street = buffer2[12].Trim();
                        IF21.District = buffer2[13].Trim();
                        IF21.City = buffer2[14].Trim();
                        IF21.Postcode = buffer2[15].Trim();
                        IF21.Country = buffer2[16].Trim();
                        IF21.TelephoneNo = buffer2[17].Trim();
                    }
                }

                returnValue.Add(IF21);
            }
            return returnValue;
        }
        //public override void SaveFamilyData(List<INFOTYPE0021> data)
        public override void SavePersonalFamilyData(List<PersonalFamilyCenter> data)
        {
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            foreach (PersonalFamilyCenter info21 in data)
            {
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = info21.BeginDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Endda = info21.EndDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Infty = info21.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = info21.ChildNo;
                item.Operation = "INS";
                item.Pernr = info21.EmployeeID;
                //�繡������Ţ����ͧ�Ҥӹǳ�������ͧ�ѹ�����ū�ӡѹ ����ѹ�� gen key �������շҧ��ӡѹ�����
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = info21.SubType;
                //T ��ҧ � ����Ҩҡ function �������� sap se37
                item.T01 = info21.FamilyMember;
                item.T02 = info21.TitleRank;
                item.T03 = info21.Name;
                item.T04 = info21.Surname;
                if (info21.BirthDate == DateTime.MinValue)
                {
                    item.T05 = "00000000";
                }
                else
                {
                    item.T05 = info21.BirthDate.Value.ToString("yyyyMMdd", DefaultCultureInfo);
                }
                item.T06 = info21.Sex;
                //Province province = Province.GetProvince(info21.BirthPlace);
                //item.T07 = province.Description;
                //ModifyBy: Ratchatawan W. (2011-10-14)
                item.T07 = info21.BirthPlace;

                item.T08 = info21.CityOfBirth;
                item.T09 = info21.Nationality;
                item.T10 = info21.FatherID;
                item.T11 = info21.MotherID;
                item.T12 = info21.SpouseID;
                item.T13 = info21.FatherSpouseID;
                item.T14 = info21.MotherSpouseID;
                item.T15 = info21.TitleName;
                item.T16 = info21.Employer;
                item.T17 = info21.JobTitle;
                //AddBy: Ratchatawan W. (2011-10-14)
                item.T18 = info21.Dead;
                //AddBy: Nuttapon.a 2013-02-19
                item.T19 = info21.Address;
                item.T20 = info21.Street;
                item.T21 = info21.District;
                item.T22 = info21.City;
                item.T23 = info21.Postcode;
                item.T24 = info21.Country;
                item.T25 = info21.TelephoneNo;
                Updatetab.Add(item);
            }
            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            string message = "";
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    message = result.Msg;
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error: " + message);
            }
        }
        //public override INFOTYPE0021 GetFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        public override PersonalFamilyCenter GetPersonalFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        {
            PersonalFamilyCenter returnValue = new PersonalFamilyCenter();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table oTable2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt, opt2;
            RFC_DB_FLD fld, fld2;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMSA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FNMZU";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAVOR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FASEX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBOT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBLD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            //Get Begda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //Get Endda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            #region INF0187

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "FTXID"; //Identity Number for Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "MTXID"; //Identity Number for Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "STXID"; //Identity Number for Spouse
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SFXID"; //Identity Number for Spouse's Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SMXID"; //Identity Number for Spouse's Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "ANRED"; //Form-of-Address Key
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "EFAML"; //Employer of Family Member (TH)
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "JOBTL"; //Job Title
            Fields2.Add(fld2);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY"; //Subtype
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS"; //Object Identification
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALDEC"; //Deceased Indicator
            Fields2.Add(fld);

            //Addby:Nuttapon.a 11.02.2013 adress
            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS"; // Address Number, Soi Trok (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT"; //Street/Road, Tambol
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02"; // District (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01"; // Province
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ"; // Postal Code
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1"; // Country Key
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TELNR"; // Telephone Number
            Fields2.Add(fld);

            #endregion INF0187

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND FAMSA = '{0}' and OBJPS = '{1}'", FamilyMember, ChildNo);
            Options.Add(opt);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt2);
            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt2);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0021", 0, 0, ref oTable, ref Fields, ref Options);
                oFunction.Zrfc_Read_Table("^", "", "PA0187", 0, 0, ref oTable2, ref Fields2, ref Options2);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }


            foreach (TAB512 item in oTable)
            {
                string tempSub1, temp1;
                //INFOTYPE0021 IF21 = new INFOTYPE0021();
                string[] buffer = item.Wa.Split('^');
                returnValue.EmployeeID = EmployeeID;
                returnValue.FamilyMember = buffer[0].Trim();
                returnValue.ChildNo = buffer[1].Trim();
                returnValue.TitleRank = buffer[2].Trim();
                returnValue.Name = buffer[3].Trim();
                returnValue.Surname = buffer[4].Trim();
                DateTime bufDate;
                DateTime.TryParseExact(buffer[5].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufDate);
                returnValue.BirthDate = bufDate;
                returnValue.Sex = buffer[6].Trim();
                returnValue.BirthPlace = buffer[7].Trim();
                returnValue.CityOfBirth = buffer[8].Trim();
                returnValue.Nationality = buffer[9].Trim();

                //nuttapon.a 07.02.2013
                DateTime oTempDatetime;
                DateTime.TryParseExact(buffer[12].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                returnValue.BeginDate = oTempDatetime;
                DateTime.TryParseExact(buffer[13].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                returnValue.EndDate = oTempDatetime;

                tempSub1 = buffer[10].Trim();
                temp1 = buffer[11].Trim();
                string tempSub2, temp2;
                foreach (TAB512 item2 in oTable2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    tempSub2 = buffer2[8].Trim();
                    temp2 = buffer2[9].Trim();
                    if (tempSub1 == tempSub2 && temp1 == temp2)
                    {
                        returnValue.FatherID = buffer2[0].Trim();
                        returnValue.MotherID = buffer2[1].Trim();
                        returnValue.SpouseID = buffer2[2].Trim();
                        returnValue.FatherSpouseID = buffer2[3].Trim();
                        returnValue.MotherSpouseID = buffer2[4].Trim();
                        returnValue.TitleName = buffer2[5].Trim();
                        returnValue.Employer = buffer2[6].Trim();
                        returnValue.JobTitle = buffer2[7].Trim();

                        //AddBY: Ratchatawan W. (2011-10-14)
                        returnValue.Dead = buffer2[10].Trim();

                        //AddBy: Nuttapon.a 11.02.2013
                        returnValue.Address = buffer2[11].Trim();
                        returnValue.Street = buffer2[12].Trim();
                        returnValue.District = buffer2[13].Trim();
                        returnValue.City = buffer2[14].Trim();
                        returnValue.Postcode = buffer2[15].Trim();
                        returnValue.Country = buffer2[16].Trim();
                        returnValue.TelephoneNo = buffer2[17].Trim();
                    }
                }
            }
            return returnValue;
        }

        #endregion " Family "

        #region "Education"

        public override void DeleteEducation(List<INFOTYPE0022> data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();

            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();

            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "

            foreach (INFOTYPE0022 info22 in data)
            {
                ZHRHRS001 item = new ZHRHRS001();

                item.Begda = info22.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = info22.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = info22.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "DEL";
                item.Pernr = info22.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                //item.Subty = info22.SubType;
                item.Subty = info22.EducationLevelCode;

                item.T01 = info22.InstituteCode;
                item.T02 = info22.EducationLevelCode;
                item.T03 = info22.Grade.ToString();
                item.T04 = info22.CertificateCode;
                item.T05 = info22.CountryCode;
                item.T06 = info22.Branch1;
                item.T07 = info22.Branch2;

                //ModifiedBy: Ratchatawan W. (2012-02-07)
                //item.T08 = info22.Duration;
                item.T08 = info22.EducationGroupCode;

                item.T09 = info22.Unit;
                item.T10 = info22.HonorCode;

                Updatetab.Add(item);
            }

            #endregion " set item "

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        //public override List<INFOTYPE0022> GetAllEducationHistory(string EmployeeID)
        public override List<PersonalEducationCenter> GetAllPersonalEducationHistory(string EmployeeID)
        {
            List<PersonalEducationCenter> returnValue = new List<PersonalEducationCenter>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLART";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "INSTI";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLAND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "AUSBI";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLABS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLTP1";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLTP2";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KSBEZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TX122";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FACCD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "EMARK";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCHCD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZKL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZEH";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            #endregion " Options "

            try
            {
                // oFunction.Zrfc_Read_Table("^", "", "PA0022", 0, 0, ref oTable, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }



            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                PersonalEducationCenter dataItem = new PersonalEducationCenter();
                string[] buffer = item.Wa.Split('^');
                DateTime bufferDate = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                dataItem.EmployeeID = EmployeeID;
                dataItem.BeginDate = bufferDate;
                DateTime.TryParseExact(buffer[1].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                dataItem.EndDate = bufferDate;
                dataItem.EducationLevelCode = buffer[2].Trim();
                dataItem.InstituteName = buffer[5].Trim();
                dataItem.CountryCode = buffer[4].Trim();
                dataItem.VocationCode = buffer[5].Trim();
                dataItem.CertificateCode = buffer[6].Trim();
                dataItem.Branch1 = buffer[7].Trim();
                dataItem.Branch2 = buffer[8].Trim();
                string bufferText = buffer[9].Trim();
                dataItem.HonorCode = buffer[10].Trim();
                dataItem.IsHiringLevel = !(bufferText == "" || bufferText.Substring(0, 1) == "N");
                dataItem.GraduateYear = dataItem.EndDate.Year.ToString();
                decimal bufferDecimal;
                decimal.TryParse(buffer[12].Trim(), out bufferDecimal);
                dataItem.Grade = bufferDecimal;
                dataItem.GradeText = buffer[12].Trim();
                dataItem.InstituteCode = buffer[5].Trim();
                //ModifiedBy: Ratchatawan W. (2012-02-07)
                //dataItem.Duration = buffer[14].Trim();
                dataItem.EducationGroupCode = buffer[14].Trim();
                dataItem.Unit = buffer[15].Trim();
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            //var distinctResult = returnValue.GroupBy(i => i.EducationLevelCode).Select(g => g.Last()).ToList();
            var distinctResult = returnValue.GroupBy(i => new { i.EducationLevelCode, i.BeginDate }).Select(g => g.Last()).ToList();
            returnValue = distinctResult.ToList();

            return returnValue;
        }

        public override void SaveEducation(List<INFOTYPE0022> data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();

            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();

            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "

            foreach (INFOTYPE0022 info22 in data)
            {
                ZHRHRS001 item = new ZHRHRS001();

                item.Begda = info22.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = info22.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = info22.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "INS";
                item.Pernr = info22.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                //item.Subty = info22.SubType;
                item.Subty = info22.EducationLevelCode;

                item.T01 = info22.InstituteCode;
                item.T02 = info22.EducationLevelCode;
                item.T03 = info22.Grade.ToString();
                item.T04 = info22.CertificateCode;
                item.T05 = info22.CountryCode;
                item.T06 = info22.Branch1;
                item.T07 = info22.Branch2;

                //ModifiedBy: Ratchatawan W. (2012-02-07)
                //item.T08 = info22.Duration;
                item.T08 = info22.EducationGroupCode;

                item.T09 = info22.Unit;
                item.T10 = info22.HonorCode;

                Updatetab.Add(item);
            }

            #endregion " set item "

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        public override List<INFOTYPE0022> GetAllCertificateHistory(string EmployeeID)
        {
            List<INFOTYPE0022> returnValue = new List<INFOTYPE0022>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLABS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "EMARK";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DPTMT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND SUBTY = 'C1'", EmployeeID);
            Options.Add(opt);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0022", 0, 0, ref oTable, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }



            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE0022 dataItem = new INFOTYPE0022();
                string[] buffer = item.Wa.Split('^');
                DateTime bufferDate = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                dataItem.EmployeeID = EmployeeID;
                dataItem.BeginDate = bufferDate;
                dataItem.CertificateCode = buffer[1].Trim();
                decimal grade = 0.0M;
                decimal.TryParse(buffer[2].Trim(), out grade);
                dataItem.Grade = grade;
                dataItem.Remark = buffer[3].Trim();
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        //public override void DeleteInsertEducation(List<INFOTYPE0022> data)
        public override void DeleteInsertEducation(List<PersonalEducationCenter> data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();

            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();

            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "

            for (int i = 0; i < data.Count; i++)
            {
                PersonalEducationCenter info22 = new PersonalEducationCenter();
                info22 = data[i];
                ZHRHRS001 item = new ZHRHRS001();
                string operate = string.Empty;

                item.Begda = info22.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = info22.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = info22.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                if (i == 0)
                    operate = "DEL";
                else
                    operate = "INS";

                item.Operation = operate;
                item.Pernr = info22.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                //item.Subty = info22.SubType;
                item.Subty = info22.EducationLevelCode;

                item.T01 = info22.InstituteCode;
                item.T02 = info22.EducationLevelCode;
                item.T03 = info22.Grade.ToString();
                item.T04 = info22.CertificateCode;
                item.T05 = info22.CountryCode;
                item.T06 = info22.Branch1;
                item.T07 = info22.Branch2;

                //ModifiedBy: Ratchatawan W. (2012-02-07)
                //item.T08 = info22.Duration;
                item.T08 = info22.EducationGroupCode;

                item.T09 = info22.Unit;
                item.T10 = info22.HonorCode;

                Updatetab.Add(item);
            }

            #endregion " set item "

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        #endregion "Education"

        #region " PreviousEmployers "

        public override List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        {
            List<INFOTYPE0023> returnValue = new List<INFOTYPE0023>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ARBGB";
            Fields.Add(fld);

            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ZZPOSITION";
            //Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BRANC";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TAETE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANSVX";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0023", 0, 0, ref oTable, ref Fields, ref Options);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }

            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE0023 dataItem = new INFOTYPE0023();
                dataItem.EmployeeID = EmployeeID;
                string[] buffer = item.Wa.Split('^');
                DateTime bufferDate = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                dataItem.BeginDate = bufferDate;
                DateTime.TryParseExact(buffer[1].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                dataItem.EndDate = bufferDate;
                dataItem.Employer = buffer[2].Trim();
                //dataItem.Position = buffer[3].Trim(); field not valid
                dataItem.Position = "";
                dataItem.CountryCode = buffer[4].Trim();
                dataItem.CompanyBusinessCode = buffer[5].Trim();
                dataItem.JobCode = buffer[6].Trim();
                dataItem.CountryCode = buffer[7].Trim();
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        #endregion " PreviousEmployers "

        #region " SearchINFOTYPE0002 "

        public override void SearchINFOTYPE0002(FilterData filter)
        {
            CultureInfo oCL = new CultureInfo("en-US");

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();

            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("BEGDA <= '{0}' AND ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);
            switch (filter.FilterName.ToUpper())
            {
                case "INFOTYPE0002.DOB":
                    if (filter.Value1 is DateTime)
                    {
                        DateTime dob = (DateTime)filter.Value1;
                        opt = new RFC_DB_OPT();
                        opt.Text = string.Format("AND GBDAT {0} '{1}'", filter.Operator, dob.ToString("yyyyMMdd", DefaultCultureInfo));
                        Options.Add(opt);
                    }
                    else
                    {
                        // can't find
                        return;
                    }
                    break;
            }

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0002", 0, 0, ref Data, ref Fields, ref Options);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "

            filter.Results.Clear();
            foreach (TAB512 item in Data)
            {
                filter.Results.Add(item.Wa);
            }

            #endregion " Parse Object "
        }

        #endregion " SearchINFOTYPE0002 "

        #region " WorkHistory Ext "

        public override List<WorkPeriod> GetCurrentWorkHistory(string EmployeeID)
        {
            CultureInfo oCLEN = new CultureInfo("en-US");
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            SAPInterfaceExt.SAPInterfaceExt oFunction = new SAPInterfaceExt.SAPInterfaceExt();
            ZHRPES006Table result = new ZHRPES006Table();
            oFunction.Connection = oConnect;
            //oFunction.Zhrpee003("245", "", "", "", EmployeeID, EmployeeID, ref result);

            DateTime dBegin = new DateTime(1800, 1, 1);
            string sBegin = dBegin.ToString("dd.MM.yyyy", oCLEN);
            string sEnd = DateTime.Now.ToString("dd.MM.yyyy", oCLEN);
            oFunction.Zhrpee003("245", sBegin, "", sEnd, EmployeeID, EmployeeID, ref result);

            #region " ParseToObject "
            List<WorkPeriod> oList = new List<WorkPeriod>();

            foreach (ZHRPES006 item in result)
            {
                WorkPeriod oTemp = new WorkPeriod();

                //ID
                oTemp.EMPLOYEEID = item.Id;
                //START
                oTemp.STARTDATE = item.Start;
                //END
                oTemp.ENDDATE = item.End;
                //POS_ID
                oTemp.POS_ID = item.Pos_Id;
                //POS_NAME
                oTemp.POS_NAME = item.Pos_Name;
                //LEVEL
                oTemp.LEVEL = item.Level;
                //ORG_ID
                oTemp.ORG_ID = item.Org_Id;
                //ORG_NAME
                oTemp.ORG_NAME = item.Org_Name;
                //REF_NUM
                oTemp.REF_NO = item.Ref_No;
                //EFFDATE
                oTemp.EFFDATE = item.Effdate;
                //ANTYPE
                oTemp.ANTYPE = item.Antype;
                //LINE
                oTemp.LINE = item.Line;
                //BUSUNIT
                oTemp.BUSUNIT = item.Busunit;
                //ACTION
                oTemp.ACTION = item.Action;

                oList.Add(oTemp);

            }
            #endregion
            oFunction.Dispose();
            return oList;
        }
        public override RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate)
        {
            CultureInfo oCLEN = new CultureInfo("en-US");
            RelatedAge returnValue = new RelatedAge();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            SAPInterfaceExt.SAPInterfaceExt oFunction = new SAPInterfaceExt.SAPInterfaceExt();
            oFunction.Connection = oConnect;

            ZHRPES056 AgeOfOrg = new ZHRPES056();
            ZHRPES056 AgeOfLevel = new ZHRPES056();
            ZHRPES056 AgeOfPosition = new ZHRPES056();
            ZHRPES056 AgeOfCompany = new ZHRPES056();

            //oFunction.Zhrpee006("^", "", "PA0008", 0, 0, ref oTable, ref Fields, ref Options);
            try
            {
                oFunction.Zhrpee006("", CheckDate.ToString("yyyyMMdd", oCLEN), EmployeeID, out AgeOfOrg, out AgeOfLevel, out AgeOfPosition, out AgeOfCompany);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }

            //Nattawat S. Add 14/04/2020
            DateTimeData oAgeOfOrg = convertDataClassForMultiCompany(AgeOfOrg);
            DateTimeData oAgeOfLevel = convertDataClassForMultiCompany(AgeOfLevel);
            DateTimeData oAgeOfPosition = convertDataClassForMultiCompany(AgeOfPosition);
            DateTimeData oAgeOfCompany = convertDataClassForMultiCompany(AgeOfCompany);
            //end Add

            returnValue.EmployeeID = EmployeeID;
            returnValue.ViewDate = CheckDate;
            returnValue.ageOfOrg = oAgeOfOrg;
            returnValue.ageOfLevel = oAgeOfLevel;
            returnValue.ageOfPosition = oAgeOfPosition;
            returnValue.ageOfCompany = oAgeOfCompany;

            return returnValue;
        }

        public DateTimeData convertDataClassForMultiCompany(ZHRPES056 oInput)
        {
            DateTimeData oOutput = new DateTimeData();
            oOutput.Zday = oInput.Zday;
            oOutput.Zmonth = oInput.Zmonth;
            oOutput.Zyear = oInput.Zyear;
            return oOutput;
        }

        #endregion " WorkHistory Ext "

        #region " WorkHistory (INFOTYPE 9002) "

        public override List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        {
            List<INFOTYPE9002> returnValue = new List<INFOTYPE9002>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();

            //changed by nuttapon.a 29.11.2012 user �������¹�Ҵ֧�ѹ���ҡ BeginDate ᷹
            // fld.Fieldname = "ANNDAT";  // �ѹ����ռ�
            fld.Fieldname = "BEGDA"; // BeginDate
            Fields.Add(fld);

            //changed by nuttapon.a 29.11.2012 user �������¹�Ҵ֧�����Ũҡ ���˹�����(����)
            fld = new RFC_DB_FLD();
            //fld.Fieldname = "NMPOST"; // ���˹�����(����)
            fld.Fieldname = "NOPOST";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NOUT"; //˹���ͧ���(����)
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NMPOST"; // ���˹���ѡ(����)
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            //opt = new RFC_DB_OPT();
            //opt.Text = "AND ( ANTYPE IN ('0101','0103','0104','0201','0207','0401','0502')";
            //Options.Add(opt);

            //opt = new RFC_DB_OPT();
            //opt.Text = "OR ANTYPE IN ('0601','0602','0604') )";
            //Options.Add(opt);

            //opt = new RFC_DB_OPT();
            //opt.Text = "AND SUBTY = '2'";
            //Options.Add(opt);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA9002", 0, 0, ref oTable, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }



            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE9002 dataItem = new INFOTYPE9002();
                dataItem.EmployeeID = EmployeeID;
                string[] buffer = item.Wa.Split('^');
                DateTime bufferDate = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                dataItem.BeginDate = bufferDate;
                //DateTime.TryParseExact(buffer[1].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                //dataItem.EndDate = bufferDate;
                if (buffer[1].Trim() != string.Empty) // ModBy:Nuttapon.a 20.02.2013 New user requirement
                {
                    dataItem.PositionName = buffer[1].Trim();
                }
                else dataItem.PositionName = buffer[3].Trim();

                dataItem.UNIT = buffer[2];
                //dataItem.DivisionName = buffer[3].Trim();
                //dataItem.DepartmentName = buffer[4].Trim();
                //dataItem.CompanyName = buffer[5].Trim();
                //dataItem.Level = buffer[6].Trim();
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        #endregion " WorkHistory (INFOTYPE 9002) "

        public override List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        {
            List<INFOTYPE9001> returnValue = new List<INFOTYPE9001>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SALARY";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA9001", 0, 0, ref oTable, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }

            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE9001 dataItem = new INFOTYPE9001();
                dataItem.EmployeeID = EmployeeID;
                string[] buffer = item.Wa.Split('^');
                DateTime dt = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.BeginDate = dt;
                dataItem.Salary = decimal.Parse(buffer[1].Trim());
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        public override List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        {
            List<INFOTYPE0008> returnValue = new List<INFOTYPE0008>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BET01";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            #endregion " Options "

            try
            {
                oFunction.Zrfc_Read_Table("^", "", "PA0008", 0, 0, ref oTable, ref Fields, ref Options);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Connection.ReturnConnection(oConnect);
                oFunction.Dispose();
            }

            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE0008 dataItem = new INFOTYPE0008();
                dataItem.EmployeeID = EmployeeID;
                string[] buffer = item.Wa.Split('^');
                DateTime dt = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.BeginDate = dt;
                DateTime.TryParseExact(buffer[1].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.EndDate = dt;
                dataItem.Salary = decimal.Parse(buffer[2].Trim());
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        public override List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate)
        {
            List<INFOTYPE0008> returnValue = new List<INFOTYPE0008>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BET01";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", CheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0008", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE0008 dataItem = new INFOTYPE0008();
                dataItem.EmployeeID = EmployeeID;
                string[] buffer = item.Wa.Split('^');
                DateTime dt = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.BeginDate = dt;
                DateTime.TryParseExact(buffer[1].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.EndDate = dt;
                dataItem.Salary = decimal.Parse(buffer[2].Trim());
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        public override List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        {
            List<INFOTYPE9003> returnValue = new List<INFOTYPE9003>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);
            //Evaluation Result
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZEVALUATE";
            Fields.Add(fld);
            //Company Adjust Rate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZMERIT";
            Fields.Add(fld);
            //Personal Adjust Rate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZBONUS_AVG";
            Fields.Add(fld);
            //Base Salary Adjust
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZBASE_SALARY";
            Fields.Add(fld);
            //Amount Salary Adjust
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZBF_SALARY";
            Fields.Add(fld);
            //Specail Salary Adjust
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZAT_SALARY";
            Fields.Add(fld);
            //Bonus1
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZBONUS_1";
            Fields.Add(fld);
            //Special Bonus
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZBONUS";
            Fields.Add(fld);
            //Remark
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZREMARK";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA9003", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE9003 dataItem = new INFOTYPE9003();
                dataItem.EmployeeID = EmployeeID;
                string[] buffer = item.Wa.Split('^');
                DateTime dt = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.BeginDate = dt;
                dataItem.EvaluationResult = buffer[1].Trim();
                dataItem.CompanyAdjustRate = decimal.Parse(buffer[2].Trim());
                dataItem.PersonalAdjustRate = decimal.Parse(buffer[3].Trim());
                dataItem.BaseSalaryAdjust = decimal.Parse(buffer[4].Trim());
                dataItem.AmountSalaryAdjust = decimal.Parse(buffer[5].Trim());
                dataItem.SpecailSalaryAdjust = decimal.Parse(buffer[6].Trim());
                dataItem.Bonus1 = decimal.Parse(buffer[7].Trim());
                dataItem.SpecialBonus = decimal.Parse(buffer[8].Trim());
                dataItem.Remark = buffer[9].Trim();
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        public override List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        {
            List<INFOTYPE9004> returnValue = new List<INFOTYPE9004>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSONNEL_GRADE";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA9004", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            #region " parse object "

            foreach (TAB512 item in oTable)
            {
                INFOTYPE9004 dataItem = new INFOTYPE9004();
                dataItem.EmployeeID = EmployeeID;
                string[] buffer = item.Wa.Split('^');
                DateTime dt = DateTime.MinValue;
                DateTime.TryParseExact(buffer[0].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.BeginDate = dt;
                DateTime.TryParseExact(buffer[1].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out dt);
                dataItem.EndDate = dt;
                dataItem.Level = buffer[2].Trim();
                returnValue.Add(dataItem);
            }

            #endregion " parse object "

            return returnValue;
        }

        public override INFOTYPE0008 GetInfotype0008(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0008 returnValue = new INFOTYPE0008();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BET01";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0008", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            foreach (TAB512 item in oTable)
            {
                string[] buffer = item.Wa.Split('^');
                returnValue.EmployeeID = buffer[0];
                returnValue.BeginDate = DateTime.ParseExact(buffer[1], "yyyyMMdd", DefaultCultureInfo);
                returnValue.EndDate = DateTime.ParseExact(buffer[2], "yyyyMMdd", DefaultCultureInfo);
                returnValue.Salary = Convert.ToDecimal(buffer[3]);
            }
            return returnValue;
        }

        //get location name such as Rayong Office...
        public override List<WorkPlaceData> GetWorkPlace()
        {
            List<WorkPlaceData> returnValue = new List<WorkPlaceData>();
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            //select
            RFC_DB_FLD fld;
            fld = new RFC_DB_FLD();
            fld.Fieldname = "COMKY";
            Fields.Add(fld);
            //where
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = "COMTY = '0'";
            Options.Add(opt);

            oConnection.Open();
            oFunction.Zrfc_Read_Table("^", "", "T536B", 0, 0, ref Data, ref Fields, ref Options);
            oConnection.Close();

            foreach (TAB512 item in Data)
            {
                WorkPlaceData obj = new WorkPlaceData();
                obj.WorkPlaceCode = item.Wa;
                returnValue.Add(obj);
            }
            oFunction.Dispose();
            return returnValue;
        }

        //get data of employee from SAP
        public override CommunicationData GetCommunicationData(string employeeID)
        {
            CommunicationData comData = new CommunicationData();
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            //�ç����ͧ new 2 �ش �֧������ collection ������繡�� add �����Ť��� table
            TAB512Table Data2 = new TAB512Table();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            RFC_DB_FLD fld;
            fld = new RFC_DB_FLD();
            fld.Fieldname = "COM01";
            Fields.Add(fld);

            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = "SUBTY = '9'";
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            //ModifyBy: Ratchatawan W. (2011-10-10)
            //opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd"));
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd"), DefaultCultureInfo);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND PERNR = '{0}'", employeeID);
            Options.Add(opt);

            RFC_DB_FLD fld2;
            //fld2 = new RFC_DB_FLD();
            //fld2.Fieldname = "USRID_LONG";
            //Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "USRID";
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "USRID_LONG";
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SUBTY";
            Fields2.Add(fld2);

            RFC_DB_OPT opt2;
            //opt2 = new RFC_DB_OPT();
            //opt2.Text = "SUBTY IN ( '0020' , '0030' , 'MPHN' )";
            //Options2.Add(opt2);

            //opt2 = new RFC_DB_OPT();
            //opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd"));
            //Options2.Add(opt2);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("PERNR = '{0}'", employeeID);
            Options2.Add(opt2);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd"), DefaultCultureInfo);
            Options2.Add(opt2);

            oConnection.Open();
            //Rfc_Read_Table used to read tables
            oFunction.Zrfc_Read_Table("^", "", "PA0006", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "PA0105", 0, 0, ref Data2, ref Fields2, ref Options2);
            oConnection.Close();

            //add employeeID
            comData.EmployeeID = employeeID;

            foreach (TAB512 item in Data)
            {
                comData.WorkPlace = item.Wa;
            }
            foreach (TAB512 item in Data2)
            {
                string[] buffer = item.Wa.Split('^');
                switch (buffer[2])
                {
                    case "9001":
                        comData.TelephoneNo = buffer[0].Trim();
                        break;

                    case "9003":
                        comData.MobilePhone = buffer[0].Trim();
                        break;

                    case "9004":
                        comData.HomePhone = buffer[0].Trim();
                        break;

                    case "9008":
                        comData.Email = buffer[1].Trim();
                        break;
                }
            }
            oFunction.Dispose();
            return comData;
        }

        //Save data to SAP
        public override void SaveCommunicationData(CommunicationData savedData, CommunicationData oldData)
        {
            //ZHRHRS001Table is collection
            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_ret = new ZHRHRS001Table();
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;
            ZHRHRS001 item2, item3;

            if (savedData.TelephoneNo != oldData.TelephoneNo)
            {
                item2 = new ZHRHRS001();
                item2.Reqnr = new Guid().ToString();
                item2.Infty = "0105";
                item2.Operation = "INS";
                item2.Pernr = savedData.EmployeeID;
                item2.Subty = "9001";
                //ModifyBy: Ratchatawan W. (2011-10-10)
                //item2.Begda = DateTime.Now.ToString("yyyyMMdd");
                item2.Begda = DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo);
                item2.Endda = "99991231";
                item2.T01 = savedData.TelephoneNo;
                Updatetab.Add(item2);
            }

            if (savedData.HomePhone != oldData.HomePhone)
            {
                item3 = new ZHRHRS001();
                item3.Reqnr = new Guid().ToString();
                item3.Infty = "0105";
                item3.Operation = "INS";
                item3.Pernr = savedData.EmployeeID;
                item3.Subty = "9004";
                //ModifyBy: Ratchatawan W. (2011-10-10)
                //item3.Begda = DateTime.Now.ToString("yyyyMMdd");
                item3.Begda = DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo);
                item3.Endda = "99991231";
                item3.T01 = savedData.HomePhone;
                Updatetab.Add(item3);
            }

            if (savedData.MobilePhone != oldData.MobilePhone)
            {
                item3 = new ZHRHRS001();
                item3.Reqnr = new Guid().ToString();
                item3.Infty = "0105";
                item3.Operation = "INS";
                item3.Pernr = savedData.EmployeeID;
                item3.Subty = "9003";
                //ModifyBy: Ratchatawan W. (2011-10-10)
                //item3.Begda = DateTime.Now.ToString("yyyyMMdd");
                item3.Begda = DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo);
                item3.Endda = "99991231";
                item3.T01 = savedData.MobilePhone;
                Updatetab.Add(item3);
            }

            if (savedData.Email != oldData.Email)
            {
                item3 = new ZHRHRS001();
                item3.Reqnr = new Guid().ToString();
                item3.Infty = "0105";
                item3.Operation = "INS";
                item3.Pernr = savedData.EmployeeID;
                item3.Subty = "9008";
                item3.Begda = DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo);
                item3.Endda = "99991231";
                item3.T03 = savedData.Email;
                Updatetab.Add(item3);
            }

            //���÷���� .Add �� �� Collection
            oConnection.Open();
            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_ret);
            oConnection.Close();
            oFunction.Dispose();
            //��� SAVE ���ŧ ���ʴ� Message error �ҡ SAP
            bool lError = false;
            ZHRHRS001 result;
            if (Updatetab_ret.Count > 0)
            {
                result = Updatetab_ret[0];
                if (result.Msgtype == "E")
                {
                    savedData.Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        #region "Salary rate"
        public override List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup)
        {
            List<SALARYRATE> returnValue = new List<SALARYRATE>();
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            //EMPLOYEE.EmployeeData empData = new EMPLOYEE.EmployeeData(EmployeeID, DateTime.Now);

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLMIN";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLREF";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SLMAX";
            Fields.Add(fld);

            #endregion

            #region " Options "

            string subGroup = "A1" + EmpSubGroup; //string.Empty; //08"
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("MOLGA = '26' AND SLLVL = '01' AND SLGRP = '{0}'", subGroup);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", oCL));
            Options.Add(opt);

            #endregion

            oFunction.Zrfc_Read_Table("^", "", "T710", 0, 0, ref oTable, ref Fields, ref Options);

            //SAP.Connector.Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " parse object "
            foreach (TAB512 item in oTable)
            {
                SALARYRATE dataItem = new SALARYRATE();
                string[] buffer = item.Wa.Split('^');
                dataItem.EmployeeID = EmployeeID;
                dataItem.Minimum = decimal.Parse(buffer[0].Trim());
                dataItem.Average = decimal.Parse(buffer[1].Trim());
                dataItem.Maximum = decimal.Parse(buffer[2].Trim());
                returnValue.Add(dataItem);
            }
            #endregion

            return returnValue;
        }
        #endregion 

        private static int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }

        public override List<string> GetIDCardData(string EmployeeID)
        {
            List<string> returnValue = new List<string>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            PYInterface oFunction1 = new PYInterface();
            ZHRPYS004Table taxAmount = new ZHRPYS004Table();
            oFunction.Connection = oConnect;
            oFunction1.Connection = oConnect;

            TAB512Table Data = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            ////�ӴѺ�Ṻ
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "BEGDA";
            //Fields.Add(fld);
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ENDDA";
            //Fields.Add(fld);
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ICOLD";
            //Fields.Add(fld);

            //�Ţ��Шӵ�����վ�ѡ�ҹ
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ICNUM";
            Fields.Add(fld);
            #endregion

            #region " Options "         
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '01'";
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);
            #endregion

            //oFunction.Zrfc_Read_Table("^", "", "PA0185", 0, 0, ref Data, ref Fields, ref Options);
            //oFunction1.Zhrpyi011(EmployeeID, Year, ref taxAmount);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            string sCardID = string.Empty;
            foreach (TAB512 item in Data)
            {
                string[] buffer = item.Wa.Split('^');
                sCardID = buffer[0].Trim();
                sCardID = sCardID.Replace("-", string.Empty);
            }

            returnValue.Add(sCardID);
            return returnValue;
        }


    }
}