﻿using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.HR.OM.ABSTRACT;
using ESS.SHAREDATASERVICE;
using SAP.Connector;
using SAPInterface;

namespace ESS.HR.PA.SAP
{
    public class HROMConfigServiceImpl : AbstractHROMConfigService
    {
        #region Constructor
        public HROMConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        private static string ModuleID = "ESS.HR.OM.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");
         
            }
        }
        #endregion Member




        #region IHRPAConfig Members

        

        #endregion IHRPAConfig Members
    }
}