﻿using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using ESS.SHAREDATASERVICE.ABSTRACT;
using ESS.SHAREDATASERVICE.DATACLASS;
using ESS.UTILITY;
using ESS.UTILITY.EXTENSION;
using ESS.SHAREDATASERVICE.CONFIG;
using System;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.LOG;
using ESS.MAIL.DATACLASS;
using ESS.WORKFLOW;
using System.Security.Cryptography;
using System.Web.UI;
using System.Text;
using System.Net;
using System.Web;

namespace ESS.SHAREDATASERVICE.DB
{
    public class ShareDataServiceImpl : AbstractShareDataService
    {
        #region Constructor

        public ShareDataServiceImpl()
        {
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Private Member
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }
        private string BaseConnStr
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }

        #endregion

        public override DataSet GetTextDescriptionByCompany(string CompanyCode, string LanguageCode)
        {
            DataSet oDataSet = new DataSet();
            oSqlManage["ProcedureName"] = "sp_TextDescriptionLoadByCompany";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CompanyCode"] = CompanyCode;
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oDataSet = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            return oDataSet;
        }

        public override List<UserRoleClass> GetAllUserRole()
        {
            List<UserRoleClass> oUserRoleList = new List<UserRoleClass>();
            oSqlManage["ProcedureName"] = "sp_UserRoleGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oUserRoleList.AddRange(DatabaseHelper.ExecuteQuery<UserRoleClass>(oSqlManage, oParamRequest));
            return oUserRoleList;
        }

        public override DataTable GetAlluserRoleEditor()
        {
            DataTable oUserRoleList = new DataTable();
            oSqlManage["ProcedureName"] = "sp_UserRoleEditorGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oUserRoleList = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);
            return oUserRoleList;
        }

        public override List<RoleClass> GetAllRole()
        {
            List<RoleClass> oUserRoleList = new List<RoleClass>();
            oSqlManage["ProcedureName"] = "sp_RoleGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oUserRoleList.AddRange(DatabaseHelper.ExecuteQuery<RoleClass>(oSqlManage, oParamRequest));
            return oUserRoleList;
        }

        public override List<UserRoleResponseType> GetAllUserRoleResponseType(string oLanguage)
        {
            List<UserRoleResponseType> oUserRoleList = new List<UserRoleResponseType>();
            //oSqlManage["ProcedureName"] = "sp_UserRoleResponseTypeGetAll";
            //Nattawat S. Change store procedure
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oSqlManage["ProcedureName"] = "sp_UserRoleGetAllCompany";
            oParamRequest["@Language"] = oLanguage;
            oUserRoleList.AddRange(DatabaseHelper.ExecuteQuery<UserRoleResponseType>(oSqlManage, oParamRequest));
            return oUserRoleList;
        }

        public override List<UserRoleResponse> GetUserRoleResponse(string EmployeeID, string CompanyCode)
        {
            List<UserRoleResponse> oUserRoleResponseList = new List<UserRoleResponse>();
            oSqlManage["ProcedureName"] = "sp_UserRoleResponseGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CompanyCode"] = CompanyCode;
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oUserRoleResponseList.AddRange(DatabaseHelper.ExecuteQuery<UserRoleResponse>(oSqlManage, oParamRequest));
            return oUserRoleResponseList;
        }
        /*
        public override void SaveUserRoleResponse(List<UserRoleResponse> lstUserRoleResponse)
        {
            if (lstUserRoleResponse.Count > 0)
            {
                SqlConnection oCon = new SqlConnection(this.BaseConnStr);
                oCon.Open();
                SqlTransaction trn = oCon.BeginTransaction();
                string oAction = "UPDATE UserRole ";
                bool oStatus = false;
                try
                {
                    // delete old user role response
                    SqlCommand oCommand = new SqlCommand("sp_UserRoleDelete", oCon, trn);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", lstUserRoleResponse[0].EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_CompanyCode", lstUserRoleResponse[0].CompanyCode);
                    oCommand.ExecuteNonQuery();
                    oCommand.Dispose();

                    oCommand = new SqlCommand("sp_UserRoleResponseDelete", oCon, trn);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", lstUserRoleResponse[0].EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_CompanyCode", lstUserRoleResponse[0].CompanyCode);
                    oCommand.ExecuteNonQuery();
                    oCommand.Dispose();

                    List<string> oUserRole = new List<string>();

                    foreach (UserRoleResponse oUserRoleResponse in lstUserRoleResponse)
                    {

                        if (oUserRole.Count == 0 || !oUserRole.Contains(oUserRoleResponse.UserRole))
                        {
                            // save user role
                            oUserRole.Add(oUserRoleResponse.UserRole);
                            oCommand = new SqlCommand("sp_UserRoleSave", oCon, trn);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_EmployeeID", oUserRoleResponse.EmployeeID);
                            oCommand.Parameters.AddWithValue("@p_CompanyCode", oUserRoleResponse.CompanyCode);
                            oCommand.Parameters.AddWithValue("@p_UserRole", oUserRoleResponse.UserRole);
                            oCommand.ExecuteNonQuery();
                            oCommand.Dispose();
                        }

                        // save user role response
                        oCommand = new SqlCommand("sp_UserRoleResponseSave", oCon, trn);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_EmployeeID", oUserRoleResponse.EmployeeID);
                        oCommand.Parameters.AddWithValue("@p_CompanyCode", oUserRoleResponse.CompanyCode);
                        oCommand.Parameters.AddWithValue("@p_UserRole", oUserRoleResponse.UserRole);
                        oCommand.Parameters.AddWithValue("@p_ResponseType", oUserRoleResponse.ResponseType);
                        oCommand.Parameters.AddWithValue("@p_ResponseCode", oUserRoleResponse.ResponseCode);
                        oCommand.Parameters.AddWithValue("@p_ResponseCompanyCode", oUserRoleResponse.ResponseCompanyCode);

                        oCommand.ExecuteNonQuery();
                        oCommand.Dispose();
                        oAction += "Completed";
                        oStatus = true;
                    }
                }
                catch (Exception e)
                {
                    trn.Rollback();
                    oCon.Close();
                    oAction += "Failed : " + e.Message;
                    throw e;
                }
                trn.Commit();
                oCon.Close();
                CallActionLog(null, lstUserRoleResponse, oAction, oStatus);
            }
        } back up */

        public override void SaveUserRoleResponse(List<UserRoleResponse> lstUserRoleResponse)
        {
            if (lstUserRoleResponse.Count > 0)
            {
                SqlConnection oCon = new SqlConnection(this.BaseConnStr);
                oCon.Open();
                SqlTransaction trn = oCon.BeginTransaction();
                string oAction = "UPDATE UserRole ";
                bool oStatus = false;
                try
                {
                    // delete old user role response
                    SqlCommand oCommand = new SqlCommand("sp_UserRoleDeleteAll", oCon, trn);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.ExecuteNonQuery();
                    oCommand.Dispose();

                    oCommand = new SqlCommand("sp_UserRoleResponseDeleteAll", oCon, trn);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.ExecuteNonQuery();
                    oCommand.Dispose();

                    List<string> oUserRole = new List<string>();

                    foreach (UserRoleResponse oUserRoleResponse in lstUserRoleResponse)
                    {

                        //if (oUserRole.Count == 0 || !oUserRole.Contains(oUserRoleResponse.UserRole))
                        //{
                        // save user role
                        oUserRole.Add(oUserRoleResponse.UserRole);
                        oCommand = new SqlCommand("sp_UserRoleSave", oCon, trn);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_EmployeeID", oUserRoleResponse.EmployeeID);
                        oCommand.Parameters.AddWithValue("@p_CompanyCode", oUserRoleResponse.CompanyCode);
                        oCommand.Parameters.AddWithValue("@p_UserRole", oUserRoleResponse.UserRole);
                        oCommand.ExecuteNonQuery();
                        oCommand.Dispose();
                        //}

                        // save user role response
                        oCommand = new SqlCommand("sp_UserRoleResponseSave", oCon, trn);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.AddWithValue("@p_EmployeeID", oUserRoleResponse.EmployeeID);
                        oCommand.Parameters.AddWithValue("@p_CompanyCode", oUserRoleResponse.CompanyCode);
                        oCommand.Parameters.AddWithValue("@p_UserRole", oUserRoleResponse.UserRole);
                        oCommand.Parameters.AddWithValue("@p_ResponseType", oUserRoleResponse.ResponseType);
                        oCommand.Parameters.AddWithValue("@p_ResponseCode", oUserRoleResponse.ResponseCode);
                        oCommand.Parameters.AddWithValue("@p_ResponseCompanyCode", oUserRoleResponse.ResponseCompanyCode);
                        oCommand.Parameters.AddWithValue("@p_includeSub", oUserRoleResponse.includeSub);

                        oCommand.ExecuteNonQuery();
                        oCommand.Dispose();
                        oStatus = true;
                    }
                }
                catch (Exception e)
                {
                    trn.Rollback();
                    oCon.Close();
                    oAction = "Failed : " + e.Message;
                    throw e;
                }

                oAction = "Completed";
                trn.Commit();
                oCon.Close();
                // Add Log by Company
                oSqlManage["ProcedureName"] = "sp_CompanyDatabaseGetAll";
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                DataTable oCompanyDB = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);

                if (oCompanyDB.Rows.Count > 0)
                {
                    foreach(DataRow rows in oCompanyDB.Rows)
                    {
                        string oCompanyConStr = rows["ConnectionString"].ToString();
                        var filterDataByCompany = lstUserRoleResponse.Where(data => data.CompanyCode == rows["CompanyCode"].ToString()).ToList();
                        if(filterDataByCompany.Count > 0)
                            CallCationLogAfterSaveUserRoleByCompany(null, filterDataByCompany, oAction, oStatus, oCompanyConStr);
                    }

                }
            }
        }

        //Nattawat S. Add 2021-02-22 add log by each company
        public bool CallCationLogAfterSaveUserRoleByCompany(object objOld, object objNew, string oAction, bool oStatus, string oConStr)
        {
            bool flag = false;
            ActionLog oActionLog = new ActionLog();
            oActionLog.LogAction = oAction;
            oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
            oActionLog.LogData = string.Format("OLD : {0}\r\n,NEW : {1}", (objOld != null) ? LogMgr.GetSerialize(objOld) : "", (objNew != null) ? LogMgr.GetSerialize(objNew) : "");
            oActionLog.LogActionDate = DateTime.Now; //  generate from db
            oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
            oActionLog.LogStatus = oStatus;

            try
            {   
                Dictionary<string, string> oSqlCompanyConStr = new Dictionary<string, string>();
                oSqlCompanyConStr["ProcedureName"] = "sp_ActionLogSet";
                oSqlCompanyConStr["BaseConnStr"] = oConStr;
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_LogAction"] = oActionLog.LogAction;
                oParamRequest["@p_LogData"] = oActionLog.LogData;
                oParamRequest["@p_LogActionBy"] = oActionLog.LogActionBy;
                oParamRequest["@p_LogStatus"] = oActionLog.LogStatus;
                flag = DatabaseHelper.ExecuteNoneQuery(oSqlCompanyConStr, oParamRequest);
                return flag;
            }catch(Exception ex)
            {
                return flag;
            }
            
        }

        public override List<Company> GetCompanyList()
        {
            oSqlManage["ProcedureName"] = "sp_CompanyGet";
            List<Company> oResult = new List<Company>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Company>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override Company GetCompanyByCompanyCode(string sCompanyCode)
        {
            oSqlManage["ProcedureName"] = "sp_CompanyGet";
            Company oResult = new Company();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CompanyCode"] = sCompanyCode;
            oResult = (DatabaseHelper.ExecuteQuery<Company>(oSqlManage, oParamRequest).First());
            return oResult;
        }
        public override Dictionary<string, string> GetModuleSettings(string oCompanyCode, string oModuleID)
        {
            Dictionary<string, string> oModuleSetting = new Dictionary<string, string>();
            oSqlManage["ProcedureName"] = "sp_ModuleSettingsGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["CompanyCode"] = oCompanyCode;
            oParamRequest["ModuleID"] = oModuleID;
            DataTable oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
            foreach (DataRow currentRow in oResult.Rows)
            {
                oModuleSetting[currentRow["Key"].ToString()] = currentRow["Value"].ToString();
            }
            return oModuleSetting;
        }
        public override Dictionary<string, Dictionary<string, Dictionary<string, string>>> GetModuleSettings()
        {
            Dictionary<string, Dictionary<string, Dictionary<string, string>>> oModuleSetting = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();

            oSqlManage["ProcedureName"] = "sp_CompanyDatabaseGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            DataTable oCompanyDB = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);

            // Get default module setting
            oSqlManage["ProcedureName"] = "sp_ModuleSettingsGetAll";
            oParamRequest = new Dictionary<string, object>();
            DataTable oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
            foreach (DataRow currentRow in oResult.Rows)
            {
                string strCompanyCode = "DEFAULT";
                string strModuleID = currentRow["ModuleID"].ToString();
                string strKey = currentRow["Key"].ToString();
                string strValue = currentRow["Value"].ToString();

                if (!oModuleSetting.ContainsKey(strCompanyCode))
                    oModuleSetting[strCompanyCode] = new Dictionary<string, Dictionary<string, string>>();
                if (!oModuleSetting[strCompanyCode].ContainsKey(strModuleID))
                    oModuleSetting[strCompanyCode][strModuleID] = new Dictionary<string, string>();

                oModuleSetting[strCompanyCode][strModuleID][strKey] = strValue;
            }


            // Get module setting from each company
            foreach (DataRow oDBRow in oCompanyDB.Rows)
            {
                Dictionary<string, string> oComapanyManage = new Dictionary<string, string>();
                oComapanyManage["BaseConnStr"] = oDBRow["ConnectionString"].ToString();
                oComapanyManage["ProcedureName"] = "sp_ModuleSettingsGetAll";
                oParamRequest = new Dictionary<string, object>();
                oResult = DatabaseHelper.ExecuteQuery(oComapanyManage, oParamRequest);
                foreach (DataRow currentRow in oResult.Rows)
                {
                    string strCompanyCode = oDBRow["CompanyCode"].ToString();
                    string strModuleID = currentRow["ModuleID"].ToString();
                    string strKey = currentRow["Key"].ToString();
                    string strValue = currentRow["Value"].ToString();

                    if (!oModuleSetting.ContainsKey(strCompanyCode))
                        oModuleSetting[strCompanyCode] = new Dictionary<string, Dictionary<string, string>>();
                    if (!oModuleSetting[strCompanyCode].ContainsKey(strModuleID))
                        oModuleSetting[strCompanyCode][strModuleID] = new Dictionary<string, string>();

                    oModuleSetting[strCompanyCode][strModuleID][strKey] = strValue;
                }
            }
            return oModuleSetting;
        }
        public override void CopyUserRoleAndResponse(string sCompanyCode)
        {
            Dictionary<string, object> oParamRequest;

            // Get Company DB connetion string
            oSqlManage["ProcedureName"] = "sp_CompanyDatabaseGetByCompanyCode";
            oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CompanyCode"] = sCompanyCode;
            DataTable oCompanyDBTable = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
            string sConnectionStr = oCompanyDBTable.Rows[0]["ConnectionString"].ToString();

            // Get UserRole from share db
            oSqlManage["ProcedureName"] = "sp_UserRoleGetByCompanyCode";
            oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CompanyCode"] = sCompanyCode;
            DataTable oUserRoleTable = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);

            // Set up connection to companydb
            Dictionary<string, string> oCompanyManage = new Dictionary<string, string>();
            oCompanyManage["BaseConnStr"] = sConnectionStr;

            // Clear old data
            oCompanyManage["ProcedureName"] = "sp_UserRoleDelete";
            oParamRequest = new Dictionary<string, object>();
            DatabaseHelper.ExecuteNoneQuery(oCompanyManage, oParamRequest);

            // Save new data
            foreach (DataRow oRow in oUserRoleTable.Rows)
            {
                oCompanyManage["ProcedureName"] = "sp_UserRoleSave";
                oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_EmployeeID"] = oRow["EmployeeID"].ToString();
                oParamRequest["@p_CompanyCode"] = oRow["CompanyCode"].ToString();
                oParamRequest["@p_UserRole"] = oRow["UserRole"].ToString();
                DatabaseHelper.ExecuteNoneQuery(oCompanyManage, oParamRequest);
            }

            // Get UserRoleResponse from share db
            oSqlManage["ProcedureName"] = "sp_UserRoleResponseGetByCompanyCode";
            oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CompanyCode"] = sCompanyCode;
            DataTable oUserRoleResponseTable = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);

            // Clear old data
            oCompanyManage["ProcedureName"] = "sp_UserRoleResponseDelete";
            oParamRequest = new Dictionary<string, object>();
            DatabaseHelper.ExecuteNoneQuery(oCompanyManage, oParamRequest);

            // Save new data
            foreach (DataRow oRow in oUserRoleResponseTable.Rows)
            {
                oCompanyManage["ProcedureName"] = "sp_UserRoleResponseSave";
                oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_EmployeeID"] = oRow["EmployeeID"].ToString();
                oParamRequest["@p_CompanyCode"] = oRow["CompanyCode"].ToString();
                oParamRequest["@p_UserRole"] = oRow["UserRole"].ToString();
                oParamRequest["@p_ResponseType"] = oRow["ResponseType"].ToString();
                oParamRequest["@p_ResponseCode"] = oRow["ResponseCode"].ToString();
                oParamRequest["@p_ResponseCompanyCode"] = oRow["ResponseCompanyCode"].ToString();
                oParamRequest["@p_IncludeSub"] = oRow["includeSub"];
                DatabaseHelper.ExecuteNoneQuery(oCompanyManage, oParamRequest);
            }
        }
        public override Dictionary<string, string> GetDatabaseLocation()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            if (oSqlManage["BaseConnStr"].ToLower().Contains(DataBaseLocation.Test.ToString().ToLower()))
            {
                oResult["DataBaseLocation"] = "Environment Test";
            }
            else if (oSqlManage["BaseConnStr"].ToLower().Contains(DataBaseLocation.Srt.ToString().ToLower()))
            {
                oResult["DataBaseLocation"] = "Environment Srt";
            }
            else if (oSqlManage["BaseConnStr"].ToLower().Contains(DataBaseLocation.Prd.ToString().ToLower()))
            {
                oResult["DataBaseLocation"] = "Environment Production";
            }
            return oResult;
        }

        public bool CallActionLog(object objOld, object objNew, string oAction, bool oStatus)
        {
            bool flg = false;
            ActionLog oActionLog = new ActionLog();
            oActionLog.LogAction = oAction;
            oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
            oActionLog.LogData = string.Format("OLD : {0}\r\n,NEW : {1}", (objOld != null) ? LogMgr.GetSerialize(objOld) : "", (objNew != null) ? LogMgr.GetSerialize(objNew) : "");
            oActionLog.LogActionDate = DateTime.Now; //  generate from db
            oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
            oActionLog.LogStatus = oStatus;

            flg = InsertActionLog(oActionLog);
            return flg;
        }

        public bool InsertActionLog(ActionLog oActionLog)
        {
            bool flg = false;
            oSqlManage["ProcedureName"] = "sp_ActionLogSet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LogAction"] = oActionLog.LogAction;
            oParamRequest["@p_LogData"] = oActionLog.LogData;
            oParamRequest["@p_LogActionBy"] = oActionLog.LogActionBy;
            oParamRequest["@p_LogStatus"] = oActionLog.LogStatus;
            flg = DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            return flg;
        }
        #region " send mail "
        private string EXCHANGESERVER
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.MAIL.EXCHANGE", "EXCHANGESERVER");
            }
        }
        private string EXCHANGESERVERPORT
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.MAIL.EXCHANGE", "EXCHANGESERVERPORT");
            }
        }
        public void send_mail(string Username, string InitPass, string Mailto, string UserEmail, string MailBody, string CompanyName, string Flag)
        {
            string[] CompanyNames = CompanyName.Split('|');
            string CompanyNameSelect = "", htmlString = "";
            for (int i = 0; i < CompanyNames.Length; i++)
            {
                if (CompanyNames[i] != "")
                {
                    CompanyNameSelect += CompanyNames[i] + ",";
                }

            }

            string cMailBody = UserEmail + MailBody + InitPass;
            if (Flag == "Ins")
            {
                CompanyNameSelect = CompanyNameSelect.Remove(CompanyNameSelect.Length - 1);
                htmlString = @"<html>
                      <body>
                     <p>Automatic password generation for IT administrators to support DESS systems.</p>
                      <p>Email:>" + UserEmail + "</p>" +
                          "<p>UserName :>" + Username + "</p>" +
                          "<p>Password:>" + InitPass + "</p>" +
                          "<p>Company:>" + CompanyNameSelect + "</p>" +
                          "<p>If you find this information correct Please forward to the requested email.</p>" +
                          "</body>" +
                          "</html>"
          ;
            }
            else
            {
                CompanyNameSelect = AdminBypassCompanyList(Username, UserEmail);
                CompanyNameSelect = CompanyNameSelect.Remove(CompanyNameSelect.Length - 1);
                htmlString = @"<html>
                      <body>
                     <p>Automatic password generation for IT administrators to support DESS systems.</p>
                      <p>Email:>" + UserEmail + "</p>" +
                "<p>UserName :>" + Username + "</p>" +
                "<p>Password:>" + InitPass + "</p>" +
                "<p>Company:>" + CompanyNameSelect + "</p>" +
                "</body>" +
                "</html>"
;
            }

            string EXCHANGESERVER = ShareDataManagement.LookupCache("DEFAULT", "ESS.MAIL.EXCHANGE", "EXCHANGESERVER");
            string EXCHANGESERVERPORT = ShareDataManagement.LookupCache("DEFAULT", "ESS.MAIL.EXCHANGE", "EXCHANGESERVERPORT");
            #region " send mail "
            System.Net.Mail.SmtpClient oClient = new System.Net.Mail.SmtpClient(EXCHANGESERVER, Convert.ToInt16(EXCHANGESERVERPORT));
            System.Net.Mail.MailAddress oSender = new System.Net.Mail.MailAddress(GetEmail("SystemMail", "Sup", Username), "WORKFLOW SYSTEM");
            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress oTo = new System.Net.Mail.MailAddress(Mailto);

            //EmployeeData oEmp = new EmployeeData(Employeeid);
            oTo = new System.Net.Mail.MailAddress(Mailto, Username);
            oMessage.To.Add(oTo);
            oClient.Timeout = 3000;
            oMessage.ReplyTo = oTo;
            oMessage.From = oSender;
            oMessage.Sender = oSender;
            oMessage.Subject = GetEmail("MSubject", "MSubject", Username);
            oMessage.Body = htmlString;
            oMessage.BodyEncoding = Encoding.UTF8;
            oMessage.IsBodyHtml = true;
            oClient.Send(oMessage);
            oMessage.Dispose();

            #endregion


        }

        #endregion
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public string CompanyCode { get; set; }
        public override string MaintainAdminBypass(string Username, string Password, string Email, string Firstlogin, string ResponseID, string CompanyCodes, string CompanyName, string CurrentCompany)
        {
            //string[] countCompanyCodes = CompanyCodes.Split('|');
            //string[] countCompanyName = CompanyName.Split('|');
            CompanyCode = CurrentCompany;
            DataSet ds = new DataSet();
            bool flg = false;
            string init_password = "", status = "";
            init_password = RandomString(8);
            if (CompanyCodes == "")
            {
                return "กรุณาเลือกบริษัทอย่างน้อย 1 บริษัท";
            }
            bool IsValidEmail(string email)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(email);
                    return addr.Address == email;
                }
                catch
                {
                    return false;
                }
            }
            if (!IsValidEmail(Email))
            {
                return status = "รูปแบบ email ไม่ถูกต้อง";
            }
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            Dictionary<string, object> oParamRequest2 = new Dictionary<string, object>();
            oParamRequest["@Flag"] = "Ins";
            oParamRequest["@Username"] = Username;
            oParamRequest["@Password"] = Encrypt(init_password);
            oParamRequest["@Email"] = Email;
            oParamRequest["@FirstLogin"] = Firstlogin;

            oSqlManage["ProcedureName"] = "sp_Admin_bypass_register";
            try
            {
                ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
                if (ds.Tables[0].Rows[0][0].ToString() == "Ins Complete")
                {
                    send_mail(oParamRequest["@Username"].ToString(), init_password, GetEmail("Email", "Sup", Username), Email, GetEmail("EMbody", "Body", Username), CompanyName, "Ins");
                }
                else
                {
                    return status = ds.Tables[0].Rows[0][0].ToString();
                }
            }
            catch
            {
                oParamRequest["@Flag"] = "Del";
                oParamRequest["@Username"] = Username;
                oParamRequest["@Password"] = Encrypt(init_password);
                oParamRequest["@Email"] = Email;
                oParamRequest["@FirstLogin"] = Firstlogin;
                oSqlManage["ProcedureName"] = "sp_Admin_bypass_register";
                flg = DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
                return status = "Cannot Send an Email / ไม่สามารถส่ง email ได้ กรุณาติดต่อทีมซัพพอร์ท";
            }
            try
            {
                for (int index = 0; index < CompanyCodes.Split('|').Length; index++)
                {
                    if (CompanyCodes.Split('|')[index] != "")
                    {
                        oParamRequest2["@Username"] = Username;
                        oParamRequest2["@Email"] = Email;
                        oParamRequest2["@ResponseID"] = AdminBypassCompany();
                        oParamRequest2["CompanyCode"] = CompanyCodes.Split('|')[index];
                        oParamRequest2["CompanyName"] = CompanyName.Split('|')[index];
                        oParamRequest2["@Flag"] = "INS";
                        oSqlManage["ProcedureName"] = "sp_Admin_bypass_Company";
                        flg = DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest2);
                    }
                }
                //for (int i = 0; i <= (countCompanyCodes.Length - 2); i++)
                //{
                //    oParamRequest2["@ResponseID"] = AdminBypassCompany();
                //    oParamRequest2["CompanyCode"] = countCompanyCodes[i];
                //    oParamRequest2["CompanyName"] = countCompanyName[i];
                //    oParamRequest2["@Flag"] = "INS";
                //    oSqlManage["ProcedureName"] = "sp_Admin_bypass_Company";
                //    flg = DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest2);
                //}
            }
            catch (Exception e)
            {
                return status = "ErrorInsertCompany";
            }

            return status = "Success";
        }
        private string Encrypt(string pws)
        {
            PasswordDeriveBytes PDB;
            RC2CryptoServiceProvider RC2CSP;
            Int32 IVSize;
            Byte[] IV;
            Int32 Counter;
            Byte[] Seed = { 0x05 , 0x02 , 0x06 ,0x04,
                              0x01 , 0x07 , 0x03 ,0x08 };
            PDB = new PasswordDeriveBytes(pws, Seed, "MD5", 5);
            RC2CSP = new RC2CryptoServiceProvider();
            IVSize = RC2CSP.BlockSize / 8;
            IV = new Byte[IVSize];
            for (Counter = 0; Counter < IV.Length; Counter++)
            {
                IV[Counter] = Convert.ToByte(Counter);
            }
            RC2CSP.Key = PDB.CryptDeriveKey("RC2", "MD5", RC2CSP.KeySize, IV);
            string strRet = "";
            for (Counter = 0; Counter < RC2CSP.Key.Length; Counter++)
            {
                strRet += int.Parse(RC2CSP.Key.GetValue(Counter).ToString()).ToString("000");
            }
            return strRet;
        }
        public override string AdminBypassLogin(string Username, string Password, string Flag, string Company)
        {

            DataSet ds;
            string status = "";
            oSqlManage["ProcedureName"] = "sp_Admin_bypass_Login";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@Username"] = Username;
            oParamRequest["@Password"] = Encrypt(Password);
            oParamRequest["@Flag"] = Flag;
            oParamRequest["@Company"] = Company;
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            try
            {
                status = ds.Tables[0].Rows[0][0].ToString();
            }
            catch { status = "เกิดข้อผิดพลาด กรุณาติดต่อทีมซัพพอร์ท"; }
            return status;
        }
        public string AdminBypassCompanyList(string Username, string Email)
        {
            DataSet ds;
            string comp_name = "";
            oSqlManage["ProcedureName"] = "sp_Admin_bypass_Company";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@Username"] = Username;
            oParamRequest["@Email"] = Email;
            oParamRequest["@ResponseID"] = "";
            oParamRequest["@CompanyCode"] = "";
            oParamRequest["@CompanyName"] = "";
            oParamRequest["@Flag"] = "COMP";
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            //comp_name = ds.Tables[0].Rows[0][0].ToString();
            if (ds.Tables != null)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i][0].ToString() != "")
                        comp_name += ds.Tables[0].Rows[i][0].ToString() + ",";
                }

            }
            return comp_name;
        }
        public override int AdminBypassCompany()
        {
            DataSet ds;
            int response_id = 0;
            oSqlManage["ProcedureName"] = "sp_Admin_bypass_Company";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@Username"] = "";
            oParamRequest["@Email"] = "";
            oParamRequest["@ResponseID"] = "";
            oParamRequest["@CompanyCode"] = "";
            oParamRequest["@CompanyName"] = "";
            oParamRequest["@Flag"] = "S";
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            response_id = int.Parse(ds.Tables[0].Rows[0][0].ToString());
            return response_id;
        }
        public override string AdminBypassResetPassword(string Username, string Flag, string Company, string CompanyName)
        {
            DataSet ds;
            string reset_pass = RandomString(8);

            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oSqlManage["ProcedureName"] = "sp_Admin_bypass_Login";
            oParamRequest["@Username"] = Username;
            oParamRequest["@Password"] = Encrypt(reset_pass);
            oParamRequest["@Flag"] = Flag;
            oParamRequest["@Company"] = Company;

            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0].ToString() == "Success")
                {
                    try
                    {
                        send_mail(Username, reset_pass, GetEmail("Email", "User", Username), GetEmail("Email", "User", Username), GetEmail("EMbody", "Body", Username), CompanyName, "Re");
                    }
                    catch
                    {
                        oSqlManage["ProcedureName"] = "sp_Admin_bypass_Login";
                        oParamRequest["@Username"] = Username;
                        oParamRequest["@Password"] = Encrypt(reset_pass);
                        oParamRequest["@Flag"] = "F";
                        oParamRequest["@Company"] = Company;

                        ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
                        return "ไม่สามารถส่งรหัสผ่านไปยัง email ได้ กรุณาติดต่อ Admin";
                    }
                }
            }
            else
            {
                return "ไม่พบข้อมูล User ในระบบ";
            }
            return ds.Tables[0].Rows[0][0].ToString();


        }
        public override void AdminBypassSaveLog(string Username, string EmployeeCode, string CompanyCode, string Description, string IPAddress, string ClientName)
        {
            string addressip = "";
            string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            addressip = IPAddress;
            HttpContext context = HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    ip = addresses[0];
                }
            }
            string user_addr = HttpContext.Current.Request.UserHostAddress;
            ip = context.Request.ServerVariables["REMOTE_ADDR"];

            string strHostName = Dns.GetHostName();
            string clientIPAddress = Dns.GetHostAddresses(strHostName).GetValue(1).ToString();

            string MainAppLogin = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            DataSet ds;
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oSqlManage["ProcedureName"] = "sp_Admin_bypass_log";
            oParamRequest["@Username"] = Username;
            oParamRequest["@EmployeeCode"] = EmployeeCode;
            oParamRequest["@CompanyCode"] = CompanyCode;
            oParamRequest["@Description"] = Description;
            oParamRequest["@IPAddress"] = "Network IP " + addressip + "/Client IP " + ip;
            oParamRequest["@ClientName"] = strHostName;
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
        }
        public override List<BypassCompany> GetBypassCompanyList()
        {
            oSqlManage["ProcedureName"] = "sp_GetBypassCompanyList";
            List<BypassCompany> oResult = new List<BypassCompany>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<BypassCompany>(oSqlManage, oParamRequest));
            return oResult;
        }
        public string GetEmail(string Key, string Flag, string Username)
        {
            string Email = "";
            DataSet ds;
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oSqlManage["ProcedureName"] = "sp_Admin_bypass_get_email";
            oParamRequest["@Key"] = Key;
            oParamRequest["@Flag"] = Flag;
            oParamRequest["@Username"] = Username;

            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            Email = (ds.Tables[0].Rows[0][0].ToString());
            return Email;
        }

    }

    public enum DataBaseLocation
    {
        Prd = 0,
        Srt = 1,
        Test = 2
    }
}