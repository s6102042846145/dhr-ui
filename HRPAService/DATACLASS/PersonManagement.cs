﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonManagement : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string OrganizationName { get; set; }
        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        
    }
}
