﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
namespace ESS.HR.PA.DATACLASS
{
    public class EducationInfo : AbstractObject
    {
        private PersonalEducationCenter __education = new PersonalEducationCenter();
        public PersonalEducationCenter Education
        {
            get { return __education; }
            set { __education = value; }
        }

        private PersonalEducationCenter __educatuinOld = new PersonalEducationCenter();
        public PersonalEducationCenter EducationOld
        {
            get { return __educatuinOld; }
            set { __educatuinOld = value; }

        }
    }
}
