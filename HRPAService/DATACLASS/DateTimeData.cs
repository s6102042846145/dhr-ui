﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.CONFIG.PA;

namespace ESS.HR.PA.DATACLASS
{
    public class DateTimeData : AbstractObject
    {
       
        public string Zyear { get; set; }
        public string Zmonth { get; set; }
        public string Zday { get; set; }

    }
}
