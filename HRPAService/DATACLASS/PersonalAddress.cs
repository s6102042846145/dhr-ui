﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    class PersonalAddress : AbstractObject
    {
        private List<PersonalAddressCenter> __personalAddr = new List<PersonalAddressCenter>();
        public List<PersonalAddressCenter> PersonalAddr
        {
            get
            {
                return __personalAddr;
            }
            set
            {
                __personalAddr = value;
            }
        }

        private List<PersonalAddressCenter> __personalAddrNew = new List<PersonalAddressCenter>();
        public List<PersonalAddressCenter> PersonalAddrNew
        {
            get
            {
                return __personalAddrNew;
            }
            set
            {
                __personalAddrNew = value;
            }
        }
        //private List<INFOTYPE0006> __personalAddr = new List<INFOTYPE0006>();
        //public List<INFOTYPE0006> PersonalAddr
        //{
        //    get
        //    {
        //        return __personalAddr;
        //    }
        //    set
        //    {
        //        __personalAddr = value;
        //    }
        //}

        //private List<INFOTYPE0006> __personalAddrNew = new List<INFOTYPE0006>();
        //public List<INFOTYPE0006> PersonalAddrNew
        //{
        //    get
        //    {
        //        return __personalAddrNew;
        //    }
        //    set
        //    {
        //        __personalAddrNew = value;
        //    }
        //}

        private List<PersonalAddressControl> __personalAddressControl = new List<PersonalAddressControl>();
        public List<PersonalAddressControl> PersonalAddressControl
        {
            get
            {
                return __personalAddressControl;
            }
            set
            {
                __personalAddressControl = value;
            }
        }

        private List<PersonalAddressType> __personalAddressType = new List<PersonalAddressType>();
        public List<PersonalAddressType> PersonalAddressType
        {
            get
            {
                return __personalAddressType;
            }
            set
            {
                __personalAddressType = value;
            }
        }
    }
}
