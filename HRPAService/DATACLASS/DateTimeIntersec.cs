﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class DateTimeIntersec
    {
        // Fields...
        private Double _EndNotOverlap;
        private Double _BeginNotOverlap;
        private Double _IntersecMinute;
        private DateTime _End2;
        private DateTime _Begin2;
        private DateTime _End1;
        private DateTime _Begin1;
        private DateTime _EndBound;
        private DateTime _BeginBound;

        public DateTime End2
        {
            get { return _End2; }
            set
            {
                _End2 = value;
            }
        }

        public DateTime Begin2
        {
            get { return _Begin2; }
            set
            {
                _Begin2 = value;
            }
        }

        public DateTime End1
        {
            get { return _End1; }
            set
            {
                _End1 = value;
            }
        }

        public DateTime Begin1
        {
            get { return _Begin1; }
            set
            {
                _Begin1 = value;
            }
        }

        public DateTime EndBound
        {
            get { return _EndBound; }
            set
            {
                _EndBound = value;
            }
        }

        public DateTime BeginBound
        {
            get { return _BeginBound; }
            set
            {
                _BeginBound = value;
            }
        }

        /// <summary>
        /// Number of Minute
        /// End1 - End2
        /// </summary>
        public Double EndNotOverlap
        {
            get
            {
                _EndNotOverlap = ((TimeSpan)(_End1 - _End2)).TotalMinutes;
                return _EndNotOverlap;
            }

        }

        /// <summary>
        /// Number of Minute
        /// Begin2 -Begin1
        /// </summary>
        public Double BeginNotOverlap
        {
            get
            {
                _BeginNotOverlap = ((TimeSpan)(_Begin2 - _Begin1)).TotalMinutes;
                return _BeginNotOverlap;
            }

        }

        public Double IntersecMinute
        {
            get
            {
                if (_BeginBound == DateTime.MinValue || _EndBound == DateTime.MinValue)
                    return 0;
                else
                {
                    _IntersecMinute = ((TimeSpan)(_EndBound - _BeginBound)).TotalMinutes;
                    return _IntersecMinute;
                }
            }

        }

        public void Intersection()
        {
            _BeginBound = _Begin1 > _Begin2 ? _Begin1 : _Begin2;
            _EndBound = _End1 > _End2 ? _End2 : _End1;
        }

        public bool IsIntersecOT()
        {
            if (_Begin1 == DateTime.MinValue | _End1 == DateTime.MinValue | _Begin2 == DateTime.MinValue | _End2 == DateTime.MinValue)
                return false;

            if (_Begin1 > _End2 | _End1 < _Begin2)
                return false;
            else
                return true;
        }

        public bool IsIntersecOT_ClockInClockOut()
        {
            if (_Begin1 == DateTime.MinValue | _End1 == DateTime.MinValue | _Begin2 == DateTime.MinValue | _End2 == DateTime.MinValue)
                return false;

            if (_Begin1 > _Begin2 && _End1 < _End2)
                return false;
            else
                return true;
        }

        public bool IsIntersecAttendanceType
        {
            get
            {
                if (_Begin1 == DateTime.MinValue | _End1 == DateTime.MinValue | _Begin2 == DateTime.MinValue | _End2 == DateTime.MinValue)
                    return false;

                if (_Begin1 >= _End2 | _End1 <= _Begin2)
                    return false;
                else
                    return true;
            }
        }

        public DateTime PreImplementBegin
        {
            get
            {
                if (IsIntersecOT())
                    return _Begin1 < _Begin2 ? _Begin1 : _Begin1 == _Begin2 ? DateTime.MinValue : _Begin2;
                else
                    return DateTime.MinValue;
            }
        }

        public DateTime PreImplementEnd
        {
            get
            {
                if (IsIntersecOT())
                    return _Begin1 > _Begin2 ? _Begin1 : _Begin1 == _Begin2 ? DateTime.MinValue : _Begin2;
                else
                    return DateTime.MinValue;
            }
        }

        public DateTime PostImplementBegin
        {
            get
            {
                if (IsIntersecOT())
                    return _End1 < _End2 ? _End1 : _End1 == _End2 ? DateTime.MinValue : _End2;
                else
                    return DateTime.MinValue;
            }
        }

        public DateTime PostImplementEnd
        {
            get
            {
                if (IsIntersecOT())
                    return _End1 > _End2 ? _End1 : _End1 == _End2 ? DateTime.MinValue : _End2;
                else
                    return DateTime.MinValue;
            }
        }
    }
}
