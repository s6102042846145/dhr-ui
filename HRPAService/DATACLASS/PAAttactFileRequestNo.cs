﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.CONFIG.PA;

namespace ESS.HR.PA.DATACLASS
{
    public class PAAttactFileRequestNo : AbstractObject
    {
        public string RequestNo { get; set; }
        public List<PAAttactFileContent> ContentItem;
    }
}
