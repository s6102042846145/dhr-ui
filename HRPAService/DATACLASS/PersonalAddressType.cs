﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    class PersonalAddressType
    {
        private string __key;
        public string Key
        {
            get
            {
                return __key;
            }
            set
            {
                __key = value;
            }
        }

        private bool __delete = false;
        public bool Delete
        {
            get
            {
                return __delete;
            }
            set
            {
                __delete = value;
            }
        }
    }
}
