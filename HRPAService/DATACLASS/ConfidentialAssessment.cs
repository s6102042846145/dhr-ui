﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
using DHR.HR.API.Model;

namespace ESS.HR.PA.DATACLASS
{
    public class ConfidentialAssessment : AbstractObject
    {
        private PAEvaluationInf __confidential = new PAEvaluationInf();
        public PAEvaluationInf Confidential
        {
            get { return __confidential; }
            set { __confidential = value; }
        }

        private PAEvaluationInf __confidentialOld = new PAEvaluationInf();
        public PAEvaluationInf ConfidentialOld
        {
            get { return __confidentialOld; }
            set { __confidentialOld = value; }

        }
    }
}
