﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;


namespace ESS.HR.PA.DATACLASS
{
    public class WorkPeriod : AbstractObject
    { 
        private string __ID;
        private string __START;
        private string __END;
        private string __ACTION;
        private string __POS_ID;
        private string __POS_NAME;
        private string __POS_NAME_EN;
        private string __LEVEL;
        private string __ORG_ID;
        private string __ORG_NAME;
        private string __ORG_NAME_EN;
        private string __REF_NO;
        private string __EFFDATE;
        private string __ANTYPE;
        private string __LINE;
        private string __BUSUNIT;


        public string EMPLOYEEID
        {
            get
            {
                return __ID;
            }
            set
            {
                __ID = value;
            }
        }

        public string STARTDATE
        {
            get
            {
                return __START;
            }
            set
            {
                __START = value;
            }
        }
        public string ENDDATE
        {
            get
            {
                return __END;
            }
            set
            {
                __END = value;
            }
        }

        public string ACTION
        {
            get
            {
                return __ACTION;
            }
            set
            {
                __ACTION = value;
            }
        }
        public string POS_ID
        {
            get
            {
                return __POS_ID;
            }
            set
            {
                __POS_ID = value;
            }
        }

        public string POS_NAME
        {
            get
            {
                return __POS_NAME;
            }
            set
            {
                __POS_NAME = value;
            }
        }

        public string POS_NAME_EN
        {
            get
            {
                return __POS_NAME_EN;
            }
            set
            {
                __POS_NAME_EN = value;
            }
        }

        public string LEVEL
        {
            get
            {
                return __LEVEL;
            }
            set
            {
                __LEVEL = value;
            }
        }

        public string ORG_ID
        {
            get
            {
                return __ORG_ID;
            }
            set
            {
                __ORG_ID = value;
            }
        }

        public string ORG_NAME
        {
            get
            {
                return __ORG_NAME;
            }
            set
            {
                __ORG_NAME = value;
            }
        }

        public string ORG_NAME_EN
        {
            get
            {
                return __ORG_NAME_EN;
            }
            set
            {
                __ORG_NAME_EN = value;
            }
        }

        public string REF_NO
        {
            get
            {
                return __REF_NO;
            }
            set
            {
                __REF_NO = value;
            }
        }

        public string EFFDATE
        {
            get
            {
                return __EFFDATE;
            }
            set
            {
                __EFFDATE = value;
            }
        }

        public string ANTYPE
        {
            get
            {
                return __ANTYPE;
            }
            set
            {
                __ANTYPE = value;
            }
        }

        public string LINE
        {
            get
            {
                return __LINE;
            }
            set
            {
                __LINE = value;
            }
        }

        public string BUSUNIT
        {
            get
            {
                return __BUSUNIT;
            }
            set
            {
                __BUSUNIT = value;
            }
        }
        public string WorkTime { get; set; }

        //public static List<WorkPeriod> Get(string employeeid)
        //{
        //    List<WorkPeriod> returnValue = new List<WorkPeriod>();
        //    returnValue = ServiceManager.ERPEmployeeService.GetWorkHistory(employeeid);
        //    returnValue.Sort(new WorkPeriodComparer());
        //    return returnValue;

        //}
        //public class WorkPeriodComparer : IComparer<WorkPeriod>
        //{
        //    //public int Compare(WorkPeriod x, WorkPeriod y)
        //    //{
        //    //    return (DateTime.ParseExact(x.STARTDATE, "dd/MM/yyyy", new CultureInfo("en-US")).CompareTo(DateTime.ParseExact(y.STARTDATE, "dd/MM/yyyy", new CultureInfo("en-US"))));
        //    //}
        //}
    }
}
