﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using SAPInterfaceExt;
namespace ESS.HR.PA.DATACLASS
{
    public class RelatedAge : AbstractObject
    {

        private string __employeeID = "";
        //private ZHRPES056 __ageOfOrg;
        //private ZHRPES056 __ageOfLevel;
        //private ZHRPES056 __ageOfPosition;
        //private ZHRPES056 __ageOfCompany;
        private DateTime __viewDate = DateTime.Now;
        private DateTimeData __ageOfOrg;
        private DateTimeData __ageOfLevel;
        private DateTimeData __ageOfPosition;
        private DateTimeData __ageOfCompany;

        //Constant AgeType
        private const string AgeTypeOfOrg = "01";
        private const string AgeTypeOfLevel = "02";
        private const string AgeTypeOfPosition = "03";
        private const string AgeTypeOfCompany = "04";
        private const string AgeTypeOfAll = "";

        public RelatedAge()
        {

        }
        //public RelatedAge(string EmployeeID)
        //{

        //    __employeeID = EmployeeID;
        //    __viewDate = DateTime.Now;
        //    RelatedAge _relatedAge = ESS.HR.PA.ServiceManager.HRPAService.GetRelatedAge(this.EmployeeID, this.ViewDate);
        //    this.ageOfCompany = _relatedAge.ageOfCompany;
        //    this.ageOfLevel = _relatedAge.ageOfLevel;
        //    this.ageOfOrg = _relatedAge.ageOfOrg;
        //    this.ageOfPosition = _relatedAge.ageOfPosition;
        //}

        //public RelatedAge(string EmployeeID, DateTime ViewDate)
        //{
        //    __employeeID = EmployeeID;
        //    __viewDate = ViewDate;
        //    RelatedAge _relatedAge = ESS.HR.PA.ServiceManager.HRPAService.GetRelatedAge(this.EmployeeID, this.ViewDate);
        //    this.ageOfCompany = _relatedAge.ageOfCompany;
        //    this.ageOfLevel = _relatedAge.ageOfLevel;
        //    this.ageOfOrg = _relatedAge.ageOfOrg;
        //    this.ageOfPosition = _relatedAge.ageOfPosition;
        //}

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public DateTime ViewDate
        {
            get
            {
                return __viewDate;
            }
            set
            {
                __viewDate = value;
            }
        }

        public DateTimeData ageOfOrg
        {
            get
            {
                return __ageOfOrg;
            }
            set
            {
                __ageOfOrg = value;
            }

        }

        public DateTimeData ageOfLevel
        {
            get
            {
                return __ageOfLevel;
            }
            set
            {
                __ageOfLevel = value;
            }
        }

        public DateTimeData ageOfPosition
        {
            get
            {
                return __ageOfPosition;
            }
            set
            {
                __ageOfPosition = value;
            }
        }

        public DateTimeData ageOfCompany
        {
            get
            {
                return __ageOfCompany;
            }
            set
            {
                __ageOfCompany = value;
            }
        }

    }
}
