﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonalAttachmentFileSet : AbstractObject
    {
        public PersonalAttachmentFileSet()
        {

        }
        public int FileID { get; set; }
        public string RequestNo { get; set; }
        public string FileName { get; set; }
        public int FileSetID { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public byte[] Data { get; set; }
        public bool IsDelete { get; set; }
    }

}
