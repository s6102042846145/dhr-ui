﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PA.INFOTYPE;
using DHR.HR.API.Model;

namespace ESS.HR.PA.DATACLASS
{
    public class ConfidentialInfo : AbstractObject
    {
        private PAPrivateInf __confidential = new PAPrivateInf();
        public PAPrivateInf Confidential
        {
            get { return __confidential; }
            set { __confidential = value; }
        }

        private PAPrivateInf __confidentialOld = new PAPrivateInf();
        public PAPrivateInf ConfidentialOld
        {
            get { return __confidentialOld; }
            set { __confidentialOld = value; }

        }
    }
}
