﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class ControlDropdownlistDataPAInt : AbstractObject
    {
        public string DLL_DATA { get; set; }
        public int DLL_VALUE { get; set; }
    }
}
