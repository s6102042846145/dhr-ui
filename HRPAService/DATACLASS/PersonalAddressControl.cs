﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    class PersonalAddressControl
    {

        private string __requestor;
        public string Requestor
        {
            get
            {
                return __requestor;
            }
            set
            {
                __requestor = value;
            }
        }

        private DateTime __requestor_Effective;
        public DateTime Requestor_Effective
        {
            get
            {
                return __requestor_Effective;
            }
            set
            {
                __requestor_Effective = value;
            }
        }

        private int __fileCount;
        public int FileCount
        {
            get
            {
                return __fileCount;
            }
            set
            {
                __fileCount = value;
            }
        }
    }
}
