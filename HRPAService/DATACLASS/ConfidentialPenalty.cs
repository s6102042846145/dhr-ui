﻿using DHR.HR.API.Model;
using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class ConfidentialPenalty : AbstractObject
    {
        private PAPunishmentInf __confidential = new PAPunishmentInf();
        public PAPunishmentInf Confidential
        {
            get { return __confidential; }
            set { __confidential = value; }
        }

        private PAPunishmentInf __confidentialOld = new PAPunishmentInf();
        public PAPunishmentInf ConfidentialOld
        {
            get { return __confidentialOld; }
            set { __confidentialOld = value; }

        }
    }
}