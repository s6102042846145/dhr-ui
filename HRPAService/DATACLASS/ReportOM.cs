﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class ReportOM : AbstractObject
    {
        public DataTable OrgStructureSnapshot { get; set; }
        public string Remark { get; set; }
    }
}
