﻿using ESS.SHAREDATASERVICE;

namespace ESS.HR.PA
{
    public class HRPAServices
    {
        public HRPAServices()
        {

        }

        private static string ModuleID = "ESS.PA.SVC";
        private string CompanyCode { get; set; }

        public static HRPAServices SVC(string oCompanyCode)
        {
            HRPAServices oHRPAServices = new HRPAServices()
            {
                CompanyCode = oCompanyCode
            };
            return oHRPAServices;
        }

        #region HRPAConfigService
        //List<PAConfiguration> GetPAConfigurationList()
        public string PA_GetPAConfigurationList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PACONFIGURATIONLIST");
            }
        }

        //List<TitleName> GetTitleList(string LanguageCode)
        public string PA_GetTitleList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_TITLELIST_LANGUAGECODE");
            }
        }

        //TitleName GetTitle(string Code)
        public string PA_GetTitle_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_TITLE_CODE");
            }
        }

        //TitleName GetTitle(string Code, string LanguageCode)
        public string PA_GetTitle_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_TITLE_CODE_LANGUAGECODE");
            }
        }

        //SaveTitleList(List<TitleName> data)
        public string PA_SaveTitleList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_TITLELIST_DATA");
            }
        }

        //List<PrefixName> GetPrefixNameList()
        public string PA_GetPrefixNameList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PREFIXNAMELIST_LANGUAGECODE");
            }
        }

        public string PA_GetPrefixNameList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PREFIXNAMELIST");
            }
        }
        //PrefixName GetPrefixName(string Code)
        public string PA_GetPrefixName_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PREFIXNAME_CODE");
            }
        }

        //SavePrefixNameList(List<PrefixName> data)
        public string PA_SavePrefixNameList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_PREFIXNAMELIST_DATA");
            }
        }

        //List<Secondtitle> GetSecondtitleList()
        public string PA_GetSecondTitleList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_SECONDTITLELIST");
            }
        }

        //List<Secondtitle> GetSecondtitleList()
        public string PA_GetSecondTitleList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_SECONDTITLELIST_LANGUAGECODE");
            }
        }

        //Secondtitle GetSecondtitle(string Code)
        public string PA_GetSecondTitle_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_SECONDTITLE_CODE");
            }
        }

        //SaveSecondtitleList(List<Secondtitle> data)
        public string PA_SaveSecondTitleList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_SECONDTITLELIST_DATA");
            }
        }

        //List<MilitaryTitle> GetMilitaryTitleList()
        public string PA_GetMilitaryTitleList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MILITARYTITLELIST");
            }
        }
        public string PA_GetMilitaryTitleList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MILITARYTITLELIST_LANGUAGECODE");
            }
        }
        //MilitaryTitle GetMilitaryTitle(string Code)
        public string PA_GetMilitaryTitle_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MILITARYTITLE_CODE");
            }
        }

        //SaveMilitaryTitleList(List<MilitaryTitle> data)
        public string PA_SaveMilitaryTitleList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_MILITARYTITLELIST_DATA");
            }
        }

        //List<AcademicTitle> GetAcademicTitleList()
        public string PA_GetAcademicTitleList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ACADEMICTITLELIST");
            }
        }
        public string PA_GetAcademicTitleList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ACADEMICTITLELIST_LANGUAGECODE");
            }
        }
        //AcademicTitle GetAcademicTitle(string Code)
        public string PA_GetAcademicTitle_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ACADEMICTITLE_CODE");
            }
        }

        //SaveAcademicTitleList(List<AcademicTitle> data)
        public string PA_SaveAcademicTitleList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ACADEMICTITLELIST_DATA");
            }
        }

        //List<MedicalTitle> GetMedicalTitleList()
        public string PA_GetMedicalTitleList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MEDICALTITLELIST");
            }
        }
        public string PA_GetMedicalTitleList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MEDICALTITLELIST_LANGUAGECODE");
            }
        }
        //MedicalTitle GetMedicalTitle(string Code)
        public string PA_GetMedicalTitle_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MEDICALTITLE_CODE");
            }
        }

        //SaveMedicalTitleList(List<MedicalTitle> data)
        public string PA_SaveMedicalTitleList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_MEDICALTITLELIST_DATA");
            }
        }

        //List<NameFormat> GetNameFormatList()
        public string PA_GetNameFormatList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_NAMEFORMATLIST");
            }
        }

        //NameFormat GetNameFormat(string Code)
        public string PA_GetNameFormat_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_NAMEFORMAT_CODE");
            }
        }


        //SaveNameFormatList(List<NameFormat> data)
        public string PA_SaveNameFormatList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_NAMEFORMATLIST_DATA");
            }
        }

        //List<Gender> GetGenderList(string LanguageCode)
        public string PA_GetGenderList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_GENDERLIST_LANGUAGECODE");
            }
        }

        //List<Gender> GetGenderList()
        public string PA_GetGenderList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_GENDERLIST");
            }
        }

        //Gender GetGender(string Code)
        public string PA_GetGender_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_GENDER_CODE");
            }
        }

        //Gender GetGender(string Code, string LanguageCode)
        public string PA_GetGender_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_GENDER_CODE_LANGUAGECODE");
            }
        }

        //SaveGenderList(List<Gender> data)
        public string PA_SaveGenderList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_GENDERLIST_DATA");
            }
        }

        //List<Country> GetCountryList()
        public string PA_GetCountryList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_COUNTRYLIST");
            }
        }

        //List<Country> GetCountryList(string LanguageCode)
        public string PA_GetCountryList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_COUNTRYLIST_LANGUAGECODE");
            }
        }

        //Country GetCountry(string Code, string LanguageCode)
        public string PA_GetCountry_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_COUNTRY_CODE_LANGUAGECODE");
            }
        }

        //List<Nationality> GetAllNationality(string LanguageCode)
        public string PA_GetAllNationality_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLNATIONALITY_LANGUAGECODE");
            }
        }

        //Nationality GetNationality(string Code, string LanguageCode)
        public string PA_GetNationality_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_NATIONALITY_CODE_LANGUAGECODE");
            }
        }

        //Country GetCountry(string Code)
        public string PA_GetCountry_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_COUNTRY_CODE");
            }
        }

        //List<Region> GetRegionByCountryCode(string Code, string LanguageCode)
        public string PA_GetRegionByCountryCode_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_REGIONBYCOUNTRYCODE_CODE_LANGUAGECODE");
            }
        }

        //SaveCountryList(List<Country> data)
        public string PA_SaveCountryList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_COUNTRYLIST_DATA");
            }
        }

        //List<MaritalStatus> GetMaritalStatusList()
        public string PA_GetMaritalStatusList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MARITALSTATUSLIST");
            }
        }

        //List<MaritalStatus> GetMaritalStatusList(string LanguageCode)
        public string PA_GetMaritalStatusList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MARITALSTATUSLIST_LANGUAGECODE");
            }
        }

        //MaritalStatus GetMaritalStatus(string Code)
        public string PA_GetMaritalStatus_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MARITALSTATUS_CODE");
            }
        }

        //MaritalStatus GetMaritalStatus(string Code, string LanguageCode)
        public string PA_GetMaritalStatus_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MARITALSTATUS_CODE_LANGUAGECODE");
            }
        }

        //SaveMaritalStatusList(List<MaritalStatus> Data)
        public string PA_SaveMaritalStatusList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_MARITALSTATUSLIST_DATA");
            }
        }

        //List<Religion> GetReligionList(string LanguageCode)
        public string PA_GetReligionList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_RELIGIONLIST_LANGUAGECODE");
            }
        }

        //Religion GetReligion(string Code)
        public string PA_GetReligion_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_RELIGION_CODE");
            }
        }

        //Religion GetReligion(string Code, string LanguageCode)
        public string PA_GetReligion_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_RELIGION_CODE_LANGUAGECODE");
            }
        }


        //SaveReligionList(List<Religion> Data)
        public string PA_SaveReligionList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_RELIGIONLIST_Data");
            }
        }

        //List<Language> GetLanguageList()
        public string PA_GetLanguageList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_LANGUAGELIST");
            }
        }

        //Language GetLanguage(string Code)
        public string PA_GetLanguage_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_LANGUAGE_CODE");
            }
        }

        //SaveLanguageList(List<Language> Data)
        public string PA_SaveLanguageList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_LANGUAGELIST_DATA");
            }
        }

        //List<AddressType> GetAddressTypeList()
        public string PA_GetAddressTypeList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ADDRESSTYPELIST");
            }
        }

        //List<AddressType> GetAddressTypeList(string LanguageCode)
        public string PA_GetAddressTypeList_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ADDRESSTYPELIST_LANGUAGECODE");
            }
        }

        //SaveAddressTypeList(List<AddressType> Data)
        public string PA_SaveAddressTypeList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ADDRESSTYPELIST_DATA");
            }
        }

        //List<Bank> GetBankList()
        public string PA_GetBankList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_BANKLIST");
            }
        }

        //Bank GetBank(string Code)
        public string PA_GetBank_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_BANK_CODE");
            }
        }

        //SaveBankList(List<Bank> Data)
        public string PA_SaveBankList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_BANKLIST_DATA");
            }
        }

        public string PA_SaveBankAccount_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_BANKACCOUNT_DATA");
            }
        }

        //FamilyMember GetFamilyMember(string Code)
        public string PA_GetFamilyMember_Code
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_FAMILYMEMBER_CODE");
            }
        }

        //SaveFamilyMemberList(List<FamilyMember> Data)
        public string PA_SaveFamilyMemberList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_FAMILYMEMBERLIST_DATA");
            }
        }

        //List<FamilyMember> GetFamilyMemberList()
        public string PA_GetFamilyMemberList
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_FAMILYMEMBERLIST");
            }
        }

        //SaveProvinceList(List<Province> Data)
        public string PA_SaveProvinceList_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_PROVINCELIST_DATA");
            }
        }

        //List<EducationLevel> GetAllEducationLevel()
        public string PA_GetAllEducationLevel
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLEDUCATIONLEVEL");
            }
        }
        public string PA_GetAllEducationLevel_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLEDUCATIONLEVEL");
            }
        }

        //SaveAllEducationLevel(List<EducationLevel> list)
        public string PA_SaveAllEducationLevel_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLEDUCATIONLEVEL_LIST");
            }
        }

        //List<Vocation> GetAllVocation()
        public string PA_GetAllVocation
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLVOCATION");
            }
        }

        //SaveAllVocation(List<Vocation> list)
        public string PA_SaveAllVocation_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLVOCATION_LIST");
            }
        }

        //List<Institute> GetAllInstitute()
        public string PA_GetAllInstitute
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLINSTITUTE");
            }
        }
        public string PA_GetAllInstitute_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLINSTITUTE");
            }
        }
        //SaveAllInstitute(List<Institute> list)
        public string PA_SaveAllInstitute_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLINSTITUTE_LIST");
            }
        }

        //List<Honor> GetAllHonor()
        public string PA_GetAllHonor
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLHONOR");
            }
        }

        //SaveAllHonor(List<Honor> list)
        public string PA_SaveAllHonor_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLHONOR_LIST");
            }
        }

        //List<Branch> GetAllBranch()
        public string PA_GetAllBranch
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLBRANCH");
            }
        }
        public string PA_GetAllBranch_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLBRANCH");
            }
        }
        //SaveAllBranch(List<Branch> list)
        public string PA_SaveAllBranch_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLBRANCH_LIST");
            }
        }

        //List<CompanyBusiness> GetAllCompanyBusiness()
        public string PA_GetAllCompanyBusiness
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLCOMPANYBUSINESS");
            }
        }

        //SaveAllCompanyBusiness(List<CompanyBusiness> list)
        public string PA_SaveAllCompanyBusiness_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLCOMPANYBUSINESS_LIST");
            }
        }

        //List<JobType> GetAllJobType()
        public string PA_GetAllJobType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLJOBTYPE");
            }
        }

        //SaveAllJobType(List<JobType> list)
        public string PA_SaveAllJobType_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLJOBTYPE_LIST");
            }
        }

        //List<Certificate> GetAllCertificate()
        public string PA_GetAllCertificate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLCERTIFICATE");
            }
        }
        public string PA_GetAllCertificate_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLCERTIFICATE");
            }
        }

        //SaveAllCertificate(List<Certificate> list)
        public string PA_SaveAllCertificate_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLCERTIFICATE_LIST");
            }
        }

        //Branch GetBranchData(string BranchCode)
        public string PA_GetBranchData_BranchCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_BRANCHDATA_BRANCHCODE");
            }
        }

        //EducationLevel GetEducationLevel(string EducationLevelCode)
        public string PA_GetEducationLevel_EducationLevelCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_EDUCATIONLEVEL_EDUCATIONLEVELCODE");
            }
        }

        //Certificate GetCertificateCode(string CertificateCode)
        public string PA_GetCertificateCode_CertificateCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_CERTIFICATECODE_CERTIFICATECODE");
            }
        }

        //Vocation GetVocation(string VocationCode)
        public string PA_GetVocation_VocationCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_VOCATION_VOCATIONCODE");
            }
        }

        //CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode)
        public string PA_GetCompanyBusiness_CompanyBusinessCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_COMPANYBUSINESS_COMPANYBUSINESSCODE");
            }
        }

        //JobType GetJobType(string JobCode)
        public string PA_GetJobType_JobCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_JOBTYPE_JOBCODE");
            }
        }

        //Institute GetInstitute(string InstituteCode)
        public string PA_GetInstitute_InstituteCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INSTITUTE_INSTITUTECODE");
            }
        }

        //List<EducationGroup> GetAllEducationGroup()
        public string PA_GetAllEducationGroup
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLEDUCATIONGROUP");
            }
        }
        public string PA_GetAllEducationGroup_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLEDUCATIONGROUP");
            }
        }
        //EducationGroup GetEducationGroupByCode(string EducationGroupCode)
        public string PA_GetEducationGroupByCode_EducationGroupCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_EDUCATIONGROUPBYCODE_EDUCATIONGROUPCODE");
            }
        }

        //List<CourseDurationUnit> GetAllUnit()
        public string PA_GetAllUnit
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLUNIT");
            }
        }

        //List<string> GetCertCodeFromEduLevelCode(string educationLevelCode)
        public string PA_GetCertCodeFromEduLevelCode_EducationLevelCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_CERTCODEFROMEDULEVELCODE_EDUCATIONLEVELCODE");
            }
        }


        //List<string> GetBranchFromEduLevelCode(string educationLevelCode)
        public string PA_GetBranchFromEduLevelCode_EducationLevelCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_BRANCHFROMEDULEVELCODE_EDUCATIONLEVELCODE");
            }
        }

        //List<Relationship> GetAllRelationship()
        public string PA_GetAllRelationship
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLRELATIONSHIP");
            }
        }

        //SaveAllRelationship(List<Relationship> list)
        public string PA_SaveAllRelationship_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ALLRELATIONSHIP_LIST");
            }
        }

        //List<Province> GetProvinceByCountryCode(string Code, string LanguageCode)
        public string PA_GetProvinceByCountryCode_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PROVINCEBYCOUNTRYCODE_CODE_LANGUAGECODE");
            }
        }

        //List<Province> GetProvinceByCountryCodeForTravel(string Code, string LanguageCode)
        public string PA_GetProvinceByCountryCodeForTravel_Code_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PROVINCEBYCOUNTRYCODEFORTRAVEL_CODE_LANGUAGECODE");
            }
        }

        //List<District> GetDistrictByProvinceCodeForTravel(string oCountryCode, string oProvinceCode, string oLanguageCode)
        public string PA_GetDistrictByProvinceCodeForTravel_CountryCode_ProvinceCode_LanguageCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_DISTRICTBYPROVINCECODEFORTRAVEL_COUNTRYCODE_PROVINCECODE_LANGUAGECODE");
            }
        }

        public string PA_DISTRICTPROVICE_PROVINCECODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_DISTRICTPROVICE_PROVINCECODE");
            }
        }
        //bool ShowTimeAwareLink(int LinkID)
        public string PA_ShowTimeAwareLink
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SHOWTIMEAWARELINK");
            }
        }

        #endregion HRPAConfigService

        #region HRPAConfigService Transfer Data
        // Create by Pariyaporn P. 20200303
        public string PA_Copy_AllCountry
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLCOUNTRY");
            }
        }

        public string PA_Copy_AllAddressType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLADDRESSTYPE");
            }
        }

        public string PA_Copy_AllCertificate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLCERTIFICATE");
            }
        }

        public string PA_Copy_AllInfotype0105_Mapping
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLINFOTYPE0105_MAPPING");
            }
        }

        public string PA_Copy_AllEducationGroup
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLEDUCATIONGROUP");
            }
        }

        public string PA_Copy_AllEducationLevel
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLEDUCATIONLEVEL");
            }
        }

        public string PA_Copy_AllEmpGroup
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLEMPGROUP");
            }
        }

        public string PA_Copy_AllTitleName
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLTITLENAME");
            }
        }

        public string PA_Copy_AllPrefixName
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLPREFIXNAME");
            }
        }

        public string PA_Copy_AllMilitaryTitle
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLMILITARYTITLE");
            }
        }

        public string PA_Copy_AllSecondTitle
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLSECONDTITLE");
            }
        }

        public string PA_Copy_AllAcademicTitle
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLACADEMICTITLE");
            }
        }

        public string PA_Copy_AllMedicalTitle
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLMEDICALTITLE");
            }
        }

        public string PA_Copy_AllInstitute
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLINSTITUTE");
            }
        }

        public string PA_Copy_AllBranch
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLBRANCH");
            }
        }

        public string PA_Copy_AllBank
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLBANK");
            }
        }

        public string PA_Copy_AllMaritalStatus
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLMARITALSTATUS");
            }
        }

        public string PA_Copy_AllNationaltity
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLNATIONALITY");
            }
        }

        public string PA_Copy_AllProvince
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLPROVINCE");
            }
        }

        public string PA_Copy_AllFamilyMember
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLFAMILYMEMBER");
            }
        }

        public string PA_Copy_AllReligion
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLRELIGION");
            }
        }

        public string PA_Copy_AllGender
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLGENDER");
            }
        }

        public string PA_Copy_AllHonor
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLHONOR");
            }
        }

        public string PA_Copy_AllDocumentType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLDOCUMENTTYPE");
            }
        }

        public string PA_Copy_AllValidityType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLVISATYPE");
            }
        }

        public string PA_Copy_AllAcademicCategoryType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLAcademicCategoryType");
            }
        }

        public string PA_Copy_AllAcademicSubType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLAcademicSubType");
            }
        }

        public string PA_Copy_AllAcademicRole
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COPY_ALLAcademicRole");
            }
        }

        #endregion

        #region HRPADataService

        //List<INFOTYPE0006> GetPersonalAddress(string EmployeeID)
        public string PA_GetPersonalAddress_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PERSONALADDRESS_EMPLOYEEID");
            }
        }

        //List<INFOTYPE0006> GetPersonalAddress(string EmployeeID, DateTime CheckDate)
        public string PA_GetPersonalAddress_EmployeeID_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PERSONALADDRESS_EMPLOYEEID_CHECKDATE");
            }
        }

        //INFOTYPE0002 GetINFOTYPE0002(string EmployeeID, DateTime CheckDate)
        public string PA_GetINFOTYPE0002_EmployeeID_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0002_EMPLOYEEID_CHECKDATE");
            }
        }

        //INFOTYPE0002 GetPersonalData(string EmployeeID)
        public string PA_GetPersonalData_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PERSONALDATA_EMPLOYEEID");
            }
        }

        //string MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        public string PA_MarkUpdate_EmployeeID_DataCategory_RequestNo_isMarkIn
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MARKUPDATE_EMPLOYEEID_DATACATEGORY_REQUESTNO_ISMARKIN");
            }
        }

        //bool CheckMark(string EmployeeID, string DataCategory)
        public string PA_CheckMark_EmployeeID_DataCategory
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CHECKMARK_EMPLOYEEID_DATACATEGORY");
            }
        }

        public string PA_CheckMark_DataCategory
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CHECKMARK_DATACATEGORY");
            }
        }

        public string PA_CheckMark_DataCategory_ReqNo
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CHECKMARK_DATACATEGORY_REQNO");
            }
        }
        //bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        public string PA_CheckMark_EmployeeID_DataCategory_ReqNo
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CHECKMARK_EMPLOYEEID_DATACATEGORY_REQNO");
            }
        }

        //SaveINFOTYPE0002(INFOTYPE0002 data)
        public string PA_SaveINFOTYPE0002_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_INFOTYPE0002_DATA");
            }
        }

        //List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2)
        public string PA_GetINFOTYPE0002List_EmployeeID1_EmployeeID2
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0002LIST_EMPLOYEEID1_EMPLOYEEID2");
            }
        }

        //SaveINFOTYPE0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        public string PA_SaveINFOTYPE0002_EmployeeID1_EmployeeID2_INFOTYPE0002List_Profile
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_INFOTYPE0002_EMPLOYEEID1_EMPLOYEEID2_INFOTYPE0002LIST_PROFILE");
            }
        }

        //SavePersonalAddress(List<INFOTYPE0006> data)
        public string PA_SavePersonalAddress_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_PERSONALADDRESS_DATA");
            }
        }

        //INFOTYPE0009 GetBankDetail(string EmployeeID)
        public string PA_GetBankDetail_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_BANKDETAIL_EMPLOYEEID");
            }
        }


        //SaveBankDetail(INFOTYPE0009 data)
        public string PA_SaveBankDetail_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_BANKDETAIL_DATA");
            }
        }

        //List<INFOTYPE0021> GetFamilyData(string EmployeeID)
        public string PA_GetFamilyData_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_FAMILYDATA_EMPLOYEEID");
            }
        }

        //SaveFamilyData(List<INFOTYPE0021> data)
        public string PA_SaveFamilyData_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_FAMILYDATA_DATA");
            }
        }

        //INFOTYPE0021 GetFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        public string PA_GetFamilyData_EmployeeID_FamilyMember_ChildNo
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_FAMILYDATA_EMPLOYEEID_FAMILYMEMBER_CHILDNO");
            }
        }

        //List<INFOTYPE0022> GetAllEducationHistory(string EmployeeID)
        public string PA_GetAllEducationHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLEDUCATIONHISTORY_EMPLOYEEID");
            }
        }

        public string PA_PositionHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_POSITIONHISTORY_EMPLOYEEID");
            }
        }

        public string PA_actingPositionHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ACTINGPOSITIONHISTORY_EMPLOYEEID");
            }
        }

        public string PA_importancedate_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_IMPORTANCEDATE_EMPLOYEEID");
            }
        }


        public string GET_SEARCH_EMPLOYEE_LIST
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_SEARCH_EMPLOYEE");
            }
        }

        



        //List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        public string PA_GetAllPreviousEmployers_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLPREVIOUSEMPLOYERS_EMPLOYEEID");
            }
        }

        //SearchINFOTYPE0002(FilterData filter)
        public string PA_SearchINFOTYPE0002_Filter
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SEARCH_INFOTYPE0002_FILTER");
            }
        }

        //List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        public string PA_GetAllWorkHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLWORKHISTORY_EMPLOYEEID");
            }
        }

        //List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        public string PA_GetAllSalaryHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLSALARYHISTORY_EMPLOYEEID");
            }
        }

        //List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        public string PA_GetSalaryList_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_SALARYLIST_EMPLOYEEID");
            }
        }

        //List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate)
        public string PA_GetSalaryList_EmployeeID_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_SALARYLIST_EMPLOYEEID_CHECKDATE");
            }
        }

        //List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        public string PA_GetAllEvaluation_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLEVALUATION_EMPLOYEEID");
            }
        }

        //List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        public string PA_GetAllJobGrade_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLJOBGRADE_EMPLOYEEID");
            }
        }

        //List<INFOTYPE0022> GetAllCertificateHistory(string EmployeeID)
        public string PA_GetAllCertificateHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ALLCERTIFICATEHISTORY_EMPLOYEEID");
            }
        }

        //DeleteEducation(List<INFOTYPE0022> data)
        public string PA_DeleteEducation_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DELETE_EDUCATION_DATA");
            }
        }

        //SaveEducation(List<INFOTYPE0022> data)
        public string PA_SaveEducation_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_EDUCATION_DATA");
            }
        }

        public string PA_SaveAcademicWorkMember_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ACADEMICWORKMEMBER_DATA");
            }
        }

        public string PA_SaveWorkHistory_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_WORKHISTORY_DATA");
            }
        }

        public string PA_SaveTrainningHistory_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_TRAINNINGHISTORY_DATA");
            }
        }

        //DeleteFamilyData(List<INFOTYPE0021> data)
        public string PA_DeleteFamilyData_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DELETE_FAMILYDATA_DATA");
            }
        }

        //INFOTYPE0008 GetInfotype0008(string EmployeeID, DateTime CheckDate)
        public string PA_GetInfotype0008_EmployeeID_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0008_EMPLOYEEID_CHECKDATE");
            }
        }

        //List<WorkPlaceData> GetWorkPlace()
        public string PA_GetWorkPlace
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_WORKPLACE");
            }
        }

        //SaveWorkPlace(List<WorkPlaceData> list)
        public string PA_SaveWorkPlace_List
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_WORKPLACE_LIST");
            }
        }

        //CommunicationData GetCommunicationData(string employeeID)
        public string PA_GetCommunicationData_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_COMMUNICATIONDATA_EMPLOYEEID");
            }
        }

        //SaveCommunicationData(CommunicationData savedData, CommunicationData oldData)
        public string PA_SaveCommunicationData_SavedData_OldData
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_COMMUNICATIONDATA_SAVEDDATA_OLDDATA");
            }
        }
        public string PA_SaveDocumentData_SavedData
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_DOCUMENTDATA_SAVEDDATA");
            }
        }
        //List<WorkPeriod> GetWorkHistory(string EmployeeID)
        public string PA_GetWorkHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_WORKHISTORY_EMPLOYEEID");
            }
        }

        public string PA_Organizationinfo_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ORGANIZATIONINFO_EMPLOYEEID");
            }
        }

        //RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate)
        public string PA_GetRelatedAge_EmployeeID_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_RELATEDAGE_EMPLOYEEID_CHECKDATE");
            }
        }


        public string PA_GetEvaluateEmployee_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_EVALUATEEMPLOYEE_EMPLOYEEID");
            }
        }

        //List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup)
        public string PA_GetSalaryRate_EmployeeID_EmpSubGroup
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_SALARYRATE_EMPLOYEEID_EMPSUBGROUP");
            }
        }

        //List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string CategoryCode)
        public string PA_GetINFOTYPE0105ByCategoryCode_EmployeeID_CategoryCode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_INFOTYPE0105BYCATEGORYCODE_EMPLOYEEID_CATEGORYNAME");
            }
        }

        //List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType)
        public string PA_GetAttachmentFileSet_EmployeeID_RequestTypeID_RequestSubType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ATTACHMENTFILESET_EMPLOYEEID_REQUESTTYPEID_REQUESTSUBTYPE");
            }
        }

        //List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType)
        public string PA_GetAttachmentExport_EmployeeID_RequestTypeID_RequestSubType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ATTACHMENTEXPORT_EMPLOYEEID_REQUESTTYPEID_REQUESTSUBTYPE");
            }
        }

        //FileAttachmentSubTypeSave(string RequestNo, string RequestSubType)
        public string PA_FileAttachmentSubTypeSave_RequestNo_RequestSubType
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "FILEATTACHMENTSUBTYPE_SAVE_REQUESTNO_REQUESTSUBTYPE");
            }
        }

        //UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        public string PA_UpdateINFOTYPE0105_INF105
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "UPDATE_INFOTYPE0105_INF105");
            }
        }

        //InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        public string PA_InsertINFOTYPE0105_INF105
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "INSERT_INFOTYPE0105_INF105");
            }
        }


        //SaveINFOTYPE0105(List<INFOTYPE0105> oListINF105Update, List<INFOTYPE0105> oListINF105Insert)
        public string PA_SaveINFOTYPE0105_ListINF105Update_ListINF105Insert
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_INFOTYPE0105_LISTINF105UPDATE_LISTINF105INSERT");
            }
        }

        //DeleteInsertEducation(List<INFOTYPE0022> data)
        public string PA_DeleteInsertEducation_Data
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DELETEINSERT_EDUCATION_DATA");
            }
        }

        //List<string> GetIDCardData(string EmployeeID)
        public string PA_GetIDCardData_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_IDCARDDATA_EMPLOYEEID");
            }
        }

        //DataTable GetCretedDocByEmployeeID(string EmployeeID, int RequestType, string language)
        public string PA_GetCretedDocByEmployeeID_EmployeeID_RequestType_Language
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_CREATEDDOCBYEMPLOYEEID_EMPLOYEEID_REQUESTTYPE_LANGUAGE");
            }
        }

        //object GetPersonalInfo(string EmployeeID, DateTime CheckDate);
        public string PA_GetPersonalInfo_EmployeeID_CheckDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PERSONALINFO_EMPLOYEEID_CHECKDATE");
            }
        }

        public string PA_GetTrainingHistory_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_TRAININGHISTORY_EMPLOYEEID");
            }
        }

        public string PA_GetAcedemicworkmember_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ACEDEMICWORKMEMBER_EMPLOYEEID");
            }
        }

        public string PA_GetPunishmentList_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_PUNISHMENTLIST_EMPLOYEEID");
            }
        }

        public string PA_GetMedicalCheckup_Start_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MEDICALCHECKUP_YEAR_EMPLOYEEID");
            }
        }

        public string PA_GetMedicalCheckup_Startdte_Enddte_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GetMedicalCheckup_Startdte_Enddte_EmployeeID");
            }
        }
        public string PA_GetMedicalCheckup_Year_EmployeeID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GetMedicalCheckup_Year_EmployeeID");
            }
        }

        public string PA_GetHealthRecordStartYear
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GetHealthRecordStartYear");
            }
        }

        public string PA_UPDATE_PERSONALDATA_AFTER_APPROVED_IMMEDIATLY
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PA_UPDATE_PERSONALDATA_AFTER_APPROVED_IMMEDIATLY");
            }
        }

        public string GET_MAX_EVALUATETIMES
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MAX_EVALUATETIMES");
            }
        }

        public string PA_GetMTValidate_DataCode_DataValue
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MTVALIDATE_DATACODE_DATAVALUE");
            }
        }

        public string PA_GetMTValidate_Title_Name
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_MTVALIDATE_TITLE_NAME");
            }
        }

        public string PA_GetEvaluate_Year_Start
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EVALUATE_YEAR_START");
            }
        }
        #endregion HRPADataService

        #region HRPAValidateMethodService
        public string VA_TestValidateMethod
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VA_TestValidateMethod");
            }
        }
        #endregion

        #region Address Service Method
        public string SaveExternalData_Address
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVEEXTERNALDATA_ADDRESS");
            }
        }

        #endregion Address Service Method

        #region Education Service Method
        public string SaveExternalData_Education
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVEEXTERNALDATA_EDUCATION");
            }
        }
        #endregion

        #region Education Service Method
        public string SaveExternalData_WorkHistory
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVEEXTERNALDATA_WORKHISTORY");
            }
        }
        #endregion


        #region Communication Service Method
        public string SaveExternalData_Communication
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVEEXTERNALDATA_COMMUNICATION");
            }
        }
        #endregion Communication

        #region Academic file path
        public string SaveExternalData_Academic
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVEEXTERNALDATA_ACADEMIC");
            }
        }
        public string GetAcademic_FolderAttachments
        {
            //ESS.PA.SVC
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ACADEMIC_ROOT_PATH");
            }
        }

        public string PA_SaveAcademic_SaveData
        {
            //ESS.PA.SVC
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ACADEMIC_SAVEDATA");
            }
        }
        //PA_SaveDocumentData_SavedData
        public string PA_SaveAcademicMember_SaveData
        {
            //ESS.PA.SVC
            get
            {//SAVE_DOCUMENTDATA_SAVEDDATA
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ACADEMIC_MEMBER_SAVEDATA");
            }
        }
        #endregion Academic file path
    }
}