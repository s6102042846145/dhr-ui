using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0023 : AbstractObject
    {
        //private string __employeeID = "";
        //private DateTime __beginDate = DateTime.MinValue;
        //private DateTime __endDate = DateTime.MinValue;
        //private string __employer = "";
        //private string __position = "";
        //private string __countryCode = "";
        //private string __companyBusinessCode = "";
        //private string __jobCode = "";
        //private string __contractTypeCode = "";

        public INFOTYPE0023()
        {
        }

        public string EmployeeID { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        public string Employer { get; set; }

        public string Position { get; set; }

        public string CountryCode { get; set; }

        public string CompanyBusinessCode { get; set; }

        public string JobCode { get; set; }

        public string ContractTypeCode { get; set; }

        public string WorkTime { get; set; }

        //public static List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        //{
        //    return HRPAManagement.GetAllPreviousEmployers(EmployeeID);
        //}
    }
}