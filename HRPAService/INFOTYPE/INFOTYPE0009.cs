using ESS.EMPLOYEE;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0009 : AbstractInfoType
    {
        //#region " Members "

        //private string __accountName; // stras
        //private string __bank; // locat
        //private string __accountNo; //ort02
        //private string _Remark = "";

        //#endregion " Members "
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string Bank { get; set; }
        public string Remark { get; set; }

        public INFOTYPE0009()
        {
        }

        public override string InfoType
        {
            get { return "0009"; }
        }

        //public static INFOTYPE0009 GetData(EmployeeData Requestor)
        //{
        //    return ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetBankDetail(Requestor.EmployeeID);
        //}
        //public static PersonalAccountCenter GetData(EmployeeData Requestor)
        //{
        //    return ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetPersonalBankDetail(Requestor.EmployeeID);
        //}
    }
}