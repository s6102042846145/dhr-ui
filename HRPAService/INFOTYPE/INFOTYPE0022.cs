using System;
using System.Collections.Generic;
using DHR.HR.API.Model;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0022 : AbstractInfoType
    {
        //private string __educationLevelCode = "";
        private DateTime __beginDate = DateTime.MinValue;
        private DateTime __endDate = DateTime.MinValue;
        private string __instituteCode = "";
        private string __instituteName = "";
        //private string __countryCode = "";
        //private string __vocationCode = "";
        //private string __certificateCode = "";
        //private string __branch1 = "";
        //private string __branch2 = "";
        //private bool __isHiringLevel = false;
        //private string __graduateYear = "";
        //private string __honorCode = "";
        //private decimal __grade = 0.0M;
        //private string __gradeText = "";
        //private string __remark = "";
        //private string __duration = "";
        //private string __unit = "";

        ////AddBy: Ratchatawan W. (2012-02-07)
        //private string __educationGroupCode = "";

        public INFOTYPE0022()
        {
        }

        public override string InfoType
        {
            get { return "0022"; }
        }

        public string EducationLevelCode { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        //public string InstituteCode
        //{
        //    get
        //    {
        //        return InstituteCode;
        //        //return this.InstituteCode;
        //    }
        //    set
        //    {
        //        value = value.Replace("0000000000", "");
        //        if (value != "0000000000" && value != "")
        //        {
        //            //this.InstituteName = "";
        //            this.InstituteName = "";
        //        }
        //    }
        //}
        public string InstituteCode
        {
            get
            {
                return __instituteCode;
            }
            set
            {
                if (value == "0000000000")
                {
                    __instituteCode = "";
                }
                else
                {
                    __instituteCode = value;
                }
                if (value != "0000000000" && value != "")
                {
                    __instituteName = "";
                }
            }
        }
        public string InstituteName
        {
            get
            {
                return __instituteName;
            }
            set
            {
                if (__instituteCode == "0000000000" || __instituteCode == "")
                    return;
                __instituteName = value;
            }
        }
        //{
        //    get { return ; }
        //    set
        //    {
        //        if (this.InstituteCode == "0000000000" || this.InstituteCode == "")
        //        {
        //            return;
        //        }
        //    }
        //}

        public string CountryCode { get; set; }

        public string VocationCode { get; set; }

        public string CertificateCode { get; set; }

        public string Branch1 { get; set; }

        public string Branch2 { get; set; }

        public bool IsHiringLevel { get; set; }

        public string GraduateYear { get; set; }

        public string HonorCode { get; set; }

        public decimal Grade { get; set; }

        public string GradeText { get; set; }

        public string Remark { get; set; }
        public string Duration { get; set; }

        public string Unit { get; set; }
        public string EducationGroupCode { get; set; }

        //public static List<INFOTYPE0022> GetEducation(string EmployeeID)
        //{
        //    return HRPAManagement.GetEducation(EmployeeID);
        //}

        //public static List<INFOTYPE0022> GetCertificate(string EmployeeID)
        //{
        //    return HRPAManagement.GetCertificate(EmployeeID);
        //}

        //public static void SaveEducation(List<INFOTYPE0022> data)
        //{
        //    HRPAManagement.SaveEducation(data);
        //}

        //public static void DeleteEducation(List<INFOTYPE0022> data)
        //{
        //    HRPAManagement.DeleteEducation(data);
        //}

        public PAEducationInf dhrEducationInf { get; set; }
    }
}