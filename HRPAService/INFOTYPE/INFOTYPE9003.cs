using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE9003 : AbstractObject
    {
        //private string __employeeID = "";
        //private DateTime __beginDate = DateTime.MinValue;
        //private string __evaluationResult = "";
        //private decimal __companyAdjustRate = 0.0M;
        //private decimal __personalAdjustRate = 0.0M;
        //private decimal __baseSalaryAdjust = 0.0M;
        //private decimal __amountSalryAdjust = 0.0M;
        //private decimal __specialSalaryAdjust = 0.0M;
        //private decimal __bonus1 = 0.0M;
        //private decimal __specialBonus = 0.0M;
        //private string __remark = "";

        public INFOTYPE9003()
        { }

        public string EmployeeID { get; set; }

        public DateTime BeginDate { get; set; }

        public int EvaluateYear
        {
            get
            {
                return BeginDate.Year;
            }
        }

        public string EvaluationResult { get; set; }

        public decimal? CompanyAdjustRate { get; set; }
        public decimal? PersonalAdjustRate { get; set; }

        public decimal? BaseSalaryAdjust { get; set; }

        public decimal? AmountSalaryAdjust { get; set; }
        public decimal? SpecailSalaryAdjust { get; set; }
        public decimal? Bonus1 { get; set; }

        public decimal? SpecialBonus { get; set; }

        public string Remark { get; set; }
        //public static List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        //{
        //    return HRPAManagement.GetAllEvaluation(EmployeeID);
        //}
    }
}