using System.Collections.Generic;
using DHR.HR.API.Model;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0006 : AbstractInfoType
    {
        #region " Members "

        //private string __addressNo; // stras
        //private string __street; // locat
        //private string __district; //ort02
        //private string __province; //ort01
        //private string __postcode; //pstlz
        //private string __country; //land1
        //private string __telephone;//telnr
        //private bool __isDelete = false;
        //private string _Remark = "";

        #endregion " Members "
        //public string AddressType { get; set; }
        public string AddressNo { get; set; }
        public string Street { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public bool IsDelete { get; set; }
        public string Remark { get; set; }

      
        public INFOTYPE0006()
        {
        }

        public override string InfoType
        {
            get { return "0006"; }
        }

        public static List<PersonalAddressCenter> GetData(EmployeeData Requestor)
        {
            return ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetPersonalAddress(Requestor.EmployeeID);
        }

        #region " AddressType "
        public string AddressType
        {
            get
            {
                return this.SubType;
            }
            set
            {
                this.SubType = value;
            }
        }
        #endregion
    }
}