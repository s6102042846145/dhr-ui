using System;
using System.Collections.Generic;
using DHR.HR.API.Model;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.HR.PA.DATACLASS;
using ESS.WORKFLOW;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0002 : AbstractInfoType
    {
        public INFOTYPE0002()
        {
        }
        public override string InfoType
        {
            get
            {
                return "0002";
            }
        }

        public string TitleID { get; set; }
        public string Prefix { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Initial { get; set; }
        public string NickName { get; set; }
        public string NameFormat { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string BirthPlace { get; set; }
        public string BirthCity { get; set; }
        public string Nationality { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime MaritalEffectiveDate { get; set; }
        public int NoOfChild { get; set; }
        public string Religion { get; set; }
        public string Language { get; set; }
        public string Remark { get; set; }
        public string MilitaryTitle { get; set; }
        public string SecondTitle { get; set; }
        public string NamePrefix1 { get; set; }
        public string NamePrefix2 { get; set; }
        public string SecondNationality { get; set; }
        public string ThirdNationality { get; set; }

       
        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> data = new List<IInfoType>();
            //data.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetINFOTYPE0002List(EmployeeID1, EmployeeID2).ToArray());
            return data;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<PersonalInfoCenter> List = new List<PersonalInfoCenter>();
            foreach (IInfoType item in Data)
            {
                List.Add((PersonalInfoCenter)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SavePersonalInfo(EmployeeID1, EmployeeID2, List, Profile);
        }

        //public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        //{
        //    List<INFOTYPE0002> List = new List<INFOTYPE0002>();
        //    foreach (IInfoType item in Data)
        //    {
        //        List.Add((INFOTYPE0002)item);
        //    }
        //    ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0002(EmployeeID1, EmployeeID2, List, Profile);
        //}

        public override object LoadData(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetPersonalInfo(EmployeeID, BeginDate);
        }
        //public override object LoadData(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate)
        //{
        //    return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetINFOTYPE0002(EmployeeID, BeginDate);
        //}
    }
}