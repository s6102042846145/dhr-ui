using System;
using System.Collections.Generic;
using DHR.HR.API.Model;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0021 : AbstractInfoType
    {
        public INFOTYPE0021()
        {
        }

        public override string InfoType
        {
            get { return "0021"; }
        }

        public string FamilyMember { get; set; }
        public string ChildNo { get; set; }
        public string TitleName { get; set; }

        public string TitleRank { get; set; }
        public string Name { get; set; }

        public string Surname { get; set; }

        public DateTime BirthDate { get; set; }

        public string Sex { get; set; }

        public string BirthPlace { get; set; }
        public string CityOfBirth { get; set; }
        public string Nationality { get; set; }

        public string FatherID { get; set; }
        public string MotherID { get; set; }
        public string SpouseID { get; set; }
        public string MotherSpouseID { get; set; }
        public string FatherSpouseID { get; set; }
        public string ChildID { get; set; }
        
        public string JobTitle { get; set; }

        public string Employer { get; set; }
        public string Dead { get; set; }
        public string Remark { get; set; }

        public string Address { get; set; }

        public string Street { get; set; }
        public string District { get; set; }

        public string City { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string TelephoneNo { get; set; }

        public int Age { get; set; }

        public static List<INFOTYPE0021> GetData(EmployeeData Requestor)
        {
            List<INFOTYPE0021> oReturn = new List<INFOTYPE0021>();
                //ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetFamilyData(Requestor.EmployeeID);

            //Addby:Nuttapon.a 12.03.2013  PTT requirement ONLY
            //Fillter subtype 6 (����Ѻ�Ż���ª��) �͡���������� user maintain ��ҹ�к��ͧ�ع���ͧ
            // 2015-10-16 : filter subtype = 7 ��� subtype = 11 ���੾�СѺ module ����Ѻ�Ż��⪹��Сѹ���Ե/�غѵ��˵�

            oReturn = oReturn.FindAll(
                delegate (INFOTYPE0021 temp)
                {
                    return temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ProvidentFundHeir && temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).Surety
                        && temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).LifeInsuranceHeir && temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).AccidentInsuranceHeir;
                });
            return oReturn;
        }

        public static List<PersonalFamilyCenter> GetProvidentMemberData(EmployeeData Requestor)
        {
            List<PersonalFamilyCenter> oReturn = ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetPersonalFamilyData(Requestor.EmployeeID);

            //Fillter ���੾�� subtype 6 (����Ѻ�Ż���ª��) 
            oReturn = oReturn.FindAll(delegate (PersonalFamilyCenter temp) { return temp.FamilyMember == "6"; });
            return oReturn;
        }

        //public static List<INFOTYPE0021> GetProvidentMemberData(EmployeeData Requestor)
        //{
        //    List<INFOTYPE0021> oReturn = ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetFamilyData(Requestor.EmployeeID);

        //    //Fillter ���੾�� subtype 6 (����Ѻ�Ż���ª��) 
        //    oReturn = oReturn.FindAll(delegate (INFOTYPE0021 temp) { return temp.FamilyMember == "6"; });
        //    return oReturn;
        //}
    }
}