using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.HR.PA.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.HR.PA
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion 

	    #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.HR.PA";

        public string CompanyCode { get; set; }
        private string DataSource { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
          
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }

        public static ServiceManager CreateInstance(string oCompanyCode, string oDataSource)
        {

            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode,
                DataSource = oDataSource
            };
            return oServiceManager;
        }

        #endregion MultiCompany  Framework

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}",ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}",ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "


        internal IHRPAConfigService GetMirrorConfig(string Mode)
        {
            Type oType = GetService(Mode, "HRPAConfigServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRPAConfigService)Activator.CreateInstance(oType,CompanyCode);
            }
        }

        internal IHRPADataService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "HRPADataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRPADataService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        public IHRPADataService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRPADataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPADataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRPAConfigService ESSConfig
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRPAConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPAConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRPADataService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRPADataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPADataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IHRPAConfigService ERPConfig
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRPAConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPAConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        #region Multi Source Data Service
        public IHRPADataService DataService
        {
            get
            {
                Type oType = GetService(DataSource, "HRPADataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPADataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRPAConfigService ConfigService
        {
            get
            {
                Type oType = GetService(DataSource, "HRPAConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPAConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
        #endregion Multi Source Data Service

        public string ProvidentFundHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ProvidentFundHeir");
            }
        }
        public string LifeInsuranceHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "LifeInsuranceHeir");
            }
        }
        public string AccidentInsuranceHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "AccidentInsuranceHeir");
            }
        }
        public string Surety
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "Surety");
            }
        }
        
    }
}