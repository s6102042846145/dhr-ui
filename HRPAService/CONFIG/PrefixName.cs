using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class PrefixName
    {
        public PrefixName()
        {
        }

        public string Key{ get; set; }
        public string Description{ get; set; }

        public override int GetHashCode()
        {
            string cCode = string.Format("PREFIXTITLENAME_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

    }
}