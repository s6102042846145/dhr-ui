﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.CONFIG
{
  public  class AcademicRole : AbstractObject
    {
        public AcademicRole()
        {
        }
        public string AcademicRoleKey { get; set; }
        public string Description { get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("ACADEMICROLE_{0}", AcademicRoleKey);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}
