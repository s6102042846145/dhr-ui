using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Certificate : AbstractObject
    {
        public Certificate()
        {
        }

        public string CertificateCode { get; set; }
        public string CertificateDescription { get; set; }
        public string Description { get; set; }
    }
}