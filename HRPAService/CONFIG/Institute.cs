using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Institute : AbstractObject
    {
        public Institute()
        {
        }

        public string EducationLevelCode{ get; set; }
        public string InstituteCode{ get; set; }
        public string InstituteText{ get; set; }
        public string Description { get; set; }

    }
}