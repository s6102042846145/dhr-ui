using System;
using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class TitleName
    {
        public TitleName()
        {
        }

        public string Key{ get; set; }
        public string Abbreviation{ get; set; }
        public string Description{ get; set; }
        public string MultiLanguageName { get; set; }
        public override int GetHashCode()
        {
            string cCode = "TITLENAME_" + Key.ToString();
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}