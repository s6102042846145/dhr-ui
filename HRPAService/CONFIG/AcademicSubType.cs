﻿using ESS.UTILITY.EXTENSION;
using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
   public class AcademicSubType : AbstractObject
    {
        public AcademicSubType()
        {
        }
        public string AcademicSubTypeKey { get; set; }
        public string Description { get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("ACADEMICSUBTYPE_{0}", AcademicSubTypeKey);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}
