using System.Collections.Generic;

//CreatedBy: Ratchatawan W. (2012-02-17)
namespace ESS.HR.PA.CONFIG
{
    public class Nationality
    {
        public Nationality()
        {
        }

        public string NationalityKey { get; set; }
        public string NationalityName { get; set; }
    }
}