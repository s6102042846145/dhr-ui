﻿using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
   public class MTValidateDataCodeValue_x : AbstractObject
    {
        public MTValidateDataCodeValue_x()
        {

        }
        public string DataCode { get; set; }
        public string DataValue { get; set; }
        public string MapDataCode { get; set; }
        public string MapDataValue { get; set; }
    }
}
