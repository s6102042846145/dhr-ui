namespace ESS.HR.PA.CONFIG
{
    public class CharCompare
    {
        public int Compare(char x, char y)
        {
            if ((int)x > (int)y)
                return 1;
            else if ((int)x < (int)y)
                return -1;
            else
                return 0;
        }
    }
}