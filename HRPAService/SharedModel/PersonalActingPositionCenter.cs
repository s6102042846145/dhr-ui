﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.UTILITY.EXTENSION;
using System;

namespace ESS.HR.PA
{
    public class PersonalActingPositionCenter : AbstractInfoType
    {

        public string PaAcId { get; set; }
        public string CompanyCode { get; set; }
        public string EmpCode { get; set; }
    
        public string ActingPosCode { get; set; }
        public string ActingPosTextTH { get; set; }
        public string ActingPosTextEN { get; set; }
        public string ActionType { get; set; }
        public string ServiceType { get; set; }
     
        public string ActingUnitTextTH { get; set; }
        public string ActingUnitTextEN { get; set; }
        public string ActingBandTextTH { get; set; }
        public string ActingBandTextEN { get; set; }
        public string ActingHeadOfUnit { get; set; }
        public string PosTextTH { get; set; }
        public string PosTextEN { get; set; }
        public string UnitTextTH { get; set; }
        public string UnitTextEN { get; set; }
        public string BandTextTH { get; set; }
        public string BandTextEN { get; set; }
        public string HeadOfUnit { get; set; }



      


        public override string InfoType
        {
            get { return "0022"; }
        }


        public PersonalActingPositionCenter()
        {
            PaAcId = string.Empty;
            CompanyCode = string.Empty;
            EmpCode = string.Empty;
            ActingPosCode = string.Empty;
            ActingPosTextTH = string.Empty;
            ActingPosTextEN = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
            ActingUnitTextTH = string.Empty;
            ActingUnitTextEN = string.Empty;
            ActingBandTextTH = string.Empty;
            ActingBandTextEN = string.Empty;
            ActingHeadOfUnit = string.Empty;
            PosTextTH = string.Empty;
            PosTextEN = string.Empty;
            UnitTextTH = string.Empty;
            UnitTextEN = string.Empty;
            BandTextTH = string.Empty;
            BandTextEN = string.Empty;
            HeadOfUnit = string.Empty;
        }
    }
}

