﻿using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA
{
    public class PersonalOrganizationCenter : AbstractInfoType
    {


        public string CompanyCode { get; set; }
        public string EmpCode { get; set; }
        public string EmpStatusCode { get; set; }
        public string EmpStatusValue { get; set; }
        public string EmpStatusTextTH { get; set; }
        public string EmpStatusTextEN { get; set; }
        public string EmpTypeCode { get; set; }
        public string EmpTypeValue { get; set; }
        public string EmpTypeTextTH { get; set; }
        public string EmpTypeTextEN { get; set; }
        public string PosCode { get; set; }
        public string PosTextTH { get; set; }
        public string PosTextEN { get; set; }
        public string SpcPosName { get; set; }
        public string UnitCode { get; set; }
        public string UnitTextTH { get; set; }
        public string UnitTextEN { get; set; }
        public string CostCenterCode { get; set; }
        public string CostCenterValue { get; set; }
        public string LevelCode { get; set; }
        public string LevelValue { get; set; }
        public string LevelText { get; set; }
        public string ActingLevelCode { get; set; }
        public string ActingLevelValue { get; set; }
        public string ActingLevelText { get; set; }
        public string BandCode { get; set; }
        public string BandValue { get; set; }
        public string BandTextTH { get; set; }
        public string BandTextEN { get; set; }
        public string ActingBandCode { get; set; }
        public string ActingBandValue { get; set; }
        public string ActingBandTextTH { get; set; }
        public string ActingBandTextEN { get; set; }
        public string CountryWorkCode { get; set; }
        public string CountryWorkValue { get; set; }
        public string CountryWorkTextTH { get; set; }
        public string CountryWorkTextEN { get; set; }
        public string WorkPlaceCode { get; set; }
        public string WorkPlaceValue { get; set; }
        public string WorkPlaceTextTH { get; set; }
        public string WorkPlaceTextEN { get; set; }
        public string WorkAreaCode { get; set; }
        public string WorkAreaValue { get; set; }
        public string WorkAreaTextTH { get; set; }
        public string WorkAreaTextEN { get; set; }
        public string WorkTimeCode { get; set; }
        public string WorkTimeValue { get; set; }
        public string WorkTimeTextTH { get; set; }
        public string WorkTimeTextEN { get; set; }
        public string ReportToPosCode { get; set; }
        public string ReportToPosTextTH { get; set; }
        public string ReportToPosTextEN { get; set; }
        public string ReportToBandCode { get; set; }
        public string ReportToBandValue { get; set; }
        public string ReportToBandTextTH { get; set; }
        public string ReportToBandTextEN { get; set; }
    
        public string SpcPosNameEn { get; set; }

        public string CostCenterText { get; set; }
        public string LevelTextTH { get; set; }
        public string LevelTextEN { get; set; }

        public string ActingLevelTextTH { get; set; }
        public string ActingLevelTextEN { get; set; }

        public string HrAdminCode { get; set; }
        public string HrAdminValue { get; set; }
        public string HrAdminTextTH { get; set; }
        public string HrAdminTextEN { get; set; }

        public string ActionType { get; set; }
        public string ServiceType { get; set; }
        public string Id { get; set; }



        public override string InfoType
        {
            get { return "0022"; }
        }

        public PersonalOrganizationCenter()
        {

            CompanyCode = string.Empty;
            EmpCode = string.Empty;
            EmpStatusCode = string.Empty;
            EmpStatusValue = string.Empty;
            EmpStatusTextTH = string.Empty;
            EmpStatusTextEN = string.Empty;
            EmpTypeCode = string.Empty;
            EmpTypeValue = string.Empty;
            EmpTypeTextTH = string.Empty;
            EmpTypeTextEN = string.Empty;
            PosCode = string.Empty;
            PosTextTH = string.Empty;
            PosTextEN = string.Empty;
            SpcPosName = string.Empty;
            UnitCode = string.Empty;
            UnitTextTH = string.Empty;
            UnitTextEN = string.Empty;
            CostCenterCode = string.Empty;
            CostCenterValue = string.Empty;
            CostCenterText = string.Empty;
            LevelCode = string.Empty;
            LevelValue = string.Empty;
            LevelText = string.Empty;
            ActingLevelCode = string.Empty;
            ActingLevelValue = string.Empty;
            ActingLevelText = string.Empty;
            BandCode = string.Empty;
            BandValue = string.Empty;
            BandTextTH = string.Empty;
            BandTextEN = string.Empty;
            ActingBandCode = string.Empty;
            ActingBandValue = string.Empty;
            ActingBandTextTH = string.Empty;
            ActingBandTextEN = string.Empty;
            CountryWorkCode = string.Empty;
            CountryWorkValue = string.Empty;
            CountryWorkTextTH = string.Empty;
            CountryWorkTextEN = string.Empty;
            WorkPlaceCode = string.Empty;
            WorkPlaceValue = string.Empty;
            WorkPlaceTextTH = string.Empty;
            WorkPlaceTextEN = string.Empty;
            WorkAreaCode = string.Empty;
            WorkAreaValue = string.Empty;
            WorkAreaTextTH = string.Empty;
            WorkAreaTextEN = string.Empty;
            WorkTimeCode = string.Empty;
            WorkTimeValue = string.Empty;
            WorkTimeTextTH = string.Empty;
            WorkTimeTextEN = string.Empty;
            ReportToPosCode = string.Empty;
            ReportToPosTextTH = string.Empty;
            ReportToPosTextEN = string.Empty;
            ReportToBandCode = string.Empty;
            ReportToBandValue = string.Empty;
            ReportToBandTextTH = string.Empty;
            ReportToBandTextEN = string.Empty;
            SpcPosNameEn = string.Empty;
            CostCenterText = string.Empty;
            LevelTextTH = string.Empty;
            LevelTextEN = string.Empty;
            ActingLevelTextTH = string.Empty;
            ActingLevelTextEN = string.Empty;
            HrAdminCode = string.Empty;
            HrAdminValue = string.Empty;
            HrAdminTextTH = string.Empty;
            HrAdminTextEN = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
            Id = string.Empty;


        }
    }
}

