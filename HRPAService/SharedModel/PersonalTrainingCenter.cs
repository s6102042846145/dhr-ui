﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.UTILITY.EXTENSION;
using System;

namespace ESS.HR.PA
{
    public class PersonalTrainingCenter : AbstractInfoType
    {

        
        public string PaTrId { get; set; }
        public string CompanyCode { get; set; }
        public string EmployeeID { get; set; }
        public string CourseName { get; set; }

        public string ActionType { get; set; }
        public string  ServiceType { get; set; }


        public override string InfoType
        {
            get { return "0022"; }
        }


        public PersonalTrainingCenter()
        {
            PaTrId = string.Empty;
            CompanyCode = string.Empty;
            EmployeeID = string.Empty;
            CourseName = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
        }
    }
}

