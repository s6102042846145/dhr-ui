﻿using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA
{
    public class HrEditByRequestNoCenter : AbstractInfoType
    {
        public string EmployeeID { get; set; }
        public string DataCategory { get; set; }
        public string RequestNo { get; set; }
        public string CreatorNo { get; set; }
      



        public override string InfoType
        {
            get { return "0022"; }
        }

        public HrEditByRequestNoCenter()
        {
            EmployeeID = string.Empty;
            DataCategory = string.Empty;
            RequestNo = string.Empty;
            //StartDate = string.Empty;
            //EndDate = string.Empty;
            CreatorNo = string.Empty;
           


        }
    }
}

