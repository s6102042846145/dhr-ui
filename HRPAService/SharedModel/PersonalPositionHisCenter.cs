﻿using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA
{
    public class PersonalPositionHisCenter : AbstractInfoType
    {
        public string CompanyCode { get; set; }
        public string EmpCode { get; set; }
        public string PosStartDate { get; set; }
        public string ActionCode { get; set; }
        public string ActionValue { get; set; }
        public string ActionTextTH { get; set; }
        public string ActionTextEN { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonValue { get; set; }
        public string ReasonTextTH { get; set; }
        public string ReasonTextEN { get; set; }
        public string CommandNo { get; set; }
        public string CmdTypeCode { get; set; }
        public string CmdTypeValue { get; set; }
        public string CmdTypeTextTH { get; set; }
        public string CmdTypeTextEN { get; set; }
        public string CmdName { get; set; }
        public string CmdNameEn { get; set; }
        public string ActionType { get; set; }
        public string ServiceType { get; set; }
        public string Id { get; set; }


        public override string InfoType
        {
            get { return "0022"; }
        }

        public PersonalPositionHisCenter()
        {
            CompanyCode = string.Empty;
            EmpCode = string.Empty;
            PosStartDate = string.Empty;
            ActionCode = string.Empty;
            ActionValue = string.Empty;
            ActionTextTH = string.Empty;
            ActionTextEN = string.Empty;
            ReasonCode = string.Empty;
            ReasonValue = string.Empty;
            ReasonTextTH = string.Empty;
            ReasonTextEN = string.Empty;
            CommandNo = string.Empty;
            CmdTypeCode = string.Empty;
            CmdTypeValue = string.Empty;
            CmdTypeTextTH = string.Empty;
            CmdTypeTextEN = string.Empty;
            CmdName = string.Empty;
            CmdNameEn = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
            Id = string.Empty;
        }
    }
}

