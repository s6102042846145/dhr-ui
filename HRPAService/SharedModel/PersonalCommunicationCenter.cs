﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA
{
    public class PersonalCommunicationCenter : AbstractObject
    {
        private string __Remark;
        private string __telephoneNo = "";
        private string __workPlace = "";
        private string __employeeID = "";
        private string __mobilePhone = "";
        private string __mobileNotPublic = "";
        private string __email = "";
        private string __privateEmail = "";
        private string __homePhone = "";
        private string __lineId = "";
        private string __personOfEmergency = "";
        public string CompanyCode { get; set; }
        private string __phoneOfEmergency = "";
        private string __primaryWebsite = "";
        private string __secondaryWebsite = "";

        public string TelephoneNo
        {
            get
            {
                if (string.IsNullOrEmpty(__telephoneNo) || __telephoneNo.Trim() == "")
                {
                    return string.Empty;
                }
                return __telephoneNo;
            }
            set
            {
                __telephoneNo = value;
            }
        }

        public string WorkPlace
        {
            get
            {
                if (string.IsNullOrEmpty(__workPlace) || __workPlace.Trim() == "")
                {
                   return string.Empty;
                }
                return __workPlace;
            }
            set
            {
                __workPlace = value;
            }
        }

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public string MobilePhone
        {
            get
            {
                if (string.IsNullOrEmpty(__mobilePhone) || __mobilePhone.Trim() == "")
                {
                   return string.Empty;
                }
                return __mobilePhone;
            }
            set
            {
                __mobilePhone = value;
            }
        }

        public string MobileNotPublic
        {
            get
            {
                if (string.IsNullOrEmpty(__mobileNotPublic) || __mobileNotPublic.Trim() == "")
                {
                   return string.Empty;
                }
                return __mobileNotPublic;
            }
            set
            {
                __mobileNotPublic = value;
            }
        }

        public string Email
        {
            get
            {
                if (string.IsNullOrEmpty(__email) || __email.Trim() == "")
                {
                   return string.Empty;
                }
                return __email;
            }
            set
            {
                __email = value;
            }
        }

        public string PrivateEmail
        {
            get
            {
                if (string.IsNullOrEmpty(__privateEmail) || __privateEmail.Trim() == "")
                {
                   return string.Empty;
                }
                return __privateEmail;
            }
            set
            {
                __privateEmail = value;
            }
        }

        public string HomePhone
        {
            get
            {
                if (string.IsNullOrEmpty(__homePhone) || __homePhone.Trim() == "")
                {
                   return string.Empty;
                }
                return __homePhone;
            }
            set
            {
                __homePhone = value;
            }
        }

        public string LineID
        {
            get
            {
                if (string.IsNullOrEmpty(__lineId) || __lineId.Trim() == "")
                {
                   return string.Empty;
                }
                return __lineId;
            }
            set
            {
                __lineId = value;
            }
        }

        public string PersonOfEmergency
        {
            get
            {
                if (string.IsNullOrEmpty(__personOfEmergency) || __personOfEmergency.Trim() == "")
                {
                    return string.Empty;
                }
                return __personOfEmergency;
            }
            set
            {
                __personOfEmergency = value;
            }
        }

        public string Remark
        {
            get { return __Remark; }
            set
            {
                __Remark = value;
            }
        }

        public string PhoneOfEmergency
        {
            get
            {
                if (string.IsNullOrEmpty(__phoneOfEmergency) || __phoneOfEmergency.Trim() == "")
                {
                    return string.Empty;
                }
                return __phoneOfEmergency;
            }
            set
            {
                __phoneOfEmergency = value;
            }
        }
        public string PrimaryWebsite
        {
            get
            {
                if (string.IsNullOrEmpty(__primaryWebsite) || __primaryWebsite.Trim() == "")
                {
                    return string.Empty;
                }
                return __primaryWebsite;
            }
            set
            {
                __primaryWebsite = value;
            }
        }

        public string SecondaryWebsite
        {
            get
            {
                if (string.IsNullOrEmpty(__secondaryWebsite) || __secondaryWebsite.Trim() == "")
                {
                    return string.Empty;
                }
                return __secondaryWebsite;
            }
            set
            {
                __secondaryWebsite = value;
            }
        }
    }
}
