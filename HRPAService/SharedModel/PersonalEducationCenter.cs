﻿using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA
{
    public class PersonalEducationCenter : AbstractInfoType
    {


        







        public string CompanyCode { get; set; }
        public string PAEduID { get; set; }
        public string EmployeeID { get; set; }
        public string EducationLevelCode { get; set; }
        public string EduLevelValue { get; set; }
        public string InstituteValue { get; set; }
        public string InstituteTextTH { get; set; }
        public string InstituteTextEN { get; set; }
        public string EducationLevelTextTH { get; set; }
        public string EducationLevelTextEN { get; set; }
        public string CountryValue { get; set; }
        public string CountryTextTH { get; set; }
        public string CountryTextEN { get; set; }
        public string CertificateValue { get; set; }
        public string CertificateTextTH { get; set; }
        public string CertificateTextEN { get; set; }




        public string Branch1TextTH { get; set; }
        public string Branch2TextTH { get; set; }
        public string HonorTextTH { get; set; }
        public string InstituteCode { get; set; }
        public string InstituteName { get; set; }
        
        public string CountryCode { get; set; }
        public string VocationCode { get; set; }
        public string CertificateCode { get; set; }
        public string Branch1 { get; set; }
        
        public string Branch2 { get; set; }
        
        public bool IsHiringLevel { get; set; }
        public string GraduateYear { get; set; }
        public string HonorCode { get; set; }
        public decimal? Grade { get; set; }
        public string GradeText { get; set; }
        public string Remark { get; set; }
        public string Duration { get; set; }
        public string Unit { get; set; }
        public string EducationGroupCode { get; set; }
        //Nattawat S. Add new attribute for display value CV Page
        public string EducationLevelName { get; set; }
        public string CountryName { get; set; }
        public string Certificatename { get; set; }
        public string EducationGroupname { get; set; }
        public string MajorName { get; set; }
        public string MinorName { get; set; }
        public string ServiceType { get; set; }
        public string ActionType { get; set; }


       
       
     
        
      
        
      
      
     
      

        public string Branch1Code { get; set; }
        public string Branch1Value { get; set; }
        public string Branch1TextEN { get; set; }
        public string Branch2Code { get; set; }
        public string Branch2Value { get; set; }
        public string Branch2TextEN { get; set; }
        public string HonorValue { get; set; }
        public string HonorTextEN { get; set; }
        public string EduGroupCode { get; set; }
        public string EduGroupValue { get; set; }
        public string EduGroupTextTH { get; set; }
        public string EduGroupTextEN { get; set; }

        //public DateTime BeginDate { get; set; }

        //public DateTime EndDate { get; set; }




        public override string InfoType
        {
            get { return "0022"; }
        }

        public PersonalEducationCenter()
        {
            CompanyCode = string.Empty;
            PAEduID = string.Empty;
            EmployeeID = string.Empty;
            EducationLevelCode = string.Empty;
            InstituteCode = string.Empty;
            InstituteName = string.Empty;
            CountryCode = string.Empty;
            VocationCode = string.Empty;
            CertificateCode = string.Empty;
            Branch1 = string.Empty;
            Branch1TextTH = string.Empty;
            Branch2 = string.Empty;
            Branch2TextTH = string.Empty;
            IsHiringLevel = default(bool);
            GraduateYear = string.Empty;
            HonorCode = string.Empty;
            Grade = default(decimal);
            GradeText = string.Empty;
            Remark = string.Empty;
            Duration = string.Empty;
            Unit = string.Empty;
            EducationGroupCode = string.Empty;
            EducationLevelName = string.Empty;
            CountryName = string.Empty;
            Certificatename = string.Empty;
            EducationGroupname = string.Empty;
            MajorName = string.Empty;
            MinorName = string.Empty;
            ServiceType = string.Empty;
            ActionType = string.Empty;
            //BeginDate = string.Empty;
            //EndDate = string.Empty;
            InstituteTextTH = string.Empty;
            EducationLevelTextTH = string.Empty;
            CountryTextTH = string.Empty;
            CertificateTextTH = string.Empty;
            Branch1TextTH = string.Empty;
            Branch2TextTH = string.Empty;
            HonorTextTH = string.Empty;
            InstituteTextEN = string.Empty;
            EducationLevelTextEN = string.Empty;
            CountryTextEN = string.Empty;
            CertificateTextEN = string.Empty;
            Branch1Code = string.Empty;
            Branch1Value = string.Empty;
            Branch1TextEN = string.Empty;
            Branch2Code = string.Empty;
            Branch2Value = string.Empty;
            Branch2TextEN = string.Empty;
            HonorValue = string.Empty;
            HonorTextEN = string.Empty;
            EduGroupCode = string.Empty;
            EduGroupValue = string.Empty;
            EduGroupTextTH = string.Empty;
            EduGroupTextEN = string.Empty;



        }
    }
}

