﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.FILE;
using ESS.HR.PA.DATACLASS;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA
{
    public class PersonalAcademicCenter : AbstractObject
    {
        public string CompanyCode { get; set; }
        public int PaReId { get; set; }
        public string EmployeeID { get; set; }
        public string SubTypeCode { get; set; }
        public string SubTypeValue { get; set; }
        public string SubTypeNameTH { get; set; }
        public string SubTypeNameEN { get; set; }
        public string Title { get; set; }
        public string YearOfPublic { get; set; }
        public string Journal { get; set; }
        public string DOI { get; set; }
        public string ImpactFactor { get; set; }
        public string RefAuthor { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime? GrantedDate { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryValue { get; set; }
        public string CategoryNameTH { get; set; }
        public string CategoryNameEN { get; set; }
        public string Remark { get; set; }
        public string AttachmentId { get; set; }
        public List<PersonalAttachmentFileSet> AttachmentFileSets { get; set; }
        public List<PersonalAcademicMemberCenter> AcademicMembers { get; set; }
        public PersonalAcademicCenter()
        {
            CompanyCode = string.Empty;
            PaReId = 0;
            EmployeeID = string.Empty;
            SubTypeCode = string.Empty;
            SubTypeValue = string.Empty;
            SubTypeNameTH = string.Empty;
            SubTypeNameEN = string.Empty;
            Title = string.Empty;
            YearOfPublic = string.Empty;
            Journal = string.Empty;
            DOI = string.Empty;
            ImpactFactor = string.Empty;
            SubmittedDate = default(DateTime);
            GrantedDate = default(DateTime);
            CategoryCode = string.Empty;
            CategoryValue = string.Empty;
            CategoryNameTH = string.Empty;
            CategoryNameEN = string.Empty;
            Remark = string.Empty;
            AttachmentId = string.Empty;
            RefAuthor = string.Empty;
            AcademicMembers = new List<PersonalAcademicMemberCenter>();
            AttachmentFileSets = new List<PersonalAttachmentFileSet>();
        }
    }
}
