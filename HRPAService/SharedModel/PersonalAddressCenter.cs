﻿using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA
{
    public class PersonalAddressCenter : AbstractInfoType
    {
        public string CompanyCode { get; set; }
        public string AddressNo { get; set; }
        public string Street { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public bool IsDelete { get; set; }
        public string Remark { get; set; }
        public string Soi { get; set; }
        public string Subdistrict { get; set; }
        public string Building { get; set; }
        public string BuildingEn { get; set; }
        public string Moo { get; set; }

        public override string InfoType
        {
            get { return "0006"; }
        }

        #region " AddressType "
        //comment by pariyaporn เนื่องจาก addresstype ไม่ยอมอัพเดทข้อมูล
        //public string AddressType
        //{
        //    get
        //    {
        //        return this.SubType;
        //    }
        //    set
        //    {
        //        this.SubType = value;
        //    }
        //}
        public string AddressType { get; set; }
        #endregion

        public string AddressTypeCode { get; set; }
        public string AddressTypeTextTH { get; set; }
        public string AddressTypeTextEN { get; set; }
        public string SubdistrictTextTH { get; set; }
        public string SubdistrictTextEN { get; set; }
        public string DistrictTextTH { get; set; }
        public string DistrictTextEN { get; set; }
        public string ProvinceTextTH { get; set; }
        public string ProvinceTextEN { get; set; }
        public string CountryCode { get; set; }
        public string CountryTextTH { get; set; }
        public string CountryTextEN { get; set; }

        public PersonalAddressCenter()
        {
            CompanyCode = string.Empty;
            AddressNo = string.Empty;
            Street = string.Empty;
            District = string.Empty;
            Province = string.Empty;
            Postcode = string.Empty;
            Country = string.Empty;
            Telephone = string.Empty;
            IsDelete = default(bool);
            Remark = string.Empty;
            Soi = string.Empty;
            Subdistrict = string.Empty;
            Building = string.Empty;
            BuildingEn = string.Empty;
            Moo = string.Empty;
            AddressType = string.Empty;
            AddressTypeCode = string.Empty;
            AddressTypeTextTH = string.Empty;
            AddressTypeTextEN = string.Empty;
            SubdistrictTextTH = string.Empty;
            SubdistrictTextEN = string.Empty;
            DistrictTextTH = string.Empty;
            DistrictTextEN = string.Empty;
            ProvinceTextTH = string.Empty;
            ProvinceTextEN = string.Empty;
            CountryCode = string.Empty;
            CountryTextTH = string.Empty;
            CountryTextEN = string.Empty;
        }

    }
}

