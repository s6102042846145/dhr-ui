﻿using ESS.EMPLOYEE.ABSTRACT;
using System;

namespace ESS.HR.PA
{
    public class PersonalDocumentCenter
    {
        public string CompanyCode { get; set; }
        public string EmployeeID { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNo { get; set; }
        public string ValidityOfEntry { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string Nationality { get; set; }

        public PersonalDocumentCenter()
        {
            CompanyCode = string.Empty;
            EmployeeID = string.Empty;
            DocumentType = string.Empty;
            DocumentNo = string.Empty;
            ValidityOfEntry = string.Empty;
            Nationality = string.Empty;
        }
    }
}

