﻿using ESS.EMPLOYEE.ABSTRACT;
using ESS.UTILITY.EXTENSION;
using System;

namespace ESS.HR.PA
{
    public class PersonalImportanceDateCenter : AbstractInfoType
    {


        
        





        public string CompanyCode { get; set; }
        public string EmpCode { get; set; }

        public DateTime StartWorkDate { get; set; }
        public DateTime EmployedDate { get; set; }
        public DateTime CountLeaveAgeDate { get; set; }
        public DateTime RegistFundDate { get; set; }
        public DateTime CountFundAgeDate { get; set; }
        public DateTime StartWorkGroupDate { get; set; }

        public DateTime LeavePfFundDate1 { get; set; }
        public DateTime LeavePfFundDate2 { get; set; }
        public DateTime LeavePfFundDate3 { get; set; }
        public DateTime RetireDate { get; set; }


        public DateTime CountLevelAgeDate { get; set; }

        public DateTime CountLevelAgeGroupDate { get; set; }
        public DateTime CountPosAgeDate { get; set; }
        public DateTime CountUnitAgeDate { get; set; }
        public DateTime CountRootJobAgeDate { get; set; }
        public string ActionType { get; set; }
        public string ServiceType { get; set; }
        public string Id { get; set; }
       


        public override string InfoType
        {
            get { return "0022"; }
        }


        public PersonalImportanceDateCenter()
        {
          
            CompanyCode = string.Empty;
            EmpCode = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
            Id = string.Empty;
        }
    }
}

