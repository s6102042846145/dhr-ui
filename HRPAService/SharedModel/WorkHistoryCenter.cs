﻿using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PA
{
    public class WorkHistoryCenter : AbstractInfoType
    {
        public string PaWoId { get; set; }
        public string CompanyCode { get; set; }
        public string EmpCode { get; set; }
        //public string StartDate { get; set; }
        //public string EndDate { get; set; }
        public string EmployerName { get; set; }
        public string Position { get; set; }
        public string WorkCountryCode { get; set; }
        public string WorkCountryValue { get; set; }
        public string WorkCountryTextTH { get; set; }
        public string WorkCountryTextEN { get; set; }
        public string BuCode { get; set; }
        public string BuValue { get; set; }
        public string BuTextTH { get; set; }
        public string BuTextEN { get; set; }
        public string JobGrpCode { get; set; }
        public string JobGrpValue { get; set; }
        public string JobGrpTextTH { get; set; }
        public string JobGrpTextEN { get; set; }
        public object ActionType { get; set; }
        public object ServiceType { get; set; }
        public string CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateBy { get; set; }



        public override string InfoType
        {
            get { return "0022"; }
        }

        public WorkHistoryCenter()
        {
            PaWoId = string.Empty;
            CompanyCode = string.Empty;
            EmpCode = string.Empty;
            //StartDate = string.Empty;
            //EndDate = string.Empty;
            EmployerName = string.Empty;
            Position = string.Empty;
            WorkCountryCode = string.Empty;
            WorkCountryValue = string.Empty;
            WorkCountryTextTH = string.Empty;
            WorkCountryTextEN = string.Empty;
            BuCode = string.Empty;
            BuValue = string.Empty;
            BuTextTH = string.Empty;
            BuTextEN = string.Empty;
            JobGrpCode = string.Empty;
            JobGrpValue = string.Empty;
            JobGrpTextTH = string.Empty;
            JobGrpTextEN = string.Empty;
            ActionType = string.Empty;
            ServiceType = string.Empty;
            CreateDate = string.Empty;
            CreateBy = string.Empty;
            UpdateDate = string.Empty;
            UpdateBy = string.Empty;


        }
    }
}

