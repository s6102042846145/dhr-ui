﻿using ESS.EMPLOYEE.ABSTRACT;
using System;

namespace ESS.HR.PA
{
    public class PersonalFamilyCenter : AbstractInfoType
    {
        public string CompanyCode { get; set; }
        public string FamilyMember { get; set; }
        public string ChildNo { get; set; }
        public string TitleName { get; set; }
        public string TitleRank { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Sex { get; set; }
        public string BirthPlace { get; set; }
        public string CityOfBirth { get; set; }
        public string Nationality { get; set; }
        public string FatherID { get; set; }
        public string MotherID { get; set; }
        public string SpouseID { get; set; }
        public string MotherSpouseID { get; set; }
        public string FatherSpouseID { get; set; }
        public string ChildID { get; set; }
        public string JobTitle { get; set; }
        public string Employer { get; set; }
        public string Dead { get; set; }
        public string Remark { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string TelephoneNo { get; set; }
        public int? Age { get; set; }

        public string AddressTypeValue { get; set; }
        public int? Moo { get; set; }
        public string Building { get; set; }
        public string BuildingEn { get; set; }
        public string Soi { get; set; }
        public string SubdistrictCode { get; set; }
        public int FamilyId { get; set; }

        public override string InfoType
        {
            get { return "0021"; }
        }

        //Family 
        public string RelateCode { get; set; }
        public string RelateCodeTextTH { get; set; }
        public string RelateCodeTextEN { get; set; }
        public string InameCode { get; set; }
        public string InameCodeTextTH { get; set; }
        public string InameCodeTextEN { get; set; }
        public string FullNameText { get; set; }
        public string SexCode { get; set; }
        public string SexCodeTextTH { get; set; }
        public string SexCodeTextEN { get; set; }
        public string CountryBirthCode { get; set; }
        public string CountryBirthCodeTextTH { get; set; }
        public string CountryBirthCodeTextEN { get; set; }
        public string NationalityCode { get; set; }
        public string NationalityCodeTextTH { get; set; }
        public string NationalityCodeTextEN { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime? PassAwayDate { get; set; }
        public DateTime? MaritalDate { get; set; }
        public DateTime? DivorceDate { get; set; }
        public string FamEmpCode { get; set; }

        //Address Family
        public string AddressTypeCode { get; set; }
        public string AddressTypeTextTH { get; set; }
        public string AddressTypeTextEN { get; set; }
        public string SubdistrictTextTH { get; set; }
        public string SubdistrictTextEN { get; set; }
        public string DistrictTextTH { get; set; }
        public string DistrictTextEN { get; set; }
        public string ProvinceTextTH { get; set; }
        public string ProvinceTextEN { get; set; }
        public string CountryCode { get; set; }
        public string CountryTextTH { get; set; }
        public string CountryTextEN { get; set; }


        public PersonalFamilyCenter()
        {
            CompanyCode = string.Empty;
            FamilyMember = string.Empty;
            ChildNo = string.Empty;
            TitleName = string.Empty;
            TitleRank = string.Empty;
            Name = string.Empty;
            Surname = string.Empty;
            BirthDate = (DateTime?)null;
            Sex = string.Empty;
            BirthPlace = string.Empty;
            CityOfBirth = string.Empty;
            Nationality = string.Empty;
            FatherID = string.Empty;
            MotherID = string.Empty;
            SpouseID = string.Empty;
            MotherSpouseID = string.Empty;
            FatherSpouseID = string.Empty;
            ChildID = string.Empty;
            JobTitle = string.Empty;
            Employer = string.Empty;
            Dead = string.Empty;
            Remark = string.Empty;
            Address = string.Empty;
            Street = string.Empty;
            District = string.Empty;
            City = string.Empty;
            Postcode = string.Empty;
            Country = string.Empty;
            TelephoneNo = string.Empty;
            Age = (int?)null;

            AddressTypeValue = string.Empty;
            Moo = (int?)null;
            Building = string.Empty;
            BuildingEn = string.Empty;
            Soi = string.Empty;
            SubdistrictCode = string.Empty;
            FamilyId = default(int);

            //Family 
            RelateCode = string.Empty;
            RelateCodeTextTH = string.Empty;
            RelateCodeTextEN = string.Empty;
            InameCode = string.Empty;
            InameCodeTextTH = string.Empty;
            InameCodeTextEN = string.Empty;
            FullNameText = string.Empty;
            SexCode = string.Empty;
            SexCodeTextTH = string.Empty;
            SexCodeTextEN = string.Empty;
            CountryBirthCode = string.Empty;
            CountryBirthCodeTextTH = string.Empty;
            CountryBirthCodeTextEN = string.Empty;
            NationalityCode = string.Empty;
            NationalityCodeTextTH = string.Empty;
            NationalityCodeTextEN = string.Empty;
            MaritalStatus = string.Empty;
            PassAwayDate = (DateTime?)null;
            MaritalDate = (DateTime?)null;
            DivorceDate = (DateTime?)null;
            FamEmpCode = string.Empty;

            //Address Family
            AddressTypeCode = string.Empty;
            AddressTypeTextTH = string.Empty;
            AddressTypeTextEN = string.Empty;
            SubdistrictTextTH = string.Empty;
            SubdistrictTextEN = string.Empty;
            DistrictTextTH = string.Empty;
            DistrictTextEN = string.Empty;
            ProvinceTextTH = string.Empty;
            ProvinceTextEN = string.Empty;
            CountryCode = string.Empty;
            CountryTextTH = string.Empty;
            CountryTextEN = string.Empty;
        }
    }
}

