﻿using DHR.HR.API.Model;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.PORTALENGINE;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Data;

namespace ESS.HR.PA
{
    public class HRPAManagement
    {
        #region Constructor
        private HRPAManagement()
        {
        }

        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, HRPAManagement> Cache = new Dictionary<string, HRPAManagement>();

        private static string ModuleID = "ESS.HR.PA";
        public string CompanyCode { get; set; }

        public static HRPAManagement CreateInstance(string oCompanyCode)
        {
            HRPAManagement oHRPAManagement = new HRPAManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oHRPAManagement;
        }
        #endregion MultiCompany  Framework

        public bool PersonalEducationAttachFiles
        {
            get
            {
                return bool.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PERSONALEDUCATIONATTACHFILES"));

            }
        }

        public bool IsViewCommunicationOnly
        {
            get
            {
                return bool.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "IS_VIEW_COMMUNICATION_ONLY"));
            }
        }
        #region Validate MaterData
        public List<MTValidateDataCodeValue> GetMTValidateDataCodeValue(string dataCode, string dataValue)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMTValidate_DataCode_DataValue;
            List<MTValidateDataCodeValue> MTDataValueCode = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetMTValidateDataCodeValue(dataCode, dataValue);
            return MTDataValueCode;
        }

        public List<MTValidateTitleName> GetMTValidateTitleName(string companyCode, string language, string iname_code = null, string iname_value = null, string iname1_code = null, string iname1_value = null,
            string iname2_code = null, string iname2_value = null, string iname3_code = null, string iname3_value = null, string iname4_code = null, string iname4_value = null, string iname5_code = null, string iname5_value = null)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMTValidate_Title_Name;
            List<MTValidateTitleName> MTDataValueCode = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetMTValidateTitleName(companyCode, language, iname_code, iname_value, iname1_code, iname1_value,
           iname2_code, iname2_value, iname3_code, iname3_value, iname4_code, iname4_value, iname5_code, iname5_value);
            return MTDataValueCode;
        }
        #endregion

        #region Configuration

        public List<PAConfiguration> GetPAConfigurationList()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPAConfigurationList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPAConfigurationList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPAConfigurationList();
        }
        public DataSet GetPAConfigurationListAll()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPAConfigurationList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPAConfigurationList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPAConfigurationListAll();
        }
        public DataSet GetPAConfigurationListByCategoryName(string categoryName)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPAConfigurationList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPAConfigurationList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPAConfigurationListByCategoryName(categoryName);
        }

        public List<PAConfiguration> GetPAConfigurationList(string[] CategoryName)
        {
            //DB
            List<PAConfiguration> oReturn = new List<PAConfiguration>();
            //List<PAConfiguration> PAConfiguration = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPAConfigurationList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPAConfigurationList;
            List<PAConfiguration> PAConfiguration = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPAConfigurationList();
            foreach (string cat in CategoryName)
            {
                oReturn.AddRange(PAConfiguration.FindAll(delegate (PAConfiguration oPAConfiguration) { return oPAConfiguration.CategoryName.ToUpper() == cat.Trim().ToUpper(); }));
            }
            return oReturn;
        }

        #endregion Configuration

        //GetContractData
        #region Personal Contract
        public List<PAContactInf> GetContractData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PAContactInf> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetContractData(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }
        #endregion Personal Contract


        //GetDocumentData
        #region Personal Document
        public List<PADocumentInf> GetDocumentData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PADocumentInf> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDocumentData(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }
        #endregion Personal Document

        //GetClothingSizeData
        #region Personal ClothingSize
        public List<PAClothingSize> GetClothingSizeData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PAClothingSize> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetClothingSizeData(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }
        #endregion Personal Document


        
        public List<PAEvaluationInf> GetEvaluationInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PAEvaluationInf> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEvaluationInfData(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }
        
        public List<PAMedicalCheckup> GetMedicalCheckupData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PAMedicalCheckup> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetMedicalCheckupData(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }public List<PAPunishmentInf> GetPunishmentInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PAPunishmentInf> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPunishmentInfData(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }


        #region Personal Info
        public List<PersonalInfoCenter> GetPersonalInfoData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PersonalInfoCenter> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalInfoData(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }
        public List<PAPersonalInf> GetPersonalInfoDataDetail(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            List<PAPersonalInf> objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalInfoDataDetail(BeginDate, EndDate, CompanyCode, EmpCode);
            return objPersonalInfo;
        }


        public PersonalInfoCenter GetPersonalInfo(string EmployeeID, DateTime checkDate)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalInfo_EmployeeID_CheckDate;
            PersonalInfoCenter objPersonalInfo = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalInfo(EmployeeID, checkDate);
            return objPersonalInfo;
        }

        public void SavePersonalData(PersonalInfoCenter data)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveINFOTYPE0002_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalInfo(data);
        }

        #endregion Personal Info

        #region Personal Address
     

        public List<PersonalAddressCenter> GetPersonalAddress(string employeeID, DateTime checkDate)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalAddress_EmployeeID_CheckDate;
            List<PersonalAddressCenter> objPersonalAddr = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalAddress(employeeID, checkDate);
            return objPersonalAddr;
        }
        public void SavePersonalAddressData(List<PersonalAddressCenter> PersonalAddrList)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_SavePersonalAddress_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalAddress(PersonalAddrList);
        }

        public List<ControlDropdownlistDataPA> GetEducationLevelDDL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationLevelDDL(EducationType, Langugae, BeginDate, EndDate, EducationCode);
        }

        public List<ControlDropdownlistDataPA> GetEducationInstitutionDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationInstitutionDLL(EducationType, Langugae, BeginDate, EndDate, EducationCode);
        }

        public List<ControlDropdownlistDataPA> GetEducationDegreeDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationDegreeDLL(EducationType, Langugae, BeginDate, EndDate, EducationCode);
        }

        public List<ControlDropdownlistDataPA> SubtypeDDL(string Type, string Langugae, DateTime BeginDate, DateTime EndDate, string Code)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SubtypeDDL(Type, Langugae, BeginDate, EndDate, Code);
        }

        public List<ControlDropdownlistDataPA> GetUserroleDLL(string Type, string Langugae, DateTime BeginDate, DateTime EndDate, string Code)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetUserroleDLL(Type, Langugae, BeginDate, EndDate, Code);
        }
        public List<ControlDropdownlistDataPAInt> GetPareDLL(string Type, string Langugae, DateTime BeginDate, DateTime EndDate, string Code)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPareDLL(Type, Langugae, BeginDate, EndDate, Code);
        }

        



        public List<ControlDropdownlistDataPA> GetEducationCountryDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationCountryDLL(EducationType, Langugae, BeginDate, EndDate, EducationCode);
        }
        public List<ControlDropdownlistDataPA> GetIndustryDLL(string Type, string Langugae, DateTime BeginDate, DateTime EndDate, string Code)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetIndustryDLL(Type, Langugae, BeginDate, EndDate, Code);
        }

        

        public List<ControlDropdownlistDataPA> GetEducationEmployeeDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string UnitCode, string UnitLevelValue, string PostCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationEmployeeDLL(EducationType, Langugae, BeginDate, EndDate, UnitCode, UnitLevelValue, PostCode);
        }


        public List<ControlDropdownlistDataPA> GetEducationMainMajorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationMainMajorDLL(EducationType, Langugae, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);
        }
        public List<ControlDropdownlistDataPA> GetEducationMinorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationMinorDLL(EducationType, Langugae, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);
        }

        


        public List<ControlDropdownlistDataPA> GetEducationQualificationDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEducationQualificationDLL(EducationType, Langugae, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);
        }

        

        public List<DistrictProvice> GetDistrict(string provinceCode, string districtCode = null, string subdistrictCode = null, string postcode = null)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_DISTRICTPROVICE_PROVINCECODE;
            List<DistrictProvice> oDistrict = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDistrictProvices(provinceCode, districtCode, subdistrictCode, postcode);
            return oDistrict;
        }
        #endregion Personal Address

        #region Personal Family

        
        public List<PAFamilyInf> GetRelationList(DateTime BeginDate, DateTime EndDate)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetFamilyData(EmployeeID, FamilyMember, ChildNo);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyData_EmployeeID;
            List<PAFamilyInf> oReturn =ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetRelationList(BeginDate, EndDate);
           // List<PAFamilyInf> oReturn =ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDHRPAFamilyInf(BeginDate, EndDate);
            return oReturn;
        }
        public List<PAFamilyInf> GetDHRPAFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate,string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyData_EmployeeID;
            List<PAFamilyInf> oReturn = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDHRPAFamilyInfByEmpCode(BeginDate, EndDate, EmpCode);
            // List<PAFamilyInf> oReturn =ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDHRPAFamilyInf(BeginDate, EndDate);
            return oReturn;
        }
        public List<PAAddressFamilyInf> GetDHRPAAddressFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate,string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyData_EmployeeID;
            List<PAAddressFamilyInf> oReturn = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDHRPAAddressFamilyInfByEmpCode(BeginDate, EndDate, EmpCode);
            // List<PAFamilyInf> oReturn =ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDHRPAFamilyInf(BeginDate, EndDate);
            return oReturn;
        }
        
        public List<PersonalFamilyCenter> GetPersonalFamilyData(string EmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyData_EmployeeID;
            List<PersonalFamilyCenter> oReturn = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalFamilyData(EmployeeID);

            oReturn = oReturn.FindAll(
                delegate (PersonalFamilyCenter temp)
                {
                    return temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).ProvidentFundHeir
                    && temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).LifeInsuranceHeir
                    && temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).AccidentInsuranceHeir
                    && temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).Surety;
                });
            return oReturn;
        }
        public PersonalFamilyCenter GetPersonalFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetFamilyData(EmployeeID, FamilyMember, ChildNo);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyData_EmployeeID_FamilyMember_ChildNo;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalFamilyData(EmployeeID, FamilyMember, ChildNo);
        }

        public void DeleteFamilyData(List<INFOTYPE0021> FamilyList)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_DeleteFamilyData_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.DeleteFamilyData(FamilyList);
        }

        //public void SaveFamilyData(List<INFOTYPE0021> FamilyList)
        //{
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveFamilyData_Data;
        //    ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SaveFamilyData(FamilyList);
        //}
        public void SaveFamilyData(List<PersonalFamilyCenter> FamilyList)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveFamilyData_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalFamilyData(FamilyList);
        }
        #endregion Personal Family


        #region Personal Corparate
        public List<PersonalPositionHisCenter> GetPositionHisByEmpCode(string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_PositionHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPositionHisByEmpCode(EmpCode);
        }

        public List<PersonalActingPositionCenter> GetActingPositionByEmpCode(string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_actingPositionHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetActingPositionByEmpCode(EmpCode);
        }

        public List<PersonalImportanceDateCenter> GetImportanceDateByEmpCode(string EmpCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_importancedate_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetImportanceDateByEmpCode(EmpCode);
        }

        #endregion Personal Corparate

        #region Personal Education
        public List<PersonalEducationCenter> GetEducation(string EmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllEducationHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllPersonalEducationHistory(EmployeeID);
        }


        

        public List<EmployeeDataCenter> GetEmployeeList(DateTime BeginDate,DateTime EndDate, string CompanyCode,string EmpCode,string UnitSelected,string LevelSelected,string PositionSelected)

        {
            string DataSource = HRPAServices.SVC(CompanyCode).GET_SEARCH_EMPLOYEE_LIST;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEmployeeList(BeginDate, EndDate, CompanyCode, EmpCode, UnitSelected, LevelSelected, PositionSelected);
        }

        public void SaveEducation(List<PersonalEducationCenter> data)
        {
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_SaveEducation_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSoruce).DataService.SavePersonalEducation(data);
        }

        public void SaveAcademicWorkMember(List<PersonalAcademicMemberCenter> data)
        {
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_SaveAcademicWorkMember_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSoruce).DataService.SavePersonalAcademicWorkMember(data);
        }


        public void SaveWorkHistory(List<WorkHistoryCenter> data)
        {
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_SaveWorkHistory_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSoruce).DataService.SaveWorkHistory(data);
        }

        public void SaveTrainningHistory(List<PersonalTrainingCenter> data)
        {
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_SaveTrainningHistory_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSoruce).DataService.SaveTrainningHistory(data);
        }

        public void DeleteEducation(List<INFOTYPE0022> data)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_DeleteEducation_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.DeleteEducation(data);
        }
        //public List<EducationLevel> GetAllEducationLevel()
        //{
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllEducationLevel;
        //    return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllEducationLevel();
        //}

        public List<EducationLevel> GetAllEducationLevel(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllEducationLevel;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllEducationLevel(LanguageCode);
        }

        public void CopyEducationLevel(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllEducationLevel;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllEducationLevel_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllEducationLevel(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllEducationLevel(LanguageCode));
        }

        public List<Certificate> GetAllCertificate()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllCertificate;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllCertificate();
        }

        public void CopyAllCertificate()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllCertificate;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllCertificate_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllCertificate(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllCertificate());
        }

        public List<Vocation> GetAllVocation()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllVocation;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllVocation();
        }

        public void CopyAllVocation()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllVocation;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllVocation_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllVocation(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllVocation());
        }

        public List<Institute> GetAllInstitute(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllInstitute;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllInstitute(LanguageCode);
        }
        public List<Certificate> GetAllCertificate(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllInstitute;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllCertificate(LanguageCode);
        }
        public Institute GetInstitute(string InstituteCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetInstitute_InstituteCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetInstitute(InstituteCode);
        }

        public void CopyInstitute()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllInstitute;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllInstitute_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllInstitute(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllInstitute());
        }

        public List<Honor> GetAllHonor()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllHonor;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllHonor();
        }
        public List<Honor> GetAllHonor(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllHonor;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllHonor(LanguageCode);
        }
        public void CopyAllHonor(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllHonor;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllHonor_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllHonor(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllHonor());
        }

        public List<Branch> GetAllBranch()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllBranch;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllBranch();
        }
   
        public List<Branch> GetAllBranch(string LanguageCode)
        {

            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllBranch;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllBranch(LanguageCode);
        }

        public void CopyAllBranch()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllBranch;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllBranch_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllBranch(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllBranch());
        }

        #endregion Personal Education

        #region Training
        public List<PersonalTrainingCenter> GetTrainingHistory(string EmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetTrainingHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.getTrainingHistory(EmployeeID);
        }
        #endregion

        #region researchmember
        public List<PersonalAcademicMemberCenter> GetAcedemicworkmember(string EmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAcedemicworkmember_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAcedemicworkmember(EmployeeID);
        }
        #endregion

        


        #region Punishment
        public List<PersonalPunishmentCenter> GetPunishmentList(string EmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPunishmentList_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPunishmentList(EmployeeID);
        }

        #endregion

        #region Personal Account

        //public PersonalAccountCenter GetBankDetail(string EmployeeID)
        //{
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_GetBankDetail_EmployeeID;
        //    return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalBankDetail(EmployeeID);
        //}

        //public void SaveBankAccountData(PersonalAccountCenter savedData)
        //{
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveBankDetail_Data;
        //    ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalBankDetail(savedData);
        //}

        #endregion Personal Account

        #region Personal Communication

        public PersonalCommunicationCenter GetCommunicationData(string employeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCommunicationData_EmployeeID;
            PersonalCommunicationCenter commData = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalCommunicationData(employeeID);
            return commData;
        }

        public void SaveCommunicationData(PersonalCommunicationCenter savedData, PersonalCommunicationCenter oldData)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveCommunicationData_SavedData_OldData;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalCommunicationData(savedData, oldData);
        }

        #endregion Personal Communication


        public void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            //DB
            //ServiceManager.CreateInstance(CompanyCode).ESSData.MarkUpdate(EmployeeID, DataCategory, RequestNo, isMarkIn);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_MarkUpdate_EmployeeID_DataCategory_RequestNo_isMarkIn;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.MarkUpdate(EmployeeID, DataCategory, RequestNo, isMarkIn);
        }

        public bool CheckMark(string EmployeeID, string DataCategory)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckMark(EmployeeID, DataCategory);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_CheckMark_EmployeeID_DataCategory;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.CheckMark(EmployeeID, DataCategory);
        }

        public bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckMark(EmployeeID, DataCategory, ReqNo);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_CheckMark_EmployeeID_DataCategory_ReqNo;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.CheckMark(EmployeeID, DataCategory, ReqNo);
        }
        public bool CheckMarkDummy(string DataCategory)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_CheckMark_DataCategory;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.CheckMarkDummy(DataCategory);
        }

        public bool CheckMarkAcademic(string DataCategory,string ReqNo)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_CheckMark_DataCategory_ReqNo;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.CheckMarkAcademic(DataCategory,ReqNo);
        }

        //public INFOTYPE0009 GetBankDetail(string EmployeeID)
        //{
        //    //SAP
        //    //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetBankDetail(EmployeeID);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_GetBankDetail_EmployeeID;
        //    return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetBankDetail(EmployeeID);
        //}



        //public List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string CategoryCode)
        //{
        //    //DB
        //    //return ServiceManager.CreateInstance(CompanyCode).ESSData.GetINFOTYPE0105ByCategoryCode(EmployeeID, CategoryCode);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_GetINFOTYPE0105ByCategoryCode_EmployeeID_CategoryCode;
        //    return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetINFOTYPE0105ByCategoryCode(EmployeeID, CategoryCode);
        //}

        #region Attachment
        public List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAttachmentFileSet(EmployeeID, RequestTypeID, RequestSubType);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAttachmentFileSet_EmployeeID_RequestTypeID_RequestSubType;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAttachmentFileSet(EmployeeID, RequestTypeID, RequestSubType);
        }

        public List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAttachmentExport(EmployeeID, RequestTypeID, RequestSubType);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAttachmentExport_EmployeeID_RequestTypeID_RequestSubType;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAttachmentExport(EmployeeID, RequestTypeID, RequestSubType);
        }

        public void FileAttachmentSubTypeSave(string RequestNo, string RequestSubType)
        {
            //DB
            //ServiceManager.CreateInstance(CompanyCode).ESSData.FileAttachmentSubTypeSave(RequestNo, RequestSubType);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_FileAttachmentSubTypeSave_RequestNo_RequestSubType;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.FileAttachmentSubTypeSave(RequestNo, RequestSubType);
        }

        #endregion Attachment


        //public void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        //{
        //    //DB
        //    //ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateINFOTYPE0105(oINF105);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_UpdateINFOTYPE0105_INF105;
        //    ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.UpdateINFOTYPE0105(oINF105);
        //}
        //public void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        //{
        //    //DB
        //    //ServiceManager.CreateInstance(CompanyCode).ESSData.InsertINFOTYPE0105(oINF105);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_InsertINFOTYPE0105_INF105;
        //    ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.InsertINFOTYPE0105(oINF105);
        //}

        //public void SaveINFOTYPE0105(List<INFOTYPE0105> oList0105, ContactInfo oContact, string EmployeeID)
        //{
        //    List<INFOTYPE0105> oListINF0105Update = new List<INFOTYPE0105>();
        //    List<INFOTYPE0105> oListINF0105Insert = new List<INFOTYPE0105>();
        //    List<string> oCategoryCode = new List<string> { "CELLPHONE", "HOMENUMBER", "MAIL", "OFFICENUMBER" };
        //    foreach (string oStr in oCategoryCode)
        //    {
        //        bool chkCategoryCode = oList0105.Any(oList => oList.CategoryCode == oStr);
        //        if (chkCategoryCode)
        //        {
        //            INFOTYPE0105 oUpdate0105 = oList0105.FirstOrDefault(a => a.CategoryCode == oStr);
        //            if (oStr == "CELLPHONE") { oUpdate0105.DataText = oContact.ContDataNew.MobilePhone; }
        //            else if (oStr == "HOMENUMBER") { oUpdate0105.DataText = oContact.ContDataNew.HomePhone; }
        //            else if (oStr == "MAIL") { oUpdate0105.DataText = oContact.ContDataNew.Email; }
        //            else if (oStr == "OFFICENUMBER") { oUpdate0105.DataText = oContact.ContDataNew.TelephoneNo; }
        //            oListINF0105Update.Add(oUpdate0105);
        //        }
        //        else
        //        {
        //            INFOTYPE0105 oInsert0105 = new INFOTYPE0105();
        //            oInsert0105.EmployeeID = EmployeeID;
        //            oInsert0105.BeginDate = DateTime.Now.Date;
        //            oInsert0105.EndDate = DateTime.MaxValue.AddTicks(-9999999);

        //            oInsert0105.CategoryCode = oStr;
        //            if (oStr == "CELLPHONE") { oInsert0105.DataText = oContact.ContDataNew.MobilePhone; }
        //            else if (oStr == "HOMENUMBER") { oInsert0105.DataText = oContact.ContDataNew.HomePhone; }
        //            else if (oStr == "MAIL") { oInsert0105.DataText = oContact.ContDataNew.Email; }
        //            else if (oStr == "OFFICENUMBER") { oInsert0105.DataText = oContact.ContDataNew.TelephoneNo; }
        //            oListINF0105Insert.Add(oInsert0105);
        //        }
        //    }
        //    //DB
        //    //ServiceManager.CreateInstance(CompanyCode).ESSData.SaveINFOTYPE0105(oListINF0105Update, oListINF0105Insert);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveINFOTYPE0105_ListINF105Update_ListINF105Insert;
        //    ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SaveINFOTYPE0105(oListINF0105Update, oListINF0105Insert);
        //}

        //public List<INFOTYPE0006> GetPersonalAddress(string employeeID, DateTime checkDate)
        //{
        //    //SAP
        //    //List<INFOTYPE0006> personalAddr = ServiceManager.CreateInstance(CompanyCode).ERPData.GetPersonalAddress(employeeID, checkDate);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPersonalAddress_EmployeeID_CheckDate;
        //    List<INFOTYPE0006> personalAddr = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalAddress(employeeID, checkDate);
        //    return personalAddr;
        //}




        public List<Relationship> GetAllRelationship()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllRelationship();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllRelationship;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllRelationship();
        }



        //public List<INFOTYPE0022> GetEducation(string EmployeeID)
        //{
        //    //SAP
        //    //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAllEducationHistory(EmployeeID);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllEducationHistory_EmployeeID;
        //    return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllEducationHistory(EmployeeID);
        //}

        public void DeleteInsertEducation(List<PersonalEducationCenter> data)
        {
            //SAP
            //ServiceManager.CreateInstance(CompanyCode).ERPData.DeleteInsertEducation(data);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_DeleteInsertEducation_Data;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.DeleteInsertEducation(data);
        }

        //public void DeleteInsertEducation(List<INFOTYPE0022> data)
        //{
        //    //SAP
        //    //ServiceManager.CreateInstance(CompanyCode).ERPData.DeleteInsertEducation(data);
        //    string DataSource = HRPAServices.SVC(CompanyCode).PA_DeleteInsertEducation_Data;
        //    ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.DeleteInsertEducation(data);
        //}

        public List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAllPreviousEmployers(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllPreviousEmployers_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllPreviousEmployers(EmployeeID);
        }

        public List<CompanyBusiness> GetAllCompanyBusiness()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllCompanyBusiness();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllCompanyBusiness;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllCompanyBusiness();
        }

        public void CopyAllCompanyBusiness()
        {
            //DB
            //ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveAllCompanyBusiness(ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllCompanyBusiness());
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllCompanyBusiness;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllCompanyBusiness_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllCompanyBusiness(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllCompanyBusiness());
        }

        public List<JobType> GetAllJobType()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllJobType();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllJobType;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllJobType();
        }

        public void CopyAllJobType()
        {
            //DB
            //ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveAllJobType(ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllJobType());
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllJobType;
            string TargetSource = HRPAServices.SVC(CompanyCode).PA_SaveAllJobType_List;
            ServiceManager.CreateInstance(CompanyCode, TargetSource).ConfigService.SaveAllJobType(ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllJobType());
        }

        public List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAllWorkHistory(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllWorkHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllWorkHistory(EmployeeID);
        }


        public List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAllSalaryHistory(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllSalaryHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllSalaryHistory(EmployeeID);
        }

        public List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetSalaryList(EmployeeID);
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_GetSalaryList_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSoruce).DataService.GetSalaryList(EmployeeID);
        }

        public List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetSalaryList(EmployeeID, CheckDate);
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_GetSalaryList_EmployeeID_CheckDate;
            return ServiceManager.CreateInstance(CompanyCode).DataService.GetSalaryList(EmployeeID, CheckDate);
        }

        public List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAllEvaluation(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllEvaluation_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllEvaluation(EmployeeID);
        }

        public List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAllJobGrade(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllJobGrade_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllJobGrade(EmployeeID);
        }

        public List<INFOTYPE0022> GetCertificate(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAllCertificateHistory(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllCertificateHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetAllCertificateHistory(EmployeeID);
        }

        public Branch GetBranchData(string BranchCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetBranchData(BranchCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetBranchData_BranchCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetBranchData(BranchCode);
        }

        public EducationLevel GetEducationLevel(string EducationLevelCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetEducationLevel(EducationLevelCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetEducationLevel_EducationLevelCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetEducationLevel(EducationLevelCode);
        }

        public Certificate GetCertificateCode(string CertificateCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCertificateCode(CertificateCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCertificateCode_CertificateCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCertificateCode(CertificateCode);
        }

        public Vocation GetVocation(string VocationCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetVocation(VocationCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetVocation_VocationCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetVocation(VocationCode);
        }

        public CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCompanyBusiness(CompanyBusinessCode);
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_GetCompanyBusiness_CompanyBusinessCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSoruce).ConfigService.GetCompanyBusiness(CompanyBusinessCode);
        }

        public JobType GetJobType(string JobCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetJobType(JobCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetJobType_JobCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetJobType(JobCode);
        }

        public List<CourseDurationUnit> GetAllUnit()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllUnit();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllUnit;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllUnit();
        }

        public List<string> GetCertCodeFromEduLevelCode(string educationLevelCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCertCodeFromEduLevelCode(educationLevelCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCertCodeFromEduLevelCode_EducationLevelCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCertCodeFromEduLevelCode(educationLevelCode);
        }

        public List<string> GetBranchFromEduLevelCode(string eduLevelCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetBranchFromEduLevelCode(eduLevelCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetBranchFromEduLevelCode_EducationLevelCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCertCodeFromEduLevelCode(eduLevelCode);
        }

        public void SaveTo(List<Certificate> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAllCertificate(Data);
        }
        //public List<Branch> GetAllBranch(string Mode)
        //{
        //    return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetAllBranch();
        //}
        public void SaveTo(List<Branch> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAllBranch(Data);
        }

        public void SaveTo(List<EducationLevel> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAllEducationLevel(Data);
        }

        public void SaveTo(List<EducationGroup> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAllEducationGroup(Data);
        }

        public List<PrefixName> GetAllPrefixNameList()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPrefixNameList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPrefixNameList();
        }

        public List<Document> GetAllDocumentList(string LanguageCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetDocumentTypeAllForDocumentInfo(LanguageCode);
        }

        public List<AcademicSubType> GetAllAcademicSubTypeList(string LanguageCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAcademicSubTypeList(LanguageCode);
        }
        public List<AcademicCategoryType> GetAllAcademicCategoryTypeList(string LanguageCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAcademicCategoryTypeList(LanguageCode);
        }
        public List<AcademicRole> GetAllAcademicRoleList(string LanguageCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAcademicRoleList(LanguageCode);
        }
        public List<ValidityType> GetAllVISAList(string LanguageCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetValidityTypeAllForDocumentInfo(LanguageCode);
        }
        public List<PrefixName> GetPrefixNameList(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetPrefixNameList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetPrefixNameList(LanguageCode);
        }

        public void SaveTo(List<PrefixName> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SavePrefixNameList(Data);
        }

        //Add by Pariyaporn 20200318
        public List<SecondTitle> GetAllSecondTitle()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetSecondTitleList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetSecondTitleList();
        }

        public List<SecondTitle> GetAllSecondTitle(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetSecondTitleList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetSecondTitleList(LanguageCode);
        }

        public SecondTitle GetSecondTitle(string Code)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetSecondTitle_Code;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetSecondTitle(Code);
        }

        public void SaveTo(List<SecondTitle> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveSecondTitleList(Data);
        }

        /// MilitaryTitle Add by Pariyaporn 20200318
        public List<MilitaryTitle> GetAllMilitaryTitle(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMilitaryTitleList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMilitaryTitleList(LanguageCode);
        }

        public List<MilitaryTitle> GetAllMilitaryTitle()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMilitaryTitleList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMilitaryTitleList();
        }

        public MilitaryTitle GetMilitaryTitle(string Code)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMilitaryTitle_Code;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMilitaryTitle(Code);
        }

        public void SaveTo(List<MilitaryTitle> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveMilitaryTitleList(Data);
        }
        /// end MilitaryTitle

        /// AcademicTitle Add by Pariyaporn 20200318
        public List<AcademicTitle> GetAllAcademicTitle(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAcademicTitleList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAcademicTitleList(LanguageCode);
        }

        public List<AcademicTitle> GetAllAcademicTitle()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAcademicTitleList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAcademicTitleList();
        }

        public AcademicTitle GetAcademicTitle(string Code)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAcademicTitle_Code;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAcademicTitle(Code);
        }

        public void SaveTo(List<AcademicTitle> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAcademicTitleList(Data);
        }
        /// end AcademicTitle

        /// MedicalTitle Add by Pariyaporn 20200318
        public List<MedicalTitle> GetAllMedicalTitle(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMedicalTitleList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMedicalTitleList(LanguageCode);
        }

        public List<MedicalTitle> GetAllMedicalTitle()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMedicalTitleList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMedicalTitleList();
        }

        public MedicalTitle GetMedicalTitle(string Code)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMedicalTitle_Code;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMedicalTitle(Code);
        }

        public void SaveTo(List<MedicalTitle> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveMedicalTitleList(Data);
        }
        /// end MedicalTitle
        //public List<Bank> GetAllBank(string Mode)
        //{
        //    return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetBankList();
        //}

        public List<Bank> GetAllBank(string LanguageCode)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPConfig.GetBankList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetBankList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetBankList(LanguageCode);
        }

        public Bank GetBank(string Code)
        {
            //DB
            //Bank oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetBank(Code);
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_GetBank_Code;
            Bank oItem = ServiceManager.CreateInstance(CompanyCode, DataSoruce).ConfigService.GetBank(Code);
            return oItem == null ? new Bank() : oItem;
        }

        public void SaveTo(List<Bank> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveBankList(Data);
        }
        public List<AddressType> GetAllAddressType()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAddressTypeList("");
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAddressTypeList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAddressTypeList("");
        }

        public List<AddressType> GetAllAddressType(string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAddressTypeList(LanguageCode);
            string DataSoruce = HRPAServices.SVC(CompanyCode).PA_GetAddressTypeList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSoruce).ConfigService.GetAddressTypeList(LanguageCode);
        }

        public void SaveTo(List<AddressType> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAddressTypeList(Data);
        }
        public List<Country> GetAllCountry()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCountryList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCountryList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCountryList();
        }

        //AddBy: Ratchatawan W. (2012-02-16)
        public List<Country> GetAllCountryList(string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCountryList(LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCountryList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCountryList(LanguageCode);
        }

        public List<Country> GetAllCountry(string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetCountryList();
        }

        public List<Region> GetRegionByCountryCode(string Code, string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetRegionByCountryCode(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetRegionByCountryCode_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetRegionByCountryCode(Code, LanguageCode);
        }

        //AddBy: Ratchatawan W. (2012-02-16)
        public Country GetCountry(string Code, string LanguageCode)
        {
            //DB
            //Country oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCountry(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCountry_Code_LanguageCode;
            Country oItem = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCountry(Code, LanguageCode);
            return oItem == null ? new Country() : oItem;
        }

        public Country GetCountry(string Code)
        {
            //DB
            //Country oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCountry(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCountry_Code;
            Country oItem = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetCountry(Code);
            return oItem == null ? new Country() : oItem;
        }

        public void SaveTo(List<Country> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveCountryList(Data);
        }

        public List<EducationGroup> GetAllEducationGroup(string LangaugeCode)
        {
            //DB
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllEducationGroup;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllEducationGroup(LangaugeCode);
        }

        public EducationGroup GetEducationGroupByCode(string EducationGroupCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetEducationGroupByCode(EducationGroupCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetEducationGroupByCode_EducationGroupCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetEducationGroupByCode(EducationGroupCode);
        }

        public void SaveTo(List<Institute> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAllInstitute(Data);
        }

        public List<Gender> GetAllGender(string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetGenderList(LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetGenderList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetGenderList(LanguageCode);
        }

        public List<Gender> GetAllGender()
        {
            return GetAllGender("");
        }

        public List<Gender> GetAllGenderList(string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetGenderList();
        }

        public Gender GetGender(string Code)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetGender(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetGender_Code;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetGender(Code);
        }

        public Gender GetGender(string Code, string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetGender(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetGender_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetGender(Code, LanguageCode);
        }

        public void SaveTo(List<Gender> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveGenderList(Data);
        }

        //public List<FamilyMember> GetAllFamilyMember(string Mode)
        //{
        //    return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetFamilyMemberList();
        //}

        public List<FamilyMember> GetAllFamilyMember()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetFamilyMemberList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyMemberList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetFamilyMemberList();
        }

        public List<FamilyMember> GetAllFamilyMember(string LanguageCode)
        {
            //DB
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyMemberList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetFamilyMemberList(LanguageCode);
        }


        public FamilyMember GetFamilyMember(string Code)
        {
            //DB
            //FamilyMember oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetFamilyMember(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyMember_Code;
            FamilyMember oItem = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetFamilyMember(Code);
            return oItem;
        }

        public void SaveTo(List<FamilyMember> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveFamilyMemberList(Data);
        }
        public void CopyFamilyMember()
        {
            //DB,SAP
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig("DB").SaveFamilyMemberList(ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig("SAP").GetFamilyMemberList());
        }

        public List<Religion> GetAllReligion()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetReligionList("");
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetReligionList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetReligionList("");
        }

        public List<Religion> GetAllReligion(string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetReligionList(LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetReligionList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetReligionList(LanguageCode);
        }

        public List<Religion> GetAllReligion(string LanguageCode, string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetReligionList(LanguageCode);
        }

        public Religion GetReligion(string Code)
        {
            //DB
            //Religion oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetReligion(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetReligion_Code;
            Religion oItem = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetReligion(Code);
            return oItem == null ? new Religion() : oItem;
        }

        public Religion GetReligion(string Code, string LanguageCode)
        {
            //DB
            //Religion oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetReligion(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetReligion_Code_LanguageCode;
            Religion oItem = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetReligion(Code, LanguageCode);
            return oItem == null ? new Religion() : oItem;
        }

        public void SaveTo(List<Religion> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveReligionList(Data);
        }


        public List<Language> GetAllLanguage()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode, DataService).ESSConfig.GetLanguageList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetLanguageList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetLanguageList();
        }

        public List<Language> GetAllLanguage(string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetLanguageList();
        }

        public Language GetLanguage(string Code)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetLanguage(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetLanguage_Code;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetLanguage(Code);
        }

        public void SaveTo(List<Language> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveLanguageList(Data);
        }
        public List<MaritalStatus> GetAllMaritalStatus()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetMaritalStatusList("");
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMaritalStatusList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMaritalStatusList("");
        }

        public List<MaritalStatus> GetAllMaritalStatus(string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetMaritalStatusList(LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMaritalStatusList_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMaritalStatusList(LanguageCode);
        }

        public List<MaritalStatus> GetAllMaritalStatus(string LanguageCode, string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetMaritalStatusList(LanguageCode);
        }

        public MaritalStatus GetMaritalStatus(string Code)
        {
            //DB
            //MaritalStatus oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetMaritalStatus(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMaritalStatus_Code;
            MaritalStatus oItem = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMaritalStatus(Code);
            return oItem == null ? new MaritalStatus() : oItem;
        }

        public MaritalStatus GetMaritalStatus(string Code, string LanguageCode)
        {
            //DB
            //MaritalStatus oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetMaritalStatus(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMaritalStatus_Code_LanguageCode;
            MaritalStatus oItem = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetMaritalStatus(Code, LanguageCode);
            return oItem == null ? new MaritalStatus() : oItem;
        }

        public void SaveTo(List<MaritalStatus> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveMaritalStatusList(Data);
        }
        public List<NameFormat> GetAllNameFormat(string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetNameFormatList();
        }

        public List<NameFormat> GetAllNameFormat()
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetNameFormatList();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetNameFormatList;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetNameFormatList();
        }

        public NameFormat GetNameFormat(string Code)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetNameFormat(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetNameFormat_Code;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetNameFormat(Code);
        }

        public void SaveTo(List<NameFormat> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveNameFormatList(Data);
        }

        public List<Nationality> GetAllNationality(string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllNationality(LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetAllNationality_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetAllNationality(LanguageCode);
        }

        public Nationality GetNationality(string Code, string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetNationality(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetNationality_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetNationality(Code, LanguageCode);
        }

        public void SaveTo(List<Province> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveProvinceList(Data);
        }

        public List<Province> GetProvinceByCountryCode(string Code, string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetProvinceByCountryCode(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetProvinceByCountryCode_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetProvinceByCountryCode(Code, LanguageCode);
        }
        public List<Province> GetProvinceAll()
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetProvinceByCountryCode_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetProvinceAll();
        }
        public List<Province> GetProvinceAll(string LanguageCode)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetProvinceByCountryCode_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetProvinceAll(LanguageCode);
        }
        public List<Province> GetProvinceByCountryCodeForTravel(string Code, string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetProvinceByCountryCodeForTravel(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetProvinceByCountryCodeForTravel_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetProvinceByCountryCodeForTravel(Code, LanguageCode);
        }
        public List<Relationship> GetAllRelationship(string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetAllRelationship();
        }

        public void SaveTo(List<Relationship> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveAllRelationship(Data);
        }
        public List<TitleName> GetAllTitleName(string LanguageCode)
        {
            //DB
            ///return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetTitleList(LanguageCode);
            List<TitleName> TitleNames = new List<TitleName>();
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetTitleList_LanguageCode;
            switch (LanguageCode.ToUpper())
            {
                case "EN":
                    TitleNames = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetTitleEnList(LanguageCode);
                    break;
                case "TH":
                    TitleNames = ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetTitleList(LanguageCode);
                    break;
            }
            return TitleNames;
        }


        public TitleName GetTitleName(string Code)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetTitle(Code);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetTitle_Code;
            return ServiceManager.CreateInstance(CompanyCode).ConfigService.GetTitle(Code);
        }

        public TitleName GetTitleName(string Code, string LanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetTitle(Code, LanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetTitle_Code_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetTitle(Code, LanguageCode);
        }

        public void SaveTo(List<TitleName> Items, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveTitleList(Items);
        }

        public List<TitleName> GetAllTitleNameList(string Mode)
        {
            return ServiceManager.CreateInstance(CompanyCode, Mode).ConfigService.GetTitleList("");
            //return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetTitleList("");
        }

        public List<District> GetDistrictByProvinceCodeForTravel(string oCountryCode, string oProvinceCode, string oLanguageCode)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetDistrictByProvinceCodeForTravel(oCountryCode, oProvinceCode, oLanguageCode);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetDistrictByProvinceCodeForTravel_CountryCode_ProvinceCode_LanguageCode;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.GetDistrictByProvinceCodeForTravel(oCountryCode, oProvinceCode, oLanguageCode);
        }

        public string GetCalculatePeriod(DateTime begindate, DateTime enddate, string oLanguage)
        {
            CommonText oCommonText = new CommonText(CompanyCode);
            //start new calculate by Pairyaporn 11/5/2020
            int monthDiff = ((enddate.Year * 12) + enddate.Month) - ((begindate.Year * 12) + begindate.Month) + 1;
            int years = (int)Math.Floor((decimal)(monthDiff / 12));
            int months = monthDiff % 12;
            //end 

            string oReturn = "";

            if (years == 0)
            {
                oReturn = string.Format("{0} {1}", months, oCommonText.LoadText("CVREPORT", oLanguage, "MONTHS"));
            }
            else
            {
                if (months == 0)
                {
                    oReturn = string.Format("{0} {1}", years, oCommonText.LoadText("CVREPORT", oLanguage, "YEARS"));
                }
                else
                {
                    oReturn = string.Format("{0} {1} {2} {3}", years, oCommonText.LoadText("CVREPORT", oLanguage, "YEARS"), months, oCommonText.LoadText("CVREPORT", oLanguage, "MONTHS"));
                }
            }

            return oReturn;
        }


        public RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetRelatedAge(EmployeeID, CheckDate);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetRelatedAge_EmployeeID_CheckDate;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetRelatedAge(EmployeeID, CheckDate);
        }

        public string GenAge(DateTimeData age, string oLanguage)
        {
            CommonText oCommonText = new CommonText(CompanyCode);
            //tmp.EducationLevelCode = oCommonText.LoadText("EDUCATIONLEVEL", oRequestParameter.CurrentEmployee.Language, row.EducationLevelCode);
            int day, month, year;
            if (age != null)
            {
                if (int.TryParse(age.Zday, out day) && int.TryParse(age.Zmonth, out month) && int.TryParse(age.Zyear, out year))
                {
                    if (age.Zyear.Equals("00"))
                    {
                        //return string.Format("{2} {3}", year, oCommonText.LoadText("CVREPORT", oLanguage, "YEARS"), month, oCommonText.LoadText("CVREPORT", oLanguage, "MONTHS"));
                        return string.Format("{2} {3}", year, oCommonText.LoadText("CVREPORT", oLanguage, "YEARS"), month, oCommonText.LoadText("CVREPORT", oLanguage, "MONTHS"));
                    }
                    else
                    {
                        if (age.Zmonth.Equals("00"))
                        {
                            return string.Format("{0} {1}", year, oCommonText.LoadText("CVREPORT", oLanguage, "YEARS"), month, oCommonText.LoadText("CVREPORT", oLanguage, "MONTHS"));
                        }
                        else
                        {
                            return string.Format("{0} {1} {2} {3}", year, oCommonText.LoadText("CVREPORT", oLanguage, "YEARS"), month, oCommonText.LoadText("CVREPORT", oLanguage, "MONTHS"));
                        }
                    }
                }
                else return "";
            }
            else
            {
                return "";
            }
        }

        public List<EvaluateDataCenter> GetEvaluateEmployee(string oEmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetEvaluateEmployee_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEvaluateEmployee(oEmployeeID);
        }

        public List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetSalaryRate(EmployeeID, EmpSubGroup);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetSalaryRate_EmployeeID_EmpSubGroup;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetSalaryRate(EmployeeID, EmpSubGroup);
        }

        public List<WorkPeriod> GetCurrentWorkHistory(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetWorkHistory(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetWorkHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetCurrentWorkHistory(EmployeeID);
        }

        public List<WorkHistoryCenter> GetWorkHistory(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetWorkHistory(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetWorkHistory_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetWorkHistory(EmployeeID);
        }

        public List<HrEditByRequestNoCenter> GetHrEditDetailByRequestNo(string DataGetegory)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetWorkHistory(EmployeeID);
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetHrEditDetailByRequestNo(DataGetegory);
        }
        public List<HrEditByRequestNoCenter> GetMarkHrEditList(string RequestType)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetWorkHistory(EmployeeID);
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetMarkHrEditList(RequestType);
        }
        


        public List<PersonalOrganizationCenter> GeOrganizationByEmpCode(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetWorkHistory(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_Organizationinfo_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GeOrganizationByEmpCode(EmployeeID);
        }

        




        public List<PersonalFamilyCenter> GetProvidentMemberData(string EmployeeID)
        {
            //SAP
            //List<INFOTYPE0021> oReturn = ServiceManager.CreateInstance(CompanyCode).ERPData.GetFamilyData(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetFamilyData_EmployeeID;
            List<PersonalFamilyCenter> oReturn = ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalFamilyData(EmployeeID);

            //Fillter àÍÒà©¾ÒÐ subtype 6 (¼ÙéÃÑº¼Å»ÃÐâÂª¹ì) 
            oReturn = oReturn.FindAll(delegate (PersonalFamilyCenter temp) { return temp.FamilyMember == "6"; });
            return oReturn;
        }


        public bool ShowTimeAwareLink(int LinkID)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.ShowTimeAwareLink(LinkID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_ShowTimeAwareLink;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.ShowTimeAwareLink(LinkID);
        }

        public List<string> GetIDCardData(string EmployeeID)
        {
            //SAP
            //return ServiceManager.CreateInstance(CompanyCode).ERPData.GetIDCardData(EmployeeID);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetIDCardData_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetIDCardData(EmployeeID);
        }

        public DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string language)
        {
            //DB
            //return ServiceManager.CreateInstance(CompanyCode).ESSData.GetCretedDocByEmployeeID(EmployeeID, RequestType, language);
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCretedDocByEmployeeID_EmployeeID_RequestType_Language;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetCreatedDocByEmployeeID(EmployeeID, RequestType, language);
        }
        public DataTable GetCreatedDocAll(int RequestType, string language)
        {
            //DB
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCretedDocByEmployeeID_EmployeeID_RequestType_Language;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetCreatedDocAll( RequestType, language);
        }
        public DataTable GetLastedDocByFlowItemCode(string EmployeeID, int RequestType, string FlowItemCode)
        {
            //DB
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetCretedDocByEmployeeID_EmployeeID_RequestType_Language;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetLastedDocByFlowItemCode(EmployeeID, RequestType, FlowItemCode);
        }

        public int GetHealthRecordStartYear()
        {
            return Convert.ToInt32(HRPAServices.SVC(CompanyCode).PA_GetHealthRecordStartYear);
        }

        public DataTable GetConfigSelectYear()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetConfigSelectYear();
        }
        public List<HealthReport> GetHealth(int Year, string EmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMedicalCheckup_Start_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetHealth(Year, EmployeeID);
        }
        public List<PAMedicalCheckup> GetMedicalCheckup(string StartDte, string EndDte, string EmployeeId)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMedicalCheckup_Startdte_Enddte_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.MedicalCheckUP(StartDte, EndDte, EmployeeId);
        }
        public List<PAMedicalCheckup> GetMedicalCheckup_Year_Empid(string Year, string EmployeeId)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_GetMedicalCheckup_Year_EmployeeID;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.MedicalCheckUP_Year_EmpID(Year, EmployeeId);
        }
        public DataTable GetConfigTableControl()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetConfigTableControl();
        }

        public bool TransferDataDHRToDB(string DataSource, string ProcedureName, out string Message)
        {
            Message = string.Empty;
            return ServiceManager.CreateInstance(CompanyCode, DataSource).ConfigService.TransferDataDHRToDB(ProcedureName, out Message);
        }
        public DataTable GetAllOrgUnit(string oLang)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAllOrgUnit(oLang);
        }
        //public List<RoleSetting> GetDataSetAdmin()
        //{
        //    return ServiceManager.CreateInstance(CompanyCode).ESSData.GetSetingAdmin();
        //}

        public DataSet GetAttachmentFileAllByEmployee(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAttachmentFileAllByEmployee(EmployeeID);
        }

        public DataTable GetUserRoleSetting()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetUserRoleSetting();
        }

        public string SaveUserRoleSetting(DataTable userRole, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.SaveUserRoleSetting(userRole, LanguageCode);
        }

        public string SetCheckViewEmployee(string adminCode,string EmpCode,string ActionType,string PosCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.SetCheckViewEmployee(adminCode, EmpCode, ActionType, PosCode);
        }

        

        public DataTable GetRoleSetting()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetRoleSetting();
        }
        public string GetStatusForResult(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetStatusForResult(RequestNo);
        }
        public DataTable GetAllResponseCodeByResponseType(string ResponseType, string CompanyCodeResponse, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAllResponseCodeByResponseType(ResponseType, CompanyCodeResponse, LanguageCode);
        }

        public DataTable GetCheckViewEmployee(string adminCode, string EmpCode, string ActionType)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetCheckViewEmployee(adminCode, EmpCode,ActionType);
        }



        

        public string GetMaxEvaluateTimes()
        {
            return HRPAServices.SVC(CompanyCode).GET_MAX_EVALUATETIMES;
        }

        //Nattawat S. Added 20200519
        //for update display values of each emp's data updated.
        public void UpdatePersonalDataAfterApprovedImmediatly(string oEmployeeID)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_UPDATE_PERSONALDATA_AFTER_APPROVED_IMMEDIATLY;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.UpdatePersonalDataAfterApprovedImmediatly(oEmployeeID);
        }

        #region Evaluate Setting
        public DataTable GetEvaluateSettingYear(string Language)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEvaluateSettingYear(Language);
        }

        public List<EvaluateSetting> GetEvaSetting()
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEvaSetting();
        }

        public List<EvaluateSetting> GetEvaluateSetting()
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEvaluateSetting();
        }

        public List<EvaluateSetting> GetEvaluateSettingByYear(int year)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEvaluateSettingByYear(year);
        }

        public List<EvaluateSetting> GetEvaluateSettingHistory(int year)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetEvaluateSettingHistory(year);
        }

        public void SaveEvaSetting(List<EvaluateSetting> EVASettingList)
        {
            string DataSource = "DB";
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SaveEvaSetting(EVASettingList);
        }
        #endregion

        public List<PhoneDirectory> GetPhoneDirectoryByCriteria(string oSearchText, string LanguageCode)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPhoneDirectoryByCriteria(oSearchText, LanguageCode);
        }

        public List<PersonalDocumentCenter> GetPersonalDocumentData(string oEmployeeID)
        {
            string DataSource = "DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalDocumentData(oEmployeeID);
        }
        public void SavePersonalDocumentData(PersonalDocumentCenter savedData)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveDocumentData_SavedData;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalDocumentInfo(savedData);
        }
        public List<PersonalAcademicCenter> GetPersonalAcademicDataByParameter(string oSubtypeCode = null, string oSubtypeValue = null, string oDOI = null)
        {
            string DataSource = "DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalAcademicDataByParameter(oSubtypeCode, oSubtypeValue, oDOI);
        }
        public List<PersonalAcademicCenter> GetPersonalAcademicDataByEmpCode(string oEmployeeID)
        {
            string DataSource = "DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalAcademicDataByEmpCode(oEmployeeID);
        }
        public List<PersonalAcademicMemberCenter> GetPersonalAcademicMemberDataByAcademicID(string oAcademicID)
        {
            string DataSource = "DHR";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetPersonalAcademicMemberDataByAcademicID(oAcademicID);
        }
        public void SavePersonalAcademicData(PersonalAcademicCenter savedData)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveAcademic_SaveData;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalAcademicInfo(savedData);
        }
        public void SavePersonalAcademicMemberData(PersonalAcademicMemberCenter savedData)
        {
            string DataSource = HRPAServices.SVC(CompanyCode).PA_SaveAcademicMember_SaveData;
            ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.SavePersonalAcademicMemberInfo(savedData);
        }
        public string GetCommonText(string Category, string Language, string Code)
        {
            CommonText oCommonText = new CommonText(CompanyCode);
            return oCommonText.LoadText(Category, Language, Code);
        }



        #region dam
        public List<ControlDropdownlistDataPA> GetDropDownDLL(string DataType, string Langugae, DateTime BeginDate, DateTime EndDate, string param1, string param2, string param3)
        {
            string DataSource = "DB";
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataService.GetDropDownDLL(DataType, Langugae, BeginDate, EndDate, param1, param2, param3);
        }

        #endregion

    }
}