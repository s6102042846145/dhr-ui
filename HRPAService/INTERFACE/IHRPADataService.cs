using System;
using System.Collections.Generic;
using System.Data;
using DHR.HR.API.Model;
using ESS.DATA;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;

namespace ESS.HR.PA.INTERFACE
{
    public interface IHRPADataService
    {

        #region Object Class Center Interface
        List<MTValidateDataCodeValue> GetMTValidateDataCodeValue(string dataCode, string dataValue);
        List<MTValidateTitleName> GetMTValidateTitleName(string companyCode, string language, string iname_code = null, string iname_value = null, string iname1_code = null, string iname1_value = null,
            string iname2_code = null, string iname2_value = null, string iname3_code = null, string iname3_value = null, string iname4_code = null, string iname4_value = null, string iname5_code = null, string iname5_value = null);
        PersonalInfoCenter GetPersonalInfo(string EmployeeID, DateTime CheckDate);
        List<PersonalInfoCenter> GetPersonalInfoData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);
        List<PAPersonalInf> GetPersonalInfoDataDetail(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);        
        void SavePersonalInfo(PersonalInfoCenter data);
        void SavePersonalDocumentInfo(PersonalDocumentCenter data);
        void SavePersonalInfo(string EmployeeID1, string EmployeeID2, List<PersonalInfoCenter> List, string Profile);

        List<PersonalAddressCenter> GetPersonalAddress(string EmployeeID);
        List<PersonalAddressCenter> GetPersonalAddress(string EmployeeID, DateTime CheckDate);
        void SavePersonalAddress(List<PersonalAddressCenter> data);

        List<PAFamilyInf> GetDHRPAFamilyInf(DateTime BeginDate, DateTime EndDate);
        List<PAFamilyInf> GetRelationList(DateTime BeginDate, DateTime EndDate);
        List<PAFamilyInf> GetDHRPAFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate, string EmpCode);
        List<PAAddressFamilyInf> GetDHRPAAddressFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate, string EmpCode);

        List<PersonalFamilyCenter> GetPersonalFamilyData(string EmployeeID);
        PersonalFamilyCenter GetPersonalFamilyData(string EmployeeID, string FamilyMember, string ChildNo);
        void SavePersonalFamilyData(List<PersonalFamilyCenter> data);

        List<PersonalEducationCenter> GetAllPersonalEducationHistory(string EmployeeID);

        List<PersonalPositionHisCenter> GetPositionHisByEmpCode(string EmpCode);

        List<PersonalActingPositionCenter> GetActingPositionByEmpCode(string EmpCode);

        List<PersonalImportanceDateCenter> GetImportanceDateByEmpCode(string EmpCode);

        List<PADocumentInf> GetDocumentData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);        
        List<PAContactInf> GetContractData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);
        
        List<PAClothingSize> GetClothingSizeData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);
        List<PAEvaluationInf> GetEvaluationInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);
        List<PAMedicalCheckup> GetMedicalCheckupData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);
        List<PAPunishmentInf> GetPunishmentInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode);
        
        List<EmployeeDataCenter> GetEmployeeList(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode, string UnitSelected, string LevelSelected, string PositionSelected);

        string ValidateEducation(List<PersonalEducationCenter> data);
        string ValidateWorkHistory(List<WorkHistoryCenter> data);
        string ValidateAcademicWorkMember(List<PersonalAcademicMemberCenter> data);
        string ValidateTranning(List<PersonalTrainingCenter> data);
        void SavePersonalEducation(List<PersonalEducationCenter> data);
        void SavePersonalAcademicWorkMember(List<PersonalAcademicMemberCenter> data);
        
        void SaveWorkHistory(List<WorkHistoryCenter> data);

        void SaveTrainningHistory(List<PersonalTrainingCenter> data);

        void DeleteInsertEducation(List<PersonalEducationCenter> data);

        List<PersonalTrainingCenter> getTrainingHistory(string oEmployeeID);

        List<PersonalAcademicMemberCenter> GetAcedemicworkmember(string oEmployeeID);

        
        List<PersonalPunishmentCenter> GetPunishmentList(string oEmployeeID);
        List<DistrictProvice> GetDistrictProvices(string provinceCode, string districtCode = null, string subdistrictCode = null, string postcode = null);

        PersonalCommunicationCenter GetPersonalCommunicationData(string employeeID);
        void SavePersonalCommunicationData(PersonalCommunicationCenter data);
        void SavePersonalCommunicationData(PersonalCommunicationCenter savedData, PersonalCommunicationCenter oldData);

        #endregion Object Class Center Interface

        void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn);

        bool CheckMark(string EmployeeID, string DataCategory);

        bool CheckMark(string EmployeeID, string DataCategory, string ReqNo);
        bool CheckMarkDummy(string DataCategory);
        bool CheckMarkAcademic(string DataCategory, string ReqNo);

        List<ControlDropdownlistDataPAInt> GetSearchContentPAIntDDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1, string Param2, string Param3);

        List<ControlDropdownlistDataPA> GetSearchContentPADDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1,string Param2,string Param3);
        List<ControlDropdownlistDataPA> GetEducationLevelDDL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate ,string EducationCode);

        List<ControlDropdownlistDataPA> GetEducationInstitutionDLL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string EducationCode);

        List<ControlDropdownlistDataPA> GetEducationDegreeDLL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string EducationCode);

        List<ControlDropdownlistDataPA> SubtypeDDL(string Type, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Code);
        
        List<ControlDropdownlistDataPA> GetUserroleDLL(string Type, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Code);

        
        List<ControlDropdownlistDataPAInt> GetPareDLL(string Type, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Code);

        List<ControlDropdownlistDataPA> GetEducationCountryDLL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string EducationCode);

        List<ControlDropdownlistDataPA> GetIndustryDLL(string Type, string Langugae, DateTime BeginDate, DateTime EndDate, string Code);




        List<ControlDropdownlistDataPA> GetEducationMainMajorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected);


        List<ControlDropdownlistDataPA> GetEducationEmployeeDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string UnitCode, string UnitLevelValue, string PostCode);





        List<ControlDropdownlistDataPA> GetEducationMinorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected);
        


        List<ControlDropdownlistDataPA> GetEducationQualificationDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected);
        



        //List<INFOTYPE0006> GetPersonalAddress(string EmployeeID);

        //List<INFOTYPE0006> GetPersonalAddress(string EmployeeID, DateTime CheckDate);

        //INFOTYPE0002 GetINFOTYPE0002(string EmployeeID, DateTime CheckDate);

        //INFOTYPE0002 GetPersonalData(string EmployeeID);



        //void SaveINFOTYPE0002(INFOTYPE0002 data);

        //List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2);

        //void SaveINFOTYPE0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile);

        //void SavePersonalAddress(List<INFOTYPE0006> data);

        //INFOTYPE0009 GetBankDetail(string EmployeeID);



        // List<INFOTYPE0021> GetFamilyData(string EmployeeID);

        // void SaveFamilyData(List<INFOTYPE0021> data);

        //INFOTYPE0021 GetFamilyData(string EmployeeID, string FamilyMember, string ChildNo);

        // List<INFOTYPE0022> GetAllEducationHistory(string EmployeeID);

        List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID);

        void SearchINFOTYPE0002(FilterData filter);

        List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID);

        List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID);

        List<INFOTYPE0008> GetSalaryList(string EmployeeID);

        List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate);

        List<INFOTYPE9003> GetAllEvaluation(string EmployeeID);

        List<INFOTYPE9004> GetAllJobGrade(string EmployeeID);

        List<INFOTYPE0022> GetAllCertificateHistory(string EmployeeID);

        void DeleteEducation(List<INFOTYPE0022> data);

        //void SaveEducation(List<INFOTYPE0022> data);

        void DeleteFamilyData(List<INFOTYPE0021> data);

        INFOTYPE0008 GetInfotype0008(string EmployeeID, DateTime CheckDate);

        List<WorkPlaceData> GetWorkPlace();

        void SaveWorkPlace(List<WorkPlaceData> list);

        List<WorkPeriod> GetCurrentWorkHistory(string EmployeeID);


        List<WorkHistoryCenter> GetWorkHistory(string EmployeeID);
        List<HrEditByRequestNoCenter> GetHrEditDetailByRequestNo(string DataGetegory);

        List<HrEditByRequestNoCenter> GetMarkHrEditList(string RequestType);
        
        List<PersonalOrganizationCenter> GeOrganizationByEmpCode(string EmployeeID);
        RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate);

        List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup);

        List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string CategoryCode);

        List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType);

        List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType);

        void FileAttachmentSubTypeSave(string RequestNo, string RequestSubType);

        void UpdateINFOTYPE0105(INFOTYPE0105 oINF105);

        void InsertINFOTYPE0105(INFOTYPE0105 oINF105);

        void SaveINFOTYPE0105(List<INFOTYPE0105> oListINF105Update, List<INFOTYPE0105> oListINF105Insert);

        //void DeleteInsertEducation(List<INFOTYPE0022> data);

        List<string> GetIDCardData(string EmployeeID);

        DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string language);
        DataTable GetCreatedDocAll( int RequestType, string language);
        DataTable GetLastedDocByFlowItemCode(string EmployeeID, int RequestType, string FlowItemCode);

        DataTable GetConfigSelectYear();

        List<HealthReport> GetHealth(int Year, string EmployeeID);
        List<PAMedicalCheckup> MedicalCheckUP(string Startdate, string Enddate, string EmployeeId);
        List<PAMedicalCheckup> MedicalCheckUP_Year_EmpID(string Year, string EmployeeId);
        DataTable GetConfigTableControl();

        DataTable GetAllOrgUnit(string oLang);

        //List<RoleSetting> GetSetingAdmin();
        DataSet GetAttachmentFileAllByEmployee(string EmployeeID);
        List<EvaluateDataCenter> GetEvaluateEmployee(string oEmployee);

        DataTable GetUserRoleSetting();
        string SaveUserRoleSetting(DataTable userRole, string LanguageCode);

        string SetCheckViewEmployee(string adminCode, string EmpCode, string ActionType,string PosCode);


        DataTable GetRoleSetting();
        string GetStatusForResult(string RequestNo);
        DataTable GetAllResponseCodeByResponseType(string ResponseType, string CompanyCodeResponse, string LanguageCode);


        DataTable GetCheckViewEmployee(string adminCode, string EmpCode, string ActionType);


        //Nattawat S. Add for update emp's data immediatly after approve
        void UpdatePersonalDataAfterApprovedImmediatly(string oEmployeeID);

        #region Evaluate Setting
        DataTable GetEvaluateSettingYear(string Language);
        List<EvaluateSetting> GetEvaSetting();
        List<EvaluateSetting> GetEvaluateSetting();
        List<EvaluateSetting> GetEvaluateSettingByYear(int year);
        List<EvaluateSetting> GetEvaluateSettingHistory(int year);
        void SaveEvaSetting(List<EvaluateSetting> oEva);
        #endregion

        List<PhoneDirectory> GetPhoneDirectoryByCriteria(string oSearchText, string oLanguageCode);
        List<PersonalDocumentCenter> GetPersonalDocumentData(string EmployeeID);
        List<PersonalAcademicCenter> GetPersonalAcademicDataByEmpCode(string EmployeeID);
        List<PersonalAcademicCenter> GetPersonalAcademicDataByParameter(string SubtypeCode = null, string SubtypeValue = null, string DOI = null);
        List<PersonalAcademicMemberCenter> GetPersonalAcademicMemberDataByAcademicID(string AcademicID);
        void SavePersonalAcademicInfo(PersonalAcademicCenter data);
        void SavePersonalAcademicMemberInfo(PersonalAcademicMemberCenter data);



        #region dam
        List<ControlDropdownlistDataPA> GetDropDownDLL(string DataType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string param1, string param2, string param3);
        List<ControlDropdownlistDataPA> GetDropdownPADDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1, string Param2, string Param3);


        #endregion
    }
}