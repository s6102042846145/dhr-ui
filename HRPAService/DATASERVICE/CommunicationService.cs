﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATASERVICE
{
    class CommunicationService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            ContactInfo oContactData = new ContactInfo();
            PersonalCommunicationCenter oComData = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetCommunicationData(Requestor.EmployeeID);

            //CommunicationData oCom = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetCommunicationData("23560073");

            oContactData.ContData = oComData;
            oContactData.ContDataNew = oComData;
            return oContactData;
        }


        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            ContactInfo oContactData = JsonConvert.DeserializeObject<ContactInfo>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //Call from editor
            ContactInfo oContactData = JsonConvert.DeserializeObject<ContactInfo>(newData.ToString());
            string dataCategory = "COMMUNICATIONDATA";
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }
        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_Communication;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }
        public void SaveExternalDataSAP(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string dataCategory = "COMMUNICATIONDATA";
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    PersonalCommunicationCenter oComDataCurrent = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetCommunicationData(Requestor.EmployeeID);
                    ContactInfo oContact = JsonConvert.DeserializeObject<ContactInfo>(Data.ToString());

                    PersonalCommunicationCenter oComDataNew = new PersonalCommunicationCenter();
                    oComDataNew.EmployeeID = Requestor.EmployeeID;
                    oComDataNew.Email = oContact.ContDataNew.Email;
                    oComDataNew.TelephoneNo = oContact.ContDataNew.TelephoneNo;
                    oComDataNew.MobilePhone = oContact.ContDataNew.MobilePhone;
                    oComDataNew.HomePhone = oContact.ContDataNew.HomePhone;
                    oComDataNew.PrimaryWebsite = oContact.ContDataNew.PrimaryWebsite;
                    oComDataNew.SecondaryWebsite = oContact.ContDataNew.SecondaryWebsite;
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveCommunicationData(oComDataNew, oComDataCurrent);
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message, oWorkAction, true, RequestNo);

                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
            }
        }

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            
            string dataCategory = "COMMUNICATIONDATA";
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    ContactInfo oContact = JsonConvert.DeserializeObject<ContactInfo>(Data.ToString());

                    PersonalCommunicationCenter oComDataNew = new PersonalCommunicationCenter();
                    oComDataNew.EmployeeID = Requestor.EmployeeID;
                    oComDataNew.TelephoneNo = oContact.ContDataNew.TelephoneNo;
                    oComDataNew.MobilePhone = oContact.ContDataNew.MobilePhone;
                    oComDataNew.HomePhone = oContact.ContDataNew.HomePhone;
                    oComDataNew.WorkPlace = oContact.ContDataNew.WorkPlace;
                    oComDataNew.Email = oContact.ContDataNew.Email;
                    oComDataNew.PrivateEmail = oContact.ContDataNew.PrivateEmail;
                    oComDataNew.LineID = oContact.ContDataNew.LineID;
                    oComDataNew.PersonOfEmergency = oContact.ContDataNew.PersonOfEmergency;
                    oComDataNew.PhoneOfEmergency = oContact.ContDataNew.PhoneOfEmergency;
                    oComDataNew.PrimaryWebsite = oContact.ContDataNew.PrimaryWebsite;
                    oComDataNew.SecondaryWebsite = oContact.ContDataNew.SecondaryWebsite;
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveCommunicationData(oComDataNew, null);
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message, oWorkAction, true, RequestNo);

                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }

                try
                {
                    // update personal data immediatly 
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).UpdatePersonalDataAfterApprovedImmediatly(Requestor.EmployeeID);
                }
                catch
                {

                }
            }
        }
        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            return "";
        }

    }
}
