﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using ESS.HR.PA.INFOTYPE;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.FILE;
using ESS.SHAREDATASERVICE;
using System.Globalization;
using System.IO;
using System.Web;

namespace ESS.HR.PA.DATASERVICE
{
    public class ResearchPatentService : AbstractDataService
    {
        private CultureInfo thCL = new CultureInfo("th-TH");
        private CultureInfo enCL = new CultureInfo("en-US");
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {

            PersonalAcademic oPersonalAcademic = new PersonalAcademic();
            // List<PersonalAcademicCenter> tmpData = new List<PersonalAcademicCenter>();
            PersonalAcademicCenter tmpData = new PersonalAcademicCenter();
            string ParamAction = string.Empty;
            string ParamPaReID = string.Empty;
            string ParamDOI = string.Empty;
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] tempParam = CreateParam.Split('|');
                if (tempParam.Count() > 2)
                {
                    ParamAction = tempParam[0];
                    ParamPaReID = tempParam[1];
                    ParamDOI = tempParam[2].Replace('*', '/');
                    tmpData = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalAcademicDataByParameter(string.Empty, string.Empty, ParamDOI).Where(a => a.DOI == ParamDOI).FirstOrDefault();
                }

                if (tmpData != null)
                {
                    DateTime oCheckDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd", enCL);
                    //check exiting member
                    if (tmpData.AcademicMembers.Where(a => a.EmployeeID == Requestor.EmployeeID).Count() == 0)
                    {
                        var personalInfo = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalInfo(Requestor.EmployeeID, oCheckDate);
                        if (personalInfo != null)
                        {
                            PersonalAcademicMemberCenter member = new PersonalAcademicMemberCenter();
                            member.EmployeeID = personalInfo.EmployeeID;
                            member.CompanyCode = personalInfo.CompanyCode;
                            member.PAReID = tmpData.PaReId.ToString();
                            member.PAReMemID = (tmpData.AcademicMembers.Count() + 1).ToString();
                            member.FullNameEN = personalInfo.FullNameEN;
                            member.FullNameTH = personalInfo.FullName;
                            member.RoleCode = string.Empty;
                            member.RoleValue = string.Empty;
                            member.RoleNameTH = string.Empty;
                            member.RoleNameEN = string.Empty;
                            tmpData.AcademicMembers.Add(member);
                        }
                    }
                    string strPathAcademic = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_PATH") + Requestor.CompanyCode + "/" + tmpData.SubTypeValue + "/";
                    string folderpath = System.Web.HttpContext.Current.Server.MapPath(strPathAcademic) + tmpData.AttachmentId + "/";

                    if (Directory.Exists(@folderpath))
                    {
                        string[] files = Directory.GetFiles(@folderpath);
                        foreach (var file in files)
                        {
                            PersonalAttachmentFileSet data = new PersonalAttachmentFileSet();
                            data.FilePath = strPathAcademic;
                            data.FileName = Path.GetFileName(file);
                            data.FileSetID = tmpData.PaReId;
                            data.Data = File.ReadAllBytes(file);
                            data.FileType = MimeMapping.GetMimeMapping(data.FileName);
                            tmpData.AttachmentFileSets.Add(data);
                        }
                    }
                }
            }
            oPersonalAcademic.AcademicData = tmpData;
            oPersonalAcademic.AcademicData_OLD = tmpData;
            return oPersonalAcademic;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PersonalAcademic oPersonalAcaData = JsonConvert.DeserializeObject<PersonalAcademic>(Data.ToString());
            DataRow dr = Info.NewRow();
            dr["ACADEMICSUBTYPE"] = oPersonalAcaData.AcademicData.SubTypeValue;
            dr["DOINO"] = oPersonalAcaData.AcademicData.DOI;
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //if (Creator.IsInRole("PAADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}

            //return ReturnKey;
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            PersonalAcademic oPersonalAcademicData = JsonConvert.DeserializeObject<PersonalAcademic>(newData.ToString());
            if (oPersonalAcademicData.AcademicData.AttachmentFileSets.Count == 0)
            {
                throw new DataServiceException("HRPA", "FILE_NOT_ENOUGH");
            }

            string subType = oPersonalAcademicData.AcademicData.DOI.ToString();
            string dataCategory = string.Format("HRPAPERSONALACADEMIC_{0}", subType);
            if (RequestNo.ToUpper() == "DUMMY")
            {
                if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMarkDummy(dataCategory))
                {
                    throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
                }
            }
            else
            {
                if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMarkAcademic(dataCategory, RequestNo))
                {
                    throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
                }
            }
        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_Academic;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

        #region SaveExternalData Delegate
        public void SaveExternalDataSAP(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {

        }

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            PersonalAcademic oPersonalAcademicData = JsonConvert.DeserializeObject<PersonalAcademic>(Data.ToString());
            string subType = oPersonalAcademicData.AcademicData.DOI.ToString();
            string dataCategory = string.Format("HRPAPERSONALACADEMIC_{0}", subType);
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    oPersonalAcademicData.AcademicData.AttachmentId = !string.IsNullOrEmpty(oPersonalAcademicData.AcademicData.AttachmentId) ? oPersonalAcademicData.AcademicData.AttachmentId : DateTime.Now.ToString("yyyyMMddHHmmss");
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalAcademicData(oPersonalAcademicData.AcademicData);
                    var modifyMember = oPersonalAcademicData.AcademicData.AcademicMembers.Where(a => a.EmployeeID == Requestor.EmployeeID).FirstOrDefault();
                    if (!string.IsNullOrEmpty(modifyMember.EmployeeID))
                    {
                        //get pareid
                        var academic = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalAcademicDataByParameter(string.Empty, string.Empty, oPersonalAcademicData.AcademicData.DOI);
                        if (academic.Count > 0)
                        {
                            modifyMember.PAReID = academic.FirstOrDefault().PaReId.ToString();
                            modifyMember.ActionType = academic.FirstOrDefault().AcademicMembers.Count > 0 ? "U" : "I";

                            HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalAcademicMemberData(modifyMember);
                        }


                    }
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message, oWorkAction, true, RequestNo);

                    throw new SaveExternalDataException("Post data error", true, ex);
                }

                try
                {
                    //save file after approve
                    bool deleteResult;
                    DeleteAcademicFile(Requestor, oPersonalAcademicData.AcademicData, out deleteResult);
                    if (deleteResult)
                    {
                        if (oPersonalAcademicData.AcademicData.AttachmentFileSets.Count > 0)
                        {
                            UploadAcademicFile(oPersonalAcademicData.AcademicData, Requestor.CompanyCode);
                        }

                    }
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("upload academic file error", true, ex);
                }
            }
            else if (State == "CANCELLED")
            {

            }
        }

        #endregion SaveExternalData Delegate

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

        public void UploadAcademicFile(PersonalAcademicCenter data, string oCompanyCode)
        {

            //  PersonalAcademic oPersonalAcademicData = JsonConvert.DeserializeObject<PersonalAcademic>(newData.ToString());
            string strPathAcademic = ShareDataManagement.LookupCache(oCompanyCode, "ESS.PA.SVC", "ACADEMIC_PATH") + oCompanyCode + "/" + data.SubTypeValue + "/";
            string strRootPath = System.Web.HttpContext.Current.Server.MapPath(strPathAcademic) + data.AttachmentId + "/";

            int FileID = 0;
            foreach (var item in data.AttachmentFileSets)
            {
                FileAttachment newatt = new FileAttachment();
                newatt.FileID = FileID + 1;
                newatt.FileSetID = data.PaReId;
                newatt.FileName = item.FileName;
                newatt.FileType = item.FileType;
                newatt.Data = item.Data;
                if (newatt.Data != null)
                {
                    item.FileID = newatt.FileID;
                    item.FileSetID = newatt.FileSetID;
                    item.FileName = newatt.FileName;
                    string filePath = strRootPath;

                    bool exists = System.IO.Directory.Exists(filePath);
                    if (!exists)
                    {
                        System.IO.Directory.CreateDirectory(filePath);
                    }
                    try
                    {
                        // Open file for reading
                        System.IO.FileStream _FileStream = new System.IO.FileStream(@filePath + newatt.FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        // Writes a block of bytes to this stream using data from
                        // a byte array.
                        _FileStream.Write(newatt.Data, 0, newatt.Data.Length);

                        // close file stream
                        _FileStream.Close();
                        FileID++;
                    }
                    catch (Exception _Exception)
                    {
                        // Error
                        Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                    }
                }
            }
        }

        public void DeleteAcademicFile(EmployeeData Requestor, PersonalAcademicCenter academicData, out bool deleteResult)
        {
            string strPathAcademic = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_PATH") + Requestor.CompanyCode + "/" + academicData.SubTypeValue + "/";
            string folderpath = System.Web.HttpContext.Current.Server.MapPath(strPathAcademic) + academicData.AttachmentId + "/";
            System.IO.DirectoryInfo di = new DirectoryInfo(folderpath);
            deleteResult = true;
            if (Directory.Exists(@folderpath))
            {
                try
                {
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                }
                catch (Exception ex)
                {
                    deleteResult = false;
                }
            }


            //foreach (DirectoryInfo dir in di.GetDirectories())
            //{
            //    dir.Delete(true);
            //}

        }
    }
}
