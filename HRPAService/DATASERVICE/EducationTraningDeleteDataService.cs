﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ESS.HR.PA.DATASERVICE
{
    public class EducationTraningDeleteDataService : AbstractDataService
    {

        public EducationTraningDeleteDataService()
        { }


        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TrainingHisInfo oTrainning = new TrainingHisInfo();
            PersonalTrainingCenter oReturn = new PersonalTrainingCenter();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                    string[] param = CreateParam.Split('|');
                    Requestor.EmployeeID = param[1];
                    oReturn = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetTrainingHistory(param[1])
                   .Where(x => x.PaTrId == param[3]).FirstOrDefault();
                    oReturn.RequestorPositionName = param[4].Replace(",", "/");
                oTrainning.Trainning = oReturn;
                    oTrainning.TrainningOld = oReturn;
            }
            return oTrainning;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            TrainingHisInfo oTrainning = JsonConvert.DeserializeObject<TrainingHisInfo>(newData.ToString());
            string DataSoruce = HRPAServices.SVC(Requestor.CompanyCode).PA_SaveWorkHistory_Data;
            oTrainning.Trainning.ActionType = "D";
            oTrainning.Trainning.ServiceType = "V";
            List<PersonalTrainingCenter> TrainningList = new List<PersonalTrainingCenter>();
            TrainningList.Add(oTrainning.Trainning);
            try
            {

                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSoruce).DataService.ValidateTranning(TrainningList);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }

        }



        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_WorkHistory;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

       

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            TrainingHisInfo oTrainning = JsonConvert.DeserializeObject<TrainingHisInfo>(Data.ToString());
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALTRAINNING_" + subType + "_" + oTrainning.Trainning.PaTrId;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    oTrainning.Trainning.ActionType = "D";
                    oTrainning.Trainning.ServiceType = "S";
                    List<PersonalTrainingCenter> trainningList = new List<PersonalTrainingCenter>();
                    trainningList.Add(oTrainning.Trainning);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveTrainningHistory(trainningList);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                    //("HROM_EXCEPTION", "OM_DATA_SAVE");
                }
            }

        }

        

    }
}
