﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;


namespace ESS.HR.PA.DATASERVICE
{
   class EducationDeleteDataService : AbstractDataService
    {

   
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            //OrganizationCenter oReturn = new OrganizationCenter();
            EducationInfo oEducationInfo = new EducationInfo();
            PersonalEducationCenter tmp = new PersonalEducationCenter();
            
            if (!String.IsNullOrEmpty(CreateParam))
            {
                string[] param = CreateParam.Split('|');
                Requestor.EmployeeID = param[1];
                tmp = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetEducation(param[1])
                    .Where(x => x.PAEduID == param[3]).FirstOrDefault();
                tmp.RequestorPositionName = param[4].Replace(",", "/");


            }
            oEducationInfo.Education = tmp;
            oEducationInfo.EducationOld = tmp;
            
            return oEducationInfo;
        }
        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {
            
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(newData.ToString());
            string DataSoruce = HRPAServices.SVC(Requestor.CompanyCode).PA_SaveEducation_Data;
            string subType = GenerateRequestSubtype(Requestor, newData);
            string dataCategory = "PERSONALEDUCATION_" + subType + "_" + oEducationInfo.Education.InstituteCode + "_" + oEducationInfo.Education.CertificateCode + "_" + oEducationInfo.Education.Branch1 + "_" + oEducationInfo.Education.PAEduID;
            oEducationInfo.Education.ActionType = "D";
            oEducationInfo.Education.ServiceType = "V";
            List<PersonalEducationCenter> educationList = new List<PersonalEducationCenter>();
            educationList.Add(oEducationInfo.Education);
            try
            {

                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSoruce).DataService.ValidateEducation(educationList);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }



            //try
            //{
            //    string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSource).DataService.ValidateOrganizationData(Organization, oActionType, oServiceType);
            //    if (oMessage != "SUCCESS")
            //        throw new Exception(oMessage);
            //}
            //catch(Exception ex)
            //{
            //    if(ex.Message.Length == 1) // when invalid by stored
            //        throw new DataServiceException("HROM_EXCEPTION", String.Format("OM_INVALID_{0}", ex.Message));
            //    else // otherwise
            //        throw new DataServiceException("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            //}
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALEDUCATION_" + subType + "_" + oEducationInfo.Education.InstituteCode + "_" + oEducationInfo.Education.CertificateCode + "_" + oEducationInfo.Education.Branch1 + "_" + oEducationInfo.Education.PAEduID;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);

            if (State == "COMPLETED")
            {
                try
                {
                    oEducationInfo.Education.ActionType = "D";
                    oEducationInfo.Education.ServiceType = "S";
                    List<PersonalEducationCenter> educationList = new List<PersonalEducationCenter>();
                    educationList.Add(oEducationInfo.Education);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveEducation(educationList);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //DataServiceException("HROM_EXCEPTION", "OM_DELETE_SAVE");
                }
                //catch (Exception ex)
                //{
                //    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                //    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                //    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message, oWorkAction, true, RequestNo);

                //    throw new SaveExternalDataException("Post data error", true, ex);
                //}
            }
            
        }

    }
}
