﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;


namespace ESS.HR.PA.DATASERVICE
{
    class PersonalContractService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PersonalInformation oPersonalInformation = new PersonalInformation();
            object personalInfo = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalInfo(Requestor.EmployeeID, DateTime.Now.Date);
            oPersonalInformation.PersonalData = (PersonalInfoCenter)personalInfo;
            oPersonalInformation.PersonalData_OLD = (PersonalInfoCenter)personalInfo;

            return oPersonalInformation;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PersonalInformation oPersonalInformation = JsonConvert.DeserializeObject<PersonalInformation>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //string ReturnKey = "";
            //if (Creator.IsInRole("PAADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}

            //return ReturnKey;
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //Call from editor
            string dataCategory = "PERSONALDATA";
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }

            PersonalInformation oPersonalInformation = JsonConvert.DeserializeObject<PersonalInformation>(newData.ToString());
            PersonalInfoCenter oPersonalData = oPersonalInformation.PersonalData;
            PersonalInfoCenter oPersonalDataOld = oPersonalInformation.PersonalData_OLD;

            List<Argument> FieldEdit = new List<Argument>();
            List<PAConfiguration> PAConfigurationList = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPAConfigurationList().FindAll(delegate (PAConfiguration item)
            { return item.CategoryName == "PersonalData" || item.CategoryName == "PersonalDataAdditional"; });

            int iFileCount = 0;
            #region Personal Data
            //FirstName	ชื่อ
            if (oPersonalData.FirstName != oPersonalDataOld.FirstName) { ValidateFileCount("FirstName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Initial	ตัวย่อ
            if (oPersonalData.Initial != oPersonalDataOld.Initial) { ValidateFileCount("Initial", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //LastName	นามสกุล
            if (oPersonalData.LastName != oPersonalDataOld.LastName) { ValidateFileCount("LastName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //MilitaryTitle	คำนำหน้าชื่อรอง
            if (oPersonalData.MilitaryTitle != oPersonalDataOld.MilitaryTitle) { ValidateFileCount("MilitaryTitle", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //NameFormat	    รูปแบบการแสดงชื่อ
            if (oPersonalData.NameFormat != oPersonalDataOld.NameFormat) { ValidateFileCount("NameFormat", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //NickName		ชื่อเล่น
            if (oPersonalData.NickName != oPersonalDataOld.NickName) { ValidateFileCount("NickName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //PrefixName	    ฐานันดรศักดิ์
            if (oPersonalData.Prefix != oPersonalDataOld.Prefix) { ValidateFileCount("PrefixName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //PrefixName1		ยศทางวิชาการ
            if (oPersonalData.AcademicTitle != oPersonalDataOld.AcademicTitle) { ValidateFileCount("AcademicTitle", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //PrefixName2		ยศทางการแพทย์
            if (oPersonalData.MedicalTitle != oPersonalDataOld.MedicalTitle) { ValidateFileCount("MedicalTitle", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //SecondTitle		คำนำหน้าชื่อรอง
            if (oPersonalData.SecondTitle != oPersonalDataOld.SecondTitle) { ValidateFileCount("SecondTitle", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //TitleName	    คำนำหน้าชื่อ
            if (oPersonalData.TitleID != oPersonalDataOld.TitleID) { ValidateFileCount("TitleID", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            #endregion

            #region Personal Data Additional
            //BirthDate				วันเกิด
            if (oPersonalData.DOB != oPersonalDataOld.DOB) { ValidateFileCount("BirthDate", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //BirthCity				ประเทศที่เกิด
            if (oPersonalData.BirthCity != oPersonalDataOld.BirthCity) { ValidateFileCount("BirthCity", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //BirthPlace				รหัสบัตรประจำตัวประชาชน
            if (oPersonalData.BirthPlace != oPersonalDataOld.BirthPlace) { ValidateFileCount("BirthPlace", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Gender				เพศ
            if (oPersonalData.Gender != oPersonalDataOld.Gender) { ValidateFileCount("Gender", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Language				ภาษา
            if (oPersonalData.Language != oPersonalDataOld.Language) { ValidateFileCount("Language", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //MaritalStatus				สถานะสมรส
            if (oPersonalData.MaritalStatus != oPersonalDataOld.MaritalStatus) { ValidateFileCount("MaritalStatus", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Nationality				สัญชาติหลัก
            if (oPersonalData.Nationality != oPersonalDataOld.Nationality) { ValidateFileCount("Nationality", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Nationality2				สัญชาติที่ 2
            if (oPersonalData.SecondNationality != oPersonalDataOld.SecondNationality) { ValidateFileCount("Nationality2", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Nationality3				สัญชาติที่ 3
            if (oPersonalData.ThirdNationality != oPersonalDataOld.ThirdNationality) { ValidateFileCount("Nationality3", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //NoOfChild				จำนวนบุตร
            if (oPersonalData.NoOfChild != oPersonalDataOld.NoOfChild) { ValidateFileCount("NoOfChild", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Religion				ศาสนา
            if (oPersonalData.Religion != oPersonalDataOld.Religion) { ValidateFileCount("Religion", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            #endregion

            if (NoOfFileAttached < iFileCount)
            {
                throw new DataServiceException("SYSTEM", "ATTACH_FILE", iFileCount.ToString());
            }
        }

        private void ValidateFileCount(string FieldName, List<PAConfiguration> PAConfigurationList, ref List<Argument> ReturnList, ref int iFileCount)
        {
            Argument arg = new Argument();
            PAConfiguration oPAConfiguration = PAConfigurationList.Find(delegate (PAConfiguration item) { return item.FieldName == FieldName; });
            if (oPAConfiguration != null && oPAConfiguration.GroupAttachFile > 0)
            {
                arg.text = oPAConfiguration.GroupFileName + "_File";
                arg.Value = oPAConfiguration.GroupAttachFile.ToString();
                if (!ReturnList.Exists(delegate (Argument filegroup) { return filegroup.text == arg.text; }))
                {
                    iFileCount += oPAConfiguration.GroupAttachFile;
                    ReturnList.Add(arg);
                }
            }
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string dataCategory = "PERSONALDATA";
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    PersonalInformation oPersonalInformation = JsonConvert.DeserializeObject<PersonalInformation>(Data.ToString());
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalData(oPersonalInformation.PersonalData);
                    
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message, oWorkAction, true, RequestNo);

                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }


                try
                {
                    // update personal data immediatly 
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).UpdatePersonalDataAfterApprovedImmediatly(Requestor.EmployeeID);
                }
                catch
                {
                    //blank
                }
            }
        }

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

    }
}
