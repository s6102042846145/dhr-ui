﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ESS.HR.PA.DATASERVICE
{
    public class EducationAcademicWorkMemberDeleteDataService : AbstractDataService
    {

        public EducationAcademicWorkMemberDeleteDataService()
        { }


        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            AcademicWorkMemberInfo oAcademicWorkMemberInfo = new AcademicWorkMemberInfo();
            PersonalAcademicMemberCenter oReturn = new PersonalAcademicMemberCenter();
            if (!string.IsNullOrEmpty(CreateParam))
            {

                string[] param = CreateParam.Split('|');
                Requestor.EmployeeID = param[1];
                oReturn = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetAcedemicworkmember(param[1])
                    .Where(x => x.PAReMemID == param[3]).FirstOrDefault();
                oReturn.RequestorPositionName = param[4].Replace(",", "/");
                oAcademicWorkMemberInfo.Academicworkmember = oReturn;
                oAcademicWorkMemberInfo.AcademicworkmemberOld = oReturn;
            }
            return oAcademicWorkMemberInfo;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            AcademicWorkMemberInfo oAcademicWorkMemberInfo = JsonConvert.DeserializeObject<AcademicWorkMemberInfo>(newData.ToString());
            string DataSoruce = HRPAServices.SVC(Requestor.CompanyCode).PA_SaveWorkHistory_Data;
            oAcademicWorkMemberInfo.Academicworkmember.ActionType = "D";
            oAcademicWorkMemberInfo.Academicworkmember.ServiceType = "V";
            List<PersonalAcademicMemberCenter> AcademicWorkMemberList = new List<PersonalAcademicMemberCenter>();
            AcademicWorkMemberList.Add(oAcademicWorkMemberInfo.Academicworkmember);
            try
            {

                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSoruce).DataService.ValidateAcademicWorkMember(AcademicWorkMemberList);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }

        }



        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_WorkHistory;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

       

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            AcademicWorkMemberInfo oAcademicWorkMemberInfo = JsonConvert.DeserializeObject<AcademicWorkMemberInfo>(Data.ToString());
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALACADEMICWORKMEMBER_"  + oAcademicWorkMemberInfo.Academicworkmember.RoleValue + "_" + oAcademicWorkMemberInfo.Academicworkmember.PAReID + "_" + oAcademicWorkMemberInfo.Academicworkmember.PAReMemID;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    oAcademicWorkMemberInfo.Academicworkmember.ActionType = "D";
                    oAcademicWorkMemberInfo.Academicworkmember.ServiceType = "S";
                    List<PersonalAcademicMemberCenter> academicworkmemberList = new List<PersonalAcademicMemberCenter>();
                    academicworkmemberList.Add(oAcademicWorkMemberInfo.Academicworkmember);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveAcademicWorkMember(academicworkmemberList);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                    //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
                }
            }

        }

        

    }
}
