﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.WORKFLOW;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ESS.HR.PA.DATASERVICE
{
    public class ReportOMService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            EmployeeManagement oEmployeeManagement = EmployeeManagement.CreateInstance(Requestor.CompanyCode);
            ReportOM oReportOM = new ReportOM();
            DataTable oDT = oEmployeeManagement.GetOrganizationStructure(Requestor.EmployeeID, Requestor.PositionID);
            oDT.Columns.Add("OrgUnitName", typeof(System.String));
            foreach (DataRow dr in oDT.Rows)
            {
                if (!string.IsNullOrEmpty(dr["Manager_ID"].ToString()) && !string.IsNullOrEmpty(dr["Manager_PositionID"].ToString()))
                {
                    EmployeeData oEmpData = new EmployeeData(dr["Manager_ID"].ToString(), dr["Manager_PositionID"].ToString(), Requestor.CompanyCode, DateTime.Now);
                    dr["OrgUnitName"] = oEmpData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                }
            }
            oReportOM.OrgStructureSnapshot = oDT;
            return oReportOM;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //string ReturnKey = "";
            //if (Creator.IsInRole("OMADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}

            //return ReturnKey;
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            ReportOM oReportOM = JsonConvert.DeserializeObject<ReportOM>(newData.ToString());
            if (string.IsNullOrEmpty(oReportOM.Remark))
            {
                throw new DataServiceException("REPORTOM", "Required_Remark");
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //throw new NotImplementedException();
        }
    }
}
