﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ESS.HR.PA.DATASERVICE
{
    public class EducationAcademicWorkMemberDataService : AbstractDataService
    {

        public EducationAcademicWorkMemberDataService()
        { }


        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            AcademicWorkMemberInfo oAcademicWorkMemberInfo = new AcademicWorkMemberInfo();
            PersonalAcademicMemberCenter tmp = new PersonalAcademicMemberCenter();
            if (!string.IsNullOrEmpty(CreateParam))
            {

                string[] param = CreateParam.Split('|');
                if (param.Count() > 0 && param[0] == "NEWID")
                {
                    Requestor.EmployeeID = param[1];
                    tmp.RequestorPositionName = param[4].Replace(",", "/");
                    tmp.EmpCode = param[1];
                    tmp.SubtypeValue = param[2];
                    tmp.PAReMemID = null;
                    tmp.BeginDate = DateTime.Now;
                    tmp.EndDate = new DateTime(9999, 12, 31, 0, 0, 0, 0);
                    oAcademicWorkMemberInfo.Academicworkmember = tmp;
                    oAcademicWorkMemberInfo.AcademicworkmemberOld = tmp;
                }
                else if(param.Count() > 2)
                {
                    Requestor.EmployeeID = param[1];
                    
                    tmp = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetAcedemicworkmember(param[1])
                    .Where(x => x.PAReMemID == param[3]).FirstOrDefault();
                    tmp.RequestorPositionName = param[4].Replace(",", "/");
                    oAcademicWorkMemberInfo.Academicworkmember = tmp;
                    oAcademicWorkMemberInfo.AcademicworkmemberOld = tmp;

                }
                   
            }
            else
            {
                tmp.EmpCode = null;
                tmp.PAReMemID = null;
                tmp.BeginDate = DateTime.Now;
                tmp.EndDate = new DateTime(9999, 12, 31, 0, 0, 0, 0);
                oAcademicWorkMemberInfo.Academicworkmember = tmp;
                oAcademicWorkMemberInfo.AcademicworkmemberOld = tmp;

            }

           
            return oAcademicWorkMemberInfo;
        }

        //public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        //{
        //    Info.Rows.Clear();
        //    AcademicWorkMemberInfo oAcademicWorkMemberInfo = JsonConvert.DeserializeObject<AcademicWorkMemberInfo>(Data.ToString());
        //    DataRow dr = Info.NewRow();
        //    Info.Rows.Add(dr);
        //}

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //string ReturnKey = "";
            //if (Creator.IsInRole("PAADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}

            //return ReturnKey;
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }
        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            
            AcademicWorkMemberInfo oAcademicWorkMemberInfo = JsonConvert.DeserializeObject<AcademicWorkMemberInfo>(Additional.ToString());
            return oAcademicWorkMemberInfo.Academicworkmember.SubtypeValue;

        }


        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            

            AcademicWorkMemberInfo oAcademicWorkMemberInfo = JsonConvert.DeserializeObject<AcademicWorkMemberInfo>(newData.ToString());
            
            //if (NoOfFileAttached < 1)
            //{
            //    throw new DataServiceException("HRPA", "FILE_NOT_ENOUGH");
            //}
            string subType = GenerateRequestSubtype(Requestor, newData);
            string dataCategory = "PERSONALACADEMICWORKMEMBER_" + subType + "_" + oAcademicWorkMemberInfo.Academicworkmember.RoleValue + "_" + oAcademicWorkMemberInfo.Academicworkmember.SubtypeValue + "_" + oAcademicWorkMemberInfo.Academicworkmember.PAReMemID;
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE");
            }
            string DataSoruce = HRPAServices.SVC(Requestor.CompanyCode).PA_SaveWorkHistory_Data;
            oAcademicWorkMemberInfo.Academicworkmember.ActionType = !string.IsNullOrEmpty(oAcademicWorkMemberInfo.Academicworkmember.PAReMemID) ? "U" : "I";
            oAcademicWorkMemberInfo.Academicworkmember.ServiceType = "V";
            List<PersonalAcademicMemberCenter> AcademicWorkMemberList = new List<PersonalAcademicMemberCenter>();
            AcademicWorkMemberList.Add(oAcademicWorkMemberInfo.Academicworkmember);
            try
            {


                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSoruce).DataService.ValidateAcademicWorkMember(AcademicWorkMemberList);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }
        }

        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_WorkHistory;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

        #region SaveExternalData Extension

        public void SaveExternalDataSAP(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALACADEMICWORKMEMBER_" + subType;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
                    List<PersonalEducationCenter> educationList = new List<PersonalEducationCenter>();
                    
                    educationList.Add(oEducationInfo.Education);
                    if (!string.IsNullOrEmpty(oEducationInfo.EducationOld.EducationLevelCode) && (oEducationInfo.Education.EducationLevelCode == oEducationInfo.EducationOld.EducationLevelCode))
                    {
                        //update by deleting Old data education
                        List<PersonalEducationCenter> educationUpdate = new List<PersonalEducationCenter>();
                        educationUpdate.Add(oEducationInfo.EducationOld);
                        educationUpdate.Add(oEducationInfo.Education);

                        HRPAManagement.CreateInstance(Requestor.CompanyCode).DeleteInsertEducation(educationUpdate);
                    }
                    else
                    {
                        HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveEducation(educationList);
                    }

                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                }
            }
        }

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
           
            AcademicWorkMemberInfo oAcademicWorkMemberInfo = JsonConvert.DeserializeObject<AcademicWorkMemberInfo>(Data.ToString());
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALACADEMICWORKMEMBER_" + subType + "_" + oAcademicWorkMemberInfo.Academicworkmember.RoleValue + "_" + oAcademicWorkMemberInfo.Academicworkmember.PAReID + "_" + oAcademicWorkMemberInfo.Academicworkmember.PAReMemID;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {

                try
                {
                    oAcademicWorkMemberInfo.Academicworkmember.ActionType = !string.IsNullOrEmpty(oAcademicWorkMemberInfo.Academicworkmember.PAReMemID) ? "U" : "I";
                    oAcademicWorkMemberInfo.Academicworkmember.ServiceType = "S";
                    List<PersonalAcademicMemberCenter> academicworkmemberList = new List<PersonalAcademicMemberCenter>();
                    academicworkmemberList.Add(oAcademicWorkMemberInfo.Academicworkmember);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveAcademicWorkMember(academicworkmemberList);
                   // HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                    //("HROM_EXCEPTION", "OM_DATA_SAVE");
                }
            }

        }

        #endregion

    }
}
