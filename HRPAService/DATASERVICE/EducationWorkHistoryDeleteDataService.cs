﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ESS.HR.PA.DATASERVICE
{
    public class EducationWorkHistoryDeleteDataService : AbstractDataService
    {

        public EducationWorkHistoryDeleteDataService()
        { }


        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            WorkHistoryInfo oWorkHistory = new WorkHistoryInfo();
            WorkHistoryCenter oReturn = new WorkHistoryCenter();
            if (!string.IsNullOrEmpty(CreateParam))
            {

                string[] param = CreateParam.Split('|');
                    Requestor.EmployeeID = param[1];
                    oReturn = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetWorkHistory(param[1])
                    .Where(x => x.PaWoId == param[3]).FirstOrDefault();
                     oReturn.RequestorPositionName = param[4].Replace(",", "/");
                    oWorkHistory.WorkHistory = oReturn;
                    oWorkHistory.WorkHistoryOld = oReturn;   
            }
            return oWorkHistory;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            return base.GenerateFlowKey(Requestor, Creator, Info, newData, RequestTypeID, RequestTypeVersionID);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            WorkHistoryInfo oWorkHistory = JsonConvert.DeserializeObject<WorkHistoryInfo>(newData.ToString());
            string DataSoruce = HRPAServices.SVC(Requestor.CompanyCode).PA_SaveWorkHistory_Data;
            oWorkHistory.WorkHistory.ActionType = "D";
            oWorkHistory.WorkHistory.ServiceType = "V";
            List<WorkHistoryCenter> WorkHistoryList = new List<WorkHistoryCenter>();
            WorkHistoryList.Add(oWorkHistory.WorkHistory);
            try
            {


                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSoruce).DataService.ValidateWorkHistory(WorkHistoryList);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }

        }



        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_WorkHistory;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

       

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            WorkHistoryInfo oWorkHistory = JsonConvert.DeserializeObject<WorkHistoryInfo>(Data.ToString());
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALWORKHISTORY" + "_" + oWorkHistory.WorkHistory.PaWoId;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {

                try
                {
                    oWorkHistory.WorkHistory.ActionType = "D";
                    oWorkHistory.WorkHistory.ServiceType = "S";
                    List<WorkHistoryCenter> workhistoryList = new List<WorkHistoryCenter>();
                    workhistoryList.Add(oWorkHistory.WorkHistory);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveWorkHistory(workhistoryList);
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                    string oWorkAction = string.Format($"EmpCode : {Requestor.EmployeeID} SaveExternalData Error");
                    EmployeeManagement.CreateInstance(Requestor.CompanyCode).InsertActionLog(null, ex.Message, oWorkAction, true, RequestNo);

                    throw new SaveExternalDataException("Post data error", true, ex);
                }
            }

        }

        

    }
}
