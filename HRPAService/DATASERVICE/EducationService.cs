﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ESS.HR.PA.DATASERVICE
{
    public class EducationService : AbstractDataService
    {

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            EducationInfo oEducationInfo = new EducationInfo();
            PersonalEducationCenter tmp = new PersonalEducationCenter();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                
                string[] param = CreateParam.Split('|');
                if (param.Count() > 0 && param[0] == "NEWID")
                {
                    Requestor.EmployeeID = param[1];
                    tmp.RequestorPositionName = param[4].Replace(",","/");
                    tmp.EmployeeID = param[1];
                    tmp.PAEduID = null;
                    tmp.EducationLevelCode = "";
                    tmp.InstituteCode = "";
                    tmp.CountryCode = "";
                    tmp.CertificateCode = "";
                    tmp.EducationGroupCode = "";
                    tmp.Branch1 = "";
                    tmp.Branch2 = "";
                    tmp.BeginDate = DateTime.Now;
                    tmp.EndDate = new DateTime(9999, 12, 31, 0, 0, 0, 0);
                    tmp.Grade = null;
                    oEducationInfo.Education = tmp;
                    oEducationInfo.EducationOld = tmp;
                }
                else if(param.Count() > 2)
                {
                    Requestor.EmployeeID = param[1];

                    tmp = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetEducation(param[1])
                    .Where(x => x.PAEduID == param[3]).FirstOrDefault();
                    tmp.RequestorPositionName = param[4].Replace(",", "/");
                }
                   
            }
            else
            {
                
                tmp.EmployeeID = Requestor.EmployeeID;

                tmp.PAEduID = "";
                tmp.EducationLevelCode = "";
                tmp.InstituteCode = "";
                tmp.CountryCode = "";
                tmp.CertificateCode = "";
                tmp.EducationGroupCode = "";
                tmp.Branch1 = "";
                tmp.Branch2 = "";
                tmp.BeginDate = DateTime.Now;
                tmp.EndDate = DateTime.Now;
                tmp.Grade = null;

            }
            oEducationInfo.Education = tmp;
            oEducationInfo.EducationOld = tmp;
            return oEducationInfo;
        }

        public override void CalculateInfoData(EmployeeData Requestor, ref object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            EducationInfo oEducation = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info, object newData, int RequestTypeID, int RequestTypeVersionID)
        {
            //string ReturnKey = "";
            //if (Creator.IsInRole("PAADMIN"))
            //{
            //    ReturnKey = "ADMIN_CREATE";
            //}
            //else
            //{
            //    ReturnKey = "EMPLOYEE_CREATE";
            //}

            //return ReturnKey;
            string ReturnKey = GenerateFlowKeyService.SVC(Requestor.CompanyCode).GetFlowKey(Requestor, Creator, RequestTypeID, RequestTypeVersionID);
            return ReturnKey;
        }
        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Additional.ToString());
            return oEducationInfo.Education.EducationLevelCode;

        }


        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(newData.ToString());
            string DataSoruce = HRPAServices.SVC(Requestor.CompanyCode).PA_SaveEducation_Data;
            //if (NoOfFileAttached < 1)
            //{
            //    throw new DataServiceException("HRPA", "FILE_NOT_ENOUGH");
            //}
            string subType = GenerateRequestSubtype(Requestor, newData);
            string dataCategory = "PERSONALEDUCATION_" + subType + "_" + oEducationInfo.Education.InstituteCode + "_" + oEducationInfo.Education.CertificateCode + "_" + oEducationInfo.Education.Branch1 + "_" + oEducationInfo.Education.PAEduID;
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE");
            }
            
            oEducationInfo.Education.ActionType = !string.IsNullOrEmpty(oEducationInfo.Education.PAEduID) ? "U" : "I";
            oEducationInfo.Education.ServiceType = "V";
            List<PersonalEducationCenter> educationList = new List<PersonalEducationCenter>();
            educationList.Add(oEducationInfo.Education);
            try {
               
                string oMessage = ServiceManager.CreateInstance(Requestor.CompanyCode, DataSoruce).DataService.ValidateEducation(educationList);
                if (oMessage != "SUCCESS")
                    throw new Exception(oMessage);
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //("HROM_EXCEPTION", "OM_DATA_VALIDATE");
            }


        }

        public delegate void SaveExternalDelegate(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode);
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string methodName = HRPAServices.SVC(Requestor.CompanyCode).SaveExternalData_Education;

            MethodInfo methodInfo = this.GetType().GetMethod(methodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance);
            SaveExternalDelegate saveExtDelegate = (SaveExternalDelegate)Delegate.CreateDelegate(typeof(SaveExternalDelegate), this, methodInfo);
            saveExtDelegate(Requestor, Info, ref Data, PreviousState, State, RequestNo, Comment, Comment2, ActionCode);

        }

        #region SaveExternalData Extension

        public void SaveExternalDataSAP(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALEDUCATION_" + subType;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
                    List<PersonalEducationCenter> educationList = new List<PersonalEducationCenter>();
                    
                    educationList.Add(oEducationInfo.Education);
                    if (!string.IsNullOrEmpty(oEducationInfo.EducationOld.EducationLevelCode) && (oEducationInfo.Education.EducationLevelCode == oEducationInfo.EducationOld.EducationLevelCode))
                    {
                        //update by deleting Old data education
                        List<PersonalEducationCenter> educationUpdate = new List<PersonalEducationCenter>();
                        educationUpdate.Add(oEducationInfo.EducationOld);
                        educationUpdate.Add(oEducationInfo.Education);

                        HRPAManagement.CreateInstance(Requestor.CompanyCode).DeleteInsertEducation(educationUpdate);
                    }
                    else
                    {
                        HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveEducation(educationList);
                    }

                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                    //("HROM_EXCEPTION", "OM_DATA_SAVE");
                }
               
            }
        }

        public void SaveExternalDataDHR(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALEDUCATION_" + subType + "_" + oEducationInfo.Education.InstituteCode + "_" + oEducationInfo.Education.CertificateCode + "_" + oEducationInfo.Education.Branch1 + "_" + oEducationInfo.Education.PAEduID;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    oEducationInfo.Education.ActionType = !string.IsNullOrEmpty(oEducationInfo.Education.PAEduID) ? "U" : "I";
                    oEducationInfo.Education.ServiceType = "S";
                    List<PersonalEducationCenter> educationList = new List<PersonalEducationCenter>();
                    educationList.Add(oEducationInfo.Education);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveEducation(educationList);
                    //HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (DataServiceException dex)
                {
                    throw new DataServiceException("VALIDATION", dex.Code, dex.Args);
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                    //("HROM_EXCEPTION", "OM_DATA_SAVE");
                }
            }

        }

        #endregion

    }
}
