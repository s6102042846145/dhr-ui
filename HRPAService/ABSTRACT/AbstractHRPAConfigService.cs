using System;
using System.Collections.Generic;
using System.Data;
using DHR.HR.API.Model;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.INTERFACE;

namespace ESS.HR.PA.ABSTRACT
{
    public abstract class AbstractHRPAConfigService : IHRPAConfigService
    {
        #region IHRPAConfig Members
        public virtual List<PAConfiguration> GetPAConfigurationList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataSet GetPAConfigurationListAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataSet GetPAConfigurationListByCategoryName(string categoryName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<TitleName> GetTitleList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<TitleName> GetTitleEnList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<Region> GetRegionByCountryCode(string Code, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual TitleName GetTitle(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual TitleName GetTitle(string Code, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTitleList(List<TitleName> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PrefixName> GetPrefixNameList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PrefixName> GetPrefixNameList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual PrefixName GetPrefixName(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePrefixNameList(List<PrefixName> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //Add by Pariyaporn 20200318
        public virtual List<SecondTitle> GetSecondTitleList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<SecondTitle> GetSecondTitleList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual SecondTitle GetSecondTitle(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveSecondTitleList(List<SecondTitle> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MilitaryTitle> GetMilitaryTitleList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<MilitaryTitle> GetMilitaryTitleList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual MilitaryTitle GetMilitaryTitle(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveMilitaryTitleList(List<MilitaryTitle> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AcademicTitle> GetAcademicTitleList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<AcademicTitle> GetAcademicTitleList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual AcademicTitle GetAcademicTitle(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAcademicTitleList(List<AcademicTitle> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MedicalTitle> GetMedicalTitleList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<MedicalTitle> GetMedicalTitleList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual MedicalTitle GetMedicalTitle(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveMedicalTitleList(List<MedicalTitle> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PAMedicalCheckup> GetMedicalCheckups(string StartDte, string EndDte, string EmployeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PAMedicalCheckup> GetMedicalCheckup_Year_Empid(string Year, string EmployeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<NameFormat> GetNameFormatList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual NameFormat GetNameFormat(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveNameFormatList(List<NameFormat> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Gender> GetGenderList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Gender> GetGenderList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Gender GetGender(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Gender GetGender(string Code, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveGenderList(List<Gender> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Country> GetCountryList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Ratchatawan W. (2012-02-16)
        public virtual List<Country> GetCountryList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Country GetCountry(string Code, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Country GetCountry(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveCountryList(List<Country> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MaritalStatus> GetMaritalStatusList()
        {
            return GetMaritalStatusList("");
        }

        public virtual List<MaritalStatus> GetMaritalStatusList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual MaritalStatus GetMaritalStatus(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual MaritalStatus GetMaritalStatus(string Code, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveMaritalStatusList(List<MaritalStatus> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Religion> GetReligionList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Religion GetReligion(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveReligionList(List<Religion> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Language> GetLanguageList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Language GetLanguage(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveLanguageList(List<Language> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Religion> GetReligionList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Religion GetReligion(string Code, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<AddressType> GetAddressTypeList()
        {
            return GetAddressTypeList("");
        }

        public virtual List<AddressType> GetAddressTypeList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAddressTypeList(List<AddressType> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Bank> GetBankList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Bank GetBank(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveBankList(List<Bank> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual FamilyMember GetFamilyMember(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveFamilyMemberList(List<FamilyMember> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<FamilyMember> GetFamilyMemberList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<FamilyMember> GetFamilyMemberList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveProvinceList(List<Province> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual List<EducationLevel> GetAllEducationLevel()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<EducationLevel> GetAllEducationLevel(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EducationLevel> GetEducationLevels(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllEducationLevel(List<EducationLevel> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EducationGroup> GetAllEducationGroup()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<EducationGroup> GetAllEducationGroup(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllEducationGroup(List<EducationGroup> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Vocation> GetAllVocation()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllVocation(List<Vocation> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Institute> GetAllInstitute()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Institute GetInstitute(string InstituteCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllInstitute(List<Institute> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Honor> GetAllHonor()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<Honor> GetAllHonor(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SaveAllHonor(List<Honor> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Branch> GetAllBranch()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<Branch> GetAllBranch(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllBranch(List<Branch> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CompanyBusiness> GetAllCompanyBusiness()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllCompanyBusiness(List<CompanyBusiness> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<JobType> GetAllJobType()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllJobType(List<JobType> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Certificate> GetAllCertificate()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllCertificate(List<Certificate> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Branch GetBranchData(string BranchCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EducationLevel GetEducationLevel(string EducationLevelCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Certificate GetCertificateCode(string CertificateCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Vocation GetVocation(string VocationCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual JobType GetJobType(string JobCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Nationality> GetAllNationality(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Nationality GetNationality(string Code, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EducationGroup GetEducationGroupByCode(string EducationGroupCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CourseDurationUnit> GetAllUnit()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetCertCodeFromEduLevelCode(string educationLevelCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetBranchFromEduLevelCode(string educationLevelCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Relationship> GetAllRelationship()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllRelationship(List<Relationship> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Province> GetProvinceByCountryCode(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<Province> GetProvinceAll()
        {
            throw new NotImplementedException();
        }
        public virtual List<Province> GetProvinceAll(string LanguageCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<Province> GetProvinceByCountryCodeForTravel(string Code, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public virtual List<District> GetDistrictByProvinceCodeForTravel(string oCountryCode, string oProvinceCode, string oLanguageCode)
        {
            throw new NotImplementedException();
        }
        public virtual bool ShowTimeAwareLink(int LinkID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual bool TransferDataDHRToDB(string oProcedureName, out string oMessage)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Document> GetDocumentTypeAllForDocumentInfo(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<ValidityType> GetValidityTypeAllForDocumentInfo(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AcademicCategoryType> GetAcademicCategoryTypeList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AcademicSubType> GetAcademicSubTypeList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AcademicRole> GetAcademicRoleList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Institute> GetAllInstitute(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Certificate> GetAllCertificate(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion
    }
}