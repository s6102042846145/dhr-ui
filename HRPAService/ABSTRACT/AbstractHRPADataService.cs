using DHR.HR.API.Model;
using ESS.DATA;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.HR.PA.INTERFACE;
using System;
using System.Collections.Generic;
using System.Data;

namespace ESS.HR.PA.ABSTRACT
{
    public abstract class AbstractHRPADataService : IHRPADataService
    {
        #region Object Class Center Interface

        public abstract PersonalInfoCenter GetPersonalInfo(string EmployeeID, DateTime CheckDate);
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}
       
        public virtual List<DistrictProvice> GetDistrictProvices(string provinceCode, string districtCode = null, string subdistrictCode = null, string postcode = null)
        {
            throw new NotImplementedException();
        }
        public virtual List<MTValidateDataCodeValue> GetMTValidateDataCodeValue(string dataCode, string dataValue)
        {
            throw new NotImplementedException();
        }
        public virtual List<MTValidateTitleName> GetMTValidateTitleName(string companyCode, string language, string iname_code = null, string iname_value = null, string iname1_code = null, string iname1_value = null,
            string iname2_code = null, string iname2_value = null, string iname3_code = null, string iname3_value = null, string iname4_code = null, string iname4_value = null, string iname5_code = null, string iname5_value = null)
        {
            throw new NotImplementedException();
        }
        public virtual List<PersonalAddressCenter> GetPersonalAddress(string EmployeeID, DateTime CheckDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<PersonalFamilyCenter> GetPersonalFamilyData(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual PersonalFamilyCenter GetPersonalFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<PersonalEducationCenter> GetAllPersonalEducationHistory(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<PersonalPositionHisCenter> GetPositionHisByEmpCode(string EmpCode)
        {
            throw new NotImplementedException();
        }

        public virtual List<PersonalActingPositionCenter> GetActingPositionByEmpCode(string EmpCode)
        {
            throw new NotImplementedException();
        }

        public virtual List<PersonalImportanceDateCenter> GetImportanceDateByEmpCode(string EmpCode)
        {
            throw new NotImplementedException();
        }


        public virtual List<PAFamilyInf> GetDHRPAFamilyInf(DateTime BeginDate, DateTime EndDatee)
        {
            throw new NotImplementedException();
        }
        public virtual List<PAFamilyInf> GetRelationList(DateTime BeginDate, DateTime EndDatee)
        {
            throw new NotImplementedException();
        }
        public virtual List<PAFamilyInf> GetDHRPAFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate, string EmpCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<PAAddressFamilyInf> GetDHRPAAddressFamilyInfByEmpCode(DateTime BeginDate, DateTime EndDate, string EmpCode)
        {
            throw new NotImplementedException();
        }

        public virtual List<PAContactInf> GetContractData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }





        public virtual List<PADocumentInf> GetDocumentData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }
        
        public virtual List<PAClothingSize> GetClothingSizeData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<PAEvaluationInf> GetEvaluationInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<PAMedicalCheckup> GetMedicalCheckupData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<PAPunishmentInf> GetPunishmentInfData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }



        public virtual List<PersonalInfoCenter> GetPersonalInfoData(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<PAPersonalInf> GetPersonalInfoDataDetail(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode)
        {
            throw new NotImplementedException();
        }
        public virtual List<EmployeeDataCenter> GetEmployeeList(DateTime BeginDate, DateTime EndDate, string CompanyCode, string EmpCode, string UnitSelected, string LevelSelected, string PositionSelected)
        {
            throw new NotImplementedException();
        }
        
        public virtual PersonalCommunicationCenter GetPersonalCommunicationData(string employeeID)
        {
            throw new NotImplementedException();
        }

        public virtual void SavePersonalInfo(PersonalInfoCenter data)
        {
            throw new NotImplementedException();
        }
        public virtual void SavePersonalDocumentInfo(PersonalDocumentCenter data)
        {
            throw new NotImplementedException();
        }
        public virtual void SavePersonalInfo(string EmployeeID1, string EmployeeID2, List<PersonalInfoCenter> List, string Profile)
        {
            throw new NotImplementedException();
        }

        public virtual void SavePersonalAddress(List<PersonalAddressCenter> data)
        {
            throw new NotImplementedException();
        }

        public virtual void SavePersonalFamilyData(List<PersonalFamilyCenter> data)
        {
            throw new NotImplementedException();
        }

        public virtual string ValidateEducation(List<PersonalEducationCenter> data)
        {
            throw new NotImplementedException();
        }

        public virtual string ValidateWorkHistory(List<WorkHistoryCenter> data)
        {
            throw new NotImplementedException();
        }

        public virtual string ValidateAcademicWorkMember(List<PersonalAcademicMemberCenter> data)
        {
            throw new NotImplementedException();
        }

     
        public virtual string ValidateTranning(List<PersonalTrainingCenter> data)
        {
            throw new NotImplementedException();
        }


        public virtual void SavePersonalEducation(List<PersonalEducationCenter> data)
        {
            throw new NotImplementedException();
        }

        public virtual void SavePersonalAcademicWorkMember(List<PersonalAcademicMemberCenter> data)
        {
            throw new NotImplementedException();
        }

        

        public virtual void SaveWorkHistory(List<WorkHistoryCenter> data)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveTrainningHistory(List<PersonalTrainingCenter> data)
        {
            throw new NotImplementedException();
        }


        public virtual void DeleteInsertEducation(List<PersonalEducationCenter> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalCommunicationData(PersonalCommunicationCenter data)
        {
            throw new NotImplementedException();
        }

        public virtual void SavePersonalCommunicationData(PersonalCommunicationCenter savedData, PersonalCommunicationCenter oldData)
        {
            throw new NotImplementedException();
        }

        public List<PersonalAddressCenter> GetPersonalAddress(string EmployeeID)
        {
            return GetPersonalAddress(EmployeeID, DateTime.Now);
        }


        #endregion Object Class Center Interface

        #region IHRPAService Members

        public virtual void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual bool CheckMarkDummy(string DataCategory)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual bool CheckMarkAcademic(string DataCategory,string ReqNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        //public abstract INFOTYPE0002 GetINFOTYPE0002(string EmployeeID, DateTime CheckDate);

        //public INFOTYPE0002 GetPersonalData(string EmployeeID)
        //{
        //    return GetINFOTYPE0002(EmployeeID, DateTime.Now);
        //}



        //public virtual void SaveINFOTYPE0002(INFOTYPE0002 data)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual void SaveINFOTYPE0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public List<INFOTYPE0006> GetPersonalAddress(string EmployeeID)
        //{
        //    return GetPersonalAddress(EmployeeID, DateTime.Now);
        //}

        //public virtual List<INFOTYPE0006> GetPersonalAddress(string EmployeeID, DateTime CheckDate)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual void SavePersonalAddress(List<INFOTYPE0006> data)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public INFOTYPE0009 GetBankDetail(string EmployeeID)
        //{
        //    return GetBankDetail(EmployeeID, DateTime.Now);
        //}

        //public virtual INFOTYPE0009 GetBankDetail(string EmployeeID, DateTime CheckDate)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual void SaveBankDetail(INFOTYPE0009 data)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual List<INFOTYPE0021> GetFamilyData(string EmployeeID)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual void SaveFamilyData(List<INFOTYPE0021> data)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual INFOTYPE0021 GetFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        //public virtual List<INFOTYPE0022> GetAllEducationHistory(string EmployeeID)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        public virtual List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SearchINFOTYPE0002(FilterData filter)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EvaluateDataCenter> GetEvaluateEmployee(string oEmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0022> GetAllCertificateHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteEducation(List<INFOTYPE0022> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveEducation(List<INFOTYPE0022> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteFamilyData(List<INFOTYPE0021> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0008 GetInfotype0008(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<WorkPlaceData> GetWorkPlace()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveWorkPlace(List<WorkPlaceData> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual CommunicationData GetCommunicationData(string employeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveCommunicationData(CommunicationData savedData, CommunicationData oldData)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<WorkPeriod> GetCurrentWorkHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<WorkHistoryCenter> GetWorkHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<HrEditByRequestNoCenter> GetHrEditDetailByRequestNo(string DataGetegory)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<HrEditByRequestNoCenter> GetMarkHrEditList(string RequestType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        

        public virtual List<PersonalOrganizationCenter> GeOrganizationByEmpCode(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string CategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void FileAttachmentSubTypeSave(string RequestNo, string RequestSubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SaveINFOTYPE0105(List<INFOTYPE0105> oListINF105Update, List<INFOTYPE0105> oListINF105Insert)
        {
            throw new Exception("The method or operation is not implemented.");
        }



        public virtual List<string> GetIDCardData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetConfigSelectYear()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<HealthReport> GetHealth(int Year, string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PAMedicalCheckup> MedicalCheckUP(string Startdate, string Enddate, string EmployeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PAMedicalCheckup> MedicalCheckUP_Year_EmpID(string Year, string EmployeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataTable GetConfigTableControl()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IHRPAService Members


        public virtual List<ControlDropdownlistDataPA> GetSearchContentPADDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1,string Param2 ,string Param3)
        {
            throw new NotImplementedException();
        }

        public virtual List<ControlDropdownlistDataPAInt> GetSearchContentPAIntDDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1, string Param2, string Param3)
        {
            throw new NotImplementedException();
        }

        


        public virtual DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string language)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataTable GetCreatedDocAll(int RequestType, string language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetLastedDocByFlowItemCode(string EmployeeID, int RequestType, string FlowItemCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataTable GetAllOrgUnit(string oLang)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        //public virtual List<RoleSetting> GetSetingAdmin()
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}
        public virtual DataSet GetAttachmentFileAllByEmployee(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PersonalTrainingCenter> getTrainingHistory(string oEmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalAcademicMemberCenter> GetAcedemicworkmember(string oEmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual List<PersonalPunishmentCenter> GetPunishmentList(string oEmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetUserRoleSetting()
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual string SetCheckViewEmployee(string adminCode, string EmpCode, string ActionType,string PosCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual string SaveUserRoleSetting(DataTable userRole, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataTable GetRoleSetting()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual string GetStatusForResult(string RequestNo)
        {
            throw new NotImplementedException();
        }
        public virtual DataTable GetAllResponseCodeByResponseType(string ResponseType, string CompanyCodeResponse, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetCheckViewEmployee(string adminCode, string EmpCode, string ActionType)
        {
            throw new NotImplementedException();
        }

       

        public virtual void UpdatePersonalDataAfterApprovedImmediatly(string oEmployeeID)
        {
            throw new NotImplementedException("UpdatePersonalDataAfterApprovedImmediatly was not implement");
        }

        public virtual List<ControlDropdownlistDataPA> GetEducationLevelDDL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ControlDropdownlistDataPA> GetEducationInstitutionDLL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual List<ControlDropdownlistDataPA> GetEducationDegreeDLL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ControlDropdownlistDataPA> SubtypeDDL(string Type, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ControlDropdownlistDataPA> GetUserroleDLL(string Type, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ControlDropdownlistDataPAInt> GetPareDLL(string Type, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ControlDropdownlistDataPA> GetEducationCountryDLL(string EducationType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string EducationCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        

        public virtual List<ControlDropdownlistDataPA> GetIndustryDLL(string Type, string Langugae, DateTime BeginDate, DateTime EndDate, string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ControlDropdownlistDataPA> GetEducationMainMajorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ControlDropdownlistDataPA> GetEducationEmployeeDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string UnitCode, string UnitLevelValue, string PostCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        

        public virtual List<ControlDropdownlistDataPA> GetEducationMinorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        


        public virtual List<ControlDropdownlistDataPA> GetEducationQualificationDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        

        #region Evaluate Setting
        public virtual DataTable GetEvaluateSettingYear(string Language)
        {
            throw new NotImplementedException();
        }
        public virtual List<EvaluateSetting> GetEvaSetting()
        {
            throw new NotImplementedException();
        }
        public virtual List<EvaluateSetting> GetEvaluateSetting()
        {
            throw new NotImplementedException();
        }
        public virtual List<EvaluateSetting> GetEvaluateSettingByYear(int year)
        {
            throw new NotImplementedException();
        }
        public virtual List<EvaluateSetting> GetEvaluateSettingHistory(int year)
        {
            throw new NotImplementedException();
        }
        public virtual void SaveEvaSetting(List<EvaluateSetting> oEva)
        {
            throw new NotImplementedException();
        }
        #endregion

        public virtual List<PhoneDirectory> GetPhoneDirectoryByCriteria(string oSearchText, string oLanguage)
        {
            throw new NotImplementedException();
        }
        public virtual List<PersonalDocumentCenter> GetPersonalDocumentData(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<PersonalAcademicCenter> GetPersonalAcademicDataByParameter(string SubtypeCode = null, string SubtypeValue = null, string DOI = null)
        {
            throw new NotImplementedException();
        }
        public virtual List<PersonalAcademicCenter> GetPersonalAcademicDataByEmpCode(string EmployeeID )
        {
            throw new NotImplementedException();
        }
        public virtual List<PersonalAcademicMemberCenter> GetPersonalAcademicMemberDataByAcademicID(string AcademicID)
        {
            throw new NotImplementedException();
        }
        public virtual void SavePersonalAcademicInfo(PersonalAcademicCenter data)
        {
            throw new NotImplementedException();
        }
        public virtual void SavePersonalAcademicMemberInfo(PersonalAcademicMemberCenter data)
        {
            throw new NotImplementedException();
        }




        #region dam
        public virtual List<ControlDropdownlistDataPA> GetDropDownDLL(string DataType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string param1, string param2, string param3)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<ControlDropdownlistDataPA> GetDropdownPADDL(string DropdownType, string LanguageCode, DateTime BeginDate, DateTime EndDate, string Param1, string Param2, string Param3)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}