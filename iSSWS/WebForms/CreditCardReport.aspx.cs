﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using ESS.HR.TR;
using ESS.HR.TR.DATACLASS;

namespace iSSWS.WebForms
{
    /// <summary>
    /// รายงานบัตรเครดิตรายเดือน
    /// </summary>
    public partial class CreditCardReport : ReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Old RDLC
                //ReportViewer1.ProcessingMode = ProcessingMode.Local;
                //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Views/Report/HR/TR/CreditCardReport.rdlc");
                //DataTable dt = GetData();
                //ReportDataSource datasource = new ReportDataSource("CreditCardReport", dt);
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_RequestorName", this.TravelReportParams.RequestorName));
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_EmployeeID", this.TravelReportParams.RequestorID));
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_CreditCardNo", this.TravelReportParams.CreditCardNo));
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_ReceiptNoBegin", this.TravelReportParams.ReceiptNoBegin));
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_ReceiptNoEnd", this.TravelReportParams.ReceiptNoEnd));
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_RequestNoBegin", this.TravelReportParams.RequestNoBegin));
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_RequestNoEnd", this.TravelReportParams.RequestNoEnd));

                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_BackgroundTableHeader", "Lavender"));
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_BorderTable", "Silver"));

                //if (this.TravelReportParams.ApproveBeginDate != default(DateTime))
                //{
                //    ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_ApproveBeginDate", this.TravelReportParams.ApproveBeginDate.ToString("dd/MM/yyyy")));
                //}

                //if (this.TravelReportParams.ApproveEndDate != default(DateTime))
                //{ 
                //    ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_ApproveEndDate", this.TravelReportParams.ApproveEndDate.ToString("dd/MM/yyyy")));
                //}

                //ReportViewer1.LocalReport.DataSources.Clear();
                //ReportViewer1.LocalReport.DataSources.Add(datasource);
                #endregion Old RDLC
                ExportExcel();
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        private DataTable GetData()
        {
            DataSet ds = new DataSet("HRTRDS");
            foreach (string companyCode in this.TravelReportParams.CompanyCode.Split(','))
            {
                if (ds.Tables.Count <= 0)
                    ds = HRTRManagement.CreateInstance(companyCode).GetCreditCardReport(this.TravelReportParams);
                else
                    ds.Tables[0].Rows.Add(HRTRManagement.CreateInstance(companyCode).GetCreditCardReport(this.TravelReportParams).Tables[0].Rows);
            }

            return ds.Tables[0];
        }
        public void ExportExcel()
        {
            try
            {
                DataTable dt = GetData();

                string attachment = "attachment; filename=CreditCardReport.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                Response.ContentType = "application/vnd.ms-excel";
                string tab = "\t";

                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("th-TH");
                //String dMonth = DateTime.Now.ToString("MMMM", culture);

                Response.Write("\t\t\t\t\t\t\t\tรายงานบัตรเครดิตรายเดือน\n");
                Response.Write("\t\t\t\t\t\t\t\t" + dt.Rows[0]["CompanyName"] + "\n");
                Response.Write("\t\t\t\t\t\t\t\tรายงาน ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + "\n");

                if (this.TravelReportParams.ClientCompanyID <= 0)
                    Response.Write("\t\tClient Company\t\t-\n");
                else
                    Response.Write("\t\tClient Company\t\t" + this.TravelReportParams.ClientCompanyName + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.RequestorID))
                    Response.Write("\t\tรหัสพนักงาน \t\t-");
                else
                    Response.Write("\t\tรหัสพนักงาน \t\t'" + this.TravelReportParams.RequestorID);

                if (DateTime.MinValue == this.TravelReportParams.ApproveBeginDate)
                    Response.Write("\t\t\t\t\t\t\t\tวันที่สร้างเอกสาร\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\t\t\t\t\t\t\tวันที่สร้างเอกสาร\t\t" + this.TravelReportParams.ApproveBeginDate + "\t\tถึง\t" + this.TravelReportParams.ApproveEndDate + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.CreditCardNo))
                    Response.Write("\t\tเลขที่บัตรเครดิต\t\t-");
                else
                    Response.Write("\t\tเลขที่บัตรเครดิต\t\t" + this.TravelReportParams.CreditCardNo);

                if (string.IsNullOrEmpty(this.TravelReportParams.ReceiptNoBegin))
                    Response.Write("\t\t\t\t\t\t\t\tเลขที่ใบเสร็จรับเงิน\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\t\t\t\t\t\t\tเลขที่ใบเสร็จรับเงิน\t\t" + this.TravelReportParams.ReceiptNoBegin + "\t\tถึง\t" + this.TravelReportParams.ReceiptNoEnd + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.RequestNoBegin))
                    Response.Write("\t\tเอกสารเลขที่\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\tเอกสารเลขที่\t\t" + this.TravelReportParams.RequestNoBegin + "\t\tถึง\t" + this.TravelReportParams.RequestNoEnd + "\n");

                Response.Write("\n\n");

                Response.Write("No." + tab);
                Response.Write("ชื่อพนักงาน" + tab + tab);
                Response.Write("ผู้ขาย/ผู้จ้าง" + tab + tab);
                Response.Write("วันที่ใบแจ้งหนี้/ในกำกับภาษี" + tab);
                Response.Write("รหัสศุนย์ต้นทุน" + tab);
                Response.Write("รหัสงบประมาณ" + tab);
                Response.Write("เลขที่เอกสาร (WEB)" + tab);
                Response.Write("เลขที่เอกสาร (SAP)" + tab);
                Response.Write("วัตถุประสงฆ์" + tab);
                Response.Write("รายการ" + tab);
                Response.Write("สกุลเงิน" + tab);
                Response.Write("จำนวนเงิน" + tab);
                Response.Write("สถานะ" + tab);
                Response.Write("\n");

                int i = 0;
                if(dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        i++;
                        Response.Write(i.ToString() + tab);
                        Response.Write("'" + dr["EmployeeID"] +tab + dr["EmployeeName"] + tab);
                        Response.Write(dr["VendorCode"] + tab + dr["VenderName"] + tab);
                        Response.Write(dr["ReceiptDate"] + tab);
                        Response.Write("'" + dr["CostCenter"] + tab);
                        Response.Write("'" + dr["IO"] + tab);
                        Response.Write(dr["RequestNo"] + tab);
                        Response.Write(dr["FIDoc"] + tab);
                        Response.Write(dr["Purpose"] + tab);
                        Response.Write(dr["ExpenseType"] + tab);
                        Response.Write(dr["Currency"] + tab);
                        Response.Write(Convert.ToDecimal(dr["Amount"]).ToString("#,###,##0.00") + tab);
                        Response.Write("สถานะ" + tab);
                        Response.Write("\n");
                    }
                }
                Response.End();
            }
            catch(Exception ex)
            {

            }
        }
    }
}