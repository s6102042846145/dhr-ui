﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using ESS.HR.TR;
using ESS.HR.TR.DATACLASS;
using System.IO;

namespace iSSWS.WebForms
{
    /// <summary>
    /// รายงานแสดงสถานะเอกสารคงค้าง
    /// </summary>
    public partial class InprogressDocReport : ReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ExportData();
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        private DataTable GetData()
        {
            DataSet ds = new DataSet("HRTRDS");
            foreach (string companyCode in this.TravelReportParams.CompanyCode.Split(','))
            {
                if (ds.Tables.Count <= 0)
                    ds = HRTRManagement.CreateInstance(companyCode).GetInprogressDocReport(this.TravelReportParams);
                else
                    ds.Tables[0].Rows.Add(HRTRManagement.CreateInstance(companyCode).GetInprogressDocReport(this.TravelReportParams).Tables[0].Rows);
            }
            return ds.Tables[0];
        }
        void ExportExcel()
        {
            try
            {
                DataTable dt = GetData();
                if (dt.Rows.Count > 0)
                {

                    string attachment = "attachment; filename=InprogressDocReport.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentEncoding = System.Text.Encoding.Unicode;
                    Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                    Response.ContentType = "application/vnd.ms-excel";
                    string tab = "\t";

                    Response.Write("\t\t\t\t\t\t\t\tรายงานแสดงสถานะเอกสารคงค้าง\n");
                    Response.Write("\t\t\t\t\t\t\t\t" + dt.Rows[0]["CompanyName"] + "\n");
                    Response.Write("\t\t\t\t\t\t\t\tรายงาน ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + "\n");

                    if (this.TravelReportParams.ClientCompanyID <= 0)
                        Response.Write("\t\tClient Company\t\t-\n");
                    else
                        Response.Write("\t\tClient Company\t\t" + this.TravelReportParams.ClientCompanyName + "\n");

                    if (string.IsNullOrEmpty(this.TravelReportParams.OrgUnitID))
                        Response.Write("\t\tรหัสหน่วยงาน \t\t-\n");
                    else
                        Response.Write("\t\tรหัสหน่วยงาน \t\t'" + this.TravelReportParams.OrgUnitID + "\n");

                    if (string.IsNullOrEmpty(this.TravelReportParams.RequestorID))
                        Response.Write("\t\tรหัสพนักงาน \t\t-\n");
                    else
                        Response.Write("\t\tรหัสพนักงาน \t\t'" + this.TravelReportParams.RequestorName + "\n");

                    if (string.IsNullOrEmpty(this.TravelReportParams.PositionID))
                        Response.Write("\t\tตำแหน่ง \t\t-\t");
                    else
                        Response.Write("\t\tตำแหน่ง \t\t" + this.TravelReportParams.Position + "\t");
                    Response.Write("\n\n");
                    //Gend Columns

                    //Response.Write("No." + tab);
                    Response.Write("ClientCompany" + tab + tab);
                    Response.Write("เลขที่สัญญา" + tab);
                    Response.Write("ชื่อพนักงาน" + tab + tab);
                    Response.Write("เลขที่เอกสาร" + tab);
                    Response.Write("ผู้ขาย/ผู้จ้าง" + tab + tab);
                    Response.Write("รหัสศูนย์ต้นทุน" + tab);
                    Response.Write("รหัสงบประมาณ" + tab);
                    Response.Write("รายการ" + tab);
                    Response.Write(tab + "จำนวนเงิน" + tab + tab + tab);
                    Response.Write("สถานะ" + tab);
                    Response.Write("รออนุมัติ" + tab);
                    Response.Write("\n");
                    Response.Write(tab + tab + tab + tab + tab + tab + tab + tab + tab + tab + tab);
                    Response.Write("สกุลเงิน" + tab + "อัตราแลกเปลี่ยน" + tab + "บาท" + tab + "VAT");
                    Response.Write("\n");

                    int i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        i++;
                        //Response.Write(i.ToString() + tab);
                        Response.Write(dr["ContractClientCompanyAbbrv"] + tab);
                        Response.Write(dr["ContractClientCompanyName"] + tab);
                        if (dr["ContractNo"].ToString() != "")
                            Response.Write("'" + dr["ContractNo"] + tab);
                        else
                            Response.Write("" + tab);
                        Response.Write("'" + dr["EmployeeID"] + tab);
                        Response.Write(dr["EmployeeName"] + tab);
                        Response.Write(dr["RequestNo"] + tab);
                        Response.Write(dr["VendorCode"] + tab);
                        Response.Write(dr["VendorName"] + tab);
                        Response.Write("'" + dr["CostCenterCode"] + tab);
                        Response.Write("'" + dr["IO"] + tab);
                        Response.Write(dr["ExpenseType"] + tab);
                        Response.Write(dr["Currency"] + tab);
                        Response.Write(dr["ExchangeRate"] + tab);
                        Response.Write(String.Format("{0:#,###,##0.00}", dr["ExpenseAmount"]) + tab);
                        Response.Write(dr["VATCode"] + tab);
                        Response.Write(dr["Status"] + tab);
                        Response.Write(dr["Receipient"] + tab);
                        Response.Write("\n");
                    }
                    Response.End();
                }
            }
            catch (Exception ex)
            {

            }
        }
        void ExportData()
        {
            string fileRdlc = "";
            string tableName = "InprogressDocReport";
            string filename = "InprogressDocReport";

            if (this.TravelReportParams.RequestorCompanyCode == "0030")
                fileRdlc = "InprogressDocByExpenseTypeReport";
            else
                fileRdlc = "InprogressDocPTTReport";

            HRTRManagement oHRTR = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode);
            string rootPath = oHRTR.FILE_ROOTPATH;
            byte[] ar;
            ar = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode).saveFileReport(fileRdlc, tableName, rootPath, this.TravelReportParams);

            MemoryStream stream = new MemoryStream();
            stream.Write(ar, 0, ar.Length);

            if(this.TravelReportParams.ExportType == "EXCEL")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            else if(this.TravelReportParams.ExportType == "PDF")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".pdf");
            Response.ContentType = "application/octectstream";
            Response.BinaryWrite(ar);
            Response.End();
        }
    }
}