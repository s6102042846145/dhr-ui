﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSSWS.Models
{
    public class FileAttachmentForMobile
    {
        public FileAttachmentForMobile()
        {
        }
        public int FileID { get; set; }
        public string RequestNo { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public byte[] Data { get; set; }
        public int FileSetID { get; set; }
        public string FilePath { get; set; }

        public bool IsDelete { get; set; }

    }
}
