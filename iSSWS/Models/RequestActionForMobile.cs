using System;
using System.Collections.Generic;
using System.Text;
using ESS.WORKFLOW;

namespace iSSWS.Models
{
    public class RequestActionForMobile
    {
        public string Code { get; set; }
        public string ActionText { get; set; }
        public string IconClass { get; set; }
        public string ColorClass { get; set; }
        public string URL { get; set; }

        public bool IsVisible { get; set; }

        public FlowItemAction oFlowItemAction { get; set; }

    }
}
