using System;
using System.Collections.Generic;
using System.Text;
using ESS.WORKFLOW.ABSTRACT;

namespace iSSWS.Models
{
    public class RequestCommentForMobile
    {
        public string CommentbyEmployeeID { get; set; }
        public string CommentbyEmployeeName { get; set; }
        public DateTime CommentDate { get; set; }
        public string CommentbyEmployeeCompanyCode { get; set; }
        public string CommentText { get; set; }
    }
}
