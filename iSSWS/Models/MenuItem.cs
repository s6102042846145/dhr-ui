﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class MenuItem
    {
        public bool isBox { get; set; }
        public bool isHeader { get; set; }
        public string icon { get; set; }
        public string text { get; set; }
        public int count { get; set; }
        public string url { get; set; }
        public int groupID { get; set; }
        public string ContentParam { get; set; }
        public string MenuCode { get; set; }
        public bool ShowCounter { get; set; } = false;
		public bool hasSub { get; set; }
        public bool IsControl { get; set; }
        public List<MenuItem> menues { get; set; }

    }
}