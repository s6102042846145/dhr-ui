﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class ObjectData
    {
        public string ObjectType { get; set; }
        public string ObjectID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ShortText { get; set; }
        public string Text { get; set; }
        public string ShortTextEn { get; set; }
        public string TextEn { get; set; }
        public string AlternativeName { get; set; }
        public string AlternativeShortName { get; set; }
    }
}