﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class RelationData
    {
        public string ObjectType { get; set; }
        public string ObjectID { get; set; }
        public string NextObjectType { get; set; }
        public string NextObjectID { get; set; }
        public string Relation { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal PercentValue { get; set; }
    }
}