﻿using System;

namespace iSSWS.Models
{
    public class PersonalData
    {
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TitleID { get; set; }
        public string TitleIDEn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string FullNameEn { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public string MaritalStatus { get; set; }
        public string Religion { get; set; }
        public string Language { get; set; }

        public PersonalData()
        {
            EmployeeID = string.Empty;
            BeginDate = default(DateTime);
            EndDate = default(DateTime);
            TitleID = string.Empty;
            TitleIDEn = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            NickName = string.Empty;
            FullNameEn = string.Empty;
            DOB = default(DateTime);
            Gender = string.Empty;
            Nationality = string.Empty;
            MaritalStatus = string.Empty;
            Religion = string.Empty;
            Language = string.Empty;
        }
    }
}