﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class OrganizationData
    {
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Name { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
        public string OrgUnit { get; set; }
        public string OrgName { get; set; }
        public string OrgNameEN { get; set; }
        public string Position { get; set; }
        public string PositionID { get; set; }
        public string CostCenter { get; set; }
        public string CostCenterText { get; set; }
        public string EmployeeName { get { return string.Format("{0} : {1}", EmployeeID, Name); } }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNameEN { get; set; }
    }
}