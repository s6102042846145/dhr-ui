﻿using ESS.EMPLOYEE;
using System;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace iSSWS
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            RouteTable.Routes.IgnoreRoute("WebServices/{resource}.asmx/{*pathInfo}");

            // This informs MVC Routing Engine to send any requests for .aspx page to the WebForms engine
            RouteTable.Routes.IgnoreRoute("WebForms/{resource}.aspx/{*pathInfo}");
            RouteTable.Routes.IgnoreRoute("WebForms/{resource}.aspx");

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }

        public void FormsAuthentication_OnAuthenticate(object sender, FormsAuthenticationEventArgs args)
        {
            if (FormsAuthentication.CookiesSupported)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(
                          Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                        if (ticket.Expired)
                        {
                            FormsAuthentication.SignOut();
                            return;
                        }
                        string[] buffer = ticket.UserData.Split(':');
                        ESS.WORKFLOW.WorkflowIdentity iden;
                        EmployeeData oEmployeeData = new ESS.EMPLOYEE.EmployeeData(buffer[1]);
                        iden = ESS.WORKFLOW.WorkflowIdentity.CreateInstance(oEmployeeData.OrgAssignment.CompanyCode).GetIdentityWithoutPassword(oEmployeeData); //buffer[3]
                        ESS.WORKFLOW.WorkflowPrinciple principle = new ESS.WORKFLOW.WorkflowPrinciple(iden);
                        args.User = principle;
                    }
                    catch (Exception ex)
                    {
                        FormsAuthentication.SignOut();
                        string Path = System.Configuration.ConfigurationManager.AppSettings["STARTPATH"];
                        Server.Transfer(string.Format("{0}/ErrorPages/GenericError.aspx?Exception={1}", Path, ex.Message));
                        //throw ex;
                        //Server.Transfer("authError.aspx?" + Server.UrlEncode("errString") + "=" + Server.UrlEncode(ex.ToString()));
                    }
                }
            }
            else
            {
                throw new HttpException("Cookieless Forms Authentication is not " +
                                        "supported for this application.");
            }
        }

        protected void Application_PostAuthorizeRequest()
        {
            if (this.IsNeedSession)
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }

        private bool IsNeedSession
        {
            get
            {
                return (HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.Contains("/Report/PrepareShowReport") ||
                        HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.Contains("/WebForms"));
            }
        }
    }
}
