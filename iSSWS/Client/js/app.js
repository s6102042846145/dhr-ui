(function(factory){if(typeof define==="function"&&define.amd){define(["jquery"],function($){return factory($)})}else if(typeof module==="object"&&typeof module.exports==="object"){exports=factory(require("jquery"))}else{factory(jQuery)}})(function($){$.easing.jswing=$.easing.swing;var pow=Math.pow,sqrt=Math.sqrt,sin=Math.sin,cos=Math.cos,PI=Math.PI,c1=1.70158,c2=c1*1.525,c3=c1+1,c4=2*PI/3,c5=2*PI/4.5;function bounceOut(x){var n1=7.5625,d1=2.75;if(x<1/d1){return n1*x*x}else if(x<2/d1){return n1*(x-=1.5/d1)*x+.75}else if(x<2.5/d1){return n1*(x-=2.25/d1)*x+.9375}else{return n1*(x-=2.625/d1)*x+.984375}}$.extend($.easing,{def:"easeOutQuad",swing:function(x){return $.easing[$.easing.def](x)},easeInQuad:function(x){return x*x},easeOutQuad:function(x){return 1-(1-x)*(1-x)},easeInOutQuad:function(x){return x<.5?2*x*x:1-pow(-2*x+2,2)/2},easeInCubic:function(x){return x*x*x},easeOutCubic:function(x){return 1-pow(1-x,3)},easeInOutCubic:function(x){return x<.5?4*x*x*x:1-pow(-2*x+2,3)/2},easeInQuart:function(x){return x*x*x*x},easeOutQuart:function(x){return 1-pow(1-x,4)},easeInOutQuart:function(x){return x<.5?8*x*x*x*x:1-pow(-2*x+2,4)/2},easeInQuint:function(x){return x*x*x*x*x},easeOutQuint:function(x){return 1-pow(1-x,5)},easeInOutQuint:function(x){return x<.5?16*x*x*x*x*x:1-pow(-2*x+2,5)/2},easeInSine:function(x){return 1-cos(x*PI/2)},easeOutSine:function(x){return sin(x*PI/2)},easeInOutSine:function(x){return-(cos(PI*x)-1)/2},easeInExpo:function(x){return x===0?0:pow(2,10*x-10)},easeOutExpo:function(x){return x===1?1:1-pow(2,-10*x)},easeInOutExpo:function(x){return x===0?0:x===1?1:x<.5?pow(2,20*x-10)/2:(2-pow(2,-20*x+10))/2},easeInCirc:function(x){return 1-sqrt(1-pow(x,2))},easeOutCirc:function(x){return sqrt(1-pow(x-1,2))},easeInOutCirc:function(x){return x<.5?(1-sqrt(1-pow(2*x,2)))/2:(sqrt(1-pow(-2*x+2,2))+1)/2},easeInElastic:function(x){return x===0?0:x===1?1:-pow(2,10*x-10)*sin((x*10-10.75)*c4)},easeOutElastic:function(x){return x===0?0:x===1?1:pow(2,-10*x)*sin((x*10-.75)*c4)+1},easeInOutElastic:function(x){return x===0?0:x===1?1:x<.5?-(pow(2,20*x-10)*sin((20*x-11.125)*c5))/2:pow(2,-20*x+10)*sin((20*x-11.125)*c5)/2+1},easeInBack:function(x){return c3*x*x*x-c1*x*x},easeOutBack:function(x){return 1+c3*pow(x-1,3)+c1*pow(x-1,2)},easeInOutBack:function(x){return x<.5?pow(2*x,2)*((c2+1)*2*x-c2)/2:(pow(2*x-2,2)*((c2+1)*(x*2-2)+c2)+2)/2},easeInBounce:function(x){return 1-bounceOut(1-x)},easeOutBounce:bounceOut,easeInOutBounce:function(x){return x<.5?(1-bounceOut(1-2*x))/2:(1+bounceOut(2*x-1))/2}})});


var mediaQuery = {
    mobile:        'only all and (max-width: 480px)',
    tablet:        'only all and (min-width: 481px) and (max-width: 768px)',
    desktop:       'only all and (min-width: 768px)',
    desktopmin:    'only all and (max-width: 992px)',
    mobiletablet:  'only all and (max-width: 768px)',
    mobiletabletLess:  'only all and (max-width: 767px)',
    force_desktop: 'only all and (min-width: 768px) and (max-width: 1200px)',
};


var _is_force_desktop = function(){
    return Modernizr.mq(mediaQuery.force_desktop);
};

var _is_tablet_up = function(){
    return Modernizr.mq(mediaQuery.desktop);
};

var _is_mobile = function(){
    return Modernizr.mq(mediaQuery.mobiletabletLess);
};
var _is_tablet = function(){
    return Modernizr.mq(mediaQuery.tablet);
};

var eqh = function eqh(group){
    var minh = 0;

    if($(group).length === 0) return false;

    $(group).css('min-height', '');
    $(group).each(function(){
        if(minh < $(this).height()){
            minh = $(this).height();
        }
    });
    $(group).css('min-height', minh);
};

function adapt_to_orientation() {
    var content_width, screen_dimension, orientation = (typeof(window.orientation) == 'undefined')?0:window.orientation;

    if (orientation == 0 || orientation == 180) {
        content_width = 1300;
        screen_dimension = screen.width * 0.98;
    } else if (orientation == 90 || orientation == -90) {
        content_width = 1300;
        screen_dimension = screen.height;
    }

    var viewport_scale = screen_dimension / content_width;

    if(_is_force_desktop()){
        $('meta[name=viewport]').attr('content', 'width=' + content_width + ',' + 'minimum-scale=' + viewport_scale + ', maximum-scale=' + viewport_scale);
        setTimeout(function(){
            $('meta[name=viewport]').attr('content', 'width=' + content_width);
        }, 1000);
    }else{
        $('meta[name=viewport]').attr('content', 'width=device-width');
    }
};

var applyViewPort = function(){
    adapt_to_orientation();
    window.addEventListener("orientationchange", function() {
        adapt_to_orientation();
    }, false);
};

var applyDatePicker = function(){
    if($('.js-datepicker').length){
        $('.js-datepicker').datepicker();
    }
};

var applySelect2 = function(){
    $('.js-select2-style-2').select2({theme: "default style-2"});

    if($('.js-select2-no-search, .js-select2').length){
        $('.js-select2-no-search, .js-select2').each(function(){
            $(this).select2({minimumResultsForSearch: -1});
            $(this).val(null).trigger('change');
        })
    }
    if($('.js-select2-no-search-style-2').length){
        $('.js-select2-no-search-style-2').each(function(){
            $(this).select2({theme: "default style-2",minimumResultsForSearch: -1});
            $(this).val(null).trigger('change');
        })
    }
};

$(function(){
    applySelect2();
    applyViewPort();
    applyDatePicker();
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();

    if($('.ba-summary-item-wrap').length){
        $('.ba-summary-item-wrap').on('click', function(){
            var $this = $(this);
            var $parent = $this.parent();
            var isActive = $this.is('.active');
            $parent.find('.ba-summary-item-wrap').removeClass('active');
            if(!isActive){
                $this.addClass('active');
            }
            return false;
        });
        $('.ba-summary-item-wrap').on('mouseenter', function(){
            var $this = $(this);
            var $parent = $this.parent();
            var isActive = $this.is('.active');
            if(!_is_mobile() && $parent.is('.ba-grid-view')){
                $parent.find('.ba-summary-item-wrap').removeClass('active');
                $this.addClass('active');
            }
        });
        $('.ba-summary-item-wrap').on('mouseleave', function(){
            var $this = $(this);
            var $parent = $this.parent();
            var isActive = $this.is('.active');
            if(!_is_mobile() && $parent.is('.ba-grid-view')){
                $parent.find('.ba-summary-item-wrap').removeClass('active');
                $this.removeClass('active');
            }
        });


        $('.ba-gridlist-view-panel').on('click', '.btn-gridlist', function(){
            var $this = $(this);
            var className = $this.data('view');
            var $parent = $this.parents('.ba-gridlist-view-panel');
            var $buttons = $parent.find('.btn-gridlist');
            var $target = $parent.find('.ba-gridlist-wrap');
            var viewClasses = 'ba-list-view ba-grid-view';

            $buttons.removeClass('active');
            $target.removeClass(viewClasses);

            $this.addClass('active');
            $target.addClass(className);
        });
    }

    eqh('.pa-document-item-hdr');
    eqh('.pa-document-reportto');
    eqh('.pa-document-item-detail-data.pa-document-employee');


    if($('.pa-document-action-checkbox').length){
        $('.pa-document-action-checkbox').on('change', 'input[type="checkbox"]', function(){
            var $parernt = $(this).parents('.pa-document-list-card');
            if($(this).is(':checked')){
                $parernt.addClass('active');
            }else{
                $parernt.removeClass('active');
            }
        });
        $('.js-select-all-in-page, .js-select-all-results').on('click', function(){
            $('.pa-document-action-checkbox input[type="checkbox"]').prop('checked', true).trigger('change');
        });
    }
});

