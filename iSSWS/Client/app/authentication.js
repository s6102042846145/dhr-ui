﻿//var SERVER = 'https://dessweb-test.pttdigital.com/DHR_UI_DEV/';
var SERVER = "http://localhost:5555/";
var adminKey;

function checkAuthorize() {
    if (window.location.hash.indexOf('#/ByPassTicket/') >= 0) {
        return true;
    }
    console.log('checkAuthorize');
    var pageName = location.pathname;
    // if (pageName === '/start.html') {
    if (pageName === '/login.html') {
        setInterval(function () {
            authorize();
        }, 1000);
    } else {
        authorize();
    }

    function authorize() {
        if (checkToken()) {
            // if (pageName === '/start.html') {
            if (pageName === '/login.html') {
                toApplication();
            }
        } else {
            window.localStorage.removeItem('com.pttdigital.dess.authorizeUser');
            window.localStorage.removeItem('com.pttdigital.dess.announcement');
            window.localStorage.removeItem('com.pttdigital.dess.menu');
            toLogin();
        }

        function checkToken() {
            var result = false;
            if (hasToken()) {
                result = checkTokenWithWebService(getEmployeeDataToken());
            }
            return result;

            function checkTokenWithWebService(userData) {
                // check token with webservice
                //
                return true;
            }
        }
    }
}

function login() {
    // add code here test login

    var tryGetAuthen = localStorage.getItem('com.pttdigital.dess.authorizeUser')
    console.debug(tryGetAuthen);
    if (tryGetAuthen !== null) {
        document.getElementById('ErrorMessage').style.visibility = "visible";
        document.getElementById('ErrorMessage').innerHTML = "Another user is logged in, please check. Otherwise, please contact system administrator.";
        return;
    }


    setLoader(true);
    var inputUsername = document.getElementById('CORE_txtInputAccount').value;
    var inputPassword = document.getElementById('CORE_txtInputPassword').value;
    var x = document.querySelector("select");
    var inputCompanyCode = x.options[x.selectedIndex].value;
    var inputCompanyName = x.options[x.selectedIndex].label;
    console.log(inputCompanyCode);

    document.getElementById('ErrorMessage').style.visibility = "hidden";
    if (inputUsername !== null && inputPassword !== null && inputUsername.length > 0 && inputPassword.length > 0
        && inputCompanyCode !== null && inputCompanyCode.length > 0) {
        document.getElementById('ErrorMessage').style.visibility = "hidden";
        var currentLocation = window.location;
        var key = gup('key', currentLocation);
        if (key === adminKey && key !== '') {
            authenWebByPassAdminService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
        } else {
            authenWebService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
        }

    } else {
        document.getElementById('ErrorMessage').style.visibility = "visible";
        document.getElementById('ErrorMessage').innerHTML = "Please input Username/Password/Company";
        setLoader(false);
    }

    function authenWebService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName) {
        //// check username, password with webservice
        var URL = SERVER + 'workflow/authenticate';
        var oUserLogin = { Username: inputUsername, Password: inputPassword, CompanyCode: inputCompanyCode, CompanyName: inputCompanyName };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            //dataType: 'json',
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data.ErrorMessage === "") {
                        //window.localStorage.setItem('com.pttdigital.ess.authorizeUser', JSON.stringify(data.EmployeeData));
                        //window.localStorage.setItem('com.pttdigital.ess.announcement', JSON.stringify(data.Announcement));
                        //window.localStorage.setItem('com.pttdigital.ess.menu', JSON.stringify(data.Menu));
                        //toApplication();
                        validateEmployeeData(data);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });

        //$.post(URL, { '': JSON.stringify(x) })
        //.done(function (data) {
        //    alert(data);
        //});

    }

    function authenWebByPassAdminService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName) {
        var URL = SERVER + 'workflow/AuthenticateWithOutPassword';
        var oUserLogin = { Username: inputUsername, Password: inputPassword, CompanyCode: inputCompanyCode, CompanyName: inputCompanyName };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data.ErrorMessage === "") {
                        validateEmployeeData(data);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });
    }


}


function validateEmployeeData(oData) {


    var URL = SERVER + 'Workflow/ValidateData';

    var oRequestParameter = { InputParameter: {}, RequestDocument: {}, CurrentEmployee: {}, Creator: {}, Requestor: oData.EmployeeData };

    $.ajax({
        type: 'POST',
        data: oRequestParameter,
        url: URL,
        success:
            function (data, textStatus, XMLHttpRequest) {
                if (data !== null && data.length > 0) {
                    // error config
                    var errorMsg = '';
                    for (var i = 0; i < data.length; i++) {
                        errorMsg += '<b>' + data[i].TableName + '</b> : ' + data[i].ErrorMessage + '<br>';
                    }
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = errorMsg;
                    setLoader(false);
                }
                else {
                    // config complete

                    window.localStorage.setItem('com.pttdigital.dess.authorizeUser', JSON.stringify(oData.EmployeeData));
                    window.localStorage.setItem('com.pttdigital.dess.announcement', JSON.stringify(oData.Announcement));
                    window.localStorage.setItem('com.pttdigital.dess.menu', JSON.stringify(oData.Menu));
                    toApplication();

                }
            },
        error:
            function (XMLHttpRequest, textStatus, errorThrown) {
                document.getElementById('ErrorMessage').style.visibility = "visible";
                document.getElementById('ErrorMessage').innerHTML = errorThrown;
                setLoader(false);
            }
    });

}

function logout() {
    console.log('logout');
    window.localStorage.removeItem("com.pttdigital.dess.authorizeUser");
    window.localStorage.removeItem("com.pttdigital.dess.announcement");
    window.localStorage.removeItem('com.pttdigital.dess.menu');
    window.localStorage.removeItem('CurrentCompanyTheme');
    window.location = 'login.html' + window.localStorage.getItem('com.pttdigital.dess.pathcookie'); //getToken(CONFIG.PATHCOOKIES);//document.cookie;//'start.html';
    localStorage.clear();
}

function toApplication() {

    //Nattawat S. Add multi-Position
    var position = getToken("com.pttdigital.dess.authorizeUser").AllPosition;
    if (position.length > 1) {
        window.localStorage.setItem('com.pttdigital.dess.selectedPosition', false);
    } else {
        window.localStorage.setItem('com.pttdigital.dess.selectedPosition', true);
    }
    //end add
    window.location.replace("index.html");
}

function toLogin() {
    window.location = 'login.html';
}

function hasToken() {
    var jsonIsValid;
    try {
        var jsonResult = JSON.parse(window.localStorage.getItem('com.pttdigital.dess.authorizeUser'));
        jsonIsValid = true;
    } catch (e) {
        console.log('token error')
        window.localStorage.removeItem("com.pttdigital.dess.authorizeUser");
        jsonIsValid = false;
    };
    return window.localStorage.getItem('com.pttdigital.dess.authorizeUser') !== null && jsonIsValid;
}

function getEmployeeDataToken() {
    var objEmployeeData = null;
    try {
        objEmployeeData = JSON.parse(window.localStorage.getItem('com.pttdigital.dess.authorizeUser'));
    } catch (e) {
        console.log('token error')
        window.localStorage.removeItem("com.pttdigital.dess.authorizeUser");
    };
    return objEmployeeData;
}

function getToken(KEY) {
    var obj = null;
    try {
        obj = JSON.parse(window.localStorage.getItem(KEY));
    } catch (e) {
        console.log('token error')
        window.localStorage.removeItem(KEY);
    };
    return obj;
}

function setLoader(enable) {
    if (enable) {
        $('#CORE_txtInputAccount').prop('disabled', true);
        $('#CORE_txtInputPassword').prop('disabled', true);
        $('#login_button').prop('disabled', true);
        $('#inputCompany').prop('disabled', true);
        $('#login_button_icon').hide();
        $('#login_loader').show()
    } else {
        $('#CORE_txtInputAccount').prop('disabled', false);
        $('#CORE_txtInputPassword').prop('disabled', false);
        $('#login_button').prop('disabled', false);
        $('#inputCompany').prop('disabled', false);
        $('#login_loader').hide();
        $('#login_button_icon').show();
    }
}

function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}

var app = angular.module('LoginApp', []);
app.controller('CompanyController', function ($http, $scope) {
    $scope.event = {
        isEvent: false,
        hiddenVisit: true
    }
    $scope.CompanyBackground = "../images/dess-background.jpg";
    $scope.CompanyLogin = 'DEFAULT';

    // single signon
    function AuthenticatewithWindowsAuthen() {
        var currentLocation = window.location;
        var KEY_COMPANY_CODE = gup('cc', currentLocation);
        var isFoundCompany = false;

        if (KEY_COMPANY_CODE == null)
            KEY_COMPANY_CODE = '';

        for (var i = 0; i < $scope.CompanyList.length; i++) {

            var decrypted = CryptoJS.AES.decrypt(KEY_COMPANY_CODE, "pttdigitalsolution");
            var currentCompanyCode = decrypted.toString(CryptoJS.enc.Utf8);

            if (currentCompanyCode.includes($scope.CompanyList[i].CompanyCode)) {
                isFoundCompany = true;
                $scope.CompanyLogin = $scope.CompanyList[i].Name;
                break;
            }

            //if (KEY_COMPANY_CODE === $scope.CompanyList[i].CompanyCode) {
            //    isFoundCompany = true;
            //    $scope.CompanyLogin = $scope.CompanyList[i].Name;
            //    break;
            //}
        }
        if (!isFoundCompany) {
            $scope.CurrentCompanyCode = KEY_COMPANY_CODE;
            return;
        }


        console.log("AuthenticateWindows");

        var MoDule = 'workflow/';
        var Functional = 'AuthenticateWindows';
        //var URL = SERVER + MoDule + Functional + "?CompanyCode=" + KEY_COMPANY_CODE;
        var URL = SERVER + MoDule + Functional;
        var oRequestParameter = { InputParameter: { "CompanyCode": KEY_COMPANY_CODE } };
        console.log(URL);
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            var data = response.data;
            console.log('success -- authen');
            if (data.ErrorMessage === "") {
                validateEmployeeData(data);
            }
            else {
                document.getElementById('ErrorMessage').style.visibility = "visible";
                document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage + ' AD1 : ' + data.UserAuthenAD1 + ' AD2 : ' + data.UserAuthenAD2;
                setLoader(false);
            }
            $scope.CompanyList = response.data;

        }, function errorCallback(response) {
            // Error
            console.log('error AuthenticatewithWindowsAuthen.', response);
        });
    };


    // external sign on
    function AuthenticatewithExternalUser() {
        var currentLocation = window.location;
        var KEY_TOKEN = gup('ex', currentLocation);
        var KEY_COMPANY = gup('ex_cc', currentLocation);
        if (!KEY_TOKEN) return;
        if (!KEY_COMPANY) return;
        var MoDule = 'workflow/';
        var Functional = 'AuthenticateForExternalUser';
        var URL = SERVER + MoDule + Functional + "?EncryptKey=" + KEY_TOKEN + "|" + KEY_COMPANY;
        console.log(URL);
        $http({
            method: 'GET',
            url: URL,
            //data: oParams
        }).then(function successCallback(response) {
            var data = response.data;
            console.debug(data);
            console.log('success -- AuthenticateForExternalUser');
            if (data.ErrorMessage === "") {
                window.localStorage.setItem('com.pttdigital.dess.authorizeUser', JSON.stringify(data.EmployeeData));
                window.localStorage.setItem('com.pttdigital.dess.announcement', JSON.stringify(data.Announcement));
                window.localStorage.setItem('com.pttdigital.dess.menu', JSON.stringify(data.Menu));
                toApplication();
            }
            else {
                setLoader(false);
            }

        }, function errorCallback(response) {
            // Error
            console.log('error AuthenticateForExternalUser.', response);
        });
    }

    $scope.GetCompanyList = function () {
        console.log("GetCompanyList");
        var MoDule = 'Share/';
        var Functional = 'GetCompanyList';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {

            console.log('suAuthenticatewithWindowsAuthenccess');
            console.log(response.data);
            $scope.CompanyList = response.data;
            console.log("$scope.CompanyList", $scope.CompanyList);
            $scope.CompanyLogo = "";
            $scope.CompanyBackground = "images/dess-background.jpg";

            //$scope.CompanyBackground = "";
            $scope.CompanyIndexSel = '';
            $scope.CompanyCodeDefault = gup('cc', window.location); // = null ? "" : gup('cc', window.location);

            if ($scope.CompanyCodeDefault === null) {
                $scope.CompanyCodeDefault = "";
                $scope.CompanyLogo = "";
            }
            //keep params of URL that company
            window.localStorage.setItem('com.pttdigital.dess.pathcookie', '?cc=' + $scope.CompanyCodeDefault);

            window.localStorage.removeItem('CurrentCompanyTheme');
            window.localStorage.removeItem('CurrentCompanyLogo');
            for (i = 0; i < $scope.CompanyList.length; i++) {
                //var encrypted = CryptoJS.AES.encrypt("[companycode4digits][companyarias4digits]", "pttdigitalsolution"); -- ห้ามลบ
                var decrypted = CryptoJS.AES.decrypt($scope.CompanyCodeDefault.toString(), "pttdigitalsolution");
                var currentCompanyCode = decrypted.toString(CryptoJS.enc.Utf8);

                if (currentCompanyCode.includes($scope.CompanyList[i].CompanyCode)) {
                    $scope.CompanyBackground = $scope.CompanyList[i].Background == null ? "images/dess-background.jpg" : $scope.CompanyList[i].Background;
                    $scope.CompanyIndexSel = i;
                    $scope.CompanyLogo = $scope.CompanyList[i].Logo == null ? '' : $scope.CompanyList[i].Logo;
                    window.localStorage.setItem('CurrentCompanyLogo', $scope.CompanyLogo);
                    window.localStorage.setItem('CurrentCompanyTheme', 'css/theme/' + $scope.CompanyList[i].Theme);
                    break;
                } else {
                    window.localStorage.setItem('CurrentCompanyLogo','');
                    window.localStorage.setItem('CurrentCompanyTheme','css/theme/Default.css');
                }
            }

            AuthenticatewithWindowsAuthen();
            AuthenticatewithExternalUser();

        }, function errorCallback(response) {
            // Error
            console.log('error CompanyController.', response);
        });
    };
    $scope.GetCompanyList();

    $scope.GetEnvironment = function () {
        console.log("GenEnvironment");
        var MoDule = 'Share/';
        var Functional = 'GetEnvironment';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {

            console.log('GetEnvironment Success');
            console.log(response.data['DataBaseLocation']);
            $scope.Environment = response.data['DataBaseLocation'];
        }, function errorCallback(response) {
            // Error
            console.log('error GetEnvironment.', response);
        });
    };
    $scope.GetEnvironment();

    $scope.switchThemeByCompany = function (CompanyInfo) {
        $scope.CompanyLogo = '';
        // alert(comp);
        //set css theme
        if (CompanyInfo.Theme !== null && CompanyInfo.Theme !== '') {
            window.localStorage.setItem('CurrentCompanyLogo', CompanyInfo.Logo);
            window.localStorage.setItem('CurrentCompanyTheme', 'css/theme/' + CompanyInfo.Theme);
        } else {
            window.localStorage.setItem('CurrentCompanyLogo', '');
            window.localStorage.setItem('CurrentCompanyTheme', 'css/theme/Default.css');
        }
        //set background 
        if (CompanyInfo.Background !== null && CompanyInfo.Background !== '') {
            $scope.CompanyBackground = CompanyInfo.Background == null ? "images/dess-background.jpg":CompanyInfo.Background;
        } else {
            $scope.CompanyBackground = "images/dess-background.jpg";
        }

        $scope.CompanyLogo = CompanyInfo.Logo == null ? '' : CompanyInfo.Logo
    }


    function getLockInBlockSetting() {
        var MoDule = 'workflow/';
        var Functional = 'GetLoginBlockSetting';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
        }).then(function successCallback(response) {
            $scope.event.isEvent = (response.data.IsEvent.toLowerCase() !== 'false');
            adminKey = response.data.AdminKey;
            if ($scope.event.isEvent) {
                var currentLocation = window.location;
                var key = gup('key', currentLocation);
                if (key === adminKey) {
                    $scope.event.isEvent = false;
                }

            }
        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });
    }
    getLockInBlockSetting();

});