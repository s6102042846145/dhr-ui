﻿(function () {
    angular.module('DESS')
        .controller('BoxContentController', ['$rootScope', '$route', '$scope', '$http', '$routeParams', '$location', '$filter', '$q', 'CONFIG', '$window', '$mdDialog', '$interval', function ($rootScope, $route, $scope, $http, $routeParams, $location, $filter, $q, CONFIG, $window, $mdDialog, $interval) {
            var lstExpandedMenu = [];
            //#### FRAMEWORK FUNCTION ### START
            //Set text category of this content
            $scope.Textcategory = 'BOXCONTENT';
            $scope.content.Header = $scope.Text[$scope.Textcategory]['BOX' + $routeParams.id];
            $scope.CurrentUser = getToken(CONFIG.USER);
            $scope.itemTemplateSelected = [];
            $scope.SearchText = '';
            $scope.loader.enable = true;

            $scope.RequestDocumentByPosition = [];
            var objPosition = {
                PositionID: null,
                PositionName: null,
                RequestDocument: [],
                arrSplited: [], // set of request split by page chunk
                splitedAmount: 0,
                selectedColumn: 0, // current page select 
                currentPage: 0,
                isAllChecked: false,
            }
            var tempAllPosition = angular.copy(getToken(CONFIG.USER).AllPosition);
            var currentLanguage = angular.copy(getToken(CONFIG.USER).Language);
            for (i = 0; i < tempAllPosition.length; i++) {
                var temp = angular.copy(objPosition);
                temp.PositionID = tempAllPosition[i].ObjectID;
                temp.PositionName = currentLanguage == 'EN' ? tempAllPosition[i].TextEn : tempAllPosition[i].Text;
                $scope.RequestDocumentByPosition.push(temp);
            }

            $scope.requestIsInPosition = function (PositionID, ReceipientCode) {
                return PositionID == ReceipientCode;
            };


            //$scope.CurrentBox = $scope.menus.filter(getCurrentBox);
            //function getCurrentBox(x) {
            //    var GroupID;
            //    for (i = 0; i < x.length; i++) {
            //        if (x[i].text.includes($scope.content.Header)) {
            //            GroupID = x[i].groupID;
            //            break;
            //        }
            //    }
            //    return x.isBox && !x.isHeader && x.groupID == GroupID;
            //}
            //#### FRAMEWORK FUNCTION ### END

            /******************* listener start ********************/
            $scope.init = function () {
                if (localStorage.getItem("hasCodeRunBefore") === null) { }
                /** Your code here. **/
                $scope.CheckMassApprove();
                $scope.GetPositionApprove();
                $scope.GetMenuContent();
            }

            /******************* listener end ********************/

            //#### OTHERS FUNCTION ### START
            //Nattawat S. Add Org mapping to position

            $scope.OragnaizationData = [];
            $scope.OrganizationRelationData = [];

            $scope.GetOrgOfPosition = function (posID) {
                var returnName = '';
                if ($scope.OrganizationRelationData.length > 0 && $scope.OragnaizationData.length > 0) {
                    var PositionBelongToUnit = angular.copy($scope.OrganizationRelationData.filter(data => data.ObjectID.split(":")[0] == posID));
                    var PositionBelongToName = angular.copy($scope.OragnaizationData.filter(data => data.ObjectID == PositionBelongToUnit[0].NextObjectID.split(':')[0]));
                    returnName = $scope.CurrentUser.Language == 'EN' ? PositionBelongToName[0].TextEn : PositionBelongToName[0].Text;
                }
                return returnName;
            }

            $scope.GetRelation = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1001GetAllHistory';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var currentDate = new Date();
                    $scope.OrganizationRelationData = response.data.filter(data => data.ObjectType == 'S' && data.NextObjectType == 'O' && data.Relation == 'Belong To' && (currentDate >= new Date(data.BeginDate) && currentDate <= new Date(data.EndDate)));

                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            };

            $scope.GetOrganizationDetail = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetAllHistory';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var currentDate = new Date();
                    $scope.OragnaizationData = response.data.filter(data => data.ObjectType == 'O' && (currentDate >= new Date(data.BeginDate) && currentDate <= new Date(data.EndDate)));
                    $scope.GetRelation();
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            };
            $scope.GetOrganizationDetail();


            $scope.content.isShowHeader = false;
            $scope.formData = { searchKeyword: '' };
            $scope.showWarning = false;
            $scope.warningActionText = "";

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.RequestNo != null && item.RequestNo.toLowerCase().indexOf(keyword) != -1)
                    || (item.RequestorNo != null && item.RequestorNo.toLowerCase().indexOf(keyword) != -1)
                    || (item.RequestorName != null && item.RequestorName.toLowerCase().indexOf(keyword) != -1)
                    || (item.Summary != null && item.Summary.toLowerCase().indexOf(keyword) != -1)
                    || (item.CompanyShortName != null && item.CompanyShortName.toLowerCase().indexOf(keyword) != -1)
                    || (item.OrgName != null && item.OrgName.toLowerCase().indexOf(keyword) != -1)
                ) {
                    return true;
                }
                return false;
            };
            $scope.searchInBox = function () {
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    $scope.data = $scope.masterDocuments;
                    temp_documents = angular.copy($scope.data);
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.data = docs;
                    temp_documents = angular.copy($scope.data);
                }
                $scope.Action = $scope.Actions[0];
                filterArry('');
            };
            $scope.CheckRequestType = function () {

                return $scope.CanMassApprove;
            };
            $scope.getWarningText = function (text) {
                if (!angular.isUndefined(text)) {
                    return text.replace("{0}", $scope.warningActionText);
                }
                return '';
            };
            $scope.ActionFromDocument = function (ActionCode, RequestNo, positionID) {
                //  $scope.loader.enable = true;
                var actioncode = ActionCode;
                //var objRequestDocIsSel = angular.element(document.querySelectorAll("[requestdoc-isselect]:checked"));
                var objPositionSel = angular.copy($scope.RequestDocumentByPosition.filter(data => data.PositionID == positionID)[0]);
                var objRequestDocIsSel = objPositionSel.arrSplited[objPositionSel.selectedColumn].filter(data => data.selected == true);
                var oRequestNoSelected = "";

                switch (actioncode) {
                    case "ALL":
                        $scope.TextAction = $scope.Text['ACTION'].CONFIRM_MASS_ACTION;
                        $scope.Comment = "*****MASS APPROVE*****";
                        break;
                    case "REJECT":
                        $scope.TextAction = $scope.Text['ACTION'].REJECT;
                        $scope.Comment = "*****MASS REJECT*****";
                        break;
                    default:
                };

                if (objRequestDocIsSel.length > 0) {
                    //for (var i = 0; i < objRequestDocIsSel.length; i++) {
                    //    if (oRequestNoSelected == "") {
                    //        oRequestNoSelected = oRequestNoSelected.concat(objRequestDocIsSel[i].value);
                    //    }
                    //    else {
                    //        oRequestNoSelected = oRequestNoSelected.concat('|', objRequestDocIsSel[i].value);
                    //    }
                    //}

                    for (var i = 0; i < objRequestDocIsSel.length; i++) {
                        var formatParameter = objRequestDocIsSel[i].RequestNo + ',' + objRequestDocIsSel[i].KeyMaster + ',' + objRequestDocIsSel[i].CreatorCompanyCode + ',' + positionID;
                        if (oRequestNoSelected == "") {
                            oRequestNoSelected = formatParameter;
                        }
                        else {
                            oRequestNoSelected = oRequestNoSelected.concat('|', formatParameter);
                        }
                    }

                    var confirm = $mdDialog.confirm()
                        .title($scope.Text['SYSTEM'].WARNING)
                        .textContent($scope.Text['SYSTEM'].CONFIRM_ITEM_PROCESS.replace("{0}", $scope.TextAction).replace("{1}", objRequestDocIsSel.length))
                        .ok('OK')
                        .cancel('Cancel');

                    $mdDialog.show(confirm).then(function () {
                        if (oRequestNoSelected != '' && actioncode != '') {
                            $scope.loader.enable = true;
                            var oRequestParameter = { InputParameter: { "RequestNoSelected": oRequestNoSelected, "ActionCode": actioncode, "Comment": $scope.Comment }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                            var URL = CONFIG.SERVER + 'workflow/MassApprove/';

                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                $scope.MessageMassApprove = '';
                                $scope.loader.enable = false;
                                if (response.data.Sum_RequestNo_Completed != '' && response.data.Sum_RequestNo_Completed != null && response.data.Sum_RequestNo_Completed != '0') {
                                    $scope.MessageMassApprove += $scope.Text['ACTION'].MASS_APPROVE_SUCCESS.replace('{0}', response.data.Sum_RequestNo_Completed) + '<br/>';
                                }
                                if (response.data.Sum_RequestNo_UnCompleted != '' && response.data.Sum_RequestNo_UnCompleted != null && response.data.Sum_RequestNo_UnCompleted != '0') {
                                    $scope.MessageMassApprove += $scope.Text['ACTION'].MASS_APPROVE_NOT_SUCCESS.replace('{0}', response.data.Sum_RequestNo_UnCompleted) + '<br/>';
                                }

                                if (response.data.Sum_RequestNo_UnCompletedMassAction != '' && response.data.Sum_RequestNo_UnCompletedMassAction != null && response.data.Sum_RequestNo_UnCompletedMassAction != '0') {
                                    $scope.MessageMassApprove += $scope.Text['ACTION'].MASS_ACTION_DENY.replace('{0}', response.data.Sum_RequestNo_UnCompletedMassAction) + '<br/>';
                                }

                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text['SYSTEM'].WARNING)
                                        .htmlContent($scope.MessageMassApprove)
                                        .ok('OK')
                                ).then($route.reload());

                            }, function errorCallback(response) {
                                $scope.loader.enable = false;
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text['SYSTEM'].WARNING)
                                        .textContent(response)
                                        .ok('OK')
                                ).then($route.reload());
                            });
                        }
                        else {
                            $scope.loader.enable = false;
                            $scope.warningActionText = $scope.Action.ActionText;
                            $scope.showWarning = true;
                        }
                    }, function () {

                    });



                } else {
                    $scope.loader.enable = false;
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].PLESE_SELECT_MASSPROCESS.replace("{0}", $scope.TextAction))
                            .ok('OK')
                    );
                }
            };

            $scope.CanMassApprove = false;
            $scope.CheckMassApprove = function () {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizeMassApprove/' + $scope.ContentParam;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CanMassApprove = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.lstPositionApprove = [];
            $scope.IsPositionApprove = false;
            $scope.GetPositionApprove = function () {
                var oRequestParameter = { InputParameter: { "BoxID": $scope.ContentParam }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'workflow/GetPositionApprove';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.Table4.length > 0) {
                        $scope.User = getToken(CONFIG.USER);
                        $scope.UserPosition = $scope.User.PositionID;
                        $scope.lstPositionApprove.push({ isCurrentPostion: 'false', PositionID: '99999999', PositionName: $scope.Text['SYSTEM'].ALL });

                        for (i = 0; i < response.data.Table4.length > 0;) {
                            if (response.data.Table4[i].IsCuurentPosition)
                                $scope.UserCurrentPosition = response.data.Table4[i].PositionID;
                            $scope.lstPositionApprove.push({ isCurrentPostion: response.data.Table4[i].IsCuurentPosition, PositionID: response.data.Table4[i].PositionID, PositionName: response.data.Table4[i].PositionName });
                            i++;
                        }
                        $scope.PositionID = "99999999";
                        $scope.PositionName = $scope.Text['SYSTEM'].ALL;
                        $scope.auto_complete.ap_pdr_Position = $scope.PositionName;
                        $scope.temp_PositionName = $scope.PositionName;

                        var menuList = (getToken(CONFIG.MENU));
                        $scope.ShowFilter = true;
                        $scope.SelectedContent = $filter('filter')(menuList, { ContentParam: $scope.ContentParam, isHeader: false });
                        if ($scope.SelectedContent && $scope.SelectedContent[0].isBox == true) {
                            if ($scope.SelectedContent[0].groupID == 30) {
                                $scope.ShowFilter = false;
                            } else if ($scope.ContentParam == "104") {
                                //ประวัติการอนุมัติ ไม่ต้องแสดง mass approve
                                $scope.ShowFilter = false;
                            }
                            let Header = $filter('filter')(menuList, { groupID: $scope.SelectedContent[0].groupID, isHeader: true });
                            if (Header && Header.length > 0) {
                                $scope.HeaderBox = Header[0].text;
                            }
                            $scope.Headermenus = $filter('filter')(menuList, { groupID: $scope.SelectedContent[0].groupID, isHeader: false });
                            $scope.Headermenus.forEach(function (item) {
                                if (item.ContentParam == $scope.ContentParam) {
                                    item.Active = "active";
                                }
                            });
                        }
                    } else {
                        var menuList = (getToken(CONFIG.MENU));
                        $scope.ShowFilter = false;
                        $scope.SelectedContent = $filter('filter')(menuList, { ContentParam: $scope.ContentParam, isHeader: false });
                        if ($scope.SelectedContent && $scope.SelectedContent[0].isBox == true) {
                            let Header = $filter('filter')(menuList, { groupID: $scope.SelectedContent[0].groupID, isHeader: true });
                            if (Header && Header.length > 0) {
                                $scope.HeaderBox = Header[0].text;
                            }
                            $scope.Headermenus = $filter('filter')(menuList, { groupID: $scope.SelectedContent[0].groupID, isHeader: false });
                            $scope.Headermenus.forEach(function (item) {
                                if (item.ContentParam == $scope.ContentParam) {
                                    item.Active = "active";
                                }
                            });
                        }
                    }
                    $scope.loader.enable = false;
                    console.log('GetPositionApprove success');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('GetPositionApprove error');
                });
            };
            $scope.iPosition = 0;
            $scope.CountPostion = function (oPosition) {
                $scope.iPosition = 0;
                for (var i = 0; i < $scope.masterDocuments.length; i++) {
                    if ($scope.masterDocuments[i].ReceipientCode == oPosition)
                        $scope.iPosition++;
                }
                return $scope.iPosition;
            };
            $scope.GetMenuContent = function () {
                var URL = CONFIG.SERVER + 'workflow/GetMenuItem';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    var menuList = response.data;
                    // set group class
                    for (var i = 0; i < menuList.length; i++) {
                        if (lstExpandedMenu.indexOf(menuList[i].groupID) <= -1) {
                            menuList[i].isShow = false;
                        }
                        else {
                            menuList[i].isShow = true;
                        }
                    }

                    menuList = initCurrentUrlData(menuList);

                    // set initial active
                    var newLocation = $location.path();
                    var selectedGroupID = '-1';
                    var selectedIndex = -1;
                    for (var i = 0; i < menuList.length; i++) {
                        if ('/' + menuList[i].url == newLocation || (newLocation == '/frmViewContent/0' && i == 0)) {
                            selectedGroupID = menuList[i].groupID;
                            selectedIndex = i;
                            break;
                        }
                    }
                    if (selectedGroupID != '-1') {
                        for (var i = 0; i < menuList.length; i++) {
                            menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                            menuList[i].isSelected = false;
                        }
                        menuList[selectedIndex].isSelected = true;
                    }

                    $rootScope.menus = menuList;

                    window.localStorage.removeItem(CONFIG.MENU);
                    window.localStorage.setItem(CONFIG.MENU, JSON.stringify($rootScope.menus));
                    $scope.loader.enable = false;
                    console.log('GetMenuContent success');
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('GetMenuContent error');

                });
            }
            $scope.SumCountDocument = function () {
                return ID;
            }

            var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentListInBox';
            var oRequestParameter = { InputParameter: { "BoxID": $scope.ContentParam }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                /**Argif test filter start **/
                filterOption = '';
                $scope.searchOption = response.data.FLAGMASTER;
                $scope.masterDocuments = response.data.REQUESTDOCUMENT;
                $scope.documents = response.data.REQUESTDOCUMENT;
                temp_documents = angular.copy($scope.documents);
                $scope.data = angular.copy($scope.documents);

                //Nattawat S. Add case : separate document group by position
                var isPersonalBox = $scope.ContentParam.startsWith("3"); // 3** เป็นกล่องเอกสารส่วนตัว , 1** 4** 5** เป็นกล่องเอกสารเข้า เช่นพวกรออนุมัติ

                if (isPersonalBox) { // group by creator position
                    for (i = 0; i < $scope.RequestDocumentByPosition.length; i++) {
                        for (item = 0; item < $scope.data.length; item++) {
                            if ($scope.data[item].CreatorPosition == $scope.RequestDocumentByPosition[i].PositionID
                                || $scope.data[item].RequestorPosition == $scope.RequestDocumentByPosition[i].PositionID) {
                                $scope.RequestDocumentByPosition[i].RequestDocument.push(angular.copy($scope.data[item]));
                            }
                        }
                    }
                } else { // group by receipientPosition
                    for (i = 0; i < $scope.RequestDocumentByPosition.length; i++) {
                        for (item = 0; item < $scope.data.length; item++) {
                            if ($scope.data[item].ReceipientCode == $scope.RequestDocumentByPosition[i].PositionID
                                || $scope.data[item].DelegatedCode == $scope.RequestDocumentByPosition[i].PositionID) {
                                $scope.RequestDocumentByPosition[i].RequestDocument.push(angular.copy($scope.data[item]));
                            }
                        }
                    }
                }
                //End add
                console.log("REQ by Position", $scope.RequestDocumentByPosition);

                $scope.Actions = response.data.ACTION;
                if ($scope.Actions != null) {
                    if ($scope.Actions.length == 2) {
                        $scope.Actions.shift();
                        $scope.Action = $scope.Actions[0];
                    }
                    else if ($scope.Actions.length > 0) {
                        $scope.Action = $scope.Actions[0];
                    }
                }

                //ประเภทเอกสาร
                $scope.DocRequestType = [];

                var item = {
                    'RequestTypeText': $scope.Text['SYSTEM'].ALL,
                };
                $scope.DocRequestType.push(item);

                for (i = 0; i <= $scope.documents.length - 1; i++) {
                    $scope.isDocRequest = false;
                    if ($scope.DocRequestType.length == 0) {
                        var item = {
                            'RequestTypeText': $scope.documents[i].RequestTypeText,
                        };
                        $scope.DocRequestType.push(item);
                    }
                    else {
                        for (j = 0; j <= $scope.DocRequestType.length - 1; j++) {
                            if ($scope.documents[i].RequestTypeText == $scope.DocRequestType[j].RequestTypeText) {
                                $scope.isDocRequest = true;
                                break;
                            }
                        }
                        if ($scope.isDocRequest == false) {
                            var item = {
                                'RequestTypeText': $scope.documents[i].RequestTypeText,
                            };
                            $scope.DocRequestType.push(item);
                        }
                    }
                }

                $scope.itemTemplateSelected = $scope.DocRequestType[0];
                $scope.filterDocumentBySelect();

                $scope.loader.enable = false;
                console.log('GetRequestDocumentListInBox success');
            }, function errorCallback(response) {
                $scope.loader.enable = false;
                console.log('GetRequestDocumentListInBox error');
            });

            var temp_documents;
            $scope.filterDocumentByAction = function () {
                if ($scope.Action.ActionCode == 'ALL') {
                    $scope.documents = angular.copy(temp_documents);
                } else {
                    var temp_documents_filtered = [];
                    for (var i = 0; i < temp_documents.length; i++) {
                        if (temp_documents[i].ActionCode == $scope.Action.ActionCode) {
                            temp_documents_filtered.push(temp_documents[i]);
                        }
                    }
                    $scope.documents = angular.copy(temp_documents_filtered);
                }
            }

            $scope.openRequest = function (id, companyCode, keyMaster, isOwner, isDataOwner, requestType) {
                $location.path('/frmViewRequest/' + id + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner);
            };


            //Pageing
            $scope.chunk = 10;//item per page
            $scope.currentPage = 0;
            $scope.pageSize = 1;
            $scope.endClick = 0;
            $scope.pageLeft = function () {
                $scope.IsAllChecked = false;
                if ($scope.data.selectedColumn > 0) {
                    $scope.data.selectedColumn = $scope.currentPage - 1;
                    $scope.currentPage = $scope.currentPage - 1;
                    $scope.afterClickPlage = $scope.data.selectedColumn;

                }
            }
            $scope.pageRight = function () {
                $scope.IsAllChecked = false;
                if ($scope.data.selectedColumn < $scope.data.arrSplited.length - 1) {
                    $scope.data.selectedColumn = $scope.currentPage + 1;
                    $scope.currentPage = $scope.currentPage + 1;
                    $scope.afterClickPlage = $scope.data.selectedColumn;
                }
            }

            //Nattawat S, 19-01-2021

            $scope.pageChange = function (posID, pointer) {
                //pointer is , if click left this turn to -1 otherwise +1
                var selectedPosition = $scope.RequestDocumentByPosition.filter(data => data.PositionID == posID)[0];
                for (col = 0; col < selectedPosition.arrSplited.length; col++) {
                    for (index = 0; index < selectedPosition.arrSplited[col].length; index++) {
                        selectedPosition.arrSplited[col][index].selected = false;
                    }
                }
                selectedPosition.isAllChecked = false;
                selectedPosition.selectedColumn = selectedPosition.currentPage + pointer;
                selectedPosition.currentPage = selectedPosition.currentPage + pointer;
                selectedPosition.afterClickPlage = selectedPosition.selectedColumn;
            }

            $scope.afterClickPlage = 0;
            //$scope.selectPage = function (page) {
            //    $scope.IsAllChecked = false;
            //    $scope.currentPage = page - 1;
            //    $scope.afterClickPlage = page;
            //    $scope.data.selectedColumn = page - 1;
            //    for (i = 0; i <= $scope.data.length - 1; i++) {
            //        $scope.data[i].selected = false;
            //    }
            //}

            $scope.selectPage = function (positionID, page) {
                var selectedPosition = $scope.RequestDocumentByPosition.filter(data => data.PositionID == positionID)[0];
                if (selectedPosition.selectedColumn != page-1 ) {
                    selectedPosition.isAllChecked = false;
                    selectedPosition.currentPage = page - 1;
                    selectedPosition.afterClickPlage = page;
                    selectedPosition.selectedColumn = page - 1;
                    for (col = 0; col < selectedPosition.arrSplited.length; col++) {
                        for (index = 0; index < selectedPosition.arrSplited[col].length; index++) {
                            selectedPosition.arrSplited[col][index].selected = false;
                        }
                    }
                }

            }

            $scope.filteredItem = [];
            var filterOption = "";

            $scope.filterDocumentBySelect = function () {
                $scope.SearchText = '';
                for (pos = 0; pos < $scope.RequestDocumentByPosition.length; pos++) {
                    $scope.RequestDocumentByPosition[pos].selectedColumn = 0;
                    $scope.RequestDocumentByPosition[pos].currentPage = 0;
                    $scope.RequestDocumentByPosition[pos].isAllChecked = false;
                }
                //$scope.data = angular.copy(temp_documents);
                //$scope.data.selectedColumn = 0;
                filterArry($scope.itemTemplateSelected.RequestTypeText);
                $scope.selectDocument = 0;
            }
            $scope.filterDocumentText = function () {

                $scope.selectDocument = 0;
                if ($scope.itemTemplateSelected.RequestTypeText == $scope.Text['SYSTEM'].ALL) {
                    $scope.data = angular.copy(temp_documents);
                    filterArry($scope.SearchText);
                }
                else {
                    if ($scope.SearchText == '')
                        $scope.filterDocumentBySelect();
                    else
                        filterArry($scope.SearchText);
                }
            }
            function filterArry(filter) {

                if (filter == $scope.Text['SYSTEM'].ALL) {
                    //$scope.filteredItem = angular.copy($scope.data);
                    ////$scope.filteredItem = $scope.filteredItem

                    //var expectedPage = 0;
                    //var i, j, temparray;
                    //$scope.data.arrSplited = [];
                    //for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                    //    temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                    //    $scope.data.arrSplited.push(temparray);
                    //    expectedPage++
                    //}
                    //$scope.data.selectedColumn = 0;
                    //$scope.data.summary = $scope.data.length;

                    //Nattawat S. 18-01-21
                    for (pos = 0; pos < $scope.RequestDocumentByPosition.length; pos++) {
                        $scope.filteredItem = angular.copy($scope.RequestDocumentByPosition[pos].RequestDocument);
                        //$scope.filteredItem = $scope.filteredItem
                        $scope.RequestDocumentByPosition[pos].selectedColumn = 0;
                        $scope.RequestDocumentByPosition[pos].splitedAmount = $scope.filteredItem.length;

                        var expectedPage = 0;
                        var i, j, temparray;
                        $scope.RequestDocumentByPosition[pos].arrSplited = [];
                        for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                            temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                            temparray.forEach(element => element.selected = false);
                            $scope.RequestDocumentByPosition[pos].arrSplited.push(temparray);
                            expectedPage++
                        }
                        $scope.RequestDocumentByPosition[pos].selectedColumn = 0;
                        //$scope.RequestDocumentByPosition[pos].summary = $scope.data.length;
                    }
                }
                else {
                    //$scope.filteredItem = $filter('filter')($scope.data, filter);
                    //$scope.filteredItem = $filter('filter')($scope.filteredItem, $scope.advacneOptionFilter);
                    //$scope.data = $scope.filteredItem;

                    //var expectedPage = 0;
                    //var i, j, temparray;
                    //$scope.data.arrSplited = [];
                    //for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                    //    temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                    //    $scope.data.arrSplited.push(temparray);
                    //    expectedPage++
                    //}
                    //$scope.data.selectedColumn = 0;

                    //Nattawat S. 18-01-21
                    for (pos = 0; pos < $scope.RequestDocumentByPosition.length; pos++) {
                        $scope.filteredItem = angular.copy($filter('filter')($scope.RequestDocumentByPosition[pos].RequestDocument, filter));
                        //$scope.data = $scope.filteredItem;
                        $scope.RequestDocumentByPosition[pos].selectedColumn = 0;
                        $scope.RequestDocumentByPosition[pos].splitedAmount = $scope.filteredItem.length;

                        var expectedPage = 0;
                        var i, j, temparray;
                        $scope.RequestDocumentByPosition[pos].arrSplited = [];
                        for (i = 0, j = $scope.filteredItem.length; i < j; i += $scope.chunk) {
                            temparray = $scope.filteredItem.slice(i, i + $scope.chunk);
                            temparray.forEach(element => element.selected = false);
                            $scope.RequestDocumentByPosition[pos].arrSplited.push(temparray);
                            expectedPage++
                        }
                    }

                }
                console.log('Filtered Doc. : ', $scope.RequestDocumentByPosition);
            }

            $scope.goBackHomePage = function () {
                $location.path('/frmViewContent/0');
            };

            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Title')
                        .textContent(ev)
                        .ok('ตกลง')
                );
            };

            $scope.showDialog = function (url) {
                var parentEl = angular.element(document.body);
                $mdDialog.show({
                    parent: parentEl,
                    //targetEvent: $event,
                    template:
                        '<md-dialog aria-label="List dialog">' +
                        '  <md-dialog-content>' +
                        //'    <md-list>' +
                        //'      <md-list-item ng-repeat="item in items">' +
                        //'       <p>Number {{item}}</p>' +
                        // '      ' +
                        '<iframe src="' + url + '" width="100%" height="700px"></iframe>' +
                        // '    </md-list-item></md-list>' +
                        '  </md-dialog-content>' +
                        '  <md-dialog-actions>' +
                        '    <md-button ng-click="closeDialog()" class="md-primary">' +
                        '      Close Dialog' +
                        '    </md-button>' +
                        '  </md-dialog-actions>' +
                        '</md-dialog>',
                    locals: {
                        items: $scope.items
                    },
                    controller: $scope.DialogController
                });
            }

            $scope.DialogController = function ($scope, $mdDialog, items) {
                $scope.items = items;
                $scope.closeDialog = function () {
                    $mdDialog.hide();
                }
            }

            $scope.getBoxText = function () {
                var text = '';
                var currentPath = $location.path();
                var menuItems = getToken('com.pttdigital.ess.menu')
                for (var i = 0; i < menuItems.length; i++) {
                    if (('/' + menuItems[i].url) == currentPath) {
                        text = menuItems[i].text;
                        break;
                    }
                }
                return text;
            };



            //Reload all request and no in box list
            $scope.onReload = function () {


                console.warn('reload');
                var deferred = $q.defer();
                setTimeout(function () {
                    deferred.resolve(true);
                }, 1000);
                return deferred.promise;
            };

            $scope.Reload = function () {

                // Reload Menu
                var args = {};
                $rootScope.$broadcast('onReloadMenu', args);
                $route.reload();
            };


            //dropdownlist position
            $scope.PositionID = '';
            $scope.PositionName = '';
            function createFilterForPosition(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.PositionID + " : " + x.PositionName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Position;
            $scope.init_Position = function () { }
            $scope.querySearchPosition = function (query) {
                if (!$scope.lstPositionApprove) return;
                var results = angular.copy(query ? $scope.lstPositionApprove.filter(createFilterForPosition(query)) : $scope.lstPositionApprove), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemPositionChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.auto_complete.ap_pdr_Position = item.PositionName;
                    $scope.PositionID = item.PositionID;
                    $scope.PositionName = item.PositionName;

                    $scope.temp_PositionName = item.PositionName;
                    tempResult_Position = angular.copy(item);
                    $scope.filterPositionSelected(item);
                }
                else {
                    $scope.auto_complete.ap_pdr_Position = '';
                }
            };
            $scope.auto_complete = {
                ap_pdr_Position: ''
            }
            $scope.selected_position = '';
            $scope.tryToSelect_Position = function (text) {
                $scope.auto_complete.ap_pdr_Position = '';
                $scope.selected_position = null;
                $scope.PositionID = '';
                $scope.PositionName = '';
            }
            $scope.checkText_Position = function (text) {
                $scope.auto_complete.ap_pdr_Position = $scope.temp_PositionName;
            }

            //function createFilterForRequestDocument(query) {
            //    var lowercaseQuery = angular.lowercase(query);
            //    return function filterFn(x) {
            //        if (!x) return false;
            //        var source = angular.lowercase(x.ReceipientCode);
            //        return (source.indexOf(lowercaseQuery) >= 0);
            //    };
            //}
            $scope.filterPositionSelected = function (item) {
                if ($scope.PositionID == "99999999") {
                    if (!$scope.documents) return;
                    $scope.data = angular.copy($scope.documents);
                    filterArry('');
                }
                else {
                    if (!$scope.documents) return;
                    var myObjects = angular.copy($filter('filter')($scope.documents, { ReceipientCode: $scope.PositionID }));
                    $scope.data = angular.copy(myObjects);
                    filterArry('');
                }
            }
            $scope.FilterPositionName = function (item) {
                var myObjects = angular.copy($filter('filter')($scope.lstPositionApprove, { PositionID: item }));
                $scope.fillPositionName = myObjects[0].PositionName;
            }
            //dropdownlist position

            //** Argif Smart Search filter **//
            var filterOption = "";
            $scope.searchOption = [

            ];
            $scope.setOptionFilter = function () {
                if ($scope.searchOption.length == 0) return;
                filterOption = "";
                for (var i = 0; i < $scope.searchOption.length; i++) {
                    if ($scope.searchOption[i].IsCheck == true) {
                        filterOption += $scope.searchOption[i].FlagID + "/";
                    }
                }
                filterArry('');
            }


            $scope.clickMenu = function (sender, url) {
                closeMenu(sender);
                // onReloadMenuFunction(true);
                // Reload Menu
                // var args = {};
                // $rootScope.$broadcast('onReloadMenu', args);
                //$scope.GetPositionApprove();
                // Redirect
                $location.path(url);
            };
            var onReloadMenuFunction = function (isReloadAfterFinish) {
                var URL = CONFIG.SERVER + 'workflow/GetMenuItem';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    var menuList = response.data;
                    // set group class
                    for (var i = 0; i < menuList.length; i++) {
                        if (lstExpandedMenu.indexOf(menuList[i].groupID) <= -1) {
                            menuList[i].isShow = false;
                        }
                        else {
                            menuList[i].isShow = true;
                        }
                    }

                    menuList = initCurrentUrlData(menuList);

                    // set initial active
                    var newLocation = $location.path();
                    var selectedGroupID = '-1';
                    var selectedIndex = -1;
                    for (var i = 0; i < menuList.length; i++) {
                        if ('/' + menuList[i].url == newLocation || (newLocation == '/frmViewContent/0' && i == 0)) {
                            selectedGroupID = menuList[i].groupID;
                            selectedIndex = i;
                            break;
                        }
                    }
                    if (selectedGroupID != '-1') {
                        for (var i = 0; i < menuList.length; i++) {
                            menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                            menuList[i].isSelected = false;
                        }
                        menuList[selectedIndex].isSelected = true;
                    }

                    $rootScope.menus = menuList;

                    window.localStorage.removeItem(CONFIG.MENU);
                    window.localStorage.setItem(CONFIG.MENU, JSON.stringify($rootScope.menus));

                    //if (typeof (isReloadAfterFinish) != 'undefined' && isReloadAfterFinish) {

                    //    $route.reload();
                    //}
                    //  $scope.CheckMassApprove();
                    //  $scope.GetPositionApprove();
                    // localStorage.setItem("hasCodeRunBefore", true);
                }, function errorCallback(response) {
                    // Error
                    //  localStorage.removeItem("hasCodeRunBefore");
                    console.log('error reload menu.', response);

                });
            };
            var initCurrentUrlData = function (menuList) {
                // set initial active
                var newLocation = $location.path();
                var isFirstMenu = newLocation === '/frmViewContent/0';
                var isSelected = false;
                for (var m = 0; m < menuList.length; m++) {

                    if (isFirstMenu === true) {
                        menuList[0].isSelected = true;
                    } else {
                        menuList[m].isSelected = false;
                    }

                    if (compareUrl(newLocation, menuList[m].url)) {
                        menuList[m].isSelected = true;
                    } else if (menuList[m].menues && menuList[m].menues.length > 0) {
                        for (var s = 0; s < menuList[m].menues.length; s++) {
                            menuList[m].menues[s].isSelected = false;
                            if (compareUrl(newLocation, menuList[m].menues[s].url)) {
                                menuList[m].isSelected = true;
                                menuList[m].menues[s].isSelected = true;
                                menuList[m].isShow = true;
                                break;
                            }
                        }
                    } else {
                        menuList[m].isSelected = false;
                    }
                }

                var selectHMenu = menuList.filter(function (m) { return m.isShow === true; });

                if (selectHMenu && selectHMenu.length > 0) {
                    menuList.forEach(function (m) {
                        if (m.groupID === selectHMenu[0].groupID && m.menues && m.menues.length > 0) {
                            m.menues.forEach(function (s) {
                                s.isShow = true;
                            });
                        }
                    });
                }

                return menuList;
            };

            var compareUrl = function (currentUrl, menuUrl) {
                return currentUrl === '/' + menuUrl;
            };
            $scope.selectDocument;


            $scope.SelectedDataApprove = function (RequestNo) {
                if ($scope.data.length > 0) {
                    for (i = 0; i <= $scope.data.length - 1; i++) {
                        if ($scope.data[i].RequestNo == RequestNo) {
                            if ($scope.data[i].selected == true) {
                                $scope.data[i].selected = false;
                                $scope.selectDocument -= 1;
                            }
                            else {
                                $scope.data[i].selected = true;
                                $scope.selectDocument += 1;
                            }
                            break;
                        }
                    }
                }
            }

            $scope.CheckUncheckHeader = function (obj, positionID) {
                var currentPosition = $scope.RequestDocumentByPosition.filter(data => data.PositionID == positionID)[0];
                currentPosition.isAllChecked = true;
                for (var i = 0; i < obj.length; i++) {
                    if (!obj[i].selected) {
                        currentPosition.isAllChecked = false;
                        break;
                    }
                };
            };

            $scope.CheckUncheckAll = function (isCheck, obj, positionID) {
                if (isCheck) {
                    $scope.selectAll(obj);
                } else {
                    $scope.clearAll(obj);
                }
            };

            $scope.selectAll = function (obj) {
                $scope.selectDocument = 0;
                for (i = 0; i <= obj.length - 1; i++) {
                    obj[i].selected = true;
                    $scope.selectDocument += 1;
                }
            }

            $scope.clearAll = function (obj) {
                for (i = 0; i <= obj.length - 1; i++) {
                    obj[i].selected = false;
                    $scope.selectDocument = 0;
                }
            }
        }]);

    //#### OTHERS FUNCTION ### END

})();
