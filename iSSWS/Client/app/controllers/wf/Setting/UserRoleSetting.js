﻿(function () {
    angular.module('DESS')
        .controller('UserRoleSettingController', ['$rootScope', '$scope', '$http', '$routeParams', '$location', '$mdDialog', 'CONFIG', function ($rootScope, $scope, $http, $routeParams, $location, $mdDialog, CONFIG) {
        //.controller('UserRoleSettingController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
            $scope.Textcategory = 'USER_ROLE_SETTING';

            $scope.CreateNew = function () {
                $location.path('/frmViewContent/406');
            };

            $scope.Edit = function (EmployeeID, CompanyCode) {
                $location.path('/frmViewContent/406/' + EmployeeID + '&' + CompanyCode);
            };
            $scope.content.isShowHeader = false;

            $scope.init = function () {
                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'workflow/GetAllUserRole';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.documents = response.data;
                    $scope.masterDocuments = angular.copy($scope.documents);
                }, function errorCallback(response) {

                });
            }

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.Name && item.Name.toLowerCase().indexOf(keyword) != -1)
                    || (item.EmployeeID.toLowerCase().indexOf(keyword) != -1)
                    || (item.FullNameEN.toLowerCase().indexOf(keyword) != -1)
                    || (item.Description.toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };

            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.documents = angular.copy($scope.masterDocuments);
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                        $scope.documents = docs;
                    }
                }
            };

            // master data callig
            function service_res_com() {
                var oRequestParameter = { CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    service_res_emp();
                }, function errorCallback(response) {

                });

            }
            function service_res_emp() {
                var oRequestParameter = { CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                }, function errorCallback(response) {
                });

            }

            $scope.get_res_employee = function () {
                getResEmployee();
            }

            function getResEmployee() {
                var oRequestParameter = { CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                }, function errorCallback(response) {
                });
            }


            $scope.save_edit_res_emp = function () {
                $scope.mode = 0;
                var oRequestParameter = { CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                }, function errorCallback(response) {
                });
            }
            $scope.save_add_res_emp = function () {
                $scope.mode = 0;
                var oRequestParameter = { CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                }, function errorCallback(response) {
                });
            }
            $scope.remove_res_emp_s = function () {
                var oRequestParameter = { CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                }, function errorCallback(response) {
                });
            }




            // employee
            $scope.remove_res_emp = function (index, item) {
                var confirm = $mdDialog.confirm()
               .title('Do you sure to remove this employee?')
               .ok('DELETE')
               .cancel('CANCEL');

                $mdDialog.show(confirm).then(function () {
                    $scope.status = 'You decided to get rid of your debt.';
                    remove_res_emp_s();
                }, function () {
                    $scope.status = 'You decided to keep your debt.';
                });
            }
            $scope.edit_res_emp = function (index, item) {
                $scope.mode = 1;
            }
            $scope.add_res_emp = function () {
                $scope.mode = 2;
                $scope.res_emp_select_company = $scope.res_com[0].companycode;
            }

            // role responsding
            $scope.remove_role_res = function (index, item) {
                var confirm = $mdDialog.confirm()
                .title('Do you sure to remove this role?')
                .ok('DELETE')
                .cancel('CANCEL');

                $mdDialog.show(confirm).then(function () {
                    $scope.status = 'You decided to get rid of your debt.';
                }, function () {
                    $scope.status = 'You decided to keep your debt.';
                });
            }
            $scope.add_role_res = function () {

            }


        }]);
})();