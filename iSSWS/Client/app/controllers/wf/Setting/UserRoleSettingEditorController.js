﻿(function () {
    angular.module('DESS')
        .controller('UserRoleSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.Textcategory = 'USER_ROLE_SETTING_EDIT';
            $scope.content.Header = $scope.Text['APPLICATION'].USER_ROLE_SETTNG;
            $scope.Textcategory2 = 'SYSTEM';

            $scope.currentCompany = angular.copy(getToken(CONFIG.USER).CompanyCode);
            $scope.currentResponseType = 'O'; // default 'O' in this phase. more response type in future

            $scope.tempResultCompany;
            $scope.tempResultOrganization;
            $scope.tempResultEmployee;

            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };

            $scope.loader.enable = true;
            //temp new row
            $scope.data = {
                EmployeeID: '',
                EmployeeName: '',
                ResponseType: '',
                ResponseDesc: '',
                ResponseCode: '', //Org unit
                ResponseCodeDesc: '', // Org name
                CompanyCode: $scope.currentCompany,
                includeSub: false,
                Roles: [],
            };

            $scope.mUserRole = [];
            $scope.UserRoleResponse = [];
            $scope.organizationDataList = [];

            $scope.init = function () {
                //$scope.GetCompany();
                //$scope.GetRole();
                //$scope.GetResponseType();
                //$scope.GetAllResponseCodeByResponseType();
                //$scope.GetAllUserRole();
            }


            $scope.GetCompany = function () {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'Share/GetCompanyList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.CompanyList = response.data;
                    //Nattawat S. filter others company off
                    if ($scope.CompanyList.length > 0)
                        for (i = 0; i < $scope.CompanyList.length; i++) {
                            var comTxt = getToken(CONFIG.USER).Language == 'EN' ? $scope.CompanyList[i].FullNameEN : $scope.CompanyList[i].FullNameTH;
                            $scope.CompanyList[i].CompanyDetail = angular.copy($scope.CompanyList[i].Name) + ' : ' + comTxt;//$scope.CompanyList[i].CompanyCode + " : " + 
                        }
                    $scope.CompanyList = $scope.CompanyList.filter(item => item.CompanyCode == $scope.currentCompany);
                    $scope.tempResultCompany = angular.copy($scope.CompanyList[0]);
                    $scope.GetEmployees(angular.copy($scope.tempResultCompany.CompanyCode));

                }, function errorCallback(response) {
                    // Error
                    console.log('error GetCompany.', response);
                });
            }
            $scope.GetCompany();

            // Nattawat S. Add 2021-02-18
            $scope.GetAllOrganization = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetAllResponseCodeByResponseType';
                var oRequestParameter = { InputParameter: { "ResponseType": 'O' }, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.organizationDataList = [];
                    if (response.data.length > 0) {
                        for (item = 0; item < response.data.length; item++) {
                            var orgData = { OrgUnit: angular.copy(response.data[item].ResponseCode), OrgName: angular.copy(response.data[item].Name), OrgDetail: angular.copy(response.data[item].ResponseCode) + ' : ' + angular.copy(response.data[item].Name) }
                            $scope.organizationDataList.push(orgData);
                        }
                    }

                    if ($scope.organizationDataList.length > 0)
                        $scope.tempResultOrganization = angular.copy($scope.organizationDataList[0]);
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            }
            $scope.GetAllOrganization();

            var previousSelectedCompany = ''
            $scope.GetEmployees = function (CompanyCode) {
                var callService = false;
                if (previousSelectedCompany == '' || previousSelectedCompany != CompanyCode) {
                    previousSelectedCompany = angular.copy(CompanyCode);
                    $scope.employeeDataList = null;
                    var URL = CONFIG.SERVER + 'HRPA/GetAllEmployeeNameForUserRole';
                    var oRequestParameter = { InputParameter: { "CompanyCode": CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.employeeDataList = response.data;
                    }, function errorCallback(response) {
                        console.log('error GetEmployees list', response);
                    });
                }
            }

            $scope.GetEmployeeName = function (EmployeeID) {
                $scope.employeeDataList = null;
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE0001Get';
                var oRequestParameter = { InputParameter: { "EmployeeID": EmployeeID }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if ($scope.oUserRole.EmployeeID) {
                        $scope.GetRole();
                        $scope.oUserRole.EmployeeName = response.data.Name;
                        $scope.oUserRole.searchEmployeeText = $scope.oUserRole.EmployeeID + ' : ' + response.data.Name;
                    }
                }, function errorCallback(response) {
                    console.log('error GetEmployees list', response);
                });
            }

            $scope.GetRole = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAllRole';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.roleList = response.data;
                }, function errorCallback(response) {
                    console.log('error GetRole list', response);
                });
            }
            $scope.GetRole();

            $scope.GetResponseType = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAllUserRoleResponseType';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.userRoleResponseTypeList = response.data;
                    $scope.GetAllUserRole();
                    if ($scope.oUserRole.EmployeeID) {
                        $scope.GetUserRoleResponse($scope.oUserRole.EmployeeID, $scope.oUserRole.CompanyCode);
                    }
                    else {
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetResponseType list', response);
                });
            }
            $scope.GetResponseType();

            $scope.GetAllResponseCodeByResponseType = function (ResponseType) {
                var URL = CONFIG.SERVER + 'HRPA/GetAllResponseCodeByResponseType';
                var oRequestParameter = { InputParameter: { "ResponseType": ResponseType }, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mResponseCode = response.data;
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            }

            $scope.GetUserRoleResponse = function (EmployeeID, CompanyCode) {
                var URL = CONFIG.SERVER + 'workflow/GetUserRoleResponse';
                var oRequestParameter = { InputParameter: { "EmployeeID": EmployeeID, "CompanyCode": CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.oUserRole.UserRoleResponse.push(angular.copy($scope.UserRoleResponse));
                        $scope.checkTextResponseCompany(response.data[i].ResponseCompanyCode, i);
                        $scope.checkTextRole(response.data[i].UserRole, i);
                        $scope.checkTextResponseType(response.data[i].ResponseType, i);
                        $scope.oUserRole.UserRoleResponse[i].ResponseCode = response.data[i].ResponseCode;
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            }

            $scope.GetAllUserRole = function () {
                $scope.loader.enable = true;
                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'workflow/GetAlluserRoleEditor';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mUserRole = response.data;
                    //Nattawat S. Filter others company off
                    $scope.mUserRole = response.data.filter(data => data.CompanyCode == $scope.currentCompany);
                    $scope.mUserRoleAll = response.data.filter(data => data.CompanyCode != $scope.currentCompany);

                    for (i = 0; i <= $scope.mUserRole.length - 1; i++) {
                        if (i == 0)
                            $scope.mUserRole[i].Show = 'show';
                        else
                            $scope.mUserRole[i].Show = '';

                        var ComDesc = $filter('filter')($scope.CompanyList, { CompanyCode: $scope.mUserRole[i].CompanyCode });
                        $scope.mUserRole[i].CompanyName = ComDesc[0].Name;
                        for (j = 0; j <= $scope.mUserRole[i].Content.length - 1; j++) {
                            $scope.mUserRole[i].Content[j].ResponseCompanyDesc = ComDesc[0].Name;
                        }
                    }

                    GetRoleSetting();

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.Showcollapse = function (user) {
                for (i = 0; i <= $scope.mUserRole.length - 1; i++) {
                    $scope.mUserRole[i].Show = '';
                }
                user.Show = 'show';
            }

            $scope.GetAllResponseCodeByResponseTypeDesc = function (ResponseType, objResponse) {
                var URL = CONFIG.SERVER + 'HRPA/GetAllResponseCodeByResponseType';
                var oRequestParameter = { InputParameter: { "ResponseType": ResponseType }, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mResponseCode = response.data;


                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            }


            //$scope.addRow = function () {
            //    for (i = 0; i <= $scope.mUserRole.length - 1; i++) {
            //        $scope.mUserRole[i].Show = '';
            //    }
            //    var item = {
            //        "Show":'show',
            //        "EmployeeID": $scope.oUserRole.EmployeeID,
            //        "EmployeeName": $scope.oUserRole.EmployeeName,
            //        "CompanyCode": $scope.oUserRole.CompanyCode,
            //        "CompanyName": $scope.oUserRole.searchCompanyText,
            //        "UserResponse" : [],
            //    }
            //    $scope.mUserRole.push(item);

            //    $scope.data.EmployeeID = $scope.oUserRole.EmployeeID;
            //    $scope.data.EmployeeName = $scope.oUserRole.EmployeeName;

            //    $scope.data.ResponseCompanyCode = $scope.oUserRole.CompanyCode;
            //    $scope.data.ResponseCompanyDesc = $scope.oUserRole.searchCompanyText;

            //    $scope.oUserRole.EmployeeID = "";
            //    $scope.oUserRole.EmployeeName = "";
            //    $scope.oUserRole.searchEmployeeText = "";
            //    $scope.oUserRole.searchOrganizationText = "";
            //};

            $scope.addRow = function () {

                if (!($scope.data.EmployeeID == null || $scope.data.EmployeeID == '') && $scope.data.Roles.length > 0) {
                    var item = {
                        CompanyCode: $scope.data.CompanyCode,
                        EmployeeID: $scope.data.EmployeeID,
                        EmployeeName: $scope.data.EmployeeName,
                        includeSub: $scope.data.includeSub,
                        ResponseType: $scope.data.ResponseType,
                        ResponseDesc: $scope.data.ResponseDesc,
                        ResponseCode: $scope.data.ResponseCode,
                        ResponseCodeDesc: $scope.data.ResponseCodeDesc,
                        ResponseCompanyCode: $scope.tempResultCompany.CompanyCode,
                        Roles: $scope.data.Roles,
                    }

                    $scope.UserRoleDisplay.push(item);

                    $scope.oUserRole.EmployeeID = "";
                    $scope.oUserRole.EmployeeName = "";
                    $scope.oUserRole.searchEmployeeText = "";
                    //renew parameter
                    $scope.data = {
                        EmployeeID: '',
                        EmployeeName: '',
                        ResponseType: 'O',
                        ResponseDesc: '',
                        ResponseCode: $scope.tempResultOrganization.OrgUnit, //Org unit
                        ResponseCodeDesc: $scope.tempResultOrganization.OrgName, // Org name
                        CompanyCode: $scope.currentCompany,
                        ResponseCompanyCode: $scope.tempResultCompany.CompanyCode,
                        includeSub: false,
                        Roles: [],
                    };


                }
            };

            $scope.removeRow = function (index, Employee) {
                //Employee.UserResponse.splice(index, 1);
                Employee.Content.splice(index, 1);
            };
            $scope.AddNewRole = function (object) {
                var item = {
                    EmployeeID: $scope.data.EmployeeID,
                    EmployeeName: $scope.data.EmployeeName,
                    CompanyCode: $scope.data.CompanyCode,
                    CompanyName: $scope.data.CompanyName,
                    UserRole: $scope.data.UserRole,
                    RoleDesc: $scope.data.RoleDesc,
                    ResponseType: $scope.data.ResponseType,
                    ResponseDesc: $scope.data.ResponseTypeDesc,
                    ResponseCode: $scope.data.ResponseCode,
                    ResponseCodeDesc: $scope.data.ResponseCodeName,
                    ResponseCompanyCode: $scope.tempResultCompany.CompanyCode,
                    ResponseCompanyDesc: $scope.tempResultCompany.Name,
                    includeSub: $scope.data.includeSub,
                }
                //object.Content.push(item);
                $scope.UserRoleDisplay.push(item);

                $scope.data.UserRole = '';
                $scope.data.RoleDesc = '';
                $scope.data.ResponseType = '';
                $scope.data.ResponseTypeDesc = '';
                $scope.data.ResponseCode = '';
                $scope.data.ResponseCodeDesc = '';
                $scope.data.ResponseCodeName = '';
                $scope.data.includeSub = '';

            }
            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.oUserRole.EmployeeID) valid = false;
                else if (!$scope.oUserRole.CompanyCode) valid = false;
                else {
                    for (var i = 0; i < $scope.oUserRole.UserRoleResponse.length; i++) {
                        if (!$scope.oUserRole.UserRoleResponse[i]
                            || !$scope.oUserRole.UserRoleResponse[i].UserRole
                            || !$scope.oUserRole.UserRoleResponse[i].ResponseType
                            || !$scope.oUserRole.UserRoleResponse[i].ResponseCode
                            || !$scope.oUserRole.UserRoleResponse[i].ResponseCompanyCode
                        ) {
                            valid = false;
                        }
                        $scope.oUserRole.UserRoleResponse[i].EmployeeID = $scope.oUserRole.EmployeeID;
                        $scope.oUserRole.UserRoleResponse[i].CompanyCode = $scope.oUserRole.CompanyCode;
                    }
                }

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "UserRoleResponse": $scope.oUserRole.UserRoleResponse }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    var URL = CONFIG.SERVER + 'workflow/SaveUserRoleResponse/';
                    console.log(URL);
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                        $location.path('/frmViewContent/405');

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ProjectSettingEditorController Save.', response);
                    });
                }
            };

            $scope.SaveRoleSetting_Backup = function () {
                $scope.UserResponse = [];
                for (i = 0; i <= $scope.mUserRole.length - 1; i++) {
                    for (j = 0; j <= $scope.mUserRole[i].Content.length - 1; j++) {
                        var item = {
                            "EmployeeID": $scope.mUserRole[i].Content[j].EmployeeID,
                            "CompanyCode": $scope.mUserRole[i].Content[j].CompanyCode,
                            "UserRole": $scope.mUserRole[i].Content[j].UserRole,
                            "ResponseType": $scope.mUserRole[i].Content[j].ResponseType,
                            "ResponseCode": $scope.mUserRole[i].Content[j].ResponseCode,
                            "ResponseCompanyCode": $scope.mUserRole[i].Content[j].ResponseCompanyCode,
                            "includeSub": $scope.mUserRole[i].Content[j].ResponseCompanyCode,
                        }
                        $scope.UserResponse.push(item);
                    }
                }

                var oRequestParameter = { InputParameter: { "UserRoleResponse": $scope.UserResponse }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'workflow/SaveUserRoleResponse/';
                console.log(URL);
                $scope.HasError = false;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                            .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                            .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                            .targetEvent(ev));
                    $location.path('/frmViewContent/405');

                }, function errorCallback(response) {
                    // Error
                    console.log('error ProjectSettingEditorController Save.', response);
                });
            }

            $scope.SaveRoleSetting = function () {
                $scope.loader.enable = true;
                $scope.UserResponse = [];
                //others company
                for (i = 0; i <= $scope.mUserRoleAll.length - 1; i++) {
                    for (j = 0; j <= $scope.mUserRoleAll[i].Content.length - 1; j++) {
                        var item = {
                            "EmployeeID": $scope.mUserRoleAll[i].Content[j].EmployeeID,
                            "CompanyCode": $scope.mUserRoleAll[i].Content[j].CompanyCode,
                            "UserRole": $scope.mUserRoleAll[i].Content[j].UserRole,
                            "ResponseType": $scope.mUserRoleAll[i].Content[j].ResponseType,
                            "ResponseCode": $scope.mUserRoleAll[i].Content[j].ResponseCode,
                            "ResponseCompanyCode": $scope.mUserRoleAll[i].Content[j].ResponseCompanyCode,
                            "includeSub": $scope.mUserRoleAll[i].Content[j].includeSub,
                        }
                        $scope.UserResponse.push(item);
                    }
                }

                //current company
                for (i = 0; i <= $scope.UserRoleDisplay.length - 1; i++) {
                    for (roles = 0; roles < $scope.UserRoleDisplay[i].Roles.length; roles++) {
                        var item = {
                            "EmployeeID": $scope.UserRoleDisplay[i].EmployeeID,
                            "CompanyCode": $scope.UserRoleDisplay[i].CompanyCode,
                            "UserRole": $scope.UserRoleDisplay[i].Roles[roles],
                            "ResponseType": $scope.UserRoleDisplay[i].ResponseType,
                            "ResponseCode": $scope.UserRoleDisplay[i].ResponseCode,
                            "ResponseCompanyCode": $scope.UserRoleDisplay[i].ResponseCompanyCode,
                            "includeSub": $scope.UserRoleDisplay[i].includeSub,
                        }
                        $scope.UserResponse.push(item);
                    }
                }
                
                var oRequestParameter = { InputParameter: { "UserRoleResponse": $scope.UserResponse }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'workflow/SaveUserRoleResponse/';
                console.log(URL);
                $scope.HasError = false;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['ROLE_SETTING'].SAVE_ROLE_COMPLETE)
                            .ok('OK')
                    );
                    $location.path('/frmViewContent/' + $routeParams.id);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['SYSTEM'].WARNING)
                                .textContent($scope.Text['SYSTEM'].SAVE_ROLE_COMPLETE)
                                .ok('OK')
                        );
                   console.log('error SaveRoleSetting Save.', response);
                });
                
            }

            //auto complete Company 
            $scope.querySearchCompany = function (query) {
                var results = angular.copy(query ? $scope.CompanyList.filter(createFilterForCompany(query)) : $scope.CompanyList), deferred;
                results = results.splice(0, 30);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemCompanyChange = function (item) {
                if (angular.isDefined(item)) {
                    var comtxt = getToken(CONFIG.USER).Language == 'EN' ? item.FullNameEN : item.FullNameTH;
                    $scope.oUserRole.CompanyCode = item.CompanyCode;
                    $scope.oUserRole.searchCompanyText = item.Name + ' : ' + comtxt;
                    $scope.data.CompanyCode = item.CompanyCode;
                    $scope.data.CompanyName = item.Name;
                    $scope.data.CompanyFullName = item.Name + ' : ' + comtxt;
                    $scope.oUserRole.searchCompanyText = item.Name + ' : ' + comtxt;
                    if (!$scope.tempResultCompany || item.CompanyCode != $scope.tempResultCompany.CompanyCode)
                        $scope.GetEmployees(item.CompanyCode);
                    $scope.tempResultCompany = item;
                }
            };

            function createFilterForCompany(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var comTxt = getToken(CONFIG.USER).Language == 'EN' ? x.FullNameEN : x.FullNameTH;
                    var source = angular.lowercase(x.Name + ' : ' + comTxt);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.tryToSelectCompany = function (item) {
                item.searchCompanyText = "";
            }
            $scope.checkTextCompany = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.CompanyList.length; i++) {
                        var comTxt = getToken(CONFIG.USER).Language == 'EN' ? $scope.CompanyList[i].FullNameEN : $scope.CompanyList[i].FullNameTH;
                        if ($scope.CompanyList[i].CompanyCode == text || ($scope.CompanyList[i].Name + ' : ' + comTxt) == text) {
                            result = $scope.CompanyList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemCompanyChange(result);
                } else if ($scope.tempResultCompany) {
                    $scope.selectedItemCompanyChange($scope.tempResultCompany);
                }
            }

            //Response Company 
            function createFilterForResponseCompany(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.Name + ' : ' + x.FullNameEN);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_ResponseCompany;
            $scope.init_ResponseCompany = function () {
            }
            $scope.querySearchResponseCompany = function (query) {
                if (!$scope.roleList) return;
                var results = angular.copy(query ? $scope.CompanyList.filter(createFilterForResponseCompany(query)) : $scope.CompanyList), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemResponseCompanyChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.ResponseCompanyCode = item.CompanyCode;
                    $scope.data.ResponseCompanyDesc = item.Name;
                    tempResult_ResponseCompany = angular.copy(item);
                }
                else {
                    $scope.data.ResponseCompanyCode = '';
                    $scope.data.ResponseCompanyDesc = '';
                }
            };
            $scope.tryToSelect_ResponseCompany = function (text) {
                $scope.data.ResponseCompanyCode = '';
                $scope.data.ResponseCompanyDesc = '';
            }
            $scope.checkText_ResponseCompany = function (text) {
                if (text == '') {
                    $scope.data.ResponseCompanyCode = '';
                    $scope.data.ResponseCompanyDesc = '';
                }
            }

            //auto complete Employee 
            $scope.querySearchEmployee = function (query) {
                var results = [];
                var tempResults = angular.copy(query ? $scope.employeeDataList.filter(createFilterForEmployee(query)) : $scope.employeeDataList), deferred;
                //Nattawat S. 2021-02-19
                tempResults = tempResults.splice(0, 30);
                if (tempResults.length > 0) {
                    var oDispleyed = angular.copy($scope.UserRoleDisplay.filter(data => data.ResponseType =='O' && data.ResponseCode == $scope.tempResultOrganization.OrgUnit));

                    for (i = 0; i < tempResults.length; i++) {
                        var Exists = false;
                        for (j = 0; j < oDispleyed.length; j++) {
                            if (oDispleyed[j].EmployeeID == tempResults[i].EmployeeID
                                && oDispleyed[j].CompanyCode == tempResults[i].CompanyCode
                                //&& oDispleyed[j].ResponseCode == tempResults[i].ResponseCode
                            ) {
                                Exists = true;
                                break;
                            }
                        }
                        if (!Exists)
                            results.push(angular.copy(tempResults[i]));
                    }
                    ;
                }

                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            //auto complete Organization 
            $scope.querySearchOrganization = function (query) {
                var results = angular.copy(query ? $scope.organizationDataList.filter(createFilterForOrganization(query)) : $scope.organizationDataList), deferred;
                results = results.splice(0, 30);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemEmployeeChange = function (item) {
                if (angular.isDefined(item)) {

                    $scope.data.EmployeeID = item.EmployeeID;
                    $scope.data.EmployeeName = item.Name;
                    $scope.data.ResponseType = 'O';
                    $scope.data.ResponseCode = $scope.tempResultOrganization.OrgUnit; //Org unit
                    $scope.data.ResponseCodeDesc = $scope.tempResultOrganization.OrgName; // Org name
                    $scope.data.CompanyCode = $scope.currentCompany;
                }

            };

            $scope.selectedItemOrganizationChange = function (item) {
                if (angular.isDefined(item)) {
                    $scope.oUserRole.ResponseType = 'O';
                    $scope.oUserRole.ResponseCode = item.OrgUnit;
                    $scope.oUserRole.searchOrganizationText = item.OrgUnit + ' : ' + item.OrgName;
                    $scope.tempResultOrganization = item;
                }
            };

            $scope.selectedItemPositionChange = function (item) {
                if (angular.isDefined(item)) {
                    $scope.oUserRole.EmployeeID = item.EmployeeID;
                    $scope.oUserRole.EmployeeName = item.Name;
                    $scope.oUserRole.searchEmployeeText = item.EmployeeID + ' : ' + item.Name;
                    tempResultOrganization = item;
                }
            };

            function createFilterForEmployee(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.EmployeeID + ' : ' + x.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterForOrganization(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.OrgUnit + ' : ' + x.OrgName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.tryToSelectEmployee = function (item) {
                item.searchEmployeeText = '';
                $scope.data.EmployeeID = '';
                $scope.data.EmployeeName = '';

            }
            $scope.tryToSelectOrganization = function (item) {
                item.searchOrganizationText = '';
                $scope.oUserRole.searchEmployeeText = '';
            }

            $scope.checkTextEmployee = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.employeeDataList.length; i++) {
                        if ($scope.employeeDataList[i].EmployeeID == text || ($scope.employeeDataList[i].EmployeeID + ' : ' + $scope.employeeDataList[i].Name) == text) {
                            result = $scope.employeeDataList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemEmployeeChange(result);
                } else if ($scope.tempResultEmployee) {
                    $scope.selectedItemEmployeeChange($scope.tempResultEmployee);
                }
            }

            $scope.checkTextOrganization = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.organizationDataList.length; i++) {
                        if ($scope.organizationDataList[i].OrgUnit == text || ($scope.organizationDataList[i].OrgUnit + ' : ' + $scope.organizationDataList[i].OrgName) == text) {
                            result = $scope.organizationDataList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemEmployeeChange(result);
                } else if ($scope.tempResultOrganization) {
                    $scope.selectedItemOrganizationChange($scope.tempResultOrganization);
                }
            }

            //auto Role 
            function createFilterForRole(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Role;
            $scope.init_Role = function () {
            }
            $scope.querySearchRole = function (query) {
                if (!$scope.roleList) return;
                var results = angular.copy(query ? $scope.roleList.filter(createFilterForRole(query)) : $scope.roleList), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemRoleChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.RoleDesc = item.Description;
                    $scope.data.UserRole = item.Role;
                    tempResult_role = angular.copy(item);
                }
                else {
                    $scope.data.RoleDesc = '';
                    $scope.data.UserRole = '';
                }
            };
            $scope.tryToSelect_Role = function (text) {
                $scope.data.RoleDesc = '';
                $scope.data.UserRole = '';
            }
            $scope.checkText_Role = function (text) {
                if (text == '') {
                    $scope.data.RoleDesc = '';
                    $scope.data.UserRole = '';
                }
            }

            //auto Response Type 
            function createFilterForResponseType(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_ResponseType;
            $scope.init_ResponseType = function () {
            }
            $scope.querySearchResponseType = function (query) {
                if (!$scope.userRoleResponseTypeList) return;
                var results = angular.copy(query ? $scope.userRoleResponseTypeList.filter(createFilterForResponseType(query)) : $scope.userRoleResponseTypeList), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemResponseTypeChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.ResponseTypeDesc = item.Description;
                    $scope.data.ResponseType = item.ResponseType;
                    $scope.GetAllResponseCodeByResponseType(item.ResponseType);
                    tempResult_ResponseType = angular.copy(item);
                }
                else {
                    $scope.data.ResponseType = '';
                    $scope.data.ResponseTypeDesc = '';
                }
            };
            $scope.tryToSelect_ResponseType = function (text) {
                $scope.data.ResponseType = '';
                $scope.data.ResponseTypeDesc = '';
            }
            $scope.checkText_ResponseType = function (text) {
                if (text == '') {
                    $scope.data.ResponseType = '';
                    $scope.data.ResponseTypeDesc = '';
                }
            }

            //auto Response Code 
            function createFilterForResponseCode(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.ResponseCode + " : " + x.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_ResponseCode;
            $scope.init_ResponseCode = function () {
            }
            $scope.querySearchResponseCode = function (query) {
                if (!$scope.mResponseCode) return;
                var results = angular.copy(query ? $scope.mResponseCode.filter(createFilterForResponseCode(query)) : $scope.mResponseCode), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemResponseCodeChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.ResponseCode = item.ResponseCode;
                    $scope.data.ResponseCodeDesc = item.ResponseCode + ' : ' + item.Name;
                    $scope.data.ResponseCodeName = item.Name;
                    tempResult_ResponseCode = angular.copy(item);
                }
                else {
                    $scope.data.ResponseCode = '';
                    $scope.data.ResponseCodeDesc = '';
                    $scope.data.ResponseCodeName = '';
                }
            };
            $scope.tryToSelect_ResponseCode = function (text) {
                $scope.data.ResponseCode = '';
                $scope.data.ResponseCodeDesc = '';
                $scope.data.ResponseCodeName = '';
            }
            $scope.checkText_ResponseCode = function (text) {
                if (text == '') {
                    $scope.data.ResponseCode = '';
                    $scope.data.ResponseCodeDesc = '';
                    $scope.data.ResponseCodeName = '';
                }
            }

            function GetRoleSetting() {
                var URL = CONFIG.SERVER + 'HRPA/GetRoleSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.mRoleSetting = response.data;
                    generateUserroleToDisplayByOrg();
                    console.log('mRoleSetting = ', $scope.mRoleSetting);
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function generateUserroleToDisplayByOrg() {

                $scope.UserRoleDisplay = [];
                $scope.beforePivotRoles = [];
                if ($scope.mUserRole.length > 0) {
                    for (empInd = 0; empInd < $scope.mUserRole.length; empInd++) {
                        for (content = 0; content < $scope.mUserRole[empInd].Content.length; content++) {
                            $scope.beforePivotRoles.push(angular.copy($scope.mUserRole[empInd].Content[content]));
                            $scope.beforePivotRoles[$scope.beforePivotRoles.length - 1].FlagDelete = false;
                        }
                    }
                }


                //$scope.UserRoleDisplay = angular.copy($scope.beforePivotRoles);

                if ($scope.beforePivotRoles.length > 0) {

                    var nodeStructure = {
                        EmployeeID: $scope.beforePivotRoles[0].EmployeeID,
                        EmployeeName: $scope.beforePivotRoles[0].EmployeeName,
                        ResponseType: $scope.beforePivotRoles[0].ResponseType,
                        ResponseDesc: $scope.beforePivotRoles[0].ResponseDesc,
                        ResponseCode: $scope.beforePivotRoles[0].ResponseCode, //Org unit
                        ResponseCodeDesc: $scope.beforePivotRoles[0].ResponseCodeDesc, // Org name
                        CompanyCode: $scope.beforePivotRoles[0].CompanyCode,
                        ResponseCompanyCode: $scope.beforePivotRoles[0].ResponseCompanyCode,
                        includeSub: $scope.beforePivotRoles[0].includeSub,
                    }

                    nodeStructure.Roles = [];
                    nodeStructure.Roles.push($scope.beforePivotRoles[0].UserRole);

                    $scope.UserRoleDisplay.push(nodeStructure);

                    for (i = 0; i < $scope.beforePivotRoles.length; i++) {
                        if (i == 0) continue;
                        else {
                            var empID = $scope.beforePivotRoles[i].EmployeeID;
                            var empOrg = $scope.beforePivotRoles[i].ResponseCode;
                            var exitsEmpData = $scope.UserRoleDisplay.filter(data => data.EmployeeID == empID && data.ResponseCode == empOrg);
                            if (exitsEmpData.length > 0) {
                                exitsEmpData[0].Roles.push($scope.beforePivotRoles[i].UserRole);
                            } else {
                                var nodeStructure = {
                                    EmployeeID: $scope.beforePivotRoles[i].EmployeeID,
                                    EmployeeName: $scope.beforePivotRoles[i].EmployeeName,
                                    ResponseType: $scope.beforePivotRoles[i].ResponseType,
                                    ResponseDesc: $scope.beforePivotRoles[i].ResponseDesc,
                                    ResponseCode: $scope.beforePivotRoles[i].ResponseCode, //Org unit
                                    ResponseCodeDesc: $scope.beforePivotRoles[i].ResponseCodeDesc, // Org name
                                    CompanyCode: $scope.beforePivotRoles[i].CompanyCode,
                                    ResponseCompanyCode: $scope.beforePivotRoles[i].ResponseCompanyCode,
                                    includeSub: $scope.beforePivotRoles[i].includeSub,
                                }
                                nodeStructure.Roles = [];
                                nodeStructure.Roles.push($scope.beforePivotRoles[i].UserRole);
                                $scope.UserRoleDisplay.push(nodeStructure);
                            }
                        }
                    }
                }

                console.log('beforePivotRoles : ', $scope.beforePivotRoles);
                console.log('UserRoleDisplay : ', $scope.UserRoleDisplay);

                $scope.loader.enable = false;
            }

            $scope.dataRoleSelect = function (RoleName) {
                var isExists = $scope.data.Roles.includes(RoleName);
                if (isExists)
                    $scope.data.Roles.splice($scope.data.Roles.indexOf(RoleName), 1);
                else
                    $scope.data.Roles.push(RoleName);
            };

            $scope.ItemChange = function (object, role) {
                var isExists = object.Roles.indexOf(role);
                if (isExists > -1) {
                    object.Roles.splice(isExists, 1);
                } else {
                    object.Roles.push(role);
                }
            }

            $scope.DeleteRole = function (row) {
                var index = $scope.UserRoleDisplay.indexOf(row);
                $scope.UserRoleDisplay.splice(index, 1);
            }

        }]);
})();