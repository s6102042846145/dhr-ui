﻿(function () {
    angular.module('DESS')
        .controller('PositionContent', ['$scope', '$http', '$routeParams', '$location', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window',
            function ($scope, $http, $routeParams, $location, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
                $scope.loader.enable = false;
                var oRequestData = $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);

                $scope.Language = oEmployeeData.Language;
                $scope.CurrentAsOfDate = new Date();
                $scope.AsOfDate = $scope.CurrentAsOfDate;
                $scope.CurrentTab = "Solid";

                $scope.LevelSelected = [];
                $scope.UnitSelected = [];
                $scope.PositionSelected = [];
                $scope.levelSearch = '';
                $scope.unitSearch = '';
                $scope.positionSearch = '';
                $scope.createdDoc;

                $scope.GetOrgLevel = function () {
                    var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            //"BeginDate": ($scope.AllPeriod) ? "1900-01-01" : getDateFormatData($scope.CurrentAsOfDate),
                            //"EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormatData($scope.CurrentAsOfDate)
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.LevelList = response.data;

                        console.log('success GetOrgLevel', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgLevel', response);
                    });
                }
                $scope.GetOrgLevel();

                $scope.GetOrgUnit = function () {
                    $scope.levelSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';

                    for (var i = 0; i < $scope.LevelSelected.length; i++) {
                        $scope.levelSearch += $scope.LevelSelected[i].DLL_VALUE + ':';
                    }

                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.UnitList = response.data.oDDL;

                        console.log('success GetOrgUnit', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgUnit', response);
                    });
                }
                $scope.GetOrgUnit();

                $scope.GetPosition = function (unit) {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDDL';

                    for (var i = 0; i < $scope.UnitSelected.length; i++) {
                        $scope.unitSearch += $scope.UnitSelected[i].DLL_VALUE + ':';
                    }

                    var oRequestParameter = {
                        InputParameter: {
                            "UnitSelected": $scope.unitSearch,
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31"
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionList = response.data.oDDL;;

                        console.log('success GetPosition', response);
                    }, function errorCallback(response) {
                        console.log('error GetPosition', response);
                    });
                }
                $scope.GetPosition();


                $scope.SetIsAllPeriod = function () {
                    if ($scope.AllPeriod) {
                        $scope.AsOfDate = '';
                    } else {
                        $scope.AsOfDate = angular.copy($scope.CurrentAsOfDate);
                    }
                }
                $scope.SetCurrentTab = function (currentTab) {
                    $scope.CurrentTab = currentTab;
                    $scope.ResetDataContent();
                }

                $scope.GetDataContent = function () {
                    $scope.loader.enable = true;
                    $scope.positionSearch = '';
                    for (var i = 0; i < $scope.PositionSelected.length; i++) {
                        $scope.positionSearch += $scope.PositionSelected[i].DLL_VALUE + ':';
                    }

                    var URL = CONFIG.SERVER + 'HROM/GetPositionDataContent';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "UnitSelected": $scope.unitSearch,
                            "PositionSelected": $scope.positionSearch,
                            "BeginDate": ($scope.AllPeriod) ? "1900-01-01" : getDateFormatData($scope.CurrentAsOfDate),
                            "EndDate": '9999-12-31' // ($scope.AllPeriod) ? "9999-12-31" : getDateFormatData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.oCreateDocEdit = response.data.oCreateDocEdit;
                        $scope.oCreateDocDelete = response.data.oCreateDocDelete;
                        $scope.oCreateDocPerDelete = response.data.oCreateDocPerDelete;
                        $scope.oCreateDocInactive = response.data.oCreateDocInactive;
                        $scope.oCreateDocNew = response.data.oCreateDocNew;

                        switch ($scope.CurrentTab) {
                            case "Solid":
                                $scope.SolidMasterDataList = response.data.oData;
                                break;
                            case "Doted":
                                $scope.DotedMasterDataList = response.data.oData;
                                break;
                            default:
                        }



                        console.log('success GetDataContent', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetDataContent', response);
                    });
                };

                $scope.GetPositionDataDetail = function (unitCode, startDate, endDate) {

                    var URL = CONFIG.SERVER + 'HROM/GetPositionDataDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "UnitCode": unitCode,
                            "BeginDate": getDateFormatData(startDate),
                            "EndDate": getDateFormatData(endDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionDataDetail = response.data.oData;
                        console.log('success GetPositionDataDetail', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetPositionDataDetail', response);
                    });
                }
                $scope.ResetDataContent = function () {
                    $scope.AllPeriod = false;
                    $scope.CurrentAsOfDate = new Date();
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                    $scope.PositionSelected = [];
                    $scope.levelSearch = '';
                    $scope.unitSearch = '';
                    $scope.positionSearch = '';
                    $scope.SolidMasterDataList = [];
                    $scope.DotedMasterDataList = [];

                }

                $scope.GetMarkDataAll = function () {
                    $scope.levelSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetMarkDataAll';
                    var oRequestParameter = {
                        InputParameter: {},
                        CurrentEmployee: oEmployeeData,
                        Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.MarkDataList = response.data;

                        console.log('success GetMarkDataAll', response);
                    }, function errorCallback(response) {
                        console.log('error GetMarkDataAll', response);
                    });
                }
                $scope.GetMarkDataAll();
                //#endregion
                //#region Show detail data
                $scope.ViewDetailData = function (data) {
                    //get data from API and view in modal
                    //#region HROMPOSITION_EDIT_xxxxxxxx
                    $scope.PositionInfoEditRequest = [];
                    var dataCategory = 'HROMPOSITION_EDIT_' + data.PosCode;
                    var result = $filter('filter')($scope.oCreateDocEdit, { DataCategory: dataCategory });
                    if (result.length > 0) {
                        $scope.PositionInfoEditRequest.push(result[0]);
                    }
                    //#endregion

                    //#region HROMPOSITION_DELETE_xxxxxxxx
                    $scope.PositionInfoDeleteRequest = [];
                    var dataCategory = 'HROMPOSITION_DELETE_' + data.PosCode;
                    var result = $filter('filter')($scope.oCreateDocDelete, { DataCategory: dataCategory });
                    if (result.length > 0) {
                        $scope.PositionInfoDeleteRequest.push(result[0]);
                    }
                    //#endregion

                    //#region HROMPOSITION_PERDELETE_xxxxxxxx
                    $scope.PositionInfoPerDeleteRequest = [];
                    var dataCategory = 'HROMPOSITION_PERDELETE_' + data.PosCode;
                    var result = $filter('filter')($scope.oCreateDocPerDelete, { DataCategory: dataCategory });
                    if (result.length > 0) {
                        $scope.PositionInfoPerDeleteRequest.push(result[0]);
                    }
                    //#endregion

                    //#region HROMPOSITION_INACTIVE_xxxxxxxx
                    $scope.PositionInfoInactiveRequest = [];
                    var dataCategory = 'HROMPOSITION_INACTIVE_' + data.PosCode;
                    var result = $filter('filter')($scope.oCreateDocInactive, { DataCategory: dataCategory });
                    if (result.length > 0) {
                        $scope.PositionInfoInactiveRequest.push(result[0]);
                    }
                    //#endregion

                    $scope.PositionData = angular.copy(data);
                    $scope.GetPositionDataDetail($scope.PositionData.UnitCode, $scope.PositionData.StartDate, $scope.PositionData.EndDate);
                }
                //#endregion

                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '10', numPage: 1
                }

                $scope.genDatas = function (obj) {
                    if (obj != undefined && obj.length > 0) {
                        var datas = obj;
                        setNumOfPage(datas);
                        var res = [];
                        if ($scope.pagin.numPage > 1) {
                            datas.forEach(function (value, i) {
                                var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                                var to = from + parseInt($scope.pagin.itemPerPage);

                                if (from <= i && to > i)
                                    res.push(value);
                            });
                        } else {
                            if (datas != null) {
                                res = datas.slice();
                            }
                        }

                        return res;
                    }

                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion

                //#region date format
                $scope.getDateFormat = function (date) {
                    return getDateFormat(date);
                }
                function getDateFormat(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

                $scope.getDateTimeFormat = function (date) {
                    return getDateTimeFormat(date);
                }
                function getDateTimeFormat(date) { return $filter('date')(date, 'dd/MM/yyyy HH:mm'); }

                $scope.getDateFormatData = function (date) {
                    return getDateFormatData(date);
                }

                function getDateFormatData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

                $scope.getDateFormatURL = function (date) {
                    return getDateFormatURL(date);
                }
                function getDateFormatURL(date) { return $filter('date')(date, 'yyyyMMdd'); }

                //#endregion

                //#region date format
                function filterTextDescription(array, attr, value, objReturn) {
                    $scope.isData = false;
                    if (value == "")
                        return "";
                    else {
                        if (array != null && array != undefined) {
                            for (var i = 0; i < array.length; i += 1) {
                                if (array[i][attr] == value) {
                                    $scope.isData = true;
                                    return array[i][objReturn];
                                }
                            }
                        }

                    }
                }
                //#endregion
                //#endregion

            }]);
})();