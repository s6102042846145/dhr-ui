﻿

(function () {
    angular.module('DESS')
        .controller('PositionDotedEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            $scope.Language = oEmployeeData.Language;
            //unitcode|poscode|startdate|enddate


            if ($routeParams.otherParam != undefined) {
                $scope.itemKeys = (unescape($routeParams.otherParam)).split('|');
                if ($scope.itemKeys.length > 0) {
                    /* $scope.BandValue = $scope.itemKeys[0];*/
                    $scope.UnitCode = $scope.itemKeys[0];
                    $scope.HeadPosCode = $scope.itemKeys[1];
                    $scope.HeadStartDate = setDateFromURL($scope.itemKeys[2]);
                    $scope.HeadEndDate = setDateFromURL($scope.itemKeys[3]);
                }
            } else {
                $scope.UnitCode = $scope.document.Additional.PositionDotedData.HeadData_UnitCode;
                $scope.HeadPosCode = $scope.document.Additional.PositionDotedData.HeadData_HeadPosCode
                $scope.HeadStartDate = $scope.document.Additional.PositionDotedData.HeadData_StartDate
                $scope.HeadEndDate = $scope.document.Additional.PositionDotedData.HeadData_EndDate
            }

            //Doted detail list
            $scope.PositionDotedDetail = $scope.document.Additional.PositionDotedData.PositionDotedDetail;
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
            };
            $scope.ChildAction.SetData();

            //ValidateData Function  : Use to validate some login in javascript berfore 
            $scope.ChildAction.ValidateData = function () {
                //Do something ...
                $scope.validateResult = true;
                for (var i = 0; i < $scope.PositionDotedDetail.length; i++) {
                    var index = $scope.PositionDotedDetail.indexOf($scope.PositionDotedDetail[i]);
                    if ($scope.PositionDotedDetail[i].StartDate == undefined || $scope.PositionDotedDetail[i].EndDate == undefined ||
                        $scope.PositionDotedDetail[i].StartDate == null || $scope.PositionDotedDetail[i].EndDate == null ||
                        $scope.PositionDotedDetail[i].StartDate == '' || $scope.PositionDotedDetail[i].EndDate == '' ||
                        $scope.PositionDotedDetail[i].StartDate > $scope.PositionDotedDetail[i].EndDate) {
                        $scope.validateResult = false;
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(false)
                                .title($scope.Text['SYSTEM']['WARNING'])
                                .textContent($scope.Text['OM_POSITION']['DATE_INCORRECT'].replace('{0}', index +1))
                                    .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                                );
                        return;
                    }
                }
                //validate overlap date 
                if ($scope.validateResult) {
                    const dotedDetailByHeadPosCode = $scope.PositionDotedDetail.reduce((acc, value) => {
                        // Group initialization
                        if (!acc[value.HeadPosCode]) {
                            acc[value.HeadPosCode] = [];
                        }
                        // Grouping
                        acc[value.HeadPosCode].push(value);
                        return acc;
                    }, {});

                    for (let key in dotedDetailByHeadPosCode) {
                        if (dotedDetailByHeadPosCode[key] != null && dotedDetailByHeadPosCode[key].length > 1)
                            $scope.validateResult = true;
                        for (i = 0; i < dotedDetailByHeadPosCode[key].length - 1; i += 1) {
                            for (j = i + 1; j < dotedDetailByHeadPosCode[key].length; j += 1) {
                                if (dateRangeOverlaps(
                                    dotedDetailByHeadPosCode[key][i].StartDate, dotedDetailByHeadPosCode[key][i].EndDate,
                                    dotedDetailByHeadPosCode[key][j].StartDate, dotedDetailByHeadPosCode[key][j].EndDate)
                                ) {
                                    $scope.validateResult = false;
                                    $scope.MessageError = 'ข้อมูลตำแหน่งผู้ใต้บังคับบัญชา : ' + dotedDetailByHeadPosCode[key][i].PositionOfSubordinate + ' วันที่ ' + getDateFormat(dotedDetailByHeadPosCode[key][i].StartDate) + '-' + getDateFormat(dotedDetailByHeadPosCode[key][i].EndDate) +
                                        ' และวันที่ ' + getDateFormat(dotedDetailByHeadPosCode[key][j].StartDate) + '-' + getDateFormat(dotedDetailByHeadPosCode[key][j].EndDate) + ' ทับซ้อนกัน';
                                    $mdDialog.show(
                                        $mdDialog.alert()
                                            .clickOutsideToClose(false)
                                            .title($scope.Text['SYSTEM']['WARNING'])
                                            .textContent($scope.MessageError)
                                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                                    );
                                    return;
                                }
                            }
                        }
                    }
                }


                return $scope.validateResult;
            }

            function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
                if (a_start <= b_start && b_start <= a_end) return true; // b starts in a
                if (a_start <= b_end && b_end <= a_end) return true; // b ends in a
                if (b_start <= a_start && a_end <= b_end) return true; // a in b
                return false;
            }

            //LoadData Function : Use to add some logic  after validate funcction to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
                for (var i = 0; i < $scope.PositionDotedDetail.length; i++) {
                    $scope.PositionDotedDetail[i].StartDate = getDateFormatSave($scope.PositionDotedDetail[i].StartDate);
                    $scope.PositionDotedDetail[i].EndDate = getDateFormatSave($scope.PositionDotedDetail[i].EndDate);
                    $scope.PositionDotedDetail[i].UnitCode = $scope.UnitCode;
                }
                $scope.document.Additional.PositionDotedData.HeadData_UnitCode = $scope.UnitCode;
                $scope.document.Additional.PositionDotedData.HeadData_HeadPosCode = $scope.HeadPosCode;
                $scope.document.Additional.PositionDotedData.HeadData_StartDate = $scope.HeadStartDate;
                $scope.document.Additional.PositionDotedData.HeadData_EndDate = $scope.HeadEndDate;
                $scope.document.Additional.PositionDotedData.PositionDotedDetail = $scope.PositionDotedDetail;
            };


            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 
            $scope.GetOrgUnit = function () {
                var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "LevelSelected": '',
                        "BeginDate": "1999-01-01",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UnitList = response.data.oDDL;

                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.GetOrgUnit();

            $scope.GetBand = function () {
                var URL = CONFIG.SERVER + 'HROM/GetBandDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "BeginDate": "1999-01-01",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.BandList = response.data.oDDL;

                    console.log('success GetBand', response);
                }, function errorCallback(response) {
                    console.log('error GetBand', response);
                });
            }
            $scope.GetBand();

            $scope.GetPositionDotedData = function () {
                if ($scope.UnitCode != null && $scope.HeadPosCode != null &&
                    $scope.UnitCode != '' && $scope.HeadPosCode != '') {
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDotedDataContent';
                    var oRequestParameter = {
                        InputParameter: {
                            "UnitSelected": $scope.UnitCode,
                            "PositionSelected": $scope.HeadPosCode,
                            "BeginDate": $scope.HeadStartDate,
                            "EndDate": $scope.HeadEndDate
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (response.data.oData.length > 0) {
                            $scope.PositionData = response.data.oData[0];
                        }

                        console.log('success GetPositionData', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetPositionData', response);
                    });
                }
            };
            $scope.GetPositionDotedData();

            $scope.GetPositionUnderSupervisor = function () {
                var URL = CONFIG.SERVER + 'HROM/GetPositionUnderSupervisorDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "PosCode": $scope.HeadPosCode,
                        "BeginDate": $scope.HeadStartDate,
                        "EndDate": $scope.HeadEndDate
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PositionUnderSupervisorList = response.data.oDDL;

                    console.log('success GetPosition', response);
                }, function errorCallback(response) {
                    console.log('error GetPosition', response);
                });
            }
            $scope.GetPositionUnderSupervisor();

            $scope.GetPositionUnderSupervisorNew = function () {
                var URL = CONFIG.SERVER + 'HROM/GetPositionUnderSupervisorNewDDL';

                var oRequestParameter = {
                    InputParameter: {
                        "BeginDate": $scope.HeadStartDate,
                        "EndDate": $scope.HeadEndDate
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PositionUnderSupervisorNewList = response.data.oDDL;
                    console.log('success GetPositionUnderSupervisorNew', response);
                }, function errorCallback(response) {
                    console.log('error GetPositionUnderSupervisorNew', response);
                });
            }
            $scope.GetPositionUnderSupervisorNew();

            //when click ddl position under supervisor
            $scope.GetPositionDotedDetail = function (headposcode, poscode) {
                //get list positon doted
                $scope.PositionDotedDetail = [];
                var URL = CONFIG.SERVER + 'HROM/GetPositionDotedDetail';
                var oRequestParameter = {
                    InputParameter: {
                        "HeadPosCode": headposcode,
                        "PosCode": poscode,
                        "BeginDate": $scope.HeadStartDate,
                        "EndDate": $scope.HeadEndDate
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PositionDotedDetail = response.data.oData;
                    console.log('success GetPositionDotedDetail', response);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('error GetPositionDotedDetail', response);
                });

            }
            $scope.selectedPosCode = function (data) {
                $scope.posSelected = data;
                $scope.posNewSelected = '';
                var TextDesc = $filter('filter')($scope.PositionUnderSupervisorNewList, { DLL_VALUE: data });
                $scope.PositionOfSubordinate = TextDesc.length == 0 ? '' : TextDesc[0].DLL_DATA;
                $scope.GetPositionDotedDetail('', $scope.posSelected);
            }
            $scope.selectedNewPosCode = function (data) {
                $scope.posSelected = '';
                $scope.posNewSelected = data;
                var TextDesc = $filter('filter')($scope.PositionUnderSupervisorNewList, { DLL_VALUE: data });
                $scope.PositionOfSubordinate = TextDesc.length == 0 ? '' : TextDesc[0].DLL_DATA;
                $scope.GetPositionDotedDetail('', $scope.posNewSelected);
            }

            //when click add position button
            $scope.AddPositionDotedData = function () {
                if ($scope.posSelected != '' && $scope.posSelected != undefined || $scope.posNewSelected != '' && $scope.posNewSelected != undefined) {
                    //get list positon doted
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDotedDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "HeadPosCode": $scope.HeadPosCode,
                            "PosCode": $scope.posSelected != '' ? $scope.posSelected : $scope.posNewSelected,
                            "BeginDate": $scope.HeadStartDate,
                            "EndDate": $scope.HeadEndDate
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.AddPositionDotedDetail = response.data.oData;

                        //0001-01-01T00:00:00
                        if ($scope.AddPositionDotedDetail.length > 0) {
                            $scope.AddPositionDotedDetail[0].StartDate = $scope.AddPositionDotedDetail[0].StartDate == '0001-01-01T00:00:00' ? '' : $scope.AddPositionDotedDetail[0].StartDate;
                            $scope.AddPositionDotedDetail[0].EndDate = $scope.AddPositionDotedDetail[0].EndDate == '0001-01-01T00:00:00' ? '' : $scope.AddPositionDotedDetail[0].EndDate;
                            $scope.PositionDotedDetail.unshift($scope.AddPositionDotedDetail[0]);
                        }
                        console.log('success AddPositionDotedData', response);
                    }, function errorCallback(response) {
                        console.log('error AddPositionDotedData', response);
                    });
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['PLEASE_SELECT'] + ' ' + $scope.Text['OM_POSITION']['SUBORDINATE_POSITION'] + ' ' + $scope.Text['SYSTEM']['OR'] + ' ' + $scope.Text['OM_POSITION']['SUBORDINATE_POSITION_NEW'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }


            }

            $scope.EditItem = function (item) {
                if ($scope.PositionDotedDetail.length > 0) {
                    var index = $scope.PositionDotedDetail.indexOf(item);
                    $scope.PositionDotedDetail[index].ActionType = "U";
                }

            }
            $scope.DeleteItem = function (item) {
                if ($scope.PositionDotedDetail.length > 0) {
                    var index = $scope.PositionDotedDetail.indexOf(item);
                    //  
                    if ($scope.PositionDotedDetail[index].Id == undefined || $scope.PositionDotedDetail[index].Id == '') {
                        $scope.PositionDotedDetail.splice(index, 1);
                    } else {
                        $scope.PositionDotedDetail[index].ActionType = "D";
                    }

                }
            }


            function multipleDateRangeOverlaps() {
                var i, j;
                if (arguments.length % 2 !== 0)
                    throw new TypeError('Arguments length must be a multiple of 2');
                for (i = 0; i < arguments.length - 2; i += 2) {
                    for (j = i + 2; j < arguments.length; j += 2) {
                        if (
                            dateRangeOverlaps(
                                arguments[i], arguments[i + 1],
                                arguments[j], arguments[j + 1]
                            )
                        ) return true;
                    }
                }
                return false;
            }

            //#region date format
            $scope.setDateFromURL = function (date) {
                return setDateFromURL(date);
            }
            function setDateFromURL(date) {
                var year = date.substring(0, 4);
                var month = date.substring(4, 6);
                var day = date.substring(6, 8);
                // var date = new Date(year, month - 1, day);
                return year + '-' + month + '-' + day;
            }

            $scope.getDateFormat = function (date) {
                return getDateFormat(date);
            }
            function getDateFormat(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

            $scope.getDateFormatData = function (date) {
                return getDateFormatData(date);
            }

            function getDateFormatData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            $scope.getDateFormatURL = function (date) {
                return getDateFormatURL(date);
            }
            function getDateFormatURL(date) { return $filter('date')(date, 'yyyyMMdd'); }

            $scope.getDateFormatSave = function (date) {
                return getDateFormatSave(date);
            }
            function getDateFormatSave(date) { return $filter('date')(date, 'yyyy-MM-ddT00:00:00'); }
            //#endregion

        }]);
})();