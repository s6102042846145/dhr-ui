﻿(function () {
    angular.module('DESS')
        .controller('PositionDeleteEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#region #### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            $scope.Language = oEmployeeData.Language;
            $scope.CurrentDate = new Date();
            $scope.Textcategory = 'OM_POSITION';
            $scope.param = [];
            $scope.IsHead = false;

            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                    $scope.IsNewID = false;
                } else {
                    $scope.IsEdit = false;
                    $scope.IsNewID = true;

                }
            }

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                $scope.PositionData = $scope.document.Additional.PositionData;
                $scope.PositionDataOld = $scope.document.Additional.PositionData_OLD;
                if ($scope.IsNewID) {
                    $scope.PositionData.StartDate = new Date();
                    $scope.PositionData.EndDate = new Date(9999, 11, 31, 0, 0, 0, 0);
                }
                if ($scope.PositionData.HeadOfUnitFlag == "Y") {
                    $scope.IsHead = true;
                } else {
                    $scope.IsHead = false;
                }
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.PositionData.StartDate = $scope.getDateFormatSave($scope.PositionData.StartDate);
                $scope.PositionData.EndDate = $scope.getDateFormatSave($scope.PositionData.EndDate);
                if ($scope.IsHead == true) {
                    $scope.PositionData.HeadOfUnitFlag = "Y";
                } else {
                    $scope.PositionData.HeadOfUnitFlag = "";
                }
                if ($scope.PositionDataOld.StartDate == null) {
                    $scope.PositionDataOld.StartDate = '0001-01-01T00:00:00';
                }
                if ($scope.PositionDataOld.EndDate == null) {
                    $scope.PositionDataOld.EndDate = '0001-01-01T00:00:00';
                }

            };


            //#endregion #### FRAMEWORK FUNCTION ### END

            //#region #### OTHERS FUNCTION ### START 

            $scope.GetOrgUnit = function () {
                var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "LevelSelected": '',
                        "BeginDate": "1999-01-01",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UnitList = response.data.oDDL;

                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.GetOrgUnit();

            $scope.GetBand = function () {
                var URL = CONFIG.SERVER + 'HROM/GetBandDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "BeginDate": "1999-01-01",
                        "EndDate": "1999-01-01"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.BandList = response.data.oDDL;

                    console.log('success GetBand', response);
                }, function errorCallback(response) {
                    console.log('error GetBand', response);
                });
            }
            $scope.GetBand();

            $scope.GetPositionDataDetail = function () {
                if ($scope.PositionData.UnitCode != null && $scope.PositionData.StartDate != null && $scope.PositionData.EndDate != null &&
                    $scope.PositionData.UnitCode != '' && $scope.PositionData.StartDate != '' && $scope.PositionData.EndDate != '') {
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDataDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "UnitCode": $scope.PositionData.UnitCode,
                            "BeginDate": getDateFormatData($scope.PositionData.StartDate),
                            "EndDate":  getDateFormatData($scope.PositionData.EndDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionDataDetail = response.data.oData;
                        console.log('success GetPositionDataDetail', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetPositionDataDetail', response);
                    });
                }
            }
            $scope.GetPositionDataDetail();

            $scope.checkValue = function (value) {
                if (value == true) {
                    $scope.IsHead = true;
                    $scope.PositionData.HeadOfUnitFlag = 'Y';
                } else {
                    $scope.IsHead = false;
                    $scope.PositionData.HeadOfUnitFlag = '';
                }
            }
            //#region date format

            $scope.getDateFormat = function (date) {
                return getDateFormat(date);
            }
            function getDateFormat(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

            $scope.getDateTimeFormate = function (date) {
                return getDateTimeFormate(date);
            }
            function getDateTimeFormate(date) { return $filter('date')(date, 'dd/MM/yyyy HH:mm'); }

            $scope.getDateFormatData = function (date) {
                return getDateFormatData(date);
            }
            function getDateFormatData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            $scope.getDateFormatURL = function (date) {
                return getDateFormatURL(date);
            }
            function getDateFormatURL(date) { return $filter('date')(date, 'yyyyMMdd'); }

            $scope.getDateFormatSave = function (date) {
                return getDateFormatSave(date);
            }
            function getDateFormatSave(date) { return $filter('date')(date, 'yyyy-MM-ddT00:00:00'); }
            //#endregion

            //#endregion  #### OTHERS FUNCTION ### END 
        }]);
})();