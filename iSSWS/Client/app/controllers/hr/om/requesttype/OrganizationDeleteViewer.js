﻿(function () {
    angular.module('DESS')
        .controller('OrganizationDeleteViewerController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.Textcategory = 'OM_UNIT_MANAGEMENT';
            $scope.OrganizationLevelList = [];
            $scope.CostCenterList = [];
            $scope.HeadOfOrgList = [];
            $scope.OrgLevelByCriteria = [];

            $scope.init = function () {

            };

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                if ($scope.document.Additional.HeadOfOraganizationID != '' && $scope.document.Additional.OrganizationID != null)
                    getRootOrgByHeadOrg($scope.document.Additional.HeadOfOrganizationID);
                $scope.BeginDate = angular.copy(getDateFormat(new Date($scope.document.Additional.BeginDate)));
                $scope.EndDate = angular.copy(getDateFormat(new Date($scope.document.Additional.EndDate)));

                getCostCenter();
                getOrganizationLevel();

                $scope.loader.enable = false;
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {

            };


            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 
            function getDateFormat(date) { return $filter('date')(date, 'yyyy-MM-dd'); }


            function getCostCenter() {
                var oRequestParameter = {
                    InputParameter: {
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date())
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetCostCenterDDL/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.CostCenterList = response.data.oDDL;
                    var cost = $scope.CostCenterList.filter(data => data.DLL_VALUE == $scope.document.Additional.CostCenterID);
                    $scope.CostCenter = cost.length > 0 ? cost[0].DLL_DATA : '';
                    console.log('success Cost center.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error Cost center ddl.');
                });
            }

            function getOrganizationLevel() {
                var oRequestParameter = {
                    InputParameter: {
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date())
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrganizationLevelList = response.data;
                    var level = $scope.OrganizationLevelList.filter(data => data.DLL_VALUE == $scope.document.Additional.OrganizationLevelID);

                    $scope.OrganizationLevel = level.length > 0 ? level[0].DLL_DATA : '';

                    getHeadOfOrganization();
                    console.log('success Organization level.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error Organization level ddl.');
                });
            }

            function getHeadOfOrganization() {
                var oRequestParameter = {
                    InputParameter: {
                        OrgLevel: '',//!$scope.document.Additional.OrganizationLevelID ? '' : $scope.document.Additional.OrganizationLevelID,
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date())
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetHeadOfOrgUnitDDL/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).
                    then(function successCallback(response) {
                        // Success
                        $scope.HeadOfOrgList = response.data.oDDL;
                        var head = $scope.HeadOfOrgList.filter(data => data.DLL_VALUE == $scope.document.Additional.HeadOfOrganizationID);
                        $scope.HeadOfOrganization = head.length > 0 ? head[0].DLL_DATA : '';
                        getOrganizationLeader();
                        console.log('success HeadOfOrgList level.');
                    }, function errorCallback(response) {
                        // Error
                        $scope.loader.enable = false;
                        console.log('error HeadOfOrgList level ddl.');
                    });
            }
            $scope.getHeadOfOrganizationByScreen = function () {
                getHeadOfOrganization();
            };

            function getOrganizationLeader() {
                $scope.LeaderOfHeadPerson = '';
                $scope.LeaderOfHeadPosition = '';
                var oRequestParameter = {
                    InputParameter: {
                        HEAD_ORG: $scope.document.Additional.HeadOfOrganizationID ? $scope.document.Additional.HeadOfOrganizationID : '',
                        BeginDate: getDateFormatToService(new Date($scope.document.Additional.BeginDate)),
                        EndDate: getDateFormatToService(new Date($scope.document.Additional.EndDate))
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/getLeaderByHeadOfOrganization/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data) {
                        $scope.LeaderOfHeadPerson = $scope.CurrentEmployee.Language == 'EN' ? response.data.HeadUnitNameEN : response.data.HeadUnitNameTH;
                        $scope.LeaderOfHeadPosition = $scope.CurrentEmployee.Language == 'EN' ? response.data.HeadUnitPositionEN : response.data.HeadUnitPositionTH;
                    }
                    console.log('success getOrganizationLeader.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getOrganizationLeader.');
                });
            }

            //Generate list of line manager
            $scope.getRootOrgByHeadOrg = function () {
                getRootOrgByHeadOrg();
            };

            function getRootOrgByHeadOrg() {
                $scope.loader.enable = true;
                var oRequestParameter = {
                    InputParameter: {
                        UNIT_ID: $scope.document.Additional.OrganizationID ? $scope.document.Additional.OrganizationID : '',
                        LEVEL_ID: $scope.document.Additional.OrganizationLevelID ? $scope.document.Additional.OrganizationLevelID : '',
                        HEADORG_ID: $scope.document.Additional.HeadOfOrganizationID ? $scope.document.Additional.HeadOfOrganizationID : '',
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date()),
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/getOrganizationLevelChart/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrgLevelByCriteria = response.data;
                    $scope.loader.enable = false;
                    console.log('success loadOrgLevelByCriteria.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error loadOrgLevelByCriteria.');
                });
            }

            //Genarate org sibling with same head org
            //Genarate org sibling with same head org
            $scope.siblingOrganization = function () {
                $scope.loader.enable = true;
                var oRequestParameter = {
                    InputParameter: {
                        ORG_ID: $scope.document.Additional.HeadOfOrganizationID,
                        BeginDate: getDateFormatToService($scope.document.Additional.BeginDate),
                        EndDate: getDateFormatToService($scope.document.Additional.EndDate)
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/getPriorityListByOrganizationID/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.unitPriorityList = response.data.length > 0 ? response.data : [];
                    SortingPriority();
                    $scope.loader.enable = false;
                    console.log('success siblingOrganization.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error siblingOrganization.');
                });
            };

            function SortingPriority() {
                var oShortName = $scope.CurrentEmployee.Language == 'EN' ? $scope.document.Additional.ShortNameEN : $scope.document.Additional.ShortNameTH;
                var oFullName = $scope.CurrentEmployee.Language == 'EN' ? $scope.document.Additional.FullNameEN : $scope.document.Additional.FullNameTH;
                var currentDetail = {
                    SEQ: parseInt($scope.document.Additional.OrderNo),
                    PRIORITY_DETAIL: oFullName + '(' + $scope.document.Additional.OrganizationID + ':' + oShortName + ')',
                    UNIT_CODE: $scope.document.Additional.OrganizationID
                };
                var spliceIndex = $scope.unitPriorityList.findIndex(data => data.UNIT_CODE == currentDetail.UNIT_CODE);
                if (spliceIndex > -1)
                    $scope.unitPriorityList.splice(spliceIndex, 1);
                $scope.unitPriorityList.push(currentDetail);
                //Sorting
                $scope.unitPriorityList.sort((b, a) => {
                    if (a.SEQ > b.SEQ) {
                        if (a.UNIT_CODE > b.UNIT_CODE) {
                            return 1
                        } else {
                            return -1
                        }

                    } else {
                        return -1
                    }
                });
                $scope.unitPriorityList.sort((b, a) => (a.SEQ < b.SEQ) ? 1 : -1);
                $scope.unitPriorityList.forEach((element, index) => {
                    element.SEQ = index + 1
                });
            }

            function getDateFormatToService(date) {
                return $filter('date')(date, 'yyyy-MM-dd');
            }

            function getDateFormat(date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

        }]);
})();