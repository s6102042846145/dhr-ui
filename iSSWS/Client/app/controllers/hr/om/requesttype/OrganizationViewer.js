﻿(function () {
    angular.module('DESS')
        .controller('OrganizationViewerController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.Textcategory = 'OM_UNIT_MANAGEMENT';
            $scope.OrganizationLevelList = [];
            $scope.CostCenterList = [];
            $scope.HeadOfOrgList = [];
            $scope.OrgLevelByCriteria = [];
            $scope.isNotAllowEdit = true;
            $scope.ExampleOMID = '';

            $scope.init = function () {
                //Do something ...
            };

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                if ($scope.document.Additional.OrganizationData.HeadOfOraganizationID != '' && $scope.document.Additional.OrganizationData.OrganizationID != null)
                    getRootOrgByHeadOrg($scope.document.Additional.OrganizationData.HeadOfOrganizationID);

                $scope.OrganizationData = angular.copy($scope.document.Additional.OrganizationData);
                $scope.OrganizationData.BeginDate = getDateFormat(new Date($scope.OrganizationData.BeginDate));
                $scope.OrganizationData.EndDate = getDateFormat(new Date($scope.OrganizationData.EndDate));

                $scope.OrganizationDataOLD = angular.copy($scope.document.Additional.OrganizationDataOLD);
                $scope.OrganizationDataOLD.BeginDate = getDateFormat(new Date($scope.OrganizationDataOLD.BeginDate));
                $scope.OrganizationDataOLD.EndDate = getDateFormat(new Date($scope.OrganizationDataOLD.EndDate));

                getCostCenter();
                getOrganizationLevel();
                getExampleID();
                $scope.loader.enable = false;
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.Additional.OrganizationData.BeginDate = new Date($scope.document.Additional.OrganizationData.BeginDate);
                $scope.document.Additional.OrganizationData.BeginDate.setHours(7);
                $scope.document.Additional.OrganizationData.EndDate = new Date($scope.document.Additional.OrganizationData.EndDate);
                $scope.document.Additional.OrganizationData.EndDate.setHours(7);
            };
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 
            function getDateFormat(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            function getExampleID() {
                if (!$scope.document.Additional.OrganizationDataOLD.OrganizationID && !$scope.document.Additional.OrganizationData.OrganizationID) {
                    var oRequestParameter = {
                        InputParameter: {
                            organization: $scope.document.Additional.OrganizationData
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HROM/getExampleOMUnitID/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.ExampleOMID = response.data;
                    }, function errorCallback(response) {
                        // Error
                        $scope.loader.enable = false;
                        console.log('error Cost center ddl.');
                    });
                }
            }

            function getCostCenter() {
                var oRequestParameter = {
                    InputParameter: {
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date())
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetCostCenterDDL/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.CostCenterList = response.data.oDDL;
                    var cost = $scope.CostCenterList.filter(data => data.DLL_VALUE == $scope.OrganizationData.CostCenterID);
                    var costOLD = $scope.CostCenterList.filter(data => data.DLL_VALUE == $scope.OrganizationDataOLD.CostCenterID);
                    $scope.OrganizationData.CostCenter = cost.length > 0 ? cost[0].DLL_DATA : '';
                    $scope.OrganizationDataOLD.CostCenter = costOLD.length > 0 ? costOLD[0].DLL_DATA : '';
                    console.log('success Cost center.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error Cost center ddl.');
                });
            }

            function getOrganizationLevel() {
                var oRequestParameter = {
                    InputParameter: {
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date())
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrganizationLevelList = response.data;
                    var level = $scope.OrganizationLevelList.filter(data => data.DLL_VALUE == $scope.OrganizationData.OrganizationLevelID);
                    var levelOLD = $scope.OrganizationLevelList.filter(data => data.DLL_VALUE == $scope.OrganizationDataOLD.OrganizationLevelID);

                    $scope.OrganizationData.OrganizationLevel = level.length > 0 ? level[0].DLL_DATA : '';
                    $scope.OrganizationDataOLD.OrganizationLevel = levelOLD.length > 0 ? levelOLD[0].DLL_DATA : '';

                    getHeadOfOrganization();
                    console.log('success Organization level.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error Organization level ddl.');
                });
            }

            function getHeadOfOrganization() {
                var oRequestParameter = {
                    InputParameter: {
                        OrgLevel: '',//!$scope.document.Additional.OrganizationData.OrganizationLevelID ? '' : $scope.document.Additional.OrganizationData.OrganizationLevelID,
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date())
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetHeadOfOrgUnitDDL/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).
                    then(function successCallback(response) {
                        // Success
                        $scope.HeadOfOrgList = response.data.oDDL;
                        var head = $scope.HeadOfOrgList.filter(data => data.DLL_VALUE == $scope.OrganizationData.HeadOfOrganizationID);
                        var headOLD = $scope.HeadOfOrgList.filter(data => data.DLL_VALUE == $scope.OrganizationDataOLD.HeadOfOrganizationID);
                        $scope.OrganizationData.HeadOfOrganization = head.length > 0 ? head[0].DLL_DATA : '';
                        $scope.OrganizationDataOLD.HeadOfOrganization = headOLD.length > 0 ? headOLD[0].DLL_DATA : '';
                        getOrganizationLeader();
                        console.log('success HeadOfOrgList level.');
                    }, function errorCallback(response) {
                        // Error
                        $scope.loader.enable = false;
                        console.log('error HeadOfOrgList level ddl.');
                    });
            }
            $scope.getHeadOfOrganizationByScreen = function () {
                getHeadOfOrganization();
            };

            function getOrganizationLeader() {
                $scope.LeaderOfHeadPerson = '';
                $scope.LeaderOfHeadPosition = '';
                var oRequestParameter = {
                    InputParameter: {
                        HEAD_ORG: $scope.document.Additional.OrganizationData.HeadOfOrganizationID ? $scope.document.Additional.OrganizationData.HeadOfOrganizationID : '',
                        BeginDate: getDateFormatToService(new Date($scope.document.Additional.OrganizationData.BeginDate)),
                        EndDate: getDateFormatToService(new Date($scope.document.Additional.OrganizationData.EndDate))
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/getLeaderByHeadOfOrganization/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data) {
                        $scope.LeaderOfHeadPerson = $scope.CurrentEmployee.Language == 'EN' ? response.data.HeadUnitNameEN : response.data.HeadUnitNameTH;
                        $scope.LeaderOfHeadPosition = $scope.CurrentEmployee.Language == 'EN' ? response.data.HeadUnitPositionEN : response.data.HeadUnitPositionTH;
                    }
                    console.log('success getOrganizationLeader.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getOrganizationLeader.');
                });
            }

            //Generate list of line manager
            $scope.getRootOrgByHeadOrg = function () {
                getRootOrgByHeadOrg();
            };

            function getRootOrgByHeadOrg() {
                $scope.loader.enable = true;
                var oRequestParameter = {
                    InputParameter: {
                        UNIT_ID: $scope.document.Additional.OrganizationData.OrganizationID ? $scope.document.Additional.OrganizationData.OrganizationID : '',
                        LEVEL_ID: $scope.document.Additional.OrganizationData.OrganizationLevelID ? $scope.document.Additional.OrganizationData.OrganizationLevelID: '',
                        HEADORG_ID: $scope.document.Additional.OrganizationData.HeadOfOrganizationID ? $scope.document.Additional.OrganizationData.HeadOfOrganizationID : '',
                        BeginDate: getDateFormatToService(new Date()),
                        EndDate: getDateFormatToService(new Date()),
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/getOrganizationLevelChart/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrgLevelByCriteria = response.data;
                    $scope.loader.enable = false;
                    console.log('success loadOrgLevelByCriteria.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error loadOrgLevelByCriteria.');
                });

            }

            //Genarate org sibling with same head org
            $scope.siblingOrganization = function () {
                $scope.loader.enable = true;
                var oRequestParameter = {
                    InputParameter: {
                        ORG_ID: $scope.document.Additional.OrganizationData.HeadOfOrganizationID,
                        BeginDate: getDateFormatToService($scope.document.Additional.OrganizationData.BeginDate),
                        EndDate: getDateFormatToService($scope.document.Additional.OrganizationData.EndDate)
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/getPriorityListByOrganizationID/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.unitPriorityList = response.data.length > 0 ? response.data : []; 
                    SortingPriority();
                    $scope.loader.enable = false;
                    console.log('success siblingOrganization.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error siblingOrganization.');
                });
            };

            function SortingPriority() {
                var oShortName = $scope.CurrentEmployee.Language == 'EN' ? $scope.document.Additional.OrganizationData.ShortNameEN : $scope.document.Additional.OrganizationData.ShortNameTH;
                var oFullName = $scope.CurrentEmployee.Language == 'EN' ? $scope.document.Additional.OrganizationData.FullNameEN : $scope.document.Additional.OrganizationData.FullNameTH;
                var currentDetail = {
                    SEQ: parseInt($scope.document.Additional.OrganizationData.OrderNo),
                    PRIORITY_DETAIL: oFullName + '(' + $scope.document.Additional.OrganizationData.OrganizationID + ':' + oShortName + ')',
                    UNIT_CODE: $scope.document.Additional.OrganizationData.OrganizationID
                };
                var spliceIndex = $scope.unitPriorityList.findIndex(data => data.UNIT_CODE == currentDetail.UNIT_CODE);
                if (spliceIndex > -1)
                    $scope.unitPriorityList.splice(spliceIndex, 1);
                $scope.unitPriorityList.push(currentDetail);
                //Sorting
                $scope.unitPriorityList.sort((b, a) => {
                    if (a.SEQ > b.SEQ) {
                        if (a.UNIT_CODE > b.UNIT_CODE) {
                            return 1
                        } else {
                            return -1
                        }

                    } else {
                        return -1
                    }
                });
                $scope.unitPriorityList.sort((b, a) => (a.SEQ < b.SEQ) ? 1 : -1);
                $scope.unitPriorityList.forEach((element, index) => {
                    element.SEQ = index + 1
                });
            }

            function getDateFormatToService(date) {
                return $filter('date')(date, 'yyyy-MM-dd');
            }

            function getDateFormat(date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

        }]);
})();