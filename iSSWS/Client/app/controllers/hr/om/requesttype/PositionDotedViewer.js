﻿(function () {
    angular.module('DESS')
        .controller('PositionDotedViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            $scope.Language = oEmployeeData.Language;
            $scope.Textcategory = "OM_POSITION";
            $scope.PositionDotedDetail = $scope.document.Additional.PositionDotedData.PositionDotedDetail;
            $scope.PositionDotedDetailOld = $scope.document.Additional.PositionDotedData_OLD.PositionDotedDetail;
            $scope.UnitCode = $scope.document.Additional.PositionDotedData.HeadData_UnitCode;
            $scope.HeadPosCode = $scope.document.Additional.PositionDotedData.HeadData_HeadPosCode
            $scope.HeadStartDate = $scope.document.Additional.PositionDotedData.HeadData_StartDate
            $scope.HeadEndDate = $scope.document.Additional.PositionDotedData.HeadData_EndDate
            //SetData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.SetData = function () {
                //Do something ...

            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset after take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };
          
            $scope.GetOrgUnit = function () {
                var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "LevelSelected": '',
                        "BeginDate": "1999-01-01",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UnitList = response.data.oDDL;

                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.GetOrgUnit();

            $scope.GetBand = function () {
                var URL = CONFIG.SERVER + 'HROM/GetBandDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "BeginDate": "1999-01-01",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.BandList = response.data.oDDL;

                    console.log('success GetBand', response);
                }, function errorCallback(response) {
                    console.log('error GetBand', response);
                });
            }
            $scope.GetBand();

            $scope.GetPositionDotedData = function () {
                if ($scope.UnitCode != null && $scope.HeadPosCode != null &&
                    $scope.UnitCode != '' && $scope.HeadPosCode != '') {
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDotedDataContent';
                    var oRequestParameter = {
                        InputParameter: {
                            "UnitSelected": $scope.UnitCode,
                            "PositionSelected": $scope.HeadPosCode,
                            "BeginDate": $scope.HeadStartDate,
                            "EndDate": $scope.HeadEndDate
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (response.data.oData.length > 0) {
                            $scope.PositionData = response.data.oData[0];
                        }

                        console.log('success GetPositionData', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetPositionData', response);
                    });
                }
            };
            $scope.GetPositionDotedData();
            //#region date format
            function filterTextDescription(array, attr, value, objReturn) {
                $scope.isData = false;
                if (value == "")
                    return "";
                else {
                    if (array != null && array != undefined) {
                        for (var i = 0; i < array.length; i += 1) {
                            if (array[i][attr] == value) {
                                $scope.isData = true;
                                return array[i][objReturn];
                            }
                        }
                    }

                }
            }
            //#endregion
            //#region date format

            $scope.getDateFormat = function (date) {
                return getDateFormat(date);
            }
            function getDateFormat(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

            $scope.getDateFormatData = function (date) {
                return getDateFormatData(date);
            }

            function getDateFormatData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            $scope.getDateFormatURL = function (date) {
                return getDateFormatURL(date);
            }
            function getDateFormatURL(date) { return $filter('date')(date, 'yyyyMMdd'); }

            $scope.getDateFormatSave = function (date) {
                return getDateFormatSave(date);
            }
            function getDateFormatSave(date) { return $filter('date')(date, 'yyyy-MM-ddT00:00:00'); }
            //#endregion

        }]);
})();