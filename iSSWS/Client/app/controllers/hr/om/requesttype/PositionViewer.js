﻿(function () {
    angular.module('DESS')
        .controller('PositionViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            $scope.Textcategory = "OM_POSITION";
            $scope.PositionData = $scope.document.Additional.PositionData;
            $scope.PositionDataOld = $scope.document.Additional.PositionData_OLD;

            $scope.init = function () {
          
            }

            //SetData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.SetData = function () {
          
                //Do something ...
                //    $scope.UnitName = filterTextDescription($scope.UnitList, 'DLL_VALUE', $scope.PositionDetail.UnitCode, 'DLL_DATA');

            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset after take any action
            $scope.ChildAction.LoadData = function () {
                if ($scope.PositionDataOld.StartDate == '') {
                    $scope.PositionDataOld.StartDate = '0001-01-01T00:00:00';
                } 

                if ($scope.PositionDataOld.EndDate == '') {
                    $scope.PositionDataOld.EndDate = '0001-01-01T00:00:00';
                } 
            };

            if ($scope.PositionDataOld.StartDate == '0001-01-01T00:00:00') {
                $scope.PositionDataOld.StartDate = '';
            } 
            if ($scope.PositionDataOld.EndDate == '0001-01-01T00:00:00') {
                $scope.PositionDataOld.EndDate = '';
            } 

            $scope.GetOrgUnit = function () {
                var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "LevelSelected": '',
                        "BeginDate": "9999-12-31",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UnitList = response.data.oDDL;

                    $scope.UnitCodeText = filterTextDescription($scope.UnitList, 'DLL_VALUE', $scope.PositionData.UnitCode, 'DLL_DATA');
                    $scope.UnitCodeTextOld = filterTextDescription($scope.UnitList, 'DLL_VALUE', $scope.PositionDataOld.UnitCode, 'DLL_DATA');

                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.GetOrgUnit();

            $scope.GetBand = function () {
                var URL = CONFIG.SERVER + 'HROM/GetBandDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "BeginDate": "9999-12-31",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.BandList = response.data.oDDL;

                    $scope.BandText = filterTextDescription($scope.BandList, 'DLL_VALUE', $scope.PositionData.BandValue, 'DLL_DATA');
                    $scope.BandTextOld = filterTextDescription($scope.BandList, 'DLL_VALUE', $scope.PositionDataOld.BandValue, 'DLL_DATA');

                    console.log('success GetBand', response);
                }, function errorCallback(response) {
                    console.log('error GetBand', response);
                });
            }
            $scope.GetBand();

            $scope.GetPositionDataDetail = function () {
                if ($scope.PositionData.UnitCode != null && $scope.PositionData.StartDate != null && $scope.PositionData.EndDate != null &&
                    $scope.PositionData.UnitCode != '' && $scope.PositionData.StartDate != '' && $scope.PositionData.EndDate != '') {
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDataDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "UnitCode": $scope.PositionData.UnitCode,
                            "BeginDate": $scope.IsNewID == true ? "1999-01-01" : getDateFormatData($scope.PositionData.StartDate),
                            "EndDate": $scope.IsNewID == true ? "1999-01-01" : getDateFormatData($scope.PositionData.EndDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionDataDetail = response.data.oData;
                        console.log('success GetPositionDataDetail', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetPositionDataDetail', response);
                    });
                }
            }
            $scope.GetPositionDataDetail();

            //#region filter text
            function filterTextDescription(array, attr, value, objReturn) {
                $scope.isData = false;
                if (value == "")
                    return "";
                else {
                    if (array != null && array != undefined) {
                        for (var i = 0; i < array.length; i += 1) {
                            if (array[i][attr] == value) {
                                $scope.isData = true;
                                return array[i][objReturn];
                            }
                        }
                    }

                }
            }
            //#endregion
            //#region date format

            $scope.getDateFormat = function (date) {
                return getDateFormat(date);
            }
            function getDateFormat(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

            $scope.getDateTimeFormate = function (date) {
                return getDateTimeFormate(date);
            }
            function getDateTimeFormate(date) { return $filter('date')(date, 'dd/MM/yyyy HH:mm'); }

            $scope.getDateFormatData = function (date) {
                return getDateFormatData(date);
            }
            function getDateFormatData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            $scope.getDateFormatURL = function (date) {
                return getDateFormatURL(date);
            }
            function getDateFormatURL(date) { return $filter('date')(date, 'yyyyMMdd'); }

            $scope.getDateFormatSave = function (date) {
                return getDateFormatSave(date);
            }
            function getDateFormatSave(date) { return $filter('date')(date, 'yyyy-MM-ddT00:00:00'); }
            //#endregion

        }]);
})();