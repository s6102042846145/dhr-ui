﻿(function () {
    angular.module('DESS')
        .controller('OrganizationInactiveEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentLanguage = $scope.CurrentEmployee.Language;
            $scope.Textcategory = 'OM_UNIT_INACTIVE';
            $scope.OrganizationUnitDataList = [];
            $scope.LastEffectiveDate = '';

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                $scope.LastEffectiveDate = new Date($scope.document.Additional.LastEffectiveDate);
                $scope.OrganizationID = angular.copy($scope.document.Additional.OrganizationID);
                getOrganizationList();

                $scope.loader.enable = false;
                $scope.loader.enable = false;
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.Additional.LastEffectiveDate = new Date($scope.LastEffectiveDate);
                $scope.document.Additional.LastEffectiveDate.setHours(7);
            };

            $scope.ChildAction.ValidateData = function () {
                if (!$scope.validateForm.$invalid) {
                    $scope.validateResult = true;
                } else {
                    $scope.validateResult = false;
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text[$scope.Textcategory].EFFECTIVEDATE_INVALID)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );
                }
                return $scope.validateResult;
            };
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 
            function getOrganizationList() {
                var oRequestParameter = {
                    InputParameter: {
                        UnitSelected: $scope.OrganizationID,
                        LevelSelected: '',
                        BeginDate: '1900-01-01',
                        EndDate: '9999-12-31'
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HROM/GetOrganizationDataContent/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrganizationList = response.data;
                    for (i = 0; i < $scope.OrganizationList.length; i++) {
                        $scope.OrganizationList[i].BeginDate = getDateFormat(new Date($scope.OrganizationList[i].BeginDate));
                        $scope.OrganizationList[i].EndDate = getDateFormat(new Date($scope.OrganizationList[i].EndDate));
                    }
                    console.log('success Cost center ddl.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error Cost center ddl.');
                });
            }

            function getDateFormat(date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }
            $scope.getDateFormat = function (objDate) {
                getDateFormat(objDate);
            };
        }]);
})();