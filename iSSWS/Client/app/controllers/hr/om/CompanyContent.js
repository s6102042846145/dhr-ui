﻿(function () {
    angular.module('DESS')
        .controller('CompanyContent', ['$scope', '$http', '$routeParams', '$location', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window',
            function ($scope, $http, $routeParams, $location, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
                var oRequestData = $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);
                $scope.CurrentAsOfDate = new Date();
                $scope.AsOfDate = $scope.CurrentAsOfDate;
                $scope.loader.enable = false;


                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '4', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion

                //#region date format
                $scope.getDateFormate = function (date) {
                    return getDateFormate(date);
                }
                function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

                $scope.getDateFormateData = function (date) {
                    return getDateFormateData(date);
                }

                function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
                //#endregion

                //#endregion
            }]);
})();