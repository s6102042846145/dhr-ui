﻿(function () {
    angular.module('DESS')
        .controller('PersonalConfidentialContentController', ['$scope', '$http', '$routeParams', '$location', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window',
            function ($scope, $http, $routeParams, $location, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
                var oRequestData = $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);
                $scope.CurrentAsOfDate = new Date();
                $scope.AsOfDate = $scope.CurrentAsOfDate;
                $scope.loader.enable = false;
                $scope.profile = getToken(CONFIG.USER);
                $scope.LevelSelected = [];
                $scope.UnitSelected = [];
                $scope.PositionSelected = [];
                $scope.EmployeeData = [];
                $scope.isLastRecord = false; // flag สำหรับดุว่าข้อมูลหน่วยงานที่เลือก เป็นอันสุดท้ายรึเปล่า
                $scope.IsAttachFile = false;
                $scope.Language = oEmployeeData.Language;






                /******************* variable start ********************/
                $scope.profile = (getToken(CONFIG.USER));
                $scope.UserImg = $scope.profile.ImageUrl + "?" + new Date().getTime();
                $scope.Textcategory = "PA_EDUCATION_MANAGEMENT";
                $scope.content.isShowHeader = true;
                $scope.PAConfigPersonalTitleFormat = [];
                $scope.Name_EmpID = $scope.requesterData.EmployeeID + ' ' + $scope.requesterData.Name;
                $scope.FirstTabIndex = '';
                $scope.EmpCode = '';
                $scope.EmpNameTH = '';
                $scope.PosCode = '';
                $scope.PositionTextTH = '';
                $scope.UnitCode = '';
                $scope.UnitTextTh = '';
                $scope.BandValue = '';
                $scope.BandTextTH = '';
                $scope.UnitLevelValue = '';
                $scope.UnitLevelTextTH = '';

                $scope.StartDateText = null;
                $scope.EndDateText = null;
                $scope.EducationLevelText = '';
                $scope.CertificateText = '';
                $scope.MajorText = '';
                $scope.MinorText = '';
                $scope.InstitutionText = '';
                $scope.CountryText = '';
                $scope.HonorText = '';
                $scope.GradeText = '';











                $scope.init = function () {
                    init();
                    GetAdminSelectEmp();
                    $scope.CompanyCode = oEmployeeData.CompanyCode;
                }
                // #endregion ******************* listener end ********************/

                // #region ******************* action start ********************/
                function init() {
                    getPAConfiguration();
                    //$scope.getAllTextDescription();
                    $scope.GetOrgLevel();
                    $scope.GetOrgUnit();
                    $scope.GetSubtype();


                    //CheckDataIsWorking('2201');
                    //CheckDataIsWorking('2202');
                    //CheckDataIsWorking('2203');
                    //CheckDataIsWorking('2204');
                    //CheckDataIsWorking('2205');
                    //CheckDataIsWorking('2206');
                    //CheckDataIsWorking('2207');
                    //CheckDataIsWorking('2208');
                    // $scope.GetPosition();

                    //#region =============== dam 
                    $scope.getDrpPeriod();


                    //#endregion
                }




                //#endregion

                //#region date format

                $scope.getDateFormat = function (date) {
                    return $filter('date')(date, 'dd/MM/yyyy');
                }

                $scope.getDateFormate = function (date) {
                    return getDateFormate(date);
                }
                function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

                $scope.getDateFormateData = function (date) {
                    return getDateFormateData(date);
                }

                function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
                //#endregion

                //#endregion


                function getPAConfiguration() {
                    $scope.loader.enable = true;
                    var oEmployeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.TabDataConfig = response.data.TabPAConfidential;




                        $scope.PersonalConfig = response.data.Personal;
                        $scope.AddressConfig = response.data.Address;
                        $scope.BankConfig = response.data.Bank;
                        $scope.FamilyConfig = response.data.Family;
                        $scope.CommuConfig = response.data.Communication;
                        $scope.EducationConfig = response.data.Education;
                        $scope.TaxAllowanceConfig = response.data.TaxAllowance;
                        $scope.TabContentConfig = response.data.TabContent;
                        $scope.DocumentConfig = response.data.Document;
                        $scope.AcademicConfig = response.data.Academic;
                        //check Personaldata Title Config
                        for (var i = 0; i < $scope.PersonalConfig.length; i++) {
                            switch ($scope.PersonalConfig[i].FieldName) {
                                case "TitleID":
                                case "AcademicTitle":
                                case "MedicalTitle":
                                case "MilitaryTitle":
                                case "PrefixName":
                                case "SecondTitle"://DR.
                                    if (!$scope.PersonalConfig[i].IsVisible) {
                                        $scope.PAConfigPersonalTitleFormat.push($scope.PersonalConfig[i].FieldName);
                                    }
                                    break;
                                default:
                            }
                        }
                        if ($scope.TabDataConfig.length > 0) {
                            var FirstTabIndex = $scope.TabDataConfig[0].FieldName;
                            $scope.getDataByTab(FirstTabIndex);
                            $scope.FirstTabIndex = FirstTabIndex;
                        }
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }


                //$scope.Text = null;
                //$scope.RequestorText = null;
                //$scope.getAllTextDescription = function () {
                //    $scope.Text = null;
                //    var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                //    var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //         Success
                //        if (response.data != null) {
                //            $scope.Text = response.data;
                //            $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
                //        }
                //    }, function errorCallback(response) {
                //         Error
                //        $scope.Text = null;
                //        console.log('error MainController TextDescription.', response);
                //    });
                //};
                //$scope.getAllTextDescription();


                $scope.getDataByTab = function (TapClick) {
                    switch (TapClick) {
                        case "TabQualiEducation":

                            break;
                        case "TabQualiWorkHistory":

                            break;
                        case "TabQualiTraininghistory ":

                            break;
                        case "TabQualiresearchMember":

                            break;
                        case "TabQualiAttachment":

                            break;

                        default:
                    }
                };

                $scope.GetOrgLevel = function () {
                    var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.LevelList = response.data;


                    }, function errorCallback(response) {

                    });
                }
                $scope.GetOrgLevel();

                $scope.GetOrgUnit = function () {
                    $scope.levelSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';

                    for (var i = 0; i < $scope.LevelSelected.length; i++) {
                        if ($scope.LevelSelected.length > 1) {
                            $scope.levelSearch += $scope.LevelSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.levelSearch = $scope.LevelSelected[0].DLL_VALUE;
                        }
                    }

                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.UnitList = response.data.oDDL;
                        $scope.GetEmployee();

                    }, function errorCallback(response) {

                    });
                }
                $scope.GetOrgUnit();

                $scope.GetPosition = function (unit) {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDDL';

                    for (var i = 0; i < $scope.UnitSelected.length; i++) {

                        if ($scope.UnitSelected.length > 1) {
                            $scope.unitSearch += $scope.UnitSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.unitSearch = $scope.UnitSelected[0].DLL_VALUE;
                        }
                    }



                    var oRequestParameter = {
                        InputParameter: {
                            "UnitSelected": $scope.unitSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionList = response.data.oDDL;;
                        $scope.GetEmployee();

                    }, function errorCallback(response) {

                    });
                }
                $scope.GetPosition();

                //Start GetFile
                //END GetFile
                function getRequestTypeFileSet(requestType, EmployeeID) {
                    if (EmployeeID != null || EmployeeID != '' || EmployeeID != undefined) {
                        $scope.requesterData.EmployeeID = EmployeeID;
                    }
                    var oEmployeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { "EmployeeID": "", "RequestTypeID": requestType, "RequestSubType": "" }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'Workflow/GetRequestTypeFileSet/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success

                        if (requestType == '2201') {
                            //$scope.FileAttachment2201List = response.data;
                            $scope.FileAttachment2201List = $filter('orderBy')(response.data, ['-RequestNo']);
                            if (response.data.length > 0) {
                                $scope.IsAttachFile = true;
                            }

                        }
                        if (requestType == '2203') {
                            //$scope.FileAttachment2203List = response.data;
                            $scope.FileAttachment2203List = $filter('orderBy')(response.data, ['-RequestNo']);
                            if (response.data.length > 0) {
                                $scope.IsAttachFile = true;
                            }

                        }
                        if (requestType == '2205') {
                            // $scope.FileAttachment2205List = response.data;
                            $scope.FileAttachment2205List = $filter('orderBy')(response.data, ['-RequestNo']);
                            if (response.data.length > 0) {
                                $scope.IsAttachFile = true;
                            }

                        }
                        if (requestType == '2207') {
                            // $scope.FileAttachment2207List = response.data;
                            $scope.FileAttachment2207List = $filter('orderBy')(response.data, ['-RequestNo']);
                            if (response.data.length > 0) {
                                $scope.IsAttachFile = true;
                            }

                        }


                    }, function errorCallback(response) {
                        // Error

                    });
                }




                $scope.GetEmployee = function () {
                    $scope.PositionSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/GetEducationEmployeeDLL';

                    for (var i = 0; i < $scope.PositionSelected.length; i++) {
                        if ($scope.PositionSelected.length > 1) {
                            $scope.PositionSearch += $scope.PositionSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.PositionSearch = $scope.PositionSelected[0].DLL_VALUE;
                        }
                    }




                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'PAOR',
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31",
                            "UnitLevelValue": $scope.levelSearch,
                            "UnitCode": $scope.unitSearch,
                            "PostCode": $scope.PositionSearch
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeList = response.data.oDDL;;


                    }, function errorCallback(response) {

                    });
                }
                $scope.GetEmployee();

                $scope.GetSubtype = function () {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/SubtypeDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'OMDL',
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31",
                            "SUBTYPE": 'SUBTYPE_CODE'
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.SubTypeList = response.data.oDDL;

                    }, function errorCallback(response) {

                    });
                }





                $scope.SearchEmployeeData = function () {
                    $scope.loader.enable = true;
                    $scope.EmployeeSearch = '';
                    for (var i = 0; i < $scope.EmployeeSelected.length; i++) {
                        if ($scope.EmployeeSelected.length > 1) {
                            $scope.EmployeeSearch += $scope.EmployeeSelected[i].DLL_VALUE + ':';
                        } else {
                            $scope.EmployeeSearch = $scope.EmployeeSelected[0].DLL_VALUE;
                        }
                    }
                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "UnitSelected": $scope.unitSearch,
                            "PositionSelected": $scope.PositionSearch,
                            "EmpCode": ($scope.EmployeeSearch) ? $scope.EmployeeSearch : "",
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeDataList = response.data;

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;

                    });
                };
                $scope.SetIsAllPeriod = function () {
                    if ($scope.AllPeriod) {
                        $scope.AsOfDate = '';
                    } else {
                        $scope.AsOfDate = angular.copy($scope.CurrentAsOfDate);
                    }
                }
                $scope.SetCurrentTab = function (currentTab) {
                    $scope.CurrentTab = currentTab;
                    $scope.ResetDataContent();
                }




                $scope.ViewDetailData = function (data) {

                    $scope.loader.enable = true;
                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": "",
                            "UnitSelected": "",
                            "PositionSelected": data.PosCode,
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        SetLastAdminSelectEmp(response.data[0]);
                        //GetPersonalEducationDetailById(response.data[0]);
                        //GetPersonalWorkHistoryDetailById(response.data[0]);

                        GetPersonalTrainningDetailById(response.data[0]);
                        GetPersonalAcedemicworktypeDetailById(response.data[0]);
                        $scope.ViewEmployeeList = response.data;
                        SetEmployeeDetail(response.data[0]);
                        $scope.loader.enable = false;

                        GetPersonalInfDetailById(response.data[0]);
                        GetDHRPAEvaluationInfByEmpCode(response.data[0]);
                        GetDHRPAMedicalCheckupByEmpCode(response.data[0]);

                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };


                function SetLastAdminSelectEmp(data) {
                    var URL = CONFIG.SERVER + 'HRPA/SetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "SET",
                            "EmpCode": data.EmpCode,
                            "PosCode": data.PosCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                    });
                }


                function GetAdminSelectEmp() {
                    var URL = CONFIG.SERVER + 'HRPA/GetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "GET",
                            "EmpCode": "",
                            "PosCode": "",
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmpCode = response.data[0].EmpCode;
                        getRequestTypeFileSet('2201', $scope.EmpCode);
                        getRequestTypeFileSet('2203', $scope.EmpCode);
                        getRequestTypeFileSet('2205', $scope.EmpCode);
                        getRequestTypeFileSet('2207', $scope.EmpCode);
                        $scope.ViewDetailData(response.data[0]);
                    });
                }




                

                

                function GetPersonalTrainningDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetTrainingHistory';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.TrainingList = response.data;


                    });
                }


                function GetPersonalAcedemicworktypeDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetAcedemicworkmember';
                    var oRequestParameter = {
                        InputParameter: {
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.Acedemicworkmemberlist = response.data;
                        $scope.Acedemicworkmembertmplist = response.data;


                    });
                }
                //Start GetDateFromTab



                //#region ===================================== view data ===================================

                //// tab 1
                $scope.ViewDetail = function (data) {
                    $scope.dataList = angular.copy(data);
                    $scope.StartDateText = data.BeginDate;
                    $scope.EndDateText = data.EndDate;
                    $scope.BirthDateText = data.BirthDate;
                    $scope.IDCardText = data.IDCard;
                    $scope.BloodTypeText = data.BloodType;
                }

                //// tab 2
                $scope.ViewEvaluationInfDetail = function (data) {
                    $scope.evaluationTime = data.evaluationTime;
                    $scope.period = data.period;
                    $scope.score = data.score;
                    $scope.average = data.average;
                    $scope.StartDateText = ConvertDate(data.startDate);
                    $scope.EndDateText = ConvertDate(data.endDate);
                    $scope.upSalaryPercentage = data.upSalaryPercentage;
                    $scope.variableBonus = data.variableBonus;
                    $scope.increaseSalary = data.increaseSalary;
                    $scope.compenAmount = data.compenAmount;
                }


                //#endregion 
                


               


                $scope.onSubtypeChange = function () {
                    $scope.subtype;
                    if ($scope.subtype == 'All') {
                        $scope.Acedemicworkmemberlist = $scope.Acedemicworkmembertmplist;
                    } else {
                        $scope.Acedemicworkmemberlist = $scope.Acedemicworkmembertmplist;
                        $scope.Acedemicworkmemberlist = $filter('filter')($scope.Acedemicworkmemberlist, { SubtypeValue: $scope.subtype });
                    }
                };





                function SetEmployeeDetail(data) {

                    $scope.EmpCode = data.EmpCode;
                    $scope.PosCode = data.PosCode;
                    $scope.UnitCode = data.UnitCode;
                    $scope.BandValue = data.BandValue;
                    $scope.EmpCodeText = '(' + data.EmpCode + ')';
                    $scope.PosCodeText = '(' + data.PosCode + ')';
                    $scope.UnitCodeText = '(' + data.UnitCode + ')';
                    $scope.BandValueText = '(' + data.BandValue + ')';
                    $scope.UnitLevelValueText = '(' + data.UnitLevelValue + ')';
                    if ($scope.Language == 'TH') {
                        $scope.EmpNameTH = data.EmpNameTH;
                        $scope.PositionTextTH = data.PositionTextTH;
                        $scope.UnitTextTh = data.UnitTextTh;
                        $scope.BandTextTH = data.BandTextTH;
                        $scope.UnitLevelTextTH = data.UnitLevelTextTH;
                    } else {
                        $scope.EmpNameTH = data.EmpNameEN;
                        $scope.PositionTextTH = data.PositionTextEN;
                        $scope.UnitTextTh = data.UnitTextEN;
                        $scope.BandTextTH = data.BandTextEN;
                        $scope.UnitLevelTextTH = data.UnitLevelTextEN;
                    }
                    getRequestTypeFileSet('2201', data.EmpCode);
                    getRequestTypeFileSet('2203', data.EmpCode);
                    getRequestTypeFileSet('2205', data.EmpCode);
                    getRequestTypeFileSet('2207', data.EmpCode);




                }


                
                $scope.ResetDataContent = function () {
                    $scope.AllPeriod = false;
                    $scope.CurrentAsOfDate = new Date();
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                    $scope.PositionSelected = [];
                    $scope.EmployeeDataList = [];
                    $scope.EmployeeList = [];
                    $scope.levelSearch = '';
                    $scope.unitSearch = '';
                    $scope.positionSearch = '';
                    $scope.EmployeeSelected = [];
                    $scope.SolidMasterDataList = [];
                    $scope.DottedMasterDataList = [];
                    $scope.GetEmployee();

                };
                



                //$scope.ViewDetailData = function (data) {
                //    //get data from API and view in modal
                //    //#region HROMPOSITION_EDIT_xxxxxxxx
                //    $scope.PositionInfoEditRequest = [];
                //    var dataCategory = 'HROMPOSITION_EDIT_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocEdit, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoEditRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_DELETE_xxxxxxxx
                //    $scope.PositionInfoDeleteRequest = [];
                //    var dataCategory = 'HROMPOSITION_DELETE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocDelete, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoDeleteRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_PERDELETE_xxxxxxxx
                //    $scope.PositionInfoPerDeleteRequest = [];
                //    var dataCategory = 'HROMPOSITION_PERDELETE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocPerDelete, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoPerDeleteRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_INACTIVE_xxxxxxxx
                //    $scope.PositionInfoInactiveRequest = [];
                //    var dataCategory = 'HROMPOSITION_INACTIVE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocInactive, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoInactiveRequest.push(result[0]);
                //    }
                //    //#endregion



                //    $scope.PositionDetail = angular.copy(data);
                //    $scope.UnitName = filterTextDescription($scope.UnitList, 'DLL_VALUE', $scope.PositionDetail.UnitCode, 'DLL_DATA');
                //};


                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };












                //#region ============================== GetData
                function GetPersonalInfDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAPrivateInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.DataList = response.data;
                        $scope.StartDateText = response.data[0].BeginDate;
                        $scope.EndDateText = response.data[0].EndDate;
                        $scope.BirthDateText = response.data[0].BirthDate;
                        $scope.IDCardText = response.data[0].IDCard;
                        $scope.BloodTypeText = response.data[0].BloodType;
                    });
                }

                //table 2
                function GetDHRPAEvaluationInfByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAEvaluationInfByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "1900-01-01" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EvaluationList = response.data;
                        //console.log("GetDHRPAEvaluationInfByEmpCode", $scope.EvaluationList)

                    });
                }
                //table 3
                function GetDHRPAMedicalCheckupByEmpCode(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetDHRPAMedicalCheckupByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "1900-01-01" : getDateFormateData(data.StartDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData(data.EndDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.MedicalCheckupList = response.data;
                    });
                }

                //#endregion













                //#region  Dropdown 
                $scope.dDataType = "OMDL";
                $scope.dStartDate = "1900-01-01";
                $scope.dEndDate = "9999-12-31";

                $scope.getDrpPeriod = function () {
                    var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                    var oRequestParameter = {
                        InputParameter: {
                            "Type": "PAME",
                            "BeginDate": $scope.dStartDate,
                            "EndDate": $scope.dEndDate,
                            "param1": "581005",
                            "param2": oEmployeeData.CompanyCode,
                            "param3": ""
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PeriodList = response.data.oDDL;
                    }, function errorCallback(response) {
                    });
                }
                //#endregion


                //#region Paging by Dam
                $scope.paginMedicalCheckup = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.paginPunishment = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }




                $scope.genDatasInfo = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }



                $scope.genDatasMedicalCheckup = function (obj) {
                    var datas = obj;
                    setNumOfPageMedicalCheckup(datas);
                    var res = [];
                    if ($scope.paginMedicalCheckup.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.paginMedicalCheckup.currentPage) - 1) * parseInt($scope.paginMedicalCheckup.itemPerPage);
                            var to = from + parseInt($scope.paginMedicalCheckup.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }
                    return res;
                }
                var setNumOfPageMedicalCheckup = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.paginMedicalCheckup.numPage = Math.ceil(totalItems / $scope.paginMedicalCheckup.itemPerPage);
                    if ($scope.paginMedicalCheckup.numPage <= 0) $scope.paginMedicalCheckup.numPage = 1;
                    if ($scope.paginMedicalCheckup.currentPage > $scope.paginMedicalCheckup.numPage) $scope.paginMedicalCheckup.currentPage = 1;
                }
                $scope.changePageMedicalCheckup = function (targetPage) {
                    $scope.paginMedicalCheckup.currentPage = angular.copy(targetPage);
                }


                $scope.genDatasPunishment = function (obj) {
                    var datas = obj;
                    setNumOfPagePunishment(datas);
                    var res = [];
                    if ($scope.paginPunishment.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.paginPunishment.currentPage) - 1) * parseInt($scope.paginPunishment.itemPerPage);
                            var to = from + parseInt($scope.paginPunishment.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }
                    return res;
                }
                var setNumOfPagePunishment = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.paginPunishment.numPage = Math.ceil(totalItems / $scope.paginPunishment.itemPerPage);
                    if ($scope.paginPunishment.numPage <= 0) $scope.paginPunishment.numPage = 1;
                    if ($scope.paginPunishment.currentPage > $scope.paginPunishment.numPage) $scope.paginPunishment.currentPage = 1;
                }
                $scope.changePagePunishment = function (targetPage) {
                    $scope.paginPunishment.currentPage = angular.copy(targetPage);
                }
                $scope.getPagesPunishment = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.paginPunishment.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }











                //#endregion






                //#region Paging
                $scope.paginEducation = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.paginWorkHist = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.paginTrainning = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.paginmember = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.paginEmployee = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.genData = function (obj) {

                    var datas = obj;
                    setNumOfPageEducation(datas);
                    var res = [];
                    if ($scope.paginEducation.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.paginEducation.currentPage) - 1) * parseInt($scope.paginEducation.itemPerPage);
                            var to = from + parseInt($scope.paginEducation.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);

                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                $scope.genDatasWorkHist = function (obj) {
                    var datas = obj;
                    setNumOfPageWorkHist(datas);
                    var res = [];
                    if ($scope.paginWorkHist.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.paginWorkHist.currentPage) - 1) * parseInt($scope.paginWorkHist.itemPerPage);
                            var to = from + parseInt($scope.paginWorkHist.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                $scope.genDatasTrainning = function (obj) {
                    var datas = obj;
                    setNumOfPageTrainning(datas);
                    var res = [];
                    if ($scope.paginTrainning.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.paginTrainning.currentPage) - 1) * parseInt($scope.paginTrainning.itemPerPage);
                            var to = from + parseInt($scope.paginTrainning.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                $scope.genDatasmember = function (obj) {
                    var datas = obj;
                    setNumOfPagemember(datas);
                    var res = [];
                    if ($scope.paginmember.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.paginmember.currentPage) - 1) * parseInt($scope.paginmember.itemPerPage);
                            var to = from + parseInt($scope.paginmember.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                $scope.genDatasEmployee = function (obj) {
                    var datas = obj;
                    setNumOfPageEmployee(datas);
                    var res = [];
                    if ($scope.paginEmployee.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.paginEmployee.currentPage) - 1) * parseInt($scope.paginEmployee.itemPerPage);
                            var to = from + parseInt($scope.paginEmployee.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPageEducation = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.paginEducation.numPage = Math.ceil(totalItems / $scope.paginEducation.itemPerPage);
                    if ($scope.paginEducation.numPage <= 0) $scope.pagin.numPageEducation = 1;

                    if ($scope.paginEducation.currentPage > $scope.paginEducation.numPage) $scope.paginEducation.currentPage = 1;
                }
                var setNumOfPageWorkHist = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.paginWorkHist.numPage = Math.ceil(totalItems / $scope.paginWorkHist.itemPerPage);
                    if ($scope.paginWorkHist.numPage <= 0) $scope.paginWorkHist.numPage = 1;

                    if ($scope.paginWorkHist.currentPage > $scope.pagin.numPage) $scope.paginWorkHist.currentPage = 1;
                }
                var setNumOfPageTrainning = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.paginTrainning.numPage = Math.ceil(totalItems / $scope.paginTrainning.itemPerPage);
                    if ($scope.paginTrainning.numPage <= 0) $scope.paginTrainning.numPage = 1;
                    if ($scope.paginTrainning.currentPage > $scope.paginTrainning.numPage) $scope.paginTrainning.currentPage = 1;
                }
                var setNumOfPagemember = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.paginmember.numPage = Math.ceil(totalItems / $scope.paginmember.itemPerPage);
                    if ($scope.paginmember.numPage <= 0) $scope.paginmember.numPage = 1;

                    if ($scope.paginmember.currentPage > $scope.paginmember.numPage) $scope.paginmember.currentPage = 1;
                }
                var setNumOfPageEmployee = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.paginEmployee.numPage = Math.ceil(totalItems / $scope.paginEmployee.itemPerPage);
                    if ($scope.paginEmployee.numPage <= 0) $scope.paginEmployee.numPage = 1;

                    if ($scope.paginEmployee.currentPage > $scope.paginEmployee.numPage) $scope.paginEmployee.currentPage = 1;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePageEducation = function (targetPage) {
                    $scope.paginEducation.currentPage = angular.copy(targetPage);
                }
                $scope.changePageWorkHist = function (targetPage) {
                    $scope.paginWorkHist.currentPage = angular.copy(targetPage);
                }
                $scope.changePageTrainning = function (targetPage) {
                    $scope.paginTrainning.currentPage = angular.copy(targetPage);
                }
                $scope.changePagemember = function (targetPage) {
                    $scope.paginmember.currentPage = angular.copy(targetPage);
                }
                $scope.changePageEmployee = function (targetPage) {
                    $scope.paginEmployee.currentPage = angular.copy(targetPage);
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPagesEducation = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.paginEducation.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                $scope.getPagesWorkHist = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.paginWorkHist.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                $scope.getPagesTrainning = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.paginTrainning.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                $scope.getPagesmember = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.paginmember.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                $scope.getPagesEmployee = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.paginEmployee.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion




                $scope.directToRequest = function (requestTypeID, actionType, EmployeeID, PAID, datalist) {
                    var dataCategory = null;
                    var res = null;
                    if (actionType == 'EDIT' || actionType == 'CANCEL') {
                        if (requestTypeID == '2201' || requestTypeID == '2202') {
                            for (i = 0; i < $scope.MarkData2201.length; i++) {
                                var splitedDataCategory = $scope.MarkData2201[i].DataCategory.split('_');
                                if (splitedDataCategory[0] == 'PERSONALEDUCATION' && splitedDataCategory[5] == PAID) {
                                    dataCategory = $scope.MarkData2201[i].DataCategory;
                                    break;
                                }
                                else {
                                    continue;
                                }
                            }
                            for (i = 0; i < $scope.MarkData2202.length; i++) {
                                var splitedDataCategory = $scope.MarkData2202[i].DataCategory.split('_');
                                if (splitedDataCategory[0] == 'PERSONALEDUCATION' && splitedDataCategory[5] == PAID) {
                                    dataCategory = $scope.MarkData2202[i].DataCategory;
                                    break;
                                }
                                else {
                                    continue;
                                }
                            }
                        }
                        if (requestTypeID == '2203' || requestTypeID == '2204') {
                            dataCategory = "PERSONALWORKHISTORY_" + PAID;
                        }
                        if (requestTypeID == '2205' || requestTypeID == '2206') {
                            for (i = 0; i < $scope.MarkData2205.length; i++) {
                                var splitedDataCategory = $scope.MarkData2205[i].DataCategory.split('_');
                                if (splitedDataCategory[0] == 'PERSONALTRAINNING' && splitedDataCategory[2] == PAID) {
                                    dataCategory = $scope.MarkData2205[i].DataCategory;
                                    break;
                                }
                                else {
                                    continue;
                                }
                            }
                            for (i = 0; i < $scope.MarkData2206.length; i++) {
                                var splitedDataCategory = $scope.MarkData2206[i].DataCategory.split('_');
                                if (splitedDataCategory[0] == 'PERSONALTRAINNING' && splitedDataCategory[2] == PAID) {

                                    dataCategory = $scope.MarkData2206[i].DataCategory;

                                    break;
                                }
                                else {
                                    continue;
                                }
                            }

                        }
                        if (requestTypeID == '2207' || requestTypeID == '2208') {
                            for (i = 0; i < $scope.MarkData2207.length; i++) {
                                var splitedDataCategory = $scope.MarkData2207[i].DataCategory.split('_');
                                if (splitedDataCategory[0] == 'PERSONALACADEMICWORKMEMBER' && splitedDataCategory[4] == PAID) {
                                    dataCategory = $scope.MarkData2207[i].DataCategory;
                                    break;
                                }
                                else {
                                    continue;
                                }
                            }
                            for (i = 0; i < $scope.MarkData2208.length; i++) {
                                var splitedDataCategory = $scope.MarkData2208[i].DataCategory.split('_');
                                if (splitedDataCategory[0] == 'PERSONALACADEMICWORKMEMBER' && splitedDataCategory[4] == PAID) {

                                    dataCategory = $scope.MarkData2208[i].DataCategory;

                                    break;
                                }
                                else {
                                    continue;
                                }
                            }
                        }
                    }




                    $scope.requesterData.EmployeeID = EmployeeID;



                    var oEmployeeData = getToken(CONFIG.USER);
                    $scope.fromBeginDate = new Date();
                    $scope.fromEndDate = new Date(9999, 11, 31, 0, 0, 0, 0);
                    var PositionText = $scope.changeCharector($scope.PositionTextTH);
                    if (actionType == 'NEWID') {
                        if (requestTypeID == '2207') {
                            if ($scope.subtype == 'All' || $scope.subtype == undefined || $scope.subtype == '') {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text['SYSTEM'].WARNING)
                                        .textContent($scope.Text['PA_EDUCATION_MANAGEMENT'].REQUESTACADEMICWORKMEMBER)
                                        .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                );
                                return false;
                            } else {
                                $scope.CreateNew('2207', 'NEWID' + '|' + EmployeeID + '|' + $scope.subtype + '|' + '-' + '|' + PositionText);
                            }
                        }
                    } else if (actionType == 'EDIT') {
                        if (requestTypeID == '2201') {

                            $scope.FilterMarkData2202 = $filter('filter')($scope.MarkData2202, { DataCategory: dataCategory });
                            $scope.FilterMarkData2201 = $filter('filter')($scope.MarkData2201, { DataCategory: dataCategory });
                            if ($scope.FilterMarkData2201.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2201[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2202.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2202[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2201', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }


                        }
                        if (requestTypeID == '2203') {
                            $scope.FilterMarkData2204 = $filter('filter')($scope.MarkData2204, { DataCategory: dataCategory });
                            $scope.FilterMarkData2203 = $filter('filter')($scope.MarkData2203, { DataCategory: dataCategory });
                            if ($scope.FilterMarkData2203.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2203[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2204.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2204[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2203', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }

                        }
                        if (requestTypeID == '2205') {
                            $scope.FilterMarkData2206 = $filter('filter')($scope.MarkData2206, { DataCategory: dataCategory });
                            $scope.FilterMarkData2205 = $filter('filter')($scope.MarkData2205, { DataCategory: dataCategory });
                            if ($scope.FilterMarkData2205.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2205[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2206.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2206[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2205', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }

                        }
                        if (requestTypeID == '2207') {


                            $scope.FilterMarkData2208 = $filter('filter')($scope.MarkData2208, { DataCategory: dataCategory });
                            $scope.FilterMarkData2207 = $filter('filter')($scope.MarkData2207, { DataCategory: dataCategory });
                            if ($scope.FilterMarkData2207.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2207[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2208.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2208[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2207', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }

                        }
                    } else if (actionType == 'CANCEL') {
                        if (requestTypeID == '2202') {


                            $scope.FilterMarkData2202 = $filter('filter')($scope.MarkData2202, { DataCategory: dataCategory });
                            console.log('$scope.FilterMarkData2202', $scope.FilterMarkData2202);
                            $scope.FilterMarkData2201 = $filter('filter')($scope.MarkData2201, { DataCategory: dataCategory });
                            console.log('$scope.FilterMarkData2201', $scope.FilterMarkData2201);
                            if ($scope.FilterMarkData2201.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2201[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2202.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2202[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2202', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }
                        }
                        if (requestTypeID == '2204') {

                            $scope.FilterMarkData2204 = $filter('filter')($scope.MarkData2204, { DataCategory: dataCategory });
                            $scope.FilterMarkData2203 = $filter('filter')($scope.MarkData2203, { DataCategory: dataCategory });
                            if ($scope.FilterMarkData2203.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2203[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2204.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2204[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2204', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }
                        }
                        if (requestTypeID == '2206') {

                            $scope.FilterMarkData2206 = $filter('filter')($scope.MarkData2206, { DataCategory: dataCategory });
                            $scope.FilterMarkData2205 = $filter('filter')($scope.MarkData2205, { DataCategory: dataCategory });
                            if ($scope.FilterMarkData2205.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2205[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2206.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2206[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2206', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }
                        }
                        if (requestTypeID == '2208') {

                            $scope.FilterMarkData2208 = $filter('filter')($scope.MarkData2208, { DataCategory: dataCategory });
                            $scope.FilterMarkData2207 = $filter('filter')($scope.MarkData2207, { DataCategory: dataCategory });
                            if ($scope.FilterMarkData2207.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2207[0].RequestNo, $scope.profile.CompanyCode);
                            } else if ($scope.FilterMarkData2208.length > 0) {
                                $scope.ViewObjAnnounce($scope.FilterMarkData2208[0].RequestNo, $scope.profile.CompanyCode);
                            }
                            else {
                                $scope.CreateNew('2208', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID + '|' + PositionText);
                            }
                        }
                    }
                };

                function getRequesNoforCheckHrEditDetail(datacategory) {
                    var oEmployeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            "DataGetegory": datacategory
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HRPA/GetHrEditDetailByRequestNo/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success

                        $scope.CreatorList = response.data;
                        if (response.data.length > 0) {
                            $scope.CreatorNo = response.data[0].CreatorNo;
                            if (oEmployeeData.EmployeeID != response.data[0].CreatorNo) {

                            }
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error getPAConfiguration.');
                    });
                }

                $scope.getFileFromPath = function (attachment) {
                    /* direct */
                    if (angular.isDefined(attachment)) {
                        if (typeof cordova != 'undefined') {

                        } else if (attachment.FilePath) {
                            var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                            console.debug(path);
                            $window.open(path);
                        } else {
                            var path = CONFIG.SERVER + 'Client/files/' + attachment;
                            console.debug(path);
                            $window.open(path);

                        }
                    }
                    /* !direct */
                }


                $scope.changeCharector = function (data) {
                    /* direct */
                    var str = data.replace('/', ',');
                    return str;
                    /* !direct */
                }



                function CheckDataIsWorking(data) {

                    var URL = CONFIG.SERVER + 'HRPA/GetMarkHrEditList';
                    var oRequestParameter = {
                        InputParameter: {
                            "RequestType": data
                        }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (data == '2201') {
                            $scope.MarkData2201 = response.data;

                        } if (data == '2202') {
                            $scope.MarkData2202 = response.data;

                        } if (data == '2203') {
                            $scope.MarkData2203 = response.data;

                        } if (data == '2204') {
                            $scope.MarkData2204 = response.data;

                        } if (data == '2205') {
                            $scope.MarkData2205 = response.data;

                        } if (data == '2206') {
                            $scope.MarkData2206 = response.data;

                        } if (data == '2207') {
                            $scope.MarkData2207 = response.data;

                        } if (data == '2208') {
                            $scope.MarkData2208 = response.data;

                        }


                        // checkDirectLocation(); // ตรวจสอบว่า มี RequestDocument ค้างอยู่ไหม 

                    }, function errorCallback(response) {

                    });
                }







            }]);
})();