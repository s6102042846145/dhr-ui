﻿(function () {
    angular.module('DESS')
        .controller('EducationTraningDeleteViewerController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.isNewUnit = false;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            $scope.CountryLabelText = null;
            $scope.IndustryLabelText = null;

            $scope.Textcategory = 'PA_EDUCATION_MANAGEMENT';
            $scope.param = [];

            console.log('datas WorkHistory', $scope.document.Additional.Trainning);



            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }


            $scope.init = function () {
                init();

            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
                getPAConfiguration();
               

            }



            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.ChildAction.SetData = function () {
                //Do something ...
                $scope.document.RequestorPositionName = $scope.document.Additional.Trainning.RequestorPositionName;

            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                
                $scope.document.Additional.Trainning.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                $scope.document.Additional.Trainning.BeginDate = $scope.getDateFormatSave($scope.document.Additional.Trainning.BeginDate);
                $scope.document.Additional.Trainning.EndDate = $scope.getDateFormatSave($scope.document.Additional.Trainning.EndDate);
                $scope.requesterData.EmployeeID = $scope.document.Additional.Trainning.EmployeeID;
                $scope.document.RequestorPositionName = $scope.document.Additional.Trainning.RequestorPositionName;
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }

            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
        }]);
})();