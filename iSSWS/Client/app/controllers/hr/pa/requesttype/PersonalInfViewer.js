﻿(function () {
    angular.module('DESS')
        .controller('PersonalInfViewer', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
           
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;

            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            var LevelSelected = null;
           
            $scope.Textcategory = 'PA_PERSONAL_INFO';
            $scope.param = [];
            $scope.EducationData = $scope.document.Additional.Education;
            $scope.EducationDataOld = $scope.document.Additional.EducationOld;
            console.log('Additional', $scope.document.Additional);
            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }

            
           
            $scope.EducationLabelText = null;
            $scope.CertificateLabelText = null;
            $scope.Branch1LabelText = null;
            $scope.Branch2LabelText = null;
            $scope.InstituteLabelText = null;
            $scope.HonorLabelText = null;
            $scope.CountryLabelText = null;
            
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            console.log('aaaaaa', $scope.EducationData)
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getPAConfiguration();
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {


                console.log('enter loaddata', $scope.EducationData.BeginDate);
                if ($scope.EducationData.BeginDate == null || $scope.EducationData.BeginDate == '' || $scope.EducationData.BeginDate == undefined) {
                    console.log('enter startdate = null');
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );

                    return false;

                } else {
                    $scope.EducationData.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                    $scope.EducationData.BeginDate = $scope.getDateFormatSave($scope.EducationData.BeginDate);
                    $scope.EducationData.EndDate = $scope.getDateFormatSave($scope.EducationData.EndDate);
                }
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }









            //#### FRAMEWORK FUNCTION ### END

            //#### EducationLeval FUNCTION ### START
            $scope.GetEducationLevel = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetEducationLevelDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'EDU_LEVEL_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate)
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $filter('filter')($scope.EducationLevelList, { DLL_VALUE: $scope.EducationData.EducationLevelCode });


                    $scope.EducationLevelList = response.data.oDDL;
                    $scope.EducationLabelText = $filter('filter')($scope.EducationLevelList, { DLL_VALUE: $scope.EducationData.EducationLevelCode });
                    if ($scope.EducationLabelText.length > 0) {
                        $scope.EducationLabelText = $scope.EducationLabelText[0];
                    } else {
                        $scope.EducationLabelText = '-';
                    }
                    console.log('$scope.EducationLevelList', $scope.EducationLevelList);
                    console.log('$scope.EducationLabelText', $scope.EducationLabelText);
                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.GetEducationLevel();


            $scope.EducationMainMajor = function () {

                if (typeof $scope.EducationData.EducationLevelCode !== "undefined") {
                    LevelSelected = $scope.EducationData.EducationLevelCode;
                    LevelCode = "EDU_LEVEL_CODE";
                } else {
                    LevelSelected = "0000";
                    LevelCode = "";
                }
                var URL = CONFIG.SERVER + 'HRPA/GetEducationMinorDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'MAJOR_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EduLevel": LevelCode,
                        "EduLevelSelected": LevelSelected
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MainMajorList = response.data.oDDL;

                    $scope.Branch1LabelText = $filter('filter')($scope.MainMajorList, { DLL_VALUE: $scope.EducationData.Branch1 });
                    if ($scope.Branch1LabelText.length > 0) {
                        $scope.Branch1LabelText = $scope.Branch1LabelText[0];
                    } else {
                        $scope.Branch1LabelText = '-';
                    }


                    $scope.EducationMinor();
                    $scope.EducationQualification();
                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.EducationMainMajor();


            $scope.EducationMinor = function () {

                if (typeof $scope.EducationData.EducationLevelCode !== "undefined") {
                    LevelSelected = $scope.EducationData.EducationLevelCode;
                    LevelCode = "EDU_LEVEL_CODE";
                } else {
                    LevelSelected = "0000";
                    LevelCode = "";
                }
                var URL = CONFIG.SERVER + 'HRPA/GetEducationMinorDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'MAJOR_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EduLevel": LevelCode,
                        "EduLevelSelected": LevelSelected
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MinorList = response.data.oDDL;
                    $scope.Branch2LabelText = $filter('filter')($scope.MinorList, { DLL_VALUE: $scope.EducationData.Branch2 });
                    if ($scope.Branch2LabelText.length > 0) {
                        $scope.Branch2LabelText = $scope.Branch2LabelText[0];
                    } else {
                        $scope.Branch2LabelText = '-';
                    }


                    if (typeof $scope.EducationData.Branch1 !== "undefined") {

                        $scope.MinorList = $scope.MinorList.filter(function (item) {
                            return item.DLL_VALUE !== $scope.EducationData.Branch1;
                        })
                    }
                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.EducationMinor();


            $scope.EducationQualification = function () {
                if (typeof $scope.EducationData.EducationLevelCode !== "undefined") {
                    LevelSelected = $scope.EducationData.EducationLevelCode;
                    LevelCode = "EDU_LEVEL_CODE";
                } else {
                    LevelSelected = "0000";
                    LevelCode = "";
                }

                var URL = CONFIG.SERVER + 'HRPA/GetEducationQualificationDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'CERTIFICATE_EDU_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EduLevel": LevelCode,
                        "EduLevelSelected": LevelSelected
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.QualificationList = response.data.oDDL;
                    $scope.CertificateLabelText = $filter('filter')($scope.QualificationList, { DLL_VALUE: $scope.EducationData.CertificateCode });
                    if ($scope.CertificateLabelText.length > 0) {
                        $scope.CertificateLabelText = $scope.CertificateLabelText[0];
                    } else {
                        $scope.CertificateLabelText = '-';
                    }


                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.EducationQualification();



            $scope.EducationInstitution = function () {

                var URL = CONFIG.SERVER + 'HRPA/GetEducationInstitutionDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'INSTITUTE_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.InstituteList = response.data.oDDL;
                    $scope.InstituteLabelText = $filter('filter')($scope.InstituteList, { DLL_VALUE: $scope.EducationData.InstituteCode });
                    if ($scope.InstituteLabelText.length > 0) {
                        $scope.InstituteLabelText = $scope.InstituteLabelText[0];
                    } else {
                        $scope.InstituteLabelText = '-';
                    }

                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.EducationInstitution();


            $scope.EducationDegree = function () {

                var URL = CONFIG.SERVER + 'HRPA/GetEducationDegreeDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'SPECIAL_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.DegreeList = response.data.oDDL;
                   
                    $scope.HonorLabelText = $filter('filter')($scope.DegreeList, { DLL_VALUE: $scope.EducationData.HonorCode });
                    if ($scope.HonorLabelText.length > 0) {
                        $scope.HonorLabelText = $scope.HonorLabelText[0];
                    } else {
                        $scope.InstituteLabelText = '-';
                    }
                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.EducationDegree();


            $scope.EducationCountry = function () {

                var URL = CONFIG.SERVER + 'HRPA/GetEducationCountryDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'COUNTRY_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CountryList = response.data.oDDL;

                    
                    $scope.CountryLabelText = $filter('filter')($scope.CountryList, { DLL_VALUE: $scope.EducationData.CountryCode });
                    if ($scope.CountryLabelText.length > 0) {
                        $scope.CountryLabelText = $scope.CountryLabelText[0];
                    } else {
                        $scope.CountryLabelText = '-';
                    }
                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.EducationCountry();


            $scope.ChangeMinor = function () {

                $scope.EducationMinor();
            }



            //#### EducationLeval FUNCTION ### END









            //#### OTHERS FUNCTION ### START 

            $scope.GetOrgUnit = function () {
                var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "LevelSelected": '',
                        "BeginDate": "9999-12-31",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UnitList = response.data.oDDL;

                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                    console.log('error GetOrgUnit', response);
                });
            }
            $scope.GetOrgUnit();

            $scope.GetBand = function () {
                var URL = CONFIG.SERVER + 'HROM/GetBandDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "BeginDate": "9999-12-31",
                        "EndDate": "9999-12-31"
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.BandList = response.data.oDDL;

                    console.log('success GetBand', response);
                }, function errorCallback(response) {
                    console.log('error GetBand', response);
                });
            }
            $scope.GetBand();

            //#region date format
            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            $scope.Text = null;
            $scope.RequestorText = null;
            $scope.getAllTextDescription = function () {
                $scope.Text = null;
                var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.Text = response.data;
                        // $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
                    }
                }, function errorCallback(response) {
                    // Error
                    $scope.Text = null;
                    console.log('error MainController TextDescription.', response);
                });
            };
            $scope.getAllTextDescription();

        }]);
})();