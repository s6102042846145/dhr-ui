﻿(function () {
    angular.module('DESS')
        .controller('PersonalDataViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.Textcategory = "HRPAPERSONALDATA";
            $scope.IDCardNo;
            $scope.LicenseID;
            $scope.TitleNameText;
            $scope.PrefixNameText;
            $scope.SecondTitleText;
            $scope.MilitaryTitleText;
            $scope.AcademicTitleText;
            $scope.MedicalTitleText;
            $scope.GenderText;
            $scope.BirthPlaceText;
            $scope.NationalityText;
            $scope.MaritalStatusText;
            $scope.BirthCityText;
            $scope.ReligionText;
            $scope.BirthDateText;
            $scope.TitleNameText_OLD;
            $scope.PrefixNameText_OLD;
            $scope.SecondTitleText_OLD;
            $scope.MilitaryTitleText_OLD;
            $scope.AcademicTitleText_OLD;
            $scope.MedicalTitleText_OLD;
            $scope.GenderText_OLD;
            $scope.BirthPlaceText_OLD;
            $scope.NationalityText_OLD;
            $scope.MaritalStatusText_OLD;
            $scope.BirthCityText_OLD;
            $scope.ReligionText_OLD;
            $scope.BirthDateText_OLD;
            $scope.MaritalEffectiveDateText_OLD;
            $scope.UserImg;
            $scope.PAConfigPersonalTitleFormat = [];
            $scope.loader.enable = true;
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getPAConfiguration();
                $scope.PersonalData = $scope.document.Additional.PersonalData;
                $scope.PersonalData_OLD = $scope.document.Additional.PersonalData_OLD;
                getPersonSelectData();
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...
            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Personal;
                    //check Personaldata Title Config
                    for (var i = 0; i < $scope.PAConfiguration.length; i++) {
                        switch ($scope.PAConfiguration[i].FieldName) {
                            case "TitleID":
                            case "TitleIDEn":
                            case "AcademicTitle":
                            case "MedicalTitle":
                            case "MilitaryTitle":
                            case "PrefixName":
                            case "SecondTitle"://DR.
                                if (!$scope.PAConfiguration[i].IsVisible) {
                                    $scope.PAConfigPersonalTitleFormat.push($scope.PAConfiguration[i].FieldName);
                                }
                                break;
                            default:
                        }
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.', response);
                });
            }

            function FilterDescription(array, attr, value, objReturn) {
                $scope.isData = false;
                if (value == "")
                    return "";
                else {
                    for (var i = 0; i < array.length; i += 1) {
                        if (array[i][attr] == value) {
                            $scope.isData = true;
                            return array[i][objReturn];
                        }
                    }
                }

                if (!$scope.isData)
                    return "";
            }

            function getPersonSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Language = response.data.Language;
                    $scope.Title = response.data.Title;

                    $scope.Prefix = response.data.Prefix;
                    $scope.SecondTitle = response.data.SecondTitle;
                    $scope.AcademicTitle = response.data.AcademicTitle;
                    $scope.MilitaryTitle = response.data.MilitaryTitle;
                    $scope.MedicalTitle = response.data.MedicalTitle;


                    $scope.TitleEN = response.data.TitleEN;
                    $scope.PrefixEN = response.data.PrefixEN;
                    $scope.SecondTitleEN = response.data.SecondTitleEN;
                    $scope.AcademicTitleEN = response.data.AcademicTitleEN;
                    $scope.MilitaryTitleEN = response.data.MilitaryTitleEN;
                    $scope.MedicalTitleEN = response.data.MedicalTitleEN;

                    $scope.TitleTH = response.data.TitleTH;
                    $scope.PrefixTH = response.data.PrefixTH;
                    $scope.SecondTitleTH = response.data.SecondTitleTH;
                    $scope.AcademicTitleTH = response.data.AcademicTitleTH;
                    $scope.MilitaryTitleTH = response.data.MilitaryTitleTH;
                    $scope.MedicalTitleTH = response.data.MedicalTitleTH;

                    $scope.Gender = response.data.Gender;
                    $scope.Nationality = response.data.Nationality;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.Religion = response.data.Religion;
                    $scope.Province = response.data.Province;
                    $scope.Country = response.data.Country;
                    $scope.UserImg = response.data.UserImg;

                    if ($scope.Prefix !== 'null' && $scope.Prefix.length > 0) {
                        $scope.PrefixNameText = FilterDescription($scope.Prefix, 'Key', $scope.PersonalData.Prefix, 'Description');
                        $scope.PrefixNameText_OLD = FilterDescription($scope.Prefix, 'Key', $scope.PersonalData_OLD.Prefix, 'Description');
                    }

                    if ($scope.SecondTitle !== 'null' && $scope.SecondTitle.length > 0) {
                        $scope.SecondTitleText = FilterDescription($scope.SecondTitle, 'Key', $scope.PersonalData.SecondTitle, 'Description');
                        $scope.SecondTitleText_OLD = FilterDescription($scope.SecondTitle, 'Key', $scope.PersonalData_OLD.SecondTitle, 'Description');
                    }

                    if ($scope.MilitaryTitle !== 'null' && $scope.MilitaryTitle.length > 0) {
                        $scope.MilitaryTitleText = FilterDescription($scope.MilitaryTitle, 'Key', $scope.PersonalData.MilitaryTitle, 'Description');
                        $scope.MilitaryTitleText_OLD = FilterDescription($scope.MilitaryTitle, 'Key', $scope.PersonalData_OLD.MilitaryTitle, 'Description');
                    }

                    if ($scope.AcademicTitle !== 'null' && $scope.AcademicTitle.length > 0) {
                        $scope.AcademicTitleText = FilterDescription($scope.AcademicTitle, 'Key', $scope.PersonalData.AcademicTitle, 'Description');
                        $scope.AcademicTitleText_OLD = FilterDescription($scope.AcademicTitle, 'Key', $scope.PersonalData_OLD.AcademicTitle, 'Description');
                    }

                    if ($scope.MedicalTitle !== 'null' && $scope.MedicalTitle.length > 0) {
                        $scope.MedicalTitleText = FilterDescription($scope.MedicalTitle, 'Key', $scope.PersonalData.MedicalTitle, 'Description');
                        $scope.MedicalTitleText_OLD = FilterDescription($scope.MedicalTitle, 'Key', $scope.PersonalData_OLD.MedicalTitle, 'Description');
                    }

                    if ($scope.Title != 'null' && $scope.Title.length > 0) {
                        $scope.TitleNameText = FilterDescription($scope.Title, 'Key', $scope.PersonalData.TitleID, 'Description');
                        $scope.TitleNameText_OLD = FilterDescription($scope.Title, 'Key', $scope.PersonalData_OLD.TitleID, 'Description');
                    }

                    if ($scope.TitleEN != 'null' && $scope.TitleEN.length > 0) {
                        $scope.TitleNameEnText = FilterDescription($scope.TitleEN, 'Key', $scope.PersonalData.TitleIDEn, 'Description');
                        $scope.TitleNameEnText_OLD = FilterDescription($scope.TitleEN, 'Key', $scope.PersonalData_OLD.TitleIDEn, 'Description');
                    }
                    if ($scope.Gender != 'null' && $scope.Gender.length > 0) {
                        $scope.GenderText = FilterDescription($scope.Gender, 'Key', $scope.PersonalData.Gender, 'Description');
                        $scope.GenderText_OLD = FilterDescription($scope.Gender, 'Key', $scope.PersonalData_OLD.Gender, 'Description');
                    }
                    if ($scope.Nationality != 'null' && $scope.Nationality.length > 0) {
                        $scope.NationalityText = FilterDescription($scope.Nationality, 'NationalityKey', $scope.PersonalData.Nationality, 'NationalityName');
                        $scope.NationalityText_OLD = FilterDescription($scope.Nationality, 'NationalityKey', $scope.PersonalData_OLD.Nationality, 'NationalityName');
                    }
                    if ($scope.MaritalStatus != 'null' && $scope.MaritalStatus.length > 0) {
                        $scope.MaritalStatusText = FilterDescription($scope.MaritalStatus, 'Key', $scope.PersonalData.MaritalStatus, 'Description');
                        $scope.MaritalStatusText_OLD = FilterDescription($scope.MaritalStatus, 'Key', $scope.PersonalData_OLD.MaritalStatus, 'Description');
                    }
                    if ($scope.Religion != 'null' && $scope.Religion.length > 0) {
                        $scope.ReligionText = FilterDescription($scope.Religion, 'Key', $scope.PersonalData.Religion, 'Description');
                        $scope.ReligionText_OLD = FilterDescription($scope.Religion, 'Key', $scope.PersonalData_OLD.Religion, 'Description');
                    }
                    if ($scope.Country != 'null' && $scope.Country.length > 0) {
                        $scope.BirthCityText = FilterDescription($scope.Country, 'CountryCode', $scope.PersonalData.BirthCity, 'CountryName');
                        $scope.BirthCityText_OLD = FilterDescription($scope.Country, 'CountryCode', $scope.PersonalData_OLD.BirthCity, 'CountryName');
                    }
                    if ($scope.Province != 'null' && $scope.Province.length > 0) {
                        $scope.BirthPlaceText = FilterDescription($scope.Province, 'ProvinceCode', $scope.PersonalData.BirthPlace, 'ProvinceName');
                        $scope.BirthPlaceText_OLD = FilterDescription($scope.Province, 'ProvinceCode', $scope.PersonalData_OLD.BirthPlace, 'ProvinceName');
                    }
                    if ($scope.PersonalData.DOB != null) {
                        $scope.BirthDateText = getDateFormate($scope.PersonalData.DOB);
                    } else {
                        $scope.BirthDateText = '';
                    }
                    if ($scope.PersonalData_OLD.DOB != null) {
                        $scope.BirthDateText_OLD = getDateFormate($scope.PersonalData_OLD.DOB);
                    } else {
                        $scope.BirthDateText_OLD = '';
                    }

                    if ($scope.PersonalData.LicenceIssueDate != null) {
                        $scope.LicenceIssueDateText = getDateFormate($scope.PersonalData.LicenceIssueDate);
                    } else {
                        $scope.LicenceIssueDateText = '';
                    }
                    if ($scope.PersonalData_OLD.LicenceIssueDate != null) {
                        $scope.LicenceIssueDateText_OLD = getDateFormate($scope.PersonalData_OLD.LicenceIssueDate);
                    } else {
                        $scope.LicenceIssueDateText_OLD = '';
                    }

                    if ($scope.PersonalData.LicenceExpiredDate != null) {
                        $scope.LicenceExpiredDateText = getDateFormate($scope.PersonalData.LicenceExpiredDate);
                    } else {
                        $scope.LicenceExpiredDateText = '';
                    }
                    if ($scope.PersonalData_OLD.LicenceExpiredDate != null) {
                        $scope.LicenceExpiredDateText_OLD = getDateFormate($scope.PersonalData_OLD.LicenceExpiredDate);
                    } else {
                        $scope.LicenceExpiredDateText_OLD = '';
                    }

                    //New
                    //$scope.FullNameTH = $scope.displayNameFormat($scope.PersonalData.TitleID, $scope.PersonalData.Prefix, $scope.PersonalData.MilitaryTitle, $scope.PersonalData.SecondTitle, $scope.PersonalData.AcademicTitle, $scope.PersonalData.MedicalTitle, 'TH') + " " + $scope.PersonalData.FirstName + " " + $scope.PersonalData.LastName;


                    //$scope.FullNameEN = $scope.displayNameFormat($scope.PersonalData.TitleID, $scope.PersonalData.Prefix, $scope.PersonalData.MilitaryTitle, $scope.PersonalData.SecondTitle, $scope.PersonalData.AcademicTitle, $scope.PersonalData.MedicalTitle, 'EN') + " " + $scope.PersonalData.FirstNameEn + " " + $scope.PersonalData.LastNameEn;
                    ////Old
                    //$scope.FullNameOLD_TH = $scope.displayNameFormat($scope.PersonalData_OLD.TitleID, $scope.PersonalData_OLD.Prefix, $scope.PersonalData_OLD.MilitaryTitle, $scope.PersonalData_OLD.SecondTitle, $scope.PersonalData_OLD.AcademicTitle, $scope.PersonalData_OLD.MedicalTitle, 'TH') + " " + $scope.PersonalData_OLD.FirstName + " " + $scope.PersonalData_OLD.LastName;;
                    //$scope.FullNameOLD_EN = $scope.displayNameFormat($scope.PersonalData_OLD.TitleID, $scope.PersonalData_OLD.Prefix, $scope.PersonalData_OLD.MilitaryTitle, $scope.PersonalData_OLD.SecondTitle, $scope.PersonalData_OLD.AcademicTitle, $scope.PersonalData_OLD.MedicalTitle, 'EN') + " " + $scope.PersonalData_OLD.FirstNameEn + " " + $scope.PersonalData_OLD.LastNameEn;
                    //PersonalInformation.PersonalData.FullNameEN
                    $scope.FullNameTH = $scope.PersonalData.FullName.replace(/  +/g, ' ');
                    $scope.FullNameOLD_TH = $scope.PersonalData_OLD.FullName.replace(/  +/g, ' ');

                    $scope.FullNameEN = $scope.PersonalData.FullNameEN.replace(/  +/g, ' ');
                    $scope.FullNameOLD_EN = $scope.PersonalData_OLD.FullNameEN.replace(/  +/g, ' ');

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('error RequestController.', response);
                });
            }

            $scope.displayNameFormat = function (iname, iname1, iname2, iname3, iname4, iname5, lang) {
                $scope.NameFormatTitleTH = '';
                $scope.NameFormatTitleEN = '';
                for (var i = 0; i < $scope.PAConfigPersonalTitleFormat.length; i++) {
                    switch ($scope.PAConfigPersonalTitleFormat[i]) {
                        case "TitleID":
                            iname = '';
                            break;
                        case "AcademicTitle":
                            iname4 = '';
                            break;
                        case "MedicalTitle":
                            iname5 = '';
                            break;
                        case "MilitaryTitle":
                            iname2 = '';
                            break;
                        case "PrefixName":
                            iname1 = '';
                            break;
                        case "SecondTitle"://DR.
                            iname3 = '';
                            break;
                    }
                }
                if (lang == 'TH') {
                    if (iname4 != undefined && iname4 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.AcademicTitleTH, 'Key', iname4, 'Description') + " ";
                        if (iname2 != undefined && iname2 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MilitaryTitleTH, 'Key', iname2, 'Description') + " ";
                        }
                        if (iname3 != undefined && iname3 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.SecondTitleTH, 'Key', iname3, 'Description') + " ";
                        }
                        if (iname1 != undefined && iname1 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                        }
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname2 != undefined && iname2 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MilitaryTitleTH, 'Key', iname2, 'Description') + " ";
                        if (iname3 != undefined && iname3 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.SecondTitleTH, 'Key', iname3, 'Description') + " ";
                        }
                        if (iname1 != undefined && iname1 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                        }
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname3 != undefined && iname3 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.SecondTitleTH, 'Key', iname3, 'Description') + " ";
                        if (iname1 != undefined && iname1 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                        }
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname1 != undefined && iname1 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname5 != undefined && iname5 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                    } else {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.TitleTH, 'Key', iname, 'Description') + " ";
                    }
                    return $scope.NameFormatTitleTH;
                } else {
                    //EN
                    if (iname4 != undefined && iname4 != '') {
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.AcademicTitleEN, 'Key', iname4, 'Description') + " ";
                        if (iname2 != undefined && iname2 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MilitaryTitleEN, 'Key', iname2, 'Description') + " ";
                        }
                        if (iname3 != undefined && iname3 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.SecondTitleEN, 'Key', iname3, 'Description') + " ";
                        }
                        if (iname1 != undefined && iname1 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                        }
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname2 != undefined && iname2 != '') {
                        $scope.NameFormatTitleTEN = $scope.NameFormatTitleTEN + FilterDescription($scope.MilitaryTitleEN, 'Key', iname2, 'Description') + " ";
                        if (iname3 != undefined && iname3 != '') {
                            $scope.NameFormatTitleTEN = $scope.NameFormatTitleTEN + FilterDescription($scope.SecondTitleEN, 'Key', iname3, 'Description') + " ";
                        }
                        if (iname1 != undefined && iname1 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                        }
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname3 != undefined && iname3 != '') {
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.SecondTitleEN, 'Key', iname3, 'Description') + " ";
                        if (iname1 != undefined && iname1 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                        }
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname1 != undefined && iname1 != '') {
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                        if (iname5 != undefined && iname5 != '') {
                            $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                        }
                    } else if (iname5 != undefined && iname5 != '') {
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                    } else {
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.TitleEN, 'Key', iname, 'Description') + " ";
                    }
                    return $scope.NameFormatTitleEN;
                }
            }
            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }
            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
            //#### OTHERS FUNCTION ### END
        }]);
})();