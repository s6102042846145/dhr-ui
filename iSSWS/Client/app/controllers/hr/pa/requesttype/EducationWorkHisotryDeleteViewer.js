﻿(function () {
    angular.module('DESS')
        .controller('EducationWorkHisotryDeleteViewerController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.isNewUnit = false;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            $scope.CountryLabelText = null;
            $scope.IndustryLabelText = null;

            $scope.Textcategory = 'PA_EDUCATION_MANAGEMENT';
            $scope.param = [];

            console.log('datas WorkHistory', $scope.document.Additional.WorkHistory);



            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }


            $scope.init = function () {
                init();

            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
                getPAConfiguration();
                $scope.Country();
                $scope.Industry();

            }



            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.ChildAction.SetData = function () {
                //Do something ...
                $scope.document.RequestorPositionName = $scope.document.Additional.WorkHistory.RequestorPositionName;

            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                
                    $scope.document.Additional.WorkHistory.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                    $scope.document.Additional.WorkHistory.BeginDate = $scope.getDateFormatSave($scope.document.Additional.WorkHistory.BeginDate);
                    $scope.document.Additional.WorkHistory.EndDate = $scope.getDateFormatSave($scope.document.Additional.WorkHistory.EndDate);
                    $scope.requesterData.EmployeeID = $scope.document.Additional.WorkHistory.EmpCode;
                    $scope.document.RequestorPositionName = $scope.document.Additional.WorkHistory.RequestorPositionName;
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }



            $scope.Country = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetEducationCountryDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'COUNTRY_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31'
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CountryList = response.data.oDDL;
                    $scope.CountryLabelText = $filter('filter')($scope.CountryList, { DLL_VALUE: $scope.document.Additional.WorkHistory.WorkCountryValue });
                    
                    if ($scope.CountryLabelText.length > 0) {
                        $scope.CountryLabelText = $scope.CountryLabelText[0];
                        console.log('$scope.CountryLabelText', $scope.CountryLabelText);
                    } else {
                        $scope.CountryLabelText = '-';
                        console.log('$scope.CountryLabelText', $scope.CountryLabelText);
                    }
                    console.log('success EducationCountry', response.data.oDDL);
                }, function errorCallback(response) {
                    console.log('error EducationCountry', response.data.oDDL);
                });
            }
            $scope.Industry = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetIndustryDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "Code": 'BU_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31'
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.BussinessList = response.data.oDDL;
                    $scope.IndustryLabelText = $filter('filter')($scope.BussinessList, { DLL_VALUE: $scope.document.Additional.WorkHistory.BuValue });
                    if ($scope.IndustryLabelText.length > 0) {
                        $scope.IndustryLabelText = $scope.IndustryLabelText[0];
                        console.log('$scope.IndustryLabelText', $scope.IndustryLabelText);
                    } else {
                        $scope.IndustryLabelText = '-';
                        console.log('$scope.IndustryLabelText', $scope.IndustryLabelText);
                    }

                    console.log('success EducationCountry', response.data.oDDL);
                }, function errorCallback(response) {
                    console.log('error EducationCountry', response.data.oDDL);
                });
            }



            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
        }]);
})();