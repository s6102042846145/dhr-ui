﻿(function () {
    angular.module('DESS')
        .controller('PersonalAddressViewer', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.IsDisable = true;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            var LevelSelected = null
           
            $scope.Textcategory = 'PA_EDUCATION_MANAGEMENT';
            $scope.param = [];
            console.log('Additional', $scope.document.Additional);
            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }

      
           
            $scope.TitleLabelText = null;
            $scope.UserroleLabelText = null;
            $scope.SubTypeLabelText = null;


            $scope.init = function () {
                init();
                
            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
                getPAConfiguration();
                $scope.PareDLL();
                $scope.UserroleDLL();
                $scope.GetSubtype();
                
                // $scope.GetPosition();

            }
            
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
           
            $scope.ChildAction.SetData = function () {
                //Do something ...
               
            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.Additional.Academicworkmember.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                $scope.document.Additional.Academicworkmember.BeginDate = $scope.getDateFormatSave($scope.document.Additional.Academicworkmember.BeginDate);
                $scope.document.Additional.Academicworkmember.EndDate = $scope.getDateFormatSave($scope.document.Additional.Academicworkmember.EndDate);
                $scope.requesterData.EmployeeID = $scope.document.Additional.Academicworkmember.EmpCode;
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }

            //#### FRAMEWORK FUNCTION ### END

            //#### EducationLeval FUNCTION ### START

            $scope.PareDLL = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetPareDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'PARE',
                        "Code": '1',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.pareList = response.data.oDDL;
                    $scope.TitleLabelText = $filter('filter')($scope.pareList, { DLL_VALUE: $scope.document.Additional.Academicworkmember.PAReID });
                    if ($scope.TitleLabelText.length > 0) {
                        $scope.TitleLabelText = $scope.TitleLabelText[0];
                    } else {
                        $scope.TitleLabelText = '-';
                    }

                    console.log('success pareList', response.data.oDDL);
                }, function errorCallback(response) {
                    console.log('error pareList', response.data.oDDL);
                });
            }
            $scope.UserroleDLL = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetUserroleDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "Code": 'ROLE_CODE',
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UserroleList = response.data.oDDL;
                    $scope.UserroleLabelText = $filter('filter')($scope.UserroleList, { DLL_VALUE: $scope.document.Additional.Academicworkmember.RoleValue });
                    if ($scope.UserroleLabelText.length > 0) {
                        $scope.UserroleLabelText = $scope.UserroleLabelText[0];
                    } else {
                        $scope.UserroleLabelText = '-';
                    }
                    console.log('success UserroleList', response.data.oDDL);
                }, function errorCallback(response) {
                    console.log('error UserroleList', response.data.oDDL);
                });
            }


            $scope.GetSubtype = function () {
                $scope.unitSearch = '';
                var URL = CONFIG.SERVER + 'HRPA/SubtypeDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "BeginDate": "2000-01-01",
                        "EndDate": "9999-12-31",
                        "SUBTYPE": 'SUBTYPE_CODE'
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SubTypeList = response.data.oDDL;
                    
                    $scope.SubTypeLabelText = $filter('filter')($scope.SubTypeList, { DLL_VALUE: $scope.document.Additional.Academicworkmember.SubtypeValue });
                    if ($scope.SubTypeLabelText.length > 0) {
                        $scope.SubTypeLabelText = $scope.SubTypeLabelText[0];
                    } else {
                        $scope.SubTypeLabelText = '-';
                    }
                    console.log('success SubTypeList', response);
                }, function errorCallback(response) {
                    console.log('error SubTypeList', response);
                });
            }



           
            //#region date format
            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }
            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
        }]);
})();