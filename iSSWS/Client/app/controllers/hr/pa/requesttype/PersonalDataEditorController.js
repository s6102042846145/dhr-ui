﻿(function () {
    angular.module('DESS')
        .controller('PersonalDataEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.profile = (getToken(CONFIG.USER));
            $scope.Textcategory = "HRPAPERSONALDATA";
            $scope.TitleNameText;
            $scope.TitleNameEnText;
            $scope.PrefixNameText;
            $scope.SecondTitleText;
            $scope.MilitaryTitleText;
            $scope.AcademicTitleText;
            $scope.MedicalTitleText;
            $scope.GenderText;
            $scope.BirthPlaceText;
            $scope.NationalityText;
            $scope.MaritalStatusText;
            $scope.BirthCityText;
            $scope.ReligionText;
            $scope.BirthDateText;
            $scope.PAConfigPersonalTitleFormat = [];
            $scope.loader.enable = true;
            $scope.oUser = getToken(CONFIG.USER);
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getPAConfiguration();
                getPersonSelectData();
                if ($scope.document.Additional.PersonalData.LicenceIssueDate == null) {
                    //  $scope.document.Additional.PersonalData.LicenceIssueDate = Date.now();
                }
                if ($scope.document.Additional.PersonalData.LicenceExpiredDate == null) {
                    //  $scope.document.Additional.PersonalData.LicenceExpiredDate = Date.now();
                }
                var oEmployeeData = getToken(CONFIG.USER);
                $scope.isAdmin = false;
                for (i = 0; i <= oEmployeeData.UserRoles.length - 1; i++) {
                    if (oEmployeeData.UserRoles[i] == "PAADMIN")
                        $scope.isAdmin = true;
                }
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.Additional.PersonalData.DOB = $filter('date')($scope.document.Additional.PersonalData.DOB, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData_OLD.DOB = $filter('date')($scope.document.Additional.PersonalData_OLD.DOB, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData.LicenceIssueDate = $filter('date')($scope.document.Additional.PersonalData.LicenceIssueDate, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData_OLD.LicenceIssueDate = $filter('date')($scope.document.Additional.PersonalData_OLD.LicenceIssueDate, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData.LicenceExpiredDate = $filter('date')($scope.document.Additional.PersonalData.LicenceExpiredDate, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData_OLD.LicenceExpiredDate = $filter('date')($scope.document.Additional.PersonalData_OLD.LicenceExpiredDate, 'yyyy-MM-dd');

                //check modify type
                //if ($scope.document.Additional.PersonalData_OLD.AcademicTitle!=) {

                //}
                $scope.ModifyPersonalInfo = true;
                $scope.ModifyDocumentInfo = false;
                for (var i = 0; i < $scope.PAConfiguration.length; i++) {
                    switch ($scope.PAConfiguration[i].FieldName) {
                        case "LicenseID":
                        case "LicenceIssueDate":
                        case "LicenceExpiredDate":
                            if ($scope.document.Additional.PersonalData_OLD[$scope.PAConfiguration[i].FieldName] != $scope.document.Additional.PersonalData[$scope.PAConfiguration[i].FieldName]) {
                                $scope.ModifyDocumentInfo = true;
                            }
                            break;
                    }

                }

                if ($scope.ModifyPersonalInfo && $scope.ModifyDocumentInfo) {
                    $scope.document.Additional.PersonalData.ModifyType = "AllInfo";
                } else if ($scope.ModifyPersonalInfo && !$scope.ModifyDocumentInfo) {
                    $scope.document.Additional.PersonalData.ModifyType = "PersonalInfo";
                } else if (!$scope.ModifyPersonalInfo && $scope.ModifyDocumentInfo) {
                    $scope.document.Additional.PersonalData.ModifyType = "DocumentInfo";
                }
            };

            //Validate Function : Use to validate data before submit form
            $scope.ChildAction.ValidateData = function () {
                if ($scope.validateForm.$error.required == undefined) {
                    if (!$scope.CumtomValidate()) {
                        $scope.validateResult = false;
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['SYSTEM'].WARNING)
                                .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                        );
                    } else {
                        $scope.validateResult = true;
                    }

                } else {
                    $scope.validateResult = false;
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );
                }
                return $scope.validateResult;
            };

            $scope.ChildAction.SetData();

            $scope.CumtomValidate = function () {
                $scope.ResultCumtomValidate = true;
                //for (var i = 0; i < $scope.PAConfiguration.length; i++) {
                //    switch ($scope.PAConfiguration[i].FieldName) {
                //        case "LicenceIssueDate":
                //            if ($scope.PAConfiguration[i].IsRequired
                //                && ($scope.document.Additional.PersonalData.LicenceIssueDate == ''
                //                    || $scope.document.Additional.PersonalData.LicenceIssueDate == null)) {
                //                $scope.ResultCumtomValidate = false;
                //            }
                //            break;
                //        case "LicenceExpiredDate":
                //            if ($scope.PAConfiguration[i].IsRequired
                //                && ($scope.document.Additional.PersonalData.LicenceExpiredDate == ''
                //                    || $scope.document.Additional.PersonalData.LicenceExpiredDate == null)) {
                //                $scope.ResultCumtomValidate = false;
                //            }
                //            break;
                //    }
                //}
                return $scope.ResultCumtomValidate;
            }

            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Personal;
                    //check Personaldata Title Config
                    for (var i = 0; i < $scope.PAConfiguration.length; i++) {
                        switch ($scope.PAConfiguration[i].FieldName) {
                            case "TitleID":
                            case "TitleIDEn":
                            case "AcademicTitle":
                            case "MedicalTitle":
                            case "MilitaryTitle":
                            case "PrefixName":
                            case "SecondTitle"://DR.
                                if (!$scope.PAConfiguration[i].IsVisible) {
                                    $scope.PAConfigPersonalTitleFormat.push($scope.PAConfiguration[i].FieldName);
                                }
                                break;
                            default:
                        }
                    }
                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }

            function FilterDescription(array, attr, value, objReturn) {
                $scope.isData = false;
                if (value == "")
                    return "";
                else {
                    for (var i = 0; i < array.length; i += 1) {
                        if (array[i][attr] == value) {
                            $scope.isData = true;
                            return array[i][objReturn];
                        }
                    }
                }

                if (!$scope.isData)
                    return "";
            }

            function getPersonSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Language = response.data.Language;
                    $scope.Title = response.data.Title;
                    $scope.TitleEN = response.data.TitleEN;
                    $scope.Prefix = response.data.Prefix;
                    $scope.SecondTitle = response.data.SecondTitle;
                    $scope.AcademicTitle = response.data.AcademicTitle;
                    $scope.MilitaryTitle = response.data.MilitaryTitle;
                    $scope.MedicalTitle = response.data.MedicalTitle;


                    $scope.PrefixEN = response.data.PrefixEN;
                    $scope.SecondTitleEN = response.data.SecondTitleEN;
                    $scope.AcademicTitleEN = response.data.AcademicTitleEN;
                    $scope.MilitaryTitleEN = response.data.MilitaryTitleEN;
                    $scope.MedicalTitleEN = response.data.MedicalTitleEN;

                    $scope.TitleTH = response.data.TitleTH;
                    $scope.PrefixTH = response.data.PrefixTH;
                    $scope.SecondTitleTH = response.data.SecondTitleTH;
                    $scope.AcademicTitleTH = response.data.AcademicTitleTH;
                    $scope.MilitaryTitleTH = response.data.MilitaryTitleTH;
                    $scope.MedicalTitleTH = response.data.MedicalTitleTH;

                    $scope.Gender = response.data.Gender;
                    $scope.Nationality = response.data.Nationality;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.Religion = response.data.Religion;
                    $scope.Province = response.data.Province;//BirtiPlace-ภูมิลำเนา
                    $scope.Country = response.data.Country;
                    $scope.FamilyMember = response.data.FamilyMember;
                    $scope.ChildNoList = response.data.MasterChild;

                    if ($scope.Title !== 'null' && $scope.Title.length > 0) {
                        $scope.TitleNameText = FilterDescription($scope.Title, 'Key', $scope.document.Additional.PersonalData.TitleID, 'Description');
                    }
                    if ($scope.TitleEN !== 'null' && $scope.TitleEN.length > 0) {
                        $scope.TitleNameEnText = FilterDescription($scope.TitleEN, 'Key', $scope.document.Additional.PersonalData.TitleIDEn, 'Description');
                    }
                    if ($scope.Prefix !== 'null' && $scope.Prefix.length > 0) {
                        $scope.PrefixNameText = FilterDescription($scope.Prefix, 'Key', $scope.document.Additional.PersonalData.Prefix, 'Description');
                    }

                    if ($scope.SecondTitle !== 'null' && $scope.SecondTitle.length > 0) {
                        $scope.SecondTitleText = FilterDescription($scope.SecondTitle, 'Key', $scope.document.Additional.PersonalData.SecondTitle, 'Description');
                    }

                    if ($scope.MilitaryTitle !== 'null' && $scope.MilitaryTitle.length > 0) {
                        $scope.MilitaryTitleText = FilterDescription($scope.MilitaryTitle, 'Key', $scope.document.Additional.PersonalData.MilitaryTitle, 'Description');
                    }

                    if ($scope.AcademicTitle !== 'null' && $scope.AcademicTitle.length > 0) {
                        $scope.AcademicTitleText = FilterDescription($scope.AcademicTitle, 'Key', $scope.document.Additional.PersonalData.AcademicTitle, 'Description');
                    }

                    if ($scope.MedicalTitle !== 'null' && $scope.MedicalTitle.length > 0) {
                        $scope.MedicalTitleText = FilterDescription($scope.MedicalTitle, 'Key', $scope.document.Additional.PersonalData.MedicalTitle, 'Description');
                    }

                    if ($scope.document.Additional.PersonalData.DOB != null) {
                        $scope.BirthDateText = getDateFormate($scope.document.Additional.PersonalData.DOB);
                    }

                    if ($scope.Gender !== 'null' && $scope.Gender.length > 0) {
                        $scope.GenderText = FilterDescription($scope.Gender, 'Key', $scope.document.Additional.PersonalData.Gender, 'Description');
                    }

                    if ($scope.Nationality !== 'null' && $scope.Nationality.length > 0) {
                        $scope.NationalityText = FilterDescription($scope.Nationality, 'NationalityKey', $scope.document.Additional.PersonalData.Nationality, 'NationalityName');
                    }

                    if ($scope.MaritalStatus !== 'null' && $scope.MaritalStatus.length > 0) {
                        $scope.MaritalStatusText = FilterDescription($scope.MaritalStatus, 'Key', $scope.document.Additional.PersonalData.MaritalStatus, 'Description');
                    }

                    if ($scope.Province !== 'null' && $scope.Province.length > 0) {
                        $scope.BirthPlaceText = FilterDescription($scope.Province, 'ProvinceCode', $scope.document.Additional.PersonalData.BirthPlace, 'ProvinceName');
                    }

                    if ($scope.Religion !== 'null' && $scope.Religion.length > 0) {
                        $scope.ReligionText = FilterDescription($scope.Religion, 'Key', $scope.document.Additional.PersonalData.Religion, 'Description');
                    }

                    if ($scope.Country !== 'null' && $scope.Country.length > 0) {
                        $scope.BirthCityText = FilterDescription($scope.Country, 'CountryCode', $scope.document.Additional.PersonalData.BirthCity, 'CountryName');
                    }

                    $scope.loader.enable = false;
                    console.log('success getPersonSelectData.');
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    console.log('error getPersonSelectData.', response);
                });
            }

            $scope.SetDate = function () {
                $scope.DefaultBeginDate = this.document.Additional.PersonalData.LicenceIssueDate;
                if (getDateFormateData(this.document.Additional.PersonalData.LicenceIssueDate) > getDateFormateData($scope.document.Additional.PersonalData.LicenceExpiredDate)) {
                    $scope.document.Additional.PersonalData.LicenceExpiredDate = this.document.Additional.PersonalData.LicenceIssueDate;
                }
            }

            $scope.displayNameFormat = function (iname, inameen, iname1, iname2, iname3, iname4, iname5) {
                $scope.NameFormatTitleTH = '';
                $scope.NameFormatTitleEN = '';
                for (var i = 0; i < $scope.PAConfigPersonalTitleFormat.length; i++) {
                    switch ($scope.PAConfigPersonalTitleFormat[i]) {
                        case "TitleID":
                            iname = '';
                            break;
                        case "TitleIDEn":
                            inameen = '';
                            break;
                        case "AcademicTitle":
                            iname4 = '';
                            break;
                        case "MedicalTitle":
                            iname5 = '';
                            break;
                        case "MilitaryTitle":
                            iname2 = '';
                            break;
                        case "PrefixName":
                            iname1 = '';
                            break;
                        case "SecondTitle"://DR.
                            iname3 = '';
                            break;
                    }
                }
                if (iname4 != undefined && iname4 != '') {
                    $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.AcademicTitleTH, 'Key', iname4, 'Description') + " ";
                    $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.AcademicTitleEN, 'Key', iname4, 'Description') + " ";
                    if (iname2 != undefined && iname2 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MilitaryTitleTH, 'Key', iname2, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MilitaryTitleEN, 'Key', iname2, 'Description') + " ";
                    }
                    if (iname3 != undefined && iname3 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.SecondTitleTH, 'Key', iname3, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.SecondTitleEN, 'Key', iname3, 'Description') + " ";
                    }
                    if (iname1 != undefined && iname1 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                    }
                    if (iname5 != undefined && iname5 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                    }
                } else if (iname2 != undefined && iname2 != '') {
                    $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MilitaryTitleTH, 'Key', iname2, 'Description') + " ";
                    $scope.NameFormatTitleTEN = $scope.NameFormatTitleTEN + FilterDescription($scope.MilitaryTitleEN, 'Key', iname2, 'Description') + " ";
                    if (iname3 != undefined && iname3 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.SecondTitleTH, 'Key', iname3, 'Description') + " ";
                        $scope.NameFormatTitleTEN = $scope.NameFormatTitleTEN + FilterDescription($scope.SecondTitleEN, 'Key', iname3, 'Description') + " ";
                    }
                    if (iname1 != undefined && iname1 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                    }
                    if (iname5 != undefined && iname5 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                    }
                } else if (iname3 != undefined && iname3 != '') {
                    $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.SecondTitleTH, 'Key', iname3, 'Description') + " ";
                    $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.SecondTitleEN, 'Key', iname3, 'Description') + " ";
                    if (iname1 != undefined && iname1 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                    }
                    if (iname5 != undefined && iname5 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                    }
                } else if (iname1 != undefined && iname1 != '') {
                    $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.PrefixTH, 'Key', iname1, 'Description') + " ";
                    $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.PrefixEN, 'Key', iname1, 'Description') + " ";
                    if (iname5 != undefined && iname5 != '') {
                        $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                        $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                    }
                } else if (iname5 != undefined && iname5 != '') {
                    $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.MedicalTitleTH, 'Key', iname5, 'Description') + " ";
                    $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.MedicalTitleEN, 'Key', iname5, 'Description') + " ";
                } else {
                    $scope.NameFormatTitleTH = $scope.NameFormatTitleTH + FilterDescription($scope.TitleTH, 'Key', iname, 'Description') + " ";
                    $scope.NameFormatTitleEN = $scope.NameFormatTitleEN + FilterDescription($scope.TitleEN, 'Key', iname, 'Description') + " ";
                }

                $scope.document.Additional.PersonalData.FullName = $scope.NameFormatTitleTH + $scope.document.Additional.PersonalData.FirstName + " " + $scope.document.Additional.PersonalData.LastName;
                $scope.FullNameEN = $scope.NameFormatTitleEN + $scope.document.Additional.PersonalData.FirstNameEn + " " + $scope.document.Additional.PersonalData.LastNameEn;
            }

            $scope.getDisplayNameFormat = function (iname,iname_en, iname1, iname2, iname3, iname4, iname5) {
                $scope.NameFormatTitleTH = '';
                $scope.NameFormatTitleEN = '';
                var oRequestData =  $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);
               
                var oRequestParameter = {
                    InputParameter: {
                        "companyCode": oEmployeeData.CompanyCode,
                        "language": $scope.oUser.Language,
                        "iname_code": "INAME_CODE",
                        "iname_value": iname,
                        "iname_en_code": "INAME_EN_CODE",
                        "iname_en_value": iname_en,
                        "iname1_code": "INAME1_CODE",
                        "iname1_value": iname1,
                        "iname2_code": "INAME2_CODE",
                        "iname2_value": iname2,
                        "iname3_code": "INAME3_CODE",
                        "iname3_value": iname3,
                        "iname4_code": "INAME4_CODE",
                        "iname4_value": iname4,
                        "iname5_code": "INAME5_CODE",
                        "iname5_value": iname5
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetMTValidateTitleName/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.NameFormatTitleTH = response.data.TitleNameTH.title;
                    $scope.NameFormatTitleEN = response.data.TitleNameEN.title;

                    $scope.document.Additional.PersonalData.FullName = $scope.NameFormatTitleTH + $scope.document.Additional.PersonalData.FirstName + " " + $scope.document.Additional.PersonalData.LastName;
                    $scope.document.Additional.PersonalData.FullNameEN = $scope.NameFormatTitleEN + $scope.document.Additional.PersonalData.FirstNameEn + " " + $scope.document.Additional.PersonalData.LastNameEn;
                    console.log('success GetMTValidateTitleName.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error GetMTValidateTitleName.');
                });
            }

            $scope.openRequest = function (requestType) {
                $location.path('/frmViewContent/' + requestType);
            };

            $scope.onAfterValidateFunc = function (event, fileList) {
                console.log('file validated.', fileList);
                var newFile = {
                    FileID: -1,
                    IsDelete: false,
                    FileSetID: $scope.document.FileSetID,
                    FileName: fileList[0].filename,
                    Data: fileList[0].base64,
                    FileType: fileList[0].filetype,
                    FileSize: fileList[0].filesize
                };
                if ($scope.AllowUploadFile(newFile.FileName)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['VALIDATE_FILENAME'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILETYPE'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE2'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else {
                    $scope.document.FileSet.Attachments.push(newFile);
                    $scope.document.HasFileAttached = true;
                }
                newFile = null;
                $scope.formInput.file = null;
                angular.element("#OM_906_imgAnnounceCover").val(null);
            };

            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) {
                if (date != undefined) {
                    return $filter('date')(date, 'dd/MM/yyyy');
                } else {
                    return '';
                }
                //  return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }
            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            //#### OTHERS FUNCTION ### END
        }]);
})();