﻿(function () {
    angular.module('DESS')
        .controller('EducationEditor', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.isNewUnit = false;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
            var LevelSelected = null;
            var LevelCode = null;

            $scope.Textcategory = 'PA_EDUCATION_MANAGEMENT';
            $scope.param = [];
           

            


            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }


            $scope.init = function () {
                init();
               
            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
              
                $scope.GetEducationLevel();
                getPAConfiguration();
                $scope.EducationMainMajor();
                $scope.EducationMinor();
                $scope.EducationQualification();
                $scope.EducationInstitution();
                $scope.EducationDegree();
                $scope.EducationCountry();
            }



            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.ChildAction.SetData = function () {
                //Do something ...
              
                //     $scope.GetEducationLevel();
               
                //$scope.EducationMinor();
                //$scope.EducationQualification();
                //$scope.EducationInstitution();
                //$scope.EducationDegree();
                //$scope.EducationCountry();


                $scope.EducationData = $scope.document.Additional.Education;
                $scope.EducationDataOld = $scope.document.Additional.EducationOld;
                $scope.requesterData.EmployeeID = $scope.document.Additional.Education.EmployeeID;
                $scope.document.RequestorNo = $scope.document.Additional.Education.EmployeeID;
                $scope.document.RequestorPositionName = $scope.document.Additional.Education.RequestorPositionName;
                //if ($scope.param[0] == "EDIT") {
                //    $scope.document.RequestActionList = [];
                //}
                if ($scope.param[0] == "EDIT") {
                    getRequesNoforCheckHrEditDetail();
                }
              
                //$scope.EducationData.BeginDate = null;
                //$scope.EducationData.EndDate = new Date(9999, 11, 31, 0, 0, 0, 0);
                

                const StartDate = Date.now();

                $scope.isNewUnit = !$scope.EducationData.PAEduID ? true : false;
                if ($scope.isNewUnit) {
                    $scope.EducationData.Grade = null;
                }

                getPAConfiguration();
                



               
              
                    console.log('dont have data');
                    $scope.EducationData.BeginDate = $scope.EducationData.BeginDate;
                    $scope.EducationData.EndDate = $scope.EducationData.EndDate;
               

            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
               

                console.log('enter loaddata', $scope.EducationData.BeginDate);
                if ($scope.EducationData.BeginDate == null || $scope.EducationData.BeginDate == '' || $scope.EducationData.BeginDate == undefined) {
                    console.log('enter startdate = null');
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );

                    return false;

                } else {
                    $scope.EducationData.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                    $scope.EducationData.BeginDate = $scope.getDateFormatSave($scope.EducationData.BeginDate);
                    $scope.EducationData.EndDate = $scope.getDateFormatSave($scope.EducationData.EndDate);
                }
                $scope.requesterData.EmployeeID = $scope.document.Additional.Education.EmployeeID;
                $scope.document.RequestorPositionName = $scope.document.Additional.Education.RequestorPositionName;
            };

          

            

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config
                   
                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }


            function getRequesNoforCheckHrEditDetail() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        "DataGetegory": "PERSONALEDUCATION_" + $scope.EducationData.EducationLevelCode + "_" + $scope.EducationData.InstituteCode + "_" + $scope.EducationData.CertificateCode + "_" + $scope.EducationData.Branch1 + "_" + $scope.EducationData.PAEduID
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetHrEditDetailByRequestNo/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data.length > 0) {
                        if (oEmployeeData.EmployeeID != response.data[0].CreatorNo) {
                            $scope.document.RequestActionList = [];
                        }
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }




            //#### FRAMEWORK FUNCTION ### END

            //#### EducationLeval FUNCTION ### START
            $scope.GetEducationLevel = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetEducationLevelDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode":'EDU_LEVEL_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31'
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.EducationLevelList = response.data.oDDL;

                    console.log('success GetEducationLevel', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error GetEducationLevel', response.data.oDDL);
                });
            }
           


            $scope.EducationMainMajor = function () {

                if($scope.isNewUnit){
                    $scope.EducationData.CertificateCode = null;
                    $scope.EducationData.Branch1 = null;
                    $scope.EducationData.Branch2 = null;
                }
                if (typeof $scope.EducationData.EducationLevelCode !== "undefined") {
                    LevelSelected = $scope.EducationData.EducationLevelCode;
                    LevelCode = "EDU_LEVEL_CODE";
                } else {
                    LevelSelected = "0000";
                    LevelCode = "";
                }
                var URL = CONFIG.SERVER + 'HRPA/GetEducationMainMajorDLL';
               
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'MAJOR_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31',
                        "EduLevel": LevelCode,
                        "EduLevelSelected": LevelSelected
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MainMajorList = response.data.oDDL;
                    //$scope.Education.Branch1 = $scope.document.Additional.Education.Branch1;
                    //console.log('$scope.Education.Branch1', $scope.Education.Branch1)
                    $scope.EducationMinor();
                    $scope.EducationQualification();


                   
                    console.log('success EducationMainMajor', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error EducationMainMajor', response.data.oDDL);
                });
            }
     


            $scope.EducationMinor = function () {

                if ($scope.isNewUnit) {
                    $scope.EducationData.Branch2 = null;
                }
                
                if (typeof $scope.EducationData.EducationLevelCode !== "undefined") {
                    LevelSelected = $scope.EducationData.EducationLevelCode;
                    LevelCode = "EDU_LEVEL_CODE";
                } else {
                    LevelSelected = "0000";
                    LevelCode = "";
                }
                var URL = CONFIG.SERVER + 'HRPA/GetEducationMinorDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'MAJOR_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31',
                        "EduLevel": LevelCode,
                        "EduLevelSelected": LevelSelected
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MinorList = response.data.oDDL;
                    if (typeof $scope.EducationData.Branch1 !== "undefined") {
                        $scope.MinorList = $scope.MinorList.filter(function (item) {
                            return item.DLL_VALUE !== $scope.EducationData.Branch1;
                        })
                    }

                    console.log('main major 0069 =', $filter('filter')($scope.MinorList, { DLL_VALUE: '0069' }));
                    console.log('success EducationMinor', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error EducationMinor', response.data.oDDL);
                });
            }
            


            $scope.EducationQualification = function () {
                if (typeof $scope.EducationData.EducationLevelCode !== "undefined") {
                    LevelSelected = $scope.EducationData.EducationLevelCode;
                    LevelCode = "EDU_LEVEL_CODE";
                } else {
                    LevelSelected = "0000";
                    LevelCode = "";
                }

                var URL = CONFIG.SERVER + 'HRPA/GetEducationQualificationDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'CERTIFICATE_EDU_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31',
                        "EduLevel": LevelCode,
                        "EduLevelSelected": LevelSelected
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.QualificationList = response.data.oDDL;

                    console.log('success EducationQualification', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error EducationQualification', response.data.oDDL);
                });
            }
            



            $scope.EducationInstitution = function () {

                var URL = CONFIG.SERVER + 'HRPA/GetEducationInstitutionDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'INSTITUTE_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31',
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.InstituteList = response.data.oDDL;

                    console.log('success EducationInstitution', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error EducationInstitution', response.data.oDDL);
                });
            }
            


            $scope.EducationDegree = function () {

                var URL = CONFIG.SERVER + 'HRPA/GetEducationDegreeDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'SPECIAL_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31',
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.DegreeList = response.data.oDDL;

                    console.log('success EducationDegree', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error EducationDegree', response.data.oDDL);
                });
            }
          


            $scope.EducationCountry = function () {

                var URL = CONFIG.SERVER + 'HRPA/GetEducationCountryDLL';

                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "EduCode": 'COUNTRY_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31',
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CountryList = response.data.oDDL;

                    console.log('success EducationCountry', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error EducationCountry', response.data.oDDL);
                });
            }
          


            $scope.ChangeMinor = function () {

                $scope.EducationMinor();
            }


           
            //#### EducationLeval FUNCTION ### END









            //#### OTHERS FUNCTION ### START 

            $scope.GetOrgUnit = function () {
                var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "LevelSelected": '',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31'
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UnitList = response.data.oDDL;

                    console.log('success GetOrgUnit', response);
                }, function errorCallback(response) {
                        console.log('error GetOrgUnit', response);
                });
            }
            $scope.GetOrgUnit();

            $scope.GetBand = function () {
                var URL = CONFIG.SERVER + 'HROM/GetBandDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31'
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.BandList = response.data.oDDL;

                    console.log('success GetBand', response);
                }, function errorCallback(response) {
                    console.log('error GetBand', response);
                });
            }
            $scope.GetBand();

            //#region date format
            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            //$scope.Text = null;
            //$scope.RequestorText = null;
            //$scope.getAllTextDescription = function () {
            //    $scope.Text = null;
            //    var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
            //    var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        // Success
            //        if (response.data != null) {
            //            $scope.Text = response.data;
            //            // $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
            //        }
            //    }, function errorCallback(response) {
            //        // Error
            //        $scope.Text = null;
            //        console.log('error MainController TextDescription.', response);
            //    });
            //};
            //$scope.getAllTextDescription();







        }]);
})();