﻿(function () {
    angular.module('DESS')
        .controller('PersonalInfEditor', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.isNewUnit = false;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);


            $scope.Textcategory = 'PA_PERSONAL_INFO';
            $scope.param = [];





            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }


            $scope.init = function () {
                init();

            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
                getPAConfiguration();
                $scope.getDrpAcademic();
                $scope.getDrpMilitary();
                $scope.getDrpMedical();
                $scope.getDrpPrefix();
                $scope.getDrpTanundon();
                $scope.getDrpSubprefix();
                $scope.getDrpMarital();
                $scope.getDrpNationality();
                $scope.getDrpProvince();
                $scope.getDrpCountry();
                $scope.getDrpReligion();
            }



            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.ChildAction.SetData = function () {
                //Do something ...
                console.log('enter startdate = xxx');
                console.log('addionnal edua = ', $scope.document.Additional.Personal);
            };
            $scope.ChildAction.SetData();
            
            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                if ($scope.document.Additional.Personal.BeginDate == null || $scope.document.Additional.Personal.BeginDate == '' || $scope.document.Additional.Personal.BeginDate == undefined) {
                    console.log('enter startdate = null');
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );

                    return false;

                } else {
                    console.log('enter startdate = not null');
                    $scope.document.Additional.Trainning.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                    $scope.document.Additional.Trainning.BeginDate = $scope.getDateFormatSave($scope.document.Additional.Trainning.BeginDate);
                    $scope.document.Additional.Trainning.EndDate = $scope.getDateFormatSave($scope.document.Additional.Trainning.EndDate);
                }
                $scope.requesterData.EmployeeID = $scope.document.Additional.Trainning.EmployeeID;
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }







            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            //////////////////////////// new
            $scope.dDataType = "OMDL";
            $scope.dStartDate = "1900-01-01";
            $scope.dEndDate = "9999-12-31";
            /* Dropdown */
            $scope.getDrpPrefix = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "INAME_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PrefixList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpTanundon = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "INAME1_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TanundonList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpSubprefix = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "INAME3_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SubprefixList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpMilitary = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "INAME2_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MilitaryList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpMedical = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "INAME5_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MedicalList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpAcademic = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "INAME4_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.AcademicList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpMarital = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "MARITAL_STATUS_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MaritalList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpNationality = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "NATIONALITY_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.NationalityList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpProvince = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'MTLC',
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ProvinceList = response.data.oDDL;
                    //console.log("ProvinceList", $scope.ProvinceList)
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpCountry = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "COUNTRY_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CountryList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

            /* Dropdown */
            $scope.getDrpReligion = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetDrpDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": $scope.dDataType,
                        "BeginDate": $scope.dStartDate,
                        "EndDate": $scope.dEndDate,
                        "param1": "RELIGION_CODE",
                        "param2": ""
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ReligionList = response.data.oDDL;
                }, function errorCallback(response) {
                });
            }

        }]);
})();