﻿(function () {
    angular.module('DESS')
        .controller('EducationAcademicWorkMemberDeleteEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $mdDialog, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.loader.enable = true;
            $scope.CurrentEmployee = getToken(CONFIG.USER);
            $scope.CurrentAsOfDate = new Date();
            $scope.AsOfDate = $scope.CurrentAsOfDate;
            $scope.isNewUnit = false;
            $scope.isEditDelete = true;
            $scope.loader.enable = false;
            var oRequestData = $scope.requesterData
            var oEmployeeData = getToken(CONFIG.USER);
          

            $scope.Textcategory = 'PA_EDUCATION_MANAGEMENT';
            $scope.param = [];
            console.log('$scope.document.Additional',$scope.document.Additional);
            if ($routeParams.otherParam != undefined) {
                $scope.param = $routeParams.otherParam.split("|");
                if ($scope.param.length > 0 && $scope.param[0] == "EDIT") {
                    $scope.IsEdit = true;
                } else {
                    $scope.IsEdit = false;

                }
            }


            $scope.init = function () {
                init();
               
            }
            // #endregion ******************* listener end ********************/

            // #region ******************* action start ********************/
            function init() {
              
                $scope.UserroleDLL();
                $scope.PareDLL();
                $scope.GetSubtype();
                getPAConfiguration();
            }



            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.ChildAction.SetData = function () {
                //Do something ...
                $scope.requesterData.EmployeeID = $scope.document.Additional.Academicworkmember.EmpCode;
                $scope.document.RequestorNo = $scope.document.Additional.Academicworkmember.EmpCode;
                $scope.document.RequestorPositionName = $scope.document.Additional.Academicworkmember.RequestorPositionName;
                //     $scope.GetEducationLevel();
               
                //$scope.EducationMinor();
                //$scope.EducationQualification();
                //$scope.EducationInstitution();
                //$scope.EducationDegree();
                //$scope.EducationCountry();

            };
            $scope.ChildAction.SetData();

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                if ($scope.document.Additional.Academicworkmember.BeginDate == null || $scope.document.Additional.Academicworkmember.BeginDate == '' || $scope.document.Additional.Academicworkmember.BeginDate == undefined) {
                    console.log('enter startdate = null');
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM'].WARNING)
                            .textContent($scope.Text['SYSTEM'].REQUIRED_TEXT)
                            .ok($scope.Text['SYSTEM'].BUTTON_OK)
                    );

                    return false;

                } else {
                    $scope.document.Additional.Academicworkmember.CompanyCode = $scope.CurrentEmployee.CompanyCode;
                    $scope.document.Additional.Academicworkmember.BeginDate = $scope.getDateFormatSave($scope.document.Additional.Academicworkmember.BeginDate);
                    $scope.document.Additional.Academicworkmember.EndDate = $scope.getDateFormatSave($scope.document.Additional.Academicworkmember.EndDate);
                }
                $scope.requesterData.EmployeeID = $scope.document.Additional.Academicworkmember.EmpCode;
                $scope.document.RequestorPositionName = $scope.document.Additional.Academicworkmember.RequestorPositionName;
            };

            function getPAConfiguration() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data.Education;
                    //check Personaldata Title Config

                    console.log('success getPAConfiguration.');
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.');
                });
            }



            $scope.PareDLL = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetPareDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'PARE',
                        "Code": $scope.document.Additional.Academicworkmember.SubtypeValue,
                        "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
                        "EndDate": getDateFormateData($scope.CurrentAsOfDate),
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.pareList = response.data.oDDL;

                    console.log('success pareList', response.data.oDDL);
                }, function errorCallback(response) {
                    console.log('error pareList', response.data.oDDL);
                });
            }



            //$scope.PareDLL = function () {
            //    var URL = CONFIG.SERVER + 'HRPA/GetPareDLL';
            //    var oRequestParameter = {
            //        InputParameter: {
            //            "Type": 'PARE',
            //            "Code": '1',
            //            "BeginDate": getDateFormateData($scope.CurrentAsOfDate),
            //            "EndDate": getDateFormateData($scope.CurrentAsOfDate),
            //        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
            //    };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        $scope.pareList = response.data.oDDL;

            //        console.log('success pareList', response.data.oDDL);
            //    }, function errorCallback(response) {
            //            console.log('error pareList', response.data.oDDL);
            //    });
            //}
            $scope.UserroleDLL = function () {
                var URL = CONFIG.SERVER + 'HRPA/GetUserroleDLL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "Code": 'ROLE_CODE',
                        "BeginDate": '1900-01-01',
                        "EndDate": '9999-12-31',
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.UserroleList = response.data.oDDL;
                    console.log('success UserroleList', response.data.oDDL);
                }, function errorCallback(response) {
                        console.log('error UserroleList', response.data.oDDL);
                });
            }


            $scope.GetSubtype = function () {
                $scope.unitSearch = '';
                var URL = CONFIG.SERVER + 'HRPA/SubtypeDDL';
                var oRequestParameter = {
                    InputParameter: {
                        "Type": 'OMDL',
                        "BeginDate": "1900-01-01",
                        "EndDate": "9999-12-31",
                        "SUBTYPE": 'SUBTYPE_CODE'
                    }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SubTypeList = response.data.oDDL;
                    console.log('success SubTypeList', response);
                }, function errorCallback(response) {
                    console.log('error SubTypeList', response);
                });
            }


            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.getDateFormatSave = function (date) {
                return $filter('date')(new Date(date), 'yyyy-MM-ddT00:00:00');
            }

            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }
            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
        }]);
})();