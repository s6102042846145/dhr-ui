﻿(function () {
    angular.module('DESS')
        .controller('PersonalCorporateContentController', ['$scope', '$http', '$routeParams', '$location', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', '$window',
            function ($scope, $http, $routeParams, $location, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log, $window) {
                var oRequestData = $scope.requesterData
                var oEmployeeData = getToken(CONFIG.USER);
                $scope.CurrentAsOfDate = new Date();
                $scope.AsOfDate = $scope.CurrentAsOfDate;
                $scope.loader.enable = false;
                $scope.profile = getToken(CONFIG.USER);
                $scope.LevelSelected = [];
                $scope.UnitSelected = [];
                $scope.PositionSelected = [];
                $scope.EmployeeData = [];
                $scope.isLastRecord = false; // flag สำหรับดุว่าข้อมูลหน่วยงานที่เลือก เป็นอันสุดท้ายรึเปล่า
                $scope.isDisable = true;
           





                /******************* variable start ********************/
                $scope.profile = (getToken(CONFIG.USER));
                $scope.UserImg = $scope.profile.ImageUrl + "?" + new Date().getTime();
                $scope.Textcategory = "PA_EDUCATION_MANAGEMENT";
                $scope.content.isShowHeader = true;
                $scope.PAConfigPersonalTitleFormat = [];
                $scope.Name_EmpID = $scope.requesterData.EmployeeID + ' ' + $scope.requesterData.Name;
                $scope.FirstTabIndex = '';
                $scope.EmpCode = '';
                $scope.EmpNameTH = '';
                $scope.PosCode = '';
                $scope.PositionTextTH = '';
                $scope.UnitCode = '';
                $scope.UnitTextTh = '';
                $scope.BandValue = '';
                $scope.BandTextTH = '';
                $scope.UnitLevelValue = '';
                $scope.UnitLevelTextTH = '';

                $scope.StartDateText = null;
                $scope.EndDateText = null;
                $scope.EducationLevelText = '';
                $scope.CertificateText = '';
                $scope.MajorText = '';
                $scope.MinorText = '';
                $scope.InstitutionText = '';
                $scope.CountryText = '';
                $scope.HonorText = '';
                $scope.GradeText = '';

              





               
               


                $scope.init = function () {
                    init();
                    GetAdminSelectEmp();
                }
                // #endregion ******************* listener end ********************/

                // #region ******************* action start ********************/
                function init() {
                    getPAConfiguration();
                    //$scope.getAllTextDescription();
                    $scope.GetOrgLevel();
                    $scope.GetOrgUnit();
                    $scope.GetSubtype();
                    
                   // $scope.GetPosition();

                }



                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion

                //#region date format

                $scope.getDateFormat = function (date) {
                    return $filter('date')(date, 'dd/MM/yyyy');
                }

                $scope.getDateFormate = function (date) {
                    return getDateFormate(date);
                }
                function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }

                $scope.getDateFormateData = function (date) {
                    return getDateFormateData(date);
                }

                function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
                //#endregion

                //#endregion

                function getPAConfiguration() {
                    $scope.loader.enable = true;
                    var oEmployeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PersonalConfig = response.data.Personal;
                        $scope.AddressConfig = response.data.Address;
                        $scope.BankConfig = response.data.Bank;
                        $scope.FamilyConfig = response.data.Family;
                        $scope.CommuConfig = response.data.Communication;
                        $scope.EducationConfig = response.data.Education;
                        $scope.TaxAllowanceConfig = response.data.TaxAllowance;
                        $scope.TabQualiConfig = response.data.TabPAQuali;
                        $scope.TabContentConfig = response.data.TabContent;
                        $scope.DocumentConfig = response.data.Document;
                        $scope.AcademicConfig = response.data.Academic;
                        $scope.TabPACoparateConfig = response.data.TabPACoparate;
                        //check Personaldata Title Config
                        for (var i = 0; i < $scope.PersonalConfig.length; i++) {
                            switch ($scope.PersonalConfig[i].FieldName) {
                                case "TitleID":
                                case "AcademicTitle":
                                case "MedicalTitle":
                                case "MilitaryTitle":
                                case "PrefixName":
                                case "SecondTitle"://DR.
                                    if (!$scope.PersonalConfig[i].IsVisible) {
                                        $scope.PAConfigPersonalTitleFormat.push($scope.PersonalConfig[i].FieldName);
                                    }
                                    break;
                                default:
                            }
                        }
                        if ($scope.TabPACoparateConfig.length > 0) {
                            var FirstTabIndex = $scope.TabPACoparateConfig[0].FieldName;
                            $scope.getDataByTab(FirstTabIndex);
                            $scope.FirstTabIndex = FirstTabIndex;
                        }
                        //$scope.DateCutoffApproveBankAccount = response.data.DateCutoffApproveBankAccount;
                        // $scope.BankAccountRemark = $scope.Text['REMARK'].BANK_ACCOUNT.replace('{0}', $scope.DateCutoffApproveBankAccount);
                        console.log('getPAConfiguration success.');
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        console.log('getPAConfiguration error.');
                        $scope.loader.enable = false;
                    });
                }
                //function getPAConfiguration() {
                //    $scope.loader.enable = true;
                //    var oEmployeeData = getToken(CONFIG.USER);
                //    var oRequestParameter = {
                //        InputParameter: { "CompanyCode": oEmployeeData.CompanyCode }
                //        , CurrentEmployee: getToken(CONFIG.USER)
                //        , Requestor: $scope.requesterData
                //    };
                //    var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //        $scope.TabQualiConfig = response.data.TabPAQuali;
                //        $scope.TabPACoparateConfig = response.data.TabPACoparate;
                        
                //        //check Personaldata Title Config
                        
                //        if ($scope.TabPACoparateConfig.length > 0) {
                //            var FirstTabIndex = $scope.TabPACoparateConfig[0].FieldName;
                //            $scope.getDataByTab(FirstTabIndex);
                //            $scope.FirstTabIndex = FirstTabIndex;
                //        }
                //        //$scope.DateCutoffApproveBankAccount = response.data.DateCutoffApproveBankAccount;
                //       // $scope.BankAccountRemark = $scope.Text['REMARK'].BANK_ACCOUNT.replace('{0}', $scope.DateCutoffApproveBankAccount);
                //        console.log('getPAConfiguration success.');
                //        $scope.loader.enable = false;
                //    }, function errorCallback(response) {
                //        console.log('getPAConfiguration error.');
                //        $scope.loader.enable = false;
                //    });
                //}


                //$scope.Text = null;
                //$scope.RequestorText = null;
                //$scope.getAllTextDescription = function () {
                //    $scope.Text = null;
                //    var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                //    var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //         Success
                //        if (response.data != null) {
                //            $scope.Text = response.data;
                //            $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
                //        }
                //    }, function errorCallback(response) {
                //         Error
                //        $scope.Text = null;
                //        console.log('error MainController TextDescription.', response);
                //    });
                //};
                //$scope.getAllTextDescription();


                $scope.getDataByTab = function (TapClick) {
                    switch (TapClick) {
                        case "TabQualiEducation":

                            break;
                        case "TabQualiWorkHistory":

                            break;
                        case "TabQualiTraininghistory ":

                            break;
                        case "TabQualiresearchMember":

                            break;
                        case "TabQualiAttachment":

                            break;

                        default:
                    }
                };

                $scope.GetOrgLevel = function () {
                    var URL = CONFIG.SERVER + 'HROM/GetOrgLevelDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.LevelList = response.data;

                        console.log('success GetOrgLevel', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgLevel', response);
                    });
                }
                $scope.GetOrgLevel();

                $scope.GetOrgUnit = function () {
                    $scope.levelSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetOrgUnitDDL';

                    for (var i = 0; i < $scope.LevelSelected.length; i++) {
                        if ($scope.LevelSelected.length > 1) {
                            $scope.levelSearch += $scope.LevelSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.levelSearch = $scope.LevelSelected[0].DLL_VALUE;
                        }
                    }

                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": $scope.levelSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.UnitList = response.data.oDDL;
                        $scope.GetEmployee();
                        console.log('success GetOrgUnit', response);
                    }, function errorCallback(response) {
                        console.log('error GetOrgUnit', response);
                    });
                }
                $scope.GetOrgUnit();

                $scope.GetPosition = function (unit) {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HROM/GetPositionDDL';

                    for (var i = 0; i < $scope.UnitSelected.length; i++) {
                       
                        if ($scope.UnitSelected.length > 1) {
                            $scope.unitSearch += $scope.UnitSelected[i].DLL_VALUE + ':';
                        }
                        else {
                            $scope.unitSearch = $scope.UnitSelected[0].DLL_VALUE;
                        }
                    }

                   

                    var oRequestParameter = {
                        InputParameter: {
                            "UnitSelected": $scope.unitSearch,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionList = response.data.oDDL;;
                        $scope.GetEmployee();
                        console.log('success GetPosition', response);
                    }, function errorCallback(response) {
                        console.log('error GetPosition', response);
                    });
                }
                $scope.GetPosition();

                //Start GetFile
                //END GetFile
                function getRequestTypeFileSet(requestType, EmployeeID) {
                    if (EmployeeID != null || EmployeeID != '' || EmployeeID != undefined) {
                        $scope.requesterData.EmployeeID = EmployeeID;
                    }
                    var oEmployeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { "EmployeeID": "", "RequestTypeID": requestType, "RequestSubType": "" }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'Workflow/GetRequestTypeFileSet/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (requestType == '2201') {
                            $scope.FileAttachment2201List = response.data;
                            console.log('$scope.FileAttachment2201List', $scope.FileAttachment2201List);
                        }
                        if (requestType == '2203') {
                            $scope.FileAttachment2203List = response.data;
                            console.log('$scope.FileAttachment2203List', $scope.FileAttachment2203List);
                        }
                        if (requestType == '2205') {
                            $scope.FileAttachment2205List = response.data;
                            console.log('$scope.FileAttachment2205List', $scope.FileAttachment2205List);
                        }
                        if (requestType == '2207') {
                            $scope.FileAttachment2207List = response.data;
                            console.log('$scope.FileAttachment2207List', $scope.FileAttachment2207List);
                        }
                        
                       
                    }, function errorCallback(response) {
                        // Error
                        console.log('error getRequestTypeFileSet.', response);
                    });
                }


                

                $scope.GetEmployee = function (position) {
                    $scope.PositionSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/GetEducationEmployeeDLL';
                    
                        for (var i = 0; i < $scope.PositionSelected.length; i++) {
                            if ($scope.PositionSelected.length > 1) {
                                $scope.PositionSearch += $scope.PositionSelected[i].DLL_VALUE + ':';
                            }
                            else {
                                $scope.PositionSearch = $scope.PositionSelected[0].DLL_VALUE;
                            }
                        }
                    
                    
                    

                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'PAOR',
                            "BeginDate": "1900-01-01",
                            "EndDate": "9999-12-31",
                            "UnitLevelValue": $scope.levelSearch,
                            "UnitCode": $scope.unitSearch,
                            "PostCode": $scope.PositionSearch
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeList = response.data.oDDL;;

                        console.log('success GetPosition', response);
                    }, function errorCallback(response) {
                        console.log('error GetPosition', response);
                    });
                }
                $scope.GetEmployee();

                $scope.GetSubtype = function () {
                    $scope.unitSearch = '';
                    var URL = CONFIG.SERVER + 'HRPA/SubtypeDDL';
                    var oRequestParameter = {
                        InputParameter: {
                            "Type": 'OMDL',
                            "BeginDate":  "2000-01-01",
                            "EndDate": "9999-12-31",
                            "SUBTYPE": 'SUBTYPE_CODE'
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.SubTypeList = response.data.oDDL;
                        console.log('success SubTypeList', response);
                    }, function errorCallback(response) {
                       console.log('error SubTypeList', response);
                    });
                }
                




                $scope.SearchEmployeeData = function () {
                    $scope.loader.enable = true;
                    $scope.EmployeeSearch = '';
                        for (var i = 0; i < $scope.EmployeeSelected.length; i++) {
                            if ($scope.EmployeeSelected.length > 1) {
                                $scope.EmployeeSearch += $scope.EmployeeSelected[i].DLL_VALUE + ':';
                            } else {
                                $scope.EmployeeSearch = $scope.EmployeeSelected[0].DLL_VALUE;
                            }
                        }
                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                            InputParameter: {
                                "LevelSelected": $scope.levelSearch,
                                "UnitSelected": $scope.unitSearch,
                                "PositionSelected": $scope.PositionSearch,
                                "EmpCode": ($scope.EmployeeSearch) ? $scope.EmployeeSearch : "",
                                "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                                "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                            }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmployeeDataList = response.data;
                        console.log('success GetDataContent', response);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetDataContent', response);
                    });
                };
                $scope.SetIsAllPeriod = function () {
                    if ($scope.AllPeriod) {
                        $scope.AsOfDate = '';
                    } else {
                        $scope.AsOfDate = angular.copy($scope.CurrentAsOfDate);
                    }
                }
                $scope.SetCurrentTab = function (currentTab) {
                    $scope.CurrentTab = currentTab;
                    $scope.ResetDataContent();
                }




                $scope.ViewDetailData = function (data) {
                    console.log("bbbbbbbbaaaaaaaaaa", data);
                    //$scope.EmpCode = data.EmpCode;
                    //$scope.EmpNameTH = data.EmpNameTH;
                    //$scope.PosCode = data.PosCode;
                    //$scope.PositionTextTH = data.PositionTextTH;
                    //$scope.UnitCode = data.UnitCode;
                    //$scope.UnitTextTh = data.UnitTextTh;
                    //$scope.BandValue = data.BandValue;
                    //$scope.BandTextTH = data.BandTextTH;
                    //$scope.UnitLevelValue = data.UnitLevelValue;
                    //$scope.UnitLevelTextTH = data.UnitLevelTextTH;

                    $scope.loader.enable = true;
                    var URL = CONFIG.SERVER + 'HRPA/GetEmployeeDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "LevelSelected": "",
                            "UnitSelected": "",
                            "PositionSelected": data.PosCode,
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        SetLastAdminSelectEmp(response.data[0]);
                        GetPositionHisDetaillById(response.data[0]);
                        GetPersonalOrganizationInfoDetailById(response.data[0]);
                        GetPersonalActingPositionDetailById(response.data[0]);
                        GetPersonalImportanceDateDetailById(response.data[0]);
                        SetEmployeeDetail(response.data[0]);
                        $scope.loader.enable = false;
                        console.log('success GetMarkDataWithRequestType', response);
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error GetMarkDataWithRequestType', response);
                    });
                };
               

                function SetLastAdminSelectEmp(data) {
                    var URL = CONFIG.SERVER + 'HRPA/SetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "SET",
                            "EmpCode": data.EmpCode,
                            "PosCode": data.PosCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        
                    });
                }


                function GetAdminSelectEmp() {
                    var URL = CONFIG.SERVER + 'HRPA/GetCheckViewEmployee';
                    var oRequestParameter = {
                        InputParameter: {
                            "ActionType": "GET",
                            "EmpCode": "",
                            "PosCode": "",
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.EmpCode = response.data[0].EmpCode;
                        getRequestTypeFileSet('2201', $scope.EmpCode);
                        getRequestTypeFileSet('2203', $scope.EmpCode);
                        getRequestTypeFileSet('2205', $scope.EmpCode);
                        getRequestTypeFileSet('2207', $scope.EmpCode);
                        $scope.ViewDetailData(response.data[0]);
                    });
                }

                $scope.ResetDataContent = function () {
                    
                    $scope.PositionSelected = [];
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                    $scope.EmployeeSelected = [];
                   
                }




                //Start GetDateFromTab
                //GetEducation
                function GetPositionHisDetaillById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetPersonalPositionHisDetail';
                    var oRequestParameter = {
                        InputParameter: {
                           
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.PositionhisList = response.data;
                        console.log('$scope.PositionhisList', $scope.PositionhisList);
                      
                    });
                }

                //GetWorkHistory
                function GetPersonalOrganizationInfoDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GeOrganizationByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.OrganizationinfoList = response.data;
                        console.log('$scope.WorkHistoryList', $scope.WorkHistoryList);

                    });
                }

                function GetPersonalActingPositionDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetActingPositionByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {

                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.TrainingList = response.data;
                        console.log('$scope.TrainingList', $scope.TrainingList);

                    });
                }


                function GetPersonalImportanceDateDetailById(data) {
                    var URL = CONFIG.SERVER + 'HRPA/GetImportanceDateByEmpCode';
                    var oRequestParameter = {
                        InputParameter: {
                            "EmpCode": data.EmpCode,
                            "BeginDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate),
                            "EndDate": ($scope.AllPeriod) ? "9999-12-31" : getDateFormateData($scope.CurrentAsOfDate)
                        }, CurrentEmployee: oEmployeeData, Requestor: oRequestData
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.ImportanceDateList = response.data;
                        
                        console.log('$scope.Acedemicworkmemberlist', $scope.Acedemicworkmemberlist);

                    });
                }
                //Start GetDateFromTab


                
                $scope.ViewEducationDetail = function (data) {
                    console.log('ViewEducationDetail', data);
                    $scope.StartDateText = data.BeginDate;
                    $scope.EndDateText = data.EndDate;
                    $scope.EducationLevelText = data.EducationLevelTextTH;
                    $scope.CertificateText = data.CertificateTextTH;
                    $scope.MajorText = data.Branch1TextTH;
                    $scope.MinorText = data.Branch2TextTH;
                    $scope.InstitutionText = data.InstituteTextTH;
                    $scope.CountryText = data.CountryTextTH;
                    $scope.HonorText = data.HonorTextTH;
                    $scope.GradeText = data.Grade;
                }


                $scope.ViewOrganizationDetail = function (data) {
                    console.log('ViewOrganizationDetail', data);
                    $scope.OrganizStartDateText = data.BeginDate;
                    $scope.OrganizEndDateText = data.EndDate;
                    $scope.PosTextTH = data.PosTextTH;
                    $scope.UnitTextTH = data.UnitTextTH;
                    $scope.EmpTypeTextTH = data.EmpTypeTextTH;
                    $scope.CostCenterValue = data.CostCenterValue;
                    $scope.CostCenterText = data.CostCenterText;
                    $scope.BandTextTH = data.BandTextTH;
                    $scope.LevelTextTH = data.LevelTextTH;
                    $scope.ActingBandTextTH = data.ActingBandTextTH;
                    $scope.ActingLevelTextTH = data.ActingLevelTextTH;
                    $scope.CountryWorkTextTH = data.CountryWorkTextTH;
                    $scope.WorkPlaceTextTH = data.WorkPlaceTextTH;
                    $scope.WorkAreaTextTH = data.WorkAreaTextTH;
                    $scope.HrAdminTextTH = data.HrAdminTextTH;

                    



                }

                $scope.ViewImportanceDateDetail = function (data) {
                    console.log('ViewImportanceDateDetail', data);
                    $scope.ImportanceStartDateText = data.BeginDate;
                    $scope.ImportanceEndDateText = data.EndDate;


                    $scope.StartWorkDate = data.StartWorkDate == '0001-01-01T00:00:00' ? '' : data.StartWorkDate;
                    $scope.EmployedDate = data.EmployedDate == '0001-01-01T00:00:00' ? '' : data.EmployedDate;
                    $scope.CountLeaveAgeDate = data.CountLeaveAgeDate == '0001-01-01T00:00:00' ? '' : data.CountLeaveAgeDate;
                    $scope.RegistFundDate = data.RegistFundDate == '0001-01-01T00:00:00' ? '' : data.RegistFundDate;
                    $scope.CountFundAgeDate = data.CountFundAgeDate == '0001-01-01T00:00:00' ? '' : data.CountFundAgeDate;
                    $scope.StartWorkGroupDate = data.StartWorkGroupDate == '0001-01-01T00:00:00' ? '' : data.StartWorkGroupDate;
                    $scope.LeavePfFundDate1 = data.LeavePfFundDate1 == '0001-01-01T00:00:00' ? '' : data.LeavePfFundDate1;
                    $scope.LeavePfFundDate2 = data.LeavePfFundDate2 == '0001-01-01T00:00:00' ? '' : data.LeavePfFundDate2;
                    $scope.LeavePfFundDate3 = data.LeavePfFundDate3 == '0001-01-01T00:00:00' ? '' : data.LeavePfFundDate3;
                    $scope.RetireDate = data.RetireDate == '0001-01-01T00:00:00' ? '' : data.RetireDate;
                    $scope.CountLevelAgeDate = data.CountLevelAgeDate == '0001-01-01T00:00:00' ? '' : data.CountLevelAgeDate;
                    $scope.CountLevelAgeGroupDate = data.CountLevelAgeGroupDate == '0001-01-01T00:00:00' ? '' : data.CountLevelAgeGroupDate;
                    $scope.CountPosAgeDate = data.CountPosAgeDate == '0001-01-01T00:00:00' ? '' : data.CountPosAgeDate;
                    $scope.CountUnitAgeDate = data.CountUnitAgeDate == '0001-01-01T00:00:00' ? '' : data.CountUnitAgeDate;
                    $scope.CountRootJobAgeDate = data.CountRootJobAgeDate == '0001-01-01T00:00:00' ? '' : data.CountRootJobAgeDate;
                   
                    $scope.AgeStartWorkDate = data.StartWorkDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.StartWorkDate));
                    $scope.AgeEmployedDate = data.EmployedDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.EmployedDate));
                    $scope.AgeCountLeaveAgeDate = data.CountLeaveAgeDate == '0001-01-01T00:00:00' ? '' :  getAge(getDateFormateData(data.CountLeaveAgeDate));
                    $scope.AgeRegistFundDate = data.RegistFundDate == '0001-01-01T00:00:00' ? '' :  getAge(getDateFormateData(data.RegistFundDate));
                    $scope.AgeCountFundAgeDate = data.CountFundAgeDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.CountFundAgeDate));
                    $scope.AgeStartWorkGroupDate = data.StartWorkGroupDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.StartWorkGroupDate));
                    $scope.AgeLeavePfFundDate1 = data.LeavePfFundDate1 == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.LeavePfFundDate1));
                    $scope.AgeLeavePfFundDate2 = data.LeavePfFundDate2 == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.LeavePfFundDate2));
                    $scope.AgeLeavePfFundDate3 = data.LeavePfFundDate3 == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.LeavePfFundDate3));
                    $scope.AgeRetireDate = data.RetireDate == '0001-01-01T00:00:00' ? '' : getAgeRetire(getDateFormateData(data.RetireDate));
                    $scope.AgeCountLevelAgeDate = data.CountLevelAgeDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.CountLevelAgeDate));
                    $scope.AgeCountLevelAgeGroupDate = data.CountLevelAgeGroupDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.CountLevelAgeGroupDate));
                    $scope.AgeCountPosAgeDate = data.CountPosAgeDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.CountPosAgeDate));
                    $scope.AgeCountUnitAgeDate = data.CountUnitAgeDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.CountUnitAgeDate));
                    $scope.AgeCountRootJobAgeDate = data.CountRootJobAgeDate == '0001-01-01T00:00:00' ? '' : getAge(getDateFormateData(data.CountRootJobAgeDate));

                }


                $scope.onSubtypeChange = function () {
                    $scope.subtype;
                    if ($scope.subtype == 'All') {
                        $scope.Acedemicworkmemberlist = $scope.Acedemicworkmembertmplist;
                    } else {
                        $scope.Acedemicworkmemberlist = $scope.Acedemicworkmembertmplist;
                        $scope.Acedemicworkmemberlist = $filter('filter')($scope.Acedemicworkmemberlist, { SubtypeValue: $scope.subtype });
                    }
                }; 


                


                function SetEmployeeDetail(data) {
                  

                    $scope.EmpCode = data.EmpCode;
                    $scope.EmpNameTH = data.EmpNameTH;
                    $scope.PosCode = data.PosCode;
                    $scope.PositionTextTH = data.PositionTextTH;
                    $scope.UnitCode = data.UnitCode;
                    $scope.UnitTextTh = data.UnitTextTh;
                    $scope.BandValue = data.BandValue;
                    $scope.BandTextTH = data.BandTextTH;
                    $scope.UnitLevelValue = data.UnitLevelValue;
                    $scope.UnitLevelTextTH = data.UnitLevelTextTH;

                    



                }

                

                $scope.ResetDataContent = function () {
                    $scope.AllPeriod = false;
                    $scope.CurrentAsOfDate = new Date();
                    $scope.LevelSelected = [];
                    $scope.UnitSelected = [];
                    $scope.PositionSelected = [];
                    $scope.levelSearch = '';
                    $scope.unitSearch = '';
                    $scope.positionSearch = '';
                    $scope.SolidMasterDataList = [];
                    $scope.DottedMasterDataList = [];

                };




                //$scope.ViewDetailData = function (data) {
                //    //get data from API and view in modal
                //    //#region HROMPOSITION_EDIT_xxxxxxxx
                //    $scope.PositionInfoEditRequest = [];
                //    var dataCategory = 'HROMPOSITION_EDIT_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocEdit, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoEditRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_DELETE_xxxxxxxx
                //    $scope.PositionInfoDeleteRequest = [];
                //    var dataCategory = 'HROMPOSITION_DELETE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocDelete, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoDeleteRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_PERDELETE_xxxxxxxx
                //    $scope.PositionInfoPerDeleteRequest = [];
                //    var dataCategory = 'HROMPOSITION_PERDELETE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocPerDelete, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoPerDeleteRequest.push(result[0]);
                //    }
                //    //#endregion

                //    //#region HROMPOSITION_INACTIVE_xxxxxxxx
                //    $scope.PositionInfoInactiveRequest = [];
                //    var dataCategory = 'HROMPOSITION_INACTIVE_' + data.PosCode;
                //    var result = $filter('filter')($scope.oCreateDocInactive, { DataCategory: dataCategory });
                //    if (result.length > 0) {
                //        $scope.PositionInfoInactiveRequest.push(result[0]);
                //    }
                //    //#endregion



                //    $scope.PositionDetail = angular.copy(data);
                //    $scope.UnitName = filterTextDescription($scope.UnitList, 'DLL_VALUE', $scope.PositionDetail.UnitCode, 'DLL_DATA');
                //};


                //#region Utility function
                $scope.sort = {
                    column: '',
                    descending: false
                };

                $scope.changeSorting = function (column) {
                    var sort = $scope.sort;
                    if (sort.column == column) {
                        sort.descending = !sort.descending;
                    } else {
                        sort.column = column;
                        sort.descending = false;
                    }
                };


                //#region Paging
                $scope.pagin = {
                    currentPage: 1, itemPerPage: '8', numPage: 1
                }
                $scope.genDatas = function (obj) {
                    var datas = obj;
                    setNumOfPage(datas);
                    var res = [];
                    if ($scope.pagin.numPage > 1) {
                        datas.forEach(function (value, i) {
                            var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                            var to = from + parseInt($scope.pagin.itemPerPage);

                            if (from <= i && to > i)
                                res.push(value);
                        });
                    } else {
                        if (datas != null) {
                            res = datas.slice();
                        }
                    }

                    return res;
                }
                var setNumOfPage = function (datas) {
                    var totalItems = 0;
                    if (datas) totalItems = datas.length;
                    $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                    if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                    if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                }
                $scope.changePage = function (targetPage) {
                    $scope.pagin.currentPage = angular.copy(targetPage);
                }
                $scope.getPages = function () {
                    var pages = [];

                    for (var i = 0; i < $scope.pagin.numPage; i++) {
                        pages.push((i + 1));
                    }

                    return pages;
                }
                //#endregion




                $scope.directToRequest = function (requestTypeID, actionType,EmployeeID, PAID) {
                    $scope.fromBeginDate = new Date();;
                    $scope.fromEndDate = new Date(9999, 11, 31, 0, 0, 0, 0);
                    if (actionType == 'NEWID') {
                        if (requestTypeID == '2207') {
                            if ($scope.subtype == 'All' || $scope.subtype == undefined || $scope.subtype == '') {
                                $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text['SYSTEM'].WARNING)
                                        .textContent($scope.Text['PA_EDUCATION_MANAGEMENT'].REQUESTACADEMICWORKMEMBER)
                                        .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                );
                                return false;
                            } else {
                                $scope.CreateNew('2207', 'NEWID' + '|' + EmployeeID + '|' + $scope.subtype);
                            }
                        }
                    }else if (actionType == 'EDIT') {
                        if (requestTypeID == '2201') {
                            $scope.CreateNew('2201', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                        }
                        if (requestTypeID == '2203') {
                            $scope.CreateNew('2203', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                        }
                        if (requestTypeID == '2205') {
                            $scope.CreateNew('2205', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                        }
                        if (requestTypeID == '2207') {
                            $scope.CreateNew('2207', 'EDIT' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                        }
                    } else if (actionType == 'CANCEL'){
                       if (requestTypeID == '2202') {
                           $scope.CreateNew('2202', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                       }
                       if (requestTypeID == '2204') {
                           $scope.CreateNew('2204', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                       }
                       if (requestTypeID == '2206') {
                           $scope.CreateNew('2206', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                       }
                       if (requestTypeID == '2208') {
                           $scope.CreateNew('2208', 'CANCEL' + '|' + EmployeeID + '|' + '-' + '|' + PAID);
                       }
                    }
                };

                $scope.getFileFromPath = function (attachment) {
                    /* direct */
                    if (angular.isDefined(attachment)) {
                        if (typeof cordova != 'undefined') {

                        } else if (attachment.FilePath) {
                            var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                            console.debug(path);
                            $window.open(path);
                        } else {
                            var path = CONFIG.SERVER + 'Client/files/' + attachment;
                            console.debug(path);
                            $window.open(path);

                        }
                    }
                    /* !direct */
                }

                function getAge(bob) {

                    var DOB = new Date(bob);
                    var today = new Date();
                    var age = today.getTime() - DOB.getTime();
                    var elapsed = new Date(age);
                    var year = elapsed.getYear() - 70;
                    var month = elapsed.getMonth();
                    var day = elapsed.getDay();
                    var ageTotal = year + " ปี " + month + " เดือน " + day + " วัน";
                    //console.log("xxx : " + ageTotal)
                    return ageTotal;
                }
                function getAgeRetire(bob) {

                    var DOB = new Date(bob);
                    var today = new Date();
                    var age = today.getTime() - DOB.getTime();
                    var elapsed = new Date(age);
                    var year = 70 - elapsed.getYear();
                    
                    var month = elapsed.getMonth();
                    var day = elapsed.getDay();
                    if (year < 1) {
                        year = 0;
                    }
                    if (month < 1) {
                        month = 0;
                    }
                    if (day < 1) {
                        day = 0;
                    }
                    var ageTotal = year + " ปี " + month + " เดือน " + day + " วัน";
                    //console.log("xxx : " + ageTotal)
                    return ageTotal;
                }
                

             

            }]);
})();