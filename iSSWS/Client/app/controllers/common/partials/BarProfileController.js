﻿(function () {
    angular.module('DESS')
        .controller('BarProfileController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', '$rootScope', '$timeout', function ($scope, $http, $routeParams, $location, CONFIG, $rootScope, $timeout) {
            $scope.profile = (getToken(CONFIG.USER));
            $scope.profile.ImageUrl = $scope.profile.ImageUrl + "?" + new Date().getTime();
            $scope.CompanyLogo = localStorage.getItem('CurrentCompanyLogo') == '' ? $scope.profile.CompanyDetail.Logo : localStorage.getItem('CurrentCompanyLogo');
            $scope.navigateTo = function (url) {
                $location.path(url);
            };
            //Nun Add
            $scope.settings = {
                PositionTH: '',
                PositionEN: ''

            };

            $scope.isNotCreateorViewerDocument = function () {
                if (window.location.href.toUpperCase().includes('EDITREQUEST')) { // frmEditRequest frmViewRequest
                    return false;
                } else {
                    return true;
                }
            };

            $scope.$watch('PROFILE_SETTING.POSITIONID', function (newObj, oldObj) {
                var oSetPosition;
                if ($scope.PROFILE_SETTING.POSITIONID != '') {
                    oSetPosition = $scope.PROFILE_SETTING.POSITIONID;
                }
                else {
                    if ($scope.profile != null) {
                        oSetPosition = $scope.profile.PositionID;
                    }
                }
                if (angular.isDefined(newObj) && newObj != null) {
                    if ($scope.profile != null && $scope.profile.AllPosition) {
                        for (var i = 0; i < $scope.profile.AllPosition.length; i++) {
                            if (oSetPosition == $scope.profile.AllPosition[i].ObjectID) {
                                $scope.settings.PositionTH = $scope.profile.AllPosition[i].ShortText;
                                $scope.settings.PositionEN = $scope.profile.AllPosition[i].ShortTextEn;
                                break;
                            }
                        }
                    }
                }
            });

            //decode html encode
            $scope.createHtml = function (htmlCode) {
                var txt = document.createElement("textarea");
                txt.innerHTML = htmlCode;
                return txt.value;
            };

            $scope.myFunction = function () {
                document.getElementById("CORE_btn_ddlLanguagechange").classList.toggle("show");
            };

            // Close the dropdown menu if the user clicks outside of it
            window.onclick = function (event) {
                if (!event.target.matches('.ddl-language-cng')) {
                    var dropdowns = document.getElementsByClassName("dropdown-content");
                    var i;
                    for (i = 0; i < dropdowns.length; i++) {
                        var openDropdown = dropdowns[i];
                        if (openDropdown.classList.contains('show')) {
                            openDropdown.classList.remove('show');
                        }
                    }

                }
            }

            $scope.reloadPageLgChange = function (language) {
                //var employeeData = getToken(CONFIG.USER);
                //var URL = CONFIG.SERVER + 'workflow/GetEmployeeData/' + language;

                var URL = CONFIG.SERVER + 'workflow/GetEmployeeData';
                var oRequestParameter = { InputParameter: { "Language": language }, CurrentEmployee: getToken(CONFIG.USER) };

                // change EmployeeData
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    window.localStorage.removeItem(CONFIG.USER);
                    window.localStorage.setItem(CONFIG.USER, JSON.stringify(response.data.EmployeeData));
                    console.log('success changeLanguage.', getToken(CONFIG.USER));

                    // Reload Menu
                    var args = {};

                    // $timeout
                    $scope.getAllTextDescription();
                    $scope.initialSystem();

                    $rootScope.$broadcast('onReloadMenu', args);


                }, function errorCallback(response) {
                    // Error
                    console.log('error changeLanguage.', response);
                });
            };

            $scope.languageChange = function (oLang) {
                if (oLang != $scope.profile.Language) {
                    var URL = CONFIG.SERVER + 'workflow/SaveUserSetting';
                    var oRequestParameter = { InputParameter: { "Language": oLang, "ReceiveMail": $scope.profile.ReceiveMail }, CurrentEmployee: $scope.profile };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                        //data: param

                    }).then(function successCallback(response) {
                        // Success
                        $scope.reloadPageLgChange(oLang);

                    }, function errorCallback(response) {
                        //$timeout(function () {
                        //    $scope.status.settingL = false;
                        //}, 1000);
                        console.log('error SettingController.', response);
                    });
                }

            }

            $scope.getManualFile = function () {
                $scope.manualPath = '';
                var path = CONFIG.SERVER + 'Client/manual/' + $scope.profile.CompanyCode + '/'
                if ($scope.profile.Language == 'TH') {
                    $scope.manualPath = path + "DESS_UserManual_TH.pdf";
                } else {
                    $scope.manualPath = path + "DESS_UserManual_EN.pdf";
                }
            }
            $scope.getManualFile();
        }]);
})();

