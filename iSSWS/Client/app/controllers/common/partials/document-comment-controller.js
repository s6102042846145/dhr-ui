﻿(function () {
    angular.module('DESS')
        .controller('DocumentCommentController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

            $scope.showDocCommentBox = true;

            $scope.triggerShow = function () {
                $scope.showDocCommentBox = !$scope.showDocCommentBox;
            };


        }]);
})();