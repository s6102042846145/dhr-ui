﻿(function () {
angular.module('DESS')
    .controller('BoxController', ['$rootScope', '$route', '$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($rootScope, $route,$scope, $http, $routeParams, $location, CONFIG) {
        $scope.requestType = $routeParams.id;
        $scope.formData = { searchKeyword: '' };
        $scope.showWarning = false;
        $scope.warningActionText = "";
        
        var search = function (item, keyword) {
            // Search Criteria
            if (!keyword
                || (item.RequestNo != null && item.RequestNo.toLowerCase().indexOf(keyword) != -1)
                || (item.RequestorNo != null && item.RequestorNo.toLowerCase().indexOf(keyword) != -1)
                || (item.RequestorName != null && item.RequestorName.toLowerCase().indexOf(keyword) != -1)
                || (item.OrgName != null && item.OrgName.toLowerCase().indexOf(keyword) != -1)
                || (item.Summary != null && item.Summary.toLowerCase().indexOf(keyword) != -1)
                ) {
                return true;
            }
            return false;
        };
        $scope.searchInBox = function () {
            console.log('searchInBox.', $scope.formData.searchKeyword);
            if (!$scope.formData.searchKeyword) {
                console.log('just reset documents.');
                $scope.documents = $scope.masterDocuments;
            }
            else {
                var count = $scope.masterDocuments.length;
                var docs = [];
                for (var i = 0; i < count; i++) {
                    if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                        docs.push($scope.masterDocuments[i]);
                    }
                }
                $scope.documents = docs;
            }
        };
        $scope.CheckRequestType = function () {
           
            return $scope.CanMassApprove;
        };
        $scope.getWarningText = function (text) {
            if (!angular.isUndefined(text)) {
                return text.replace("{0}", $scope.warningActionText);
            }
            return '';
        };
        $scope.ActionFromDocument = function () {
            //requestno,acctioncode,current user
            //requestno,keymaster|requestno,keymaster, ActionCode , currentuser

            $scope.loader.enable = true;
            var actioncode = $scope.Action.ActionCode;
            var objRequestDocIsSel = angular.element(document.querySelectorAll("[requestdoc-isselect]:checked"));
            var oRequestNoSelected = "";
            for (var i = 0; i < objRequestDocIsSel.length; i++) {
                if (oRequestNoSelected == "")
                {
                    oRequestNoSelected = oRequestNoSelected.concat(objRequestDocIsSel[i].value);
                }
                else
                {
                    oRequestNoSelected = oRequestNoSelected.concat('|', objRequestDocIsSel[i].value);

                }
            }
            //alert(oRequestNoSelected);
            if (oRequestNoSelected != '' && actioncode != '')
            {

                var URL = CONFIG.SERVER + 'workflow/MassApprove/' + oRequestNoSelected + '/' + actioncode;
               // alert(URL);
                $http({
                    method: 'POST',
                    url: URL,
                    data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;

                    // Reload Menu
                    var args = {};
                    $rootScope.$broadcast('onReloadMenu', args);
                   
                    $route.reload();
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                    console.log('error boxcontroller.', response);
                });
            }
            else
            {
                $scope.loader.enable = false;
                $scope.warningActionText = $scope.Action.ActionText;
                $scope.showWarning = true;
            }
        };

        $scope.CanMassApprove = false;
        $scope.CheckMassApprove = function () {
            var URL = CONFIG.SERVER + 'workflow/GetAuthorizeMassApprove/' + $scope.requestType;
            $http({
                method: 'POST',
                url: URL,
                data: getToken(CONFIG.USER)
            }).then(function successCallback(response) {
                $scope.CanMassApprove = response.data;
            }, function errorCallback(response) {
                console.log('error boxcontroller.', response);
            });
        };

        var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentListInBox';
        var oRequestParameter = { InputParameter: { "BoxID": $scope.requestType }, CurrentEmployee: getToken(CONFIG.USER) };
        //var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentListInBox/' + $scope.requestType;

        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
            //data: getToken(CONFIG.USER)
        }).then(function successCallback(response) {
            $scope.masterDocuments = response.data.Table;
            $scope.documents = response.data.Table;
            $scope.Actions = response.data.Table1;
            if ($scope.Actions.length > 0) {
                $scope.Action = $scope.Actions[0];
            }

            //console.log('documents.', $scope.documents);
        }, function errorCallback(response) {
            console.log('error boxcontroller.', response);
            $scope.loader.enable = false;
        });


        $scope.openRequest = function (id, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
            $location.path('/frmViewRequest/' + id + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner);// + '/' + encodeURIComponent(BoxDescription));
        };

        $scope.getBoxText = function () {
            var text = '';
            var currentPath = $location.path();
            var menuItems = getToken('com.pttdigital.ess.menu')
            for (var i = 0; i < menuItems.length; i++) {
                if (('/' + menuItems[i].url) == currentPath) {
                    text = menuItems[i].text;
                    break;
                }
            }
            return text;
        };
        
        $scope.CheckMassApprove();
    }]);
})();