﻿(function () {
angular.module('DESS')
    .controller('RequestSupportController', ['$rootScope', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$window', function ($rootScope, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $window) {
        $scope.content = {};
        $scope.content.isShowHeader = true;
        $scope.content.Header = '';
        $scope.content.isShowError = true;
        $scope.content.isShowDocumentWarning = false;
        $scope.content.documentWarningText = '';
        $scope.isEditor = false;
        $scope.requestNo = $routeParams.id;
        $scope.requestCompanyCode = $routeParams.requestCompanyCode;
        $scope.keyMaster = $routeParams.keyMaster;
        $scope.isOwner = $routeParams.isOwner;
        $scope.isDataOwner = $routeParams.isDataOwner;

        $scope.itemID = $routeParams.itemID;

        $scope.runtime = Date.now();
        $scope.loader.enable = false;
        $scope.employeeData = getToken(CONFIG.USER);
        $scope.isActionInsteadOfMode = false;
        $scope.limitToday = $filter('date')(new Date, 'yyyy-MM-ddT00:00:00+07:00');

        
        

        $scope.VIEW_DETAIL_MODE = false;
        $scope.viewDetailMode = function(set) {
            $scope.VIEW_DETAIL_MODE = set;
        }


        //$scope.actionInsteadOf = {};
        if (!(angular.isUndefined($routeParams.requesterEmployeeId) || angular.isUndefined($routeParams.requesterPositionId))) {
            // using action instead of
            //alert('create with actionInsteadOf');
            $scope.employeeData.RequesterEmployeeID = $routeParams.requesterEmployeeId;
            $scope.employeeData.RequesterPositionID = $routeParams.requesterPositionId;
            $scope.requesterName = $routeParams.requesterName;
            $scope.isActionInsteadOfMode = true;
        }

        $scope.Textcategory = 'REQUESTHEADER';
        if (!angular.isUndefined($routeParams.textHeader) && $routeParams.textHeader != '') {
            $scope.content.Header = decodeURIComponent($routeParams.textHeader);
        } else {
            $scope.content.Header = $scope.Text['SYSTEM']['BACK'];
        }

        $scope.templateControl = {
            isDeleteRequest: false,
            isNotSubEditor: true
        };

        /* Set wizard style form editor */
        $scope.beginFormWizard = function () {
            $scope.templateControl.isNotSubEditor = false;
        };

        $scope.finishFormWizard = function () {
            $scope.templateControl.isNotSubEditor = true;
        }

        $scope.loader.enable = true; // เปิด

        var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentSupportByRequestNo';
        var oRequestParameter = { InputParameter: { "RequestNo": $scope.requestNo, "RequestCompanyCode": $scope.requestCompanyCode, "KeyMaster": $scope.keyMaster, "IsOwnerView": $scope.isOwner, "IsDataOwnerView": $scope.isDataOwner, "ItemID": $scope.itemID }, CurrentEmployee: getToken(CONFIG.USER) };

        //var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentByRequestNo/' + $scope.requestNo + '/' + $scope.keyMaster + '/' + $scope.isOwner + '/' + $scope.isDataOwner;
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
            //data: getToken(CONFIG.USER)
        }).then(function successCallback(response) {
            // Success
            $scope.document = response.data;
            console.log('document.', $scope.document);
            $scope.loader.enable = false; // เปิด

        }, function errorCallback(response) {
            // Error
            console.log('error RequestController.', response);
            $scope.document = null;
            $scope.loader.enable = false; // เปิด
        });

        $scope.getRequestViewerTemplate = function (r) {
            var viewer = $scope.document.Viewer;
            //return 'views/' + viewer.replace(/\./g, '/') + '.html?t=' + r;
            //return 'views/hr/tm/absenceviewer.html?t=' + r;
            return 'views/' + viewer + '.html?t=' + r;
        }

        //Functin Set Data / Load Data
        $scope.ChildAction = {
            LoadData: null,
            SetData: null
        };
        var TEMP_TO_RESEND_ACTION_URL;
        var reqActInpr = false;
        $scope.doRequestAction = function (actionUrl) {

            if (reqActInpr) {
                return;
            }
            if ($scope.blockAction.isBLock) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('WARNING')
                    .textContent($scope.blockAction.blockMessage)
                    .ok('OK')
                );
                return;
            }
            reqActInpr = true;
            TEMP_TO_RESEND_ACTION_URL = actionUrl;
            console.log('doRequestAction.', CONFIG.SERVER + actionUrl);
            var authUser = getToken(CONFIG.USER);

            if ($scope.ChildAction.LoadData == null)
                alert('Please implement function $scope.ChildAction.LoadData before');
            else {
                $scope.loader.enable = true;

                $scope.ChildAction.LoadData();
                var actionDoc = angular.copy($scope.document);
                if (actionDoc.ActionBy != null) //AddBy: Ratchatawan W. (2016-10-05)
                {
                    if (actionDoc.ActionBy.EmployeeID != authUser.EmployeeID)
                    { actionDoc.ActionBy = authUser; }
                }
                else {
                    actionDoc.ActionBy = authUser;
                    { actionDoc.ActionBy = authUser;  }
                }
                if ($scope.document.Additional != null) {
                    var obj = actionDoc.Additional;
                    console.log('LoadData.', obj[Object.keys(obj)[0]][0]);
                    actionDoc.AdditionalJson = JSON.stringify(obj[Object.keys(obj)[0]][0]);
                    actionDoc.Additional = [];
                }
                console.log('obj doRequestAction.', angular.copy(actionDoc));
                var oRequestParameter = { InputParameter: {}, RequestDocument: actionDoc, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + actionUrl,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.loader.enable = false;
                    console.log('success doRequestAction.', response.data);

                    $scope.document = response.data;
                    $scope.$parent.PassingDocument.document = angular.copy($scope.document);
                    console.log('passing document.', angular.copy($scope.$parent.PassingDocument));
                    // Redirect to response.data.RedirectURL (if empty then show error)

                    if (response.data != null && $scope.document.HasError)
                    {
                        $scope.ChildAction.SetData();
                    }

                    if (response.data != null && response.data.RedirectURL != '') {
                        // Reload Menu
                        var args = {};
                        $rootScope.$broadcast('onReloadMenu', args);
                        reqActInpr = false;
                        $location.path(response.data.RedirectURL);
                    } else {
                        if (response.data == null) {
                            $scope.document.HasError = true;
                            $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                        } else {
                            $scope.document.HasError = response.data.HasError;
                            $scope.document.ErrorText = response.data.ErrorText;
                            
                            if (response.data.RequireComment) {
                                chekreason();
                            }
                        }
                    }
                    reqActInpr = false;
                }, function errorCallback(response) {
                    // Error
                    $scope.loader.enable = false;
                    $scope.document.HasError = true;
                    reqActInpr = false;
                    $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                    console.log('error doRequestAction.', response);

                });
            }
        };


        var btnTxt = '';
        $scope.setCurentButtonTxt = function (iBtnTxt) {
            btnTxt = iBtnTxt;
        }
        function chekreason() {
            var parentEl = angular.element(document.body);
            $mdDialog.show({
                parent: parentEl,
                template:
                  '<md-dialog aria-label="List dialog" style=" width: 100%; max-width: 700px;height: 100%;max-height: 300px; padding: 10px;">' +
                  '  <md-dialog-content class="-txt-blue" style="    height: 65px; font-size:20px; padding: 15px;">' +
                  '     {{description.title}}' +
                  '  </md-dialog-content>' +
                  '  <md-dialog-actions style=" border: none; position:relative;">' +
                  '    <div class="alert alert-warning" role="alert" style="width: 100%;">' +
                  '      <div style="font-size:20px;" class="ng-binding">{{description.box_title}}</div>' +
                  '         <div>' +
                  '            <textarea rows="4" style="width:100%;resize: none; padding:10px; border: 1px solid;" ng-model="comment" class="ng-pristine ng-untouched ng-valid" aria-invalid="false"></textarea>' +
                  '         </div>' +
                  '     <div class="clearfix"></div>' +
                  '    </div>' +
                  '  </md-dialog-actions>' +
                  '    <md-button ng-click="closeAndSendDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 100px; font-size: 20px">' +
                  '      {{description.btnTxt}}' +
                  '    </md-button>' +
                  '    <md-button ng-click="closeDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 0;  font-size: 20px">' +
                  '      {{description.btnCancel}}' +
                  '    </md-button>' +
                  '</md-dialog>',
                locals: {
                    description: {
                        title: $scope.document.ErrorText, // get text error to display in the dialog
                        box_title: $scope.Text['SYSTEM']['reason_'],
                        btnTxt: btnTxt,
                        btnCancel: $scope.Text['SYSTEM']['CLOSE_DOWN']
                    }
                },
                controller: DialogController
            }).then(function (obj) {
                $scope.document.Comment = obj.comment;
                if (obj.isResend) {
                    $scope.doRequestAction(TEMP_TO_RESEND_ACTION_URL);
                }

            });
            function DialogController($scope, $mdDialog, description) {
                $scope.comment = '';
                $scope.description = description;
                
                $scope.closeDialog = function () {
                    var retunrParam = {
                        comment: $scope.comment,
                        isResend:false
                    };
                    $mdDialog.hide(retunrParam);
                }
                $scope.closeAndSendDialog = function () {
                    var retunrParam = {
                        comment: $scope.comment,
                        isResend: true
                    };
                    $mdDialog.hide(retunrParam);
                }
            }
        }


        $scope.getFileAttach = function (setId, id) {
            var URL = CONFIG.SERVER + 'workflow/GetFile/' + setId + '/' + id + '/' + $scope.employeeData.CompanyCode + '/' + $scope.employeeData.EmployeeID;
            if (typeof cordova != 'undefined') {
                cordova.InAppBrowser.open(URL, '_system', 'location=no');
            } else {
                window.open(URL, '_blank');
            }
        };
        $scope.getFileFromPath = function (attachment) {
            /* direct */
            if (angular.isDefined(attachment)) {
                if (typeof cordova != 'undefined') {

                } else if (attachment.FilePath) {
                    var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                    console.debug(path);
                    $window.open(path);
                } else {
                    var path = CONFIG.SERVER + 'Client/files/' + attachment;
                    console.debug(path);
                    $window.open(path);

                }
            }
            /* !direct */
        }

        //$scope.getFileAttach = function (setId, id) {
        //    //var URL = CONFIG.SERVER + 'workflow/GetFile';
        //    var oRequestParameter = { InputParameter: { "FileSetID": setId, "FileID": id }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

        //    //var URL = CONFIG.SERVER + 'workflow/GetFile/' + setId + '/' + id;
        //    //if (typeof cordova != 'undefined') {
        //    //    cordova.InAppBrowser.open(URL, '_system', 'location=no');
        //    //} else {
        //    //    window.open(URL, '_blank');
        //    //}
        //    var MoDule = 'workflow/';
        //    var Functional = 'GetFile';
        //    var URL = CONFIG.SERVER + MoDule + Functional;
        //    // Success
        //    $http({
        //        method: 'POST',
        //        url: URL,
        //        data: oRequestParameter
        //    }).then(function successCallback(response) {
        //        //console.log(response);
        //        if (typeof cordova != 'undefined') {
        //            cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
        //        } else {
        //            window.open("data:application/octet-stream, " + escape(response.data));
        //        }
        //    }, function errorCallback(response) {
        //        // Error
        //        console.log('error getFileAttach.', response);
        //    });
        //};

        $scope.Textcategory = 'SYSTEM';

        $scope.LoadResponseLogPost = function ()
        {
            //$scope.requestNo
            var URL = CONFIG.SERVER + 'workflow/GetResponseLogPost';
            var oRequestParameter = { InputParameter: { "RequestNo": $scope.requestNo, "RequestCompanyCode": $scope.requestCompanyCode }, CurrentEmployee: getToken(CONFIG.USER) };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.PostLog = response.data;
                console.log('document.', $scope.document);

            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
                $scope.document = null;
            });

        }

    }])
 
})();