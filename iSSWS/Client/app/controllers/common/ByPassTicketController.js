﻿(function () {
    angular.module('DESS')
        .controller('ByPassTicketController', ['$rootScope', '$scope', '$http', '$routeParams', '$location', 'CONFIG', '$mdDialog', '$timeout', function ($rootScope, $scope, $http, $routeParams, $location, CONFIG, $mdDialog, $timeout) {

            $scope.CompanyCode = $routeParams.CompanyCode;
            $scope.TicketID = $routeParams.TicketID;
            $scope.ExternaluserID = $routeParams.ExternaluserID;
            $scope.ErrorMessage = "";

            if (getToken(CONFIG.USER) == null && $scope.ExternaluserID == null) {
                $scope.ErrorMessage = "NO LOGIN USER. We are going to loging page within secound...";
                showAlert($scope.ErrorMessage);
                $timeout(function () {
                    window.location = CONFIG.SERVER + 'client/login.html';
                }, 2000);
            }
            else {
                var URL = CONFIG.SERVER + 'workflow/ByPassTicket';
                var oRequestParameter = { InputParameter: { "CompanyCode": $scope.CompanyCode, "TicketID": $scope.TicketID, "ExternaluserID": $scope.ExternaluserID }, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if ($scope.ExternaluserID != "" && $scope.ExternaluserID) {
                        window.localStorage.setItem('com.pttdigital.ess.authorizeUser', JSON.stringify(response.data.EmployeeData));
                        window.localStorage.setItem('com.pttdigital.ess.menu', JSON.stringify(response.data.Menu));
                    }

                    if (angular.isDefined(response.data.Result[0].Error)) {
                        $scope.ErrorMessage = response.data.Result[0].Error;
                        showAlert($scope.ErrorMessage);
                    }
                    else {
                        $location.path('/frmViewRequest/' + response.data.Result[0].RequestNo + '/' + response.data.Result[0].CompanyCode + '/' + response.data.Result[0].KeyMaster + '/false/false/');
                        $timeout(function () {
                            location.reload();
                        }, 500);
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error ByPassTicketController.', response);
                    showAlert(response);
                    $scope.document = null;
                });
            }


            function showAlert(message) {
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('INFORMATION')
                    .textContent(message)
                    .ok('OK')
                );
            };

        }]);
})();