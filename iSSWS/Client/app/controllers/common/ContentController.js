﻿(function () {
    angular.module('DESS')
        .controller('FormViewContentController', ['$scope', '$http', '$routeParams', '$rootScope', '$location', 'CONFIG', '$window', '$mdDialog', function ($scope, $http, $routeParams, $rootScope, $location, CONFIG, $window, $mdDialog) {
            $scope.contentId = $routeParams.id;
            $scope.contentParam = $routeParams.itemKey;  //Add: 2016-Oct-20
            $scope.param = $rootScope.contentParam;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.requesterData = getToken(CONFIG.USER); //AddBy: Ratchatawan W. (9 jan 2016) - Work across company

            if (!angular.isUndefined($routeParams.requesterName)) {
                $scope.requesterData.Name = $routeParams.requesterName;
                $scope.requesterData.PositionID = $routeParams.requesterPositionId;
                $scope.requesterData.Position = $routeParams.requesterPosition;
            }

            $scope.content = {};
            $scope.loader.enable = true;
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.content.Header = '';
            $scope.content.Param = '';
            $scope.isActionInsteadOfMode = false;
            $scope.positionObj;
            $scope.positionShortText = '';
            $scope.urlImage = '';

            //$scope.actionInsteadOf = {};
            if (!(angular.isUndefined($routeParams.requesterEmployeeId) || angular.isUndefined($routeParams.requesterPositionId))) {
                // using action instead of
                $scope.employeeData.RequesterEmployeeID = $routeParams.requesterEmployeeId;
                $scope.employeeData.RequesterPositionID = $routeParams.requesterPositionId;
                $scope.requesterName = decodeURIComponent($routeParams.requesterName);
                $scope.requesterEmployeeID = $routeParams.requesterEmployeeId;
                $scope.isActionInsteadOfMode = true;
                //Nattawat S. Add 2021-01-27
                if ($scope.isActionInsteadOfMode)
                    searchEmployee();

                //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                $scope.requesterData.EmployeeID = $routeParams.requesterEmployeeId;
                $scope.requesterData.PositionID = $routeParams.requesterPositionId;
                $scope.requesterData.CompanyCode = $routeParams.requesterCompanyCode;


                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetPosition';
                var oRequestParameter = {
                    InputParameter: { "ObjectID": $scope.requesterData.PositionID }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.positionObj = response.data;
                    if ($scope.employeeData.Language == 'TH') {
                        $scope.positionShortText = $scope.positionObj.ShortText;
                    }
                    else {
                        $scope.positionShortText = $scope.positionObj.ShortTextEn;
                    }
                }, function errorCallback(response) {
                });


                var URL = CONFIG.SERVER + 'Employee/GetImageUrl';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.requesterData.EmployeeID }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.urlImage = response.data;
                }, function errorCallback(response) {
                });

            }
            $scope.runtime = Date.now();

            if ($scope.contentId == 0) {
                // Announcement
                $scope.contentInfo = {
                    ContentTemplate: 'common/main',
                    HaveActionInsteadOf: false
                };
            } else if ($scope.contentId == 1) {
                // Profile
                $scope.contentInfo = {
                    ContentTemplate: 'common/profile',
                    HaveActionInsteadOf: false
                };
            } else if ($scope.contentId == 2) {
                // Profile
                $scope.contentInfo = {
                    ContentTemplate: 'common/SettingPanel',
                    HaveActionInsteadOf: false
                };
            } else {
                var oRequestParameter = { InputParameter: { "CONTENTID": $scope.contentId, "CONTENTPARAM": $scope.contentParam }, CurrentEmployee: getToken(CONFIG.USER) }

                var URL = CONFIG.SERVER + 'workflow/GetContentSetting';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.contentInfo = response.data;
                    if ($scope.contentInfo.IsContent == true) {
                        $scope.content.Header = $scope.contentInfo.Header
                        $scope.content.ContentParam = $scope.contentParam //Add: 2016-Oct-20                       
                    }
                    else {
                        $location.path('/frmViewContent/0');
                        window.open($scope.contentInfo.ContentTemplate, "_parent ");
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error ContentController.', response);
                    $scope.contentInfo = {
                        ContentTemplate: 'common/main',
                        HaveActionInsteadOf: false
                    };
                    window.location = 'login.html';
                });
            }
            $scope.ContentParam = "";
            $scope.getTemplate = function () {
                $scope.ContentParam = $scope.contentInfo.ContentParam;
                return 'views/' + $scope.contentInfo.ContentTemplate + '.html?t=' + $scope.runtime;
            }
            $scope.showListActionInsteadOf = function () {

                $location.path('/actionInsteadOf/' + $scope.contentId);
            };

            /* Create Document from Content */
            $scope.CreateNew = function (requestTypeId, paramValue,) {
                //add by pariyaporn remove backdrop modal 2021-06-29
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                if (angular.isUndefined($scope.content.Header) || $scope.content.Header == '') {
                    $scope.content.Header = '-';
                }
                //$routeParams = { Key1: paramValue };
                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf
                    $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + +$routeParams.requesterCompanyCode + '/' + paramValue);
                } else {
                    $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + paramValue);
                }
            };

            $scope.CreateNewWithReference = function (requestTypeId, referRequestNo, itemKey, paramValue) {
                //add by pariyaporn remove backdrop modal 2021-06-29
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                if (angular.isUndefined($scope.content.Header) || $scope.content.Header == '') {
                    $scope.content.Header = '-';
                }
                if ($scope.isActionInsteadOfMode) {
                    if (angular.isUndefined(itemKey) || itemKey == null || itemKey == '') {
                        $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + referRequestNo + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode);
                    } else {
                        $location.path('/frmCreateRequest/' + requestTypeId + '/' + itemKey + '/' + referRequestNo + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode);
                    }
                }
                else {
                    if (angular.isUndefined(itemKey) || itemKey == null || itemKey == '') {
                        if (angular.isUndefined(paramValue) || paramValue == null || paramValue == '') {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + referRequestNo);
                        }
                        else {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/0/null/' + paramValue);
                        }
                    } else {
                        if (angular.isUndefined(paramValue) || paramValue == null || paramValue == '') {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/' + itemKey + '/' + referRequestNo);
                        }
                        else {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/' + itemKey + '/' + referRequestNo + '/' + paramValue);
                        }
                    }
                }
            };

            /* Create Document from Content */
            $scope.CreateNewWithCheckWorkSchedule = function (requestTypeId, paramValue) {
                //add by pariyaporn remove backdrop modal 2021-06-29
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                var currentMonth = new Date().getMonth() + 1 < 10 ? '0' + (new Date().getMonth() + 1) : new Date().getMonth() + 1;
                var currentYear = new Date().getFullYear();

                var URL = CONFIG.SERVER + 'HRTM/GetMonthlyWorkscheduleByPeriodYearMonth';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    //InputParameter: { 'Month': Month, 'Year': Year }
                    InputParameter: { 'beginMonth': currentMonth, 'endMonth': currentMonth, 'beginYear': currentYear, 'endYear': currentYear }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter

                }).then(function successCallback(response) {
                    if (response.data.DayOfMonth.length > 0) {
                        $scope.CreateNew(requestTypeId, paramValue);
                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['SYSTEM'].WARNING)
                                .textContent($scope.Text['TM_LEAVE'].NOT_FOUND_WS)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                        );

                    }

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            /* View Document from Content */
            $scope.ViewObj = function (RequestNo, CompanyCode) {
                console.log('RequestNo.', RequestNo);

                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf
                    $location.path('/frmViewRequest/' + RequestNo + '/' + CompanyCode + '/null/true/false' + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode);
                } else {
                    $location.path('/frmViewRequest/' + RequestNo + '/' + CompanyCode + '/null/true/false');
                }
                console.log('REDIRECT', encodeURIComponent($scope.content.Header));
            };
            $scope.ViewObjAnnounce = function (RequestNo, CompanyCode) {
                $location.path('/frmViewRequest/' + RequestNo + '/' + CompanyCode + '/null/false/false');
            };

            /* Open Request Document from Content */
            $scope.openRequest = function (requestno, companyCode, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
                //add by pariyaporn remove backdrop modal 2021-06-29
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $location.path('/frmViewRequest/' + requestno + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + encodeURIComponent(BoxDescription));
            };

            /* Create Cancel Document from Content */
            $scope.DeleteObj = function (requestTypeId, Itemkey) {

                console.log('key.', Itemkey);
                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf
                    $location.path('/frmCreateRequest/' + requestTypeId + '/' + Itemkey + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode);
                } else {
                    $location.path('/frmCreateRequest/' + requestTypeId + '/' + Itemkey);
                }
                console.log('REDIRECT', encodeURIComponent($scope.content.Header));
            };

            /* Create Cancel Document from Content */
            $scope.DeleteObjWithParams = function (requestTypeId, Itemkey, otherParam) {

                console.log('key.', Itemkey);
                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf
                    $location.path('/frmCreateRequest/' + requestTypeId + '/' + Itemkey + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode + '/' + otherParam);
                } else {
                    $location.path('/frmCreateRequest/' + requestTypeId + '/' + Itemkey + '/' + otherParam);
                }
                console.log('REDIRECT', encodeURIComponent($scope.content.Header));
            };

            $scope.getFileAttach = function (setId, id) {
                var URL = CONFIG.SERVER + 'workflow/GetFile/' + setId + '/' + id + '/' + $scope.employeeData.CompanyCode + '/' + $scope.employeeData.EmployeeID;
                if (typeof cordova != 'undefined') {
                    cordova.InAppBrowser.open(URL, '_system', 'location=no');
                } else {
                    window.open(URL, '_blank');
                }
            };

            $scope.getFileFromPath = function (attachment) {
                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {

                    } else if (attachment.FilePath) {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        console.debug(path);
                        $window.open(path);
                    } else {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment;
                        console.debug(path);
                        $window.open(path);

                    }
                }
                /* !direct */
            }

            //decode html encode
            $scope.createHtml = function (htmlCode) {
                var txt = document.createElement("textarea");
                txt.innerHTML = htmlCode;
                return txt.value;
            };

            $scope.openViewContent = function (requestType) {
                // $scope.loader.enable = false;
                $location.path('/frmViewContent/' + requestType);
            }

            //decode html encode
            $scope.createHtml = function (htmlCode) {
                var txt = document.createElement("textarea");
                txt.innerHTML = htmlCode;
                return txt.value;
            };

            //Nattawat S. 
            function searchEmployee() {
                $scope.loader.enable = true;
                var oRequestParameter = { InputParameter: { "CONTENTID": $scope.contentId, "SEARCHTEXT": '' }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'workflow/ActionInsteadOf',
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.actionInsteadOfList = response.data;
                    if ($scope.isActionInsteadOfMode) {
                        let filteredRequestor = $scope.actionInsteadOfList.filter(data => data.EmployeeID == $scope.requesterEmployeeID);
                        if (filteredRequestor.length == 0)
                            $location.path('/frmViewContent/' + $scope.contentId);
                    }
                }, function errorCallback(response) {
                    // Error
                    $scope.actionInsteadOfList = [];
                });
            }

        }])
        .controller('ProfileController', ['$scope', '$http', '$routeParams', '$location', '$sce', 'CONFIG', function ($scope, $http, $routeParams, $location, $sce, CONFIG) {
            $scope.contentId = $routeParams.id;
            $scope.Textcategory = 'HRPAPERSONALDATA';
            $scope.content.Header = "ข้อมูลส่วนตัว";
            $scope.profile = (getToken(CONFIG.USER));
            $scope.UserImg = $scope.profile.ImageUrl + "?" + new Date().getTime();;
            $scope.PositionID = $scope.profile.PositionID;
            $scope.companyNameTH = $scope.profile.CompanyDetail.FullNameTH;
            $scope.companyNameEN = $scope.profile.CompanyDetail.FullNameEN;
            $scope.OragnaizationData = [];
            $scope.OrganizationRelationData = [];


            $scope.setPosition = function (NewPositionID, NewPositionName) {
                //601009
                //$scope.PROFILE_SETTING.POSITIONID = NewPositionID;
                //window.localStorage.setItem(CONFIG.USER, JSON.stringify($scope.profile))
                $scope.PROFILE_SETTING.POSITIONID = NewPositionID;
                $scope.profile.PositionID = NewPositionID;
                $scope.profile.Position = NewPositionName;
                //Nattawat S. case: swap position 
                $scope.profile.RequesterPositionID = NewPositionID;

                window.localStorage.setItem(CONFIG.USER, JSON.stringify($scope.profile))
                window.location.reload();

            };
            $scope.loader.enable = false;

            var decoded = angular.element('<div />').html($scope.companyNameTH).text();
            $scope.decodedData = $sce.trustAsHtml(decoded);

            //decode html encode
            $scope.createHtml = function (htmlCode) {
                var txt = document.createElement("textarea");
                txt.innerHTML = htmlCode;
                return txt.value;
            };

            $scope.GetRelation = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1001GetAllHistory';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var currentDate = new Date();
                    $scope.OrganizationRelationData = response.data.filter(data => data.ObjectType == 'S' && data.NextObjectType == 'O' && data.Relation == 'Belong To' && (currentDate >= new Date(data.BeginDate) && currentDate <= new Date(data.EndDate)));
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            };

            $scope.GetOrganizationDetail = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetAllHistory';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var currentDate = new Date();
                    $scope.OragnaizationData = response.data.filter(data => data.ObjectType == 'O' && (currentDate >= new Date(data.BeginDate) && currentDate <= new Date(data.EndDate)));
                    $scope.GetRelation();
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            };
            $scope.GetOrganizationDetail();

            $scope.getOrgByPositionBelongTo = function (posID) {
                if ($scope.OrganizationRelationData.length > 0 && $scope.OragnaizationData.length > 0) {
                    var belongto = $scope.OrganizationRelationData.filter(data => data.ObjectID.split(':')[0] == posID);
                    var orgdata = $scope.OragnaizationData.filter(data => data.ObjectID == belongto[0].NextObjectID.split(':')[0]);
                    return $scope.profile.Language == 'EN' ? angular.copy(orgdata[0].TextEn) : angular.copy(orgdata[0].Text);
                }
            };

        }])
        .controller('AnnouncementController', ['$scope', '$http', '$routeParams', '$location', '$sce', '$mdDialog', '$filter', 'CONFIG', '$interval', function ($scope, $http, $routeParams, $location, $sce, $mdDialog, $filter, CONFIG, $interval) {
            $scope.contentId = $routeParams.id;
            $scope.content.isShowBackIcon = false;
            $scope.firstAnnouncementID = "";
            $scope.CurrentLanguage = '';
            $scope.data = {
                ShowAnnouncement: [],
            }
            $scope.announcementIndexOnMainpage = 0;

            /***** System Text Description *****/
            $scope.setSystemText = function () {
                // Set system text here ...
                $scope.content.Header = $scope.Text['SYSTEM'].ANNOUNCEMENT;
            };
            $scope.$on("systemTextReady", function (event, args) {
                $scope.setSystemText();
            });
            if (!angular.isUndefined($scope.Text['SYSTEM'])) {
                $scope.setSystemText();
            }
            /***** !System Text Description *****/

            //function for get style display on ng-bind-html
            $scope.renderHtml = function (html_code, maxLength) {
                if (maxLength == null || maxLength == '') {
                    return $sce.trustAsHtml(html_code);
                } else {
                    return $sce.trustAsHtml(html_code.substr(0, maxLength));
                }

            };

            $scope.init = function () {
                $scope.CurrentLanguage = $scope.employeeData.Language;
                getAnnouncement();
            }


            var handle_nav = function (e) {
                e.preventDefault();
                var nav = $(this);
                nav.parents('.carousel').carousel(nav.data('slide'));
            }

            function seenFirstAnnouncement(announcementID, statusType) {
                var URL = CONFIG.SERVER + 'Announcement/acceptAnnouncement';
                var oRequestParameter = {
                    InputParameter: { "announcementID": announcementID, "statusType": statusType },
                    CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    data: oRequestParameter,
                    url: URL
                }).then(function successCallback(response) {
                    // Success
                    $scope.data = response.data;
                    //$route.reload();
                }, function errorCallback(response) {
                    // Error
                    console.log('error AnnouncementController.', response);
                });
            }

            function getAnnouncement() {
                var URL = CONFIG.SERVER + 'Announcement/getAnnouncement';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.announcements = announcementOnPeriod(response.data);
                    if ($scope.announcements.length > 0) {
                        $scope.firstAnnouncementID = response.data[0].ID;
                        GetRootPath();
                        $scope.setDataAnnouncementOnUI($scope.announcements[0].ID);
                        seenFirstAnnouncement($scope.firstAnnouncementID, "SEEN");
                        if ($scope.announcements[0].Acceptable) {
                            checkAcceptAnnounceByEmp($scope.announcements[0].AcceptedList);
                        }
                    }
                    chColor();
                    $scope.loader.enable = false;
                },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.error(response);
                        return response;
                    });
            };
            function checkAcceptAnnounceByEmp(acceptList) {
                $scope.isAcceptedAnnounceByEmployeeID = false;
                var emplist = new Array();
                emplist = acceptList.split(",");
                $scope.employeeData.EmployeeID;
                for (var i = 0; i < emplist.length; i++) {
                    if (emplist[i] == $scope.employeeData.EmployeeID) {
                        $scope.isAcceptedAnnounceByEmployeeID = true;
                        break;
                    }
                }
            }

            function GetRootPath() {
                var URL = CONFIG.SERVER + 'Workflow/GetRootPath';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.RootPath = response.data;
                },
                    function errorCallback(response) {
                        console.error(response);
                        return response;
                    });
            };

            $scope.acceptAnnouncementTakeActionFromUIx = function (announcementID, statusType, index) {
                $('#AcceptedAnnounceModal').show();
                setTimeout(function () {
                    $('#AcceptedAnnounceModal').hide();
                }, 5000);
            };

            $scope.acceptAnnouncementTakeActionFromUI = function (announcementID, statusType, index) {
                $scope.DataShowAnn.Acceptable = false;
                var URL = CONFIG.SERVER + 'Announcement/acceptAnnouncement';
                var oRequestParameter = {
                    InputParameter: { "announcementID": announcementID, "statusType": statusType },
                    CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    data: oRequestParameter,
                    url: URL
                }).then(function successCallback(response) {
                    //getAnnouncement();
                    //$scope.announcements[0].Acceptable
                    $scope.AnnounceResult = $filter('filter')($scope.announcements, { ID: announcementID });
                    if ($scope.AnnounceResult.length > 0) {
                        $scope.AnnounceResult[0].Acceptable = false;
                    }
                    $scope.isAcceptedAnnounceByEmployeeID = true;
                    $('#AcceptedAnnounceModal').show();
                    setTimeout(function () {
                        $('#AcceptedAnnounceModal').hide();
                    }, 5000);
                }, function errorCallback(response) {
                    console.log('error AnnouncementController.', response);
                });
            };

            function acceptAnnouncement(announcementID, statusType, index) {
                if (statusType == "ACCEPTED") {
                    $scope.DataShowAnn.Acceptable = false;
                }

                var URL = CONFIG.SERVER + 'Announcement/acceptAnnouncement';
                var oRequestParameter = {
                    InputParameter: { "announcementID": announcementID, "statusType": statusType },
                    CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    data: oRequestParameter,
                    url: URL
                }).then(function successCallback(response) {
                    // Success
                    if (statusType == "ACCEPTED")
                        employeeSeeAlert();
                    //$route.reload();
                }, function errorCallback(response) {
                    // Error
                    console.log('error AnnouncementController.', response);
                });
            };

            function checkAcceptAnnounceh(announceID, acceptedList) {

            }

            //function zeroPad(vParam) {
            //    if (vParam < 10)
            //        return vParam = '0' + vParam;
            //    else
            //        return vParam.toString();
            //}

            function announcementOnPeriod(announcement) {
                var out = [];
                $scope.TodayDate = getDateFormateData(new Date());

                for (var i = 0; i < announcement.length; i++) {
                    $scope.annBeginDate = getDateFormateData(new Date(announcement[i].BeginDate));
                    $scope.annEndDate = getDateFormateData(new Date(announcement[i].EndDate));

                    if ($scope.TodayDate >= $scope.annBeginDate
                        && $scope.TodayDate <= $scope.annEndDate) {
                        out.push(announcement[i]);
                    }
                }

                if (out.length > 0)
                    $scope.currentAnnID = out[0].ID;
                return out;
            };
            $scope.setDataAnnouncementOnUI = function (objID) {
                //  $scope.AnnounceResult = $filter('filter')($scope.announcements, { ID: objID });
                for (i = 0; i <= $scope.announcements.length - 1; i++) {
                    if ($scope.announcements[i].ID == objID) {
                        $scope.DataShowAnn = angular.copy($scope.announcements[i]);
                        $scope.announcementIndexOnMainpage = i;
                        acceptAnnouncement($scope.DataShowAnn.ID, "SEEN", i);
                        if ($scope.announcements[i].Acceptable) {
                            checkAcceptAnnounceByEmp($scope.DataShowAnn.AcceptedList);
                        } else {
                            $scope.isAcceptedAnnounceByEmployeeID = false;
                        }

                        break;
                    }
                }

                //set current active li
                $scope.currentAnnID = objID;

            }


            $scope.showDetailAnnouncements = function (objID) {
                for (i = 0; i <= $scope.announcements.length - 1; i++) {
                    if ($scope.announcements[i].ID == objID) {
                        $scope.Detail = angular.copy($scope.announcements[i]);
                        break;
                    }
                }
                $scope.detailContent = '';
                if ($scope.CurrentLanguage == 'TH') {
                    $scope.detailContent = $scope.Detail.Content;
                } else {
                    $scope.detailContent = $scope.Detail.ContentEN != '' ? $scope.Detail.ContentEN : $scope.Detail.Content;
                }
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title($scope.Text['ANNOUNCEMENT'].CONTENT)
                        .htmlContent($scope.detailContent)
                        .ok($scope.Text['SYSTEM'].BUTTON_OK)
                );
            }

            var slideIndex = 1;
            // showDivs(slideIndex);

            function plusDivs(n) {
                //  showDivs(slideIndex += n);
            }

            $scope.currentDiv = function (n) {
                //  showDivs(n);
            }

            function showDivs(n) {
                //var i;
                //var x = document.getElementsByClassName("mySlides");
                //var dots = document.getElementsByClassName("demo");
                //if (n > x.length) { slideIndex = 1 }
                //if (n < 1) { slideIndex = x.length }
                //for (i = 0; i < x.length; i++) {
                //    x[i].style.display = "none";
                //}
                //for (i = 0; i < dots.length; i++) {
                //    dots[i].className = dots[i].className.replace(" w3-white", "");
                //}
                //x[slideIndex - 1].style.display = "block";
                //dots[slideIndex - 1].className += " w3-white";
            }

            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }

            $scope.OpenVoteContent = function () {
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('TitleVote')
                        //.htmlContent($scope.Detail.Content)
                        .text('Vote Vote Vote')
                        .ok($scope.Text['SYSTEM'].BUTTON_OK)
                );
            }


            function chColor() {
                //$scope.countAnnounce = $scope.announcements.length;
                //var counter = setInterval(callAnnouncement, 5000);
                //console.log($scope.announceIndex);

                //var counter = 0;
                //var maxAnnounce = $scope.announcements.length;
                //var looper = setInterval(function () {
                //    counter++;
                //    console.log("Announcement is: " + counter);
                //    $scope.setDataAnnouncementOnUI(counter);
                //    if (counter >= maxAnnounce) {
                //        clearInterval(looper);
                //    }

                //}, 5000);

            }


            //function callAnnouncement() {
            //    if ($scope.announceIndex == $scope.countAnnounce) {
            //        $scope.announceIndex = 0;
            //    }
            //    $scope.setDataAnnouncementOnUI($scope.announceIndex);
            //    $scope.announceIndex++;
            //    console.log($scope.announceIndex);
            //}

        }])
        .controller('PersonalWorkScheduleContentController', ['$scope', '$http', '$routeParams', '$mdDialog', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $mdDialog, $location, $filter, CONFIG) {
            $scope.Textcategory = "TM_WORKSCHEDULE";
            $scope.MonthUI = {};
            $scope.DayUI = {};
            $scope.DataModalS = "12";
            $scope.CurrentDays = new Date().getDate();
            $scope.CurrentWorkScheduleText = $scope.MonthCurrent + "/" + $scope.YearCurrent;
            $scope.LeaveContent;
            $scope.LeaveContentUI;
            $scope.LeaveList;
            $scope.GetMonthlyWorkscheduleByYearMonth = function (Month) {
                $scope.loader.enable = true;
                $scope.MonthCurrent = Month;
                var URL = CONFIG.SERVER + 'HRTM/GetMonthlyWorkscheduleByYearMonthCalendar';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { 'Month': Month, 'Year': $scope.YearCurrent }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MasterData = response.data.DayOfMonth;
                    $scope.HolidayList = response.data.HolidayList;
                    $scope.LeaveOfMonth = response.data.LeaveOfMonth;
                    for (var i = 0; i < $scope.MasterData.length; i++) {
                        var beginTimeVal = $scope.MasterData[i].beginTimeLeaveLog;
                        var endTimeVal = $scope.MasterData[i].endTimeLeaveLog;
                        if (beginTimeVal !== null && beginTimeVal !== 'undefined' && beginTimeVal !== "") {
                            $scope.MasterData[i].beginTimeLeaveLog = $scope.getTimeFormateShow(beginTimeVal);
                            $scope.MasterData[i].endTimeLeaveLog = $scope.getTimeFormateShow(endTimeVal);
                        } else {
                            $scope.MasterData[i].beginTimeLeaveLog = $scope.MasterData[i].WorkBeginTime;
                            $scope.MasterData[i].endTimeLeaveLog = $scope.MasterData[i].WorkEndTime;
                        }
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.getTimeFormateShow = function (timeValue) {
                var timeSplit = timeValue.split(':');
                return timeSplit[0] + ":" + timeSplit[1];
            }
            $scope.GetClockInClockOutTimeElement = function () {
                $scope.CurrentDate = getDateFormateData(new Date());

                var URL = CONFIG.SERVER + 'HRTM/GetClockInClockTimeElementWeekly';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: { 'Month': $scope.MonthCurrent, 'Year': $scope.YearCurrent }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data = response.data.ClockInOutWeekly;
                    $scope.ClockInOutWeekly = [];
                    //for test
                    $scope.CurrentDate = $scope.YearCurrent + '-0' + $scope.MonthCurrent + '-' + new Date().getDate();
                    for (var i = 0; i < $scope.data.length; i++) {
                        if (getDateFormateData($scope.data[i].Date) == $scope.CurrentDate) {
                            $scope.isCurrentDate = true;

                        } else {
                            $scope.isCurrentDate = false;
                        }
                        $scope.ClockInOutWeekly.push({
                            "Date": $scope.data[i].Date,
                            "ClockIn": $scope.data[i].ClockIn,
                            "ClockOut": $scope.data[i].ClockOut,
                            "IsCurrentDate": $scope.isCurrentDate
                        });
                        if (i == 6) {
                            break;
                        }
                    }
                }, function errorCallback(response) {

                });
            }
            $scope.GetQuotaLeaveList = function (objYear) {
                var URL = CONFIG.SERVER + 'HRTM/SelectLeaveFromYear';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { 'Year': objYear }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.LeaveDashboardContent = [];
                    $scope.LeaveContent = response.data.LeaveContent;
                    $scope.LeaveDetail = response.data.LeaveDetail;
                    $scope.LeaveDetailHeader = response.data.oLeaveDetailHeader;
                    $scope.AttendanceDetailHeader = response.data.oAttendanceDetailHeader;
                    //01 = ลาพักผ่อน
                    //02 = ลาป่วย
                    //03 = ลากิจ
                    //08 = ลาอื่นๆ
                    for (var i = 0; i < $scope.LeaveContent.length; i++) {
                        if ($scope.LeaveContent[i].IsShowOnDashboard) {
                            $scope.LeaveDashboardContent.push({
                                "Seq": i,
                                "LeaveQuotaCode": $scope.LeaveContent[i].LeaveQuotaCode,
                                "LeaveKey": $scope.LeaveContent[i].LeaveTypeKey,
                                "LeaveName": $scope.LeaveContent[i].LeaveTypeName,
                                "RightsGranted": $scope.LeaveContent[i].RightsGranted,
                                "UserLeave": $scope.LeaveContent[i].UserLeave,
                                "UserLeavePercent": Math.round($scope.LeaveContent[i].UserLeavePercent),
                                "BalanceLeave": $scope.LeaveContent[i].BalanceLeave,
                                "BalancePercent": $scope.LeaveContent[i].BalancePercent,
                                "WaitApprove": $scope.LeaveContent[i].WaitApprove
                            });
                        }
                        //switch ($scope.LeaveContent[i].LeaveTypeKey) {
                        //    case "0100":
                        //    case "0210":
                        //    case "0310":
                        //    case "0800":
                        //        $scope.LeaveDashboardContent.push({
                        //            "Seq": i,
                        //            "LeaveKey": $scope.LeaveContent[i].LeaveTypeKey,
                        //            "LeaveName": $scope.LeaveContent[i].LeaveTypeName,
                        //            "RightsGranted": $scope.LeaveContent[i].RightsGranted,
                        //            "UserLeave": $scope.LeaveContent[i].UserLeave,
                        //            "UserLeavePercent": Math.round($scope.LeaveContent[i].UserLeavePercent),
                        //            "BalanceLeave": $scope.LeaveContent[i].BalanceLeave,
                        //            "BalancePercent": $scope.LeaveContent[i].BalancePercent,
                        //            "WaitApprove": $scope.LeaveContent[i].WaitApprove
                        //        });
                        //        break;
                        //    default:
                        //}
                    }
                    if ($scope.LeaveContent.length > 0) {
                        $scope.itemSelected = $scope.LeaveContent[0].LeaveTypeID + ""; //set default
                        $scope.LeaveContentUI = $scope.LeaveContent[0];
                        $scope.LeavePercentage = $scope.LeaveContentUI.UserLeave / $scope.LeaveContentUI.RightsGranted * 100;
                        $scope.currentLeaveID = $scope.LeaveContent[0].LeaveTypeID;
                    }
                }, function errorCallback(response) {
                });
            }
            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) { return $filter('date')(date, 'dd/MM/yyyy'); }
            $scope.getDateFormateData = function (date) {
                return getDateFormateData(date);
            }
            function getDateFormateData(date) { return $filter('date')(date, 'yyyy-MM-dd'); }
        }])
})();