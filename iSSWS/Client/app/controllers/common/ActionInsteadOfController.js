﻿(function () {
angular.module('DESS')
    .controller('ActionInsteadOfController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, CONFIG, $mdDialog) {
        $scope.contentId = $routeParams.id;
        $scope.personDetail = '';
        //var oRequestParameter = { InputParameter: { "CONTENTID": $scope.contentId }, CurrentEmployee: getToken(CONFIG.USER)}
        //var URL = CONFIG.SERVER + 'workflow/GetContentSetting';
        //var URL = CONFIG.SERVER + 'workflow/GetContentSetting/' + $scope.contentId;
        //$http({
        //    method: 'POST',
        //    url: URL,
        //    data: oRequestParameter
        //}).then(function successCallback(response) {
        //    // Success
        //    $scope.actionInsteadOfList = response.data.ManualEmployeeList;
        //    console.log('actionInsteadOfList.', $scope.actionInsteadOfList);
        //}, function errorCallback(response) {
        //    // Error
        //    console.log('error ActionInsteadOfController.', response);
        //    $scope.actionInsteadOfList = {
        //    };
        //});

        $("#findinstandofemployee").on('keyup', function (e) {
            if (e.keyCode == 13) {
                // Do something
                $scope.searchEmployee();
            }
        });
        $scope.searchEmployee = function () {
            $scope.loader.enable = true;
            var oRequestParameter = { InputParameter: { "CONTENTID": $scope.contentId, "SEARCHTEXT": $scope.personDetail }, CurrentEmployee: getToken(CONFIG.USER) }
            $http({
                method: 'POST',
                url:   CONFIG.SERVER + 'workflow/ActionInsteadOf',
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.actionInsteadOfList = response.data;
                $scope.loader.enable = false;
                //if (response.data.length > 0) {
                //    $scope.actionInsteadOfList = response.data;
                //    $scope.loader.enable = false;
                //} else {
                //    $scope.loader.enable = false;
                //    $mdDialog.show(
                //      $mdDialog.alert()
                //        .clickOutsideToClose(true)
                //        .title('INFORMATION')
                //        .textContent($scope.Text['SYSTEM']['notfound'])
                //        .ok('OK')
                //    );
                //}
                

            }, function errorCallback(response) {
                $scope.loader.enable = false;
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('INFORMATION')
                    .textContent($scope.Text['SYSTEM']['notfound'])
                    .ok('OK')
                );
                // Error
                $scope.actionInsteadOfList = [];


            });

        }

        $scope.takeActionInsteadOf = function (contentId, actionInsteadOf_EmployeeID, actionInsteadOf_PositionID, actionInsteadOf_EmployeeName) {
            console.log(contentId + ', ' + actionInsteadOf_EmployeeID + ', ' + actionInsteadOf_PositionID + ', ' + actionInsteadOf_EmployeeName);
            $scope.CurrentEmployee2 = getToken(CONFIG.USER)
            if (angular.isUndefined(actionInsteadOf_EmployeeID) || angular.isUndefined(actionInsteadOf_PositionID) || angular.isUndefined(actionInsteadOf_EmployeeName)) {
                // self action
                $location.path('/frmViewContent/' + contentId).replace();
            } else {
                $location.path('/frmViewContent/' + contentId + '/' + actionInsteadOf_EmployeeID + '/' + actionInsteadOf_PositionID + '/' + actionInsteadOf_EmployeeName + '/' + $scope.CurrentEmployee2.CompanyCode).replace();
            }
        };

        //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
        $scope.takeActionInsteadOfForWorkAcrossCompany = function (contentId, actionInsteadOf_EmployeeID, actionInsteadOf_PositionID, actionInsteadOf_Position, actionInsteadOf_EmployeeName, actionInsteadOf_CompanyCode) {
            console.log(contentId + ', ' + actionInsteadOf_EmployeeID + ', ' + actionInsteadOf_PositionID + ', ' + actionInsteadOf_EmployeeName);
            if (angular.isUndefined(actionInsteadOf_EmployeeID) || angular.isUndefined(actionInsteadOf_PositionID) || angular.isUndefined(actionInsteadOf_EmployeeName) || angular.isUndefined(actionInsteadOf_CompanyCode)) {
                // self action
                $location.path('/frmViewContent/' + contentId).replace();
            } else {
                $location.path('/frmViewContent/' + contentId + '/' + actionInsteadOf_EmployeeID + '/' + actionInsteadOf_PositionID + '/' + actionInsteadOf_Position + '/' + actionInsteadOf_EmployeeName + '/' + actionInsteadOf_CompanyCode).replace();
            }
        };

    }]);
})();