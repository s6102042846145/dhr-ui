﻿(function () {
    angular.module('DESS')
        .controller('RequestViewerController', ['$rootScope', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$window', '$sce', function ($rootScope, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $window, $sce) {
            $scope.content = {};
            $scope.content.isShowHeader = true;
            $scope.content.isShowError = true;
            $scope.content.isShowDocumentWarning = false;
            $scope.isEditor = false;
            $scope.loader.enable = true;
            $scope.content.Header = '';
            $scope.content.documentWarningText = '';
            $scope.requestNo = $routeParams.id;
            $scope.requestCompanyCode = $routeParams.requestCompanyCode;
            $scope.keyMaster = $routeParams.keyMaster;
            $scope.isOwner = $routeParams.isOwner;
            $scope.isDataOwner = $routeParams.isDataOwner;
            $scope.runtime = Date.now();

            $scope.employeeData = getToken(CONFIG.USER);
            if (angular.isUndefined($scope.document)) {
                $scope.requesterData = $scope.employeeData;
            } else {
                $scope.requesterData = $scope.document.Requestor;
            }
            //$scope.requesterData = getToken(CONFIG.USER);//AddBy: Ratchatawan W. (9 jan 2016) - Work across company
            $scope.isActionInsteadOfMode = false;
            $scope.limitToday = $filter('date')(new Date, 'yyyy-MM-ddT00:00:00+07:00');
            $scope.isOldVersion = false;
            $scope.CheckDocumentVersions = function () {
                var doc_create_date = new Date($scope.document.CreatedDate);
                var last_apply_date = new Date($scope.start_date_for_apply_perdiem_distribution); // assign the last date of apply old document for exactly project
                if (doc_create_date < last_apply_date) { $scope.isOldVersion = true; } else { $scope.isOldVersion = false; }
            }
            $scope.tabs = {
                selectedIndex: 0
            };

            $scope.MinDate = new Date(0);
            $scope.VIEW_DETAIL_MODE = false;
            $scope.viewDetailMode = function (set) {
                $scope.VIEW_DETAIL_MODE = set;
            }

            //function for get style display on ng-bind-html
            $scope.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };

            if (!(angular.isUndefined($routeParams.requesterEmployeeId) || angular.isUndefined($routeParams.requesterPositionId))) {
                $scope.employeeData.RequesterEmployeeID = $routeParams.requesterEmployeeId;
                $scope.employeeData.RequesterPositionID = $routeParams.requesterPositionId;
                $scope.requesterName = decodeURIComponent($routeParams.requesterName);
                $scope.isActionInsteadOfMode = true;

                //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                $scope.requesterData.EmployeeID = $routeParams.requesterEmployeeId;
                $scope.requesterData.PositionID = $routeParams.requesterPositionId;
                $scope.requesterData.CompanyCode = $routeParams.requesterCompanyCode;
            }

            $scope.Textcategory = 'REQUESTHEADER';
            if (!angular.isUndefined($routeParams.textHeader) && $routeParams.textHeader != '') {
                $scope.content.Header = decodeURIComponent($routeParams.textHeader);
            } else {
                $scope.content.Header = $scope.Text['SYSTEM']['BACK'];
            }

            $scope.templateControl = {
                isDeleteRequest: false,
                isNotSubEditor: true,
                isShowDialogComment: true,
                isRequestViewerController: true
            };

            /* Set wizard style form editor */
            $scope.beginFormWizard = function () {
                $scope.templateControl.isNotSubEditor = false;
            };

            $scope.finishFormWizard = function () {
                $scope.templateControl.isNotSubEditor = true;
            }

            var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentByRequestNo';
            var oRequestParameter = { InputParameter: { "RequestNo": $scope.requestNo, "RequestCompanyCode": $scope.requestCompanyCode, "KeyMaster": $scope.keyMaster, "IsOwnerView": $scope.isOwner, "IsDataOwnerView": $scope.isDataOwner }, CurrentEmployee: getToken(CONFIG.USER) };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.document = response.data;
                console.log('RequestViewerController GetRequestDocumentByRequestNo success.');
                $scope.loader.enable = false;
            }, function errorCallback(response) {
                console.log('RequestViewerController GetRequestDocumentByRequestNo error.');
                $scope.document = null;
                $scope.loader.enable = false;
            });

            $scope.getRequestViewerTemplate = function (r) {
                var viewer = $scope.document.Viewer;
                return 'views/' + viewer + '.html?t=' + r;
            }

            //Functin Set Data / Load Data
            $scope.ChildAction = {
                LoadData: null,
                SetData: null
            };

            var TEMP_TO_RESEND_ACTION_URL;
            var reqActInpr = false;
            $scope.doRequestAction = function (actionUrl) {
                $scope.loader.enable = true;
                reqActInpr = false;
                if (reqActInpr) {
                    return;
                }
                if ($scope.blockAction.isBLock) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('WARNING')
                            .textContent($scope.blockAction.blockMessage)
                            .ok('OK')
                    );
                    return;
                }
                reqActInpr = true;
                TEMP_TO_RESEND_ACTION_URL = actionUrl;

                var authUser = getToken(CONFIG.USER);

                if ($scope.ChildAction.LoadData == null)
                    alert('Please implement function $scope.ChildAction.LoadData before');
                else {
                    $scope.ChildAction.LoadData();
                    var actionDoc = angular.copy($scope.document);

                    if (actionDoc.ActionBy != null) //AddBy: Ratchatawan W. (2016-10-05)
                    {
                        if (actionDoc.ActionBy.EmployeeID != authUser.EmployeeID) { actionDoc.ActionBy = authUser; }
                    }
                    else {
                        actionDoc.ActionBy = authUser;
                        { actionDoc.ActionBy = authUser; }
                    }
                    if ($scope.document.Additional != null) {
                        var obj = actionDoc.Additional;
                    }
                    var oRequestParameter = { InputParameter: {}, RequestDocument: actionDoc, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                    $http({
                        method: 'POST',
                        url: CONFIG.SERVER + actionUrl,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.loader.enable = false;
                        $scope.document = response.data;
                        $scope.$parent.PassingDocument.document = angular.copy($scope.document);
                        // Redirect to response.data.RedirectURL (if empty then show error)

                        if (response.data != null && $scope.document.HasError) {
                            $scope.ChildAction.SetData();
                        }

                        if (response.data != null && response.data.RedirectURL != '') {
                            // Reload Menu
                            var args = {};
                            $rootScope.$broadcast('onReloadMenu', args);
                            reqActInpr = false;
                            $location.path(response.data.RedirectURL);
                        } else {
                            $scope.loader.enable = false;
                            if (response.data == null) {
                                $scope.document.HasError = true;
                                $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                            }
                            //else if (response.data.ErrorText.trim() == $scope.Text['TRAVELEXPENSE']['REPORT_OVERDUE_POPUP']) {
                            //    $scope.document.HasError = response.data.HasError;
                            //    $scope.document.ErrorText = response.data.ErrorText;
                            //    reqOverdueReason();
                            //}
                            //else if (response.data.ErrorText.trim() == $scope.Text['TRAVELEXPENSE']['CUSTODIAN_COMMENT_REQUIRED']) {
                            //    $scope.document.HasError = response.data.HasError;
                            //    $scope.document.ErrorText = response.data.ErrorText;
                            //    reqCustodianReason();
                            //}
                            else {
                                $scope.document.HasError = response.data.HasError;
                                $scope.document.ErrorText = response.data.ErrorText;

                                if (response.data.RequireComment) {
                                    chekreason();
                                }
                            }
                        }
                        // reqActInpr = false;
                        //  console.log.bind(console, '%c %s');
                    }, function errorCallback(response) {
                        // Error
                        $scope.loader.enable = false;
                        $scope.document.HasError = true;
                        reqActInpr = false;
                        $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                        // console.log.bind(console, '%c %s');
                    });
                }
            };

            function reqCustodianReason() {
                reqActInpr = false;
                var parentEl = angular.element(document.body);
                $mdDialog.show({
                    parent: parentEl,
                    template:
                        '<md-dialog aria-label="List dialog" style=" width: 100%; max-width: 700px;height: 100%;max-height: 300px; padding: 10px;">' +
                        '  <md-dialog-content class="-txt-blue"  style=" font-size:20px; padding: 15px; height: 76px; overflow: hidden;" ng-bind-html="description.title" >' +
                        '     ' +
                        '  </md-dialog-content>' +
                        '  <md-dialog-actions style=" border: none; position:relative;">' +
                        '    <div class="alert alert-warning" role="alert" style="left: 10px; right: 10px;">' +
                        '      <div style="font-size:20px;" class="ng-binding">{{description.box_title}}</div>' +
                        '         <div>' +
                        '            <textarea rows="4" style="width:100%;resize: none; padding:10px; border: 1px solid;" ng-model="comment" class="ng-pristine ng-untouched ng-valid" aria-invalid="false"></textarea>' +
                        '         </div>' +
                        '     <div class="clearfix"></div>' +
                        '    </div>' +
                        '  </md-dialog-actions>' +
                        '    <md-button ng-click="closeAndSendDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 100px; font-size: 20px">' +
                        '      {{description.btnTxt}}' +
                        '    </md-button>' +
                        '    <md-button ng-click="closeDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 0;  font-size: 20px">' +
                        '      {{description.btnCancel}}' +
                        '    </md-button>' +
                        '</md-dialog>',
                    locals: {
                        description: {
                            title: $scope.Text['TRAVELEXPENSE']['CUSTODIAN_COMMENT_REQUIRED_POPUP'],
                            box_title: $scope.Text['SYSTEM']['reason_'],
                            btnTxt: $scope.Text['TRAVELEXPENSE']['CUSTODIAN_COMMENT_REQUIRED_POPUP_CONFIRM'],
                            btnCancel: $scope.Text['TRAVELEXPENSE']['CUSTODIAN_COMMENT_REQUIRED_POPUP_CANCEL']
                        }
                    },
                    controller: DialogController
                }).then(function (obj) {
                    $scope.document.Comment2 = obj.comment;
                    if (obj.isResend) {
                        if ($scope.document.Comment2) {
                            $scope.doRequestAction(TEMP_TO_RESEND_ACTION_URL);
                        } else {
                            $scope.doRequestAction(TEMP_TO_RESEND_ACTION_URL);
                        }

                    }
                    else {
                        //เมื่อทำการ กรอกข้อมูล Remark แล้วทำการปิด Popup แล้วกด ไม่อนุมัติ ให้ทำการ clear ค่า Comment เป็น ค่าว่าง
                        $scope.document.Comment2 = "";   //Modify by Nipon Supap
                    }
                });

                function DialogController($scope, $mdDialog, description) {
                    $scope.comment = '';
                    $scope.description = description;

                    $scope.closeDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: false
                        };
                        $mdDialog.hide(retunrParam);
                    }
                    $scope.closeAndSendDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: true
                        };
                        $mdDialog.hide(retunrParam);
                    }
                }

            }

            function reqOverdueReason() {
                reqActInpr = false;
                var parentEl = angular.element(document.body);
                $mdDialog.show({
                    parent: parentEl,
                    template:
                        '<md-dialog aria-label="List dialog" style=" width: 100%; max-width: 700px;height: 100%;max-height: 300px; padding: 10px;">' +
                        '  <md-dialog-content class="-txt-blue"  style=" font-size:20px; padding: 15px; height: 76px; overflow: hidden;" ng-bind-html="description.title" >' +
                        '     ' +
                        '  </md-dialog-content>' +
                        '  <md-dialog-actions style=" border: none; position:relative;">' +
                        '    <div class="alert alert-warning" role="alert" style="left: 10px; right: 10px;">' +
                        '      <div style="font-size:20px;" class="ng-binding">{{description.box_title}}</div>' +
                        '         <div>' +
                        '            <textarea rows="4" style="width:100%;resize: none; padding:10px; border: 1px solid;" ng-model="comment" class="ng-pristine ng-untouched ng-valid" aria-invalid="false"></textarea>' +
                        '         </div>' +
                        '     <div class="clearfix"></div>' +
                        '    </div>' +
                        '  </md-dialog-actions>' +
                        '    <md-button ng-click="closeAndSendDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 100px; font-size: 20px">' +
                        '      {{description.btnTxt}}' +
                        '    </md-button>' +
                        '    <md-button ng-click="closeDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 0;  font-size: 20px">' +
                        '      {{description.btnCancel}}' +
                        '    </md-button>' +
                        '</md-dialog>',
                    locals: {
                        description: {
                            title: $scope.Text['TRAVELEXPENSE']['REPORT_OVERDUE_POPUP'],
                            box_title: $scope.Text['SYSTEM']['reason_'],
                            btnTxt: $scope.Text['TRAVELEXPENSE']['REPORT_OVERDUE_POPUP_CONFIRM'],
                            btnCancel: $scope.Text['TRAVELEXPENSE']['REPORT_OVERDUE_POPUP_CANCEL']
                        }
                    },
                    controller: DialogController
                }).then(function (obj) {
                    $scope.document.Comment = obj.comment;
                    if (obj.isResend) {
                        if ($scope.document.Comment) {
                            $scope.doRequestAction(TEMP_TO_RESEND_ACTION_URL);
                        } else {
                            $scope.doRequestAction(TEMP_TO_RESEND_ACTION_URL);
                        }

                    }
                    else {
                        //เมื่อทำการ กรอกข้อมูล Remark แล้วทำการปิด Popup แล้วกด ไม่อนุมัติ ให้ทำการ clear ค่า Comment เป็น ค่าว่าง
                        $scope.document.Comment = "";   //Modify by Nipon Supap
                    }
                });

                function DialogController($scope, $mdDialog, description) {
                    $scope.comment = '';
                    $scope.description = description;

                    $scope.closeDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: false
                        };
                        $mdDialog.hide(retunrParam);
                    }
                    $scope.closeAndSendDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: true
                        };
                        $mdDialog.hide(retunrParam);
                    }
                }

            }

            var btnTxt = '';
            $scope.setCurentButtonTxt = function (iBtnTxt) {
                btnTxt = iBtnTxt;
            }
            function chekreason() {
                var parentEl = angular.element(document.body);
                $mdDialog.show({
                    parent: parentEl,
                    template:
                        '<md-dialog aria-label="List dialog" style=" width: 100%; max-width: 700px;height: 100%;max-height: 300px; padding: 10px;">' +
                        '  <md-dialog-content class="-txt-blue" style=" font-size:20px; margin-bottom: 30px; height: 76px; overflow: hidden;" ng-bind-html="description.title" >' +
                        '    ' +
                        '  </md-dialog-content>' +
                        '  <md-dialog-actions style=" border: none; position:relative;">' +
                        '    <div class="alert alert-warning" role="alert" style="left: 10px; right: 10px;">' +
                        '      <div style="font-size:20px;" class="ng-binding">{{description.box_title}}</div>' +
                        '         <div>' +
                        '            <textarea rows="4" style="width:100%;resize: none; padding:10px; border: 1px solid;" ng-model="comment" class="ng-pristine ng-untouched ng-valid" aria-invalid="false"></textarea>' +
                        '         </div>' +
                        '     <div class="clearfix"></div>' +
                        '    </div>' +
                        '  </md-dialog-actions>' +
                        '    <md-button ng-click="closeAndSendDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 100px; font-size: 20px">' +
                        '      {{description.btnTxt}}' +
                        '    </md-button>' +
                        '    <md-button ng-click="closeDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 0;  font-size: 20px">' +
                        '      {{description.btnCancel}}' +
                        '    </md-button>' +
                        '</md-dialog>',
                    locals: {
                        description: {
                            title: $scope.document.ErrorText, // get text error to display in the dialog
                            box_title: $scope.Text['SYSTEM']['reason_'],
                            btnTxt: btnTxt,
                            btnCancel: $scope.Text['SYSTEM']['CLOSE_DOWN']
                        }
                    },
                    controller: DialogController
                }).then(function (obj) {
                    $scope.document.Comment = obj.comment;
                    if (obj.isResend) {
                        $scope.doRequestAction(TEMP_TO_RESEND_ACTION_URL);
                    }
                    else {
                        //เมื่อทำการ กรอกข้อมูล Remark แล้วทำการปิด Popup แล้วกด ไม่อนุมัติ ให้ทำการ clear ค่า Comment เป็น ค่าว่าง
                        $scope.document.Comment = "";   //Modify by Nipon Supap
                    }
                });
                function DialogController($scope, $mdDialog, description) {
                    $scope.comment = '';
                    $scope.description = description;

                    $scope.closeDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: false
                        };
                        $mdDialog.hide(retunrParam);
                    }
                    $scope.closeAndSendDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: true
                        };
                        $mdDialog.hide(retunrParam);
                    }
                }
            }

            $scope.getFileAttach = function (setId, id) {
                var URL = CONFIG.SERVER + 'workflow/GetFile/' + setId + '/' + id + '/' + $scope.employeeData.CompanyCode + '/' + $scope.employeeData.EmployeeID;
                if (typeof cordova != 'undefined') {
                    cordova.InAppBrowser.open(URL, '_system', 'location=no');
                } else {
                    window.open(URL, '_blank');
                }
            };
            $scope.getFileFromPath = function (attachment) {
                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {

                    } else if (attachment.FilePath) {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        console.debug(path);
                        $window.open(path);
                    } else {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment;
                        console.debug(path);
                        $window.open(path);

                    }
                }
                /* !direct */
            }
            $scope.Textcategory = 'SYSTEM';

            $scope.LoadResponseLogPost = function () {
                //$scope.requestNo
                var URL = CONFIG.SERVER + 'workflow/GetResponseLogPost';
                var oRequestParameter = { InputParameter: { "RequestNo": $scope.requestNo, "RequestCompanyCode": $scope.requestCompanyCode }, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PostLog = response.data;
                    console.log('document.', $scope.document);

                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                    $scope.document = null;
                });
            }
            //Add by Nipon Supap 06-06-2017
            $scope.LoadResponseLogManual = function () {
                var URL = CONFIG.SERVER + 'workflow/GetResponseManualLog';
                var oRequestParameter = { InputParameter: { "RequestNo": $scope.requestNo, "RequestCompanyCode": $scope.requestCompanyCode }, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PostLogManual = response.data;
                    console.log('document.', $scope.document);

                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                    $scope.document = null;
                });
            }
            $scope.LoadFBCJResponseLogPost = function () {
                //$scope.requestNo
                var URL = CONFIG.SERVER + 'workflow/GetFBCJResponseLogPost';
                var oRequestParameter = { InputParameter: { "RequestNo": $scope.requestNo, "RequestCompanyCode": $scope.requestCompanyCode }, CurrentEmployee: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.FBCJPostLog = response.data;
                    console.log('document.', $scope.document);
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                    $scope.document = null;
                });
            }
            $scope.openViewContent = function (requestType) {
                $location.path('/frmViewContent/' + requestType);
            }
        }])
        .controller('RequestEditorController', ['$rootScope', '$scope', '$http', '$routeParams', '$location', '$timeout', 'CONFIG', '$mdDialog', '$window', '$sce', function ($rootScope, $scope, $http, $routeParams, $location, $timeout, CONFIG, $mdDialog, $window, $sce) {
            $scope.content = {};
            $scope.content.isShowHeader = true;
            $scope.content.Header = '';
            $scope.content.isShowError = true;
            $scope.isEditor = true;
            $scope.requestNo = $routeParams.id;
            $scope.keyMaster = $routeParams.keyMaster;
            $scope.isOwner = $routeParams.isOwner;
            $scope.isDataOwner = $routeParams.isDataOwner;
            $scope.employeeData = getToken(CONFIG.USER);
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            //function for get style display on ng-bind-html
            $scope.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };

            //change by Nattawat S. case action instead of
            if (angular.isUndefined($scope.$parent.PassingDocument.document)) {
                $scope.requesterData = $scope.employeeData;
            } else {
                //$scope.requesterData.EmployeeID = $scope.$parent.PassingDocument.document.RequestorNo;
                //$scope.requesterData.RequestorName = $scope.$parent.PassingDocument.document.RequestorName;
                //$scope.requesterData.PositionID = $scope.$parent.PassingDocument.document.RequestorPosition;
                //$scope.requesterData.Position = $scope.$parent.PassingDocument.document.RequestorPositionName;

                $scope.requesterData = $scope.$parent.PassingDocument.document.Requestor;
                $scope.requesterData.Language = $scope.employeeData.Language;
            }


            //$scope.requesterData = getToken(CONFIG.USER);//AddBy: Ratchatawan W. (9 jan 2016) - Work across company
            $scope.isActionInsteadOfMode = false;
            $scope.documentState.isDirty = true;
            $scope.isOldVersion = false;
            $scope.CheckDocumentVersions = function () {
                var doc_create_date = new Date($scope.document.CreatedDate);
                var last_apply_date = new Date($scope.start_date_for_apply_perdiem_distribution); // assign the last date of apply old document for exactly project
                if (doc_create_date < last_apply_date) { $scope.isOldVersion = true; } else { $scope.isOldVersion = false; }
                console.log('line 544');
            }

            if (!angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey > 0) {
                $scope.itemKeys = (unescape($routeParams.itemKey)).split('|');
            }
            $scope.referRequestNo = angular.isUndefined($routeParams.referRequestNo) && $routeParams.referRequestNo != '0' ? '' : $routeParams.referRequestNo;
            if (!(angular.isUndefined($routeParams.requesterEmployeeId) || angular.isUndefined($routeParams.requesterPositionId))) {
                // using action instead of
                //alert('create with actionInsteadOf');
                $scope.employeeData.RequesterEmployeeID = $routeParams.requesterEmployeeId;
                $scope.employeeData.RequesterPositionID = $routeParams.requesterPositionId;
                $scope.requesterName = decodeURIComponent($routeParams.requesterName);
                $scope.isActionInsteadOfMode = true;

                //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                $scope.requesterData.EmployeeID = $routeParams.requesterEmployeeId;
                $scope.requesterData.PositionID = $routeParams.requesterPositionId;
                $scope.requesterData.CompanyCode = $routeParams.requesterCompanyCode;
                console.log('line 563');
            }

            $scope.runtime = Date.now();
            $scope.Textcategory = 'REQUESTHEADER';
            if (!angular.isUndefined($routeParams.textHeader) && $routeParams.textHeader != '') {
                $scope.content.Header = decodeURIComponent($routeParams.textHeader);
            } else {
                if (angular.isDefined($scope.Text['SYSTEM'])) {
                    $scope.content.Header = $scope.Text['SYSTEM']['BACK'];
                }
            }
            $scope.templateControl = {
                isDeleteRequest: false,
                isNotSubEditor: true,
                isRequestViewerController: false
            };

            /* Set wizard style form editor */
            $scope.beginFormWizard = function () {
                $scope.templateControl.isNotSubEditor = false;
            };

            $scope.finishFormWizard = function () {
                $scope.templateControl.isNotSubEditor = true;
            }

            $scope.document = $scope.$parent.PassingDocument.document;
            //Add by Koissares 20200326
            //if (!(angular.isUndefined($scope.document.Requestor))) {
            //    $scope.requesterData = $scope.document.Requestor;
            //}
            $scope.getRequestEditorTemplate = function (r) {
           
                var editor = $scope.document.Editor;
                return 'views/' + editor + '.html?t=' + r;
            }

            //Functin Set Data / Load Data
            $scope.ChildAction = {
                LoadData: null,
                SetData: null,
                ValidateData: null
            };
            var temp_list_perdiumDistributionList = [];
            var TEMP_TO_RESEND_ACTION_URL;
            var timerErrorMsg = null;
            var reqActInpr = false;
            $scope.doRequestAction = function (actionUrl) {
                $scope.loader.enable = true;
                reqActInpr = false;
                if (reqActInpr) {
                    return;
                }
                if ($scope.blockAction.isBLock) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('WARNING')
                            .textContent($scope.blockAction.blockMessage)
                            .ok('OK')
                    );
                    return;
                }
                if (timerErrorMsg) {
                    $timeout.cancel(timerErrorMsg);
                }
                TEMP_TO_RESEND_ACTION_URL = actionUrl;
                var authUser = getToken(CONFIG.USER);

                if ($scope.ChildAction.LoadData == null)
                    alert('Please implement function $scope.ChildAction.LoadData');
                else {
                    $scope.ChildAction.LoadData();
                    //if validateData==null because not implement in js controller
                    if ($scope.ChildAction.ValidateData == null) {
                        reqActInpr = true;
                        var actionDoc = angular.copy($scope.document);
                        actionDoc.ActionBy = authUser;
                        var oRequestParameter = { InputParameter: {}, RequestDocument: actionDoc }
                        $scope.document.HasError = false;
                        $scope.content.isShowError = true;
                        $http({
                            method: 'POST',
                            url: CONFIG.SERVER + actionUrl,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success  
                            $scope.loader.enable = false;
                            // Redirect to response.data.RedirectURL (if empty then show error)
                            if (response.data != null && response.data.RedirectURL != '') {
                                $scope.documentState.isDirty = false;
                                // fix bug control in child not update when submit after validate fail
                                $scope.document = response.data;
                                $scope.$parent.PassingDocument.document = angular.copy($scope.document);
                                console.log('passing document.', $scope.$parent.PassingDocument);
                                // !fix bug control in child not update when submit after validate fail

                                // Reload Menu
                                var args = {};
                                $rootScope.$broadcast('onReloadMenu', args);
                                reqActInpr = false;

                                $location.path(response.data.RedirectURL).replace();
                            } else {
                                $scope.loader.enable = false;
                                if (response.data == null) {
                                    $scope.document.HasError = true;
                                    $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                                } else {
                                    $scope.document.HasError = response.data.HasError;
                                    $scope.document.ErrorText = response.data.ErrorText;
                                    if (response.data.RequireComment) {
                                        chekreason();
                                    }
                                }
                                if (temp_list_perdiumDistributionList.length > 0) {
                                    $scope.document.Additional.PerdiumDistributionList = angular.copy(temp_list_perdiumDistributionList);
                                }
                            }
                            reqActInpr = false;
                        }, function errorCallback(response) {
                            // Error
                            $scope.loader.enable = false;
                            $scope.document.HasError = true;
                            reqActInpr = false;
                            $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                            timerErrorMsg = $timeout(function () {
                                $scope.content.isShowError = false;
                            }, 15000);
                            console.log('error doRequestAction.', response);

                        });
                    } else {
                        if ($scope.ChildAction.ValidateData()) {
                            reqActInpr = true;
                            var actionDoc = angular.copy($scope.document);
                            actionDoc.ActionBy = authUser;
                            console.log('obj doRequestAction.', actionDoc);
                            var oRequestParameter = { InputParameter: {}, RequestDocument: actionDoc }
                            $scope.document.HasError = false;
                            $scope.content.isShowError = true;
                            $http({
                                method: 'POST',
                                url: CONFIG.SERVER + actionUrl,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                // Success
                                // Redirect to response.data.RedirectURL (if empty then show error)
                                if (response.data != null && response.data.RedirectURL != '') {
                                    $scope.documentState.isDirty = false;
                                    // fix bug control in child not update when submit after validate fail
                                    $scope.document = response.data;
                                    $scope.$parent.PassingDocument.document = angular.copy($scope.document);
                                    console.log('passing document.', $scope.$parent.PassingDocument);
                                    // !fix bug control in child not update when submit after validate fail

                                    // Reload Menu
                                    var args = {};
                                    $rootScope.$broadcast('onReloadMenu', args);
                                    reqActInpr = false;
                                    $location.path(response.data.RedirectURL).replace();
                                } else {
                                    $scope.loader.enable = false;
                                    if (response.data == null) {
                                        $scope.document.HasError = true;
                                        $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                                    } else {
                                        $scope.document.HasError = response.data.HasError;
                                        $scope.document.ErrorText = response.data.ErrorText;

                                        if (response.data.RequireComment) {
                                            chekreason();
                                        }

                                    }
                                    if (temp_list_perdiumDistributionList.length > 0) {
                                        $scope.document.Additional.PerdiumDistributionList = angular.copy(temp_list_perdiumDistributionList);
                                    }
                                    //timerErrorMsg = $timeout(function () {
                                    //    $scope.content.isShowError = false;
                                    //}, 15000);
                                }
                                //reqActInpr = false;
                            }, function errorCallback(response) {
                                // Error
                                $scope.loader.enable = false;
                                $scope.document.HasError = true;
                                reqActInpr = false;
                                $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                                timerErrorMsg = $timeout(function () {
                                    $scope.content.isShowError = false;
                                }, 15000);
                                console.log('error doRequestAction.', response);

                            });
                        } else {
                            $scope.loader.enable = false;
                            reqActInpr = false;
                        }
                    }


                }
            };
            var btnTxt = '';
            $scope.setCurentButtonTxt = function (iBtnTxt) {
                btnTxt = iBtnTxt;
            }
            function chekreason() {
                var parentEl = angular.element(document.body);
                $mdDialog.show({
                    parent: parentEl,
                    template:
                        '<md-dialog aria-label="List dialog" style=" width: 100%; max-width: 700px;height: 100%;max-height: 300px; padding: 10px;">' +
                        '  <md-dialog-content class="-txt-blue" style="    height: 65px; font-size:20px; padding: 15px;">' +
                        '     {{description.title}}' +
                        '  </md-dialog-content>' +
                        '  <md-dialog-actions style=" border: none; position:relative;">' +
                        '    <div class="alert alert-warning" role="alert" style="left: 10px; right: 10px;">' +
                        '      <div style="font-size:20px;" class="ng-binding">{{description.box_title}}</div>' +
                        '         <div>' +
                        '            <textarea rows="4" style="width:100%;resize: none; padding:10px; border: 1px solid;" ng-model="comment" class="ng-pristine ng-untouched ng-valid" aria-invalid="false"></textarea>' +
                        '         </div>' +
                        '     <div class="clearfix"></div>' +
                        '    </div>' +
                        '  </md-dialog-actions>' +
                        '    <md-button ng-click="closeAndSendDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 100px; font-size: 20px">' +
                        '      {{description.btnTxt}}' +
                        '    </md-button>' +
                        '    <md-button ng-click="closeDialog()" class="md-primary" style="position: absolute; bottom: 0; right: 0;  font-size: 20px">' +
                        '      {{description.btnCancel}}' +
                        '    </md-button>' +
                        '</md-dialog>',
                    locals: {
                        description: {
                            title: $scope.document.ErrorText, // get text error to display in the dialog
                            box_title: $scope.Text['SYSTEM']['reason_'],
                            btnTxt: btnTxt,
                            btnCancel: $scope.Text['SYSTEM']['CLOSE_DOWN']
                        }
                    },
                    controller: DialogController
                }).then(function (obj) {
                    $scope.document.Comment = obj.comment;
                    if (obj.isResend) {
                        $scope.doRequestAction(TEMP_TO_RESEND_ACTION_URL);
                    }

                });
                function DialogController($scope, $mdDialog, description) {
                    $scope.comment = '';
                    $scope.description = description;

                    $scope.closeDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: false
                        };
                        $mdDialog.hide(retunrParam);
                    }
                    $scope.closeAndSendDialog = function () {
                        var retunrParam = {
                            comment: $scope.comment,
                            isResend: true
                        };
                        $mdDialog.hide(retunrParam);
                    }
                }
            }

            $scope.getFileAttach = function (setId, id) {
                var URL = CONFIG.SERVER + 'workflow/GetFile/' + setId + '/' + id + '/' + $scope.employeeData.CompanyCode + '/' + $scope.employeeData.EmployeeID;
                if (typeof cordova != 'undefined') {
                    cordova.InAppBrowser.open(URL, '_system', 'location=no');
                } else {
                    window.open(URL, '_blank');
                }
                console.log('line 841');
            };

            $scope.getFileFromPath = function (attachment) {
                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {

                    } else if (attachment.FilePath) {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        console.debug(path);
                        $window.open(path);
                    } else {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment;
                        console.debug(path);
                        $window.open(path);

                    }
                }
                console.log('line 860');
            }

            /* File attachment */
            $scope.formInput = { file: null };
            $scope.AddFile = function () {
                var f = document.getElementById('file').files[0], r = new FileReader();

            }
            $scope.onAfterValidateFunc = function (event, fileList) {
                var newFile = {
                    FileID: -1,
                    IsDelete: false,
                    FileSetID: $scope.document.FileSetID,
                    FileName: fileList[0].filename,
                    Data: fileList[0].base64,
                    FileType: fileList[0].filetype,
                    FileSize: fileList[0].filesize
                };
                if ($scope.AllowUploadFile(newFile.FileName)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['VALIDATE_FILENAME'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILETYPE'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE2'])
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else {
                    $scope.document.FileSet.Attachments.push(newFile);
                    $scope.document.HasFileAttached = true;
                }
                newFile = null;
                $scope.formInput.file = null;
                angular.element("#CORE_document-attachment-file-input").val(null); //Nattawat S. Add New Naming , prepare for automated test
            };

            $scope.deleteFileAttach = function (rowIndex) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['SYSTEM']['WARNING'])
                    .textContent($scope.Text['SYSTEM']['CONFIRM_DELETE'])
                    .targetEvent(rowIndex)
                    .ok('OK')
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);

                $mdDialog.show(confirm).then(function () {
                    if ($scope.document.FileSet.Attachments[rowIndex].FileID != -1) {
                        // delete server attach file (set flag delete)
                        $scope.document.FileSet.Attachments[rowIndex].IsDelete = true;
                        console.log('after delete file attachment.', angular.copy($scope.document.FileSet));
                    } else {
                        // delete new attach file (remove from array)
                        $scope.document.FileSet.Attachments.splice(rowIndex, 1);
                        console.log('after delete file attachment.', angular.copy($scope.document.FileSet));
                    }

                }, function () {

                });
                if ($scope.document.FileSet.Attachments.length <= 0) {
                    // ignore flag isDelete in array
                    $scope.document.HasFileAttached = false;
                }

            };
            function remove(oDelegate, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text[$scope.PATextcategory].M3)
                    .textContent($scope.Text[$scope.PATextcategory].M1)
                    .targetEvent(ev)
                    .ok('OK')
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);

                $mdDialog.show(confirm).then(function () {


                    $scope.delegationloader = true
                    reqRemoveDelegation(oDelegate);

                }, function () {

                });
            }
            $scope.recoveryFileAttach = function (rowIndex) {
                $scope.document.FileSet.Attachments[rowIndex].IsDelete = false;
                $scope.document.HasFileAttached = true;
                console.log('line 982');
            };

            $scope.openViewContent = function (requestType) {
                console.log('line 986');
                $location.path('/frmViewContent/' + requestType);
            };

        }])
        .controller('RequestCreateController', ['$rootScope', '$scope', '$http', '$routeParams', '$location', 'CONFIG', '$mdDialog', function ($rootScope, $scope, $http, $routeParams, $location, CONFIG, $mdDialog) {
            $scope.isEditor = false;
            $scope.requestTypeId = $routeParams.id;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? '' : $routeParams.itemKey;
            $scope.referRequestNo = angular.isUndefined($routeParams.referRequestNo) && $routeParams.referRequestNo != '0' ? '' : $routeParams.referRequestNo;
            $scope.otherParam = angular.isUndefined($routeParams.otherParam) && $routeParams.otherParam != '0' ? '' : $routeParams.otherParam;
            $scope.runtime = Date.now();
            $scope.employeeData = getToken(CONFIG.USER);
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if (angular.isUndefined($scope.document)) {
                $scope.requesterData = $scope.employeeData;
            } else {
                $scope.requesterData = $scope.document.Requestor;
            }
            //$scope.requesterData = getToken(CONFIG.USER);//AddBy: Ratchatawan W. (9 jan 2016) - Work across company
            $scope.isActionInsteadOfMode = false;
            $scope.loader.enable = true;

            $scope.templateControl = {
                isDeleteRequest: false,
                isNotSubEditor: true,
                isShowDialogComment: false,
                isRequestViewerController: false
            };


            if (!(angular.isUndefined($routeParams.requesterEmployeeId) || angular.isUndefined($routeParams.requesterPositionId))) {
                // using action instead of
                //alert('create with actionInsteadOf');
                $scope.employeeData.RequesterEmployeeID = $routeParams.requesterEmployeeId;
                $scope.employeeData.RequesterPositionID = $routeParams.requesterPositionId;
                $scope.requesterName = decodeURIComponent($routeParams.requesterName);
                $scope.isActionInsteadOfMode = true;

                //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                $scope.requesterData.EmployeeID = $routeParams.requesterEmployeeId;
                $scope.requesterData.PositionID = $routeParams.requesterPositionId;
                $scope.requesterData.CompanyCode = $routeParams.requesterCompanyCode;
            }

            var oRequestParameter = { InputParameter: { "REQUESTTYPEID": $scope.requestTypeId, "ITEMKEY": $scope.itemKey, "REFER_REQUESTNO": $scope.referRequestNo, "OTHER_PARAM": $scope.otherParam }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData, Creator: getToken(CONFIG.USER) }
            var actionUrl = 'workflow/CreateRequest';
            $http({
                method: 'POST',
                url: CONFIG.SERVER + actionUrl,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.loader.enable = false;
                $scope.document = response.data;
                if ($scope.document.HasError) {
                    // Has error
                } else {
                    // Success
                    $scope.$parent.PassingDocument.document = angular.copy(response.data);

                    console.log('passing document.', $scope.$parent.PassingDocument);
                    // Redirect to response.data.RedirectURL (if empty then show error)
                    if (response.data != null && response.data.RedirectURL != '') {
                        if (!angular.isUndefined($routeParams.textHeader) && $routeParams.textHeader != '') {

                            if ($scope.isActionInsteadOfMode) {
                                // has actionInsteadOf
                                $location.path(response.data.RedirectURL + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode + '/' + $routeParams.otherParam).replace();
                            } else {
                                $location.path(response.data.RedirectURL).replace();
                            }

                        } else {
                            $location.path(response.data.RedirectURL).replace();
                        }
                    }
                }

            }, function errorCallback(response) {
                // Error
                $scope.loader.enable = false;
                if (angular.isUndefined($scope.document) || $scope.document == null) {
                    $scope.document = {};
                }

                $scope.document.HasError = true;
                $scope.document.ErrorText = $scope.Text['SYSTEM']['REQUEST_WEBSERVICE_ERROR'];
                console.log('error doRequestAction.', response);

            });
        }]);
})();