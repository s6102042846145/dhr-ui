﻿(function () {
    "use strict";
    angular.module('DESS')
        // datepicker แบบเลือกวันที่เดียว
        //<ess-datepicker ng-model="model ที่ต้องการ binding" min="วันที่น้อยสุด" max="วันที่มากสุด" 
        //              width="ความกว้าง px ถ้าไม่ใส่ จะเป็น 100%" 
        //              ng-change="call back function จะใส่ argument อะไรเข้ามาก็ได้ "></ess-datepicker>

        .directive('gDatepicker', function () {
            return {
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    date: '=ngModel',

                    text: '=',
                    title: '=',
                    disabled: '=',
                    format: '=',
                    ngChange: '&'
                },
                templateUrl: 'views/common/components/datepicker-g.html',
                compile: function (element, attrs) {
                    if (angular.isUndefined(attrs.disabled) || attrs.disabled === null) { attrs.disabled = false; }
                    if (angular.isUndefined(attrs.format) || attrs.format === null) { attrs.format = 'yyyy-MM-dd'; }
                },
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {

                    $scope.runtime = Date.now();
                    $scope.isShow = false;
                    $scope.errMessage = '';

                    $scope.months = [];

                    $scope.data = {
                        day: '', month: '0', year: ''
                    };
                    $scope.showText = '';


                    if (angular.isUndefined($scope.min))
                        $scope.min = null;

                    if (angular.isUndefined($scope.max))
                        $scope.max = null;

                    var init = function () {

                        initMonths();


                        $scope.errMessage = '';

                        $scope.$watch('date', function (newvalue, oldvalue) {
                            
                            setShowText();
                            if (typeof $scope.ngChange === 'function') {
                                $scope.ngChange({ newvalue: dtMoment(newvalue) });
                            }
                        });

                    };

                    $scope.onClickInput = function () {

                        $scope.isShow = true;

                    };

                    var initMonths = function () {

                        $scope.months.push({ value: '0', text: $scope.text['SYSTEM'].D_M1 });
                        $scope.months.push({ value: '1', text: $scope.text['SYSTEM'].D_M2 });
                        $scope.months.push({ value: '2', text: $scope.text['SYSTEM'].D_M3 });
                        $scope.months.push({ value: '3', text: $scope.text['SYSTEM'].D_M4 });
                        $scope.months.push({ value: '4', text: $scope.text['SYSTEM'].D_M5 });
                        $scope.months.push({ value: '5', text: $scope.text['SYSTEM'].D_M6 });
                        $scope.months.push({ value: '6', text: $scope.text['SYSTEM'].D_M7 });
                        $scope.months.push({ value: '7', text: $scope.text['SYSTEM'].D_M8 });
                        $scope.months.push({ value: '8', text: $scope.text['SYSTEM'].D_M9 });
                        $scope.months.push({ value: '9', text: $scope.text['SYSTEM'].D_M10 });
                        $scope.months.push({ value: '10', text: $scope.text['SYSTEM'].D_M11 });
                        $scope.months.push({ value: '11', text: $scope.text['SYSTEM'].D_M12 });

                    };

                    var setShowText = function () {
                        if ($scope.date) {
                            var date = dtMoment($scope.date);
                            $scope.showText = $filter('date')(date, $scope.format);
                            $scope.data = {
                                day: date.getDate().toString(),
                                month: date.getMonth().toString(),
                                year: date.getFullYear().toString()
                            };
                        } else {
                            $scope.data = {
                                day: '', month: '0', year: ''
                            };
                        }
                    };

                    $scope.cancle = function () {
                        $scope.isShow = false;
                    };

                    $scope.ok = function () {

                        var isValid = isValidDate($scope.data.day, $scope.data.month, $scope.data.year);
                        if (isValid) {
                            $scope.date = new Date(parseInt($scope.data.year), parseInt($scope.data.month), parseInt($scope.data.day));

                            $scope.isShow = false;
                        } else {
                            $scope.errMessage = $scope.text['SYSTEM'].INVALID_DATE;
                        }
                    };

                    init();

                }],
                link: function (scope, element, attrs, controller) {

                }
            };
        });
})();