﻿(function () {
    "use strict";
    angular.module('DESS')
        // datepicker แบบเลือกวันที่เดียว
        //<ess-datepicker ng-model="model ที่ต้องการ binding" min="วันที่น้อยสุด" max="วันที่มากสุด" 
        //              width="ความกว้าง px ถ้าไม่ใส่ จะเป็น 100%" 
        //              ng-change="call back function จะใส่ argument อะไรเข้ามาก็ได้ "></ess-datepicker>

        .directive('essPagin', function () {
            return {
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    pagin: '=ngModel',
                    onChangePage: '&'
                },
                templateUrl: 'views/directives/pagination.html',
                compile: function (element, attrs) {

                },
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog'
                    , function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {

                        $scope.pages = [];
                        var showSize = 6;

                        var initPages = function () {
                            var start = 1;
                            var psg = [];

                            if ($scope.pagin.currentPage - showSize < 1) start = 1;
                            else {
                                psg.push({
                                    txt: '...', value: -1
                                });

                                if (($scope.pagin.totalPage - $scope.pagin.currentPage) < (showSize / 2)) {
                                    start = $scope.pagin.currentPage - (showSize / 2) - ($scope.pagin.totalPage - $scope.pagin.currentPage);
                                } else {
                                    start = $scope.pagin.currentPage - (showSize / 2);
                                }
                            }

                            for (var i = 0; i < showSize; i++) {
                                psg.push({
                                    txt: start, value: start
                                });

                                start++;
                                if (start > $scope.pagin.totalPage) break;
                            }

                            if (start <= $scope.pagin.totalPage) {
                                psg.push({
                                    txt: '...', value: -1
                                });
                            }

                            $scope.pages = psg;
                        };

                        $scope.getPages = function () {
                            return $scope.pages;
                        };


                        $scope.changePage = function (page) {
                            if (page === -1) return;
                            $scope.pagin.currentPage = page;
                            initPages();
                            if (typeof $scope.onChangePage === 'function') {
                                $scope.onChangePage({ pagin: $scope.pagin });
                            }
                        };

                        $scope.$watch('pagin', function (newValue, oldValue, scope) {
                            //Do anything with $scope.letters
                            initPages();
                        });

                        initPages();
                    }],
                link: function (scope, element, attrs, controller) {

                }
            };
        });
})();