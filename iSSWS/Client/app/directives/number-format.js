﻿angular.module('DESS')
    .filter('proNumber', function ($filter) {
        var standardNumberFilterFn = $filter('number');
        return function (a, c) {
            var isThereDot = false;
            var isPointZero = false;
            a = a + '';
            if (a.indexOf('.') !== -1) {
                if (a.split('.')[1] == '') isThereDot = true;
                else if (a.split('.')[1] == '0') isPointZero = true;
            }

            if (a.split('.').length > 1) {
                if (a.split('.')[1].length > 2) {
                    a = parseFloat(a.split('.')[0] + '.' + a.split('.')[1].substring(0, 2));
                }
            }

            //a = Math.round(parseFloat(a) * 100) / 100;

            if (isThereDot) return standardNumberFilterFn(a) + '.';
            else if (isPointZero) return standardNumberFilterFn(a) + '.0';
            else return standardNumberFilterFn(a);
        };
    })
    .filter('proNumber2len', function ($filter) {
        var standardNumberFilterFn = $filter('number');
        return function (a, c) {
            var isThereDot = false;
            var isPointZero = false;
            a = a + '';
            if (a.indexOf('.') !== -1) {
                if (a.split('.')[1] == '') isThereDot = true;
                else if (a.split('.')[1] == '0') isPointZero = true;
            }

            if (a.split('.')[0].length > 2) {
                a = a.split('.')[0].substring(0, 2) + '.' + a.split('.')[1];
            }

            if (a.split('.').length > 1) {
                if (a.split('.')[1].length > 2) {
                    a = parseFloat(a.split('.')[0] + '.' + a.split('.')[1].substring(0, 2));
                }
            }

            //a = Math.round(parseFloat(a) * 100) / 100;

            if (isThereDot) return standardNumberFilterFn(a) + '.';
            else if (isPointZero) return standardNumberFilterFn(a) + '.0';
            else return standardNumberFilterFn(a);
        };
    })
    .filter('proNumberFull', function ($filter) {
        var standardNumberFilterFn = $filter('number');
        return function (a, c) {
            var isThereDot = false;
            var isPointZero = false;
            var isMultiDot = false;
            a = a + '';
            if (a.indexOf('.') !== -1) {
                if (a.split('.')[1] == '') isThereDot = true;
                else if (a.split('.')[1] == '0') isPointZero = true;

                if (a.split('.').length > 2) isMultiDot = true;
            }

            if (isMultiDot === true) {
                a = a.slice(0, a.lastIndexOf('.')) + a.slice(a.lastIndexOf('.') + 1);
            }


            if (isThereDot) return parseFloat(a) + '.';
            else if (isPointZero) return a + '.0';
            else return a;
        };
    })
    .directive('format', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;


                ctrl.$formatters.unshift(function (a) {
                    return $filter(attrs.format)(ctrl.$modelValue)
                });


                ctrl.$parsers.unshift(function (viewValue) {
                    bindingInputNumberChecking();
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    if (attrs.format == 'number') attrs.format = 'proNumber';
                    elem.val($filter(attrs.format)(plainNumber));
                    return plainNumber;
                });

            }
        };
    }]);