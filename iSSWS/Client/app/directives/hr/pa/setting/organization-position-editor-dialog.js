﻿(function () {
    "use strict";
    angular.module('DESS')
        .directive('organizationPositionEditorDialog', function () {
            return {
                restrict: 'E',
                scope: {
                    organizationId: '=',
                    positionId: '=',

                    organizationText: '=',
                    positionText: '=',

                    organizationItems: '=',
                    positionItems: '=',

                    itemKey: '=',

                    text: '=',

                    organizationChang: '&',
                    positionChange: '&'
                },
                templateUrl: 'views/hr/pa/setting/partial/organization-position-editor-dialog.html',
                compile: function (element, attrs) {
                    if (angular.isUndefined(attrs.organizationId)) { attrs.organizationId = null; }
                    if (angular.isUndefined(attrs.positionId)) { attrs.positionId = null; }

                    if (angular.isUndefined(attrs.organizationText) || attrs.organizationText === null) { attrs.organizationText = ''; }
                    if (angular.isUndefined(attrs.positionText) || attrs.positionText === null) { attrs.positionText = ''; }

                    if (angular.isUndefined(attrs.itemKey) || attrs.itemKey === null) { attrs.itemKey = ''; }

                    if (angular.isUndefined(attrs.organizationItems) || attrs.organizationItems === null) { attrs.organizationItems = []; }
                },
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {
                    $scope.showDialog = false;
                    $scope.orgBoxClass = 'organization-ul-box';
                    $scope.posBoxClass = 'position-ul-box';

                    $scope.searchOrg = '';
                    $scope.searchPos = '';

                    $scope.selectOrg = null;
                    $scope.selectPos = null;

                    $scope.closeDialog = function () {
                        $scope.showDialog = false;
                    };
                    $scope.openDialog = function () {
                        $scope.selectOrg = angular.copy($scope.organizationId);
                        $scope.selectPos = angular.copy($scope.positionId);

                        $scope.showDialog = true;
                        $scope.autoScroll();
                    };

                    var init = function () {

                        $scope.selectOrg = angular.copy($scope.organizationId);
                        $scope.selectPos = angular.copy($scope.positionId);

                    };

                    $scope.selectOrganization = function (o) {

                        $scope.selectOrg = o.ObjectID;
                        $scope.selectPos = null;
                        $scope.searchPos = '';

                    };
                    $scope.selectPosition = function (p) {

                        $scope.selectPos = p.ObjectID;

                    };

                    $scope.filterPosition = function (p) {
                        return p.NextObjectID === $scope.selectOrg && (p.ObjectID + ' : ' + p.ObjectName).indexOf($scope.searchPos) > -1;
                    };

                    $scope.save = function () {
                        setMainValue();
                        if (typeof $scope.organizationChang === 'function') {
                            $scope.organizationChang({ id: $scope.organizationId, text: $scope.organizationText });
                        }

                        if (typeof $scope.positionChange === 'function') {
                            $scope.positionChange({ id: $scope.positionId, text: $scope.positionText });
                        }
                        $scope.showDialog = false;
                    };

                    var setMainValue = function () {
                        $scope.organizationId = angular.copy($scope.selectOrg);
                        $scope.positionId = angular.copy($scope.selectPos);

                        var orgs = $scope.organizationItems.filter(function (m) { return m.ObjectID === $scope.selectOrg; })
                        var poss = $scope.positionItems.filter(function (m) { return m.ObjectID === $scope.selectPos && m.NextObjectID === $scope.selectOrg; })

                        if (orgs && orgs.length > 0) {
                            $scope.organizationText = orgs[0].ObjectID + ': ' + orgs[0].AlternativeShortName;
                        } if (poss && poss.length > 0) {
                            $scope.positionText = poss[0].ObjectID + ': ' + poss[0].ObjectName;
                        }
                    };

                    var doScroll = function (div, active) {

                        if (active && active.position()) {
                            var dt = div.scrollTop();
                            var at = active.position().top;
                            var toPosTop = (dt - (active.height() * 2)) + at;

                            div.animate({
                                scrollTop: toPosTop
                            }, 0);
                        }

                    };

                    $scope.autoScroll = function () {

                        setTimeout(function (e) {
                            $('.' + $scope.orgBoxClass).each(function (index) {
                                var div = $(this);
                                var active = $(this).find('.active:first');
                              

                                doScroll(div, active);
                            });

                            $('.' + $scope.posBoxClass).each(function (index) {
                                var div = $(this);
                                var active = $(this).find('.active:first');


                                doScroll(div, active);
                            });
                        }, 100);

                      
                    };


                    init();

                    $scope.$watch('positionItems', function (newvalue, oldvalue) {

                        if (newvalue && newvalue.length > 0) {
                            setMainValue();
                        }
                        
                    });

                }],
                link: function (scope, element, attrs, controller) {

                }
            };
        });
})();