﻿(function () {
    "use strict";
    angular.module('DESS')
        // datepicker แบบเลือกวันที่เดียว
        //<ess-datepicker ng-model="model ที่ต้องการ binding" min="วันที่น้อยสุด" max="วันที่มากสุด" 
        //              width="ความกว้าง px ถ้าไม่ใส่ จะเป็น 100%" 
        //              ng-change="call back function จะใส่ argument อะไรเข้ามาก็ได้ "></ess-datepicker>

        .directive('essDatepicker', function () {
            return {
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    date: '=ngModel',
         
                    disabled: '=',
                    min: '=',
                    max: '=',
                    ngChange: '&',
                    ngClick: '&'
                },
                templateUrl: 'views/common/components/date-picker.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {

                    $scope.runtime = Date.now();
					$scope.block = null;
                    $scope.isInit = false;

                    if (angular.isUndefined($scope.disabled))
                        $scope.disabled = true;

                    if (angular.isUndefined($scope.min))
                        $scope.min = null;

                    if (angular.isUndefined($scope.max))
                        $scope.max = null;

                    var init = function () {
                       

                        if (angular.isUndefined($scope.disabled))
                            $scope.disabled = true;

                        if (angular.isUndefined($scope.min))
                            $scope.min = null;

                        if (angular.isUndefined($scope.max))
                            $scope.max = null;

                        $scope.$watch('date', function (newvalue, oldvalue) {
                            if (typeof $scope.ngChange === 'function') {
                                $scope.ngChange({ newvalue: newvalue });
                            }
                        });

                    };

                    $scope.onClickInput = function () {
                        if (typeof $scope.ngClick === 'function') {
                            $scope.ngClick();
                        }
                    };

                    init();

                }],
                link: function (scope, element, attrs, controller) {

                }
            };
        })
        .directive('essDatepickerRange', function () {
            return {
                restrict: 'AE',
                require: 'ngModel',
                scope: {
                    date: '=ngModel',
                    
                    disabled: '=',
                    min: '=',
                    max: '=',
                    ngChange: '&',
                    ngClick: '&'
                },
                templateUrl: 'views/common/components/date-picker-range.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {

                    $scope.runtime = Date.now();
                    $scope.block = null;
                    $scope.isInit = false;
            

                    if (angular.isUndefined($scope.disabled))
                        $scope.disabled = true;

                    if (angular.isUndefined($scope.min))
                        $scope.min = null;

                    if (angular.isUndefined($scope.max))
                        $scope.max = null;

                    var init = function () {


                        $scope.isInit = true;

                        if (angular.isUndefined($scope.disabled))
                            $scope.disabled = true;

                        if (angular.isUndefined($scope.min))
                            $scope.min = null;

                        if (angular.isUndefined($scope.max))
                            $scope.max = null;

                        $scope.$watch('date', function (newvalue, oldvalue) {
                            if (typeof $scope.ngChange === 'function' && $scope.isInit === false) {
                                $scope.ngChange({ newvalue: newvalue });
                            }
                            $scope.isInit = false;
                        });

                    };

                    $scope.onClickInput = function () {
                        if (typeof $scope.ngClick === 'function') {
                            $scope.ngClick();
                        }
                    };

                    var setInputData = function (val) {
                        $($scope.block.find('input')[0]).val(val);
                    };

                    init();

                }],
                link: function (scope, element, attrs, controller) {
                    scope.block = $($(element).find('.ess-date-picker')[0]);
                }
            };
        });
})();