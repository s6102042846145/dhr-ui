﻿/*
 * Plant model


 * 
 * */

(function () {
    "use strict";
    angular.module('DESS')
        .directive('consoleGraph', function () {
            return {
                restrict: 'AE',
                scope: {
                    text: '='
                },
                templateUrl: 'views/directives/console/console-graph.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$rootScope',
                    function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $rootScope) {

                        $scope.showGraph1 = true;
                        $scope.showGraph2 = true;

                        $scope.loading = false;

                        $scope.years = [];
                        $scope.year = '';

                        var init = function () {

                            getYears();

                        };

                        // #region  Control event

                        $scope.changeYear = function () {
                            GetGraphSummaryData($scope.year);
                        };

                        // #endregion

                        // #region  Call service

                        var getYears = function () {

                            $scope.loading = true;

                            var URL = CONFIG.SERVER + 'HRTR/GetAllYearForGetGraphSummary';
                            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };


                            $http.post(URL, oRequestParameter).then(function (response) {

                                var ys = response.data;//.sort().reverse();

                                $scope.years = [];

                                ys.forEach(function (y) {
                                    $scope.years.push(y.Year + '');
                                });

                                $scope.years = $scope.years.sort().reverse();

                                if ($scope.years && $scope.years.length > 0) {
                                    $scope.year = $scope.years[0] + '';
                                } else {
                                    $scope.years = [new Date().getFullYear() + ''];
                                    $scope.year = $scope.years[0] + '';
                                }

                                GetGraphSummaryData($scope.year);

                            }, function (error) {

                                $scope.loading = false;
                                console.error('error InitialConfig.', error);
                            });

                        };

                        var GetGraphSummaryData = function (year) {
                            $scope.loading = true;
                            var URL = CONFIG.SERVER + 'HRTR/GetGraphSummaryForDashboard';
                            var oRequestParameter = {
                                InputParameter: {
                                    year: year
                                }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER)
                            };


                            $http.post(URL, oRequestParameter).then(function (response) {

                                setChart1(response.data);
                                setChart2(response.data);

                                $scope.loading = false;

                            }, function (error) {

                                $scope.loading = false;
                                console.error('error InitialConfig.', error);
                            });

                        };

                        // #endregion

                        // #region set chart

                        var setChart1 = function (data) {
                            $('#pieChart1Box').html('<canvas id="pieChart1" ng-show="showGraph1"></canvas>');
                            var Graph1 = data.Table;

                            if (Graph1.length === 0) {
                                $scope.showGraph1 = false;
                            } else {
                                $scope.showGraph1 = true;
                            }

                            /* Piechart 1 : แสดงข้อมูลจาก Table 1 ที่ Return มาจาก Store */
                            var i;
                            var title1 = '';
                            var labels1 = [];
                            var backgroundColor1 = [];
                            var data1 = [];
                            for (i = 0; i < Graph1.length; i++) {
                                title1 = Graph1[i].Title;
                                labels1.push(Graph1[i].DataName);
                                backgroundColor1.push(Graph1[i].Color);
                                data1.push(Graph1[i].DataValue);
                            }
                            var pieChartData = {
                                labels: labels1,
                                datasets: [{
                                    label: title1,
                                    backgroundColor: backgroundColor1,
                                    data: data1
                                }]
                            };
                            var piehartOpiton = {
                                title: {
                                    display: true,
                                    text: title1
                                },
                                legend: {
                                    display: false
                                }, scaleShowValues: true,
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            autoSkip: false
                                        }
                                    }]
                                }
                            };
                            var ctx_pie = document.getElementById('pieChart1').getContext('2d');

                            var myPieChart = new Chart(ctx_pie, {
                                type: 'bar',//'pie',
                                data: pieChartData,
                                responsive: true,
                                options: piehartOpiton
                            });
                        };

                        var setChart2 = function (data) {
                            $('#pieChart2Box').html('<canvas id="pieChart2" ng-show="showGraph2"></canvas>');
                            var Graph2 = data.Table1;

                            if (Graph2.length === 0) {
                                $scope.showGraph2 = false;
                            } else {
                                $scope.showGraph2 = true;
                            }

                            /* Piechart 2 : แสดงข้อมูลจาก Table 2 ที่ Return มาจาก Store */
                            var i = 0;
                            var title2 = '';
                            var labels2 = [];
                            var backgroundColor2 = [];
                            var data2 = [];
                            for (i = 0; i < Graph2.length; i++) {
                                title2 = Graph2[i].Title;
                                labels2.push(Graph2[i].DataName);
                                backgroundColor2.push(Graph2[i].Color);
                                data2.push(Graph2[i].DataValue);
                            }
                            var pieChartData2 = {
                                labels: labels2,
                                datasets: [{
                                    label: title2,
                                    backgroundColor: backgroundColor2,
                                    data: data2
                                }]
                            };
                            var piehartOpiton2 = {
                                title: {
                                    display: true,
                                    text: title2
                                }
                            };
                            var ctx_pie2 = document.getElementById('pieChart2').getContext('2d');
                            var myPieChart2 = new Chart(ctx_pie2, {
                                type: 'doughnut',
                                data: pieChartData2,
                                options: piehartOpiton2
                            });
                        };

                        // #endregion


                        init();

                    }],
                link: function (scope, element, attrs, controller) {

                }
            };
        });
})();