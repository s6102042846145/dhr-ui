using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public delegate void ChangeDeleteItemEventHandler(object sender,ChangeDeleteItemEventArgs e);
public class ChangeDeleteItemEventArgs: EventArgs
{
    private string __itemKey;
    public ChangeDeleteItemEventArgs(string itemKey)
    {
        __itemKey = itemKey;
    }

    public string ItemKey
    {
        get
        {
            return __itemKey;
        }
        set
        {
            __itemKey = value;
        }
    }
}
