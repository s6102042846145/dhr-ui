using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ESS.DATA;
using ESS.WORKFLOW;
using ESS.PORTALENGINE;
using ESS.EMPLOYEE;
using iSSWS.Models;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.INTERFACE;
/// <summary>
/// Summary description for CacheManager
/// </summary>
/// 

    public class CacheManager
    {
        //AddBy: Ratchatawan W. (2012-02-22)
        private static string __defaultLanguage = ConfigurationManager.AppSettings["DEFAULT_LANGUAGE_CODE"];

        public CacheManager()
        {

        }
        public static Dictionary<string, INFOTYPE0001> LoadInfoType1()
        {
            Dictionary<string, INFOTYPE0001> info1List;
            string cKey = string.Format("INFO1DICT");
            if (HttpContext.Current.Cache[cKey] == null)
            {
                INFOTYPE0001 oInfo1 = new INFOTYPE0001();
                List<IInfoType> buffer = oInfo1.GetList("00000000", "99999999", "DB", "PRD");
                info1List = new Dictionary<string, INFOTYPE0001>();
                foreach (IInfoType item in buffer)
                {
                    oInfo1 = (INFOTYPE0001)item;
                    if (oInfo1.EndDate >= DateTime.Now.Date && oInfo1.BeginDate <= DateTime.Now.Date)
                    {
                        info1List.Add(oInfo1.EmployeeID, oInfo1);
                    }
                }
                HttpContext.Current.Cache.Insert(cKey, info1List, null, DateTime.Now.AddHours(-5).Date.AddDays(1).AddHours(5), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            else
            {
                info1List = (Dictionary<string, INFOTYPE0001>)HttpContext.Current.Cache[cKey];
            }
            return info1List;
        }

        public static EmployeeData GetEmpData(string EmployeeID)
        {
            string key = string.Format("EMP_{0}", EmployeeID);
            EmployeeData oEmp;
            if (HttpContext.Current.Cache.Get(key) == null)
            {
                oEmp = new EmployeeData(EmployeeID);
                HttpContext.Current.Cache.Add(key, oEmp, null, DateTime.Now.AddHours(-5).Date.AddDays(1).AddHours(5), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
            }
            else
            {
                oEmp = (EmployeeData)HttpContext.Current.Cache.Get(key);
            }
            return oEmp;
        }

        public static string GetCommonText(string Category, string LangaugeCode, string Code)
        {
            return GetCommonText(Category, LangaugeCode, Code, true);
        }
        public static string GetCommonText(string Category, string LangaugeCode, string Code, bool UseDefault)
        {
            string oReturn;
            string cKey = string.Format("COMMENTTEXT_{0}_{1}_{2}", Category.ToUpper(), LangaugeCode.ToUpper(), Code.ToUpper());
            if (HttpContext.Current.Cache[cKey] == null)
            {
                oReturn = new CommonText(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText(Category.ToUpper(), LangaugeCode.ToUpper(), Code.ToUpper(), UseDefault);
                if (UseDefault && oReturn == "")
                {
                    HttpContext.Current.Cache.Insert(cKey, oReturn, null, DateTime.MaxValue, new TimeSpan(0, 10, 0));
                }
            }
            else
            {
                oReturn = (string)HttpContext.Current.Cache[cKey];
            }
            return oReturn;
        }
    public static string GetCommonText(string Category, string LangaugeCode, string Code,string CompanyCode)
    {
        return GetCommonText(Category, LangaugeCode, Code, CompanyCode,true);
    }
    public static string GetCommonText(string Category, string LangaugeCode, string Code ,string CompanyCode,bool UseDefault)
        {
            string oReturn;
            string cKey = string.Format("COMMENTTEXT_{0}_{1}_{2}", Category.ToUpper(), LangaugeCode.ToUpper(), Code.ToUpper());
            if (HttpContext.Current.Cache[cKey] == null)
            {
                oReturn = new CommonText(CompanyCode).LoadText(Category.ToUpper(), LangaugeCode.ToUpper(), Code.ToUpper(), UseDefault);
                if (UseDefault && oReturn == "")
                {
                    HttpContext.Current.Cache.Insert(cKey, oReturn, null, DateTime.MaxValue, new TimeSpan(0, 10, 0));
                }
            }
            else
            {
                oReturn = (string)HttpContext.Current.Cache[cKey];
            }
            return oReturn;
        }
        public static string GetRequestText(string Category, string LangaugeCode, string Code)
        {
            string oReturn;
            string cKey = string.Format("REQUESTTEXT_{0}_{1}_{2}", Category.ToUpper(), LangaugeCode.ToUpper(), Code.ToUpper());
            if (HttpContext.Current.Cache[cKey] == null)
            {
                oReturn = RequestText.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText(Category.ToUpper(), LangaugeCode.ToUpper(), Code.ToUpper());
                HttpContext.Current.Cache.Insert(cKey, oReturn, null, DateTime.MaxValue, new TimeSpan(0, 10, 0));
            }
            else
            {
                oReturn = (string)HttpContext.Current.Cache[cKey];
            }
            return oReturn;
        }

        public static string GetMonthName(int MonthNumber, string LangaugeCode)
        {
            string oReturn;
            string cKey = string.Format("MONTHNAME_{0}_{1}", LangaugeCode.ToUpper(), MonthNumber.ToString("00").ToUpper());
            if (HttpContext.Current.Cache[cKey] == null)
            {
                oReturn = new CommonText(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText("MONTH", LangaugeCode, MonthNumber.ToString("00"));
                HttpContext.Current.Cache.Insert(cKey, oReturn, null, DateTime.MaxValue, new TimeSpan(1, 0, 0, 0));
            }
            else
            {
                oReturn = (string)HttpContext.Current.Cache[cKey];
            }
            return oReturn;
        }
        public static string GetRequestTypeName(string RequestTypeCode, string LangaugeCode)
        {
            string oReturn;
            string cKey = string.Format("REQUESTTYPENAME_{0}_{1}", LangaugeCode.ToUpper(), RequestTypeCode.ToUpper());
            if (HttpContext.Current.Cache[cKey] == null)
            {
                oReturn = RequestText.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText("REQUESTTYPE", LangaugeCode, RequestTypeCode);
                HttpContext.Current.Cache.Insert(cKey, oReturn, null, DateTime.MaxValue, new TimeSpan(2, 0, 0));
            }
            else
            {
                oReturn = (string)HttpContext.Current.Cache[cKey];
            }
            return oReturn;
        }
        public static string GetWagetypeText(string WageType, string LangaugeCode)
        {
            string oReturn;
            string cKey = string.Format("WAGETYPETEXT_{0}_{1}", LangaugeCode.ToUpper(), WageType.ToUpper());
            if (HttpContext.Current.Cache[cKey] == null)
            {
                oReturn = new CommonText(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText("WAGETYPE", LangaugeCode, WageType);
                HttpContext.Current.Cache.Insert(cKey, oReturn, null, DateTime.MaxValue, new TimeSpan(2, 0, 0));
            }
            else
            {
                oReturn = (string)HttpContext.Current.Cache[cKey];
            }
            return oReturn;
        }

        public static string FormatNumber(decimal input)
        {
            if (input == 0)
            {
                return "";
            }
            decimal mod = Math.Floor(input); ;
            if (mod == 0)
            {
                return "-";
            }
            return mod.ToString("#,##0");
        }

        public static string FormatDecimal(decimal input)
        {
            if (input == 0)
            {
                return "";
            }
            return ((input * 100) % 100).ToString("00");
        }

        public static string NumberToText(decimal money485, string LanguageCode)
        {
            string returnText = "";
            int intValue = (int)decimal.Truncate(money485); //���Ţ�ӹǹ���
            int decimalValue = (int)((money485 % 1) * 100);    //���Ţ��ѧ�ȹ���
            string tempIntValue = ModNumber(intValue, LanguageCode);
            string tempDecimalValue = "";
            tempDecimalValue = ModNumber(decimalValue, LanguageCode);
            if (tempDecimalValue != "")
            {
                returnText = string.Format("{0}{2} {1}{3}", tempIntValue, tempDecimalValue, GetCommonText("SYSTEM", LanguageCode, "BATH"), GetCommonText("SYSTEM", LanguageCode, "SATANG"));
            }
            else if (tempIntValue == "" && tempDecimalValue == "")
            {
                returnText = GetCommonText("SYSTEM", LanguageCode, "ZERO");
            }
            else
            {
                returnText = string.Format("{0}{1}", tempIntValue, GetCommonText("SYSTEM", LanguageCode, "BATHABS"));
            }
            return returnText;
        }

        public static string ModNumber(int value, string LanguageCode)
        {
            string temp1 = "";
            string temp2 = "";
            string temp3 = "";
            string temp4 = "";
            string temp5 = "";
            string temp6 = "";
            string temp7 = "";
            string temp8 = "";

            string textNumber = "";
            int valueLength = 0;
            valueLength = value.ToString().Length;
            int[] textInt = new int[valueLength];

            int tempSubValue = 0;
            int tempSubNumber = (int)value;

            for (int i = 0; i < valueLength; i++)
            {
                tempSubValue = (int)tempSubNumber % 10;
                tempSubNumber = (int)tempSubNumber / 10;
                textInt[i] = tempSubValue;
                if (i == 0) //˹���
                {
                    if (textInt[i] == 0)
                    {
                        temp1 = "";
                    }
                    else if (textInt[i] == 1 && valueLength > 1)
                    {
                        temp1 = GetCommonText("SYSTEM", LanguageCode, "UNIT1");
                    }
                    else
                    {
                        temp1 = Translate(textInt[i], LanguageCode);
                    }
                }
                if (i == 1) //�Ժ
                {
                    if (textInt[i] == 0)
                    {
                        temp2 = "";
                    }
                    else if (textInt[i] == 1)
                    {
                        //�Ժ
                        temp2 = GetCommonText("SYSTEM", LanguageCode, "UNIT2");
                    }
                    else if (textInt[i] == 2)
                    {
                        //����Ժ
                        temp2 = GetCommonText("SYSTEM", LanguageCode, "UNIT3");
                    }
                    else
                    {
                        temp2 = string.Format("{0}{1}", Translate(textInt[i], LanguageCode), GetCommonText("SYSTEM", LanguageCode, "UNIT2"));
                    }
                }
                if (i == 2) //����
                {
                    if (textInt[i] == 0)
                    {
                        temp3 = "";
                    }
                    else
                    {
                        temp3 = string.Format("{0}{1}", Translate(textInt[i], LanguageCode), GetCommonText("SYSTEM", LanguageCode, "UNIT4"));
                    }
                }
                if (i == 3) //�ѹ
                {
                    if (textInt[i] == 0)
                    {
                        temp4 = "";
                    }
                    else
                    {
                        temp4 = string.Format("{0}{1}", Translate(textInt[i], LanguageCode), GetCommonText("SYSTEM", LanguageCode, "UNIT5"));
                    }
                }
                if (i == 4) //����
                {
                    if (textInt[i] == 0)
                    {
                        temp5 = "";
                    }
                    else
                    {
                        temp5 = string.Format("{0}{1}", Translate(textInt[i], LanguageCode), GetCommonText("SYSTEM", LanguageCode, "UNIT6"));
                    }
                }
                if (i == 5) //�ʹ
                {
                    if (textInt[i] == 0)
                    {
                        temp6 = "";
                    }
                    else
                    {
                        temp6 = string.Format("{0}{1}", Translate(textInt[i], LanguageCode), GetCommonText("SYSTEM", LanguageCode, "UNIT7"));
                    }
                }
                if (i == 6) //��ҹ
                {
                    if (textInt[i] == 0)
                    {
                        temp7 = "";
                    }
                    else if (textInt[i] == 1 && valueLength > 7)
                    {
                        temp7 = GetCommonText("SYSTEM", LanguageCode, "UNIT8");
                    }
                    else
                    {
                        temp7 = string.Format("{0}{1}", Translate(textInt[i], LanguageCode), GetCommonText("SYSTEM", LanguageCode, "UNIT9"));
                    }
                }
                if (i == 7) //�Ժ��ҹ
                {
                    if (textInt[i] == 1)
                    {
                        //temp8 = string.Format("�Ժ", Translate(textInt[i],LanguageCode)); 
                        temp8 = string.Format(GetCommonText("SYSTEM", LanguageCode, "UNIT2"), Translate(textInt[i], LanguageCode));
                    }
                    else if (textInt[i] == 2)
                    {
                        temp8 = GetCommonText("SYSTEM", LanguageCode, "UNIT3");
                    }
                    else
                    {
                        temp8 = string.Format("{0}{1}", Translate(textInt[i], LanguageCode), GetCommonText("SYSTEM", LanguageCode, "UNIT9"));
                    }
                }
                //textNumber = ConvertToString(valueLength);
            }

            #region "Convert To String"

            string convertedString = "";
            if (valueLength == 1)   //˹���
            {
                convertedString = temp1;
            }
            else if (valueLength == 2)   //�Ժ
            {
                convertedString = string.Format(" {0}{1}", temp2, temp1);
            }
            else if (valueLength == 3)   //����
            {
                convertedString = string.Format(" {0}{1}{2}", temp3, temp2, temp1);
            }
            else if (valueLength == 4)   //�ѹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}", temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 5)   //����
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}", temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 6)   //�ʹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}", temp6, temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 7)   //��ҹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}{6}", temp7, temp6, temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 8)   //�Ժ��ҹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}{6}{7}", temp8, temp7, temp6, temp5, temp4, temp3, temp2, temp1);
            }
            textNumber = convertedString;
            #endregion

            return textNumber;
        }

        public static string ConvertToString(int valueLength)
        {
            string temp1 = "";
            string temp2 = "";
            string temp3 = "";
            string temp4 = "";
            string temp5 = "";
            string temp6 = "";
            string temp7 = "";
            string temp8 = "";

            string convertedString = "";
            if (valueLength == 1)   //˹���
            {
                convertedString = temp1;
            }
            else if (valueLength == 2)   //�Ժ
            {
                convertedString = string.Format(" {0}{1}", temp2, temp1);
            }
            else if (valueLength == 3)   //����
            {
                convertedString = string.Format(" {0}{1}{2}", temp3, temp2, temp1);
            }
            else if (valueLength == 4)   //�ѹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}", temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 5)   //����
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}", temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 6)   //�ʹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}", temp6, temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 7)   //��ҹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}{6}", temp7, temp6, temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 8)   //�Ժ��ҹ
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}{6}{7}", temp8, temp7, temp6, temp5, temp4, temp3, temp2, temp1);
            }
            return convertedString;
        }

        public static string Translate(int number, string LanguageCode)
        {
            string returnString = "";
            switch (number)
            {
                case 1:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER1");
                    break;
                case 2:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER2"); ;
                    break;
                case 3:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER3"); ;
                    break;
                case 4:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER4"); ;
                    break;
                case 5:
                    returnString = CacheManager.GetCommonText("SYSTEM", LanguageCode, "NUMBER5"); ;
                    break;
                case 6:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER6"); ;
                    break;
                case 7:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER7"); ;
                    break;
                case 8:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER8"); ;
                    break;
                case 9:
                    returnString = GetCommonText("SYSTEM", LanguageCode, "NUMBER9"); ;
                    break;
            }
            return returnString;
        }
        public static string GenerateEmployeeIDOrUserID(string oEmployeeID,string oCompanyCode)
        {
            string sEmployeeID = oEmployeeID;
            if (!string.IsNullOrEmpty(oEmployeeID) && !string.IsNullOrEmpty(oCompanyCode))
            {
                List<string> oEmployeeIDList = oEmployeeID.Split(',', ' ').Where(all => all.IndexOf("-") < 0 && !all.Trim().Equals(string.Empty)).ToList();
                string[] sEmpSplit;
                foreach (var currentEmp in oEmployeeID.Split(',', ' ').Where(all => all.IndexOf("-") >= 0 && !all.Trim().Equals(string.Empty)).ToList())
                {
                    sEmpSplit = currentEmp.Split('-');
                    for (int i = Convert.ToInt32(sEmpSplit[0]); i <= Convert.ToInt32(sEmpSplit[1]); i++)
                    {
                        oEmployeeIDList.Add(i.ToString(""));
                    }
                }
                int tmpInt = 0;
                List<string> oEmployeeIDResult = oEmployeeIDList.Where(allnum => int.TryParse(allnum.ToString(), out tmpInt) == true).Select(allReplace => allReplace).ToList();//.PadLeft(8, '0')).ToList();
                oEmployeeIDResult.AddRange(oEmployeeIDList.Where(allnotnumm => int.TryParse(allnotnumm, out tmpInt) == false).ToList());
                sEmployeeID = string.Join(",", oEmployeeIDResult);
            }
            return sEmployeeID;
        }
    }


