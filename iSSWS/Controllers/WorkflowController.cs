﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.ANNOUNCEMENT;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.DATA.FILE;
using ESS.DATA.INTERFACE;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.HR.PA;
using ESS.HR.PA.DATACLASS;
using ESS.FILE;
using ESS.PORTALENGINE;
using ESS.PORTALENGINE.DATACLASS;
using ESS.SECURITY;
using ESS.WORKFLOW;
using iSSWS.Models;
using ESS.UTILITY.EXTENSION;
using ESS.SHAREDATASERVICE;
using ESS.EMPLOYEE.DATACLASS;
using ESS.UTILITY.CONVERT;
using System.Threading;
using ESS.SHAREDATASERVICE.CONFIG;
//using ESS.ANNOUNCEMENT.DATACLASS;
using System.Reflection;
using DHR.HR.API;
using ESS.ANNOUNCEMENT.DATACLASS;
using Microsoft.Security.Application;
using Newtonsoft.Json;
using ESS.SHAREDATASERVICE.DATACLASS;
using System.Globalization;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WorkflowController : ApiController
    {
        #region Constructor

        public WorkflowController()
        {
        }

        #endregion Constructor

        private string DefaultLanguage
        {
            get
            {
                string _DefaultLanguage = "EN";
                if (ConfigurationManager.AppSettings["DefaultLanguage"] != null)
                    _DefaultLanguage = ConfigurationManager.AppSettings["DefaultLanguage"].ToString();
                return _DefaultLanguage;
            }
        }

        [HttpPost]
        public IEnumerable<MenuItem> GetMenuItem([FromBody] RequestParameter oRequestParameter)
        {
            //EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            EmployeeDataForMobile oCurrentEmployee = ESS.UTILITY.CONVERT.JSon.CloneJson<EmployeeDataForMobile>(oRequestParameter.CurrentEmployee);
            SetAuthenticate(oCurrentEmployee);
            WorkflowManagement oWorkflowMan = WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode);

            List<MenuItem> oMenuItemList = new List<MenuItem>();
            MenuItem menu;
            RequestBox boxf = RequestBox.CreateInstance(oCurrentEmployee.CompanyCode);
            List<RequestBox> boxes = boxf.GetAuthorizeRequestBoxes("MAINMENU", oCurrentEmployee.EmployeeID);
            menu = new MenuItem();
            menu.isHeader = true;
            menu.icon = "icon_home";//"fa-2x fal fa-bullhorn";
            menu.text = CacheManager.GetCommonText("SUBJECT", oCurrentEmployee.Language, "MAINMENU");
            menu.count = 0;
            menu.url = "";
            oMenuItemList.Add(menu);

            /* Application */
            //RequestApplication oRequestApplication = new RequestApplication() { EnableForMobile = true };
            bool isMobile = false;

            MenuItem MainMenu = new MenuItem();
            List<RequestApplication> RequestApplicationList = oWorkflowMan.GetAuthorizeApplications(isMobile);
            foreach (RequestApplication oRequestApplication in RequestApplicationList)
            {
                MainMenu = new MenuItem();
                MainMenu.isBox = oRequestApplication.ShowCounter;
                MainMenu.isHeader = true;
                MainMenu.MenuCode = oRequestApplication.Code;
                MainMenu.icon = oRequestApplication.IconClass;
                MainMenu.text = CacheManager.GetCommonText("APPGROUP", oCurrentEmployee.Language, oRequestApplication.Code);
                MainMenu.count = 0;
                MainMenu.url = "";
                MainMenu.groupID = oRequestApplication.ID;

                oMenuItemList.Add(MainMenu);

                foreach (ApplicationSubject subject in oWorkflowMan.GetAuthorizeApplicationSubject(oRequestApplication.ID, isMobile))
                {
                    PortalEngineManagement oPortalMan = PortalEngineManagement.CreateInstance(oCurrentEmployee.CompanyCode);
                    ContentSetting oContentSetting = oPortalMan.GetContentSetting(subject.ID);
                    if (oContentSetting == null)
                        continue;

                    menu = new MenuItem();
                    menu.isHeader = false;
                    menu.isBox = MainMenu.isBox;
                    menu.icon = subject.IconClass;
                    // menu.isBox = oContentSetting.IsShowCounter;
                    menu.isBox = !string.IsNullOrEmpty(oContentSetting.ContentParam) ? true : false;
                    menu.MenuCode = subject.Code;
                    menu.groupID = oRequestApplication.ID;
                    menu.ContentParam = oContentSetting.ContentParam;
                    if (oContentSetting.IsShowCounter)
                    {
                        menu.count = oPortalMan.CountContent(oContentSetting, oCurrentEmployee.EmployeeID);
                    }
                    else
                        menu.count = 0;

                    MainMenu.count += menu.count < 0 ? 0 : menu.count;

                    menu.text = CacheManager.GetCommonText("APPLICATION", oCurrentEmployee.Language, subject.Code);                   
                    menu.url = "frmViewContent/" + subject.ID.ToString();
                    oMenuItemList.Add(menu);
                }
            }


            for (var i = 0; i < oMenuItemList.Count; i++)
            {
                var tmpHeaderCount = 0;
                if (oMenuItemList[i].isBox == true && oMenuItemList[i].isHeader == true)
                {
                    tmpHeaderCount = oMenuItemList[i].count;
                    var tmpHeader = oMenuItemList.FirstOrDefault(
                    p => p.MenuCode == "ARCHIVE_BOX" && p.groupID == oMenuItemList[i].groupID);
                    if (tmpHeader != null && tmpHeader.count > 0)
                    {
                        oMenuItemList[i].count = tmpHeaderCount - tmpHeader.count;
                    }
                }
            }


            return oMenuItemList;
        }

        [HttpPost]
        public ContentSettingForMobile GetContentSetting([FromBody] RequestParameter oRequestParameter)
        {
            ContentSettingForMobile oContentSettingForMobile = new ContentSettingForMobile();
            if (oRequestParameter.CurrentEmployee != null && oRequestParameter.CurrentEmployee.EmployeeID != null)
            {
                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;

                //Condition may be difference each Module
                try
                {
                    if (oRequestParameter.InputParameter.ContainsKey("CONTENTID"))
                    {
                        //Parse Json string to Object
                        SetAuthenticate(oCurrentEmployee);
                        string oSubjectID = oRequestParameter.InputParameter["CONTENTID"].ToString();
                        oSubjectID = oSubjectID.IndexOf("#") > -1 ? oSubjectID.Substring(0, oSubjectID.IndexOf("#")) : oSubjectID;
                        int SubjectID = int.Parse(oSubjectID);
                        oContentSettingForMobile.SubjectID = SubjectID;
                        ContentSetting oSetting = PortalEngineManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetContentSetting(SubjectID);

                        ApplicationSubject oSubject = ApplicationSubject.CreateInstance(oCurrentEmployee.CompanyCode).LoadApplicationSubject(SubjectID);
                        List<UserRoleResponseSetting> oAuthorizeRoles = ApplicationSubject.CreateInstance(oCurrentEmployee.CompanyCode).LoadUserResponse(SubjectID);
                        oContentSettingForMobile.HaveActionInsteadOf = false;
                        foreach (UserRoleResponseSetting role in oAuthorizeRoles)
                        {
                            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole(role.UserRole))
                                oContentSettingForMobile.HaveActionInsteadOf = true;
                        }

                        oSubject.CheckAuthorize();
                        oContentSettingForMobile.Header = RequestText.CreateInstance(oCurrentEmployee.CompanyCode).LoadText("APPLICATION", WorkflowPrinciple.Current.UserSetting.Language, oSubject.Code);
                        if (oSetting.IsControl)
                        {
                            oContentSettingForMobile.ManualEmployeeList = new List<EmployeeInResponse>();
                            //// If child control is not override function GenerateEmployeeListinResponse, Used default employee list
                            // EmployeeInResponse oEmployeeInResponse;
                            // DataTable dtResponse = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetUserInResponseForActionOfInstead(WorkflowPrinciple.CurrentIdentity.EmployeeID, SubjectID, DateTime.Now);
                            // DataTable dtResultByCompany = new DataTable();
                            // foreach (DataRow row in dtResponse.Rows)
                            // {
                            //     foreach (DataRow row2 in EmployeeManagement.CreateInstance(row["ResponseCompanyCode"].ToString()).GetUserInResponseForActionOfInsteadByResponseType(row["ResponseType"].ToString(), row["ResponseCode"].ToString(), row["ResponseCompanyCode"].ToString(), (bool)row["IncludeSub"], DateTime.Now).Rows)
                            //     {
                            //         oEmployeeInResponse = new EmployeeInResponse();
                            //         oEmployeeInResponse.IsHeader = Convert.ToBoolean(row2["IsHeader"]);
                            //         oEmployeeInResponse.OrgName = row2["Org" + WorkflowPrinciple.Current.UserSetting.Language].ToString();
                            //         oEmployeeInResponse.EmployeeID = row2["EmployeeID"].ToString();
                            //         oEmployeeInResponse.PositionID = row2["PositionID"].ToString();
                            //         oEmployeeInResponse.PositionName = row2["Position" + WorkflowPrinciple.Current.UserSetting.Language].ToString();
                            //         oEmployeeInResponse.CompanyCode = row["ResponseCompanyCode"].ToString();
                            //         oEmployeeInResponse.EmployeeName = string.Format("{0} - {1}", row2["EmployeeID"].ToString(), row2["Name" + WorkflowPrinciple.Current.UserSetting.Language].ToString());
                            //         oContentSettingForMobile.ManualEmployeeList.Add(oEmployeeInResponse);
                            //     }
                            // }
                            oContentSettingForMobile.IsContent = true;
                            oContentSettingForMobile.ContentTemplate = (string.Format("{0}", oSetting.ContentViewerClass.Replace('.', '/').ToLower()));
                            if (oSetting.IsShowCounter)
                                oContentSettingForMobile.CounterTemplate = (string.Format("{0}", oSetting.CounterClass.Replace('.', '/').ToLower()));
                            oContentSettingForMobile.ContentParam = oSetting.ContentParam;
                            oContentSettingForMobile.Creator = oCurrentEmployee;
                            oContentSettingForMobile.Requestor = oCurrentEmployee;
                        }
                        else
                        {
                            oContentSettingForMobile.ContentTemplate = oSetting.ContentViewerClass;
                            oContentSettingForMobile.IsContent = false;
                        }
                    }
                    // Call function
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    //
                }
            }

            return oContentSettingForMobile;
        }

        [HttpPost]
        public List<EmployeeInResponse> ActionInsteadOf([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            int SubjectID = int.Parse(oRequestParameter.InputParameter["CONTENTID"].ToString());
            string SearchText = oRequestParameter.InputParameter["SEARCHTEXT"].ToString();

            List<EmployeeInResponse> oReturn = new List<EmployeeInResponse>();
            EmployeeInResponse oEmployeeInResponse;
            DataSet dtResponse = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetUserInResponseForActionOfInsteadWithIncludeSub(WorkflowPrinciple.CurrentIdentity.EmployeeID, SubjectID, DateTime.Now, "12", SearchText);
            //DataSet dtResponse = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetUserInResponseForActionOfInstead(WorkflowPrinciple.CurrentIdentity.EmployeeID, SubjectID, DateTime.Now, SearchText);
            DataTable dtResultByCompany = new DataTable();

            string oLang = oRequestParameter.CurrentEmployee.Language;
            foreach (DataRow row in dtResponse.Tables[0].Rows)
            {
                oEmployeeInResponse = new EmployeeInResponse();
                //oEmployeeInResponse.ParseToObject(row);
                oEmployeeInResponse.EmployeeID = row["EmployeeID"].ToString();
                oEmployeeInResponse.EmployeeName = oLang == "TH" ? row["NameTH"].ToString() : row["NameEN"].ToString();
                oEmployeeInResponse.OrgUnit = row["OrgUnit"].ToString();
                oEmployeeInResponse.OrgName = oLang == "TH" ? row["OrgTH"].ToString() : row["OrgEN"].ToString();
                oEmployeeInResponse.PositionID = row["Position"].ToString();
                oEmployeeInResponse.PositionName = oLang == "TH" ? row["PositionTH"].ToString() : row["PositionEN"].ToString();
                oEmployeeInResponse.CompanyCode = row["CompanyCode"].ToString();
                oEmployeeInResponse.IsHeader = (bool)row["IsHeader"];
                oReturn.Add(oEmployeeInResponse);
            }
            //ส่วนนี้จาก SP เดิม จะดึง 2 Tables ที่เป็น ของ บ.ตัวเอง และอีกอันของบ.อื่น ปรับให้ดึงSP ใหม่ return 1 table
            //if (dtResponse.Tables[1].Rows.Count > 0)
            //{
            //    foreach (DataRow row in dtResponse.Tables[1].Rows)
            //    {
            //        foreach (DataRow row2 in EmployeeManagement.CreateInstance(row["ResponseCompanyCode"].ToString()).GetUserInResponseForActionOfInsteadByResponseType(row["ResponseType"].ToString(), row["ResponseCode"].ToString(), row["ResponseCompanyCode"].ToString(), (bool)row["IncludeSub"], DateTime.Now, SearchText).Rows)
            //        {
            //            oEmployeeInResponse = new EmployeeInResponse();
            //            oEmployeeInResponse.IsHeader = Convert.ToBoolean(row2["IsHeader"]);
            //            oEmployeeInResponse.OrgName = row2["Org" + WorkflowPrinciple.Current.UserSetting.Language].ToString();
            //            oEmployeeInResponse.EmployeeID = row2["EmployeeID"].ToString();
            //            oEmployeeInResponse.PositionID = row2["PositionID"].ToString();
            //            oEmployeeInResponse.PositionName = row2["Position" + WorkflowPrinciple.Current.UserSetting.Language].ToString();
            //            oEmployeeInResponse.CompanyCode = row["ResponseCompanyCode"].ToString();
            //            //oEmployeeInResponse.EmployeeName = string.Format("{0} - {1}", row2["EmployeeID"].ToString(), row2["Name" + WorkflowPrinciple.Current.UserSetting.Language].ToString());
            //            oEmployeeInResponse.EmployeeName = string.Format("{0}", row2["Name" + WorkflowPrinciple.Current.UserSetting.Language].ToString());
            //            oReturn.Add(oEmployeeInResponse);
            //        }
            //    }
            //}

            return oReturn;
        }


        [HttpPost]
        public string ValidateTicket([FromBody] RequestParameter oRequestParameter)
        {
            string Return = string.Empty;
            SetAuthenticate(oRequestParameter.CurrentEmployee);
            try
            {
                DataTable dt = WorkflowManagement.CreateInstance(oRequestParameter.InputParameter["TicketCompanyCode"].ToString()).ValidateTicket(oRequestParameter.InputParameter["TicketID"].ToString(), oRequestParameter.CurrentEmployee.EmployeeID);
                foreach (DataRow dr in dt.Rows)
                    Return = string.Format("frmViewRequest/{0}/{1}/{2}/{3}/{4}", dr["RequestNo"].ToString(), dr["CompanyCode"].ToString(), dr["KeyMaster"].ToString(), false, false);
            }
            catch (Exception ex)
            {

            }
            return Return;
        }
        [HttpPost]
        public Dictionary<string, string> GetLoginBlockSetting()
        {
            Dictionary<string, string> oReturn = new Dictionary<string, string>();
            oReturn["IsEvent"] = ShareDataManagement.IsEvent;
            oReturn["AdminKey"] = ShareDataManagement.AdminKey;
            return oReturn;
        }



        //[HttpPost, HttpGet]
        [HttpPost]
        public Dictionary<string, object> Authenticate([FromBody] UserLogin oUserLogin)
        {
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            string oUserID = string.Empty;
            CommonText oCommonText = new CommonText(oUserLogin.CompanyCode);

            EmployeeData oEmployeeData = new EmployeeData();
            if (!string.IsNullOrEmpty(oUserLogin.Username))
            {
                oUserID = oUserLogin.Username;
                if (string.IsNullOrEmpty(oUserID))
                {
                    oResult["ErrorMessage"] = "INCOMPLETE_ID";
                }
                else
                {
                    try
                    {
                        //Get EmployeeData
                        oEmployeeData = new EmployeeData(oEmployeeData.GetEmployeeIDFromUserID(oUserID, oUserLogin.CompanyCode));
                        if (string.IsNullOrEmpty(oEmployeeData.EmployeeID))
                        {
                            //oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                            oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTEXIST");
                            return oResult;
                        }
                        //else
                        //{

                        EmployeeManagement oEmpMan = EmployeeManagement.CreateInstance(oUserLogin.CompanyCode);
                        EmployeeDataForMobile oInf1Mobile = new EmployeeDataForMobile();
                        INFOTYPE0001 oInf1Desktop = oEmpMan.GetINFOTYPE0001ByEmpID(oEmployeeData.EmployeeID);
                        if (oInf1Desktop != null)
                        {
                            oInf1Mobile = Convert<EmployeeDataForMobile>.ObjectFrom(oInf1Desktop);
                            oInf1Mobile.PositionID = oInf1Desktop.Position;
                            SetAuthenticate(oInf1Mobile);
                            oEmployeeData.CompanyCode = oInf1Mobile.CompanyCode;
                            oInf1Mobile.IsViewPost = true;
                            oInf1Mobile.IsViewEmployeeLevel = oEmpMan.VISIBLE_EMPLOYEE_LEVEL;
                            oInf1Mobile.Name = oEmployeeData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                            oInf1Mobile.Language = WorkflowPrinciple.Current.UserSetting.Language;
                            oInf1Mobile.ReceiveMail = WorkflowPrinciple.Current.UserSetting.ReceiveMail;
                            try
                            {
                                oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                            }
                            catch
                            {
                                throw new Exception("NO_ORG");
                            }
                            //oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                            oInf1Mobile.EmployeeID = oEmployeeData.EmployeeID;
                            oInf1Mobile.CompanyDetail = ShareDataManagement.GetCompanyByCompanyCode(oUserLogin.CompanyCode);
                            // Nattawat S. Add 20214-04-26
                            if (oEmployeeData.OrgAssignment.PositionData.ObjectID == "99999999")
                            {
                                throw new Exception("EXPIRED_POSITION");
                            }
                            else
                            {
                                try
                                {
                                    oInf1Mobile.PositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                                    oInf1Mobile.Position = oEmployeeData.OrgAssignment.PositionData.AlternativeName(oInf1Mobile.Language);
                                }
                                catch
                                {
                                    throw new Exception("EXCEPTION_USER_POSITION_NOT_FOUND");
                                }

                            }
                            oInf1Mobile.EmailAddress = oEmployeeData.EmailAddress;
                            oInf1Mobile.OfficeTelephoneNo = oEmployeeData.OfficeNumber;
                            oInf1Mobile.HomeTelephoneNo = oEmployeeData.HomeNumber;
                            oInf1Mobile.MobileNo = oEmployeeData.MobileNo;
                            oInf1Mobile.AllPosition = oEmployeeData.GetAllPositions(oInf1Mobile.Language);
                            //Modify by Morakot.t 2018-10-03
                            // string ImageFile = string.Format("{0}{1}.jpg", oEmpMan.PHOTO_PATH, oInf1Mobile.EmployeeID.Substring(2));
                            string ImageFile = string.Format("{0}{1}.jpg", oEmpMan.PHOTO_PATH, oInf1Mobile.EmployeeID);
                            oInf1Mobile.ImageUrl = ImageFile;
                            //AddBy: Ratchatawan.W (2017May17) Start: ใช้ในการดึงค่าวันจ้าง-ลาออกของพนักงานจาก SAP
                            DateSpecificData oDateSpecificData = oEmployeeData.DateSpecific;
                            oInf1Mobile.HiringDate = oDateSpecificData.HiringDate;
                            oInf1Mobile.RetirementDate = oDateSpecificData.RetirementDate;
                            oInf1Mobile.StartWorkingDate = oDateSpecificData.StartWorkingDate;
                            //End
                            oInf1Mobile.RequesterEmployeeID = oEmployeeData.EmployeeID;
                            oInf1Mobile.RequesterPositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                            oInf1Mobile.RequesterCompanyCode = oEmployeeData.CompanyCode; //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                            oInf1Mobile.UserRoles = oEmployeeData.GetUserRoles();

                            //WorkflowIdentity iden = WorkflowIdentity.CreateInstance(oUserLogin.CompanyCode).GetIdentity(ShareDataManagement.LookupCache(oUserLogin.CompanyCode, "ESS.AUTHORIZE.AD", "Path"), oUserLogin.Username, oUserLogin.Password, oInf1Mobile.PositionID);
                            WorkflowIdentity iden;
                            //Login by AD
                            if (oInf1Mobile.CompanyDetail.AuthenticationMode == "AD")
                                iden = WorkflowIdentity.CreateInstance(oUserLogin.CompanyCode).GetIdentity(string.Empty, oUserLogin.Username, oUserLogin.Password, oInf1Mobile.PositionID);
                            //Login by Local Password
                            else
                                iden = WorkflowIdentity.CreateInstance(oUserLogin.CompanyCode).GetIdentity(oUserLogin.Username, ESS.UTILITY.ENCRYPT.HashUtil.HashString(oUserLogin.Password), oInf1Mobile.PositionID);

                            if (iden.IsAuthorize)
                            {
                                // Request API Key
                                // admin for test
                                //oInf1Mobile.tokenRequests = APIAuthenManagement.CreateInstance(oUserLogin.CompanyCode).RequestApiToken("admin", oUserLogin.Password);
                                oResult["EmployeeData"] = oInf1Mobile;
                                if (!SecurityManagement.CreateInstance(oUserLogin.CompanyCode).ALLOWSYSTEMUSER)
                                {
                                    if (oEmployeeData.ActionOfPosition != null)
                                    {
                                        List<INFOTYPE1000> lstINFOTYPE1000 = oEmployeeData.GetAllPositions(ConfigurationManager.AppSettings["DEFAULT_LANGUAGE_CODE"]);
                                        if (lstINFOTYPE1000.Count > 0)
                                        {
                                            foreach (INFOTYPE1000 info in lstINFOTYPE1000)
                                            {
                                                if (string.IsNullOrEmpty(info.ShortText))
                                                {
                                                    oResult["ErrorMessage"] = string.Format(CacheManager.GetCommonText("SYSTEM", DefaultLanguage, "POSITION_IS_NO_ORG_BELONGTO"), oEmployeeData.EmployeeID, oEmployeeData.Name, info.ObjectID, info.Text);
                                                }
                                            }
                                        }
                                    }
                                }
                                //  string ticketName = SetFormAuthenticate(iden);
                                //if (iden != null && iden.IsAuthenticated)
                                //{
                                oResult["ErrorMessage"] = "";
                                //Get Announcement
                                //  oResult["Announcement"] = AnnouncementManagement.CreateInstance(oUserLogin.CompanyCode).GetAnnoncementList();
                                //Get Menu
                                oResult["Menu"] = GetMenuItem(new RequestParameter() { CurrentEmployee = oInf1Mobile });

                            }
                            else
                            {
                                oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                            }

                        }
                        else
                        {
                            oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                        }
                        //}
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null && ex.InnerException.Message == "NO_ORG")
                        {
                            oResult["ErrorMessage"] = ex.Message;
                        }
                        else if (ex is COMException || ex is DirectoryServicesCOMException)
                        {
                            oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                        }
                        else
                        {
                            oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, ex.Message);
                        }
                    }
                    finally
                    {
                        //if (oResult["EmployeeData"] == null)
                        //{
                        //    oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", "NOT_AUTHENTICATED");
                        //}
                    }
                }
            }
            else
            {
                oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                //CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                //oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
            }

            return oResult;
        }

        [HttpPost]
        public Dictionary<string, object> AuthenticateWithOutPassword([FromBody] UserLogin oUserLogin)
        {
            // dynamic obj = await Request.Content.ReadAsAsync<JObject>();
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            string oUserID = string.Empty;
            CommonText oCommonText = new CommonText(oUserLogin.CompanyCode);
            //string oPassword = oUserLogin.Password;
            //string oCompanyCode = oUserLogin.CompanyCode;

            EmployeeData oEmployeeData = new EmployeeData();
            if (!string.IsNullOrEmpty(oUserLogin.Username))
            {
                oUserID = CacheManager.GenerateEmployeeIDOrUserID(oUserLogin.Username, oUserLogin.CompanyCode);
                if (string.IsNullOrEmpty(oUserID))
                {
                    oResult["ErrorMessage"] = "INCOMPLETE_ID";
                }
                else
                {
                    try
                    {
                        //Get EmployeeData
                        oEmployeeData = new EmployeeData(oEmployeeData.GetEmployeeIDFromUserID(oUserID, oUserLogin.CompanyCode));
                        if (string.IsNullOrEmpty(oEmployeeData.EmployeeID))
                        {
                            oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                            return oResult;
                        }
                        //else
                        //{

                        EmployeeManagement oEmpMan = EmployeeManagement.CreateInstance(oUserLogin.CompanyCode);
                        EmployeeDataForMobile oInf1Mobile = new EmployeeDataForMobile();
                        INFOTYPE0001 oInf1Desktop = oEmpMan.GetINFOTYPE0001ByEmpID(oEmployeeData.EmployeeID);
                        if (oInf1Desktop != null)
                        {
                            oInf1Mobile = Convert<EmployeeDataForMobile>.ObjectFrom(oInf1Desktop);
                            oInf1Mobile.PositionID = oInf1Desktop.Position;
                            SetAuthenticate(oInf1Mobile);
                            oEmployeeData.CompanyCode = oInf1Mobile.CompanyCode;
                            oInf1Mobile.IsViewPost = true;
                            oInf1Mobile.IsViewEmployeeLevel = oEmpMan.VISIBLE_EMPLOYEE_LEVEL;
                            oInf1Mobile.Name = oEmployeeData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                            oInf1Mobile.Language = WorkflowPrinciple.Current.UserSetting.Language;
                            oInf1Mobile.ReceiveMail = WorkflowPrinciple.Current.UserSetting.ReceiveMail;
                            try
                            {
                                oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                            }
                            catch
                            {
                                throw new Exception("NO_ORG");
                            }
                            //oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                            oInf1Mobile.EmployeeID = oEmployeeData.EmployeeID;
                            oInf1Mobile.CompanyDetail = ShareDataManagement.GetCompanyByCompanyCode(oUserLogin.CompanyCode);
                            // Nattawat S. Add 20214-04-26
                            if (oEmployeeData.OrgAssignment.PositionData.ObjectID == "99999999")
                                throw new Exception("EXPIRED_POSITION");
                            else
                            {
                                try
                                {
                                    oInf1Mobile.PositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                                    oInf1Mobile.Position = oEmployeeData.OrgAssignment.PositionData.AlternativeName(oInf1Mobile.Language);
                                }
                                catch
                                {
                                    throw new Exception("EXCEPTION_USER_POSITION_NOT_FOUND");
                                }
                            }

                            oInf1Mobile.EmailAddress = oEmployeeData.EmailAddress;
                            oInf1Mobile.OfficeTelephoneNo = oEmployeeData.OfficeNumber;
                            oInf1Mobile.HomeTelephoneNo = oEmployeeData.HomeNumber;
                            oInf1Mobile.MobileNo = oEmployeeData.MobileNo;
                            oInf1Mobile.AllPosition = oEmployeeData.GetAllPositions(oInf1Mobile.Language);
                            //Modify By Morakot.t 2018-10-03
                            // string ImageFile = string.Format("{0}{1}.jpg", oEmpMan.PHOTO_PATH, oInf1Mobile.EmployeeID.Substring(2));
                            string ImageFile = string.Format("{0}{1}.jpg", oEmpMan.PHOTO_PATH, oInf1Mobile.EmployeeID);
                            oInf1Mobile.ImageUrl = ImageFile;
                            //AddBy: Ratchatawan.W (2017May17) Start: ใช้ในการดึงค่าวันจ้าง-ลาออกของพนักงานจาก SAP
                            DateSpecificData oDateSpecificData = oEmployeeData.DateSpecific;
                            oInf1Mobile.HiringDate = oDateSpecificData.HiringDate;
                            oInf1Mobile.RetirementDate = oDateSpecificData.RetirementDate;
                            oInf1Mobile.StartWorkingDate = oDateSpecificData.StartWorkingDate;
                            //End
                            oInf1Mobile.RequesterEmployeeID = oEmployeeData.EmployeeID;
                            oInf1Mobile.RequesterPositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                            oInf1Mobile.RequesterCompanyCode = oEmployeeData.CompanyCode; //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                            oInf1Mobile.UserRoles = oEmployeeData.GetUserRoles();

                            oResult["EmployeeData"] = oInf1Mobile;
                            if (!SecurityManagement.CreateInstance(oUserLogin.CompanyCode).ALLOWSYSTEMUSER)
                            {
                                if (oEmployeeData.ActionOfPosition != null)
                                {
                                    List<INFOTYPE1000> lstINFOTYPE1000 = oEmployeeData.GetAllPositions(ConfigurationManager.AppSettings["DEFAULT_LANGUAGE_CODE"]);
                                    if (lstINFOTYPE1000.Count > 0)
                                    {
                                        foreach (INFOTYPE1000 info in lstINFOTYPE1000)
                                        {
                                            if (string.IsNullOrEmpty(info.ShortText))
                                            {
                                                oResult["ErrorMessage"] = string.Format(CacheManager.GetCommonText("SYSTEM", DefaultLanguage, "POSITION_IS_NO_ORG_BELONGTO"), oEmployeeData.EmployeeID, oEmployeeData.Name, info.ObjectID, info.Text);
                                            }
                                        }
                                    }
                                }
                            }

                            //  string ticketName = SetFormAuthenticate(iden);
                            //if (iden != null && iden.IsAuthenticated)
                            //{
                            oResult["ErrorMessage"] = "";
                            //Get Announcement
                            oResult["Announcement"] = AnnouncementManagement.CreateInstance(oUserLogin.CompanyCode).GetAnnoncementList();
                            //Get Menu
                            oResult["Menu"] = GetMenuItem(new RequestParameter() { CurrentEmployee = oInf1Mobile });
                            //Get TextDescription
                            // oResult["SystemTextDescription"] = GetTextDescription("", oInf1Mobile.Language, "", true);
                            // }
                        }
                        else
                        {
                            oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                        }
                        //}
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null && ex.InnerException.Message == "NO_ORG")
                        {
                            oResult["ErrorMessage"] = ex.Message;
                        }
                        else if (ex is COMException || ex is DirectoryServicesCOMException)
                        {
                            oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                        }
                        else
                        {
                            oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, ex.Message);
                        }
                    }
                    finally
                    {
                        //if (oResult["EmployeeData"] == null)
                        //{
                        //    oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", "NOT_AUTHENTICATED");
                        //}
                    }
                }
            }
            else
            {
                oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");//CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
            }

            return oResult;
        }

        //[HttpGet]
        [HttpPost]
        public Dictionary<string, object> AuthenticateForExternalUser(string EncryptKey)
        {
            //string oUserID = ESS.UTILITY.ENCRYPT.AESUtil.Decrypt(EncryptKey);
            string[] paramsEncrypted = EncryptKey.Split('|');
            string oUserID = paramsEncrypted[0]; ;
            string CompanyCode = paramsEncrypted[1];
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            CommonText oCommonText = new CommonText(CompanyCode);

            EmployeeData oEmployeeData = new EmployeeData();
            if (string.IsNullOrEmpty(oUserID))
            {
                oResult["ErrorMessage"] = "INCOMPLETE_ID";
            }
            else
            {
                try
                {
                    //Get EmployeeData
                    DataTable dtUser = EmployeeManagement.CreateInstance(CompanyCode).GetExternalUserbyID(oUserID, "TH");
                    EmployeeManagement oEmpMan = EmployeeManagement.CreateInstance(CompanyCode);
                    oEmpMan.CompanyCode = CompanyCode;

                    if (dtUser.Rows.Count <= 0)
                    {
                        oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                        return oResult;
                    }

                    EmployeeDataForMobile oInf1Mobile = new EmployeeDataForMobile();
                    oInf1Mobile.CompanyCode = CompanyCode;
                    oInf1Mobile.PositionID = string.Empty;
                    oInf1Mobile.EmployeeID = dtUser.Rows[0]["UserID"].ToString();
                    oInf1Mobile.Name = dtUser.Rows[0]["Name"].ToString();
                    oInf1Mobile.EmailAddress = dtUser.Rows[0]["Email"].ToString();
                    oInf1Mobile.IsExternalUser = true;
                    oInf1Mobile.IsViewPost = true;
                    oInf1Mobile.IsViewEmployeeLevel = oEmpMan.VISIBLE_EMPLOYEE_LEVEL;
                    oInf1Mobile.IsExternalUser = true;
                    oInf1Mobile.Language = "TH";
                    oInf1Mobile.ReceiveMail = true;
                    SetAuthenticate(oInf1Mobile);
                    oInf1Mobile.OrgUnitName = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXTERNAL_USER"); ;
                    oInf1Mobile.HiringDate = DateTime.MinValue;
                    oInf1Mobile.RetirementDate = DateTime.MaxValue;
                    oInf1Mobile.StartWorkingDate = DateTime.MinValue;
                    //End
                    oInf1Mobile.CompanyDetail = new ESS.SHAREDATASERVICE.DATACLASS.Company();
                    oInf1Mobile.UserRoles = oEmployeeData.GetUserRoles();

                    oResult["EmployeeData"] = oInf1Mobile;
                    oResult["ErrorMessage"] = "";
                    oResult["Menu"] = GetMenuItem(new RequestParameter() { CurrentEmployee = oInf1Mobile });
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.Message == "NO_ORG")
                    {
                        oResult["ErrorMessage"] = ex.Message;
                    }
                    else if (ex is COMException || ex is DirectoryServicesCOMException)
                    {
                        oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
                    }
                    else
                    {
                        oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, ex.Message);
                    }
                }
                finally
                {

                }
            }

            return oResult;
        }

        //[HttpPost, HttpGet]
        [HttpPost]
        public Dictionary<string, object> AuthenticateWindows([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            UserLogin oUserLogin = new UserLogin();
            string oUserWindowsAuthen = HttpContext.Current.User.Identity.Name;
            oResult["UserAuthenAD1"] = HttpContext.Current.User.Identity.Name;
            oResult["UserAuthenAD2"] = HttpContext.Current.Request.ServerVariables["LOGON_USER"];
            string CompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            CommonText oCommonText = new CommonText(CompanyCode);
            string oUserID = string.Empty;
            if (!string.IsNullOrEmpty(oUserWindowsAuthen))
            {
                string strDomain = oUserWindowsAuthen.Split('\\')[0].ToString();
                oUserLogin.Username = oUserWindowsAuthen.Split('\\')[1].ToString();
                oUserLogin.CompanyCode = CompanyCode;
                EmployeeData oEmployeeData = new EmployeeData();
                if (!string.IsNullOrEmpty(oUserLogin.Username) && WorkflowIdentity.CreateInstance(oUserLogin.CompanyCode).ValidateDomain(strDomain))
                {
                    oUserID = CacheManager.GenerateEmployeeIDOrUserID(oUserLogin.Username, oUserLogin.CompanyCode);
                    if (string.IsNullOrEmpty(oUserID))
                    {
                        oResult["ErrorMessage"] = "INCOMPLETE_ID";
                    }
                    else
                    {
                        try
                        {
                            //Get EmployeeData
                            oEmployeeData = new EmployeeData(oEmployeeData.GetEmployeeIDFromUserID(oUserID, oUserLogin.CompanyCode));
                            if (string.IsNullOrEmpty(oEmployeeData.EmployeeID))
                            {
                                oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_MAPPINGNOTFOUND");
                                return oResult;
                            }
                            //else
                            //{

                            EmployeeManagement oEmpMan = EmployeeManagement.CreateInstance(oUserLogin.CompanyCode);
                            EmployeeDataForMobile oInf1Mobile = new EmployeeDataForMobile();
                            INFOTYPE0001 oInf1Desktop = oEmpMan.GetINFOTYPE0001ByEmpID(oEmployeeData.EmployeeID);
                            if (oInf1Desktop != null)
                            {
                                oInf1Mobile = Convert<EmployeeDataForMobile>.ObjectFrom(oInf1Desktop);
                                oInf1Mobile.PositionID = oInf1Desktop.Position;
                                SetAuthenticate(oInf1Mobile);
                                oEmployeeData.CompanyCode = oInf1Mobile.CompanyCode;
                                oInf1Mobile.IsViewPost = true;
                                oInf1Mobile.IsViewEmployeeLevel = oEmpMan.VISIBLE_EMPLOYEE_LEVEL;
                                oInf1Mobile.Name = oEmployeeData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                                oInf1Mobile.Language = WorkflowPrinciple.Current.UserSetting.Language;
                                oInf1Mobile.ReceiveMail = WorkflowPrinciple.Current.UserSetting.ReceiveMail;
                                try
                                {
                                    oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                                }
                                catch
                                {
                                    throw new Exception("NO_ORG");
                                }
                                //oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                                oInf1Mobile.EmployeeID = oEmployeeData.EmployeeID;
                                oInf1Mobile.CompanyDetail = ShareDataManagement.GetCompanyByCompanyCode(oUserLogin.CompanyCode);
                                try
                                {
                                    oInf1Mobile.PositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;

                                    oInf1Mobile.Position = oEmployeeData.OrgAssignment.PositionData.AlternativeName(oInf1Mobile.Language);
                                }
                                catch
                                {
                                    throw new Exception("EXCEPTION_USER_POSITION_NOT_FOUND");
                                }
                                oInf1Mobile.EmailAddress = oEmployeeData.EmailAddress;
                                oInf1Mobile.OfficeTelephoneNo = oEmployeeData.OfficeNumber;
                                oInf1Mobile.HomeTelephoneNo = oEmployeeData.HomeNumber;
                                oInf1Mobile.MobileNo = oEmployeeData.MobileNo;
                                oInf1Mobile.AllPosition = oEmployeeData.GetAllPositions(oInf1Mobile.Language);
                                //Modify By Morakot.t 2018-10-03
                                //   string ImageFile = string.Format("{0}{1}.jpg", oEmpMan.PHOTO_PATH, oInf1Mobile.EmployeeID.Substring(2));
                                string ImageFile = string.Format("{0}{1}.jpg", oEmpMan.PHOTO_PATH, oInf1Mobile.EmployeeID);
                                oInf1Mobile.ImageUrl = ImageFile;
                                //AddBy: Ratchatawan.W (2017May17) Start: ใช้ในการดึงค่าวันจ้าง-ลาออกของพนักงานจาก SAP
                                DateSpecificData oDateSpecificData = oEmployeeData.DateSpecific;
                                oInf1Mobile.HiringDate = oDateSpecificData.HiringDate;
                                oInf1Mobile.RetirementDate = oDateSpecificData.RetirementDate;
                                oInf1Mobile.StartWorkingDate = oDateSpecificData.StartWorkingDate;
                                //End
                                oInf1Mobile.RequesterEmployeeID = oEmployeeData.EmployeeID;
                                oInf1Mobile.RequesterPositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                                oInf1Mobile.CompanyCode = oEmployeeData.CompanyCode;//AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                                oInf1Mobile.UserRoles = oEmployeeData.GetUserRoles();

                                oResult["EmployeeData"] = oInf1Mobile;
                                if (!SecurityManagement.CreateInstance(oUserLogin.CompanyCode).ALLOWSYSTEMUSER)
                                {
                                    if (oEmployeeData.ActionOfPosition != null)
                                    {
                                        List<INFOTYPE1000> lstINFOTYPE1000 = oEmployeeData.GetAllPositions(ConfigurationManager.AppSettings["DEFAULT_LANGUAGE_CODE"]);
                                        if (lstINFOTYPE1000.Count > 0)
                                        {
                                            foreach (INFOTYPE1000 info in lstINFOTYPE1000)
                                            {
                                                if (string.IsNullOrEmpty(info.ShortText))
                                                {
                                                    oResult["ErrorMessage"] = string.Format(CacheManager.GetCommonText("SYSTEM", DefaultLanguage, "POSITION_IS_NO_ORG_BELONGTO"), oEmployeeData.EmployeeID, oEmployeeData.Name, info.ObjectID, info.Text);
                                                }
                                            }
                                        }
                                    }
                                }

                                //  string ticketName = SetFormAuthenticate(iden);
                                //if (iden != null && iden.IsAuthenticated)
                                //{
                                oResult["ErrorMessage"] = "";
                                //Get Announcement
                                oResult["Announcement"] = AnnouncementManagement.CreateInstance(oUserLogin.CompanyCode).GetAnnoncementList();
                                //Get Menu
                                oResult["Menu"] = GetMenuItem(new RequestParameter() { CurrentEmployee = oInf1Mobile });
                                //Get TextDescription
                                // oResult["SystemTextDescription"] = GetTextDescription("", oInf1Mobile.Language, "", true);
                                // }
                            }
                            else
                            {
                                oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_INF1_NOTFOUND");
                            }
                            //}
                        }
                        catch (Exception ex)
                        {
                            if (ex.InnerException != null && ex.InnerException.Message == "NO_ORG")
                            {
                                oResult["ErrorMessage"] = ex.Message;
                            }
                            else if (ex is COMException || ex is DirectoryServicesCOMException)
                            {
                                oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_SSO_ERROR");
                            }
                            else
                            {
                                oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", DefaultLanguage, ex.Message);
                            }
                        }
                        finally
                        {
                            //if (oResult["EmployeeData"] == null)
                            //{
                            //    oResult["ErrorMessage"] = CacheManager.GetCommonText("LOGON", "NOT_AUTHENTICATED");
                            //}
                        }
                    }
                }
                else
                {
                    oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_DOMAIN_NOTMATCH");
                }
            }
            else
            {
                oResult["ErrorMessage"] = oCommonText.LoadText("LOGON", DefaultLanguage, "EXCEPTION_SSO_NOTENABLE");//CacheManager.GetCommonText("LOGON", DefaultLanguage, "EXCEPTION_USER_NOTMATCH");
            }

            return oResult;
        }

        [HttpPost]
        public DataTable ValidateData([FromBody] RequestParameter oRequestParameter)
        {
            if (oRequestParameter == null)
            {
                DataTable dt = new DataTable();
                return dt;
            }
            SetAuthenticate(oRequestParameter.Requestor);
            EmployeeData oEmployee = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            string EmpoloyeeID = oEmployee.EmployeeID;
            return oEmployee.ValidateData();
        }

        /// <summary>
        /// GetTextDescription: used for get text description from iService3
        /// </summary>
        /// <param name="Param1">oCategoryCode</param>
        /// <param name="Param2">oLanguageCode</param>
        /// <param name="Param3">oTextCode</param>
        /// <param name="Param4">oUseDefault</param>
        /// <returns></returns>
        [HttpPost]
        public Dictionary<string, object> GetTextDescription([FromBody] RequestParameter oRequestParameter)
        {
            string oCategory = string.Empty;
            if (oRequestParameter.InputParameter.ContainsKey("Category"))
            {
                oCategory = oRequestParameter.InputParameter["Category"].ToString();
            }
            string oTextCode = string.Empty;
            if (oRequestParameter.InputParameter.ContainsKey("TextCode"))
            {
                oTextCode = oRequestParameter.InputParameter["TextCode"].ToString();
            }
            string oStatus = string.Empty;
            if (oRequestParameter.InputParameter.ContainsKey("STATUS"))
            {
                oStatus = oRequestParameter.InputParameter["STATUS"].ToString();
            }
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            if (oCurrentEmployee != null)
            {
                SetAuthenticate(oCurrentEmployee);
            }
            try
            {
                if (oCurrentEmployee != null)
                {
                    if (oStatus != string.Empty)
                    {
                        oResult["TextDescription"] = ESS.PORTALENGINE.PortalEngineManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetTextDescription(oRequestParameter.InputParameter["STATUS"].ToString(), oCurrentEmployee.Language);
                    }
                    else
                    {
                        oResult["TextDescription"] = CacheManager.GetCommonText(oCategory, oCurrentEmployee.Language, oTextCode, false);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return oResult;
        }

        [HttpPost]
        public Dictionary<string, object> GetTextDescriptionBySystem([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            try
            {
                string oSystemName = string.Empty;
                if (oRequestParameter.InputParameter.ContainsKey("SYSTEM"))
                {
                    oSystemName = oRequestParameter.InputParameter["SYSTEM"].ToString();
                }
                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
                EmployeeDataForMobile oRequestor = oRequestParameter.Requestor;

                if (oRequestor != null)
                {
                    SetAuthenticate(oRequestor);
                }
                else
                {
                    SetAuthenticate(oCurrentEmployee);
                }

                DataSet ds = ShareDataManagement.GetTextDescriptionByCompany(WorkflowPrinciple.CurrentIdentity.CompanyCode, WorkflowPrinciple.Current.UserSetting.Language);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    oResult.Add(dr["CategoryCode"].ToString(), ds.Tables[1].Select("CategoryCode = '" + dr["CategoryCode"].ToString() + "'").ToDictionary<DataRow, string, string>(row => row.Field<string>("TextCode"), row => row.Field<string>("TextDescription")));
                }

            }
            catch (Exception ex)
            {
            }
            return oResult;
            //Dictionary<string, object> oResult = new Dictionary<string, object>();
            //try
            //{
            //    string oSystemName = string.Empty;
            //    if (oRequestParameter.InputParameter.ContainsKey("SYSTEM"))
            //    {
            //        oSystemName = oRequestParameter.InputParameter["SYSTEM"].ToString();
            //    }
            //    EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            //    if (oCurrentEmployee != null)
            //    {
            //        SetAuthenticate(oCurrentEmployee);
            //        string[] CategoryCode = new string[0];
            //        if (oSystemName == "TE&E")
            //        {
            //            CategoryCode = new string[]
            //            {
            //                "ACCOUNT_SETTING",
            //                "ACCUMMULATEOFFSETUNIT",
            //                "APPLICATION",
            //                "AREA",
            //                "BOXCONTENT",
            //                "CoverSheet",
            //                "CURRENCY",
            //                "DELEGATION",
            //                "EMPGROUP",
            //                "EMPSUBGROUP",
            //                "EXPENSE",
            //                "EXPENSETYPE",
            //                "HRPAPERSONALDATA",
            //                "HRTMABSENCE",
            //                "HRTMABSENCEQUOTA",
            //                "LANGUAGE",
            //                "OBJECTSETTING",
            //                "ORGANIZATION",
            //                "PERSONALADMINISTRATION",
            //                "PettyCashStatusReport",
            //                "POSITION",
            //                "RELATION",
            //                "REPORT_FILTER",
            //                "REQUESTHEADER",
            //                "SETTING",
            //                "STATUS",
            //                "SUBAREA",
            //                "SYSTEM",
            //                "TIMEMANAGEMENT",
            //                "TMREPORT_TIMESHEET",
            //                "TRAVELEXPENSE",
            //                "TRAVELREQUEST",
            //                "MONITORING",
            //                "TRANSPORTATION_TYPE",
            //                "PERSONALVEHICLE",
            //                "GENERALEXPENSE"
            //            };
            //        }

            //        for (int i = 0; i < CategoryCode.Length; i++)
            //        {
            //            Dictionary<string, string> oDict = ESS.PORTALENGINE.PortalEngineManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetTextDescription(CategoryCode[i], oCurrentEmployee.Language);
            //            try
            //            {
            //                oResult.Add(CategoryCode[i], oDict);
            //            }
            //            catch
            //            {
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            //return oResult;
        }


        [HttpPost]
        public Dictionary<string, object> InitialSystemByModule([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            try
            {
                string moduleName = string.Empty;
                if (oRequestParameter.InputParameter.ContainsKey("Module"))
                {
                    moduleName = oRequestParameter.InputParameter["Module"].ToString();
                }
                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;

                //
                // Implement Something
                //
            }
            catch (Exception ex)
            {
            }
            return oResult;
        }


        public void SetAuthenticate(EmployeeDataForMobile oCurrentEmployee)
        {
            Escape(ref oCurrentEmployee);
            WorkflowIdentity iden;
            if (oCurrentEmployee.IsExternalUser)
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityForExternalUser(oCurrentEmployee.EmployeeID, oCurrentEmployee.Name);
            else
            {
                EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oCurrentEmployee);
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            }
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
            //Nattawat S. set current position ID
            WorkflowPrinciple.Current.UserSetting.Employee = new EmployeeData(oCurrentEmployee.EmployeeID, oCurrentEmployee.PositionID, oCurrentEmployee.CompanyCode);
        }

        private void Escape(ref EmployeeDataForMobile oEmployeeDataForMobile)
        {
            oEmployeeDataForMobile.EmployeeID = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.EmployeeID);
            oEmployeeDataForMobile.PositionID = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.PositionID);
            oEmployeeDataForMobile.Position = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.PositionID);
            oEmployeeDataForMobile.RequesterEmployeeID = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.RequesterEmployeeID);
            oEmployeeDataForMobile.RequesterPositionID = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.RequesterPositionID);
            oEmployeeDataForMobile.RequesterCompanyCode = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.RequesterCompanyCode);
            oEmployeeDataForMobile.AdminGroup = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.AdminGroup);
            oEmployeeDataForMobile.SubArea = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.SubArea);
            oEmployeeDataForMobile.tokenRequests = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.tokenRequests);
            oEmployeeDataForMobile.Area = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.Area);
            oEmployeeDataForMobile.CostCenter = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CostCenter);
            oEmployeeDataForMobile.OrgUnit = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.OrgUnit);
            oEmployeeDataForMobile.OrgUnitName = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.OrgUnitName);
            oEmployeeDataForMobile.EmailAddress = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.EmailAddress);
            oEmployeeDataForMobile.EmpGroup = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.EmpGroup);
            oEmployeeDataForMobile.EmpSubGroup = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.EmpSubGroup);
            oEmployeeDataForMobile.HomeTelephoneNo = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.HomeTelephoneNo);
            oEmployeeDataForMobile.ImageUrl = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.HomeTelephoneNo);
            oEmployeeDataForMobile.CompanyCode = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyCode);
            oEmployeeDataForMobile.Language = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.Language);
            oEmployeeDataForMobile.MobileNo = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.MobileNo);
            oEmployeeDataForMobile.Name = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.Name);
            oEmployeeDataForMobile.OfficeTelephoneNo = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.OfficeTelephoneNo);
            if (oEmployeeDataForMobile.CompanyDetail != null)
            {
                oEmployeeDataForMobile.CompanyDetail.Background = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.Background);
                oEmployeeDataForMobile.CompanyDetail.CompanyCode = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.CompanyCode);
                oEmployeeDataForMobile.CompanyDetail.DomainName = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.DomainName);
                oEmployeeDataForMobile.CompanyDetail.FullNameEN = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.FullNameEN);
                oEmployeeDataForMobile.CompanyDetail.FullNameTH = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.FullNameTH);
                oEmployeeDataForMobile.CompanyDetail.Logo = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.Logo);
                oEmployeeDataForMobile.CompanyDetail.Name = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.Name);
                oEmployeeDataForMobile.CompanyDetail.Theme = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.CompanyDetail.Theme);
            }
            if (oEmployeeDataForMobile.AllPosition != null)
            {
                foreach (INFOTYPE1000 info10 in oEmployeeDataForMobile.AllPosition)
                {
                    info10.ObjectID = Microsoft.Security.Application.Encoder.HtmlEncode(info10.ObjectID);
                    info10.ShortText = Microsoft.Security.Application.Encoder.HtmlEncode(info10.ShortText);
                    info10.ShortTextEn = Microsoft.Security.Application.Encoder.HtmlEncode(info10.ShortTextEn);
                    info10.SubBand = Microsoft.Security.Application.Encoder.HtmlEncode(info10.SubBand);
                    info10.Text = Microsoft.Security.Application.Encoder.HtmlEncode(info10.Text);
                    info10.TextEn = Microsoft.Security.Application.Encoder.HtmlEncode(info10.TextEn);
                }
            }
            if (oEmployeeDataForMobile.UserRoles != null)
            {
                for (int i = 0; i < oEmployeeDataForMobile.UserRoles.Count; i++)
                    oEmployeeDataForMobile.UserRoles[i] = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.UserRoles[i]);
            }

            oEmployeeDataForMobile.AreaSetting.City = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.AreaSetting.City);
            oEmployeeDataForMobile.AreaSetting.CountryCode = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.AreaSetting.CountryCode);
            oEmployeeDataForMobile.AreaSetting.CurrencyCode = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.AreaSetting.CurrencyCode);
            oEmployeeDataForMobile.AreaSetting.Description = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.AreaSetting.Description);
            oEmployeeDataForMobile.AreaSetting.PersonalAreaCode = Microsoft.Security.Application.Encoder.HtmlEncode(oEmployeeDataForMobile.AreaSetting.PersonalAreaCode);

        }


        /// <summary>
        /// GetRequestDocumentListInBox: used for get RequestDocument List in each box but return only used information for mobile version
        /// </summary>
        /// <param name="Param1">BoxID</param>
        /// <param name="CurrentEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        public DataSet GetRequestDocumentListInBox([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oCurrentEmployee);
            int oBoxID = int.Parse(oRequestParameter.InputParameter["BoxID"].ToString());

            DataSet ds = new DataSet();
            DataTable dtRequest = new DataTable("REQUESTDOCUMENT");
            DataTable dtAction = new DataTable("ACTION");
            DataTable dtFlagMaster = new DataTable("FLAGMASTER");
            DataSet ds1 = WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetRequestDocumentListForMobile(oBoxID, oEmployeeData, string.Empty);
            if (ds1.Tables[0].Rows.Count > 0)
                dtRequest = ds1.Tables[0].Copy();
            if (ds1.Tables.Count > 1)
            {
                dtAction = ds1.Tables[1].Copy();
            }
            if (ds1.Tables[2].Rows.Count > 0)
                dtFlagMaster = ds1.Tables[2].Copy();

            foreach (DataRow dr in WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetUserRoleResponseForOtherCompany(oCurrentEmployee.EmployeeID).Rows)
            {
                ds1 = WorkflowManagement.CreateInstance(dr["ResponseCompanyCode"].ToString()).GetRequestDocumentListForMobile(oBoxID, oEmployeeData, string.Empty);

                if (ds1.Tables[0].Rows.Count > 0)
                {
                    if (dtRequest.Rows.Count <= 0)
                        dtRequest = ds1.Tables[0].Copy();
                    else
                        ds1.Tables[0].AsEnumerable().ToList().ForEach(row => dtRequest.ImportRow(row));
                }
                if (ds1.Tables.Count > 1)
                {
                    if (dtAction.Rows.Count <= 0)
                        dtAction = ds1.Tables[1].Copy();
                    else
                    {
                        foreach (DataRow action in ds1.Tables[1].Rows)
                        {
                            if (dtAction.Select("ActionCode = '" + action["ActionCode"] + "'").Length <= 0)
                                dtAction.ImportRow(action);
                        }
                    }
                }
            }

            if (dtAction.Rows.Count > 1)
            {
                DataRow drAction = dtAction.NewRow();
                drAction[0] = "ALL";
                drAction[1] = PortalEngineManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTextDescription("ACTION", WorkflowPrinciple.Current.UserSetting.Language, "ALL");
                dtAction.Rows.InsertAt(drAction, 0);
            }

            dtRequest.TableName = "REQUESTDOCUMENT";
            dtAction.TableName = "ACTION";
            dtFlagMaster.TableName = "FLAGMASTER";
            ds.Tables.Add(dtRequest);
            ds.Tables.Add(dtAction);
            ds.Tables.Add(dtFlagMaster);
            return ds;
        }
        [HttpPost]
        public DataSet GetPositionApprove([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oCurrentEmployee);
            DataSet ds = new DataSet();
            try
            {
                int BoxID = Convert.ToInt32(oRequestParameter.InputParameter["BoxID"].ToString());
                string EmpID = oCurrentEmployee.EmployeeID;
                string PositionID = oCurrentEmployee.PositionID;
                string Language = oCurrentEmployee.Language;
                string CompanyCode = oCurrentEmployee.CompanyCode;
                ds = WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetPositionApprove(BoxID, EmpID, PositionID, Language);
            }
            catch (Exception ex)
            {

            }
            return ds;
        }

        public Dictionary<string, object> ByPassTicket([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, object> oResult = new Dictionary<string, object>();

            EmployeeDataForMobile oCurrentEmployee = new EmployeeDataForMobile();
            string oCompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            string oTicketID = oRequestParameter.InputParameter["TicketID"].ToString();
            string oExternaluserID = string.Empty;
            if (oRequestParameter.InputParameter.ContainsKey("ExternaluserID"))
                oExternaluserID = oRequestParameter.InputParameter["ExternaluserID"].ToString();
            if (!string.IsNullOrEmpty(oExternaluserID))
            {
                oResult = AuthenticateForExternalUser(string.Format("{0}|{1}", oExternaluserID, oCompanyCode));
            }
            else
            {
                oCurrentEmployee = oRequestParameter.CurrentEmployee;
                SetAuthenticate(oCurrentEmployee);
            }

            DataTable oReturn = new DataTable();
            try
            {
                oReturn.TableName = "ReturnDocument";
                if (string.IsNullOrEmpty(oExternaluserID))
                    oReturn = WorkflowManagement.CreateInstance(oCompanyCode).ValidateTicket(oTicketID, oRequestParameter.CurrentEmployee.EmployeeID);
                else
                    oReturn = WorkflowManagement.CreateInstance(oCompanyCode).ValidateTicketForExternaluser(oTicketID, WorkflowPrinciple.CurrentIdentity.EmployeeID);
            }
            catch (Exception ex)
            {
                oReturn.TableName = "Error";
                oReturn.Columns.Add("Error");
                DataRow dr = oReturn.NewRow();
                dr[0] = PortalEngineManagement.CreateInstance(oCompanyCode).GetTextDescription("TICKET", WorkflowPrinciple.Current.UserSetting.Language, ex.Message);
                oReturn.Rows.Add(dr);
            }

            oResult.Add("Result", oReturn);
            return oResult;
        }



        [HttpPost]
        /// <summary>
        /// GetRequestDocumentByRequestNo
        /// </summary>
        ///<param name="Param1">RequestNo (string)</param>
        ///<param name="Param2">KeyMaster (string)</param>
        ///<param name="Param3">IsOwnerView (bool)</param>
        ///<param name="Param4">IsDataOwnerView (bool)</param>
        ///<param name="CurrentEmployee"></param>
        /// <returns></returns>
        public RequestDocumentForMobile GetRequestDocumentByRequestNo([FromBody] RequestParameter oRequestParameter)
        {
            RequestDocumentForMobile oDocForMobile = new RequestDocumentForMobile();

            try
            {
                string oRequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                string oRequestCompanyCode = oRequestParameter.InputParameter["RequestCompanyCode"].ToString();
                string oKeyMaster = oRequestParameter.InputParameter["KeyMaster"].ToString();
                bool oIsOwnerView = bool.Parse(oRequestParameter.InputParameter["IsOwnerView"].ToString());
                bool oIsDataOwnerView = bool.Parse(oRequestParameter.InputParameter["IsDataOwnerView"].ToString());
                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
                SetAuthenticate(oCurrentEmployee);
                RequestDocument doc;

                if (oKeyMaster.ToLower() == "null")
                {
                    doc = GetRequestDocument(oRequestNo, oKeyMaster, oRequestCompanyCode, oCurrentEmployee, oCurrentEmployee, oIsOwnerView, oIsDataOwnerView);
                }
                else
                {
                    doc = GetRequestDocument(oRequestNo, oKeyMaster, oRequestCompanyCode, oCurrentEmployee, new EmployeeDataForMobile(), oIsOwnerView, oIsDataOwnerView);
                }

                WorkflowManagement oWorkflowManagement = WorkflowManagement.CreateInstance(doc.RequestorCompanyCode);
                doc.IsOwnerView = oIsOwnerView;
                doc.IsDataOwnerView = oIsDataOwnerView;
                PortalSetting oSetting = PortalEngineManagement.CreateInstance(doc.RequestorCompanyCode).GetPortalSetting(doc.RequestType);
                IDataService dataservice = UIService.GetDataService(oSetting.DataAssembly, oSetting.DataClass);
                doc.SetDataService(dataservice);
                oDocForMobile.ParseToObject(doc, oCurrentEmployee);
                oDocForMobile.Requestor = Convert<EmployeeDataForMobile>.ObjectFrom(doc.Requestor);
                oDocForMobile.Requestor.AreaSetting = doc.Requestor.OrgAssignment.AreaSetting;
                oDocForMobile.Requestor.OrgUnit = doc.Requestor.CurrentOrganization.ObjectID;
                oDocForMobile.Requestor.OrgUnitName = doc.Requestor.CurrentOrganization.AlternativeName(oCurrentEmployee.Language);
                oDocForMobile.Requestor.Name = doc.Requestor.AlternativeName(oCurrentEmployee.Language);
                // Added by Nattawat.s 
                oDocForMobile.Requestor.ImageUrl = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestCompanyCode).PHOTO_PATH, oDocForMobile.Requestor.EmployeeID);
                DataTable dt = oWorkflowManagement.GetRecipientByItemID(doc.RequestID, doc.CurrentItemID);
                foreach (DataRow dr in dt.Select(string.Format("EmployeeID = '{0}'", oCurrentEmployee.EmployeeID)))
                {
                    if (dr["PositionID"].ToString() != oCurrentEmployee.PositionID)
                    {
                        oDocForMobile.ActionBy = Convert<EmployeeDataForMobile>.ObjectFrom(oCurrentEmployee);
                        oDocForMobile.ActionBy.PositionID = dr["PositionID"].ToString();
                    }
                }
                // Added by Pariyaporn P. 
                oDocForMobile.FlowIDForCheck = oDocForMobile.FlowID.ToString();
                oDocForMobile.Status = CacheManager.GetCommonText("STATUS", oCurrentEmployee.Language, oDocForMobile.CurrentFlowItemCode);
                oDocForMobile.RequestFlowList = doc.SimulateFlow(doc);
                oDocForMobile.RequestTypeSummary = CacheManager.GetCommonText("REQUESTTYPE", oCurrentEmployee.Language, doc.RequestType.Code);
                return oDocForMobile;
            }
            catch (DataServiceException ex)
            {
                oDocForMobile.HasError = true;
                oDocForMobile.ErrorText = ProcessError(ex);
            }
            catch (Exception ex)
            {
                oDocForMobile.HasError = true;
                oDocForMobile.ErrorText = ex.Message;

            }
            return oDocForMobile;
        }

        [HttpPost]
        /// <summary>
        /// GetRequestDocumentByRequestNo
        /// </summary>
        ///<param name="Param1">RequestNo (string)</param>
        ///<param name="Param2">KeyMaster (string)</param>
        ///<param name="Param3">IsOwnerView (bool)</param>
        ///<param name="Param4">IsDataOwnerView (bool)</param>
        ///<param name="CurrentEmployee"></param>
        /// <returns></returns>
        public RequestDocumentForMobile GetRequestDocumentByRequestNoWithReSimulate([FromBody] RequestParameter oRequestParameter)
        {
            RequestDocumentForMobile oDocForMobile = new RequestDocumentForMobile();

            try
            {
                string oRequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                string oRequestCompanyCode = oRequestParameter.InputParameter["RequestCompanyCode"].ToString();
                // string oKeyMaster = oRequestParameter.InputParameter["KeyMaster"].ToString();
                bool oIsOwnerView = bool.Parse(oRequestParameter.InputParameter["IsOwnerView"].ToString());
                bool oIsDataOwnerView = bool.Parse(oRequestParameter.InputParameter["IsDataOwnerView"].ToString());
                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
                SetAuthenticate(oCurrentEmployee);
                RequestDocument doc;

                doc = GetRequestDocument(oRequestNo, "null", oRequestCompanyCode, oCurrentEmployee, oCurrentEmployee, oIsOwnerView, oIsDataOwnerView);
                //TravelGroupRequest oTravelGroupRequest = HRTRManagement.CreateInstance(doc.CompanyCode).GetTravelRequest(doc.RequestNo, string.Empty);
                //oTravelGroupRequest.DefaultCurrencyCode = doc.Requestor.OrgAssignment.AreaSetting.CurrencyCode;
                //DataSet oResult = new DataSet("TravelGroupRequest");
                //oResult.Tables.Add(oTravelGroupRequest.ToADODataTable());
                //doc.Additional = oResult;
                //doc.HistoryList = HRTRManagement.CreateInstance(doc.CompanyCode).GetTravelHistory(oRequestNo, doc.RequestorNo, oCurrentEmployee.Language,true);

                doc.IsOwnerView = oIsOwnerView;
                doc.IsDataOwnerView = oIsDataOwnerView;
                oDocForMobile.ParseToObject(doc, oCurrentEmployee);
                oDocForMobile.Requestor = Convert<EmployeeDataForMobile>.ObjectFrom(doc.Requestor);
                oDocForMobile.Requestor.AreaSetting = doc.Requestor.OrgAssignment.AreaSetting;
                oDocForMobile.Requestor.OrgUnit = doc.Requestor.OrgAssignment.OrgUnit;
                oDocForMobile.Requestor.OrgUnitName = doc.Requestor.OrgAssignment.OrgUnitData.AlternativeName(oCurrentEmployee.Language);
                oDocForMobile.Requestor.Name = doc.Requestor.AlternativeName(oCurrentEmployee.Language);
                // Added by Nattawat.s 
                oDocForMobile.Requestor.ImageUrl = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestCompanyCode).PHOTO_PATH, oDocForMobile.Requestor.EmployeeID);
                WorkflowManagement oWorkflowManagement = WorkflowManagement.CreateInstance(doc.RequestorCompanyCode);

                DataTable dt = oWorkflowManagement.GetRecipientByItemID(doc.RequestID, doc.CurrentItemID);
                foreach (DataRow dr in dt.Select(string.Format("EmployeeID = '{0}'", oCurrentEmployee.EmployeeID)))
                {
                    if (dr["PositionID"].ToString() != oCurrentEmployee.PositionID)
                    {
                        oDocForMobile.ActionBy = Convert<EmployeeDataForMobile>.ObjectFrom(oCurrentEmployee);
                        oDocForMobile.ActionBy.PositionID = dr["PositionID"].ToString();
                    }
                }

                oDocForMobile.Status = CacheManager.GetCommonText("STATUS", oCurrentEmployee.Language, oDocForMobile.CurrentFlowItemCode);
                oDocForMobile.RequestFlowList = doc.SimulateFlow(doc);
                oDocForMobile.RequestTypeSummary = CacheManager.GetCommonText("REQUESTTYPE", oCurrentEmployee.Language, doc.RequestType.Code);
                return oDocForMobile;
            }
            catch (DataServiceException ex)
            {
                oDocForMobile.HasError = true;
                oDocForMobile.ErrorText = ProcessError(ex);
            }
            catch (Exception ex)
            {
                oDocForMobile.HasError = true;
                oDocForMobile.ErrorText = ex.ToString();

            }
            return oDocForMobile;
        }

        [HttpPost]
        /// <summary>
        /// GetRequestDocumentByRequestNo
        /// </summary>
        ///<param name="Param1">RequestNo (string)</param>
        ///<param name="Param2">KeyMaster (string)</param>
        ///<param name="Param3">IsOwnerView (bool)</param>
        ///<param name="Param4">IsDataOwnerView (bool)</param>
        ///<param name="CurrentEmployee"></param>
        /// <returns></returns>
        public RequestDocumentForMobile GetRequestDocumentSupportByRequestNo([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string oRequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();

                string oRequestCompanyCode = oRequestParameter.InputParameter["RequestCompanyCode"].ToString();
                string oKeyMaster = oRequestParameter.InputParameter["KeyMaster"].ToString();
                bool oIsOwnerView = bool.Parse(oRequestParameter.InputParameter["IsOwnerView"].ToString());
                bool oIsDataOwnerView = bool.Parse(oRequestParameter.InputParameter["IsDataOwnerView"].ToString());

                int oItemID = int.Parse(oRequestParameter.InputParameter["ItemID"].ToString());

                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
                SetAuthenticate(oCurrentEmployee);
                RequestDocument doc;

                if (oKeyMaster.ToLower() == "null")
                {
                    doc = GetRequestDocument(oRequestNo, oKeyMaster, oRequestCompanyCode, oCurrentEmployee, oCurrentEmployee, oIsOwnerView, oIsDataOwnerView);
                }
                else
                {
                    doc = GetRequestDocument(oRequestNo, oKeyMaster, oRequestCompanyCode, oCurrentEmployee, new EmployeeDataForMobile(), oIsOwnerView, oIsDataOwnerView);
                }

                doc.CurrentItemID = oItemID;

                RequestDocumentForMobile oDocForMobile = new RequestDocumentForMobile();
                doc.IsOwnerView = oIsOwnerView;
                doc.IsDataOwnerView = oIsDataOwnerView;
                oDocForMobile.ParseToObject(doc, oCurrentEmployee);
                oDocForMobile.Requestor = Convert<EmployeeDataForMobile>.ObjectFrom(doc.Requestor);
                oDocForMobile.Requestor.AreaSetting = doc.Requestor.OrgAssignment.AreaSetting;
                oDocForMobile.Requestor.OrgUnit = doc.Requestor.OrgAssignment.OrgUnit;
                oDocForMobile.Requestor.OrgUnitName = doc.Requestor.OrgAssignment.OrgUnitData.AlternativeName(oCurrentEmployee.Language);
                oDocForMobile.Requestor.Name = doc.Requestor.AlternativeName(oCurrentEmployee.Language);
                // Added by Nattawat.s 
                oDocForMobile.Requestor.ImageUrl = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestCompanyCode).PHOTO_PATH, oDocForMobile.Requestor.EmployeeID);
                DataTable dt = WorkflowManagement.CreateInstance(doc.RequestorCompanyCode).GetRecipientByItemID(doc.RequestID, doc.CurrentItemID);
                foreach (DataRow dr in dt.Select(string.Format("EmployeeID = '{0}'", oCurrentEmployee.EmployeeID)))
                {
                    if (dr["PositionID"].ToString() != oCurrentEmployee.PositionID)
                    {
                        oDocForMobile.ActionBy = Convert<EmployeeDataForMobile>.ObjectFrom(oCurrentEmployee);
                        oDocForMobile.ActionBy.PositionID = dr["PositionID"].ToString();
                    }
                }

                // oDocForMobile.RequestFlowList = doc.SimulateFlow(doc);
                oDocForMobile.Status = CacheManager.GetCommonText("STATUS", oCurrentEmployee.Language, oDocForMobile.CurrentFlowItemCode);
                oDocForMobile.RequestTypeSummary = CacheManager.GetCommonText("REQUESTTYPE", oCurrentEmployee.Language, doc.RequestType.Code);
                return oDocForMobile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetFile(string Param1, string Param2, string Param3, string Param4)
        {
            int oFileSetID = int.Parse(Param1);
            int oFileID = int.Parse(Param2);
            string CompanyCode = Param3;
            string EmployeeData = Param4;
            FileAttachmentSet oSet = FileAttachmentSystemManagement.CreateInstance(CompanyCode).LoadFileSet(oFileSetID, CompanyCode);
            FileAttachment oAttachment = oSet.Find(obj => obj.FileID == oFileID);



            Stream stream = new MemoryStream(oAttachment.Data);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentLength = oAttachment.Data.LongLength;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = HttpUtility.UrlEncode(oAttachment.FileName)
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="Param1">FileSetID (int)</param>
        /// <param name="Param2">FileID (int)</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetFile([FromBody] RequestParameter oRequestParameter)
        {
            int oFileSetID = int.Parse(oRequestParameter.InputParameter["FileSetID"].ToString());
            int oFileID = int.Parse(oRequestParameter.InputParameter["FileID"].ToString());
            SetAuthenticate(oRequestParameter.Requestor);
            FileAttachmentSet oSet = FileAttachmentSystemManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).LoadFileSet(oFileSetID);
            FileAttachment oAttachment = oSet.Find(obj => obj.FileID == oFileID);
            Stream stream = new MemoryStream(oAttachment.Data);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentLength = oAttachment.Data.LongLength;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = HttpUtility.UrlEncode(oAttachment.FileName)
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }

        /// <summary>
        /// Used to create new request
        /// </summary>
        /// <param name="Param1">RequestTypeID (int)</param>
        /// <param name="Param2">ItemKey (string), If not used return 'null' to webservice</param>
        /// <param name="CurrentEmployee">EmployeeDataForMobile</param>
        /// <returns></returns>
        [HttpPost]
        public RequestDocumentForMobile CreateRequest([FromBody] RequestParameter oRequestParameter)
        {
            RequestDocumentForMobile oRequestDocumentForMobile = new RequestDocumentForMobile();
            if (oRequestParameter.CurrentEmployee != null && oRequestParameter.CurrentEmployee.EmployeeID != null)
            {
                //Condition may be difference each Module
                int oRequestTypeID = 0;
                string oItemKey = string.Empty;
                string oReferRequestNo = string.Empty;
                string otherParam = string.Empty;
                try
                {
                    //Parse Json string to Object
                    if (oRequestParameter.InputParameter.ContainsKey("REQUESTTYPEID"))
                    {
                        oRequestTypeID = int.Parse(oRequestParameter.InputParameter["REQUESTTYPEID"].ToString());
                    }
                    if (oRequestParameter.InputParameter.ContainsKey("ITEMKEY"))
                    {
                        oItemKey = oRequestParameter.InputParameter["ITEMKEY"].ToString();
                    }
                    if (oRequestParameter.InputParameter.ContainsKey("REFER_REQUESTNO"))
                    {
                        oReferRequestNo = oRequestParameter.InputParameter["REFER_REQUESTNO"].ToString();
                    }
                    if (oRequestParameter.InputParameter.ContainsKey("OTHER_PARAM"))
                    {
                        otherParam = oRequestParameter.InputParameter["OTHER_PARAM"].ToString();
                    }


                    // Call function
                    EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
                    SetAuthenticate(oCurrentEmployee);
                    EmployeeData Requestor = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
                    //if(oCurrentEmployee.EmployeeID != oRequestParameter.Requestor.RequesterEmployeeID)
                    //    Requestor = new EmployeeData(oRequestParameter.Requestor.RequesterEmployeeID, oRequestParameter.Requestor.RequesterPositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

                    oRequestDocumentForMobile.ActionBy = oCurrentEmployee;
                    RequestDocument Document = RequestDocument.CreateInstance(Requestor.CompanyCode);
                    RequestType rt = RequestType.CreateInstance(Requestor.CompanyCode).GetRequestType(oRequestTypeID);
                    PortalSetting oSetting = PortalEngineManagement.CreateInstance(Requestor.CompanyCode).GetPortalSetting(rt);

                    string[] EditorString = oSetting.EditorClass.Split('.');
                    string[] ViewerString = oSetting.ViewerClass.Split('.');
                    oSetting.EditorClass = string.Empty;
                    oSetting.ViewerClass = string.Empty;
                    for (int i = 0; i < EditorString.Length; i++)
                    {
                        if (EditorString.Length - 1 == i)
                            oSetting.EditorClass += EditorString[i];
                        else
                            oSetting.EditorClass += EditorString[i].ToLower() + ".";
                    }
                    for (int i = 0; i < ViewerString.Length; i++)
                    {
                        if (ViewerString.Length - 1 == i)
                            oSetting.ViewerClass += ViewerString[i];
                        else
                            oSetting.ViewerClass += ViewerString[i].ToLower() + ".";
                    }

                    IDataService dataservice;
                    try
                    {
                        dataservice = UIService.GetDataService(oSetting.DataAssembly, oSetting.DataClass);
                        Dictionary<string, string> CreateDocParam = new Dictionary<string, string>();
                        CreateDocParam["ReferRequestNo"] = oReferRequestNo;
                        CreateDocParam["OtherParam"] = otherParam;
                        Document = RequestDocument.CreateInstance(Requestor.CompanyCode).CreateRequest(Requestor, rt, dataservice, CreateDocParam);
                        // 2020-10-02 add by Pariyaporn P. get flow id,key code for milestonestep
                        try
                        {
                            oRequestDocumentForMobile.FlowKey = !string.IsNullOrEmpty(Document.FlowKey) ? Document.FlowKey : "";
                            DataTable FlowItem = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetFlowIDByRequestTypeIDKeyCode(oRequestTypeID.ToString());
                            if (FlowItem.Rows.Count == 1)
                            {
                                oRequestDocumentForMobile.FlowKey = FlowItem.Rows[0]["KeyCode"].ToString();
                                oRequestDocumentForMobile.FlowIDForCheck = FlowItem.Rows[0]["FlowID"].ToString();
                            }
                            else
                            {
                                foreach (DataRow r in FlowItem.Rows)
                                {
                                    string FlowID = r["FlowID"].ToString();
                                    string KeyCode = r["KeyCode"].ToString();
                                    if (oRequestDocumentForMobile.FlowKey == KeyCode)
                                    {
                                        oRequestDocumentForMobile.FlowIDForCheck = FlowID;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                        // 2020-10-02 end by Pariyapron P.

                    }
                    catch (DataServiceException ex)
                    {
                        oRequestDocumentForMobile.HasError = true;
                        oRequestDocumentForMobile.ErrorText = ProcessError(ex);
                    }
                    catch (Exception ex)
                    {
                        oRequestDocumentForMobile.HasError = true;
                        oRequestDocumentForMobile.ErrorText = "Can't create data's service";
                    }
                    oRequestDocumentForMobile.ParseToObject(Document, oCurrentEmployee);
                    oRequestDocumentForMobile.Viewer = oSetting.ViewerClass.Replace('.', '/').ToLower();
                    oRequestDocumentForMobile.Editor = oSetting.EditorClass.Replace('.', '/').ToLower();
                    oRequestDocumentForMobile.Requestor = Convert<EmployeeDataForMobile>.ObjectFrom(Document.Requestor);

                    oRequestDocumentForMobile.Requestor.AreaSetting = Document.Requestor.OrgAssignment.AreaSetting;
                    oRequestDocumentForMobile.Requestor.OrgUnit = Document.Requestor.CurrentOrganization.ObjectID;
                    oRequestDocumentForMobile.Requestor.OrgUnitName = Document.Requestor.CurrentOrganization.AlternativeName(oCurrentEmployee.Language);
                    // Added by Nattawat.s 
                    oRequestDocumentForMobile.Requestor.ImageUrl = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(Requestor.CompanyCode).PHOTO_PATH, oRequestDocumentForMobile.Requestor.EmployeeID);
                    DateSpecificData oDateSpecificData = Document.Requestor.DateSpecific;
                    oRequestDocumentForMobile.Requestor.HiringDate = oDateSpecificData.HiringDate;
                    oRequestDocumentForMobile.Requestor.StartWorkingDate = oDateSpecificData.StartWorkingDate;
                    oRequestDocumentForMobile.Requestor.RetirementDate = oDateSpecificData.RetirementDate;

                    //oRequestDocumentForMobile.Requestor.OrgUnit = Document.Requestor.OrgAssignment.OrgUnit;
                    //oRequestDocumentForMobile.Requestor.OrgUnitName = Document.Requestor.OrgAssignment.OrgUnitData.AlternativeName(oCurrentEmployee.Language);
                    oRequestDocumentForMobile.NewRequest = true;
                    oRequestDocumentForMobile.Status = CacheManager.GetCommonText("STATUS", oCurrentEmployee.Language, eState.CREATE.ToString());
                    oRequestDocumentForMobile.RequestTypeSummary = CacheManager.GetCommonText("REQUESTTYPE", oCurrentEmployee.Language, Document.RequestType.Code);

                    oRequestDocumentForMobile.RequestActionList = new List<RequestActionForMobile>();
                    RequestActionForMobile mobileAction = new RequestActionForMobile();
                    if (Document.RequestType.CanSave)
                    {
                        mobileAction.IsVisible = true;
                        mobileAction.ActionText = RequestText.CreateInstance(Requestor.CompanyCode).LoadText("ACTION", oRequestDocumentForMobile.ActionBy.Language, "SAVE");
                        mobileAction.IconClass = "fal fa-paperclip_arrow_right";
                        mobileAction.ColorClass = "-tab-green";
                        mobileAction.Code = "SAVE";
                        mobileAction.URL = "Workflow/TakeActionFromEditRequest/SAVE";
                        oRequestDocumentForMobile.RequestActionList.Add(mobileAction);
                    }
                    mobileAction.IsVisible = true;
                    mobileAction.ActionText = RequestText.CreateInstance(Requestor.CompanyCode).LoadText("ACTION", oRequestDocumentForMobile.ActionBy.Language, "SAVENEXT");
                    mobileAction.IconClass = "icon-save";
                    mobileAction.ColorClass = "-tab-green";
                    mobileAction.Code = "SAVENEXT";
                    mobileAction.URL = "Workflow/TakeActionFromEditRequest/SAVENEXT";

                    oRequestDocumentForMobile.RequestActionList.Add(mobileAction);

                    oRequestDocumentForMobile.RedirectURL = string.Format("frmEditRequest/{0}/{1}/{2}/{3}/{4}/{5}/{6}", Document.RequestNo, Document.KeyMaster, Document.IsOwnerView, Document.IsDataOwnerView, string.IsNullOrEmpty(oItemKey) ? "0" : oItemKey, string.IsNullOrEmpty(oReferRequestNo) ? "0" : oReferRequestNo, otherParam);
                }
                catch (Exception ex)
                {
                    oRequestDocumentForMobile.HasError = true;
                    oRequestDocumentForMobile.ErrorText = "Cannot create request. {0}";
                }
                finally
                {
                    //
                }
            }
            return oRequestDocumentForMobile;
        }

        private string ProcessError(Exception e)
        {
            string ErrorText = string.Empty;
            CultureInfo DefaultCultureInfo = new CultureInfo("en-US");
            if (e is DataServiceException)
            {
                DataServiceException oException = (DataServiceException)e;
                string ExceptionText = string.Empty;
                if (oException.Code.Split(',').Length > 0)
                {
                    foreach (string Code in oException.Code.Split(','))
                        ErrorText += CacheManager.GetCommonText(oException.Category, WorkflowPrinciple.Current.UserSetting.Language, Code) + " ";
                }
                else
                    ErrorText += CacheManager.GetCommonText(oException.Category, WorkflowPrinciple.Current.UserSetting.Language, oException.Code) + " ";

                if (oException.Args != null)
                {
                    ErrorText = string.Format(ErrorText, oException.Args);
                }
            }
            else if (e is SaveExternalDataException)
            {
                ErrorText += "Error in save external function: " + e.Message;
            }
            else if (e is RequireCommentException)
            {
                RequireCommentException oException = (RequireCommentException)e;
                string ExceptionText = string.Empty;
                if (oException.Code.Split(',').Length > 0)
                {
                    foreach (string Code in oException.Code.Split(','))
                        ErrorText += CacheManager.GetCommonText(oException.Category, WorkflowPrinciple.Current.UserSetting.Language, Code) + " ";
                }
                else
                    ErrorText += CacheManager.GetCommonText(oException.Category, WorkflowPrinciple.Current.UserSetting.Language, oException.Code) + " ";

                if (oException.Args != null)
                {
                    ErrorText = string.Format(ErrorText, oException.Args);
                }


                //    ErrorText += string.Format(" {0} {1} {2}", e.Message, e.InnerException, e.StackTrace);
            }

            if (!string.IsNullOrEmpty(e.Message) && string.IsNullOrEmpty(ErrorText))
                ErrorText = e.Message;
            return ErrorText;
        }

        private List<RequestFlowForMobile> GetFlow(RequestDocument Document)
        {
            List<RequestFlowForMobile> oReturn = new List<RequestFlowForMobile>();
            List<RequestFlow> flows = Document.SimulateFlow();
            RequestFlowForMobile oRequestFlowForMobile = new RequestFlowForMobile();
            foreach (RequestFlow flow in flows)
            {
                oRequestFlowForMobile = new RequestFlowForMobile();
                //oRequestFlowForMobile.ActionBy = flow.ActionBy;
                //oRequestFlowForMobile.ActionCode = flow.ActionCode;
                oReturn.Add(oRequestFlowForMobile);
            }
            return oReturn;
        }

        private RequestDocument GetRequestDocument(string RequestNo, string KeyMaster, string CompanyCode, EmployeeDataForMobile oCurrentEmployee, EmployeeDataForMobile Requestor, bool IsOwnerView, bool IsDataOwnerView)
        {
            RequestDocument doc;

            if (!string.IsNullOrEmpty(Requestor.EmployeeID))
            {
                EmployeeData emp = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID);
                doc = RequestDocument.LoadDocumentReadOnly(RequestNo, CompanyCode, emp, IsOwnerView, IsDataOwnerView);
            }
            else
                doc = RequestDocument.LoadDocument(RequestNo, CompanyCode, KeyMaster, IsOwnerView, IsDataOwnerView);

            PortalSetting oSetting = PortalEngineManagement.CreateInstance(CompanyCode).GetPortalSetting(doc.RequestType);
            IDataService dataservice = UIService.GetDataService(oSetting.DataAssembly, oSetting.DataClass);
            doc.SetDataService(dataservice);
            doc.Viewer = oSetting.ViewerClass.Replace('.', '/').ToLower();
            doc.Editor = oSetting.EditorClass.Replace('.', '/').ToLower();
            doc.IsOwnerView = IsOwnerView;
            doc.IsDataOwnerView = IsDataOwnerView;
            // doc.CompanyCode = doc.requ

            if (doc.RequestorNo == WorkflowPrinciple.CurrentIdentity.EmployeeID)
            {
                doc.IsOwnerView = true;

                List<string> IsOwnerViewFalse = new List<string>();
                IsOwnerViewFalse.Add("ED");
                IsOwnerViewFalse.Add("CR");

                if (IsOwnerViewFalse.Contains(doc.CurrentFlowItem.State.Code.ToUpper()))
                    doc.IsOwnerView = false;
            }

            return doc;
        }

        [HttpPost]
        public RequestDocumentForMobile TakeActionFromViewRequest(string Param1, [FromBody] RequestParameter oRequestParameter)
        {
            string oActionCode = Param1; // oRequestParameter.InputParameter["ActionCode"].ToString();
            RequestDocumentForMobile oRequestDocumentForMobile = oRequestParameter.RequestDocument;
            oRequestDocumentForMobile.HasError = false;
            oRequestDocumentForMobile.RedirectURL = string.Empty;

            SetAuthenticate(oRequestDocumentForMobile.ActionBy);
            /* map additional data */
            PortalSetting oSetting = new PortalSetting();
            IDataService dataservice;
            RequestType rt = RequestType.CreateInstance(oRequestParameter.RequestDocument.RequestorCompanyCode).GetRequestType(oRequestDocumentForMobile.RequestTypeID);
            oSetting = PortalEngineManagement.CreateInstance(oRequestDocumentForMobile.Requestor.CompanyCode).GetPortalSetting(rt);
            dataservice = UIService.GetDataService(oSetting.DataAssembly, oSetting.DataClass);

            /* !map additional data */
            EmployeeData TakeActionBy = new EmployeeData(oRequestDocumentForMobile.ActionBy.EmployeeID, oRequestDocumentForMobile.ActionBy.PositionID, oRequestDocumentForMobile.ActionBy.CompanyCode, DateTime.Now);
            RequestDocument Document = GetRequestDocument(oRequestDocumentForMobile.RequestNo, oRequestDocumentForMobile.KeyMaster, oRequestDocumentForMobile.RequestorCompanyCode, oRequestDocumentForMobile.ActionBy, new EmployeeDataForMobile(), oRequestDocumentForMobile.IsOwnerView, oRequestDocumentForMobile.IsDataOwnerView);
            Document.IsOwnerView = oRequestDocumentForMobile.IsOwnerView;
            //เช็ดคนที่กดเอกสาร มีสิทธิ์ submit รึเปล่า
            //Document.IsDataOwnerView = ESS.HR.PD.HRPDManagement.CreateInstance(oRequestDocumentForMobile.RequestorCompanyCode).GetIsDataOwner(oRequestDocumentForMobile.RequestNo, oRequestDocumentForMobile.ActionBy.EmployeeID);

            RequestActionForMobile oRequestActionForMobile = oRequestDocumentForMobile.RequestActionList.Find(act => act.Code == oActionCode);
            EmployeeData oCurrentEmployee = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oRequestDocumentForMobile.ActionBy);
            List<IActionData> IActionDataList = Document.GetPossibleActionsForMobile(oCurrentEmployee);
            IActionData action = IActionDataList.Find(act => act.Code == oActionCode);
            FlowItemAction oFlowItemAction = new FlowItemAction();
            if (action is FlowItemAction)
                oFlowItemAction = (FlowItemAction)action;

            bool canAction = true;
            // toolBar1.IsCanAction = true;
            if (!string.IsNullOrEmpty(Document.IsTimeout) && string.IsNullOrEmpty(oRequestDocumentForMobile.Comment) && oFlowItemAction.NextItemID != 3)
            {
                canAction = false;
                //toolBar1.IsCanAction = false;
            }
            DataTable FlowItemActionPreference = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetFlowItemActionPreference();
            Document.Comment = oRequestDocumentForMobile.Comment;
            Document.Comment2 = oRequestDocumentForMobile.Comment2;
            if (action.Code == "EDIT")
            {
                oRequestDocumentForMobile.RequestActionList = new List<RequestActionForMobile>();
                RequestActionForMobile mobileAction = new RequestActionForMobile();
                if (Document.RequestType.CanSave)
                {
                    mobileAction.ActionText = RequestText.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText("ACTION", oRequestDocumentForMobile.ActionBy.Language, "SAVE");
                    mobileAction.Code = "SAVE";
                    foreach (DataRow Preference in FlowItemActionPreference.Select("FlowItemActionCode = '" + mobileAction.Code + "'"))
                    {
                        mobileAction.ColorClass = Preference["ColorClass"].ToString();
                        mobileAction.IconClass = Preference["IconClass"].ToString();
                    }
                    mobileAction.IsVisible = true;
                    mobileAction.URL = "Workflow/TakeActionFromEditRequest/SAVE";
                    oRequestDocumentForMobile.RequestActionList.Add(mobileAction);
                }
                mobileAction.ActionText = RequestText.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText("ACTION", oRequestDocumentForMobile.ActionBy.Language, "SAVENEXT");
                mobileAction.Code = "SAVENEXT";
                foreach (DataRow Preference in FlowItemActionPreference.Select("FlowItemActionCode = '" + mobileAction.Code + "'"))
                {
                    mobileAction.ColorClass = Preference["ColorClass"].ToString();
                    mobileAction.IconClass = Preference["IconClass"].ToString();
                }
                mobileAction.IsVisible = true;
                mobileAction.URL = "Workflow/TakeActionFromEditRequest/SAVENEXT";
                oRequestDocumentForMobile.RequestActionList.Add(mobileAction);

                mobileAction = new RequestActionForMobile();
                mobileAction.ActionText = RequestText.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadText("ACTION", oRequestDocumentForMobile.ActionBy.Language, "BACK");
                mobileAction.Code = "BACK";
                //foreach (DataRow Preference in FlowItemActionPreference.Select("FlowItemActionCode = '" + mobileAction.Code + "'"))
                //{
                mobileAction.ColorClass = "-tab-org";
                mobileAction.IconClass = "icon-back";
                //}
                mobileAction.IsVisible = true;
                mobileAction.URL = "Workflow/TakeActionFromEditRequest/BACK";
                oRequestDocumentForMobile.RequestActionList.Add(mobileAction);

                //0 ตัวสุดท้าายสำหรับเก็บ Itemkey เพื่อ mapping กับ Route.js ใน UI
                oRequestDocumentForMobile.RedirectURL = string.Format("frmEditRequest/{0}/{1}/{2}/{3}/0", Document.RequestNo, Document.KeyMaster, Document.IsOwnerView, Document.IsDataOwnerView);
            }
            else
            {

                RequestActionResult result;
                try
                {
                    if (canAction)
                    {
                        Document.Additional = oRequestDocumentForMobile.Additional;
                        dataservice.ValidateDataInAction(Document.Additional, Document.Data, Document.Requestor, Document.RequestNo, Document.CurrentFlowItem.Code, oFlowItemAction.Code, !string.IsNullOrEmpty(Document.Comment), !string.IsNullOrEmpty(Document.Comment2), Document.SubmittedDate);
                        CollectData(ref Document, oRequestDocumentForMobile, true, oFlowItemAction.Code);
                        result = Document.Process((FlowItemAction)action, TakeActionBy);
                    }
                    else
                    {
                        oRequestDocumentForMobile.HasError = true;
                        oRequestDocumentForMobile.ErrorText = CacheManager.GetCommonText("TIMEOUT", WorkflowPrinciple.Current.UserSetting.Language, Document.IsTimeout) + "\r\n" + CacheManager.GetCommonText("TIMEOUT", WorkflowPrinciple.Current.UserSetting.Language, "TIMEOUT_BOLDREMARK");
                    }
                }
                catch (RequireCommentException ex)
                {
                    oRequestDocumentForMobile.HasError = true;
                    oRequestDocumentForMobile.RequireComment = true;
                    oRequestDocumentForMobile.ErrorText = ProcessError(ex);
                }
                catch (DataServiceException ex)
                {
                    var errId = WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode)
                        .AddErrorLog(Document != null ? Document.RequestNo : "-"
                        , oCurrentEmployee.EmployeeID, ex as Exception);

                    oRequestDocumentForMobile.HasError = true;
                    oRequestDocumentForMobile.ErrorText = ProcessError(ex);
                }
                catch (SmtpException ex)
                {
                }
                catch (ProcessRequestException ex)
                {
                    Document.HasError = true;
                    Document.ErrorText = ex.ToString();
                }
                catch (Exception ex)
                {
                    oRequestDocumentForMobile.HasError = true;
                    oRequestDocumentForMobile.ErrorText = "Unexpected error. Please send this message to system administrator<br/>" + ex.Message;
                }


                if (canAction && !oRequestDocumentForMobile.HasError)
                {
                    bool isError = Document.HasError;
                    string strPopupTitle = string.Empty;
                    string strPopupMessage = string.Empty;
                    string strErrorMessage = string.Empty;

                    //
                    //Implement something, If you need to show some popup result when user process this request
                    //
                    if (isError)
                    {
                        strErrorMessage = Document.ErrorText;
                    }

                    Document = GetRequestDocument(oRequestDocumentForMobile.RequestNo, "null", oRequestDocumentForMobile.RequestorCompanyCode, oRequestDocumentForMobile.ActionBy, oRequestDocumentForMobile.ActionBy, oRequestDocumentForMobile.IsOwnerView, oRequestDocumentForMobile.IsDataOwnerView);
                    Document.IsOwnerView = oRequestDocumentForMobile.IsOwnerView;
                    Document.IsDataOwnerView = oRequestDocumentForMobile.IsDataOwnerView;

                    oRequestDocumentForMobile.RequestFlowList = Document.SimulateFlow();
                    oRequestDocumentForMobile.RedirectURL = "frmViewResult";
                    oRequestDocumentForMobile.PopupTitle = strPopupTitle;
                    oRequestDocumentForMobile.PopupMessage = strPopupMessage;
                    oRequestDocumentForMobile.HasError = isError;
                    oRequestDocumentForMobile.ErrorText = strErrorMessage;
                }

            }

            return oRequestDocumentForMobile;
        }

        public IDataService GetDataService(string AssemblyName, string ClassName)
        {
            try
            {
                Assembly oAssembly = Assembly.Load(AssemblyName);
                Type oType = oAssembly.GetType(ClassName);
                IDataService oService = (IDataService)Activator.CreateInstance(oType);
                return oService;
            }
            catch (Exception ex)
            {
                throw new Exception("Can't get data service", ex);
            }
        }

        [HttpPost]
        public RequestDocumentForMobile TakeActionFromEditRequest(string Param1, [FromBody] RequestParameter oRequestParameter)
        {
            string oActionCode = Param1;
            RequestDocumentForMobile oRequestDocumentForMobile = oRequestParameter.RequestDocument;
            string Exception_Category = string.Empty;
            oRequestDocumentForMobile.HasError = false;
            oRequestDocumentForMobile.ErrorText = string.Empty;
            oRequestDocumentForMobile.RedirectURL = string.Empty;

            SetAuthenticate(oRequestDocumentForMobile.ActionBy);

            if (oActionCode == "BACK")
            {
                RequestDocument newRequestDocument = RequestDocument.LoadDocumentReadOnly(oRequestDocumentForMobile.RequestNo, oRequestDocumentForMobile.Requestor.CompanyCode);
                oRequestDocumentForMobile.RedirectURL = string.Format("frmViewRequest/{0}/{1}/{2}/{3}/{4}", oRequestDocumentForMobile.RequestNo, oRequestDocumentForMobile.Requestor.CompanyCode, newRequestDocument.KeyMaster, oRequestDocumentForMobile.IsOwnerView, oRequestDocumentForMobile.IsDataOwnerView);
            }
            else
            {
                EmployeeData TakeActionBy = new EmployeeData(oRequestDocumentForMobile.ActionBy.EmployeeID, oRequestDocumentForMobile.ActionBy.PositionID, oRequestDocumentForMobile.ActionBy.CompanyCode, DateTime.Now);
                EmployeeData Requestor = new EmployeeData(oRequestDocumentForMobile.Requestor.EmployeeID, oRequestDocumentForMobile.Requestor.PositionID, oRequestDocumentForMobile.Requestor.CompanyCode, DateTime.Now);
                // EmployeeData Requestor = new EmployeeData(oRequestDocumentForMobile.Requestor.EmployeeID, oRequestDocumentForMobile.Requestor.PositionID) { CompanyCode = oRequestDocumentForMobile.ActionBy.CompanyCode };
                RequestDocument Document = new RequestDocument();
                PortalSetting oSetting = new PortalSetting();
                IDataService dataservice;
                RequestType rt = RequestType.CreateInstance(Requestor.CompanyCode).GetRequestType(oRequestDocumentForMobile.RequestTypeID);
                oSetting = PortalEngineManagement.CreateInstance(Requestor.CompanyCode).GetPortalSetting(rt);
                dataservice = UIService.GetDataService(oSetting.DataAssembly, oSetting.DataClass);
                /* map additional data */
                //if (oRequestDocumentForMobile.AdditionalJson != null && oRequestDocumentForMobile.AdditionalJson != string.Empty)
                //{
                //    oRequestDocumentForMobile.Additional = JsonConvert.DeserializeObject(oRequestDocumentForMobile.AdditionalJson);// //dataservice.ParseJsonToDataSet(oRequestDocumentForMobile.AdditionalJson);
                //    oRequestDocumentForMobile.AdditionalJson = "";
                //}
                /* !map additional data */

                if (oRequestDocumentForMobile.NewRequest)
                {
                    try
                    {
                        Dictionary<string, string> dictParam = new Dictionary<string, string>();
                        dictParam["ReferRequestNo"] = oRequestDocumentForMobile.ReferRequestNo;
                        Document = RequestDocument.CreateInstance(Requestor.CompanyCode).CreateRequest(Requestor, rt, dataservice, dictParam);
                        // 2020-10-02 add by Pariyaporn P. get flow id,key code for milestonestep
                        //try
                        //{
                        //    oRequestDocumentForMobile.FlowKey = !string.IsNullOrEmpty(Document.FlowKey) ? Document.FlowKey : "";
                        //    DataTable FlowItem = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetFlowIDByRequestTypeIDKeyCode(oRequestTypeID.ToString());
                        //    if (FlowItem.Rows.Count == 1)
                        //    {
                        //        oRequestDocumentForMobile.FlowKey = FlowItem.Rows[0]["KeyCode"].ToString();
                        //        oRequestDocumentForMobile.FlowIDForCheck = FlowItem.Rows[0]["FlowID"].ToString();
                        //    }
                        //    else
                        //    {
                        //        foreach (DataRow r in FlowItem.Rows)
                        //        {
                        //            string FlowID = r["FlowID"].ToString();
                        //            string KeyCode = r["KeyCode"].ToString();
                        //            if (oRequestDocumentForMobile.FlowKey == KeyCode)
                        //            {
                        //                oRequestDocumentForMobile.FlowIDForCheck = FlowID;
                        //                break;
                        //            }
                        //        }
                        //    }
                        //}
                        //catch (Exception ex)
                        //{

                        //}
                        // 2020-10-02 end by Pariyapron P.
                    }
                    catch (Exception ex)
                    {
                        oRequestDocumentForMobile.HasError = true;
                        oRequestDocumentForMobile.ErrorText = ProcessError(ex);
                    }
                }
                else
                    Document = GetRequestDocument(oRequestDocumentForMobile.RequestNo, oRequestDocumentForMobile.KeyMaster, Requestor.CompanyCode, oRequestDocumentForMobile.ActionBy, new EmployeeDataForMobile(), oRequestDocumentForMobile.IsOwnerView, oRequestDocumentForMobile.IsDataOwnerView);

                try
                {
                    Document.SetDataService(dataservice);
                }
                catch (Exception ex)
                {
                    oRequestDocumentForMobile.HasError = true;
                    oRequestDocumentForMobile.ErrorText = "Cant create datas service";
                }
                // IDataEditor oEditor = (IDataEditor)Page.LoadControl(string.Format("Control/{0}.ascx", oSetting.EditorClass.Replace('.', '/')));
                //oEditor.ProcessError()HRTM_EXCEPTION

                try
                {
                    Document.Comment = oRequestDocumentForMobile.Comment;
                    Document.Comment2 = oRequestDocumentForMobile.Comment2;
                    CollectData(ref Document, oRequestDocumentForMobile, false, string.Empty);

                    // update new requestno
                    oRequestDocumentForMobile.RequestNo = Document.RequestNo;
                }
                catch (Exception ex)
                {
                    var errId = WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode)
                        .AddErrorLog(Document != null ? Document.RequestNo : "-"
                        , oRequestDocumentForMobile.ActionBy.EmployeeID, ex as Exception);

                    oRequestDocumentForMobile.HasError = true;
                    oRequestDocumentForMobile.ErrorText = ProcessError(ex);
                }

                if (!oRequestDocumentForMobile.HasError)
                {
                    if (oActionCode == "SAVE")
                    {
                        oRequestDocumentForMobile.RedirectURL = "frmViewResult";
                        //Response.Redirect(string.Format("frmViewResult.aspx?SessKey={0}", newSessKey));
                    }
                    else if (oActionCode == "SAVENEXT")
                    {
                        RequestDocument newRequestDocument = RequestDocument.LoadDocumentReadOnly(oRequestDocumentForMobile.RequestNo, Requestor.CompanyCode);
                        oRequestDocumentForMobile.RedirectURL = string.Format("frmViewRequest/{0}/{1}/{2}/{3}/{4}", oRequestDocumentForMobile.RequestNo, Requestor.CompanyCode, newRequestDocument.KeyMaster, oRequestDocumentForMobile.IsOwnerView, oRequestDocumentForMobile.IsDataOwnerView);
                        //Response.Redirect(string.Format("frmViewRequest.aspx?SessKey={0}&RefreshMainPage=Y", newSessKey));
                    }
                }
                else
                {

                }
            }

            return oRequestDocumentForMobile;
        }

        private void CollectData(ref RequestDocument Document, RequestDocumentForMobile oRequestDocumentForMobile, bool IsView, string ActionCode)
        {
            WorkflowManagement oWorkflowManagement = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode);
            Object newData = oRequestDocumentForMobile.Additional;
            //oEditor.LoadData(Document.Requestor, newData, Document.DocumentDate);
            FileAttachmentSet oAttach = Document.LoadFileAttached();
            int NoOfFileAttached = 0;
            if (oRequestDocumentForMobile.HasFileAttached)
            {
                NoOfFileAttached = oRequestDocumentForMobile.FileSet.Attachments.Where(x => !x.IsDelete).Count();
                foreach (FileAttachmentForMobile attMobile in oRequestDocumentForMobile.FileSet.Attachments)
                {
                    //If existsing file and delete
                    if (oAttach.Exists(att => att.FileID == attMobile.FileID))
                    {
                        if (attMobile.IsDelete)
                        {
                            if (File.Exists(oWorkflowManagement.FILE_ROOTPATH + attMobile.FilePath + "/" + attMobile.FileName))
                            {
                                File.Delete(oWorkflowManagement.FILE_ROOTPATH + attMobile.FilePath + "/" + attMobile.FileName);
                            }
                            oAttach.RemoveAll(att => att.FileID == attMobile.FileID);
                        }
                    }
                    //If new file
                    else
                    {
                        FileAttachment newatt = new FileAttachment();
                        int FileID = 0;
                        if (oAttach.Count > 0)
                            int.TryParse(oAttach.Max(obj => obj.FileID).ToString(), out FileID);
                        newatt.FileID = FileID + 1;
                        newatt.FileSetID = attMobile.FileSetID;
                        newatt.FileName = attMobile.FileName;
                        newatt.FileType = attMobile.FileType;
                        newatt.Data = attMobile.Data;
                        oAttach.Add(newatt);
                    }
                    //FileAttachmentSet oAttach = Document.LoadFileAttached();
                    //oEditor.LoadAttachment(oAttach);
                }
                Document.AttachFile(oAttach);
            }

            if (IsView)
            {
                if (ActionCode == "SUBMIT")// && Document.CreatedDate.Date < DateTime.Now.Date) CommentBy: Ratchatawan W. 31 Oct 2017
                    Document.ApplyDataForView(newData, true, ActionCode, Document.SubmittedDate);
                else
                    Document.ApplyDataForView(newData, false, ActionCode, Document.SubmittedDate);
            }
            else
                Document.ApplyData(newData, NoOfFileAttached, ActionCode, Document.SubmittedDate);
        }

        //private void CollectData(ref RequestDocument Document, RequestDocumentForMobile oRequestDocumentForMobile)
        //{
        //    DataSet newData = oRequestDocumentForMobile.Additional;
        //    //oEditor.LoadData(Document.Requestor, newData, Document.DocumentDate);
        //    //FileAttachmentSet oAttach = Document.LoadFileAttached();
        //    FileAttachmentSet oAttach = Document.LoadFileAttached();
        //    if (oRequestDocumentForMobile.HasFileAttached)
        //    {
        //        oAttach.Clear();
        //        int oFileId = 0;
        //        oRequestDocumentForMobile.FileSet.Attachments.Where(cw => !cw.IsDelete).ForEach(current => 
        //            {
        //                oFileId++;
        //                FileAttachment oFileAttachment = new FileAttachment();
        //                oFileAttachment.Data = current.Data;
        //                oFileAttachment.FileID = oFileId;
        //                oFileAttachment.FileName = current.FileName;
        //                oFileAttachment.FileSetID = oFileId;
        //                oFileAttachment.FileType = current.FileType;

        //                oAttach.Add(oFileAttachment);
        //            });

        //        //foreach (FileAttachmentForMobile attMobile in oRequestDocumentForMobile.FileSet.Attachments)
        //        //{
        //        //    //If existsing file and delete
        //        //    if (oAttach.Exists(att => att.FileID == attMobile.FileID))
        //        //    {
        //        //        if (attMobile.IsDelete)
        //        //            oAttach.RemoveAll(att => att.FileID == attMobile.FileID);
        //        //    }
        //        //    //If new file
        //        //    else
        //        //    {
        //        //        FileAttachment newatt = new FileAttachment();
        //        //        newatt.FileID = oAttach.Max(obj => obj.FileID) + 1;
        //        //        newatt.FileSetID = attMobile.FileSetID;
        //        //        newatt.FileName = attMobile.FileName;
        //        //        newatt.FileType = attMobile.FileType;
        //        //        newatt.Data = attMobile.Data;
        //        //        oAttach.Add(newatt);
        //        //    }
        //        //    //FileAttachmentSet oAttach = Document.LoadFileAttached();
        //        //    //oEditor.LoadAttachment(oAttach);
        //        //}
        //        Document.AttachFile(oAttach);
        //    }
        //    Document.ApplyData(newData);
        //}

        /// <summary>
        /// param1 = Language EN or TH
        /// param2 = IsReceuveMail True or False
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public bool SaveUserSetting([FromBody] RequestParameter oRequestParameter)
        {
            bool flgSave = false;
            string oLanguage = oRequestParameter.InputParameter["Language"].ToString();
            bool oReceiveMail = bool.Parse(oRequestParameter.InputParameter["ReceiveMail"].ToString());
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            WorkflowPrinciple.Current.UserSetting.Language = oLanguage;
            WorkflowPrinciple.Current.UserSetting.ReceiveMail = oReceiveMail;

            try
            {
                if (WorkflowPrinciple.Current.IsInRole("OTPMREQUESTOR"))
                {
                    WorkflowPrinciple.Current.UserSetting.Roles.Remove("OTPMREQUESTOR");
                }
                WorkflowPrinciple.Current.UserSetting.ReceiveMail = oReceiveMail;
                EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).SaveUserSetting(WorkflowPrinciple.Current.UserSetting);
                flgSave = true;
            }
            catch (Exception ex)
            {
            }

            return flgSave;
        }

        /// <summary>
        /// param1 = Language EN or TH
        /// </summary>
        /// <param name="Param1"></param>
        /// <param name="oCurrentEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        public Dictionary<string, object> GetEmployeeData([FromBody] RequestParameter oRequestParameter)
        {
            string oLanguage = oRequestParameter.InputParameter["Language"].ToString();
            Dictionary<string, object> oResult = new Dictionary<string, object>();
            SetAuthenticate(oRequestParameter.CurrentEmployee);
            EmployeeData oEmployeeData = new EmployeeData(oRequestParameter.CurrentEmployee.EmployeeID);
            if (oEmployeeData != null)
            {
                oRequestParameter.CurrentEmployee.Name = oEmployeeData.AlternativeName(oLanguage);
                oRequestParameter.CurrentEmployee.Position = oEmployeeData.OrgAssignment.PositionData.AlternativeName(oLanguage);
                oRequestParameter.CurrentEmployee.Language = WorkflowPrinciple.Current.UserSetting.Language;
                oRequestParameter.CurrentEmployee.ReceiveMail = WorkflowPrinciple.Current.UserSetting.ReceiveMail;
                oRequestParameter.CurrentEmployee.CompanyDetail = ShareDataManagement.GetCompanyByCompanyCode(oRequestParameter.CurrentEmployee.CompanyCode);
                oRequestParameter.CurrentEmployee.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(oLanguage);
                oRequestParameter.CurrentEmployee.AllPosition = oEmployeeData.GetAllPositions(oLanguage);
            }
            oResult["EmployeeData"] = oRequestParameter.CurrentEmployee;
            return oResult;
        }

        /// <param name="Param1">RequestNo List</param>
        /// <param name="Param2">ActionCode</param>
        /// <returns></returns>
        [HttpPost]
        public object MassApprove([FromBody] RequestParameter oRequestParameter)//string Param1, string Param2, 
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string RequestNo = string.Empty;
            string KeyMaster = string.Empty;
            string CompanyCode = string.Empty;
            string PositionID = string.Empty;
            string ActionCode = string.Empty;
            int Sum_RequestNo_Completed = 0;
            int Sum_RequestNo_UnCompleted = 0;
            int Sum_RequestNo_UnCompletedMassAction = 0;
            //string RequestNo_Completed = string.Empty;
            //string RequestNo_UnCompleted = string.Empty;
            foreach (string Request in oRequestParameter.InputParameter["RequestNoSelected"].ToString().Split('|'))
            {
                RequestNo = Request.Split(',')[0];
                KeyMaster = Request.Split(',')[1];
                CompanyCode = Request.Split(',')[2];
                PositionID = Request.Split(',')[3];
                RequestDocument oDoc = RequestDocument.LoadDocument(RequestNo, CompanyCode, KeyMaster, false, false);
                EmployeeData TakeActionBy = new EmployeeData(oCurrentEmployee.EmployeeID, PositionID, oCurrentEmployee.CompanyCode, DateTime.Now);

                //If document is timeout, it can't use MassApprove function

                PortalSetting oSetting = PortalEngineManagement.CreateInstance(CompanyCode).GetPortalSetting(oDoc.RequestType);
                IDataService oDataService = UIService.GetDataService(oSetting.DataAssembly, oSetting.DataClass);
                oDoc.SetDataService(oDataService);

                Object Data = oDoc.Additional;
                oDataService.SetAdditionalDataToDocument(oDoc.Requestor, ref Data);

                if (string.IsNullOrEmpty(oDoc.IsTimeout))
                {
                    DataTable FlowItemActionPreference = WorkflowManagement.CreateInstance(oDoc.CompanyCode).GetFlowItemActionPreference();
                    List<IActionData> IActionDataList = oDoc.GetPossibleActions(WorkflowPrinciple.Current.UserSetting.Employee);

                    switch (oRequestParameter.InputParameter["ActionCode"].ToString().ToUpper())
                    {
                        case "ALL":
                            ActionCode = "MassApprove";
                            break;
                        case "REJECT":
                            ActionCode = "MassReject";
                            break;
                        default:
                            break;
                    }
                    if (IActionDataList.Count > 0)
                    {
                        bool IsMassAction = false;
                        foreach (var a in IActionDataList)
                        {
                            var MassAction = FlowItemActionPreference.Select("FlowItemActionCode = '" + a.Code + "' and MassActionSelector = '" + ActionCode + "'");
                            if (MassAction.Count() > 0 &&
                                (MassAction[0]["MassActionSelector"].ToString() == "MassApprove" || MassAction[0]["MassActionSelector"].ToString() == "MassReject"))
                            {
                                IsMassAction = true;

                                FlowItemAction oFlowItemAction = new FlowItemAction();
                                if (a is FlowItemAction)
                                {
                                    oFlowItemAction = (FlowItemAction)a;

                                    if (MassAction[0]["MassActionSelector"].ToString() == "MassReject")
                                    {
                                        oFlowItemAction.Code = "REJECT";
                                    }

                                    oDoc.Comment = oRequestParameter.InputParameter["Comment"].ToString();
                                    try
                                    {
                                        oDoc.Process(oFlowItemAction, TakeActionBy);
                                        Sum_RequestNo_Completed++;
                                    }
                                    catch
                                    {
                                        Sum_RequestNo_UnCompleted++;
                                    }
                                }
                                break;
                            }
                            //get massAction
                        }

                        if (!IsMassAction)
                        {
                            Sum_RequestNo_UnCompletedMassAction++;
                        }

                    }
                }
            }
            object oReturn = new
            {
                Sum_RequestNo_Completed,
                Sum_RequestNo_UnCompleted,
                Sum_RequestNo_UnCompletedMassAction
            };

            return oReturn;
        }

        /// <summary>
        /// GetAuthorizeMassApprove: check authorized for view Mass Approve of current user
        /// </summary>
        /// <param name="Param1">BoxID</param>
        /// <returns></returns>
        [HttpPost]
        public bool GetAuthorizeMassApprove(int Param1, [FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            return RequestBox.CreateInstance(oCurrentEmployee.CompanyCode).GetAuthorizeMassApproveForMobile(Param1, WorkflowPrinciple.CurrentIdentity.EmployeeID, WorkflowPrinciple.CurrentIdentity.CurrentPosition);
        }

        [HttpGet]
        public HttpResponse GetProfileImageProxy(string Param1)
        {
            //string url = "http://ict-ho-web02.pttict.corp/phonedirectory/photo/530163.jpg";
            string url = Param1;
            HttpResponse resp = null;

            //Create a stream for the file
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            int bytesToRead = 10000;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(url);

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                string fileName = "profile_image.jpg";
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read


            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
            return resp;
        }

        //ใช้สำหรับการสร้าง Delegate ใหม่
        [HttpPost]
        public DelegateHeader GetDelegateObject([FromBody] RequestParameter oRequestParameter)
        {
            // Dictionary<string, Object> oReturn = new Dictionary<string, object>();

            EmployeeDataForMobile oRequestor = oRequestParameter.Requestor;
            SetAuthenticate(oRequestor);

            Dictionary<string, Object> oAllEmployeePossibleToDelegated = new Dictionary<string, object>();
            foreach (INFOTYPE1000 info10 in oRequestor.AllPosition)
                oAllEmployeePossibleToDelegated.Add(info10.ObjectID, GetDelegateToByEmployeePosition(oRequestor.EmployeeID, info10.ObjectID, oRequestor.CompanyCode));

            //oReturn.Add("DELEGATETO", DelegateTo);

            DelegateHeader oDelegateHeader = new DelegateHeader();
            oDelegateHeader.DelegateFrom = oRequestor.EmployeeID;
            oDelegateHeader.DelegateFromPosition = oRequestor.PositionID;
            oDelegateHeader.DelegateFromPositionName = oRequestor.Position;
            oDelegateHeader.DelegateFromOrg = oRequestor.OrgUnit;
            oDelegateHeader.DelegateFromOrgName = oRequestor.OrgUnitName;
            oDelegateHeader.AllEmployeePossibleToDelegated = oAllEmployeePossibleToDelegated;
            oDelegateHeader.DelegateFromCompanyCode = oRequestor.CompanyCode;

            List<EmployeeDataForMobile> DelegatedList = (List<EmployeeDataForMobile>)oAllEmployeePossibleToDelegated[oRequestor.PositionID];
            if (DelegatedList.Count > 0)
            {
                oDelegateHeader.DelegateTo = DelegatedList[0].EmployeeID;
                oDelegateHeader.DelegateToPosition = DelegatedList[0].PositionID;
                oDelegateHeader.DelegateFromPositionName = DelegatedList[0].Position;
                oDelegateHeader.DelegateFromOrg = DelegatedList[0].OrgUnit;
                oDelegateHeader.DelegateFromOrgName = DelegatedList[0].OrgUnitName;
                oDelegateHeader.DelegateToCompanyCode = oRequestor.CompanyCode;
            }
            oDelegateHeader.BeginDate = DateTime.Now.Date;
            oDelegateHeader.EndDate = DateTime.Now.Date;
            //oDelegateHeader.CreatorNo = oRequestor.EmployeeID;
            //oDelegateHeader.CreatorCompanyCode = oRequestor.CompanyCode;

            oDelegateHeader.DetailList = new List<DelegateDetail>();
            foreach (DataRow dr in WorkflowManagement.CreateInstance(oRequestor.CompanyCode).GetAllRequestType(oRequestor.Language).Rows)
            {
                DelegateDetail oDetail = new DelegateDetail();
                oDetail.DelegateID = oDelegateHeader.DelegateID;
                oDetail.RequestTypeID = int.Parse(dr["RequestTypeID"].ToString());
                oDetail.RequestTypeName = PortalEngineManagement.CreateInstance(oRequestor.CompanyCode).GetTextDescription("REQUESTTYPE", WorkflowPrinciple.Current.UserSetting.Language, dr["RequestTypeCode"].ToString());
                oDetail.IsCheck = Convert.ToBoolean(dr["IsCheck"]);
                oDelegateHeader.DetailList.Add(oDetail);
            }

            // oReturn.Add("DELEGATEOBJECT", oDelegateHeader);

            return oDelegateHeader;
        }

        [HttpPost]
        public Dictionary<string, Object> GetEmployeeToDelegated([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oRequestor = oRequestParameter.Requestor;
            SetAuthenticate(oRequestor);
            Dictionary<string, Object> oAllEmployeePossibleToDelegated = new Dictionary<string, object>();
            foreach (INFOTYPE1000 info10 in oRequestor.AllPosition)
                oAllEmployeePossibleToDelegated.Add(info10.ObjectID, GetDelegateToByEmployeePosition(oRequestor.EmployeeID, info10.ObjectID, oRequestor.CompanyCode));
            return oAllEmployeePossibleToDelegated;
        }

        //ใช้สำหรับการดึงข้อมูลมาแสดงในหน้าจอตารางของหน้า Delegate และใช้ในการ Edit
        [HttpPost]
        public List<DelegateHeader> GetDelegateDataByCriteria([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.Requestor;//oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            List<DelegateHeader> oTemp = new List<DelegateHeader>();
            List<DelegateHeader> oReturn = new List<DelegateHeader>();
            int iMonth = int.Parse(oRequestParameter.InputParameter["Month"].ToString());
            int iYear = int.Parse(oRequestParameter.InputParameter["Year"].ToString());
            oTemp = WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetDelegateDataByCriteria(oCurrentEmployee.EmployeeID, iMonth, iYear);

            if (oTemp.Count > 0)
            {
                foreach (DelegateHeader item in oTemp)
                {
                    if (item.DelegateTo == oCurrentEmployee.EmployeeID && !(item.Status == "DRAFT" || item.Status == "WAITFOREDIT"))
                    {
                        oReturn.Add(item);
                    }
                    if (item.DelegateFrom == oCurrentEmployee.EmployeeID)
                    {
                        oReturn.Add(item);
                    }
                }
            }
            return oReturn;
        }

        //ใช้สำหรับการดึงข้อมูลพนักงานที่มีสิทธิ์ในการทำแทน
        public List<EmployeeDataForMobile> GetDelegateToByEmployeePosition(string EmployeeID, string PositionID, string CompanyCode)
        {
            List<EmployeeDataForMobile> oReturn = new List<EmployeeDataForMobile>();
            List<EmployeeData> EmployeeList = WorkflowManagement.CreateInstance(CompanyCode).GetDelegateToByEmployeePosition(EmployeeID, PositionID);
            foreach (EmployeeData oEmployeeData in EmployeeList)
            {
                EmployeeDataForMobile oInf1Mobile = Convert<EmployeeDataForMobile>.ObjectFrom(oEmployeeData.OrgAssignment);
                oInf1Mobile.Name = oEmployeeData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                oInf1Mobile.EmployeeID = oEmployeeData.EmployeeID;
                try
                {
                    oInf1Mobile.PositionID = oEmployeeData.ActionOfPosition.ObjectID;//oEmployeeData.OrgAssignment.PositionData.ObjectID;
                    oInf1Mobile.Position = WorkflowPrinciple.Current.UserSetting.Language == "EN" ? oEmployeeData.ActionOfPosition.TextEn : oEmployeeData.ActionOfPosition.Text; //oEmployeeData.OrgAssignment.PositionData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                }
                catch
                {
                    throw new Exception("EXCEPTION_USER_POSITION_NOT_FOUND");
                }
                oInf1Mobile.AllPosition = oEmployeeData.GetAllPositions(WorkflowPrinciple.Current.UserSetting.Language);
                //Modify By Morakot.t 2018-10-03
                //string ImageFile = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(CompanyCode).PHOTO_PATH, oInf1Mobile.EmployeeID.Substring(2));
                string ImageFile = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(CompanyCode).PHOTO_PATH, oInf1Mobile.EmployeeID);
                oInf1Mobile.ImageUrl = ImageFile;
                oInf1Mobile.RequesterEmployeeID = oEmployeeData.EmployeeID;
                oInf1Mobile.RequesterPositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                oInf1Mobile.CompanyCode = oEmployeeData.CompanyCode;//AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                oReturn.Add(oInf1Mobile);
            }
            return oReturn;
        }

        [HttpPost]
        public DataTable GetAllRequestType([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.Requestor == null ? oRequestParameter.CurrentEmployee : oRequestParameter.Requestor;
            SetAuthenticate(oCurrentEmployee);
            return WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetAllRequestType(WorkflowPrinciple.Current.UserSetting.Language);
        }

        [HttpPost]
        public List<UserRoleClass> GetAllUserRole([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            return ShareDataManagement.GetAllUserRole();
        }

        [HttpPost]
        public List<UserRoleEdit> GetAlluserRoleEditor([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DataTable dtUserRole = ShareDataManagement.GetAlluserRoleEditor();
            List<RoleClass> oRoleList = ShareDataManagement.GetAllRole();
            List<UserRoleResponseType> oUserRoleResponseType = ShareDataManagement.GetAllUserRoleResponseType(oCurrentEmployee.Language);

            List<UserRoleEdit> oUserList = new List<UserRoleEdit>();
            string TmpEmpID = "";
            string TmpCompanyCode = String.Empty;
            foreach (DataRow dr in dtUserRole.Rows)
            {
                if (TmpEmpID != dr["EmployeeID"].ToString() || TmpCompanyCode != dr["CompanyCode"].ToString())
                {
                    TmpEmpID = dr["EmployeeID"].ToString();
                    TmpCompanyCode = dr["CompanyCode"].ToString();
                    UserRoleEdit oUser = new UserRoleEdit();
                    oUser.EmployeeID = dr["EmployeeID"].ToString();
                    oUser.EmployeeName = dr["Name"].ToString();
                    oUser.CompanyCode = dr["CompanyCode"].ToString();
                    oUser.CompanyName = "";
                    oUser.Content = new List<UserRoleResponseContent>();
                    List<UserRoleResponseContent> oUserContentList = new List<UserRoleResponseContent>();

                    foreach (DataRow drCon in dtUserRole.Rows)
                    {
                        if (TmpEmpID == drCon["EmployeeID"].ToString() && TmpCompanyCode == drCon["CompanyCode"].ToString())
                        {
                            UserRoleResponseContent oContent = new UserRoleResponseContent();
                            oContent.EmployeeID = drCon["EmployeeID"].ToString();
                            oContent.EmployeeName = drCon["Name"].ToString();
                            oContent.CompanyCode = drCon["CompanyCode"].ToString();
                            oContent.CompanyName = "";
                            oContent.UserRole = drCon["UserRole"].ToString();
                            var sRoleDesc = oRoleList.Where(p => p.Role == drCon["UserRole"].ToString()).Select(p => p).ToList();
                            if (sRoleDesc.Count > 0)
                                oContent.RoleDesc = sRoleDesc[0].Description;
                            oContent.ResponseType = drCon["ResponseType"].ToString();
                            var sResponseDesc = oUserRoleResponseType.Where(p => p.ResponseType == drCon["ResponseType"].ToString()).Select(p => p).ToList();
                            if (sResponseDesc.Count > 0)
                                oContent.ResponseDesc = sResponseDesc[0].Description;
                            oContent.ResponseCode = drCon["ResponseCode"].ToString();

                            DataTable dtResCodeByResType = HRPAManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetAllResponseCodeByResponseType(drCon["ResponseType"].ToString(), drCon["CompanyCode"].ToString(), oCurrentEmployee.Language);
                            foreach (DataRow drResCodeByResType in dtResCodeByResType.Rows)
                            {
                                if (drResCodeByResType["ResponseCode"].ToString() == drCon["ResponseCode"].ToString())
                                {
                                    oContent.ResponseCodeDesc = drResCodeByResType["Name"].ToString();
                                    break;
                                }
                            }
                            oContent.ResponseCompanyCode = drCon["ResponseCompanyCode"].ToString();
                            oContent.ResponseCompanyDesc = "";
                            //Nattawat S. Add includesub
                            oContent.includeSub = Convert.ToBoolean(String.IsNullOrEmpty(drCon["includeSub"].ToString()) ? "False" : drCon["includeSub"].ToString());
                            oUser.Content.Add(oContent);
                        }
                    }
                    oUserList.Add(oUser);
                }
            }

            return oUserList;
        }

        [HttpPost]
        public List<RoleClass> GetAllRole([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            return ShareDataManagement.GetAllRole();
        }

        [HttpPost]
        public List<UserRoleResponseType> GetAllUserRoleResponseType([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            return ShareDataManagement.GetAllUserRoleResponseType(oCurrentEmployee.Language);
        }

        [HttpPost]
        public List<ESS.SHAREDATASERVICE.CONFIG.UserRoleResponse> GetUserRoleResponse([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            return ShareDataManagement.GetUserRoleResponse(oRequestParameter.InputParameter["EmployeeID"].ToString(), oRequestParameter.InputParameter["CompanyCode"].ToString());
        }

        [HttpPost]
        public void SaveUserRoleResponse([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            SetAuthenticate(oCurrentEmployee);

            List<ESS.SHAREDATASERVICE.CONFIG.UserRoleResponse> oUserRoleResponseList = new List<ESS.SHAREDATASERVICE.CONFIG.UserRoleResponse>();
            oUserRoleResponseList = (JSon.Deserialize<List<ESS.SHAREDATASERVICE.CONFIG.UserRoleResponse>>(oRequestParameter.InputParameter["UserRoleResponse"].ToString()));
            ShareDataManagement.SaveUserRoleResponse(oUserRoleResponseList);

            var oCompanyList = from usrl in oUserRoleResponseList
                               group usrl by usrl.CompanyCode;
            //only current company is update , others update by jobDaily automatic.
            foreach (var oCom in oCompanyList)
            {
                if (oCom.Key == oCurrentEmployee.CompanyCode)
                {
                    OMManagement oMan = OMManagement.CreateInstance(oCom.Key);
                    oMan.GenUserRoleResponseSnapshot();
                    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, null, "Copy UserRole, UserRoleResponse, UserResponceSnapshot Complete", true);
                }
            }

        }

        [HttpPost]
        public void SaveDelegateHeader([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DelegateHeader oHeader = JSon.Deserialize<DelegateHeader>(oRequestParameter.InputParameter["DelegateHeader"].ToString());
            WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode).SaveDelegateHeader(oHeader);
        }

        [HttpPost]
        public void DeleteDelegateHeader([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DelegateHeader oHeader = JSon.Deserialize<DelegateHeader>(oRequestParameter.InputParameter["DelegateHeader"].ToString());
            WorkflowManagement.CreateInstance(oCurrentEmployee.CompanyCode).DeleteDelegateHeader(oHeader);
        }

        [HttpPost]
        public List<FileAttachmentForMobile> GetRequestTypeFileSet([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            //string employeeID = oRequestParameter.InputParameter["EmployeeID"].ToString();
            string employeeID = oRequestParameter.Requestor.EmployeeID;//Start 26082021 for tae
            int requestTypeID = int.Parse(oRequestParameter.InputParameter["RequestTypeID"].ToString());
            string requestSubType = oRequestParameter.InputParameter["RequestSubType"].ToString();

            //File Attachment
            List<FileAttachmentForMobile> FileAttachmentForMobileList = new List<FileAttachmentForMobile>();


            List<PersonalAttachmentFileSet> oAttExport = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentExport(employeeID, requestTypeID, requestSubType);
            for (int i = 0; i < oAttExport.Count; i++)
            {
                FileAttachmentForMobile file = new FileAttachmentForMobile();
                file.FileID = oAttExport[i].FileID;
                file.RequestNo = oAttExport[i].RequestNo;
                file.FileName = oAttExport[i].FileName;
                file.FileSetID = oAttExport[i].FileSetID;
                file.FilePath = oAttExport[i].FilePath;
                file.FileType = oAttExport[i].FileType;
                FileAttachmentForMobileList.Add(file);
            }

            List<PersonalAttachmentFileSet> oAttFileSet = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentFileSet(employeeID, requestTypeID, requestSubType);
            for (int i = 0; i < oAttFileSet.Count; i++)
            {
                FileAttachmentForMobile file = new FileAttachmentForMobile();
                file.FileID = oAttFileSet[i].FileID;
                file.RequestNo = oAttFileSet[i].RequestNo;
                file.FileName = oAttFileSet[i].FileName;
                file.FileSetID = oAttFileSet[i].FileSetID;
                file.FilePath = oAttFileSet[i].FilePath;
                file.FileType = oAttFileSet[i].FileType;
                FileAttachmentForMobileList.Add(file);
            }

            //FileAttachmentSet oFileSet = RequestTypeFileSet.GetRequestTypeFileSet(employeeID, requestTypeID, requestSubType);
            //for (int i = 0; i < oFileSet.Count; i++)
            //{
            //    FileAttachmentForMobile file = new FileAttachmentForMobile();
            //    file.FileID = oFileSet[i].FileID;
            //    file.FileName = oFileSet[i].FileName;
            //    file.FileSetID = oFileSet[i].FileSetID;
            //    file.FilePath = oFileSet[i].FilePath;
            //    file.FileType = oFileSet[i].FileType;
            //    FileAttachmentForMobileList.Add(file);
            //}

            return FileAttachmentForMobileList;
        }

        [HttpPost]
        public string GetRootPath([FromBody] RequestParameter oRequestParameter)
        {   //Nattawat S.
            //string strRootPath = ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "ESS.ANNOUNCEMENT", "IMAGE_ROOT_PATH");
            string strRootPath = ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "ESS.ANNOUNCEMENT", "IMAGE_ROOT_PATH") + oRequestParameter.CurrentEmployee.CompanyCode + "/";
            return strRootPath;
        }



        //Method for check requestor's userrole can access any data by criteria.
        [HttpPost]
        public bool checkUserroleAccessToPanel([FromBody] RequestParameter oRequestParameter)
        {
            bool oReturn = false;
            string oKeyCode = oRequestParameter.InputParameter["KeyCode"].ToString(),
                oCompanyCode = oRequestParameter.Requestor.CompanyCode;

            try
            {
                string userroleList = ShareDataManagement.LookupCache(oCompanyCode, "USERROLE.AUTHEN.UI", oKeyCode);
                string[] userroles = userroleList.Split(',');

                if (userroles.Length == 0)
                    oReturn = true;
                else
                {
                    foreach (string str in userroles)
                    {
                        if (oRequestParameter.Requestor.UserRoles.Contains(str))
                        {
                            oReturn = true;
                            break;
                        }

                    }
                }

                return oReturn;
            }
            catch
            {
                return oReturn;
            }
        }

        //Method for check requestor's empsubgroup can access any data by criteria.
        [HttpPost]
        public bool checkEmpSubGroupAccessToPanel([FromBody] RequestParameter oRequestParameter)
        {
            bool oReturn = false;
            string oKeyCode = oRequestParameter.InputParameter["KeyCode"].ToString(),
                oCompanyCode = oRequestParameter.Requestor.CompanyCode;

            try
            {
                string EmpsubList = ShareDataManagement.LookupCache(oCompanyCode, "EMPSUBGROUP.AUTHEN.UI", oKeyCode);
                string[] Empsubs = EmpsubList.Split(',');

                if (Empsubs.Length == 0)
                    oReturn = true;
                else
                {
                    foreach (string str in Empsubs)
                    {
                        if (Convert.ToInt16(oRequestParameter.Requestor.EmpSubGroup) >= Convert.ToInt16(str))
                        {
                            oReturn = true;
                            break;
                        }

                    }
                }

                return oReturn;
            }
            catch
            {
                return oReturn;
            }
        }

        [HttpPost]
        public void EditPhotoByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            string EmployeeID = oRequestParameter.InputParameter["EmployeeID"].ToString();
            FileAttachment fielEmp = JSon.Deserialize<FileAttachment>(oRequestParameter.InputParameter["PhotoEmployee"]);

            //string rootPath = WorkflowManagement.CreateInstance(CompanyCode).FILE_ROOTPATH;

            string strPathEmp = ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "ESS.EMPLOYEE", "EMPLOYEE_PHOTO_PATH") + CompanyCode + "/";
            string strRootPath = System.Web.HttpContext.Current.Server.MapPath(strPathEmp);
            //EMPLOYEE_PHOTO_PATH

            FileAttachment newatt = new FileAttachment();
            int FileID = 0;
            newatt.FileID = FileID + 1;
            newatt.FileSetID = fielEmp.FileSetID;
            newatt.FileName = fielEmp.FileName;
            newatt.FileType = fielEmp.FileType;
            newatt.Data = fielEmp.Data;


            string filePath = strRootPath;
            // string fileFullPath = "D:/" + filePath+"/";
            bool exists = System.IO.Directory.Exists(filePath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(filePath);
            }

            if (newatt.Data != null)
            {
                //Add By Nipon Supap 2018-01-26 (Support-005) Number Index
                string nFileName = newatt.FileName.Substring(0, newatt.FileName.IndexOf('.'));
                string nFileType = newatt.FileName.Substring(newatt.FileName.IndexOf('.') + 1, newatt.FileName.Length - newatt.FileName.IndexOf('.') - 1);
                //string nIdentity = GetPad2SolveStupidThaiFilenameProblemOnAL11(fileSet.RequestNo);
                newatt.FileName = EmployeeID + "." + nFileType;

                try
                {

                    System.IO.DirectoryInfo di = new DirectoryInfo(filePath);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        if (file.Name.Substring(0, file.Name.Length - 4) == EmployeeID)
                            file.Delete();
                    }

                    // Open file for reading
                    System.IO.FileStream _FileStream =
                       new System.IO.FileStream(filePath + newatt.FileName, System.IO.FileMode.Create,
                                                System.IO.FileAccess.Write);
                    // Writes a block of bytes to this stream using data from
                    // a byte array.
                    _FileStream.Write(newatt.Data, 0, newatt.Data.Length);

                    // close file stream
                    _FileStream.Close();
                }
                catch (Exception _Exception)
                {
                    // Error
                    Console.WriteLine("Exception caught in process: {0}",
                                      _Exception.ToString());
                }
            }
        }
        [HttpPost]
        public string Admin_bypass_register([FromBody] RequestParameter oRequestParameter)
        {
            return ShareDataManagement.MaintainAdminBypass(oRequestParameter.InputParameter["Username"].ToString(), oRequestParameter.InputParameter["password"].ToString(), oRequestParameter.InputParameter["email"].ToString(), oRequestParameter.InputParameter["firstlogin"].ToString(), oRequestParameter.InputParameter["responseid"].ToString(), oRequestParameter.InputParameter["CompanyCode"].ToString(), oRequestParameter.InputParameter["CompanyName"].ToString(), oRequestParameter.InputParameter["CurrentCompany"].ToString());
        }
        [HttpPost]
        public string Admin_bypass_login([FromBody] RequestParameter oRequestParameter)
        {
            return ShareDataManagement.AdminBypassLogin(oRequestParameter.InputParameter["Username"].ToString(), oRequestParameter.InputParameter["password"].ToString(), oRequestParameter.InputParameter["flag"].ToString(), oRequestParameter.InputParameter["Company"].ToString());
        }
        [HttpPost]
        public int AdminBypassCompany([FromBody] RequestParameter oRequestParameter)
        {
            return ShareDataManagement.AdminBypassCompany();
        }
        [HttpPost]
        public string AdminBypassResetPassword([FromBody] RequestParameter oRequestParameter)
        {
            return ShareDataManagement.AdminBypassResetPassword(oRequestParameter.InputParameter["Username"].ToString(), oRequestParameter.InputParameter["flag"].ToString(), oRequestParameter.InputParameter["Company"].ToString(), oRequestParameter.InputParameter["CompanyName"].ToString());
        }
        [HttpPost]
        public void AdminBypassSaveLog([FromBody] RequestParameter oRequestParameter)
        {
            ShareDataManagement.AdminBypassSaveLog(oRequestParameter.InputParameter["Username"].ToString(), oRequestParameter.InputParameter["EmployeeCode"].ToString(), oRequestParameter.InputParameter["CompanyCode"].ToString(), oRequestParameter.InputParameter["Description"].ToString(), oRequestParameter.InputParameter["IPAddress"].ToString(), oRequestParameter.InputParameter["ClientName"].ToString());
        }
        [HttpGet, HttpPost]
        public List<BypassCompany> GetBypassCompanyList()
        {
            return ShareDataManagement.GetBypassCompanyList();
        }



    }
}