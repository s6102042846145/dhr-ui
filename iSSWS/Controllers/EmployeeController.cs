﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.UTILITY.CONVERT;
using iSSWS.Models;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.WORKFLOW;
using System.Data;
using ESS.HR.PA;
using SAPInterfaceExt;
using SAP.Connector;
using ESS.PORTALENGINE;
using ESS.HR.PA.DATACLASS;
using System.Globalization;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmployeeController : ApiController
    {
        private CultureInfo enCL = new CultureInfo("en-US");

        #region " Personal Administration "
        [HttpPost]
        public List<OrganizationData> INFOTYPE0001GetAllEmployeeHistory([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            List<OrganizationData> oResult = new List<OrganizationData>();
            List<INFOTYPE0001> oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0001GetAllEmployeeHistory(oRequestParameter.CurrentEmployee.Language);
            foreach (INFOTYPE0001 row in oINF1)
            {
                oResult.Add(Convert<OrganizationData>.ObjectFrom(row));
            }
            //Convert<List<OrganizationData>>.ObjectFrom(EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0001GetAllEmployeeHistory());

            return oResult;
        }

        [HttpPost]
        public void DeletePersonManagementData([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DeleteINFOTYPE0002And0182And0001And0105And1001Data(oRequestParameter.InputParameter["EmployeeID"].ToString());
        }

        [HttpPost]
        public object GetDataHistory([FromBody] RequestParameter oRequestParameter)
        {
            object oResult = new
            {
                PersonData = INFOTYPE0002And0182GetAllHistory(oRequestParameter),
                OrganizationData = INFOTYPE0001GetAllHistory(oRequestParameter),
                ContactData = INFOTYPE0105GetAllHistory(oRequestParameter)
            };
            return oResult;
        }

        [HttpPost]
        public List<PersonalData> INFOTYPE0002And0182GetAllHistory([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalData> oPersonalData = new List<PersonalData>();
                      

            //Dictionary<string, object> oTemp = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0002And0182GetAllHistory(oRequestParameter.InputParameter["EmployeeID"].ToString());
            Dictionary<string, object> oTemp = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0002And0182GetAllHistory(oRequestParameter.Requestor.EmployeeID);
            string oINF2Str = JSon.Serialize(oTemp["INFOTYPE0002"]);
            List<INFOTYPE0002> oINF2 = JSon.Deserialize<List<INFOTYPE0002>>(oINF2Str);
            string oINF182Str = JSon.Serialize(oTemp["INFOTYPE0182"]);
            List<INFOTYPE0182> oINF182 = JSon.Deserialize<List<INFOTYPE0182>>(oINF182Str);
            int i = 0;
            foreach (INFOTYPE0002 row in oINF2)
            {
                oPersonalData.Add(new PersonalData
                {
                    EmployeeID = row.EmployeeID,
                    BeginDate = row.BeginDate.Date.AddDays(1),
                    EndDate = row.EndDate ,//row.EndDate.Date.AddDays(1),
                    TitleID = row.TitleID,
                    FirstName = row.FirstName,
                    LastName = row.LastName,
                    NickName = row.NickName,
                    FullNameEn = oINF182[i].AlternateName,
                    DOB = row.DOB,
                    Gender = row.Gender,
                    Nationality = row.Nationality,
                    MaritalStatus = row.MaritalStatus,
                    Religion = row.Religion,
                    Language = row.Language
                });
                i++;
            }
            return oPersonalData;
            //return EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0002And0182GetAllHistory(oRequestParameter.InputParameter["EmployeeID"].ToString());
        }

        [HttpPost]
        public List<OrganizationData> INFOTYPE0001GetAllHistory([FromBody] RequestParameter oRequestParameter)
        {
            List<OrganizationData> oResult = new List<OrganizationData>();
            List<INFOTYPE0001> oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0001GetAllHistory(oRequestParameter.InputParameter["EmployeeID"].ToString(), oRequestParameter.CurrentEmployee.Language);
            foreach (INFOTYPE0001 row in oINF1)
            {
                oResult.Add(Convert<OrganizationData>.ObjectFrom(row));
            }
            return oResult;
        }

        [HttpPost]
        public List<ContactData> INFOTYPE0105GetAllHistory([FromBody] RequestParameter oRequestParameter)
        {
            List<ContactData> oResult = new List<ContactData>();
            List<INFOTYPE0105> oINF105 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0105GetAllHistory(oRequestParameter.InputParameter["EmployeeID"].ToString());
            foreach (INFOTYPE0105 row in oINF105)
            {
                oResult.Add(Convert<ContactData>.ObjectFrom(row));
            }
            return oResult;
        }

        [HttpPost]
        public void DeleteINFOTYPE0002And0182([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DeleteINFOTYPE0002And0182(oRequestParameter.InputParameter["EmployeeID"].ToString(), BeginDate, EndDate.AddSeconds(-1).AddDays(1));
        }

        [HttpPost]
        public void DeleteINFOTYPE0001([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DeleteINFOTYPE0001(oRequestParameter.InputParameter["EmployeeID"].ToString(), BeginDate);
        }

        [HttpPost]
        public void DeleteINFOTYPE0105([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DeleteINFOTYPE0105(oRequestParameter.InputParameter["EmployeeID"].ToString(), BeginDate, oRequestParameter.InputParameter["SubType"].ToString());
        }

        [HttpPost]
        public void Resign([FromBody] RequestParameter oRequestParameter)
        {
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).Resign(oRequestParameter.InputParameter["EmployeeID"].ToString(), EndDate);
        }

        [HttpPost]
        public object GetEmployeeAllData([FromBody] RequestParameter oRequestParameter)
        {

            PersonalData oPersonalData = new PersonalData();
            DateTime oBeginDate = !oRequestParameter.InputParameter.ContainsKey("BeginDate") ? DateTime.MinValue : DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            Dictionary<string, object> oTemp = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0002And0182Get(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate);
            string oINF2Str = JSon.Serialize(oTemp["INFOTYPE0002"]);
            List<INFOTYPE0002> oINF2 = JSon.Deserialize<List<INFOTYPE0002>>(oINF2Str);
            string oINF182Str = JSon.Serialize(oTemp["INFOTYPE0182"]);
            List<INFOTYPE0182> oINF182 = JSon.Deserialize<List<INFOTYPE0182>>(oINF182Str);
            DateSpecificData oDateSpecificData = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetDateSpecificData(oRequestParameter.InputParameter["EmployeeID"].ToString());
            DateTime hiringdate = oDateSpecificData.HiringDate;
            int i = 0;
            foreach (INFOTYPE0002 row in oINF2)
            {
                oPersonalData = new PersonalData
                {
                    EmployeeID = oRequestParameter.Requestor.EmployeeID,
                    BeginDate = row.BeginDate,
                    EndDate = row.EndDate,
                    TitleID = row.TitleID,
                    FirstName = row.FirstName,
                    LastName = row.LastName,
                    NickName = row.NickName,
                    FullNameEn = oINF182[i].AlternateName,
                    DOB = row.DOB,
                    Gender = row.Gender,
                    Nationality = row.Nationality,
                    MaritalStatus = row.MaritalStatus,
                    Religion = row.Religion,
                    Language = row.Language
                };
                i++;
            }

            string period = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCalculatePeriod(oINF2[0].DOB, DateTime.Now, oRequestParameter.CurrentEmployee.Language);
            //DateSpecificData oDateSpecificData = JSon.Deserialize<DateSpecificData>(info41);
            INFOTYPE0001 oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0001Get(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate, oRequestParameter.CurrentEmployee.Language);
            
            OrganizationData oResultOraData = Convert<OrganizationData>.ObjectFrom(oINF1);

            INFOTYPE0105 oINF105 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0105Get(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate, "");
            ContactData oResultContData = Convert<ContactData>.ObjectFrom(oINF105);
            oResultContData.Name = oINF1.Name;

            ESS.HR.PA.DATACLASS.RelatedAge EmpRelatedAge = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetRelatedAge(oRequestParameter.InputParameter["EmployeeID"].ToString(), DateTime.Now);
            string ageOfCompany = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GenAge(EmpRelatedAge.ageOfCompany, oRequestParameter.CurrentEmployee.Language);
            string ageOfLevel = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GenAge(EmpRelatedAge.ageOfLevel, oRequestParameter.CurrentEmployee.Language);

            object oResult = new
            {
                PersonData = oPersonalData,
                OrgData = oResultOraData,
                ContData = oResultContData,
                DateSpecificData = oDateSpecificData,
                period,
                ageOfCompany,
                ageOfLevel

            };

            return oResult;
        }

        [HttpPost]
        public PersonalData INFOTYPE0002And0182Get([FromBody] RequestParameter oRequestParameter)
        {
            PersonalData oPersonalData = new PersonalData();
            DateTime oBeginDate = !oRequestParameter.InputParameter.ContainsKey("BeginDate") ? DateTime.MinValue : DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            Dictionary<string, object> oTemp = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0002And0182Get(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate);
            string oINF2Str = JSon.Serialize(oTemp["INFOTYPE0002"]);
            List<INFOTYPE0002> oINF2 = JSon.Deserialize<List<INFOTYPE0002>>(oINF2Str);
            string oINF182Str = JSon.Serialize(oTemp["INFOTYPE0182"]);
            List<INFOTYPE0182> oINF182 = JSon.Deserialize<List<INFOTYPE0182>>(oINF182Str);
            int i = 0;
            foreach (INFOTYPE0002 row in oINF2)
            {
                oPersonalData = new PersonalData
                {
                    EmployeeID = row.EmployeeID,
                    BeginDate = row.BeginDate,
                    EndDate = row.EndDate,
                    TitleID = row.TitleID,
                    FirstName = row.FirstName,
                    LastName = row.LastName,
                    NickName = row.NickName,
                    FullNameEn = oINF182[i].AlternateName,
                    DOB = row.DOB,
                    Gender = row.Gender,
                    Nationality = row.Nationality,
                    MaritalStatus = row.MaritalStatus,
                    Religion = row.Religion,
                    Language = row.Language
                };
                i++;
            }

            return oPersonalData;
        }

        [HttpPost]
        public OrganizationData INFOTYPE0001Get([FromBody] RequestParameter oRequestParameter)
        {
            DateTime oBeginDate = !oRequestParameter.InputParameter.ContainsKey("BeginDate") ? DateTime.Now : DateTime.Parse(oRequestParameter.InputParameter["BeginDate"].ToString(),null);
            INFOTYPE0001 oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).INFOTYPE0001Get(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate, oRequestParameter.CurrentEmployee.Language);
            OrganizationData oResult = Convert<OrganizationData>.ObjectFrom(oINF1);

            if (!EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ValidateActiveEmployeeForTravel(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate))
                throw new Exception("NOT_TRAVEL");

            return oResult;
        }

        [HttpPost]
        public ContactData INFOTYPE0105Get([FromBody] RequestParameter oRequestParameter)
        {
            DateTime oBeginDate = !oRequestParameter.InputParameter.ContainsKey("BeginDate") ? DateTime.Now : DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            INFOTYPE0105 oINF105 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0105Get(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate, oRequestParameter.InputParameter["SubType"].ToString());
            List<INFOTYPE0001> oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0001GetAllHistory(oRequestParameter.InputParameter["EmployeeID"].ToString(), oRequestParameter.CurrentEmployee.Language);
            ContactData oResult = Convert<ContactData>.ObjectFrom(oINF105);
            oResult.Name = oINF1[0].Name;
            return oResult;
        }

        //[HttpPost]
        private List<ObjectData> GetGenderCheckBoxData(EmployeeDataForMobile oCurrentEmployee)
        {
            string Language = oCurrentEmployee.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetGenderCheckBoxData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetGenderCheckBoxData();
        }

        //[HttpPost]
        private List<ObjectData> GetPrefixDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            string Language = oCurrentEmployee.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetPrefixDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetPrefixDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetMaritalStatusDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            string Language = oCurrentEmployee.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetMaritalStatusDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetMaritalStatusDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetNationalityDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oCurrentEmployee);

            string Language = oCurrentEmployee.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetNationalityDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetNationalityDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetLanguageDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oCurrentEmployee);

            string Language = oCurrentEmployee.Language;//string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetLanguageDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetLanguageDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetReligionDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oCurrentEmployee);

            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetReligionDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetReligionDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetEmpGroupDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oCurrentEmployee);

            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetEmpGroupDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetEmpGroupDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetEmpSubGroupDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oCurrentEmployee);

            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetEmpSubGroupDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetEmpSubGroupDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetSubTypeDropdownData(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oCurrentEmployee);

            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetSubTypeDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
            //return EmployeeManagement.CreateInstance(oCurrentEmployee.CompanyCode).GetSubTypeDropdownData();
        }

        //[HttpPost]
        private List<ObjectData> GetCostCenterDropdownData(RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);
            DateTime BeginDate;
            if (oRequestParameter.InputParameter.Count != 0)
            {
                BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            }
            else
            {
                BeginDate = DateTime.Now;
            }
            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCostCenterDropdownData(BeginDate);
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
        }

        [HttpPost]
        public List<ObjectData> GetCostCenterByOrganize(RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);
            DateTime BeginDate;
            if (oRequestParameter.InputParameter.ContainsKey("BeginDate"))
            {
                BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            }
            else
            {
                BeginDate = DateTime.Now;
            }
            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCostCenterByOrganize(oRequestParameter.InputParameter["Organize"].ToString());
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
        }

        //[HttpPost]
        private List<ObjectData> GetAreaDropdownData(RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            List<ObjectData> oResult = new List<ObjectData>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAreaDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                oResult.Add(oData);
            }
            return oResult;
        }

        //[HttpPost]
        private Dictionary<string, List<ObjectData>> GetSubAreaDropdownData(RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);
            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            Dictionary<string, List<ObjectData>> oResult = new Dictionary<string, List<ObjectData>>();
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetSubAreaDropdownData();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                string area = oData.TextEn;
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                if (oResult.ContainsKey(area))
                {
                    oResult[area].Add(oData);
                }
                else
                {
                    oResult.Add(area, new List<ObjectData>() { oData });
                }
            }
            return oResult;
        }

        [HttpPost]
        public object GetPersonSelectData(RequestParameter oRequestParameter)
        {
            object oReturn = new
            {
                Gender = GetGenderCheckBoxData(oRequestParameter.CurrentEmployee),
                Prefix = GetPrefixDropdownData(oRequestParameter.CurrentEmployee),
                MaritalStatus = GetMaritalStatusDropdownData(oRequestParameter.CurrentEmployee),
                Nationality = GetNationalityDropdownData(oRequestParameter.CurrentEmployee),
                Language = GetLanguageDropdownData(oRequestParameter.CurrentEmployee),
                Religion = GetReligionDropdownData(oRequestParameter.CurrentEmployee),
            };
            return oReturn;
        }

        [HttpPost]
        public object GetOrganizationSelectData([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, List<ObjectData>> oAllObject = GetAllObject(oRequestParameter);
            object oReturn = new
            {
                EmpGroup = GetEmpGroupDropdownData(oRequestParameter.CurrentEmployee),
                EmpSubGroup = GetEmpSubGroupDropdownData(oRequestParameter.CurrentEmployee),
                CostCenter = GetCostCenterDropdownData(oRequestParameter),
                Position = oAllObject["POSITION"],
                Organize = oAllObject["ORGANIZE"],
                Area = GetAreaDropdownData(oRequestParameter),
                SubArea = GetSubAreaDropdownData(oRequestParameter)
            };
            return oReturn;
        }

        [HttpPost]
        public object GetContactSelectData(RequestParameter oRequestParameter)
        {
            object oReturn = new
            {
                SubType = GetSubTypeDropdownData(oRequestParameter.CurrentEmployee)
            };
            return oReturn;
        }

        [HttpPost]
        public Dictionary<string, bool> CheckStatus([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, bool> oResult = new Dictionary<string, bool>();
            oResult.Add("Duplicate", false);
            oResult.Add("CanInsert", false);
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            DateTime minDate = DateTime.Now;
            if (oRequestParameter.InputParameter["DataObjectType"].ToString() == "Person")
            {
                List<PersonalData> obj = INFOTYPE0002And0182GetAllHistory(oRequestParameter);
                if (obj.Count == 0)
                {
                    oResult["CanInsert"] = true;
                    oResult["Duplicate"] = false;
                }
                else
                {
                    foreach (var row in obj)
                    {
                        if (DateBetween(row.BeginDate, row.EndDate, BeginDate)
                            || DateBetween(row.BeginDate, row.EndDate, EndDate)
                            || DateBetween(BeginDate, EndDate, row.BeginDate)
                            || DateBetween(BeginDate, EndDate, row.EndDate))
                        {
                            oResult["Duplicate"] = true;
                            oResult["CanInsert"] = true;
                            break;
                        }
                        else if (row.BeginDate.AddDays(-2) < EndDate) { oResult["CanInsert"] = true; }
                    }
                }
            }
            else if (oRequestParameter.InputParameter["DataObjectType"].ToString() == "Org")
            {
                List<OrganizationData> obj = INFOTYPE0001GetAllHistory(oRequestParameter);
                if (obj.Count == 0)
                {
                    oResult["CanInsert"] = true;
                    oResult["Duplicate"] = false;
                }
                else
                {
                    foreach (var row in obj)
                    {
                        if (DateBetween(row.BeginDate, row.EndDate, BeginDate)
                            || DateBetween(row.BeginDate, row.EndDate, EndDate)
                            || DateBetween(BeginDate, EndDate, row.BeginDate)
                            || DateBetween(BeginDate, EndDate, row.EndDate))
                        {
                            oResult["Duplicate"] = true;
                            oResult["CanInsert"] = true;
                            break;
                        }
                        else if (row.BeginDate.AddDays(-2) < BeginDate) { oResult["CanInsert"] = true; }
                    }
                }
            }
            else if (oRequestParameter.InputParameter["DataObjectType"].ToString() == "Contact")
            {
                List<ContactData> obj = INFOTYPE0105GetAllHistory(oRequestParameter);
                if (obj.Count == 0)
                {
                    oResult["CanInsert"] = true;
                    oResult["Duplicate"] = false;
                }
                else
                {
                    foreach (var row in obj)
                    {
                        if ((DateBetween(row.BeginDate, row.EndDate, BeginDate)
                            || DateBetween(row.BeginDate, row.EndDate, EndDate)
                            || DateBetween(BeginDate, EndDate, row.BeginDate)
                            || DateBetween(BeginDate, EndDate, row.EndDate)) && row.CategoryCode == oRequestParameter.InputParameter["CategoryCode"].ToString())
                        {
                            oResult["Duplicate"] = true;
                            oResult["CanInsert"] = true;
                            break;
                        }
                        else if (row.BeginDate.AddDays(-2) < BeginDate) { oResult["CanInsert"] = true; }
                    }
                }
            }
            else if (oRequestParameter.InputParameter["DataObjectType"].ToString() == "Object")
            {
                List<ObjectData> obj = INFOTYPE1000GetAllHistory(oRequestParameter);
                int count = 0;
                if (obj.Count == 0)
                {
                    oResult["CanInsert"] = true;
                    oResult["Duplicate"] = false;
                }
                else
                {
                    foreach (var row in obj)
                    {
                        if (row.ObjectType == oRequestParameter.InputParameter["ObjectType"].ToString() && row.ObjectID == oRequestParameter.InputParameter["ObjectID"].ToString())
                        count = 1;
                        if (DateBetween(row.BeginDate, row.EndDate, BeginDate)
                            || DateBetween(row.BeginDate, row.EndDate, EndDate)
                            || DateBetween(BeginDate, EndDate, row.BeginDate)
                            || DateBetween(BeginDate, EndDate, row.EndDate))
                        {
                            oResult["Duplicate"] = true;
                            oResult["CanInsert"] = true;
                            break;
                        }
                        else if (row.BeginDate.AddDays(-2) < BeginDate) { oResult["CanInsert"] = true; }
                    }
                }
            }
            else if (oRequestParameter.InputParameter["DataObjectType"].ToString() == "Relation")
            {
                List<RelationData> obj = INFOTYPE1001GetAllHistory(oRequestParameter);
                RelationData objParam = JSon.Deserialize<RelationData>(oRequestParameter.InputParameter["RelationData"].ToString());
                int count = 0;
                if (obj.Count == 0)
                {
                    oResult["CanInsert"] = true;
                    oResult["Duplicate"] = false;
                }
                else
                {
                    foreach (var row in obj)
                    {
                        if (row.ObjectType == objParam.ObjectType && row.ObjectID == objParam.ObjectID && row.Relation == objParam.Relation
                            && row.NextObjectType == objParam.NextObjectType && row.NextObjectID == objParam.NextObjectID)
                        {
                            count = 1;
                            if (DateBetween(row.BeginDate, row.EndDate, BeginDate)
                                || DateBetween(row.BeginDate, row.EndDate, EndDate)
                                || DateBetween(BeginDate, EndDate, row.BeginDate)
                                || DateBetween(BeginDate, EndDate, row.EndDate))
                            {
                                oResult["Duplicate"] = true;
                                oResult["CanInsert"] = true;
                                break;
                            }
                            else if (row.BeginDate.AddDays(-2) < BeginDate) { oResult["CanInsert"] = true; }
                        }
                        if(objParam.ObjectType == "POSITION" && objParam.NextObjectType == "EMPLOYEE" && objParam.PercentValue == 100M
                                    && row.PercentValue == 100M && row.ObjectType == "POSITION" && row.NextObjectType == "EMPLOYEE" && row.NextObjectID.Split(':')[0] == objParam.NextObjectID) 
                            {
                                count = 1;
                                if (DateBetween(row.BeginDate, row.EndDate, BeginDate)
                                    || DateBetween(row.BeginDate, row.EndDate, EndDate)
                                    || DateBetween(BeginDate, EndDate, row.BeginDate)
                                    || DateBetween(BeginDate, EndDate, row.EndDate))
                                {
                                    oResult["Duplicate"] = true;
                                    oResult["CanInsert"] = false;
                                    break;
                                }
                                else
                                {
                                    oResult["Duplicate"] = false;
                                    oResult["CanInsert"] = true;
                                }
                            }
                    }
                    if (count == 0)
                    {
                        oResult["CanInsert"] = true;
                        oResult["Duplicate"] = false;
                    }
                }
            }
            else if (oRequestParameter.InputParameter["DataObjectType"].ToString() == "Position")
            {
                List<PositionLevelData> obj = INFOTYPE1013GetAllHistory(oRequestParameter);
                int count = 0;
                if (obj.Count == 0)
                {
                    oResult["CanInsert"] = true;
                    oResult["Duplicate"] = false;
                }
                else
                {
                    foreach (var row in obj)
                    {
                        if (row.ObjectID == oRequestParameter.InputParameter["ObjectID"].ToString())
                        {
                            count = 1;
                            if (DateBetween(row.BeginDate, row.EndDate, BeginDate)
                                || DateBetween(row.BeginDate, row.EndDate, EndDate)
                                || DateBetween(BeginDate, EndDate, row.BeginDate)
                                || DateBetween(BeginDate, EndDate, row.EndDate))
                            {
                                oResult["Duplicate"] = true;
                                oResult["CanInsert"] = true;
                                break;
                            }
                            else if (row.BeginDate.AddDays(-2) < BeginDate) { oResult["CanInsert"] = true; }
                        }
                    }
                }
                if (count == 0)
                {
                    oResult["CanInsert"] = true;
                    oResult["Duplicate"] = false;
                }
            }
            return oResult;
        }

        private bool DateBetween(DateTime BeginDate, DateTime EndDate, DateTime CheckDate)
        {
            if (CheckDate >= BeginDate && CheckDate <= EndDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void UpdateINFOTYPE0002And0182([FromBody] RequestParameter oRequestParameter)
        {
            PersonalData oPers = JSon.Deserialize<PersonalData>(oRequestParameter.InputParameter["PersonData"].ToString());
            INFOTYPE0002 oINF2 = new INFOTYPE0002
            {
                EmployeeID = oPers.EmployeeID,
                BeginDate = oPers.BeginDate,
                EndDate = oPers.EndDate,
                TitleID = oPers.TitleID,
                FirstName = oPers.FirstName,
                LastName = oPers.LastName,
                NickName = oPers.NickName,
                DOB = oPers.DOB,
                Gender = oPers.Gender,
                MaritalStatus = oPers.MaritalStatus,
                Nationality = oPers.Nationality,
                Language = oPers.Language,
                Religion = oPers.Religion
            };
            INFOTYPE0182 oINF182 = new INFOTYPE0182 { EmployeeID = oPers.EmployeeID, BeginDate = oPers.BeginDate, EndDate = oPers.EndDate, AlternateName = oPers.FullNameEn };
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).UpdateINFOTYPE0002And0182(oINF2, oINF182);
        }

        public void InsertINFOTYPE0001([FromBody] RequestParameter oRequestParameter)
        {
            OrganizationData oOrgData = JSon.Deserialize<OrganizationData>(oRequestParameter.InputParameter["OrgData"].ToString());
            INFOTYPE0001 oINF1 = Convert<INFOTYPE0001>.ObjectFrom(oOrgData);
            oINF1.CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPE0001(oINF1);
        }

        public void UpdateINFOTYPE0001([FromBody] RequestParameter oRequestParameter)
        {
            OrganizationData oOrgData = JSon.Deserialize<OrganizationData>(oRequestParameter.InputParameter["OrgData"].ToString());
            INFOTYPE0001 oINF1 = Convert<INFOTYPE0001>.ObjectFrom(oOrgData);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).UpdateINFOTYPE0001(oINF1);
        }

        public void InsertINFOTYPE0105([FromBody] RequestParameter oRequestParameter)
        {
            ContactData oContData = JSon.Deserialize<ContactData>(oRequestParameter.InputParameter["ContData"].ToString());
            INFOTYPE0105 oINF105 = new INFOTYPE0105
            {
                EmployeeID = oContData.EmployeeID,
                BeginDate = oContData.BeginDate,
                EndDate = oContData.EndDate,
                CategoryCode = oContData.CategoryCode,
                DataText = oContData.DataText
            };
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPE0105(oINF105);
        }

        public void UpdateINFOTYPE0105([FromBody] RequestParameter oRequestParameter)
        {
            ContactData oContData = JSon.Deserialize<ContactData>(oRequestParameter.InputParameter["ContData"].ToString());
            INFOTYPE0105 oINF105 = new INFOTYPE0105
            {
                EmployeeID = oContData.EmployeeID,
                BeginDate = oContData.BeginDate,
                EndDate = oContData.EndDate,
                CategoryCode = oContData.CategoryCode,
                DataText = oContData.DataText
            };
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).UpdateINFOTYPE0105(oINF105);
        }

        public string InsertINFOTYPE0002And0182([FromBody] RequestParameter oRequestParameter)
        {
            PersonalData oPers = JSon.Deserialize<PersonalData>(oRequestParameter.InputParameter["PersonData"].ToString());
            INFOTYPE0002 oINF2 = new INFOTYPE0002
            {
                EmployeeID = oPers.EmployeeID,
                BeginDate = oPers.BeginDate,
                EndDate = oPers.EndDate,
                TitleID = oPers.TitleID,
                FirstName = oPers.FirstName,
                LastName = oPers.LastName,
                NickName = oPers.NickName,
                DOB = oPers.DOB,
                Gender = oPers.Gender,
                MaritalStatus = oPers.MaritalStatus,
                Nationality = oPers.Nationality,
                Language = oPers.Language,
                Religion = oPers.Religion
            };
            INFOTYPE0182 oINF182 = new INFOTYPE0182 { EmployeeID = oPers.EmployeeID, BeginDate = oPers.BeginDate, EndDate = oPers.EndDate, AlternateName = oPers.FullNameEn };
            return EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPE0002And0182(oINF2, oINF182);
        }

        public string InsertINFOTYPE0002And0182And0001And0105Data([FromBody] RequestParameter oRequestParameter)
        {
            PersonalData oPers = JSon.Deserialize<PersonalData>(oRequestParameter.InputParameter["PersonData"].ToString());
            INFOTYPE0002 oINF2 = new INFOTYPE0002
            {
                EmployeeID = oPers.EmployeeID,
                BeginDate = oPers.BeginDate,
                EndDate = oPers.EndDate,
                TitleID = oPers.TitleID,
                FirstName = oPers.FirstName,
                LastName = oPers.LastName,
                NickName = oPers.NickName,
                DOB = oPers.DOB,
                Gender = oPers.Gender,
                MaritalStatus = oPers.MaritalStatus,
                Nationality = oPers.Nationality,
                Language = oPers.Language,
                Religion = oPers.Religion
            };
            INFOTYPE0182 oINF182 = new INFOTYPE0182 { EmployeeID = oPers.EmployeeID, BeginDate = oPers.BeginDate, EndDate = oPers.EndDate, AlternateName = oPers.FullNameEn };
            OrganizationData oOrgData = JSon.Deserialize<OrganizationData>(oRequestParameter.InputParameter["OrgData"].ToString());
            if (string.IsNullOrEmpty(oOrgData.CompanyCode)) oOrgData.CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            INFOTYPE0001 oINF1 = Convert<INFOTYPE0001>.ObjectFrom(oOrgData);
            oINF1.CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            ContactData oContData = JSon.Deserialize<ContactData>(oRequestParameter.InputParameter["ContData"].ToString());
            INFOTYPE0105 oINF105 = new INFOTYPE0105
            {
                EmployeeID = oContData.EmployeeID,
                BeginDate = oContData.BeginDate,
                EndDate = oContData.EndDate,
                CategoryCode = oContData.CategoryCode,
                DataText = oContData.DataText
            };
            return EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPE0002And0182And0001And0105Data(oINF2, oINF182, oINF1, oINF105);
        }
        #endregion

        #region " OM "

        #region " Object "
        [HttpPost]
        public List<ObjectData> INFOTYPE1000GetAllHistory([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            List<ObjectData> oResult = new List<ObjectData>();
            ObjectData item;
            List<INFOTYPE1000> oINF1000 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1000GetAllHistory();
            foreach (INFOTYPE1000 row in oINF1000)
            {
                item = Convert<ObjectData>.ObjectFrom(row);
                item.AlternativeName = row.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                item.AlternativeShortName = row.AlternativeShortName(WorkflowPrinciple.Current.UserSetting.Language);
                oResult.Add(item);
            }
            return oResult;
        }

        public ObjectData INFOTYPE1000Get([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            DateTime oBeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime oEndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string oObjectType = Convert2ObjectTypeOneBit(oRequestParameter.InputParameter["ObjectType"].ToString());
            string oObjectID = oRequestParameter.InputParameter["ObjectID"].ToString();
            INFOTYPE1000 oINF1000 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1000Get(oObjectType, oObjectID, oBeginDate, oEndDate);
            ObjectData oResult = Convert<ObjectData>.ObjectFrom(oINF1000);

            return oResult;
        }

        [HttpPost]
        public INFOTYPE1000 INFOTYPE1000GetPosition([FromBody] RequestParameter oRequestParameter)
        {
            INFOTYPE1000 oResult = new INFOTYPE1000();
            oResult = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetObjectData(ObjectType.S, oRequestParameter.InputParameter["ObjectID"].ToString(),DateTime.Now, oRequestParameter.CurrentEmployee.Language);
            return oResult;
        }

        [HttpPost]
        public INFOTYPE1000 INFOTYPE1000GetOrgUnit([FromBody] RequestParameter oRequestParameter)
        {
            INFOTYPE1000 oResult = new INFOTYPE1000();
            oResult = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetObjectData(ObjectType.O, oRequestParameter.InputParameter["ObjectID"].ToString(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);
            return oResult;
        }

        [HttpPost]
        public string GetImageUrl([FromBody] RequestParameter oRequestParameter)
        {
            string sImgURL = string.Empty;
            sImgURL = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).PHOTO_PATH, oRequestParameter.InputParameter["EmployeeID"].ToString());
            return sImgURL;
        }

        public ObjectType Convert2ObjectType(string ObjectTypeString)
        {
            ObjectType oReturn;
            switch (ObjectTypeString)
            {
                case "P":
                    oReturn = ObjectType.P;
                    break;
                case "S":
                    oReturn = ObjectType.S;
                    break;
                case "O":
                    oReturn = ObjectType.O;
                    break;
                case "C":
                    oReturn = ObjectType.C;
                    break;
                case "K":
                    oReturn = ObjectType.K;
                    break;
                case "Q":
                    oReturn = ObjectType.Q;
                    break;
                default:
                    oReturn = ObjectType.QK;
                    break;
            }
            return oReturn;
        }

        public string Convert2ObjectTypeString(string ObjectType)
        {
            string oReturn = "";
            switch (ObjectType)
            {
                case "P":
                    oReturn = "EMPLOYEE";
                    break;
                case "S":
                    oReturn = "POSITION";
                    break;
                case "O":
                    oReturn = "ORGANIZE";
                    break;
                case "C":
                    oReturn = "JOB";
                    break;
                case "K":
                    oReturn = "COSTCENTER";
                    break;
                case "Q":
                    oReturn = "QUALIFICATION";
                    break;
                default:
                    oReturn = ObjectType;
                    break;
            }
            return oReturn;
        }

        public string Convert2ObjectTypeOneBit(string ObjectType)
        {
            string oReturn = "";
            switch (ObjectType)
            {
                case "EMPLOYEE":
                    oReturn = "P";
                    break;
                case "POSITION":
                    oReturn = "S";
                    break;
                case "ORGANIZE":
                    oReturn = "O";
                    break;
                case "JOB":
                    oReturn = "C";
                    break;
                case "COSTCENTER":
                    oReturn = "K";
                    break;
                case "QUALIFICATION":
                    oReturn = "Q";
                    break;
                default:
                    oReturn = ObjectType;
                    break;
            }
            return oReturn;
        }

        [HttpPost]
        public void DeleteINFOTYPE1000([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DeleteINFOTYPE1000(oRequestParameter.InputParameter["ObjectType"].ToString(), oRequestParameter.InputParameter["ObjectID"].ToString(), BeginDate, EndDate);
        }
        [HttpPost]
        public void InsertINFOTYPE1000([FromBody] RequestParameter oRequestParameter)
        {
            ObjectData oObjData = JSon.Deserialize<ObjectData>(oRequestParameter.InputParameter["ObjectData"].ToString());
            INFOTYPE1000 oINF1000 = Convert<INFOTYPE1000>.ObjectFrom(oObjData);
            Dictionary<string, ObjectType> ObjectTypeDic = new Dictionary<string, ObjectType>();
            ObjectTypeDic.Add("POSITION", ObjectType.S);
            ObjectTypeDic.Add("ORGANIZE", ObjectType.O);
            ObjectTypeDic.Add("EMPLOYEE", ObjectType.P);
            ObjectTypeDic.Add("COSTCENTER", ObjectType.K);
            ObjectTypeDic.Add("JOB",ObjectType.C);
            ObjectTypeDic.Add("QUALIFICATION", ObjectType.Q);
            oINF1000.ObjectType = ObjectTypeDic[oObjData.ObjectType];
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPE1000(oINF1000);
        }
        [HttpPost]
        public void UpdateINFOTYPE1000([FromBody] RequestParameter oRequestParameter)
        {
            ObjectData oObjData = JSon.Deserialize<ObjectData>(oRequestParameter.InputParameter["ObjectData"].ToString());
            INFOTYPE1000 oINF1000 = Convert<INFOTYPE1000>.ObjectFrom(oObjData);
            Dictionary<string, ObjectType> ObjectTypeDic = new Dictionary<string, ObjectType>();
            ObjectTypeDic.Add("POSITION", ObjectType.S);
            ObjectTypeDic.Add("ORGANIZE", ObjectType.O);
            ObjectTypeDic.Add("EMPLOYEE", ObjectType.P);
            ObjectTypeDic.Add("COSTCENTER", ObjectType.K);
            ObjectTypeDic.Add("JOB",ObjectType.C);
            ObjectTypeDic.Add("QUALIFICATION", ObjectType.Q);
            oINF1000.ObjectType = ObjectTypeDic[oObjData.ObjectType];
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).UpdateINFOTYPE1000(oINF1000);
        }
        #endregion

        #region " Relation "
        [HttpPost]
        public List<RelationData> INFOTYPE1001GetAllHistory([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            List<RelationData> oResult = new List<RelationData>();
            List<INFOTYPE1001> oINF1001 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1001GetAllHistory();
            foreach (INFOTYPE1001 row in oINF1001)
            {
                oResult.Add(Convert<RelationData>.ObjectFrom(row));
            }
            return oResult;
        }

        [HttpPost]
        public List<RelationData> GetPositionForPerson([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            List<RelationData> oResult = new List<RelationData>();
            string oNextObjectID = oRequestParameter.InputParameter["NextObjectID"].ToString();
            List<INFOTYPE1001> oINF1001 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPositionForPerson(oNextObjectID);
            foreach (INFOTYPE1001 row in oINF1001)
            {
                oResult.Add(Convert<RelationData>.ObjectFrom(row));
            }
            return oResult;
        }

        [HttpPost]
        public List<RelationData> GetBelongToRelation([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            List<RelationData> oResult = new List<RelationData>();
            List<INFOTYPE1001> oINF1001 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetBelongToRelation();
            foreach (INFOTYPE1001 row in oINF1001)
            {
                oResult.Add(Convert<RelationData>.ObjectFrom(row));
            }
            return oResult;
        }

        public RelationData INFOTYPE1001Get([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            DateTime oBeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime oEndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string oObjectType = Convert2ObjectTypeOneBit(oRequestParameter.InputParameter["ObjectType"].ToString());
            string oObjectID = oRequestParameter.InputParameter["ObjectID"].ToString();
            string oNextObjectType = Convert2ObjectTypeOneBit(oRequestParameter.InputParameter["NextObjectType"].ToString());
            string oNextObjectID = oRequestParameter.InputParameter["NextObjectID"].ToString();
            string oRelation = oRequestParameter.InputParameter["Relation"].ToString();
            INFOTYPE1001 oINF1001 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1001Get(oObjectType, oObjectID, oNextObjectType, oNextObjectID, oRelation, oBeginDate, oEndDate);
            RelationData oResult = Convert<RelationData>.ObjectFrom(oINF1001);

            return oResult;
        }

        [HttpPost]
        public void DeleteINFOTYPE1001([FromBody] RequestParameter oRequestParameter)
        {
            RelationData oRelaData = JSon.Deserialize<RelationData>(oRequestParameter.InputParameter["RelationData"].ToString());
            List<INFOTYPE1000> oRelation = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetRelationSelectData();
            oRelaData.ObjectID = oRelaData.ObjectID.Split((':'))[0];
            oRelaData.NextObjectID = oRelaData.NextObjectID.Split((':'))[0];
            foreach (var row in oRelation)
            {
                if (row.ShortText == oRelaData.Relation)
                {
                    oRelaData.Relation = row.ObjectID;
                }
            }
            INFOTYPE1001 oINF1001 = Convert<INFOTYPE1001>.ObjectFrom(oRelaData);
            //oINF1001.ObjectType = oRelaData.ObjectType.ToString() == "ORGANIZE" ? ObjectType.O : oRelaData.ObjectType.ToString() == "POSITION" ? ObjectType.S : oRelaData.ObjectType.ToString() == "EMPLOYEE" ? ObjectType.P : ObjectType.K;
            //oINF1001.NextObjectType = oRelaData.NextObjectType.ToString() == "ORGANIZE" ? ObjectType.O : oRelaData.NextObjectType.ToString() == "POSITION" ? ObjectType.S : oRelaData.NextObjectType.ToString() == "EMPLOYEE" ? ObjectType.P : ObjectType.K;
            oINF1001.ObjectType = Convert2ObjectType(oRelaData.ObjectType.ToString());
            oINF1001.NextObjectType = Convert2ObjectType(oRelaData.NextObjectType.ToString());
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DeleteINFOTYPE1001(oINF1001);
        }

        [HttpPost]
        public void InsertINFOTYPE1001([FromBody] RequestParameter oRequestParameter)
        {
            RelationData oRelaData = JSon.Deserialize<RelationData>(oRequestParameter.InputParameter["RelationData"].ToString());
            RelationData oOldRelaData = oRequestParameter.InputParameter.ContainsKey("OldRelationData")?JSon.Deserialize<RelationData>(oRequestParameter.InputParameter["OldRelationData"].ToString()) : new RelationData();
            INFOTYPE1001 oINF1001 = Convert<INFOTYPE1001>.ObjectFrom(oRelaData);
            INFOTYPE1001 oOldINF1001 = Convert<INFOTYPE1001>.ObjectFrom(oOldRelaData);
            Dictionary<string, ObjectType> ObjectTypeDic = new Dictionary<string, ObjectType>();
            ObjectTypeDic.Add("POSITION", ObjectType.S);
            ObjectTypeDic.Add("ORGANIZE", ObjectType.O);
            ObjectTypeDic.Add("EMPLOYEE", ObjectType.P);
            ObjectTypeDic.Add("COSTCENTER", ObjectType.K);
            ObjectTypeDic.Add("JOB",ObjectType.C);
            ObjectTypeDic.Add("QUALIFICATION", ObjectType.Q);
            oINF1001.ObjectType = ObjectTypeDic[oRelaData.ObjectType];
            oINF1001.NextObjectType = ObjectTypeDic[oRelaData.NextObjectType];
            if (oOldINF1001.BeginDate != DateTime.MinValue) oOldINF1001.ObjectType = ObjectTypeDic[oOldRelaData.ObjectType];
            if (oOldINF1001.BeginDate != DateTime.MinValue) oOldINF1001.NextObjectType = ObjectTypeDic[oOldRelaData.NextObjectType];
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPE1001(oINF1001, oOldINF1001);
        }
        [HttpPost]
        public void UpdateINFOTYPE1001([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController oWorkflowController = new WorkflowController();
            oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);

            RelationData oRelaData = JSon.Deserialize<RelationData>(oRequestParameter.InputParameter["RelationData"].ToString());
            INFOTYPE1001 oINF1001 = Convert<INFOTYPE1001>.ObjectFrom(oRelaData);
            Dictionary<string, ObjectType> ObjectTypeDic = new Dictionary<string, ObjectType>();
            ObjectTypeDic.Add("POSITION", ObjectType.S);
            ObjectTypeDic.Add("ORGANIZE", ObjectType.O);
            ObjectTypeDic.Add("EMPLOYEE", ObjectType.P);
            ObjectTypeDic.Add("COSTCENTER", ObjectType.K);
            ObjectTypeDic.Add("JOB",ObjectType.C);
            ObjectTypeDic.Add("QUALIFICATION", ObjectType.Q);
            //oINF1001.ObjectType = (ObjectType)Enum.ToObject(typeof(ObjectType), oRelaData.ObjectType.Trim());
            //oINF1001.NextObjectType = (ObjectType)Enum.ToObject(typeof(ObjectType), oRelaData.NextObjectType.Trim());
            oINF1001.ObjectType = ObjectTypeDic[oRelaData.ObjectType];
            oINF1001.NextObjectType = ObjectTypeDic[oRelaData.NextObjectType];
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).UpdateINFOTYPE1001(oINF1001);
        }

        [HttpPost]
        public object GetRelationSelectData([FromBody] RequestParameter oRequestParameter)
        {
            List<INFOTYPE1000> oRelation = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetRelationSelectData();
            List<ObjectData> Relation = new List<ObjectData>();
            foreach (var row in oRelation)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(oRequestParameter.CurrentEmployee.Language);
                oData.AlternativeShortName = row.AlternativeShortName(oRequestParameter.CurrentEmployee.Language);
                Relation.Add(oData);
            }
            List<INFOTYPE1000> oRV = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetRelationValidationSelectData();
            Dictionary<string, Dictionary<string, List<string>>> oRelationValidation = new Dictionary<string, Dictionary<string, List<string>>>();
            foreach (var row in oRV)
            {
                if (oRelationValidation.ContainsKey(row.ObjectID.Substring(0, 4)))
                {
                    oRelationValidation[row.ObjectID.Substring(0, 4)]["Object"].Add(Convert2ObjectTypeString(row.ObjectID.Substring(4, 1)));
                    oRelationValidation[row.ObjectID.Substring(0, 4)]["NextObject"].Add(Convert2ObjectTypeString(row.ObjectID.Substring(5, 1)));
                }
                else
                {
                    Dictionary<string, List<string>> temp = new Dictionary<string, List<string>>();
                    temp.Add("Object", new List<string>() { Convert2ObjectTypeString(row.ObjectID.Substring(4, 1)) });
                    temp.Add("NextObject", new List<string>() { Convert2ObjectTypeString(row.ObjectID.Substring(5, 1)) });
                    oRelationValidation.Add(row.ObjectID.Substring(0, 4), temp);
                }

            }

            object oReturn = new
            {
                Relation = Relation,
                RelationValidation = oRelationValidation,
                AllObject = GetAllObject(oRequestParameter)
            };
            return oReturn;
        }


        [HttpPost]
        public List<ObjectData> GetRelation([FromBody] RequestParameter oRequestParameter)
        {
            List<ObjectData> oData = new List<ObjectData>();
            List<INFOTYPE1000> oPosition = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetRelationSelectData();
            foreach (INFOTYPE1000 row in oPosition)
            {
                oData.Add(Convert<ObjectData>.ObjectFrom(row));
            }
            return oData;
        }
        [HttpPost]
        public Dictionary<string,List<string>> GetRelationValidation([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, List<string>> temp = new Dictionary<string, List<string>>();
            List<string> ObjectType = new List<string>();
            List<string> NextObjectType = new List<string>();
            List<string> oRelationValidation = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetRelationValidationByRelation(oRequestParameter.InputParameter["Relation"].ToString());
            foreach (string row in oRelationValidation)
            {
                ObjectType.Add(Convert2ObjectTypeString(row.Split(',')[0]));
                NextObjectType.Add(Convert2ObjectTypeString(row.Split(',')[1]));
            }
            temp.Add("ObjectType", ObjectType);
            temp.Add("NextObjectType", NextObjectType);
            return temp;
        }
        [HttpPost]
        public Dictionary<string, List<ObjectData>> GetAllObject([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);
            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            DateTime BeginDate = DateTime.Now;
            if (oRequestParameter.InputParameter.ContainsKey("BeginDate"))
            {
                BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            }
            Dictionary<string, List<ObjectData>> oResult = new Dictionary<string, List<ObjectData>>();
            List<ObjectData> oResultO = new List<ObjectData>();
            List<ObjectData> oResultS = new List<ObjectData>();
            List<ObjectData> oResultP = new List<ObjectData>();
            List<ObjectData> oResultK = new List<ObjectData>();
            List<ObjectData> oResultC = new List<ObjectData>(); 
            List<ObjectData> oResultQ = new List<ObjectData>();
            List<ObjectType> oObjTpe = new List<ObjectType>() { ObjectType.P, ObjectType.S, ObjectType.O, ObjectType.K };
            List<INFOTYPE1000> oINF1000 = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllObject(oObjTpe, BeginDate);
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                if (string.IsNullOrEmpty(oData.AlternativeName)) oData.AlternativeName = oData.AlternativeShortName;
                if (string.IsNullOrEmpty(oData.AlternativeShortName)) oData.AlternativeShortName = oData.AlternativeName;
                if (row.ObjectType == ObjectType.O)
                {
                    oResultO.Add(oData);
                }
                else if (row.ObjectType == ObjectType.S)
                {
                    oResultS.Add(oData);
                }
                else if (row.ObjectType == ObjectType.P)
                {
                    oResultP.Add(oData);
                }
                else if (row.ObjectType == ObjectType.K) { oResultK.Add(oData); }
                else if (row.ObjectType ==ObjectType.C) { oResultC.Add(oData); }
                else if (row.ObjectType == ObjectType.Q) { oResultQ.Add(oData); }
            }
            oResult.Add("ORGANIZE", oResultO);
            oResult.Add("POSITION", oResultS);
            oResult.Add("EMPLOYEE", oResultP);
            oResult.Add("COSTCENTER", oResultK);
            oResult.Add("JOB", oResultC);
            oResult.Add("QUALIFICATION", oResultQ);
            return oResult;
        }
        [HttpPost]
        public List<ObjectData> GetPositionByOrganize([FromBody] RequestParameter oRequestParameter)
        {
            List<ObjectData> oData = new List<ObjectData>();
            List<INFOTYPE1000> oPosition = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPositionByOrganize(oRequestParameter.InputParameter["Organize"].ToString(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);
            foreach (INFOTYPE1000 row in oPosition)
            {
                ObjectData temp = Convert<ObjectData>.ObjectFrom(row);
                temp.AlternativeName = row.AlternativeName(oRequestParameter.CurrentEmployee.Language);
                temp.AlternativeShortName = row.AlternativeShortName(oRequestParameter.CurrentEmployee.Language);
                if (string.IsNullOrEmpty(temp.AlternativeName)) temp.AlternativeName = temp.AlternativeShortName;
                if (string.IsNullOrEmpty(temp.AlternativeShortName)) temp.AlternativeShortName = temp.AlternativeName;
                oData.Add(temp);
            }
            return oData;
        }

        [HttpPost]
        public List<ObjectData> GetPositionByOrganizeForHRMaster([FromBody] RequestParameter oRequestParameter)
        {
            List<ObjectData> oData = new List<ObjectData>();
            List<INFOTYPE1000> oPosition = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPositionByOrganizeForHRMaster(oRequestParameter.InputParameter["Organize"].ToString(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);
            foreach (INFOTYPE1000 row in oPosition)
            {
                ObjectData temp = Convert<ObjectData>.ObjectFrom(row);
                temp.AlternativeName = row.AlternativeName(oRequestParameter.CurrentEmployee.Language);
                temp.AlternativeShortName = row.AlternativeShortName(oRequestParameter.CurrentEmployee.Language);
                if (string.IsNullOrEmpty(temp.AlternativeName)) temp.AlternativeName = temp.AlternativeShortName;
                if (string.IsNullOrEmpty(temp.AlternativeShortName)) temp.AlternativeShortName = temp.AlternativeName;
                oData.Add(temp);
            }
            return oData;
        }

        [HttpPost]
        public List<ObjectData> GetAllPositionForHRMaster([FromBody] RequestParameter oRequestParameter)
        {
            List<ObjectData> oData = new List<ObjectData>();
            List<INFOTYPE1000> oPosition = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllPositionForHrMaster(DateTime.Now, oRequestParameter.CurrentEmployee.Language);
            foreach (INFOTYPE1000 row in oPosition)
            {
                ObjectData temp = Convert<ObjectData>.ObjectFrom(row);
                temp.AlternativeName = row.AlternativeName(oRequestParameter.CurrentEmployee.Language);
                temp.AlternativeShortName = row.AlternativeShortName(oRequestParameter.CurrentEmployee.Language);
                if (string.IsNullOrEmpty(temp.AlternativeName)) temp.AlternativeName = temp.AlternativeShortName;
                if (string.IsNullOrEmpty(temp.AlternativeShortName)) temp.AlternativeShortName = temp.AlternativeName;
                oData.Add(temp);
            }
            return oData;
        }
        #endregion

        #region " Position Level "
        [HttpPost]
        public List<PositionLevelData> INFOTYPE1013GetAllHistory([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            List<PositionLevelData> oResult = new List<PositionLevelData>();
            List<INFOTYPE1013> oINF1013 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1013GetAllHistory();
            foreach (INFOTYPE1013 row in oINF1013)
            {
                oResult.Add(Convert<PositionLevelData>.ObjectFrom(row));
            }
            return oResult;
        }
        [HttpPost]
        public PositionLevelData INFOTYPE1013Get([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            DateTime oBeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime oEndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string oObjectID = oRequestParameter.InputParameter["ObjectID"].ToString();
            INFOTYPE1013 oINF1013 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1013Get(oObjectID, oBeginDate, oEndDate);
            PositionLevelData oResult = Convert<PositionLevelData>.ObjectFrom(oINF1013);

            return oResult;
        }
        [HttpPost]
        public void DeleteINFOTYPE1013([FromBody] RequestParameter oRequestParameter)
        {
            PositionLevelData oPosData = JSon.Deserialize<PositionLevelData>(oRequestParameter.InputParameter["PositionData"].ToString());
            INFOTYPE1013 oINF1013 = Convert<INFOTYPE1013>.ObjectFrom(oPosData);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DeleteINFOTYPE1013(oINF1013);
        }
        [HttpPost]
        public void InsertINFOTYPE1013([FromBody] RequestParameter oRequestParameter)
        {
            PositionLevelData oPosData = JSon.Deserialize<PositionLevelData>(oRequestParameter.InputParameter["PositionData"].ToString());
            INFOTYPE1013 oINF1013 = Convert<INFOTYPE1013>.ObjectFrom(oPosData);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPE1013(oINF1013);
        }
        [HttpPost]
        public void UpdateINFOTYPE1013([FromBody] RequestParameter oRequestParameter)
        {
            PositionLevelData oPosData = JSon.Deserialize<PositionLevelData>(oRequestParameter.InputParameter["PositionData"].ToString());
            INFOTYPE1013 oINF1013 = Convert<INFOTYPE1013>.ObjectFrom(oPosData);
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).UpdateINFOTYPE1013(oINF1013);
        }

        [HttpPost]
        public List<ObjectData> GetPositionObject([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);
            string Language = WorkflowPrinciple.Current.UserSetting.Language;
            DateTime BeginDate;
            if (oRequestParameter.InputParameter.Count != 0)
            {
                BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            }
            else
            {
                BeginDate = DateTime.Now;
            }

            List<ObjectData> oResult = new List<ObjectData>();
            List<ObjectType> oObjTpe = new List<ObjectType>() { ObjectType.S };
            List<INFOTYPE1000> oINF1000 = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllPositionNoCheckDate(oRequestParameter.CurrentEmployee.Language);
            foreach (INFOTYPE1000 row in oINF1000)
            {
                ObjectData oData = Convert<ObjectData>.ObjectFrom(row);
                oData.AlternativeName = row.AlternativeName(Language);
                oData.AlternativeShortName = row.AlternativeShortName(Language);
                if (string.IsNullOrEmpty(oData.AlternativeName)) oData.AlternativeName = oData.AlternativeShortName;
                if (string.IsNullOrEmpty(oData.AlternativeShortName)) oData.AlternativeShortName = oData.AlternativeName;
                oResult.Add(oData);
            }
            return oResult;
        }

        [HttpPost]
        public object GetPositionDataForEdit([FromBody] RequestParameter oRequestParameter)
        {
            return new
            {
                PositionData = INFOTYPE1013Get(oRequestParameter),
                PositionSelector = GetPositionObject(oRequestParameter),
                EmpGroupSelector = GetEmpGroupDropdownData(oRequestParameter.CurrentEmployee)
            };
        }

        [HttpPost]
        public object GetPositionLevelObject([FromBody] RequestParameter oRequestParameter)
        {
            return new
            {
                PositionSelector = GetPositionObject(oRequestParameter),
                EmpGroupSelector = GetEmpGroupDropdownData(oRequestParameter.CurrentEmployee)
            };
        }
        #endregion
        #endregion

        [HttpPost]
        public DataTable GetOrganizationStructure([FromBody] RequestParameter oRequestParameter)
        {
            WorkflowController wfCtrl = new WorkflowController();
            wfCtrl.SetAuthenticate(oRequestParameter.CurrentEmployee);

            DataTable oDT = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetOrganizationStructure(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID);

            PersonalInformation oResult = new PersonalInformation();

            oDT.Columns.Add("OrgUnitName", typeof(System.String));
            oDT.Columns.Add("FirstNameEn", typeof(System.String));
            oDT.Columns.Add("LastNameEn", typeof(System.String));
            oDT.Columns.Add("OrgUnitNo", typeof(System.String));
            //oDT.Columns.Add("PositionEn", typeof(System.String));

            foreach (DataRow dr in oDT.Rows)
            {
                if (!string.IsNullOrEmpty(dr["Manager_ID"].ToString()) && !string.IsNullOrEmpty(dr["Manager_PositionID"].ToString()))
                {
                    PersonalInfoCenter personalInfo = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPersonalInfo(dr["Manager_ID"].ToString(), DateTime.ParseExact(oRequestParameter.InputParameter["checkDate"].ToString(), "yyyy-MM-dd", enCL));
                    oResult.PersonalData = (PersonalInfoCenter)personalInfo;
                    EmployeeData oEmpData = new EmployeeData(dr["Manager_ID"].ToString(), dr["Manager_PositionID"].ToString(), oRequestParameter.CurrentEmployee.CompanyCode, DateTime.Now);
                    dr["OrgUnitName"] = oEmpData.CurrentOrganization.AlternativeName(oRequestParameter.CurrentEmployee.Language);
                    dr["FirstNameEn"] = oResult.PersonalData.FirstNameEn;
                    dr["LastNameEn"] = oResult.PersonalData.LastNameEn;
                    dr["OrgUnitNo"] = oEmpData.CurrentOrganization.ObjectID;
                    //dr["PositionEn"] = oResult.PersonalData.Poi
                }
            }
            return oDT;
        }

        [HttpPost]
        public string GetOrgUnitName([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeData oEmpData = new EmployeeData(oRequestParameter.InputParameter["EmployeeID"].ToString(), oRequestParameter.InputParameter["PositionID"].ToString(),oRequestParameter.CurrentEmployee.CompanyCode, DateTime.Now);
            return oEmpData.OrgAssignment.OrgUnitData.AlternativeName(oRequestParameter.CurrentEmployee.Language);
        }

        [HttpPost]
        public void InsertINFOTYPEAllForEmployee([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalData> oPers = JSon.Deserialize<List<PersonalData>>(oRequestParameter.InputParameter["PersonData"].ToString());
            List<INFOTYPE0002> oINF2 = new List<INFOTYPE0002>();
            List<INFOTYPE0182> oINF0182 = new List<INFOTYPE0182>();
            foreach (PersonalData per in oPers)
            {
                INFOTYPE0002 INF2 = new INFOTYPE0002
                {
                    EmployeeID = oRequestParameter.InputParameter["EmployeeID"].ToString(),
                    BeginDate = per.BeginDate.Date,
                    EndDate = per.EndDate.Date,
                    TitleID = per.TitleID,
                    FirstName = per.FirstName,
                    LastName = per.LastName,
                    NickName = per.NickName,
                    DOB = per.DOB.Date,
                    Gender = per.Gender,
                    MaritalStatus = per.MaritalStatus,
                    Nationality = per.Nationality,
                    Language = per.Language,
                    Religion = per.Religion
                };
                oINF2.Add(INF2);

                INFOTYPE0182 INF182 = new INFOTYPE0182 { EmployeeID = per.EmployeeID, BeginDate = per.BeginDate, EndDate = per.EndDate, AlternateName = per.FullNameEn };
                oINF0182.Add(INF182);
            }


            List<OrganizationData> oOrgData = JSon.Deserialize<List<OrganizationData>>(oRequestParameter.InputParameter["OrgData"].ToString());
            List<INFOTYPE0001> oINF1 = new List<INFOTYPE0001>();
            foreach (OrganizationData org in oOrgData)
            {
                if (string.IsNullOrEmpty(org.CompanyCode)) org.CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                INFOTYPE0001 temp = new INFOTYPE0001
                {
                    EmployeeID = oRequestParameter.InputParameter["EmployeeID"].ToString(),
                    Name = org.Name,
                    Area = org.Area,
                    SubArea = org.SubArea,
                    EmpGroup = org.EmpGroup,
                    PositionID = org.PositionID,
                    EmpSubGroup = org.EmpSubGroup,
                    OrgUnit = org.OrgUnit,
                    Position = org.Position,
                    CostCenter = org.CostCenter,
                    BeginDate = org.BeginDate.Date,
                    EndDate = org.EndDate.Date,
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode
                };
                oINF1.Add(temp);
            }
            //List<INFOTYPE0001> oINF1 = Convert<List<INFOTYPE0001>>.ObjectFrom(oOrgData);

            List<ContactData> oContData = JSon.Deserialize<List<ContactData>>(oRequestParameter.InputParameter["ContData"].ToString());
            List<INFOTYPE0105> oINF0105 = new List<INFOTYPE0105>();
            foreach (ContactData con in oContData)
            {
                INFOTYPE0105 INF105 = new INFOTYPE0105
                {
                    EmployeeID = oRequestParameter.InputParameter["EmployeeID"].ToString(),
                    BeginDate = con.BeginDate.Date,
                    EndDate = con.EndDate.Date,
                    CategoryCode = con.CategoryCode,
                    DataText = con.DataText
                };
                oINF0105.Add(INF105);
            }
            
            List<RelationData> oRelaData = JSon.Deserialize<List<RelationData>>(oRequestParameter.InputParameter["PositionData"].ToString());
            
            Dictionary<string, ObjectType> ObjectTypeDic = new Dictionary<string, ObjectType>();
            ObjectTypeDic.Add("POSITION", ObjectType.S);
            ObjectTypeDic.Add("ORGANIZE", ObjectType.O);
            ObjectTypeDic.Add("EMPLOYEE", ObjectType.P);
            ObjectTypeDic.Add("COSTCENTER", ObjectType.K);
            ObjectTypeDic.Add("JOB",ObjectType.C);
            ObjectTypeDic.Add("QUALIFICATION", ObjectType.Q);
            //int count = 0;
            //List<INFOTYPE1001> oINF1001 = Convert<List<INFOTYPE1001>>.ObjectFrom(oRelaData);
            List<INFOTYPE1001> oINF1001 = new List<INFOTYPE1001>();
            foreach (RelationData rel in oRelaData)
            {
                INFOTYPE1001 temp = new INFOTYPE1001 {
                    ObjectID = rel.ObjectID,
                    ObjectType = ObjectTypeDic[rel.ObjectType],
                    NextObjectID = rel.NextObjectID,
                    NextObjectType = ObjectTypeDic[rel.NextObjectType],
                    Relation = rel.Relation,
                    PercentValue = rel.PercentValue,
                    BeginDate = rel.BeginDate.Date,
                    EndDate = rel.EndDate.Date
                };
                oINF1001.Add(temp);
                //INF1001.ObjectType = ObjectTypeDic[oRelaData[count].ObjectType];
                //INF1001.NextObjectType = ObjectTypeDic[oRelaData[count].NextObjectType];
                //count = count + 1;
            }
            EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertINFOTYPEAllForEmployee(oINF1, oINF2, oINF0182, oINF0105, oINF1001);
        }

        //for get limit year for any dropdown depend on publish date of company
        [HttpPost]
        public int GetYearsLimit([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                return 5;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        
        [HttpPost]
        public DataTable GetOffCycleTypeList([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oDT = new DataTable();
            string oCompanyCode = oRequestParameter.Requestor.CompanyCode,
                oLanguage = oRequestParameter.Requestor.Language;
            try
            {
                oDT = EmployeeManagement.CreateInstance(oCompanyCode).GetOffCycleTypeList(oLanguage);
                return oDT;
            }
            catch (Exception ex)
            {
                return oDT;
            }
        }

        [HttpPost]
        public DataTable GetYearTMContentSelect([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.Requestor.CompanyCode.ToString();
            //Nattawat S. updated for support fiscalyear / calendaryear
            string oYearType = oRequestParameter.InputParameter["YearType"].ToString(); // Including FISCAL_YEAR, CALENDAR_YEAR

            DataTable dt = new DataTable();
            DataTable dtYear = new DataTable();
            dtYear.Columns.Add("Year", typeof(string));
            dt = EmployeeManagement.CreateInstance(CompanyCode).GetConfigSelectYear();
            if (dt.Rows.Count > 0)
            {
                int iOldYear = Convert.ToInt32(dt.Rows[0]["Value"].ToString());
                int cYear = Convert.ToInt32(DateTime.Now.Year); //current year
                FiscalYear oFiscal = EmployeeManagement.CreateInstance(CompanyCode).GetFiscalYear();
                if (string.IsNullOrEmpty(oFiscal.startMonth))
                    oFiscal.startMonth = "01";

                DateTime tempDT;
                if (oYearType == "FISCAL_YEAR")
                    tempDT = new DateTime(cYear, Convert.ToInt16(oFiscal.startMonth), 1);
                else //CALENDAR_YEAR
                    tempDT = new DateTime(cYear, 1, 1);

                if (tempDT > DateTime.Now.Date)
                    cYear -= 1;

                for (int i = 0; i < iOldYear; i++)
                {
                    dtYear.Rows.Add(cYear - i);
                }
            }

            return dtYear;
        }

        [HttpPost]
        public FiscalYear GetFiscalYearDetail([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode.ToString();
            FiscalYear oFiscal = new FiscalYear();
            try
            {
                var tmpFY = EmployeeManagement.CreateInstance(CompanyCode).GetFiscalYear();
                if (tmpFY != null)
                    oFiscal = tmpFY;
            }
            catch (Exception ex)
            {

            }
            return oFiscal;
        }
    }
}