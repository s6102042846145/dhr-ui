﻿using System.Web.Mvc;

namespace iSSWS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Provider(string ID)
        {
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            return Json(ID, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Caller()
        {
            return View();
        }

        public ActionResult Index(string id)
        {
            return Redirect("~/Client/login.html");
        }
        public ActionResult ImportExport(string id)
        {
            return Redirect("~/ClientIE/index.html");
        }
    }
}