﻿using iSSWS.HttpFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace iSSWS.Controllers
{
    [HTTPAuthentication]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DummysController : ApiController
    {
        private string resourceDSN, viewerEmployeeID, viewerCompanyCode;

        [HttpGet]
        public IHttpActionResult GetDummy()
        {
            return Ok(1);
        }
    }
}