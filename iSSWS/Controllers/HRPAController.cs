﻿using DHR.HR.API.Model;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.HR.PA;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.PORTALENGINE;
using ESS.UTILITY.CONVERT;
using ESS.WORKFLOW;
using iSSWS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using QRCoder;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.FILE;
using System.Web;
using ESS.HR;
using Newtonsoft.Json.Linq;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HRPAController : ApiController
    {
        private readonly CultureInfo thCL = new CultureInfo("th-TH");
        private readonly CultureInfo enCL = new CultureInfo("en-US");

        #region "PAConfiguration"
        [HttpPost]
        public object GetPAConfigurationList([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            DataSet PAConfig = HRPAManagement.CreateInstance(CompanyCode).GetPAConfigurationListAll();
            DataTable Personal = new DataTable("Personal");
            DataTable Address = new DataTable("Adress");
            DataTable Bank = new DataTable("Bank");
            DataTable Family = new DataTable("Family");
            DataTable Education = new DataTable("Education");
            DataTable Communication = new DataTable("Communication");
            DataTable TaxAllowance = new DataTable("TaxAllowance");
            DataTable TabPAQuali = new DataTable("TabPAQuali");
            DataTable TabContent = new DataTable("TabContent");
            DataTable Employee_CV = new DataTable("CV");
            DataTable Document = new DataTable("Document");
            DataTable Academic = new DataTable("Academic");
            DataTable TabPAInfo = new DataTable("TabPAInfo");
            DataTable TabPAConfidential = new DataTable("TabPAConfidential");
            DataTable TabPACoparate = new DataTable("TabPACoparate");
            Personal = PAConfig.Tables[0];
            Address = PAConfig.Tables[1];
            Bank = PAConfig.Tables[2];
            Family = PAConfig.Tables[3];
            Education = PAConfig.Tables[4];
            Communication = PAConfig.Tables[5];
            TaxAllowance = PAConfig.Tables[6];
            TabPAQuali = PAConfig.Tables[7];
            TabContent = PAConfig.Tables[8];
            Employee_CV = PAConfig.Tables[9];
            Document = PAConfig.Tables[10];
            Academic = PAConfig.Tables[11];
            TabPAInfo = PAConfig.Tables[13];
            TabPAConfidential = PAConfig.Tables[14];
            TabPACoparate = PAConfig.Tables[15];

            string DateCutoffApproveBankAccount = ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "ESS.PY.SVC", "SET_DATE_CUTOFF_APPROVE_BANKACCOUNT");

            object DataConfig = new
            {
                Personal,
                Address,
                Bank,
                Family,
                Education,
                Communication,
                TaxAllowance,
                TabPAQuali,
                TabContent,
                Employee_CV,
                Document,
                Academic,
                DateCutoffApproveBankAccount,
                TabPAInfo,
                TabPAConfidential,
                TabPACoparate
            };

            return DataConfig;
        }
        [HttpPost]
        public object GetPAConfigurationListByCategoryName([FromBody] RequestParameter oRequestParameter)
        {

            string CompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            string CategoryName = oRequestParameter.InputParameter["CategoryName"].ToString();
            DataSet PAConfig = HRPAManagement.CreateInstance(CompanyCode).GetPAConfigurationListByCategoryName(CategoryName);
            DataTable ConfigData = new DataTable("Config");
            ConfigData = PAConfig.Tables[0];

            object DataConfig = new
            {
                ConfigData
            };

            return DataConfig;
        }
        #endregion

        #region Personal Data Center

        [HttpPost]
        public object GetPersonalData([FromBody] RequestParameter oRequestParameter)
        {
            int requestTypeID = 601;
            string empImg = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).PHOTO_PATH, oRequestParameter.Requestor.EmployeeID);
            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID, oRequestParameter.CurrentEmployee.Language);

            PersonalInformation oResult = new PersonalInformation();

            PersonalInfoCenter personalInfo = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalInfo(oRequestParameter.Requestor.EmployeeID, DateTime.ParseExact(oRequestParameter.InputParameter["checkDate"].ToString(), "yyyy-MM-dd", enCL));
            oResult.PersonalData = (PersonalInfoCenter)personalInfo;
            oResult.PersonalData_OLD = (PersonalInfoCenter)personalInfo;

            object oReturn = new
            {
                oResult,
                createdDoc,
                empImg
            };

            return oReturn;
        }

        [HttpPost]
        public object GetPersonalData_PY([FromBody] RequestParameter oRequestParameter)
        {
            int requestTypeID = 601;
            string empImg = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).PHOTO_PATH, oRequestParameter.Requestor.EmployeeID);
            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.InputParameter["EmpID"].ToString(), requestTypeID, oRequestParameter.CurrentEmployee.Language);

            PersonalInformation oResult = new PersonalInformation();

            PersonalInfoCenter personalInfo = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalInfo(oRequestParameter.InputParameter["EmpID"].ToString(), DateTime.ParseExact(oRequestParameter.InputParameter["checkDate"].ToString(), "yyyy-MM-dd", enCL));
            oResult.PersonalData = (PersonalInfoCenter)personalInfo;
            oResult.PersonalData_OLD = (PersonalInfoCenter)personalInfo;

            object oReturn = new
            {
                oResult,
                createdDoc,
                empImg
            };

            return oReturn;
        }
        [HttpPost]
        public string GetIDCardData([FromBody] RequestParameter oRequestParameter)
        {
            List<string> oResult = new List<string>();
            string sIDCardNo = string.Empty;

            oResult = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetIDCardData(oRequestParameter.Requestor.EmployeeID);
            if (oResult != null && oResult.Count > 0)
            {
                sIDCardNo = oResult[0].ToString();
            }
            return sIDCardNo;
        }


        [HttpPost]
        public object GetPersonSelectData(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;

            List<Child> MasterChild = new List<Child>();
            for (int i = 1; i <= 10; i++)
            {
                Child iChild = new Child();
                iChild.ChildID = i;
                iChild.ChildName = i.ToString();
                MasterChild.Add(iChild);
            }

            object oReturn = new
            {
                Language = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllLanguage(),
                Title = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllTitleName(LanguageCode),
                SecondTitle = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllSecondTitle(LanguageCode),
                Prefix = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPrefixNameList(LanguageCode),
                MilitaryTitle = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMilitaryTitle(LanguageCode),
                AcademicTitle = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllAcademicTitle(LanguageCode),
                MedicalTitle = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMedicalTitle(LanguageCode),
                TitleEN = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllTitleName("EN"),//ระบุ TitleNameEN เพื่อที่จะได้ไปดึงข้อมูลที่ dbo.TextDescription ได้ถูก เนื่องจากข้อมูลจาก DHR แตกต่างกันระหว่าง TitleTH,TitleEn
                SecondTitleEN = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllSecondTitle("EN"),
                PrefixEN = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPrefixNameList("EN"),
                MilitaryTitleEN = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMilitaryTitle("EN"),
                AcademicTitleEN = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllAcademicTitle("EN"),
                MedicalTitleEN = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMedicalTitle("EN"),
                TitleTH = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllTitleName("TH"),
                SecondTitleTH = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllSecondTitle("TH"),
                PrefixTH = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPrefixNameList("TH"),
                MilitaryTitleTH = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMilitaryTitle("TH"),
                AcademicTitleTH = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllAcademicTitle("TH"),
                MedicalTitleTH = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMedicalTitle("TH"),
                Gender = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllGender(LanguageCode),
                Nationality = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllNationality(LanguageCode),
                MaritalStatus = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMaritalStatus(LanguageCode),
                Country = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCountryList(LanguageCode),
                Religion = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllReligion(LanguageCode),
                Province = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetProvinceAll(LanguageCode),
                FamilyMember = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllFamilyMember(LanguageCode),
                MasterChild,
                UserImg = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).PHOTO_PATH, oRequestParameter.Requestor.EmployeeID)

            };
            return oReturn;
        }

        [HttpPost]
        public object GetProvinceByCountryCode(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            object oReturn = new
            {
                Province = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetProvinceByCountryCode(oRequestParameter.InputParameter["Code"].ToString(), LanguageCode)
            };
            return oReturn;
        }

        [HttpPost]
        public object GetMTValidateDataCodeValue(RequestParameter oRequestParameter)
        {
            //  MTValidateDataCodeValue
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            var MTDataValueCode = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetMTValidateDataCodeValue(oRequestParameter.InputParameter["dataCode"].ToString(), oRequestParameter.InputParameter["dataValue"].ToString());
            var oCertificate = MTDataValueCode.Where(a => a.mapDataCode == "CERTIFICATE_EDU_CODE").ToList();
            var oMajor = MTDataValueCode.Where(a => a.mapDataCode == "MAJOR_CODE").ToList();
            object oReturn = new
            {
                oCertificate,
                oMajor
            };
            return oReturn;
        }

        [HttpPost]
        public object GetMTValidateTitleName(RequestParameter oRequestParameter)
        {
            var TitleNameTH = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetMTValidateTitleName(oRequestParameter.InputParameter["companyCode"].ToString(), "TH"
                  , oRequestParameter.InputParameter["iname_code"].ToString(), oRequestParameter.InputParameter["iname_value"].ToString()
                   , oRequestParameter.InputParameter["iname1_code"].ToString(), oRequestParameter.InputParameter["iname1_value"].ToString()
                    , oRequestParameter.InputParameter["iname2_code"].ToString(), oRequestParameter.InputParameter["iname2_value"].ToString()
                     , oRequestParameter.InputParameter["iname3_code"].ToString(), oRequestParameter.InputParameter["iname3_value"].ToString()
                      , oRequestParameter.InputParameter["iname4_code"].ToString(), oRequestParameter.InputParameter["iname4_value"].ToString()
                       , oRequestParameter.InputParameter["iname5_code"].ToString(), oRequestParameter.InputParameter["iname5_value"].ToString()).FirstOrDefault();
            //ภาษาอังกฤษ ดึงแค่ iname_value ไม่ต้องเอายศอื่น ๆ เนื่องจาก dhr เช็คแค่นี้
            var TitleNameEN = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetMTValidateTitleName(oRequestParameter.InputParameter["companyCode"].ToString(), "EN"
             , oRequestParameter.InputParameter["iname_code"].ToString(), oRequestParameter.InputParameter["iname_value"].ToString()).FirstOrDefault();
            object oReturn = new
            {
                TitleNameTH,
                TitleNameEN
            };
            return oReturn;
        }

        [HttpPost]
        public object GetProvinceAll([FromBody] RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            object oReturn = new
            {
                MasterProvince = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetProvinceAll()
            };
            return oReturn;
        }


        [HttpPost]
        public object GetEducationLevelDDL([FromBody] RequestParameter oRequestParameter)
        {
            string EducationType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string EducationCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduCode"].ToString()) ? oRequestParameter.InputParameter["EduCode"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationLevelDDL(EducationType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, EducationCode);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetEducationInstitutionDLL([FromBody] RequestParameter oRequestParameter)
        {
            string EducationType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string EducationCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduCode"].ToString()) ? oRequestParameter.InputParameter["EduCode"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationInstitutionDLL(EducationType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, EducationCode);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        #region "PersonalEducationDetail"
        [HttpPost]
        public object GetPersonalPositionHisDetail([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalPositionHisCenter> oResult = new List<PersonalPositionHisCenter>();
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPositionHisByEmpCode(EmpCode);
            return oResult;
        }
        #endregion "PersonalEducationDetail





        #region "GetDHRPAPrivateInfByEmpCode"
        [HttpPost]
        public object GetDHRPAPrivateInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {

            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

            List<PersonalInfoCenter> oResult = new List<PersonalInfoCenter>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalInfoData(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAPrivateInfByEmpCode








        #region "GetDHRPAPersonalInfByEmpCode"
        [HttpPost]
        public object GetDHRPAPersonalInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {

            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

            List<PAPersonalInf> oResult = new List<PAPersonalInf>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalInfoDataDetail(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAPersonalInfByEmpCode

        #region "GetDHRPAAddressInfByEmpCode"
        [HttpPost]
        public object GetDHRPAAddressInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {

            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime oCheckDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd", enCL);

            List<PersonalAddressCenter> oResult = new List<PersonalAddressCenter>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalAddress(EmpCode, oCheckDate);
            return oResult;
        }
        #endregion "GetDHRPAAddressInfByEmpCode


        #region "GetDHRPAFamilyInfByEmpCode"
        [HttpPost]
        public object GetDHRPAFamilyInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {

            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

            List<PAFamilyInf> oResult = new List<PAFamilyInf>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDHRPAFamilyInfByEmpCode(BeginDate, EndDate, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAFamilyInfByEmpCode

        #region "GetDHRPAAddressFamilyInfByEmpCode"
        [HttpPost]
        public object GetDHRPAAddressFamilyInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {

            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

            List<PAAddressFamilyInf> oResult = new List<PAAddressFamilyInf>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDHRPAAddressFamilyInfByEmpCode(BeginDate, EndDate, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAAddressFamilyInfByEmpCode




        #region "PersonalEducationDetail"
        [HttpPost]
        public object GetPersonalEducationDetail([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalEducationCenter> oResult = new List<PersonalEducationCenter>();
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducation(EmpCode);
            return oResult;
        }
        #endregion "PersonalEducationDetail


        #region "GetDHRPAContractByEmpCode"
        [HttpPost]
        public object GetDHRPAContractByEmpCode([FromBody] RequestParameter oRequestParameter)
        {
            var ccc = oRequestParameter.InputParameter["BeginDate"].ToString();
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

            List<PAContactInf> oResult = new List<PAContactInf>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetContractData(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAContractByEmpCode

        #region "GetDHRPADocumentInfByEmpCode"
        [HttpPost]
        public object GetDHRPADocumentInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {

            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);

            List<PADocumentInf> oResult = new List<PADocumentInf>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDocumentData(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPADocumentInfByEmpCode

        #region "GetDHRPAClothingSizeByEmpCode"
        [HttpPost]
        public object GetDHRPAClothingSizeByEmpCode([FromBody] RequestParameter oRequestParameter)
        {
            //1/1/1900 12:00:00 AM
            //var stDate = Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"].ToString()).ToString("yyyy-MM-dd");
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            //DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            //DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            DateTime BeginDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);

            List<PAClothingSize> oResult = new List<PAClothingSize>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetClothingSizeData(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPADocumentInfByEmpCode

        #region "GetDHRPAEvaluationInfByEmpCode"
        [HttpPost]
        public object GetDHRPAEvaluationInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);

            List<PAEvaluationInf> oResult = new List<PAEvaluationInf>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEvaluationInfData(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAEvaluationInfByEmpCode


        #region "GetDHRPAMedicalCheckupByEmpCode"
        [HttpPost]
        public object GetDHRPAMedicalCheckupByEmpCode([FromBody] RequestParameter oRequestParameter)
        {
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);

            List<PAMedicalCheckup> oResult = new List<PAMedicalCheckup>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetMedicalCheckupData(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAMedicalCheckupByEmpCode

        #region "GetDHRPAPunishmentInfByEmpCode"
        [HttpPost]
        public object GetDHRPAPunishmentInfByEmpCode([FromBody] RequestParameter oRequestParameter)
        {
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"].ToString()).ToString("yyyy-MM-dd"), "yyyy-MM-dd", null);

            List<PAPunishmentInf> oResult = new List<PAPunishmentInf>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPunishmentInfData(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode);
            return oResult;
        }
        #endregion "GetDHRPAPunishmentInfByEmpCode






  


        #region "PersonalEducationDetail"
        [HttpPost]
        public object GeOrganizationByEmpCode([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalOrganizationCenter> oResult = new List<PersonalOrganizationCenter>();
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            oResult = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GeOrganizationByEmpCode(EmpCode);
            return oResult;
        }

        #endregion "PersonalEducationDetail"




        #region "PersonalEducationDetail"
        [HttpPost]
        public object GetWorkHistoryDetail([FromBody] RequestParameter oRequestParameter)
        {
            List<WorkHistoryCenter> oResult = new List<WorkHistoryCenter>();
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            oResult = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetWorkHistory(EmpCode);
            return oResult;
        }

        #endregion "PersonalEducationDetail"



        [HttpPost]
        public object GetEducationDegreeDLL([FromBody] RequestParameter oRequestParameter)
        {
            string EducationType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string EducationCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduCode"].ToString()) ? oRequestParameter.InputParameter["EduCode"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationDegreeDLL(EducationType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, EducationCode);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }
        [HttpPost]
        public object AddressTypeDDL([FromBody] RequestParameter oRequestParameter)
        {
            string Type = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string Code = !string.IsNullOrEmpty(oRequestParameter.InputParameter["SUBTYPE"].ToString()) ? oRequestParameter.InputParameter["SUBTYPE"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).SubtypeDDL(Type, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, Code);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }
        [HttpPost]
        public object RelationList([FromBody] RequestParameter oRequestParameter)
        {
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRelationList( BeginDate, EndDate);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }
        [HttpPost]
        public object SubtypeDDL([FromBody] RequestParameter oRequestParameter)
        {
            string Type = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string Code = !string.IsNullOrEmpty(oRequestParameter.InputParameter["SUBTYPE"].ToString()) ? oRequestParameter.InputParameter["SUBTYPE"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).SubtypeDDL(Type, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, Code);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetUserroleDLL([FromBody] RequestParameter oRequestParameter)
        {
            string Type = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string Code = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Code"].ToString()) ? oRequestParameter.InputParameter["Code"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetUserroleDLL(Type, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, Code);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetPareDLL([FromBody] RequestParameter oRequestParameter)
        {
            string Type = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string Code = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Code"].ToString()) ? oRequestParameter.InputParameter["Code"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPareDLL(Type, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, Code);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }


        [HttpPost]
        public object GetEducationCountryDLL([FromBody] RequestParameter oRequestParameter)
        {
            string EducationType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string EducationCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduCode"].ToString()) ? oRequestParameter.InputParameter["EduCode"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationCountryDLL(EducationType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, EducationCode);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }




        [HttpPost]
        public object GetEducationEmployeeDLL([FromBody] RequestParameter oRequestParameter)
        {
            string Type = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string UnitCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitCode"].ToString()) ? oRequestParameter.InputParameter["UnitCode"].ToString() : string.Empty;
            string UnitLevelValue = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitLevelValue"].ToString()) ? oRequestParameter.InputParameter["UnitLevelValue"].ToString() : string.Empty;
            string PostCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PostCode"].ToString()) ? oRequestParameter.InputParameter["PostCode"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationEmployeeDLL(Type, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, UnitCode, UnitLevelValue, PostCode);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }





        [HttpPost]
        public object GetEducationMainMajorDLL([FromBody] RequestParameter oRequestParameter)
        {
            string EducationType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string EducationCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduCode"].ToString()) ? oRequestParameter.InputParameter["EduCode"].ToString() : string.Empty;
            string EducationLevel = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduLevel"].ToString()) ? oRequestParameter.InputParameter["EduLevel"].ToString() : string.Empty;
            string EducationLevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduLevelSelected"].ToString()) ? oRequestParameter.InputParameter["EduLevelSelected"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationMainMajorDLL(EducationType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }


        [HttpPost]
        public object GetEducationMinorDLL([FromBody] RequestParameter oRequestParameter)
        {
            string EducationType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string EducationCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduCode"].ToString()) ? oRequestParameter.InputParameter["EduCode"].ToString() : string.Empty;
            string EducationLevel = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduLevel"].ToString()) ? oRequestParameter.InputParameter["EduLevel"].ToString() : string.Empty;
            string EducationLevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduLevelSelected"].ToString()) ? oRequestParameter.InputParameter["EduLevelSelected"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationMinorDLL(EducationType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }






        [HttpPost]
        public object GetEducationQualificationDLL([FromBody] RequestParameter oRequestParameter)
        {
            string EducationType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string EducationCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduCode"].ToString()) ? oRequestParameter.InputParameter["EduCode"].ToString() : string.Empty;
            string EducationLevel = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduLevel"].ToString()) ? oRequestParameter.InputParameter["EduLevel"].ToString() : string.Empty;
            string EducationLevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EduLevelSelected"].ToString()) ? oRequestParameter.InputParameter["EduLevelSelected"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationQualificationDLL(EducationType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }

        [HttpPost]
        public object GetIndustryDLL([FromBody] RequestParameter oRequestParameter)
        {
            string Type = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string Code = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Code"].ToString()) ? oRequestParameter.InputParameter["Code"].ToString() : string.Empty;
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducationCountryDLL(Type, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, Code);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }





        [HttpPost]
        public object GetDistrictProvince([FromBody] RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            string ProvinceCode = oRequestParameter.InputParameter["ProvinceCode"].ToString();
            List<DistrictProvice> MasterDistrict = new List<DistrictProvice>();
            object oReturn = new
            {
                MasterDistrict = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDistrict(ProvinceCode)
            };
            return oReturn;
        }

        [HttpPost]
        public object GetDistrictByProvinceCode([FromBody] RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            string ProvinceCode = oRequestParameter.InputParameter["ProvinceCode"].ToString();
            List<DistrictProvice> MasterDistrict = new List<DistrictProvice>();
            MasterDistrict = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDistrict(ProvinceCode);
            //var District = (from x in q_District
            //                select new { x.districtCode, x.districtName, x.districtNameEn, x.postcode }).Distinct();
            var District = (from x in MasterDistrict
                            select new
                            {
                                x.districtCode,
                                x.districtName,
                                x.districtNameEn
                            }).Distinct().ToList();
            object oReturn = new
            {
                District
            };
            return oReturn;
        }


        #endregion Personal Data Center

        #region "Personal Account Center"



        [HttpPost]
        public Bank GetBank([FromBody] RequestParameter oRequestParameter)
        {
            Bank oResult = new Bank();

            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBank(oRequestParameter.InputParameter["Code"] != null ? oRequestParameter.InputParameter["Code"].ToString() : string.Empty);

            return oResult;
        }
        [HttpPost]
        public List<Bank> GetAllBank([FromBody] RequestParameter oRequestParameter)
        {
            List<Bank> oResult = new List<Bank>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllBank(oRequestParameter.CurrentEmployee.Language);

            return oResult;
        }


        #endregion "Personal Account Center"

        #region "Personal Family Center

        [HttpPost]
        public object GetPersonalFamily([FromBody] RequestParameter oRequestParameter)
        {

            Dictionary<string, List<FileAttachmentForMobile>> dictFileAttachmentList = new Dictionary<string, List<FileAttachmentForMobile>>();
            List<PersonalFamilyCenter> FamilyData = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalFamilyData(oRequestParameter.Requestor.EmployeeID);
            EmployeeDataForMobile oRequestor = oRequestParameter.Requestor;
            SetAuthenticate(oRequestor);
            int requestTypeID = 604;

            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID, oRequestParameter.CurrentEmployee.Language);

            string category = string.Empty;
            if (createdDoc != null && createdDoc.Rows.Count > 0)
            {
                category = ((createdDoc.Rows[0]["DataCategory"].ToString()).Split('_'))[0];
            }

            foreach (PersonalFamilyCenter member in FamilyData)
            {
                PersonalFamily oPersonalFamily = new PersonalFamily();

                string employeeID = oRequestor.EmployeeID;

                string requestSubType = (string.IsNullOrEmpty(member.ChildNo)) ? member.FamilyMember : member.FamilyMember + member.ChildNo;

                if (!string.IsNullOrEmpty(category))
                {

                    string categorytype = category + "_" + requestSubType;
                    createdDoc.AsEnumerable().Where(s => Convert.ToString(s["DataCategory"]).Equals(categorytype)).ToList<DataRow>()
                       .ForEach(r =>
                       {
                           r["subType"] = member.FamilyMember;
                           r["childNo"] = (string.IsNullOrEmpty(member.ChildNo)) ? "" : member.ChildNo;
                       });
                }
                //File Attachment
                List<FileAttachmentForMobile> FileAttachmentList = new List<FileAttachmentForMobile>();


                List<PersonalAttachmentFileSet> oAttExport = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentExport(employeeID, requestTypeID, requestSubType);
                for (int i = 0; i < oAttExport.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttExport[i].FileID;
                    file.FileName = oAttExport[i].FileName;
                    file.FileSetID = oAttExport[i].FileSetID;
                    file.FilePath = oAttExport[i].FilePath;
                    file.FileType = oAttExport[i].FileType;
                    FileAttachmentList.Add(file);
                }

                if (!dictFileAttachmentList.ContainsKey(requestSubType))
                    dictFileAttachmentList.Add(requestSubType, FileAttachmentList);
            }

            object oReturn = new
            {
                FamilyData,
                dictFileAttachmentList,
                createdDoc
            };

            foreach (KeyValuePair<string, List<FileAttachmentForMobile>> dictFile in dictFileAttachmentList)
            {
                List<FileAttachmentForMobile> FileAttachmentForMobileList = new List<FileAttachmentForMobile>();
                List<PersonalAttachmentFileSet> oAttFileSet = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentFileSet(oRequestor.EmployeeID, requestTypeID, dictFile.Key);
                for (int i = 0; i < oAttFileSet.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttFileSet[i].FileID;
                    file.FileName = oAttFileSet[i].FileName;
                    file.FileSetID = oAttFileSet[i].FileSetID;
                    file.FilePath = oAttFileSet[i].FilePath;
                    file.FileType = oAttFileSet[i].FileType;
                    //FileAttachmentForMobileList.Add(file);
                    dictFile.Value.Add(file);
                }
            }

            return oReturn;
        }

        public void SetAuthenticate(EmployeeDataForMobile oEmployee)
        {
            WorkflowIdentity iden;
            if (oEmployee.IsExternalUser)
                iden = WorkflowIdentity.CreateInstance(oEmployee.CompanyCode).GetIdentityForExternalUser(oEmployee.EmployeeID, oEmployee.Name);
            else
            {
                EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oEmployee);
                iden = WorkflowIdentity.CreateInstance(oEmployee.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            }
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
        }


        #endregion "Personal Family Center

        #region "Personal Contact Center"

        [HttpPost]
        public object GetCommunicationData([FromBody] RequestParameter oRequestParameter)
        {
            int requestTypeID = 603;

            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID, oRequestParameter.CurrentEmployee.Language);

            PersonalCommunicationCenter Contact = new PersonalCommunicationCenter();
            Contact = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCommunicationData(oRequestParameter.Requestor.EmployeeID);

            object oReturn = new
            {
                Contact,
                createdDoc
            };

            return oReturn;
        }

        [HttpPost]
        public bool GetIsViewCommunicationOnly([FromBody] RequestParameter oRequestParameter)
        {
            bool isViewCommunicationOnly = false;
            isViewCommunicationOnly = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).IsViewCommunicationOnly;
            return isViewCommunicationOnly;
        }

        #endregion "Personal Contact Center"

        #region Personal Address Center

        [HttpPost]
        public object GetPersonalAddress([FromBody] RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            Dictionary<string, List<FileAttachmentForMobile>> dictFileAttachmentList = new Dictionary<string, List<FileAttachmentForMobile>>();
            int requestTypeID = 602;

            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID, oRequestParameter.CurrentEmployee.Language);

            string category = string.Empty;
            if (createdDoc != null && createdDoc.Rows.Count > 0)
            {
                category = ((createdDoc.Rows[0]["DataCategory"].ToString()).Split('_'))[0];
            }
            List<PersonalAddressCenter> oResult = new List<PersonalAddressCenter>();
            List<Province> Province = new List<Province>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalAddress(oRequestParameter.Requestor.EmployeeID, DateTime.Now);
            Province = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetProvinceAll(LanguageCode);

            for (int i = 0; i < oResult.Count; i++)
            {
                string requestSubType = oResult[i].AddressType;
                List<FileAttachmentForMobile> FileAttachmentList = new List<FileAttachmentForMobile>();

                if (!string.IsNullOrEmpty(category))
                {
                    string categorytype = category + "_" + requestSubType;
                    createdDoc.AsEnumerable().Where(s => Convert.ToString(s["DataCategory"]).Equals(categorytype)).ToList<DataRow>()
                       .ForEach(r =>
                       {
                           r["subType"] = requestSubType;

                       });
                }

                List<PersonalAttachmentFileSet> oAttExport = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentExport(oRequestParameter.Requestor.EmployeeID, requestTypeID, requestSubType);
                for (int j = 0; j < oAttExport.Count; j++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttExport[j].FileID;
                    file.FileName = oAttExport[j].FileName;
                    file.FileSetID = oAttExport[j].FileSetID;
                    file.FilePath = oAttExport[j].FilePath;
                    file.FileType = oAttExport[j].FileType;
                    FileAttachmentList.Add(file);
                }

                if (!dictFileAttachmentList.ContainsKey(requestSubType))
                    dictFileAttachmentList.Add(requestSubType, FileAttachmentList);

            }
            object oReturn = new
            {
                oResult,
                dictFileAttachmentList,
                createdDoc,
                Province
            };

            foreach (KeyValuePair<string, List<FileAttachmentForMobile>> dictFile in dictFileAttachmentList)
            {
                List<FileAttachmentForMobile> FileAttachmentForMobileList = new List<FileAttachmentForMobile>();
                List<PersonalAttachmentFileSet> oAttFileSet = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentFileSet(oRequestParameter.Requestor.EmployeeID, requestTypeID, dictFile.Key);
                for (int i = 0; i < oAttFileSet.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttFileSet[i].FileID;
                    file.FileName = oAttFileSet[i].FileName;
                    file.FileSetID = oAttFileSet[i].FileSetID;
                    file.FilePath = oAttFileSet[i].FilePath;
                    file.FileType = oAttFileSet[i].FileType;
                    dictFile.Value.Add(file);
                }
            }

            return oReturn;
        }

        [HttpPost]
        public object GetAllCountryData(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            object oReturn = new
            {
                Country = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCountryList(LanguageCode),
            };
            return oReturn;
        }

        #endregion Personal Address Center


        #region "Personal Employee Center"

        [HttpPost]
        public object GetEmployeeDetail([FromBody] RequestParameter oRequestParameter)
        { 
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string UnitSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitSelected"].ToString()) ? oRequestParameter.InputParameter["UnitSelected"].ToString() : string.Empty;
            string LevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["LevelSelected"].ToString()) ? oRequestParameter.InputParameter["LevelSelected"].ToString() : string.Empty;
            string PositionSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PositionSelected"].ToString()) ? oRequestParameter.InputParameter["PositionSelected"].ToString() : string.Empty;
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            List<EmployeeDataCenter> oResult = new List<EmployeeDataCenter>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEmployeeList(BeginDate, EndDate, oRequestParameter.Requestor.CompanyCode, EmpCode, UnitSelected, LevelSelected, PositionSelected);
            return oResult;
        }

        [HttpPost]
        public string SetCheckViewEmployee([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oRole = new DataTable();
            string adminCode = oRequestParameter.CurrentEmployee.EmployeeID;
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            string PosCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PosCode"].ToString()) ? oRequestParameter.InputParameter["PosCode"].ToString() : string.Empty;
            string ActionType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["ActionType"].ToString()) ? oRequestParameter.InputParameter["ActionType"].ToString() : string.Empty;
            return HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).SetCheckViewEmployee(adminCode, EmpCode, ActionType, PosCode);
        }


        [HttpPost]
        public DataTable GetCheckViewEmployee([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oReturn = new DataTable();
            string adminCode = oRequestParameter.CurrentEmployee.EmployeeID;
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            string ActionType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["ActionType"].ToString()) ? oRequestParameter.InputParameter["ActionType"].ToString() : string.Empty;
            oReturn = HRPAManagement.CreateInstance(CompanyCode).GetCheckViewEmployee(adminCode, EmpCode, ActionType);
            return oReturn;
        }



        #endregion "Personal Employee Center"

        #region "Personal Education Center"

        [HttpPost]
        public object GetAllPersonalEducationHistory([FromBody] RequestParameter oRequestParameter)
        {

            Dictionary<string, List<FileAttachmentForMobile>> dictFileAttachmentList = new Dictionary<string, List<FileAttachmentForMobile>>();
            int requestTypeID = 607;

            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID, oRequestParameter.Requestor.Language);
            string category = string.Empty;
            string educationlevel = string.Empty;
            if (createdDoc != null && createdDoc.Rows.Count > 0)
            {
                category = ((createdDoc.Rows[0]["DataCategory"].ToString()).Split('_'))[0];
                educationlevel = ((createdDoc.Rows[0]["DataCategory"].ToString()).Split('_'))[1];
            }

            CommonText oCommonText = new CommonText(oRequestParameter.Requestor.CompanyCode);

            List<PersonalEducationCenter> oEducationData = new List<PersonalEducationCenter>();
            PersonalEducationCenter tmp = new PersonalEducationCenter();
            List<PersonalEducationCenter> oResult = new List<PersonalEducationCenter>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducation(oRequestParameter.Requestor.EmployeeID);

            for (int i = 0; i < oResult.Count; i++)
            {

                string requestSubType = oResult[i].EducationLevelCode;
                if (oResult[i].EducationLevelCode == educationlevel)
                {
                    createdDoc.Rows[0]["subType"] = oResult[i].EducationLevelCode;
                }
                // createdDoc.Columns["subType"] = oResult[i].EducationLevelCode;
                //if (!string.IsNullOrEmpty(category))
                //{
                //    string categorytype = category + "_" + requestSubType;
                //    createdDoc.AsEnumerable().Where(s => Convert.ToString(s["DataCategory"]).Equals(categorytype)).ToList<DataRow>()
                //       .ForEach(r =>
                //       {
                //           r["subType"] = requestSubType;

                //       });
                //}

                List<FileAttachmentForMobile> FileAttachmentList = new List<FileAttachmentForMobile>();


                List<PersonalAttachmentFileSet> oAttExport = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentExport(oRequestParameter.Requestor.EmployeeID, requestTypeID, requestSubType);
                for (int j = 0; j < oAttExport.Count; j++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttExport[j].FileID;
                    file.FileName = oAttExport[j].FileName;
                    file.FileSetID = oAttExport[j].FileSetID;
                    file.FilePath = oAttExport[j].FilePath;
                    file.FileType = oAttExport[j].FileType;
                    FileAttachmentList.Add(file);
                }

                if (!dictFileAttachmentList.ContainsKey(requestSubType))
                    dictFileAttachmentList.Add(requestSubType, FileAttachmentList);

            }


            object oReturn = new
            {
                oResult,
                dictFileAttachmentList,
                createdDoc
            };

            foreach (KeyValuePair<string, List<FileAttachmentForMobile>> dictFile in dictFileAttachmentList)
            {
                List<FileAttachmentForMobile> FileAttachmentForMobileList = new List<FileAttachmentForMobile>();
                List<PersonalAttachmentFileSet> oAttFileSet = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentFileSet(oRequestParameter.Requestor.EmployeeID, requestTypeID, dictFile.Key);
                for (int i = 0; i < oAttFileSet.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttFileSet[i].FileID;
                    file.FileName = oAttFileSet[i].FileName;
                    file.FileSetID = oAttFileSet[i].FileSetID;
                    file.FilePath = oAttFileSet[i].FilePath;
                    file.FileType = oAttFileSet[i].FileType;
                    //FileAttachmentForMobileList.Add(file);
                    dictFile.Value.Add(file);
                }
            }

            return oReturn;
        }

        #endregion "Personal Education Center"

        #region "Personal Document Center"

        [HttpPost]
        public object GetPersonalDocumentData([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalDocumentCenter> oPersonalDocumentList = new List<PersonalDocumentCenter>();
            int requestTypeID = 611;
            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID, oRequestParameter.CurrentEmployee.Language);
            oPersonalDocumentList = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPersonalDocumentData(oRequestParameter.Requestor.EmployeeID);
            Dictionary<string, string> oDocumentValidate = new Dictionary<string, string>();

            foreach (var item in oPersonalDocumentList)
            {
                var Dockey = "VALIDATELICENCEEXPIRED_" + item.DocumentType;
                oDocumentValidate.Add(Dockey, ShareDataManagement.LookupCache(oRequestParameter.Requestor.CompanyCode, "ESS.PY.SVC", Dockey));
            }
            // string[] oDocumentValidateList = documentValidate.Keys.ToArray();
            object oReturn = new
            {
                createdDoc,
                oPersonalDocumentList,
                oDocumentValidate
            };
            return oReturn;
        }

        #endregion

        #region "Personal Academic Center"
        [HttpPost]
        public object GetPersonalAcademicDataByDOI([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalAcademicCenter> oPersonalAcademicList = new List<PersonalAcademicCenter>();
            string DOINo = oRequestParameter.InputParameter["DOINo"].ToString();
            string SubTypeValue = oRequestParameter.InputParameter["SubTypeValue"].ToString();
            DateTime oCheckDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd", enCL);
            oPersonalAcademicList = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalAcademicDataByParameter(string.Empty, string.Empty, DOINo);
            var oAcademicDetail = oPersonalAcademicList.Where(a => a.DOI == DOINo.Trim() && a.SubTypeValue == SubTypeValue).FirstOrDefault();
            if (oAcademicDetail != null)
            {

                //check exiting member
                if (oAcademicDetail.AcademicMembers.Where(a => a.EmployeeID == oRequestParameter.Requestor.EmployeeID).Count() == 0)
                {
                    var personalInfo = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalInfo(oRequestParameter.Requestor.EmployeeID, oCheckDate);
                    if (personalInfo != null)
                    {
                        PersonalAcademicMemberCenter member = new PersonalAcademicMemberCenter();
                        member.EmployeeID = personalInfo.EmployeeID;
                        member.CompanyCode = personalInfo.CompanyCode;
                        member.PAReID = oAcademicDetail.PaReId.ToString();
                        member.PAReMemID = (oAcademicDetail.AcademicMembers.Count() + 1).ToString();
                        member.FullNameEN = personalInfo.FullNameEN;
                        member.FullNameTH = personalInfo.FullName;
                        member.RoleCode = string.Empty;
                        member.RoleValue = string.Empty;
                        member.RoleNameTH = string.Empty;
                        member.RoleNameEN = string.Empty;
                        oAcademicDetail.AcademicMembers.Add(member);
                    }
                }
                string strPathAcademic = ShareDataManagement.LookupCache(oRequestParameter.Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_ROOT_PATH") + oRequestParameter.Requestor.CompanyCode + "/" + oAcademicDetail.SubTypeValue + "/";
                string checkFolderPathAcademic = ShareDataManagement.LookupCache(oRequestParameter.Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_PATH") + oRequestParameter.Requestor.CompanyCode + "/" + oAcademicDetail.SubTypeValue + "/";
                string folderpath = System.Web.HttpContext.Current.Server.MapPath(checkFolderPathAcademic) + oAcademicDetail.AttachmentId + "/";
                oAcademicDetail.AttachmentFileSets = new List<PersonalAttachmentFileSet>();
                if (Directory.Exists(@folderpath))
                {
                    string[] filePaths = Directory.GetFiles(@folderpath);
                    foreach (var file in filePaths)
                    {
                        PersonalAttachmentFileSet data = new PersonalAttachmentFileSet();
                        data.FilePath = strPathAcademic;
                        data.FileName = Path.GetFileName(file);
                        data.FileSetID = oAcademicDetail.PaReId;
                        data.Data = File.ReadAllBytes(file);
                        data.FileType = MimeMapping.GetMimeMapping(data.FileName);
                        oAcademicDetail.AttachmentFileSets.Add(data);
                    }
                }
            }
            else
            {
                //new data
                oAcademicDetail = new PersonalAcademicCenter();
                oAcademicDetail.ImpactFactor = string.Empty;
                oAcademicDetail.SubmittedDate = SubTypeValue == "1" ? DateTime.MinValue : DateTime.Now;
                oAcademicDetail.GrantedDate = SubTypeValue == "1" ? DateTime.MinValue : DateTime.Now;
                var personalInfo = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalInfo(oRequestParameter.Requestor.EmployeeID, oCheckDate);
                if (personalInfo != null)
                {
                    PersonalAcademicMemberCenter member = new PersonalAcademicMemberCenter();
                    member.EmployeeID = personalInfo.EmployeeID;
                    member.CompanyCode = personalInfo.CompanyCode;
                    member.PAReID = oAcademicDetail.PaReId.ToString();
                    member.PAReMemID = (oAcademicDetail.AcademicMembers.Count() + 1).ToString();
                    member.FullNameEN = personalInfo.FullNameEN;
                    member.FullNameTH = personalInfo.FullName;
                    member.RoleCode = string.Empty;
                    member.RoleValue = string.Empty;
                    member.RoleNameTH = string.Empty;
                    member.RoleNameEN = string.Empty;
                    oAcademicDetail.AcademicMembers.Add(member);
                }
            }

            object oReturn = new
            {
                oAcademicDetail

            };
            return oReturn;
        }

        [HttpPost]
        public object GetPersonalAcademicDataByEmpCode([FromBody] RequestParameter oRequestParameter)
        {

            List<PersonalAcademicCenter> oPersonalAcademicList = new List<PersonalAcademicCenter>();
            List<PersonalAcademicMemberCenter> oPersonalAcademicMemberList = new List<PersonalAcademicMemberCenter>();
            int requestTypeID = 612;
            oPersonalAcademicList = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalAcademicDataByEmpCode(oRequestParameter.Requestor.EmployeeID);
            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocAll(requestTypeID, oRequestParameter.Requestor.Language);
            oPersonalAcademicList = oPersonalAcademicList.OrderBy(a => a.SubTypeValue).OrderByDescending(a => a.YearOfPublic).ThenByDescending(b => b.GrantedDate).ToList();

            /*
             var query = grades.GroupBy(student => student.Name)
                  .Select(group => 
                        new { Name = group.Key,
                              Students = group.OrderByDescending(x => x.Grade) })
                  .OrderBy(group => group.Students.First().Grade);
             */
            foreach (var item in oPersonalAcademicList)
            {
                string strPathAcademic = ShareDataManagement.LookupCache(oRequestParameter.Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_ROOT_PATH") + oRequestParameter.Requestor.CompanyCode + "/" + item.SubTypeValue + "/";
                string checkFolderPathAcademic = ShareDataManagement.LookupCache(oRequestParameter.Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_PATH") + oRequestParameter.Requestor.CompanyCode + "/" + item.SubTypeValue + "/";
                string filePathForView = strPathAcademic + item.AttachmentId + "/";
                string folderpath = System.Web.HttpContext.Current.Server.MapPath(checkFolderPathAcademic) + item.AttachmentId + "/";
                item.AttachmentFileSets = new List<PersonalAttachmentFileSet>();
                if (Directory.Exists(folderpath))
                {
                    string[] files = Directory.GetFiles(@folderpath);
                    foreach (var file in files)
                    {
                        PersonalAttachmentFileSet data = new PersonalAttachmentFileSet();
                        data.FilePath = filePathForView;
                        data.FileName = Path.GetFileName(file);
                        data.FileSetID = item.PaReId;
                        data.Data = File.ReadAllBytes(file);
                        data.FileType = MimeMapping.GetMimeMapping(data.FileName);
                        item.AttachmentFileSets.Add(data);
                    }
                }
            }
            object oReturn = new
            {
                createdDoc,
                oPersonalAcademicList

            };
            return oReturn;
        }

        [HttpPost]
        public object GetPersonalAcademicDataByEmpCodeCV([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalAcademicCenter> oPersonalAcademicList = new List<PersonalAcademicCenter>();
            List<PersonalAcademicMemberCenter> oPersonalAcademicMemberList = new List<PersonalAcademicMemberCenter>();
            int requestTypeID = 612;
            oPersonalAcademicList = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalAcademicDataByEmpCode(oRequestParameter.Requestor.EmployeeID);
            DataTable createdDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCreatedDocByEmployeeID(oRequestParameter.Requestor.EmployeeID, requestTypeID, oRequestParameter.Requestor.Language);

            foreach (var item in oPersonalAcademicList)
            {
                string strPathAcademic = ShareDataManagement.LookupCache(oRequestParameter.Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_ROOT_PATH") + oRequestParameter.Requestor.CompanyCode + "/" + item.SubTypeValue + "/";
                string checkFolderPathAcademic = ShareDataManagement.LookupCache(oRequestParameter.Requestor.CompanyCode, "ESS.PA.SVC", "ACADEMIC_PATH") + oRequestParameter.Requestor.CompanyCode + "/" + item.SubTypeValue + "/";
                string filePathForView = strPathAcademic + item.AttachmentId + "/";
                string folderpath = System.Web.HttpContext.Current.Server.MapPath(checkFolderPathAcademic) + item.AttachmentId + "/";
                item.AttachmentFileSets = new List<PersonalAttachmentFileSet>();
                if (Directory.Exists(folderpath))
                {
                    string[] files = Directory.GetFiles(@folderpath);
                    foreach (var file in files)
                    {
                        PersonalAttachmentFileSet data = new PersonalAttachmentFileSet();
                        data.FilePath = filePathForView;
                        data.FileName = Path.GetFileName(file);
                        data.FileSetID = item.PaReId;
                        data.Data = File.ReadAllBytes(file);
                        data.FileType = MimeMapping.GetMimeMapping(data.FileName);
                        item.AttachmentFileSets.Add(data);
                    }
                }
            }
            object oReturn = new
            {
                createdDoc,
                oPersonalAcademicList

            };
            return oReturn;
        }



        [HttpPost]
        public string GetAcademicRootPath([FromBody] RequestParameter oRequestParameter)
        {
            string strRootPath = ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "DHR.ROUTE.PA", "ACADEMIC_ROOT_PATH") + oRequestParameter.CurrentEmployee.CompanyCode + "/";
            return strRootPath;
        }
        #endregion


        #region "Personal Attactment file"

        [HttpPost]
        public object GetAttactmentFile([FromBody] RequestParameter oRequestParameter)
        {
            DataSet dSet = new DataSet();
            dSet = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAttachmentFileAllByEmployee(oRequestParameter.Requestor.EmployeeID);
            List<PAAttactFileSummary> PAFileSummay = new List<PAAttactFileSummary>();

            foreach (DataTable dt in dSet.Tables)
            {
                if (dt.Rows.Count > 0)
                {
                    PAAttactFileSummary oPA = new PAAttactFileSummary();
                    oPA.RequestTypeID = Convert.ToInt32(dt.Rows[0]["RequestTypeID"].ToString());

                    oPA.TextCode = dt.Rows[0]["TabCode"].ToString();

                    oPA.ContentNo = new List<PAAttactFileRequestNo>();
                    string TmpNo = "";
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (dt.Rows[i]["RequestNo"].ToString() != TmpNo)
                        {
                            TmpNo = dt.Rows[i]["RequestNo"].ToString();
                            PAAttactFileRequestNo oPANo = new PAAttactFileRequestNo();
                            oPANo.RequestNo = dt.Rows[i]["RequestNo"].ToString();
                            oPANo.ContentItem = new List<PAAttactFileContent>();
                            for (int k = 0; k <= dt.Rows.Count - 1; k++)
                            {
                                if (dt.Rows[k]["RequestNo"].ToString() == TmpNo)
                                {
                                    PAAttactFileContent oPAItem = new PAAttactFileContent();
                                    oPAItem.RequestNo = dt.Rows[k]["RequestNo"].ToString();
                                    oPAItem.FileSetID = Convert.ToInt32(dt.Rows[k]["FileSetID"].ToString());
                                    oPAItem.FileName = dt.Rows[k]["FileName"].ToString();
                                    oPAItem.FileType = dt.Rows[k]["FileName"].ToString().Substring(dt.Rows[k]["FileName"].ToString().Length - 3, 3);
                                    oPAItem.FilePath = dt.Rows[k]["FilePath"].ToString();
                                    oPAItem.SubmitDate = dt.Rows[k]["SubmitDate"].ToString();
                                    oPANo.ContentItem.Add(oPAItem);
                                }
                            }
                            oPA.ContentNo.Add(oPANo);
                        }
                    }
                    PAFileSummay.Add(oPA);
                }
            }

            object oReturn = new
            {
                PAFileSummay
            };
            return oReturn;
        }
        #endregion "Personal Attactment file"

        #region "Education Select Data"
        [HttpPost]
        public object GetEducationSelectData(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            // List<EducationLevel> oEducationLevel = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllEducationLevel();
            // string levelDefault = oRequestParameter.InputParameter["EducationLevelCode"]!=null? oRequestParameter.InputParameter["EducationLevelCode"].ToString():string.Empty;
            //List<Branch> branchlist = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllBranch();

            object oReturn = new
            {
                educationgroup = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllEducationGroup(LanguageCode),
                educationlevel = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllEducationLevel(LanguageCode),
                institute = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllInstitute(LanguageCode),
                country = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCountryList(LanguageCode),
                certificate = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCertificate(LanguageCode),
                branch1 = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllBranch(LanguageCode),
                honor = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllHonor(LanguageCode),
            };
            return oReturn;
        }
        #endregion

        #region "Branch"
        [HttpPost]
        public string GetBranch([FromBody] RequestParameter oRequestParameter)
        {
            string oResult = string.Empty;
            Branch branch = new Branch();
            branch = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBranchData(oRequestParameter.InputParameter["BranchCode"].ToString());
            if (branch != null)
                oResult = branch.BranchText;
            return oResult;
        }
        #endregion

        #region "Work History"
        [HttpPost]
        public object GetCurrentWorkHistory([FromBody] RequestParameter oRequestParameter)
        {
            List<WorkPeriod> oResult = new List<WorkPeriod>();
            List<WorkPeriod> workHistory = new List<WorkPeriod>();
            WorkPeriod tmp;
            CultureInfo oCL = new CultureInfo("en-US");
            oResult = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCurrentWorkHistory(oRequestParameter.Requestor.EmployeeID);


            foreach (WorkPeriod row in oResult)
            {
                string period = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCalculatePeriod(DateTime.ParseExact(row.STARTDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(row.ENDDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture), oRequestParameter.Requestor.Language);
                tmp = new WorkPeriod();
                tmp = row;
                tmp.WorkTime = period;
                workHistory.Add(tmp);
            }

            string oLang = oRequestParameter.CurrentEmployee.Language;
            string CurrentPosition = oLang == "TH" ? workHistory[workHistory.Count - 1].POS_NAME : workHistory[workHistory.Count - 1].POS_NAME_EN;
            string CurrentOrganization = oLang == "TH" ? workHistory[workHistory.Count - 1].ORG_NAME : workHistory[workHistory.Count - 1].ORG_NAME_EN;

            object oReturn = new
            {
                workHistory,
                CurrentPosition,
                CurrentOrganization
            };

            return oReturn;
        }

        [HttpPost]
        public List<INFOTYPE0023> GetAllPreviousEmployers([FromBody] RequestParameter oRequestParameter)
        {
            List<INFOTYPE0023> oResult = new List<INFOTYPE0023>();
            List<INFOTYPE0023> workHistory = new List<INFOTYPE0023>();
            INFOTYPE0023 tmp;
            CultureInfo oCL = new CultureInfo("en-US");
            oResult = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllPreviousEmployers(oRequestParameter.Requestor.EmployeeID);

            foreach (INFOTYPE0023 row in oResult)
            {
                string period = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCalculatePeriod(row.BeginDate, row.EndDate, oRequestParameter.Requestor.Language);
                tmp = new INFOTYPE0023();
                tmp = row;
                tmp.WorkTime = period;
                workHistory.Add(tmp);
            }

            return workHistory;
        }
        #endregion

        #region "SalaryRate"
        //[HttpPost]
        //public List<SALARYRATE> GetSalaryRate([FromBody] RequestParameter oRequestParameter)
        //{
        //    List<SALARYRATE> oResult = new List<SALARYRATE>();
        //    ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0001 oRequestor = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).INFOTYPE0001Get(oRequestParameter.Requestor.EmployeeID, DateTime.Now, oRequestParameter.Requestor.Language);

        //    oResult = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetSalaryRate(oRequestor.EmployeeID, oRequestor.EmpSubGroup);

        //    return oResult;
        //}
        #endregion

        #region Training

        [HttpPost]
        public object GetTrainingHistory([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalTrainingCenter> oTrainingList = new List<PersonalTrainingCenter>();
            string oCompanyCode = oRequestParameter.Requestor.CompanyCode;
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            string oStatus = "";
            oTrainingList = HRPAManagement.CreateInstance(oCompanyCode).GetTrainingHistory(EmpCode);
            return oTrainingList;
        }

        [HttpPost]
        public object GetAcedemicworkmember([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalAcademicMemberCenter> oAcademicMemberList = new List<PersonalAcademicMemberCenter>();
            string oCompanyCode = oRequestParameter.Requestor.CompanyCode;
            string EmpCode = !string.IsNullOrEmpty(oRequestParameter.InputParameter["EmpCode"].ToString()) ? oRequestParameter.InputParameter["EmpCode"].ToString() : string.Empty;
            oAcademicMemberList = HRPAManagement.CreateInstance(oCompanyCode).GetAcedemicworkmember(EmpCode);
            return oAcademicMemberList;

        }

        #endregion

        #region Punishment
        [HttpPost]
        public object GetPunishmentEmployee([FromBody] RequestParameter oRequestParameter)
        {
            List<PersonalPunishmentCenter> oPunishmentList = new List<PersonalPunishmentCenter>();
            string oCompanyCode = oRequestParameter.Requestor.CompanyCode,
                oEmployeeID = oRequestParameter.Requestor.EmployeeID;

            string oStatus = "";
            try
            {
                oPunishmentList = HRPAManagement.CreateInstance(oCompanyCode).GetPunishmentList(oEmployeeID);
                oStatus = "Success";
            }
            catch (Exception ex)
            {
                oStatus = "Error GetPunishmentEmployee : " + ex.InnerException;
            }

            object oResult = new
            {
                status = oStatus,
                PunishmentList = oPunishmentList
            };

            return oResult;

        }
        #endregion

        //Use function for CV Report
        [HttpPost]
        public object GetEmployeeAllData([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                CommonText oCommonText = new CommonText(oRequestParameter.Requestor.CompanyCode);
                string oLang = oRequestParameter.CurrentEmployee.Language;
                PersonalData oPersonalData;// = new PersonalData();
                string ImageUrl = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).PHOTO_PATH, oRequestParameter.Requestor.EmployeeID);

                DateSpecificData oSpecificDate = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDateSpecificData(oRequestParameter.Requestor.EmployeeID);
                DateTime HiringDate = oSpecificDate.HiringDate; //Requestor.DateSpecific.HiringDate;
                DateTime RetirementDate = oSpecificDate.RetirementDate; //Requestor.DateSpecific.RetirementDate;
                DateTime StartWorkdate = oSpecificDate.StartWorkingDate;

                DateTime oCheckDate = DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd"), "yyyy-MM-dd", enCL);
                PersonalInfoCenter personalInfo = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalInfo(oRequestParameter.Requestor.EmployeeID, oCheckDate);
                List<TitleName> Title = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllTitleName(oRequestParameter.Requestor.Language);

                var queryTitle = (from oTitle in Title
                                  where oTitle.Key == personalInfo.TitleID
                                  select new { oTitle.Description }).First();
                var queryTitleEn = (from oTitle in Title
                                    where oTitle.Key == personalInfo.TitleIDEn
                                    select new { oTitle.Description }).First();

                oPersonalData = new PersonalData
                {
                    EmployeeID = oRequestParameter.Requestor.EmployeeID,
                    BeginDate = personalInfo.BeginDate,
                    EndDate = personalInfo.EndDate,
                    TitleID = queryTitle.Description,//personalInfo.TitleID,
                    FirstName = oLang == "TH" ? personalInfo.FirstName : personalInfo.FirstNameEn,
                    LastName = oLang == "TH" ? personalInfo.LastName : personalInfo.LastNameEn,
                    NickName = personalInfo.NickName,
                    TitleIDEn = queryTitleEn.Description,
                    FullNameEn = personalInfo.FirstNameEn + personalInfo.LastNameEn, //oINF182[i].AlternateName,
                    DOB = personalInfo.DOB,
                    Gender = personalInfo.Gender,
                    Nationality = personalInfo.Nationality,
                    MaritalStatus = personalInfo.MaritalStatus,
                    Religion = personalInfo.Religion,
                    Language = personalInfo.Language
                };

                string AgePeriod = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCalculatePeriod(personalInfo.DOB, DateTime.Now, oRequestParameter.Requestor.Language);
                DateTime dtDOB = Convert.ToDateTime(personalInfo.DOB);
                DateTime dtNow = DateTime.Now;

                //int now = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
                //int dob = int.Parse(dtDOB.ToString("yyyyMMdd"));
                //string dif = (now - dob).ToString();
                //string Age = "0";
                //if (dif.Length > 4)
                //    Age = dif.Substring(0, dif.Length - 4) +  oCommonText.LoadText("CVREPORT", oRequestParameter.CurrentEmployee.Language, "YEARS");

                DateTime oBeginDate = !oRequestParameter.InputParameter.ContainsKey("BeginDate") ? DateTime.MinValue : DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
                ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0001 oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).INFOTYPE0001Get(oRequestParameter.Requestor.EmployeeID, oBeginDate, oRequestParameter.Requestor.Language);
                OrganizationData oResultOraData = Convert<OrganizationData>.ObjectFrom(oINF1);


                RelatedAge EmpRelatedAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRelatedAge(oRequestParameter.Requestor.EmployeeID, DateTime.Now);
                string ageOfCompany = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfCompany, oRequestParameter.Requestor.Language);
                //string ageOfLevel = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfLevel, oRequestParameter.Requestor.Language);
                string ageOfPosition = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfPosition, oRequestParameter.Requestor.Language);
                string ageOfOrganization = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfOrg, oRequestParameter.Requestor.Language);

                object oResult = new
                {
                    PersonData = oPersonalData,
                    ImageUrl = ImageUrl,
                    OrgData = oResultOraData,
                    StartWorkdate,
                    HiringDate,
                    RetirementDate,
                    AgePeriod,
                    //Age,
                    ageOfCompany,
                    ageOfPosition,
                    ageOfOrganization,
                    PositionName = oResultOraData.Position,//Requestor.CurrentPosition.Text,
                    OrgUnitName = oResultOraData.OrgName,//Requestor.OrgAssignment.OrgUnitData.Text,
                    //Section_Division = Suan + "/" + Phai,
                    AreaCode = oINF1.Area
                };

                return oResult;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }


        [HttpPost]
        public bool ShowTimeAwareLink([FromBody] RequestParameter oRequestParameter)
        {
            bool isShowTimeAwareLink = false;
            isShowTimeAwareLink = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ShowTimeAwareLink(Convert.ToInt32(oRequestParameter.InputParameter["LinkID"]));
            return isShowTimeAwareLink;
        }

        //private string GetOrganizationType(string OrgID, OrgLevelType orgType, string companyCode)
        //{
        //    string _orgid = OrgID;
        //    INFOTYPE1010 orgLevel = null;
        //    do
        //    {
        //        if ((OrgLevelType)Convert.ToInt16(OMManagement.CreateInstance(companyCode).GetLevelByOrg(_orgid, DateTime.Now).OrgLevel) == orgType)
        //        {
        //            orgLevel = OMManagement.CreateInstance(companyCode).GetLevelByOrg(_orgid, DateTime.Now);
        //            break;
        //        }

        //        else
        //        {
        //            try
        //            {
        //                _orgid = OMManagement.CreateInstance(companyCode).GetOrgParent(_orgid, DateTime.Now, "TH").ObjectID;

        //            }
        //            catch
        //            {
        //                _orgid = string.Empty;
        //            }
        //        }
        //    } while (!String.IsNullOrEmpty(_orgid));


        //    if (orgLevel != null)
        //        return OMManagement.CreateInstance(companyCode).GetObjectData(ObjectType.O, orgLevel.ObjectID, DateTime.Now, "TH").Text;

        //    else
        //        return "-";
        //}

        //[HttpPost]
        //public DataTable GetHealth([FromBody] RequestParameter oRequestParameter)
        //{
        //    string CompanyCode = oRequestParameter.Requestor.CompanyCode.ToString();
        //    DataTable dt = new DataTable();
        //    DataTable dtYear = new DataTable();
        //    dtYear.Columns.Add("Year", typeof(string));
        //    dt = HRPAManagement.CreateInstance(CompanyCode).GetConfigSelectYear();
        //    if (dt.Rows.Count > 0)
        //    {
        //        int iOldYear = Convert.ToInt32(dt.Rows[0]["Value"].ToString());
        //        int cYear = Convert.ToInt32(DateTime.Now.Year);
        //        for (int i = 0; i < iOldYear; i++)
        //        {
        //            dtYear.Rows.Add(cYear - i);
        //        }
        //    }

        //    return dtYear;
        //}

        [HttpPost]
        public DataTable GetHealthYear([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.Requestor.CompanyCode.ToString();
            //DataTable dt = new DataTable();
            DataTable dtYear = new DataTable();
            dtYear.Columns.Add("Year", typeof(string));
            //dt = HRPAManagement.CreateInstance(CompanyCode).GetConfigSelectYear();

            //if (dt.Rows.Count > 0) GetHealthRecordStartYear
            //{
            int iOldYear = HRPAManagement.CreateInstance(CompanyCode).GetHealthRecordStartYear();// Convert.ToInt32(dt.Rows[0]["Value"].ToString());
            int cYear = Convert.ToInt32(DateTime.Now.Year);
            while (cYear >= iOldYear)
            {
                dtYear.Rows.Add(cYear.ToString());
                cYear--;
            }
            //

            return dtYear;
        }

        [HttpPost]
        public object GetHealthForYear([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.Requestor.CompanyCode.ToString();
            string LanguageCode = oRequestParameter.Requestor.Language;
            string EmployeeID = oRequestParameter.Requestor.EmployeeID;
            int Year = Convert.ToInt32(oRequestParameter.InputParameter["Year"]);
            List<PAMedicalCheckup> Health = new List<PAMedicalCheckup>();
            Health = HRPAManagement.CreateInstance(CompanyCode).GetMedicalCheckup_Year_Empid(Year.ToString(), EmployeeID);
            object oReturn = new
            {
                Health,
            };
            return oReturn;
        }

        [HttpPost]
        public DataTable GetTableControl([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.Requestor.CompanyCode.ToString();
            return HRPAManagement.CreateInstance(CompanyCode).GetConfigTableControl();
        }
        [HttpPost]
        public DataTable GetOrgUnitAll([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            return HRPAManagement.CreateInstance(CompanyCode).GetAllOrgUnit(CompanyCode);
        }
        //[HttpPost]
        //public List<RoleSetting> GetDataSetAdmin([FromBody] RequestParameter oRequestParameter)
        //{

        //    List<RoleSetting> RoleSetting = new List<RoleSetting>();
        //    string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
        //    RoleSetting = HRPAManagement.CreateInstance(CompanyCode).GetDataSetAdmin();
        //    return RoleSetting;
        //}

        [HttpPost]
        public DataTable GetUserRoleSetting([FromBody] RequestParameter oRequestParameter)
        {

            DataTable RoleSetting = new DataTable();
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            RoleSetting = HRPAManagement.CreateInstance(CompanyCode).GetUserRoleSetting();
            return RoleSetting;
        }

        //[HttpPost]
        //public string SaveRoleSetting1([FromBody] RequestParameter oRequestParameter)
        //{
        //    List<RoleSetting> oRole = new List<RoleSetting>();
        //    oRole = JSon.Deserialize<List<RoleSetting>>(oRequestParameter.InputParameter["DataRoleSetting"].ToString());

        //    return "Success";
        //}

        [HttpPost]
        public DataTable GetRoleSetting([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oRole = new DataTable();
            return HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRoleSetting();
        }

        [HttpPost]
        public string SaveRoleSetting([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oRole = new DataTable();
            oRole = JSon.Deserialize<DataTable>(oRequestParameter.InputParameter["DataRoleSetting"].ToString());
            return HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).SaveUserRoleSetting(oRole, oRequestParameter.Requestor.Language);
        }


        //#region Get Data Content
        //[HttpPost]
        //public object GetEmployeeList([FromBody] RequestParameter oRequestParameter)
        //{
        //    List<EmployeeDataCenter> oData = new List<EmployeeDataCenter>();
        //    string LevelSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["LevelSelected"].ToString()) ? oRequestParameter.InputParameter["LevelSelected"].ToString() : string.Empty;
        //    DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
        //    DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
        //    string UnitSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["UnitSelected"].ToString()) ? oRequestParameter.InputParameter["UnitSelected"].ToString() : string.Empty;
        //    string PositionSelected = !string.IsNullOrEmpty(oRequestParameter.InputParameter["PositionSelected"].ToString()) ? oRequestParameter.InputParameter["PositionSelected"].ToString() : string.Empty;

        //    oData = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEmployeeList(LevelSelected, UnitSelected, PositionSelected, BeginDate, EndDate);

            
        //    return oData;
        //}





        [HttpPost]
        public Object GetEvaluateEmployee([FromBody] RequestParameter oRequestParameter)
        {

            List<EvaluateDataCenter> oEvaluate = new List<EvaluateDataCenter>();
            List<INFOTYPE0008> oSalary = new List<INFOTYPE0008>();
            List<INFOTYPE0008> oBonus = new List<INFOTYPE0008>();
            List<EvaluateSetting> oEvaSetting = new List<EvaluateSetting>();
            string ReturnStatus = "Success";
            string oCompany = oRequestParameter.Requestor.CompanyCode.ToString();
            string oEmployeeID = oRequestParameter.Requestor.EmployeeID.ToString();
            try
            {
                List<EvaluateDataCenter> tmpEvaluate = HRPAManagement.CreateInstance(oCompany).GetEvaluateEmployee(oEmployeeID);
                oSalary = HRPAManagement.CreateInstance(oCompany).GetSalaryList(oEmployeeID);
                //oBonus = HRPYManagement.CreateInstance(oCompany).GetBonusList(oEmployeeID);
                oEvaSetting = HRPAManagement.CreateInstance(oCompany).GetEvaSetting();

                foreach (var item in oEvaSetting)
                {
                    List<EvaluateDataCenter> temp = new List<EvaluateDataCenter>();
                    temp = tmpEvaluate.FindAll(a => a.Period == item.Year.ToString());
                    if (temp != null)
                    {
                        oEvaluate.AddRange(temp);
                    }
                }

            }
            catch (Exception ex)
            {
                ReturnStatus = "Error GetEvaluateEmployee : " + ex.InnerException;
            }


            object oResult = new
            {
                oEvaluate,
                oSalary,
                oBonus,
                ReturnStatus
            };

            return oResult;
        }

        [HttpPost]
        public string GetStatusForResult([FromBody] RequestParameter oRequestParameter)
        {
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            string RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
            return HRPAManagement.CreateInstance(CompanyCode).GetStatusForResult(RequestNo);
        }
        [HttpPost]
        public List<OrganizationData> GetAllEmployeeNameForUserRole([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oEmployeeData = new EmployeeDataForMobile();
            if (oRequestParameter.Requestor != null && oRequestParameter.Requestor.EmployeeID != null)
                oEmployeeData = oRequestParameter.Requestor;
            else if (oRequestParameter.CurrentEmployee != null && oRequestParameter.CurrentEmployee.EmployeeID != null)
                oEmployeeData = oRequestParameter.CurrentEmployee;

            WorkflowController oWorkflowController = new WorkflowController();
            oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);
            List<INFOTYPE0001> lstEmployeeData = new List<INFOTYPE0001>();
            List<OrganizationData> lstOrgAssignment = new List<OrganizationData>();
            OrganizationData oOrgData = new OrganizationData();
            if (oEmployeeData != null && oEmployeeData.EmployeeID != null)
            {
                try
                {
                    DateTime oCheckDate = DateTime.Now;
                    string oCompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
                    lstEmployeeData = EmployeeManagement.CreateInstance(oCompanyCode).GetAllEmployeeNameForUserRole(oRequestParameter.CurrentEmployee.Language);
                    if (lstEmployeeData != null)
                    {
                        lstEmployeeData.ForEach(inf1 =>
                        {
                            lstOrgAssignment.Add(Convert<OrganizationData>.ObjectFrom(inf1));
                            /*oOrgData = new OrganizationData();
                            oOrgData.EmployeeID = inf1.EmployeeID;
                            oOrgData.Name = inf1.Name;
                            lstOrgAssignment.Add(oOrgData);*/
                        });
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                }
            }
            return lstOrgAssignment;
        }

        [HttpPost]
        public DataTable GetAllResponseCodeByResponseType([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oReturn = new DataTable();
            string ResponseType = oRequestParameter.InputParameter["ResponseType"].ToString();
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;

            return HRPAManagement.CreateInstance(CompanyCode).GetAllResponseCodeByResponseType(ResponseType, CompanyCode, LanguageCode); ;
        }

        #region Evaluate Setting
        [HttpPost]
        public string GetEvaluateStartYear([FromBody] RequestParameter oRequestParameter)
        {
            return HRPAServices.SVC(oRequestParameter.CurrentEmployee.CompanyCode).PA_GetEvaluate_Year_Start;
        }

        [HttpPost]
        public DataTable GetEvaluateSettingYear([FromBody] RequestParameter oRequestParameter)
        {
            DataTable YearList = new DataTable();
            YearList = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEvaluateSettingYear(oRequestParameter.CurrentEmployee.Language);
            return YearList;
        }

        [HttpPost]
        public List<EvaluateSetting> GetEvaSetting([FromBody] RequestParameter oRequestParameter)
        {
            return HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEvaSetting();
        }

        [HttpPost]
        public List<EvaluateSetting> GetEvaluateSetting([FromBody] RequestParameter oRequestParameter)
        {
            return HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEvaluateSetting();
        }

        [HttpPost]
        public object EvaluateSetting([FromBody] RequestParameter oRequestParameter)
        {
            string oException = string.Empty;
            string companyCode = oRequestParameter.Requestor.CompanyCode;
            string languageCode = oRequestParameter.CurrentEmployee.Language;
            List<EvaluateSetting> EVASettingList = JsonConvert.DeserializeObject<List<EvaluateSetting>>(oRequestParameter.InputParameter["EVASettingList"].ToString());
            EvaluateSetting periodSetting = new EvaluateSetting();
            try
            {
                EvaluateSetting tmpreturn = new EvaluateSetting();
                periodSetting = JsonConvert.DeserializeObject<EvaluateSetting>(oRequestParameter.InputParameter["EVAPeriodSetting"].ToString());
                var endDate = periodSetting.EndDate;
                periodSetting.EndDate = endDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                periodSetting.UpdateByEmployeeID = oRequestParameter.Requestor.EmployeeID;
                periodSetting.UpdateByPositionID = oRequestParameter.Requestor.PositionID;
                if (IsOverLapPeriodSetting(EVASettingList, periodSetting, companyCode, out tmpreturn))
                {
                    CommonText oCommon = new CommonText(companyCode);
                    throw new Exception(oCommon.LoadText("HRPA_EXCEPTION", languageCode, "OVERLAP_SETTING") + " | " + tmpreturn.BeginDate.ToString("dd-MM-yyyy") + " - " + tmpreturn.EndDate.ToString("dd-MM-yyyy"));
                    //throw new Exception(HRPYManagement.CreateInstance(companyCode).GetCommonText("HRPA_EXCEPTION", languageCode, "OVERLAP_SETTING") + " | " + tmpreturn.BeginDate.ToString("dd-MM-yyyy") + " - " + tmpreturn.EndDate.ToString("dd-MM-yyyy"));
                }
                EVASettingList.Add(periodSetting);
            }
            catch (Exception e)
            {
                string strError = e.Message.ToString();
                oException = strError;
                string[] strTextCode = strError.Split('|');
                if (oException.Contains("{0}"))
                {
                    oException = oException.Replace("{0}", strTextCode[1]);
                }
            }

            object oReturn = new
            {
                oException,
                EVASettingList
            };
            return oReturn;
        }

        [HttpPost]
        public List<EvaluateSetting> GetEvaluateSettingByYear([FromBody] RequestParameter oRequestParameter)
        {
            int Year = Convert.ToInt32(oRequestParameter.InputParameter["YearSetting"].ToString());
            List<EvaluateSetting> periodSetting = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEvaluateSettingByYear(Year);

            return periodSetting;
        }

        [HttpPost]
        public List<EvaluateSetting> GetEvaluateSettingHistory([FromBody] RequestParameter oRequestParameter)
        {
            JObject yearHis = JObject.Parse(oRequestParameter.InputParameter["YearHistory"].ToString());
            int Year = yearHis["Year"].Value<int>();
            List<EvaluateSetting> periodSetting = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEvaluateSettingHistory(Year);

            return periodSetting;
        }

        [HttpPost]
        public object SaveEvaSetting([FromBody] RequestParameter oRequestParameter)
        {
            List<EvaluateSetting> oEva = new List<EvaluateSetting>();
            string oException = string.Empty;
            string saveSuccess = "Success";
            string companyCode = oRequestParameter.Requestor.CompanyCode;
            string languageCode = oRequestParameter.CurrentEmployee.Language;
            List<EvaluateSetting> EVASettingList = new List<EvaluateSetting>();
            try
            {
                EVASettingList = JsonConvert.DeserializeObject<List<EvaluateSetting>>(oRequestParameter.InputParameter["EvaSetting"].ToString());
                if (EVASettingList == null || EVASettingList.Count == 0)
                {
                    throw new Exception(HRPAManagement.CreateInstance(companyCode).GetCommonText("HRPA_EXCEPTION", languageCode, "DATAATLEAST1"));
                }
                EVASettingList.ToList().ForEach(a =>
                {
                    var endDate = a.EndDate.Date;
                    a.UpdateByEmployeeID = oRequestParameter.Requestor.EmployeeID;
                    a.UpdateByPositionID = oRequestParameter.Requestor.PositionID;
                });
                HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).SaveEvaSetting(EVASettingList);
            }
            catch (Exception ex)
            {
                oException = ex.Message;
            }

            object oReturn = new
            {
                saveSuccess,
                oException
            };
            return oReturn;
        }

        private bool IsOverLapPeriodSetting(List<EvaluateSetting> EVASettingList, EvaluateSetting PeriodSetting, string companyCode, out EvaluateSetting tmpreturn)
        {
            bool isOverlap = false;
            tmpreturn = null;
            DateTimeIntersec oDateTimeIntersec;
            //ภายในปีเดียวกัน ห้ามคาบเกี่ยวช่วงวันเวลา ระหว่างปีคาบเกี่ยวได้
            foreach (var item in EVASettingList)
            {
                oDateTimeIntersec = new DateTimeIntersec();
                oDateTimeIntersec.Begin1 = PeriodSetting.BeginDate;
                oDateTimeIntersec.End1 = PeriodSetting.EndDate;
                oDateTimeIntersec.Begin2 = item.BeginDate;
                oDateTimeIntersec.End2 = item.EndDate;
                if (oDateTimeIntersec.IsIntersecOT())
                {
                    isOverlap = true;
                    tmpreturn = item;
                    break;
                }
            }
            int year = (int)PeriodSetting.Year;

            return isOverlap;
        }
        #endregion

        //2020-10-16
        [HttpPost]
        public object GetLastedDocByFlowItemCode([FromBody] RequestParameter oRequestParameter)
        {
            int requestTypeID = 606;
            string FlowItemCode = oRequestParameter.InputParameter["FlowItemCode"].ToString();
            DataTable lastedDoc = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetLastedDocByFlowItemCode(oRequestParameter.Requestor.EmployeeID, requestTypeID, FlowItemCode);
            object oReturn = new
            {
                lastedDoc
            };
            return oReturn;
        }

        [HttpPost]
        public List<PhoneDirectory> getPhoneDirectoryByCriteria([FromBody] RequestParameter oRequestParameter)
        {
            List<PhoneDirectory> oReturn;
            string oLanguageCode = oRequestParameter.InputParameter["LANGUAGE"].ToString();
            string oSearchText = Convert.ToString(oRequestParameter.InputParameter["SEARCHTEXT"]);
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;

            try
            {
                oReturn = HRPAManagement.CreateInstance(CompanyCode).GetPhoneDirectoryByCriteria(oSearchText, oLanguageCode);

            }
            catch (Exception ex)
            {
                oReturn = new List<PhoneDirectory>();
            }

            return oReturn;
        }

        [HttpPost]
        public string generateImageQRCode([FromBody] RequestParameter oRequestParameter)
        {

            string QRCode = String.Empty;
            string LanguageCode = oRequestParameter.InputParameter["LANGUAGE"].ToString();
            PhoneDirectory oDict = JsonConvert.DeserializeObject<PhoneDirectory>(oRequestParameter.InputParameter["PHONECARD"].ToString());


            string mainName = LanguageCode == "TH" ? oDict.NameTH : oDict.NameEN;

            QRCoder.PayloadGenerator.ContactData generator;

            generator = new QRCoder.PayloadGenerator.ContactData(
                QRCoder.PayloadGenerator.ContactData.ContactOutputType.VCard3
                , mainName
                , null
                , oDict.NickName
                , null
                , oDict.CELLPHONE
                , oDict.OFFICENUMBER
                , oDict.MAIL
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , null
                , new PayloadGenerator.ContactData.AddressOrder());

            string payload = generator.ToString();

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(payload, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            var qrCodeAsBitmap = qrCode.GetGraphic(20);

            System.Web.UI.WebControls.Image imgbarcode = new System.Web.UI.WebControls.Image();
            imgbarcode.Height = 200;
            imgbarcode.Width = 200;
            using (Bitmap bitmap = qrCodeAsBitmap)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    imgbarcode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                    QRCode = imgbarcode.ImageUrl;
                }
                //= imgbarcode;
            }

            return QRCode;
        }

        [HttpGet, HttpPost]
        public object ExportPhoneDirectory([FromBody] RequestParameter oRequestParameter)
        {
            List<PhoneDirectory> oPDList;
            string oLanguageCode = oRequestParameter.InputParameter["LANGUAGE"].ToString();
            string oSearchText = Convert.ToString(oRequestParameter.InputParameter["SEARCHTEXT"]);
            string CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;

            string oReportName = oRequestParameter.InputParameter["ReportName"].ToString();

            oPDList = HRPAManagement.CreateInstance(CompanyCode).GetPhoneDirectoryByCriteria(oSearchText, oLanguageCode); //oDTTemplate;

            DataTable tblPhoneDirectoryReport = new DataTable();
            #region tblPhoneDirectoryReport
            tblPhoneDirectoryReport.Columns.Add("EmployeeID");
            tblPhoneDirectoryReport.Columns.Add("EmployeeName");
            tblPhoneDirectoryReport.Columns.Add("Nickname");
            tblPhoneDirectoryReport.Columns.Add("OrgUnit");
            tblPhoneDirectoryReport.Columns.Add("OrgName");
            tblPhoneDirectoryReport.Columns.Add("Position");
            tblPhoneDirectoryReport.Columns.Add("PositionName");
            tblPhoneDirectoryReport.Columns.Add("HOMENUMBER");
            tblPhoneDirectoryReport.Columns.Add("OFFICEPHONE");
            tblPhoneDirectoryReport.Columns.Add("CELLPHONE");
            tblPhoneDirectoryReport.Columns.Add("LINEID");
            tblPhoneDirectoryReport.Columns.Add("MAIL");
            tblPhoneDirectoryReport.Columns.Add("PERSONALMAIL");
            tblPhoneDirectoryReport.Columns.Add("USERID");
            tblPhoneDirectoryReport.Columns.Add("EMERGENCYPERSON");
            tblPhoneDirectoryReport.Columns.Add("EMERGENCYPHONE");
            tblPhoneDirectoryReport.Columns.Add("PRIMARYWEBSITE");
            tblPhoneDirectoryReport.Columns.Add("SECONDARYWEBSITE");
            #endregion tblPhoneDirectoryReport


            foreach (PhoneDirectory pdData in oPDList)
            {

                DataRow row = tblPhoneDirectoryReport.NewRow();
                row["EmployeeID"] = pdData.EmployeeID;
                row["EmployeeName"] = oLanguageCode == "TH" ? pdData.NameTH : pdData.NameEN;
                row["Nickname"] = pdData.NickName;
                row["OrgUnit"] = pdData.OrgUnit;
                row["OrgName"] = pdData.OrgName;
                row["Position"] = pdData.Position;
                row["PositionName"] = pdData.PositionName;
                row["HOMENUMBER"] = pdData.HOMENUMBER;
                row["OFFICEPHONE"] = pdData.OFFICENUMBER;
                row["CELLPHONE"] = pdData.CELLPHONE;
                row["LINEID"] = pdData.LINEID;
                row["MAIL"] = pdData.MAIL;
                row["PERSONALMAIL"] = pdData.PERSONALMAIL;
                row["USERID"] = pdData.USERID;
                row["EMERGENCYPERSON"] = pdData.EMERGENCYPERSON;
                row["EMERGENCYPHONE"] = pdData.EMERGENCYPHONE;
                row["PRIMARYWEBSITE"] = pdData.PRIMARYWEBSITE;
                row["SECONDARYWEBSITE"] = pdData.SECONDARYWEBSITE;
                tblPhoneDirectoryReport.Rows.Add(row);
            }

            var Employee_id = oRequestParameter.CurrentEmployee.EmployeeID;
            var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();

            string companyLogo = CompanyCode;// "0010";
            string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;


            try
            {

                ReportViewer oReportViewer = new ReportViewer();
                oReportViewer.LocalReport.DataSources.Clear();
                oReportViewer.ProcessingMode = ProcessingMode.Local;
                oReportViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PA/PhoneDirectoryReport.rdlc");  //oReportObject.ReportName;
                oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PADataset_PhoneDirectory", tblPhoneDirectoryReport));

                List<ReportParameter> parameters = new List<ReportParameter>();
                parameters.Add(new ReportParameter("Header", oReportName));

                oReportViewer.LocalReport.SetParameters(parameters);
                oReportViewer.LocalReport.EnableExternalImages = true;
                oReportViewer.ShowRefreshButton = false;

                ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = oReportViewer.LocalReport.Render(
                    ExportType, null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                string oFilePath = ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "ESS.WORKFLOW", "REPORT_PHONEDIRECTORY");
                oFilePath += oRequestParameter.CurrentEmployee.CompanyCode + "/";
                string oMapPath = System.Web.HttpContext.Current.Server.MapPath(oFilePath);

                bool exists = System.IO.Directory.Exists(oMapPath);
                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(oMapPath);
                }

                string oFileName = string.Format("{0}{1}{2}", oReportName, "_", oSearchText) + "." + filenameExtension;
                string oFullPath = oFilePath + oFileName;
                FileManager.SaveFile(System.Web.HttpContext.Current.Server.MapPath(oFullPath), bytes, true);


                object oReturn = new
                {
                    FileName = oFileName,
                    URL = oFullPath.Replace("~/", "")
                };

                return oReturn;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
        }

        [HttpPost]
        public List<Document> GetDocumentTypeAllForDocumentInfo([FromBody] RequestParameter oRequestParameter)
        {
            List<Document> oResult = new List<Document>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllDocumentList(oRequestParameter.CurrentEmployee.Language);

            return oResult;
        }

        [HttpPost]
        public List<ValidityType> GetValidityTypeAllForDocumentInfo([FromBody] RequestParameter oRequestParameter)
        {
            List<ValidityType> oResult = new List<ValidityType>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllVISAList(oRequestParameter.CurrentEmployee.Language);

            return oResult;
        }

        //Nattawat S. 2021-02-22
        [HttpPost]
        public object GetINFOTYPE1001AllRelation([FromBody] RequestParameter oRequestParameter)
        {
            List<ESS.EMPLOYEE.CONFIG.OM.INFOTYPE1001> oRelation = new List<ESS.EMPLOYEE.CONFIG.OM.INFOTYPE1001>();
            oRelation = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1001GetAllHistory();
            List<ESS.EMPLOYEE.CONFIG.OM.INFOTYPE1000> oOrganizationData = new List<ESS.EMPLOYEE.CONFIG.OM.INFOTYPE1000>();
            oOrganizationData = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE1000GetAllHistory();

            object oResult = new
            {
                Relation = oRelation,
                OrganizationData = oOrganizationData
            };

            return oResult;
        }

        [HttpPost]
        public List<AcademicSubType> GetAcademicSubTypeAll([FromBody] RequestParameter oRequestParameter)
        {
            List<AcademicSubType> oResult = new List<AcademicSubType>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllAcademicSubTypeList(oRequestParameter.CurrentEmployee.Language);
            return oResult;
        }

        [HttpPost]
        public List<AcademicCategoryType> GetAcademicCategoryTypeAll([FromBody] RequestParameter oRequestParameter)
        {
            List<AcademicCategoryType> oResult = new List<AcademicCategoryType>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllAcademicCategoryTypeList(oRequestParameter.CurrentEmployee.Language);
            return oResult;
        }

        [HttpPost]
        public List<AcademicRole> GetAcademicRoleAll([FromBody] RequestParameter oRequestParameter)
        {
            List<AcademicRole> oResult = new List<AcademicRole>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllAcademicRoleList(oRequestParameter.CurrentEmployee.Language);
            return oResult;
        }





        #region dam
        [HttpPost]
        public object GetDrpDLL([FromBody] RequestParameter oRequestParameter)
        {
            string DataType = !string.IsNullOrEmpty(oRequestParameter.InputParameter["Type"].ToString()) ? oRequestParameter.InputParameter["Type"].ToString() : string.Empty;
            DateTime BeginDate = DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            DateTime EndDate = DateTime.ParseExact(oRequestParameter.InputParameter["EndDate"].ToString(), "yyyy-MM-dd", null);
            string param1 = !string.IsNullOrEmpty(oRequestParameter.InputParameter["param1"].ToString()) ? oRequestParameter.InputParameter["param1"].ToString() : null;
            string param2 = !string.IsNullOrEmpty(oRequestParameter.InputParameter["param2"].ToString()) ? oRequestParameter.InputParameter["param2"].ToString() : null;
            string param3 = string.Empty;
            if (DataType == "PACO")
            {
                param1 = oRequestParameter.Requestor.CompanyCode;
                param2 = !string.IsNullOrEmpty(oRequestParameter.InputParameter["param1"].ToString()) ? oRequestParameter.InputParameter["param1"].ToString() : null;
                param3 = !string.IsNullOrEmpty(oRequestParameter.InputParameter["param2"].ToString()) ? oRequestParameter.InputParameter["param2"].ToString() : null;
            }

            //exec sp_DHR_UI_DllController 'PACO', null, '1900-01-01', '9999-12-31', '1000', '2018-05-01', '2019-12-31'
            var oDDL = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDropDownDLL(DataType, oRequestParameter.CurrentEmployee.Language, BeginDate, EndDate, param1, param2, param3);

            object oReturn = new
            {
                oDDL
            };
            return oReturn;
        }
        #endregion
    }
}