﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.SHAREDATASERVICE;
using ESS.SHAREDATASERVICE.DATACLASS;
using System.Web;
using System.Collections;
using System.Reflection;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShareController : ApiController
    {
        [HttpGet, HttpPost]
        public List<Company> GetCompanyList()
        {
            return ShareDataManagement.GetCompanyList();
        }

        [HttpGet, HttpPost]
        public Dictionary<string, string> GetEnvironment()
        {
            return ShareDataManagement.GetDatabaseLocation();
        }

        [HttpGet, HttpPost]
        public string ClearCache()
        {
            // Clear Config Cache
            ShareDataManagement.ClearConfigCache();

            // Clear Text Description Cache
            foreach (DictionaryEntry pair in HttpContext.Current.Cache)
            {
                HttpContext.Current.Cache.Remove((string)pair.Key);
            }
           
            return "clear cache completed";
        }

        [HttpGet]
        public string GetCurrentVersion() {
            string assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string fileVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
            string productVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            return productVersion;
        }
    }
}