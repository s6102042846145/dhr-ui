using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using System.Data;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;

namespace iSSWS.HttpFilters
{
    public class HTTPAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        private string authenticationScheme = "PTTQUOTAAPI", authenticationMode;
        private bool isAPIAllowStupidInsecureCall = true;
        private readonly Int64 reqMaxAgeInSeconds = 200;

        private string appID = "", nonce = "", signatureBase64 = "", reqTimestamp = "", viewerEmployeeID = "", viewerCompanyCode = "", docCompanyCode = "";
        private string apiMethod = "", appKey = "";
        private int authorizationHeaderPartCount = 0, apiVersion = 2;
        private bool isStupidInsecureMode = false, isHttpAuthOK = false, isAppAuthOK = false;

        private Encoding enc = Encoding.UTF8;
        private string resourceDSN = "", viewerDSN = "";
        private DateTime TODAY = DateTime.Today;


        public HTTPAuthenticationAttribute()
        {
            // Nothing to construct!
        }

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            string rawAuthorizationHeader = ""; //,tmp ="";
            Dictionary<string, string> SQLCONFIG = new Dictionary<string, string>();
            Dictionary<string, string> companyDSN = new Dictionary<string, string>();
            HttpRequestMessage req = context.Request;
            AuthenticationHeaderValue authorization = req.Headers.Authorization;

            if (authorization == null)
            {
                context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
            }
            else
            {
                if (authenticationScheme.Equals(req.Headers.Authorization.Scheme, StringComparison.OrdinalIgnoreCase))
                {
                    rawAuthorizationHeader = req.Headers.Authorization.Parameter;
                    string[] authorizationHeaderArray = GetAuthorizationHeaderArray(rawAuthorizationHeader);
                    authorizationHeaderPartCount = authorizationHeaderArray.Count();

                    if (authorizationHeaderPartCount < 3)
                    {
                        context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                    }
                    else
                    {
                        viewerEmployeeID = authorizationHeaderArray[1];
                        viewerEmployeeID = enc.GetString(Convert.FromBase64String(viewerEmployeeID));
                        viewerCompanyCode = viewerEmployeeID.Substring(0,4);
                        viewerEmployeeID = viewerEmployeeID.Substring(4);
                        if (isAPIAllowStupidInsecureCall && authorizationHeaderArray.Count() == 3)
                        {
                            // Fallback to STUPID mode
                            isStupidInsecureMode = true;
                            apiVersion = 1;
                            appID = authorizationHeaderArray[0];
                        }
                        else
                        {
                        	isStupidInsecureMode = false;
                            appID = authorizationHeaderArray[3];
                            signatureBase64 = authorizationHeaderArray[2];
                            nonce = authorizationHeaderArray[0];
                            reqTimestamp = authorizationHeaderArray[4];
                        }
                    }

                    viewerDSN = ShareDataManagement.LookupCache(viewerCompanyCode, "CONNECTION", "DB_CONNECTION");

                    // use c# 6.0 initializer, so VS2013 can not understand below syntax!
                    Dictionary<string, object> PARAMS = new Dictionary<string, object>()
                    {
                        ["p_AppID"] = string.Format("0x{0}", appID),
                        ["p_APIVersion"] = apiVersion
                    };
                    if (isStupidInsecureMode)
                    {
                        PARAMS["p_IsStupidInsecure"] = 1;
                    }
                    SQLCONFIG["BaseConnStr"] = viewerDSN;
                    SQLCONFIG["ProcedureName"] = "sp_TR_GetResourceAPIRegistration";
                    DataTable dt = DatabaseHelper.ExecuteQuery(SQLCONFIG, PARAMS);

                    if (dt.Rows.Count > 0)
                    {
                        authenticationScheme = dt.Rows[0]["AuthenticationScheme"].ToString();
                        authenticationMode = dt.Rows[0]["AuthenticationType"].ToString();
                        apiMethod = dt.Rows[0]["Method"].ToString();
                        appKey = dt.Rows[0]["APIKey"].ToString();
                    }
                    else
                    {
                        context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                    }

                    switch (authenticationMode)
                    {
                        case "STUPID":
                            isHttpAuthOK = IsHttpAuthOK(req, appID);
                            break;
                        case "HMAC256":
                            isHttpAuthOK = IsHttpAuthOK(req, appID, nonce, reqTimestamp);
                            break;
                    }

                    isAppAuthOK = IsAppAuthOK();
                    if (isHttpAuthOK && isAppAuthOK)
                    {
                        var principalIdentity = new GenericPrincipal(new GenericIdentity(appID), null);
                        context.Principal = principalIdentity;
                        context.Request.Properties.Add("resourceDSN", viewerDSN);
                        context.Request.Properties.Add("viewerEmployeeID", viewerEmployeeID);
                        context.Request.Properties.Add("viewerCompanyCode", viewerCompanyCode);
                    }
                    else
                    {
                        context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                    }
                }
                else
                {
                    context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                }
            }

            return Task.FromResult(0);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            context.Result = new ResultWithChallenge(context.Result);
            return Task.FromResult(0);
        }

        public bool AllowMultiple
        {
            get { return false; }
        }

        private string[] GetAuthorizationHeaderArray(string rawAuthorizationHeader)
        {
            var authorizationHeaderArray = rawAuthorizationHeader.Split(':');
            return authorizationHeaderArray;
        }

        private bool IsHttpAuthOK(HttpRequestMessage req, string appID, string nonce="", string reqTimestamp="")
        {
            string reqUri = HttpUtility.UrlEncode(req.RequestUri.AbsoluteUri.ToLower());
            string reqHttpMethod = req.Method.ToString();
            string reqAuthExpected = "-- Hey!, I know who you are! --";
            string reqAuth = req.Headers.Authorization.Parameter;
            string textB4Sign = "", textSigned = "", textSignedBase64 = "", viewerIDBase64 = "";
            byte[] textB4SignByte, appKeyByte, textSignedByte;
            bool isOK = false;
            char bel = (char)7;

            if (appKey == "")
            {
                return isOK;
            }

            viewerIDBase64 = Convert.ToBase64String(enc.GetBytes(String.Format("{0}{1}", viewerCompanyCode, viewerEmployeeID)));
            switch (authenticationMode)
            {
                case "STUPID":
                    reqAuthExpected = String.Format("{0}:{1}:{2}", appID, viewerIDBase64, appKey);
                    break;
                case "HMAC256":
                    textB4Sign = String.Format("{0}{6}{1}{6}{2}{6}{3}{6}{4}{6}{5}", nonce, appID, viewerIDBase64, apiMethod, reqUri, reqTimestamp, bel);
                    textB4SignByte = enc.GetBytes(textB4Sign);
                    appKeyByte = enc.GetBytes(appKey);

                    using (HMACSHA256 hmac = new HMACSHA256(appKeyByte))
                    {
                        textSignedByte = hmac.ComputeHash(textB4SignByte);
                        textSigned = enc.GetString(textSignedByte);
                        textSignedBase64 = Convert.ToBase64String(textSignedByte);
                    }
                    reqAuthExpected = String.Format("{0}:{1}:{2}:{3}:{4}", nonce, viewerIDBase64, textSignedBase64, appID, reqTimestamp);
                    break;
            }

            isOK = reqAuth.Equals(reqAuthExpected, StringComparison.Ordinal);
            // Thanatip S. (hong) - 08 1659-2346
            // August 28, 2019 @ 17:10
            // Timing Attack ?! Not sure about StringComparison.Ordinal
            //      have to check further to prevent Timing Attack

            if (isOK && !isStupidInsecureMode)
            {
                if (IsReplayRequest(nonce, reqTimestamp))
                {
                    isOK = false;
                }
            }

            return isOK;
        }

        private bool IsAppAuthOK()
        {
            Dictionary<string, string> SQLCONFIG = new Dictionary<string, string>()
            {
                ["BaseConnStr"] = viewerDSN,
                ["ProcedureName"] = "sp_ValidateAppAuth"
            };
            Dictionary<string, object> PARAMS = new Dictionary<string, object>
            {
                ["@p_EmployeeID"] = viewerEmployeeID,
                ["@p_CheckDate"] = TODAY
            };

            return DatabaseHelper.ExecuteScalarToBoolean(SQLCONFIG, PARAMS);
        }

        // For future use in HMAC Authentication
        // I have to convince client (I am not sure that PI or Chat Bot in this case)
        //      to use HMAC Authentication instead of stupid API Key
        // To do so, I have to give them some implementation guidelines
        //      for instance, nonce implementation
        // Thanatip S. (hong) - 08-1659-2346
        // August 20, 2019 @ 08:45
        // 
        // Updated on September 29, 2019
        // After a long time
        //		PI annouce that they can bypass header as I suggested before
        // (I tried to convince them that it is so common that integrater or adapter should be done like this)
        // And from annoucement of TE Authentication Methodology v2 (via email)
        //		chatbot implements it without problem (only  a glitch on beginning)
        private bool IsReplayRequest(string nonce, string reqTimeStamp)
        {
            if (System.Runtime.Caching.MemoryCache.Default.Contains(nonce))
            {
                return true;
            }

            DateTimeOffset utcNow = DateTimeOffset.UtcNow;
            Int64 serverNowTimeStamp = ((DateTimeOffset)utcNow).ToUnixTimeSeconds();
            Int64 requestTimeStamp = Convert.ToInt64(reqTimeStamp);

            if ((serverNowTimeStamp - requestTimeStamp) > reqMaxAgeInSeconds)
            {
                return true;
            }

            System.Runtime.Caching.MemoryCache.Default.Add(nonce, reqTimeStamp, DateTimeOffset.UtcNow.AddSeconds(reqMaxAgeInSeconds));
            return false;
        }

        // This digest stream function also for digest message verification
        //      will be used in nearly future along with HMAC Authentiaction scheme
        // Thanatip S. (hong) - 08-1659-2346
        // August 20, 2019 @ 08:45
        private static async Task<byte[]> MD5Stream(HttpContent httpContent)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = null;
                var content = await httpContent.ReadAsByteArrayAsync();
                if (content.Length != 0)
                {
                    hash = md5.ComputeHash(content);
                }
                return hash;
            }
        }

        public class ResultWithChallenge : IHttpActionResult
        {
            private string authenticationScheme = "PTTQUOTAAPI";
            private IHttpActionResult next;

            public ResultWithChallenge(IHttpActionResult next)
            {
                this.next = next;
            }

            public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response = await next.ExecuteAsync(cancellationToken);
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue(authenticationScheme));
                }
                return response;
            }
        }
    }
}
