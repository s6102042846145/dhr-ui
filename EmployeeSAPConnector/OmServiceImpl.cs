using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Reflection;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.SHAREDATASERVICE;
using SAP.Connector;
using SAPInterface;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.SAP
{
    public class OmServiceImpl : AbstractOMService
    {
        #region Constructor
        public OmServiceImpl(string oCompanyCode)
        {
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.EMPLOYEE.SAP";

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member
        private CultureInfo DefaultCultureInfo
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new CultureInfo("en-US");
                }
                return WorkflowPrinciple.Current.UserSetting.CultureInfo;
            }
        }
        private string DefaultDateFormat
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return ShareDataManagement.LookupCache(CompanyCode, "ESS.EMPLOYEE", "DEFAULTDATEFORMAT");
                }
                return WorkflowPrinciple.Current.UserSetting.DateFormat;
            }
        }
        private DateTime MINIMUM_DATE_IN_SAP
        {
            get
            {
                return DateTime.ParseExact(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MINIMUM_DATE_IN_SAP"), DefaultDateFormat, DefaultCultureInfo);
            }
        }
        private string SAP_VERSION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");
            }
        }

        private bool IS_OBJECT_NAME_IN_DESCRIPTION
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "IS_OBJECT_NAME_IN_DESCRIPTION"));
            }
        }
        private string LANGUAGE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "LANGUAGE");
            }
        }

        #region " GetAllObject "

        public override List<INFOTYPE1513> GetAllJobIndex(string objId1, string objId2, string Profile)
        {
            List<INFOTYPE1513> oReturn = new List<INFOTYPE1513>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            
            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MGRP";
            Fields.Add(fld);
            
            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OTYPE = 'S'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OBJID >= '{0}'", objId1);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OBJID <= '{0}'", objId2);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA >= '{0}'", this.MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);
            #endregion " Options "

            #region " Excute "
            oFunction.Zrfc_Read_Table("^", "", "HRP1513", 0, 0, ref Data, ref Fields, ref Options);
            #endregion

            #region " ParseObject "
            foreach (TAB512 line in Data)
            {
                INFOTYPE1513 oInf = new INFOTYPE1513();
                string[] arrTemp = line.Wa.Split('^');
                oInf.ObjectID = arrTemp[0];
                try
                {
                    oInf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", DefaultCultureInfo);
                } catch (Exception ex)
                {
                    oInf.BeginDate = new DateTime(1900, 1, 1);
                }
                try
                {
                    oInf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                }
                catch (Exception ex)
                {
                    oInf.EndDate = new DateTime(1900, 1, 1);
                }
                oInf.MainGroup = arrTemp[3];
                oReturn.Add(oInf);
            }
            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);

            return oReturn;
        }
        public override List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Mode)
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            TAB512Table Data1 = new TAB512Table();
            TAB512Table Data2 = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Data "

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OTYPE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SHORT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND LANGU = '{0}'", LANGUAGE);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA >= '{0}'", this.MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            if (objectTypes.Count > 0)
            {
                string cOtype = "";

                foreach (ObjectType item in objectTypes)
                {
                    cOtype += string.Format(",'{0}'", item.ToString());
                }

                cOtype = cOtype.TrimStart(',');

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND OTYPE IN ( {0} )", cOtype);
                Options.Add(opt);
            }

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "HRP1000", 0, 0, ref Data, ref Fields, ref Options);

            #endregion " Data "

            Fields = new RFC_DB_FLDTable();
            Options = new RFC_DB_OPTTable();

            #region " Data1 "

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OTYPE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TABNR";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND LANGU = '{0}' AND SUBTY = '0003'", LANGUAGE);
            Options.Add(opt);

            if (objectTypes.Count > 0)
            {
                string cOtype = "";

                foreach (ObjectType item in objectTypes)
                {
                    cOtype += string.Format(",'{0}'", item.ToString());
                }

                cOtype = cOtype.TrimStart(',');

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND OTYPE IN ( {0} )", cOtype);
                Options.Add(opt);
            }

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "HRP1002", 0, 0, ref Data1, ref Fields, ref Options);

            #endregion " Data1 "

            Fields = new RFC_DB_FLDTable();
            Options = new RFC_DB_OPTTable();

            #region " Data2 "

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TABNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TABSEQNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TLINE";
            Fields.Add(fld);

            #endregion " Fields "

            oFunction.Zrfc_Read_Table("^", "", "HRT1002", 0, 0, ref Data2, ref Fields, ref Options);
            oFunction.Dispose();

            #endregion " Data2 "

            #region " ParseObject "

            #region " ParseTable1 "

            Dictionary<string, DataTable> Dict1 = new Dictionary<string, DataTable>();
            DataTable oTable1;
            DataTable oTable1_Sch = new DataTable();
            oTable1_Sch.Columns.Add("OTYPE", typeof(string));
            oTable1_Sch.Columns.Add("OBJID", typeof(string));
            oTable1_Sch.Columns.Add("BEGDA", typeof(DateTime));
            oTable1_Sch.Columns.Add("ENDDA", typeof(DateTime));
            oTable1_Sch.Columns.Add("TABNR", typeof(string));
            foreach (TAB512 data in Data1)
            {
                string[] arrTemp = data.Wa.Split('^');
                string cKey = string.Format("{0}|{1}", arrTemp[0].Trim(), arrTemp[1].Trim());
                if (!Dict1.ContainsKey(cKey))
                {
                    oTable1 = oTable1_Sch.Clone();
                    Dict1.Add(cKey, oTable1);
                }
                else
                {
                    oTable1 = Dict1[cKey];
                }
                DataRow oRow = oTable1.NewRow();
                //fld.Fieldname = "OTYPE";
                oRow["OTYPE"] = arrTemp[0].Trim();
                //fld.Fieldname = "OBJID";
                oRow["OBJID"] = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                oRow["BEGDA"] = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "ENDDA";
                oRow["ENDDA"] = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "SHORT";
                oRow["TABNR"] = arrTemp[4].Trim();
                oTable1.Rows.Add(oRow);
            }
            Data1 = null;

            #endregion " ParseTable1 "

            #region " ParseTable2 "

            Dictionary<string, string> Dict2 = new Dictionary<string, string>();
            DataTable oTable2 = new DataTable();
            oTable2.Columns.Add("TABNR", typeof(string));
            oTable2.Columns.Add("TABSEQNR", typeof(int));
            oTable2.Columns.Add("TLINE", typeof(string));
            string[] arrTemp2;
            string cKey2;

            foreach (TAB512 data in Data2)
            {
                //string[] arrTemp = data.Wa.Split('^');
                //string cKey = string.Format("{0}|{1}", arrTemp[0].Trim(), int.Parse(arrTemp[1].Trim()));
                arrTemp2 = data.Wa.Split('^');
                cKey2 = string.Format("{0}|{1}", arrTemp2[0].Trim(), int.Parse(arrTemp2[1].Trim()));
                if (!Dict2.ContainsKey(cKey2))
                {
                    Dict2.Add(cKey2, arrTemp2[2].Trim());
                }
            }
            Data2 = null;

            #endregion " ParseTable2 "

            int index = -1;
            //Console.WriteLine("{0} rows", Data.Count); //By Kiattiwat 02-02-2012 for resolved job 4
            foreach (TAB512 data in Data)
            {
                index++;
                //Console.WriteLine(index); By Kiattiwat 02-02-2012 for resolved job 4
                string[] arrTemp = data.Wa.Split('^');

                INFOTYPE1000 Item = new INFOTYPE1000();
                //fld.Fieldname = "OTYPE";
                Item.ObjectType = (ObjectType)Enum.ToObject(typeof(ObjectType), arrTemp[0].Trim()[0]);
                //fld.Fieldname = "OBJID";
                Item.ObjectID = arrTemp[1].Trim();

                //fld.Fieldname = "BEGDA";
                Item.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "ENDDA";
                Item.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "SHORT";
                Item.ShortText = arrTemp[4].Trim();
                Item.ShortTextEn = arrTemp[4].Trim();
                //fld.Fieldname = "STEXT";
                Item.Text = arrTemp[5].Trim();
                Item.TextEn = arrTemp[5].Trim();
                string cKey = string.Format("{0}|{1}", arrTemp[0].Trim(), arrTemp[1].Trim());
                if (Dict1.ContainsKey(cKey))
                {
                    foreach (DataRow dr in Dict1[cKey].Rows)
                    {
                        DateTime date1, date2;
                        date1 = (DateTime)dr["BEGDA"];
                        date2 = (DateTime)dr["ENDDA"];
                        if (date1 < Item.EndDate && date2 > Item.BeginDate)
                        {
                            string Text = "";
                            string TextEn = "";

                            string cTABNR = (string)dr["TABNR"];
                            string cTextKey;
                            cTextKey = string.Format("{0}|{1}", cTABNR, 1);
                            if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
                            {
                                Text += Dict2[cTextKey].Trim();
                            }
                            cTextKey = string.Format("{0}|{1}", cTABNR, 2);
                            if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
                            {
                                Text += Dict2[cTextKey].Trim();
                            }
                            if (Text != "")
                            {
                                Item.Text = Text;
                            }

                            cTextKey = string.Format("{0}|{1}", cTABNR, 3);
                            if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
                            {
                                TextEn += Dict2[cTextKey].Trim();
                            }
                            cTextKey = string.Format("{0}|{1}", cTABNR, 4);
                            if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
                            {
                                TextEn += Dict2[cTextKey].Trim();
                            }

                            if (TextEn != "")
                            {
                                Item.TextEn = TextEn;
                            }
                            /*
                            cTextKey = string.Format("{0}|{1}", cTABNR, 5);
                            if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
                            {
                                int nBuffer = 0;
                                int.TryParse(Dict2[cTextKey].Trim(), out nBuffer);
                                Item.Level = nBuffer;
                            }
                             */
                        }
                    }
                }
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Data = null;

            Connection.ReturnConnection(oConnection);

            return oReturn;
        }

        public List<string> GetDescripton(string objectType, string objectID, string subtype, DateTime checkDate, string mode)
        {
            List<string> oReturn = new List<string>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;
            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data1 = new TAB512Table();
            TAB512Table Data2 = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Get TextReference "

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TABNR";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OTYPE = '{0}' AND OBJID = '{1}'", objectType, objectID);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", checkDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND LANGU = '{0}' AND SUBTY = '{1}'", LANGUAGE, subtype);
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "HRP1002", 0, 0, ref Data1, ref Fields, ref Options);

            foreach (TAB512 desc in Data1)
            {
                string[] arrTemp = desc.Wa.Split('^');
                string tabnr = arrTemp[0];

                #region " Get TextLine "

                Fields = new RFC_DB_FLDTable();
                Options = new RFC_DB_OPTTable();

                #region " Fields "

                fld = new RFC_DB_FLD();
                fld.Fieldname = "TLINE";
                Fields.Add(fld);

                #endregion " Fields "

                #region " Options "

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("TABNR = '{0}'", tabnr);
                Options.Add(opt);

                #endregion " Options "

                oFunction.Zrfc_Read_Table("^", "", "HRT1002", 0, 0, ref Data2, ref Fields, ref Options);

                foreach (TAB512 text in Data2)
                {
                    oReturn.Add(text.Wa.Split('^')[0]);
                }

                #endregion " Get TextLine "
            }
            oFunction.Dispose();
            Connection.ReturnConnection(oConnection);

            #endregion " Get TextReference "

            return oReturn;
        }

        //public override List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Mode)
        //{
        //    List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

        //    Connection oConnection = Connection.GetConnectionFromPool(GetConnStr(Mode));
        //    RFCREADTABLE6 oFunction = null;

        //    oFunction = new RFCREADTABLE6();
        //    oFunction.Connection = oConnection;

        //    TAB512Table Data = new TAB512Table();
        //    TAB512Table Data1 = new TAB512Table();
        //    TAB512Table Data2 = new TAB512Table();
        //    RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

        //    RFC_DB_FLD fld;
        //    RFC_DB_OPT opt;

        //    #region " Data "

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "OTYPE";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "OBJID";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "BEGDA";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "ENDDA";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "SHORT";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "STEXT";
        //    Fields.Add(fld);

        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("PLVAR = '01' AND ISTAT = '1'");
        //    Options.Add(opt);

        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("AND LANGU = '2'");
        //    Options.Add(opt);

        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("AND OBJID >= '{0}' AND OBJID <= '{1}'",objId1,objId2);
        //    Options.Add(opt);

        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("AND BEGDA >= '18000000'");
        //    Options.Add(opt);

        //    if (objectTypes.Count > 0)
        //    {
        //        string cOtype = "";

        //        foreach (ObjectType item in objectTypes)
        //        {
        //            cOtype += string.Format(",'{0}'", (char)item);
        //        }

        //        cOtype = cOtype.TrimStart(',');

        //        opt = new RFC_DB_OPT();
        //        opt.Text = string.Format("AND OTYPE IN ( {0} )", cOtype);
        //        Options.Add(opt);
        //    }
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "HRP1000", 0, 0, ref Data, ref Fields, ref Options);

        //    #endregion

        //    Fields = new RFC_DB_FLDTable();
        //    Options = new RFC_DB_OPTTable();

        //    #region " Data1 "

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "OTYPE";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "OBJID";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "BEGDA";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "ENDDA";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "TABNR";
        //    Fields.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("PLVAR = '01' AND ISTAT = '1'");
        //    Options.Add(opt);

        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
        //    Options.Add(opt);

        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("AND LANGU = '2' AND SUBTY = '0003'");
        //    Options.Add(opt);

        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("AND OBJID >= '{0}' AND OBJID <= '{1}'", objId1, objId2);
        //    Options.Add(opt);

        //    if (objectTypes.Count > 0)
        //    {
        //        string cOtype = "";

        //        foreach (ObjectType item in objectTypes)
        //        {
        //            cOtype += string.Format(",'{0}'", (char)item);
        //        }

        //        cOtype = cOtype.TrimStart(',');

        //        opt = new RFC_DB_OPT();
        //        opt.Text = string.Format("AND OTYPE IN ( {0} )", cOtype);
        //        Options.Add(opt);
        //    }
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "HRP1002", 0, 0, ref Data1, ref Fields, ref Options);

        //    #endregion

        //    string strTABNR = string.Empty;

        //    #region " ParseTable1 "
        //    Dictionary<string, DataTable> Dict1 = new Dictionary<string, DataTable>();
        //    DataTable oTable1;
        //    DataTable oTable1_Sch = new DataTable();
        //    oTable1_Sch.Columns.Add("OTYPE", typeof(string));
        //    oTable1_Sch.Columns.Add("OBJID", typeof(string));
        //    oTable1_Sch.Columns.Add("BEGDA", typeof(DateTime));
        //    oTable1_Sch.Columns.Add("ENDDA", typeof(DateTime));
        //    oTable1_Sch.Columns.Add("TABNR", typeof(string));

        //    foreach (TAB512 data in Data1)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        string cKey = string.Format("{0}|{1}", arrTemp[0].Trim(), arrTemp[1].Trim());
        //        if (!Dict1.ContainsKey(cKey))
        //        {
        //            oTable1 = oTable1_Sch.Clone();
        //            Dict1.Add(cKey, oTable1);
        //        }
        //        else
        //        {
        //            oTable1 = Dict1[cKey];
        //        }
        //        DataRow oRow = oTable1.NewRow();
        //        //fld.Fieldname = "OTYPE";
        //        oRow["OTYPE"] = arrTemp[0].Trim();
        //        //fld.Fieldname = "OBJID";
        //        oRow["OBJID"] = arrTemp[1].Trim();
        //        //fld.Fieldname = "BEGDA";
        //        oRow["BEGDA"] = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
        //        //fld.Fieldname = "ENDDA";
        //        oRow["ENDDA"] = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
        //        //fld.Fieldname = "SHORT";
        //        oRow["TABNR"] = arrTemp[4].Trim();
        //        strTABNR += "'"+arrTemp[4].Trim() + "',";
        //        oTable1.Rows.Add(oRow);
        //    }

        //    strTABNR = strTABNR.TrimEnd(',');
        //    Data1 = null;
        //    #endregion

        //    Fields = new RFC_DB_FLDTable();
        //    Options = new RFC_DB_OPTTable();

        //    #region " Data2 "

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "TABNR";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "TABSEQNR";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "TLINE";
        //    Fields.Add(fld);
        //    #endregion

        //    #region " Options "
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "HRT1002", 0, 0, ref Data2, ref Fields, ref Options);
        //    oFunction.Dispose();

        //    #endregion

        //    #region " ParseTable2 "
        //    Dictionary<string, string> Dict2 = new Dictionary<string, string>();
        //    DataTable oTable2 = new DataTable();
        //    oTable2.Columns.Add("TABNR", typeof(string));
        //    oTable2.Columns.Add("TABSEQNR", typeof(int));
        //    oTable2.Columns.Add("TLINE", typeof(string));
        //    string[] arrTemp2;
        //    string cKey2;

        //    foreach (TAB512 data in Data2)
        //    {
        //        //string[] arrTemp = data.Wa.Split('^');
        //        //string cKey = string.Format("{0}|{1}", arrTemp[0].Trim(), int.Parse(arrTemp[1].Trim()));
        //        arrTemp2 = data.Wa.Split('^');
        //        int iLine = int.Parse(arrTemp2[1].Trim());
        //        if (!strTABNR.Contains(arrTemp2[0].Trim()))
        //            continue;
        //        cKey2 = string.Format("{0}|{1}", arrTemp2[0].Trim(), iLine);
        //        if (!Dict2.ContainsKey(cKey2) && !String.IsNullOrEmpty(arrTemp2[2].Trim()))
        //        {
        //            Dict2.Add(cKey2, arrTemp2[2].Trim());
        //        }
        //    }
        //    Data2 = null;
        //    #endregion

        //    #region " ParseObject "

        //    int index = -1;
        //    Console.WriteLine("{0} rows", Data.Count); //By Kiattiwat 02-02-2012 for resolved job 4
        //    foreach (TAB512 data in Data)
        //    {
        //        index++;
        //        //Console.WriteLine(index); By Kiattiwat 02-02-2012 for resolved job 4
        //        string[] arrTemp = data.Wa.Split('^');

        //        INFOTYPE1000 Item = new INFOTYPE1000();
        //        //fld.Fieldname = "OTYPE";
        //        Item.ObjectType = (ObjectType)Enum.ToObject(typeof(ObjectType), arrTemp[0].Trim()[0]);
        //        //fld.Fieldname = "OBJID";
        //        Item.ObjectID = arrTemp[1].Trim();

        //        //fld.Fieldname = "BEGDA";
        //        Item.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
        //        //fld.Fieldname = "ENDDA";
        //        Item.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
        //        //fld.Fieldname = "SHORT";
        //        Item.ShortText = arrTemp[4].Trim();
        //        Item.ShortTextEn = arrTemp[4].Trim();
        //        //fld.Fieldname = "STEXT";
        //        Item.Text = arrTemp[5].Trim();
        //        Item.TextEn = arrTemp[5].Trim();
        //        string cKey = string.Format("{0}|{1}", arrTemp[0].Trim(), arrTemp[1].Trim());
        //        if (Dict1.ContainsKey(cKey))
        //        {
        //            foreach (DataRow dr in Dict1[cKey].Rows)
        //            {
        //                DateTime date1, date2;
        //                date1 = (DateTime)dr["BEGDA"];
        //                date2 = (DateTime)dr["ENDDA"];
        //                if (date1 < Item.EndDate && date2 > Item.BeginDate)
        //                {
        //                    string Text = "";
        //                    string TextEn = "";

        //                    string cTABNR = (string)dr["TABNR"];
        //                    string cTextKey;
        //                    cTextKey = string.Format("{0}|{1}", cTABNR, 1);
        //                    if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
        //                    {
        //                        Text += Dict2[cTextKey].Trim();
        //                    }
        //                    cTextKey = string.Format("{0}|{1}", cTABNR, 2);
        //                    if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
        //                    {
        //                        Text += Dict2[cTextKey].Trim();
        //                    }
        //                    if (Text != "")
        //                    {
        //                        Item.Text = Text;
        //                    }

        //                    cTextKey = string.Format("{0}|{1}", cTABNR, 3);
        //                    if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
        //                    {
        //                        TextEn += Dict2[cTextKey].Trim();
        //                    }
        //                    cTextKey = string.Format("{0}|{1}", cTABNR, 4);
        //                    if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
        //                    {
        //                        TextEn += Dict2[cTextKey].Trim();
        //                    }

        //                    if (TextEn != "")
        //                    {
        //                        Item.TextEn = TextEn;
        //                    }
        //                    /*
        //                    cTextKey = string.Format("{0}|{1}", cTABNR, 5);
        //                    if (Dict2.ContainsKey(cTextKey) && Dict2[cTextKey].Trim() != "")
        //                    {
        //                        int nBuffer = 0;
        //                        int.TryParse(Dict2[cTextKey].Trim(), out nBuffer);
        //                        Item.Level = nBuffer;
        //                    }
        //                     */
        //                }
        //            }
        //        }
        //        oReturn.Add(Item);
        //    }
        //    #endregion

        //    Dict1.Clear();
        //    Dict2.Clear();
        //    Dict1 = null;
        //    Dict2 = null;
        //    Data = null;

        //    Connection.ReturnConnection(oConnection);

        //    return oReturn;
        //}

        public override List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Mode)
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Data "

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OTYPE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SHORT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STEXT";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND LANGU = '{0}'", LANGUAGE);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OBJID >= '{0}' AND OBJID <= '{1}'", objId1, objId2);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA >= '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            if (objectTypes.Count > 0)
            {
                string cOtype = "";

                foreach (ObjectType item in objectTypes)
                {
                    cOtype += string.Format(",'{0}'", item.ToString());
                }

                cOtype = cOtype.TrimStart(',');

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND OTYPE IN ( {0} )", cOtype);
                Options.Add(opt);
            }

            #endregion " Options "

            if (SAP_VERSION == "6")
            {
                oFunction.Zrfc_Read_Table("^", "", "HRP1000", 0, 0, ref Data, ref Fields, ref Options);
            }

            #endregion " Data "

            Fields = new RFC_DB_FLDTable();
            Options = new RFC_DB_OPTTable();

            #region " ParseObject "

            //Console.WriteLine("{0} rows", Data.Count);

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');

                INFOTYPE1000 Item = new INFOTYPE1000();
                //fld.Fieldname = "OTYPE";
                Item.ObjectType = (ObjectType)Enum.Parse(typeof(ObjectType), arrTemp[0].Trim(), true);

                //fld.Fieldname = "OBJID";
                Item.ObjectID = arrTemp[1].Trim();

                //fld.Fieldname = "BEGDA";
                Item.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "ENDDA";
                Item.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "SHORT";
                Item.ShortText = arrTemp[4].Trim();
                Item.ShortTextEn = arrTemp[4].Trim();
                //fld.Fieldname = "STEXT";
                Item.Text = arrTemp[5].Trim();
                Item.TextEn = arrTemp[5].Trim();

                if (IS_OBJECT_NAME_IN_DESCRIPTION)
                {
                    List<string> descLine = GetDescripton(((char)Item.ObjectType).ToString(), Item.ObjectID, "0003", Item.EndDate, Mode);

                    if (descLine.Count > 0)
                    {
                        Item.Text = descLine[0];
                        if (descLine.Count > 1)
                        {
                            Item.Text += descLine[1];
                            if (descLine.Count > 2)
                            {
                                Item.TextEn = descLine[2];
                                if (descLine.Count > 3)
                                {
                                    Item.TextEn += descLine[3];
                                }
                            }
                        }
                    }
                }
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Data = null;

            Connection.ReturnConnection(oConnection);

            return oReturn;
        }

        #endregion " GetAllObject "

        #region " GetAllRelation "

        public override List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            List<INFOTYPE1001> list = new List<INFOTYPE1001>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OTYPE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PRIOX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCLAS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SOBID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PROZT";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("RSIGN = 'A' AND PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA > '19000000' AND OBJID >= '00000000' AND OBJID <= '00999999'");
            Options.Add(opt);

            if (objectTypes.Count > 0)
            {
                string cOType = "";
                foreach (ObjectType item in objectTypes)
                {
                    cOType += string.Format(",'{0}'", item.ToString());
                }
                cOType = cOType.TrimStart(',');

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND OTYPE IN ({0})", cOType);
                Options.Add(opt);
            }

            if (relationCodes.Count > 0)
            {
                string cRelation = "";
                foreach (string item in relationCodes)
                {
                    cRelation += string.Format(",'{0}'", item);
                }
                cRelation = cRelation.TrimStart(',');

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND SUBTY IN ({0})", cRelation);
                Options.Add(opt);
            }

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "HRP1001", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Dispose();

            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            int index = -1;
            foreach (TAB512 item in Data)
            {
                index++;
                //Console.WriteLine(index);
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE1001 inf = new INFOTYPE1001();
                //OTYPE
                inf.ObjectType = (ObjectType)Enum.Parse(typeof(ObjectType), arrTemp[0].Trim(), true);
                fld = new RFC_DB_FLD();
                //OBJID
                inf.ObjectID = arrTemp[1].Trim();
                //PRIOX
                inf.OrderKey = arrTemp[2].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[4], "yyyyMMdd", DefaultCultureInfo);
                //SUBTY
                inf.Relation = arrTemp[5].Trim();
                //SCLAS
                //inf.NextObjectType = arrTemp[6].Trim();
                inf.ObjectType = (ObjectType)Enum.Parse(typeof(ObjectType), arrTemp[6].Trim(), true);
                //SOBID
                inf.NextObjectID = arrTemp[7].Trim();
                //PROZT
                decimal buffer;
                if (decimal.TryParse(arrTemp[8].Trim(), out buffer))
                {
                    inf.PercentValue = buffer;
                }
                //else
                //{
                //    Console.WriteLine(arrTemp[8]);
                //}
                list.Add(inf);
            }

            #endregion " ParseToObject "

            return list;
        }

        public override List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            List<INFOTYPE1001> list = new List<INFOTYPE1001>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OTYPE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PRIOX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCLAS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SOBID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PROZT";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("RSIGN = 'A' AND PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA >= '{2}' AND OBJID >= '{0}' AND OBJID <= '{1}'", objId1, objId2, MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            if (objectTypes.Count > 0)
            {
                string cOType = "";
                foreach (ObjectType item in objectTypes)
                {
                    cOType += string.Format(",'{0}'", item.ToString());
                }
                cOType = cOType.TrimStart(',');

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND OTYPE IN ({0})", cOType);
                Options.Add(opt);
            }

            if (relationCodes.Count > 0)
            {
                string cRelation = "";
                foreach (string item in relationCodes)
                {
                    cRelation += string.Format(",'{0}'", item);
                }
                cRelation = cRelation.TrimStart(',');

                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND SUBTY IN ({0})", cRelation);
                Options.Add(opt);
            }

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "HRP1001", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Dispose();

            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            int index = -1;
            foreach (TAB512 item in Data)
            {
                index++;
                //Console.WriteLine(index);
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE1001 inf = new INFOTYPE1001();
                //OTYPE
                inf.ObjectType = (ObjectType)Enum.Parse(typeof(ObjectType), arrTemp[0].Trim(), true);
                //OBJID
                inf.ObjectID = arrTemp[1].Trim();
                //PRIOX
                inf.OrderKey = arrTemp[2].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[4], "yyyyMMdd", DefaultCultureInfo);
                //SUBTY
                inf.Relation = arrTemp[5].Trim();
                //SCLAS
                inf.NextObjectType = (ObjectType)Enum.Parse(typeof(ObjectType), arrTemp[6].Trim(), true);
                //SOBID
                inf.NextObjectID = arrTemp[7].Trim();
                //PROZT
                decimal buffer;
                if (decimal.TryParse(arrTemp[8].Trim(), out buffer))
                {
                    inf.PercentValue = buffer;
                }
                //else
                //{
                //    Console.WriteLine(arrTemp[8]);
                //}
                list.Add(inf);
            }

            #endregion " ParseToObject "

            //Console.WriteLine("{0} rows", Data.Count);
            return list;
        }

        #endregion " GetAllRelation "

        #region " GetAllPositionBandLevel "

        public override List<INFOTYPE1013> GetAllPositionBandLevel(string Profile)
        {
            List<INFOTYPE1013> list = new List<INFOTYPE1013>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSK";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = "OTYPE = 'S' AND PLVAR = '01' AND ISTAT = '1'";
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = "AND OBJID >= '23000000' AND OBJID <= '23999999'";
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "HRP1013", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Dispose();

            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            //Console.WriteLine("{0} rows", Data.Count);
            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE1013 inf = new INFOTYPE1013();
                //OBJID
                inf.ObjectID = arrTemp[0].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", DefaultCultureInfo);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //PERSG
                inf.EmpGroup = arrTemp[3];
                //PERSK
                inf.EmpSubGroup = arrTemp[4];
                list.Add(inf);
            }

            #endregion " ParseToObject "

            return list;
        }

        public override List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile)
        {
            List<INFOTYPE1013> list = new List<INFOTYPE1013>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSK";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = "OTYPE = 'S' AND PLVAR = '01' AND ISTAT = '1'";
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OBJID >= '{0}' AND OBJID <= '{1}'", objId1, objId2);
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "HRP1013", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Dispose();

            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            //Console.WriteLine("{0} rows", Data.Count);
            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE1013 inf = new INFOTYPE1013();
                //OBJID
                inf.ObjectID = arrTemp[0].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", DefaultCultureInfo);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //PERSG
                inf.EmpGroup = arrTemp[3];
                //PERSK
                inf.EmpSubGroup = arrTemp[4];
                list.Add(inf);
            }

            #endregion " ParseToObject "

            return list;
        }

        #endregion " GetAllPositionBandLevel "

        #region " GetAllApprovalData "

        public override List<INFOTYPE1003> GetAllApprovalData(string Profile)
        {
            List<INFOTYPE1003> list = new List<INFOTYPE1003>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OTYPE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STABS";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("OTYPE = 'S' AND PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = "AND OBJID >= '23000000' AND OBJID <= '23999999'";
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "HRP1003", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Dispose();

            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE1003 inf = new INFOTYPE1003();
                //OBJID
                inf.ObjectType = (ObjectType)Enum.ToObject(typeof(ObjectType), arrTemp[0].Trim()[0]);
                //OBJID
                inf.ObjectID = arrTemp[1].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
                //STABS
                inf.IsStaff = arrTemp[4].Trim() == "X";
                list.Add(inf);
            }

            #endregion " ParseToObject "

            return list;
        }

        public override List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile)
        {
            List<INFOTYPE1003> list = new List<INFOTYPE1003>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OTYPE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STABS";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("OTYPE = 'S' AND PLVAR = '01' AND ISTAT = '1'");
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OBJID >= '{0}' AND OBJID <= '{1}'", objId1, objId2);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA >= '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "HRP1003", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Dispose();

            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE1003 inf = new INFOTYPE1003();
                //OBJID
                inf.ObjectType = (ObjectType)Enum.ToObject(typeof(ObjectType), arrTemp[0].Trim()[0]);
                //OBJID
                inf.ObjectID = arrTemp[1].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", DefaultCultureInfo);
                //STABS
                inf.IsStaff = arrTemp[4].Trim() == "X";
                list.Add(inf);
            }

            #endregion " ParseToObject "

            return list;
        }

        #endregion " GetAllApprovalData "

        public override List<INFOTYPE1010> GetAllOrgUnitLevel(string ObjectID1, string ObjectID2, string Profile)
        {
            List<INFOTYPE1010> list = new List<INFOTYPE1010>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            if (SAP_VERSION == "6")
            {
                oFunction = new RFCREADTABLE6();
                oFunction.Connection = oConnection;
            }
            

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "HILFM";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("OBJID >= '{0}' AND OBJID <= '{1}'", ObjectID1, ObjectID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND OTYPE = 'O' AND SUBTY = '9000' AND ISTAT = '1'");
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA >= '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);
            #endregion " SetOption "

            #region " Execute "
            oFunction.Zrfc_Read_Table("^", "", "HRP1010", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Dispose();
            
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            //Console.WriteLine("{0} rows", Data.Count);
            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE1010 inf = new INFOTYPE1010();
                //OBJID
                inf.ObjectID = arrTemp[0].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", DefaultCultureInfo);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", DefaultCultureInfo);
                //HILFM
                inf.OrgLevel = arrTemp[3].Trim();
                list.Add(inf);
            }

            #endregion " ParseToObject "

            return list;
        }
    }
}