using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.SHAREDATASERVICE;
using SAP.Connector;
using SAPInterface;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.SAP
{
    public class EmployeeConfigServiceImpl : AbstractEmployeeConfigService
    {
        #region Constructor
        public EmployeeConfigServiceImpl(string oCompanyCode)
        {
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.EMPLOYEE.SAP";

        private CultureInfo DefaultCultureInfo
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new CultureInfo("en-US");
                }
                return WorkflowPrinciple.Current.UserSetting.CultureInfo;

            }
        }

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member




        //private string GetConnStr(string Profile)
        //{
        //    return ShareDataManager.LookupCache(CompanyCode, ModuleID, Profile.ToUpper()];
        //}

        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");
            }
        }


        #region " GetPersonalSubAreaSettingList "

        public override List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile)
        {
            List<PersonalSubAreaSetting> oReturn = new List<PersonalSubAreaSetting>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "WERKS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BTRTL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BTEXT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOLGA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOABW";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOZKO";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOFID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOSID";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("MOLGA = '26'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T001P", 0, 0, ref Data, ref Fields, ref Options);

            TAB512Table Data1 = new TAB512Table();
            RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOSID";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOTPR";
            Fields1.Add(fld);

            #endregion " Fields "

            oFunction.Rfc_Read_Table("^", "", "T508Z", 0, 0, ref Data1, ref Fields1, ref Options1);

            Dictionary<string, string> listText = new Dictionary<string, string>();
            foreach (TAB512 data in Data1)
            {
                string[] arrTemp = data.Wa.Split('^');
                listText.Add(string.Format("{0}", arrTemp[0].Trim()), arrTemp[1].Trim());
            }

            #region " ParseObject "

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                PersonalSubAreaSetting Item = new PersonalSubAreaSetting();
                //fld.Fieldname = "WERKS";
                Item.PersonalArea = arrTemp[0].Trim();
                //fld.Fieldname = "BTRTL";
                Item.PersonalSubArea = arrTemp[1].Trim();
                //fld.Fieldname = "BTEXT";
                Item.Description = arrTemp[2].Trim();
                //fld.Fieldname = "MOLGA";
                Item.CountryGrouping = arrTemp[3].Trim();
                //fld.Fieldname = "MOABW";
                Item.AbsAttGrouping = arrTemp[4].Trim();
                //fld.Fieldname = "MOZKO";
                Item.TimeQuotaGrouping = arrTemp[5].Trim();
                //fld.Fieldname = "MOFID";
                Item.HolidayCalendar = arrTemp[6].Trim();
                //fld.Fieldname = "MOSID";
                Item.WorkScheduleGrouping = arrTemp[7].Trim();

                if (listText.ContainsKey(Item.WorkScheduleGrouping))
                {
                    Item.DailyWorkScheduleGrouping = listText[Item.WorkScheduleGrouping];
                }
                else
                {
                    Item.DailyWorkScheduleGrouping = "";
                }

                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetPersonalSubAreaSettingList "

        #region " GetPersonalSubGroupSettingList "

        public override List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string Profile)
        {
            List<PersonalSubGroupSetting> oReturn = new List<PersonalSubGroupSetting>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSK";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZEITY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KONTY";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERSG >= 'A' AND PERSG <= 'Z'");
            Options.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T503", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                PersonalSubGroupSetting Item = new PersonalSubGroupSetting();
                //fld.Fieldname = "PERSG";
                Item.EmpGroup = arrTemp[0];
                //fld.Fieldname = "PERSK";
                Item.EmpSubGroup = arrTemp[1];
                //fld.Fieldname = "ZEITY";
                Item.WorkScheduleGrouping = arrTemp[2].Trim();
                //fld.Fieldname = "KONTY";
                Item.TimeQuotaTypeGrouping = arrTemp[3].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetPersonalSubGroupSettingList "

        #region " GetMonthlyWorkscheduleList "

        public override List<MonthlyWS> GetMonthlyWorkscheduleList(int Year, string Profile)
        {
            List<MonthlyWS> oReturn = new List<MonthlyWS>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZEITY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOFID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOSID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCHKZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KJAHR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MONAT";
            Fields.Add(fld);

            for (int counter = 1; counter <= 31; counter++)
            {
                fld = new RFC_DB_FLD();
                fld.Fieldname = string.Format("TPR{0}", counter.ToString("00"));
                Fields.Add(fld);

                fld = new RFC_DB_FLD();
                fld.Fieldname = string.Format("TTP{0}", counter.ToString("00"));
                Fields.Add(fld);
            }

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("KJAHR = {0}", Year);
            Options.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T552A", 0, 0, ref Data, ref Fields, ref Options);

            TAB512Table Data1 = new TAB512Table();
            RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZEITY";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOFID";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOSID";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCHKZ";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZMODN";
            Fields1.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("ENDDA >= {0}", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options1.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T508A", 0, 0, ref Data1, ref Fields1, ref Options1);

            Dictionary<string, string> listText = new Dictionary<string, string>();
            foreach (TAB512 data in Data1)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = string.Format("{0}#{1}#{2}#{3}", arrTemp[0].Trim(), arrTemp[1].Trim(), arrTemp[2].Trim(), arrTemp[3].Trim());
                if (listText.ContainsKey(key))
                {
                    System.Console.WriteLine("Duplicate T508A {0} => {1}", key, arrTemp[4].Trim());
                }
                listText[key] = arrTemp[4].Trim();
            }

            Data1 = new TAB512Table();
            Fields1 = new RFC_DB_FLDTable();
            Options1 = new RFC_DB_OPTTable();

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOTPR";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZMODN";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZMODK";
            Fields1.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("ENDDA >= {0}", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options1.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T551C", 0, 0, ref Data1, ref Fields1, ref Options1);

            Dictionary<string, string> listText1 = new Dictionary<string, string>();
            foreach (TAB512 data in Data1)
            {
                string[] arrTemp = data.Wa.Split('^');
                string key = string.Format("{0}#{1}", arrTemp[0].Trim(), arrTemp[1].Trim());
                if (listText1.ContainsKey(key))
                {
                    System.Console.WriteLine("Duplicate T551C {0} => {1}", key, arrTemp[2].Trim());
                }
                listText1[key] = arrTemp[2].Trim();
            }

            Data1 = new TAB512Table();
            Fields1 = new RFC_DB_FLDTable();
            Options1 = new RFC_DB_OPTTable();

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOSID";
            Fields1.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOTPR";
            Fields1.Add(fld);

            #endregion " Fields "

            oFunction.Rfc_Read_Table("^", "", "T508Z", 0, 0, ref Data1, ref Fields1, ref Options1);

            Dictionary<string, string> listText2 = new Dictionary<string, string>();
            foreach (TAB512 data in Data1)
            {
                string[] arrTemp = data.Wa.Split('^');
                listText2.Add(string.Format("{0}", arrTemp[0].Trim()), arrTemp[1].Trim());
            }

            #region " ParseObject "

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                MonthlyWS Item = new MonthlyWS();
                //fld.Fieldname = "ZEITY";
                Item.EmpSubGroupForWorkSchedule = arrTemp[0].Trim();
                //fld.Fieldname = "MOFID";
                Item.PublicHolidayCalendar = arrTemp[1].Trim();
                //fld.Fieldname = "MOSID";
                Item.EmpSubAreaForWorkSchedule = arrTemp[2].Trim();
                //fld.Fieldname = "SCHKZ";
                Item.WorkScheduleRule = arrTemp[3].Trim();
                //fld.Fieldname = "KJAHR";
                Item.WS_Year = int.Parse(arrTemp[4].Trim());
                //fld.Fieldname = "MONAT";
                Item.WS_Month = int.Parse(arrTemp[5].Trim());
                string cMonth;
                for (int index = 1; index <= 31; index++)
                {
                    cMonth = index.ToString("00");
                    System.Reflection.PropertyInfo oProp;
                    oProp = Item.GetType().GetProperty(string.Format("Day{0}", cMonth));
                    oProp.SetValue(Item, arrTemp[5 + 2 * index - 1].Trim(), null);

                    oProp = Item.GetType().GetProperty(string.Format("Day{0}_H", cMonth));
                    oProp.SetValue(Item, arrTemp[5 + 2 * index].Trim(), null);
                }

                // find periodWorkSchedule (ZMODN) from listText (T508A)
                string cT508A_KEY = string.Format("{0}#{1}#{2}#{3}", Item.EmpSubGroupForWorkSchedule, Item.PublicHolidayCalendar, Item.EmpSubAreaForWorkSchedule, Item.WorkScheduleRule);
                string periodWorkSchedule = "";
                if (listText.ContainsKey(cT508A_KEY))
                {
                    periodWorkSchedule = listText[cT508A_KEY];
                }
                else
                {
                    continue;
                }

                // find subAreaGroupingForDailyWS (MOTPR) from listText2 (T508Z)
                string cT508Z_KEY = string.Format("{0}", Item.EmpSubAreaForWorkSchedule);
                string subAreaGroupingForDailyWS = "";
                if (listText2.ContainsKey(cT508Z_KEY))
                {
                    subAreaGroupingForDailyWS = listText2[cT508Z_KEY];
                }

                // find valuationClass (ZMODK) from listText1 (T551C)
                string cT551C_KEY = string.Format("{0}#{1}", subAreaGroupingForDailyWS, periodWorkSchedule);
                string valuationClass = "";
                if (listText1.ContainsKey(cT551C_KEY))
                {
                    valuationClass = listText1[cT551C_KEY];
                }

                Item.ValuationClass = valuationClass;

                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetMonthlyWorkscheduleList "

        #region " GetDailyWorkscheduleList "

        public override List<DailyWS> GetDailyWorkscheduleList(string Profile)
        {
            List<DailyWS> oReturn = new List<DailyWS>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOTPR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TPROG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SOLLZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SOBEG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SOEND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NOBEG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NOEND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TPKLA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PAMOD";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            RFC_DB_OPT opt;

            opt = new RFC_DB_OPT();
            opt.Text = "VARIA = ' ' AND ENDDA > '20120630'";
            Options.Add(opt);

            #endregion " Options "

            oFunction.Rfc_Read_Table("^", "", "T550A", 0, 0, ref Data, ref Fields, ref Options);

            #region " ParseObject "

            foreach (TAB512 data in Data)
            {
                string[] arrTemp = data.Wa.Split('^');
                DailyWS Item = new DailyWS();
                //fld.Fieldname = "MOTPR";
                Item.DailyWorkscheduleGrouping = arrTemp[0].Trim();
                //fld.Fieldname = "TPROG";
                Item.DailyWorkscheduleCode = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                Item.BeginDate = DateTime.ParseExact(arrTemp[2].Trim(), "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "ENDDA";
                Item.EndDate = DateTime.ParseExact(arrTemp[3].Trim(), "yyyyMMdd", DefaultCultureInfo);
                //fld.Fieldname = "SOLLZ";
                Item.WorkHours = decimal.Parse(arrTemp[4].Trim());
                string buffer;
                DateTime value;
                value = DateTime.MinValue;
                //fld.Fieldname = "SOBEG";
                buffer = arrTemp[5].Trim();
                if (buffer == "240000")
                {
                    Item.WorkBeginTime = new TimeSpan(0, 0, 0);
                }
                else
                {
                    DateTime.TryParseExact(buffer, "HHmmss", DefaultCultureInfo, DateTimeStyles.None, out value);
                    if (value != DateTime.MinValue)
                    {
                        Item.WorkBeginTime = value.TimeOfDay;
                    }
                    else
                    {
                        Item.WorkBeginTime = TimeSpan.MinValue;
                    }
                }
                value = DateTime.MinValue;
                //fld.Fieldname = "SOEND";
                buffer = arrTemp[6].Trim();
                if (buffer == "240000")
                {
                    Item.WorkEndTime = new TimeSpan(0, 0, 0);
                }
                else
                {
                    DateTime.TryParseExact(buffer, "HHmmss", DefaultCultureInfo, DateTimeStyles.None, out value);
                    if (value != DateTime.MinValue)
                    {
                        Item.WorkEndTime = value.TimeOfDay;
                    }
                    else
                    {
                        Item.WorkEndTime = TimeSpan.MinValue;
                    }
                }
                value = DateTime.MinValue;
                //fld.Fieldname = "NOBEG";
                buffer = arrTemp[7].Trim();
                if (buffer == "240000")
                {
                    Item.NormalBeginTime = new TimeSpan(0, 0, 0);
                }
                else
                {
                    DateTime.TryParseExact(buffer, "HHmmss", DefaultCultureInfo, DateTimeStyles.None, out value);
                    if (value != DateTime.MinValue)
                    {
                        Item.NormalBeginTime = value.TimeOfDay;
                    }
                    else
                    {
                        Item.NormalBeginTime = TimeSpan.MinValue;
                    }
                }
                value = DateTime.MinValue;
                //fld.Fieldname = "NOEND";
                buffer = arrTemp[8].Trim();
                if (buffer == "240000")
                {
                    Item.NormalEndTime = new TimeSpan(0, 0, 0);
                }
                else
                {
                    DateTime.TryParseExact(buffer, "HHmmss", DefaultCultureInfo, DateTimeStyles.None, out value);
                    if (value != DateTime.MinValue)
                    {
                        Item.NormalEndTime = value.TimeOfDay;
                    }
                    else
                    {
                        Item.NormalEndTime = TimeSpan.MinValue;
                    }
                }
                //fld.FieldName = "TPKLA";
                Item.WorkscheduleClass = arrTemp[9].Trim();
                //fld.FieldName = "PAMOD";
                Item.BreakCode = arrTemp[10].Trim();
                oReturn.Add(Item);
            }

            #endregion " ParseObject "

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }

        #endregion " GetDailyWorkscheduleList "
    }
}