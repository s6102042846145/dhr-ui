﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using ESS.HR.OM.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.OM.DB
{
    public class HROMDataServiceImpl : AbstractHROMDataService
    {
        #region Constructor
        public HROMDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");
        private static string ModuleID = "ESS.HR.OM.DB";

        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string User
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SPUSER");
            }
        }

        #endregion Member

        #region Default Datetime
        private string DefaultDateGet
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DESS.HR", "DEFAULTDATEFORMATGET");
            }
        }
        private string DefaultEndDateGet
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DESS.HR", "DEFAULTENDDATEFORMATGET");
            }
        }

        #endregion GET

        #region Position
        public override List<PositionCenter> GetPositionByPosCode(DateTime StartDate, DateTime EndDate, string PosCode)
        {
            var PosData = HROMManagement.CreateInstance(CompanyCode).GetPositionDataContent(StartDate, EndDate);
            return PosData;
        }
        public override List<PositionCenter> GetPositionDataContent(DateTime StartDate, DateTime EndDate, string LevelCode = null, string UnitCode = null, string PosCode = null)
        {
            List<PositionCenter> oReturn = new List<PositionCenter>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommandPermiss = null;
                    SqlCommand oCommand = null;
                    try
                    {

                        oCommandPermiss = new SqlCommand("sp_CheckPermission", oConnection, oTransaction);
                        oCommandPermiss.CommandType = CommandType.StoredProcedure;
                        SqlParameter param = new SqlParameter("@user_name", SqlDbType.VarChar);
                        param.Value = this.User;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@service_name", SqlDbType.VarChar);
                        param.Value = "UI_UIGetOMPosition";
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        oCommandPermiss.ExecuteNonQuery();
                        var resultPermiss = oCommandPermiss.Parameters["@result"].Value;
                        var result_permiss = oCommandPermiss.Parameters["@result_message"].Value;

                        if (resultPermiss.ToString().ToUpper() == "S")
                        {
                            oCommand = new SqlCommand("sp_DHR_UI_GetOMPosition", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@start_date", SqlDbType.DateTime);
                            param.Value = StartDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@end_date", SqlDbType.DateTime);
                            param.Value = EndDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@company_code", SqlDbType.VarChar);
                            param.Value = CompanyCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@unit_level_value", SqlDbType.VarChar);
                            param.Value = LevelCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@unit_code", SqlDbType.VarChar);
                            param.Value = UnitCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@pos_code", SqlDbType.VarChar);
                            param.Value = PosCode;
                            oCommand.Parameters.Add(param);

                            DataTable oTable = new DataTable();
                            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                            oAdapter.Fill(oTable);

                            var result = oCommand.Parameters["@result"].Value;
                            var result_message = oCommand.Parameters["@result_message"].Value;

                            if (result.ToString().ToUpper() == "S")
                            {
                                foreach (DataRow dr in oTable.Rows)
                                {
                                    PositionCenter item = new PositionCenter();
                                    item.CompanyCode = dr["COMPANY_CODE"].ToString();
                                    item.PosCode = dr["POS_CODE"].ToString();
                                    item.PosShortName = dr["POS_SHORTNAME"].ToString();
                                    item.PosName = dr["POS_NAME"].ToString();
                                    item.PosShortNameEN = dr["POS_SHORTNAME_EN"].ToString();
                                    item.PosNameEN = dr["POS_NAME_EN"].ToString();
                                    item.HeadOfUnitFlag = dr["HEAD_OF_UNIT_F"].ToString();
                                    item.UnitCode = dr["UNIT_CODE"].ToString();
                                    item.UnitTextTH = dr["UNIT_TEXT_TH"].ToString();
                                    item.UnitTextEN = dr["UNIT_TEXT_EN"].ToString();
                                    item.EmpOfPosTextTH = dr["EMP_POSITION_TH"].ToString();
                                    item.EmpOfPosTextEN = dr["EMP_POSITION_EN"].ToString();
                                    item.BandCode = dr["BAND_CODE"].ToString();
                                    item.BandValue = dr["BAND_VALUE_CODE"].ToString();
                                    item.BandValueTextTH = dr["BAND_VALUE_TEXT_TH"].ToString();
                                    item.BandValueTextEN = dr["BAND_VALUE_TEXT_EN"].ToString();
                                    item.StartDate = Convert.ToDateTime(dr["START_DATE"].ToString());
                                    item.EndDate = Convert.ToDateTime(dr["END_DATE"].ToString());
                                    item.CreateDate = Convert.ToDateTime(dr["CREATE_DATE"].ToString());
                                    item.CreateBy = dr["CREATE_BY"].ToString();
                                    item.UpdateDate = Convert.ToDateTime(dr["UPDATE_DATE"].ToString());
                                    item.UpdateBy = dr["UPDATE_BY"].ToString();
                                    item.PositionTextTH = dr["POSITION_TEXT_TH"].ToString();
                                    item.PositionTextEN = dr["POSITION_TEXT_EN"].ToString();
                                    item.BandTextEN = dr["BAND_TEXT_EN"].ToString();
                                    item.BandTextTH = dr["BAND_TEXT_TH"].ToString();
                                    oReturn.Add(item);
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oTransaction.Commit();
                        oCommandPermiss.Dispose();
                        oCommand.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return oReturn;
        }
        public override PositionDetailCenter GetPositionDataDetail(string UnitCode, string Language, DateTime StartDate, DateTime EndDate)
        {
            PositionDetailCenter oReturn = new PositionDetailCenter();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlParameter oParam = new SqlParameter();
                    try
                    {

                        oCommand = new SqlCommand("sp_DHR_UI_GetOMPositionDetails", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;

                        oParam = new SqlParameter("@lang_type", SqlDbType.VarChar, 2);
                        oParam.Value = Language;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@start_date", SqlDbType.DateTime);
                        oParam.Value = StartDate;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@end_date", SqlDbType.DateTime);
                        oParam.Value = EndDate;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@company_code", SqlDbType.VarChar);
                        oParam.Value = CompanyCode;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@unit_code", SqlDbType.VarChar);
                        oParam.Value = UnitCode;
                        oCommand.Parameters.Add(oParam);

                        DataTable oTable = new DataTable();
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);
                        foreach (DataRow dr in oTable.Rows)
                        {
                            PositionDetailCenter item = new PositionDetailCenter();
                            oReturn.UnitCode = dr["UNIT_CODE"].ToString();
                            oReturn.LevelText = dr["LEVEL_TEXT"].ToString();
                            oReturn.CostCenterText = dr["COST_CENTER_TEXT"].ToString();
                            oReturn.SupervisorName = dr["HEAD_NAME"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oTransaction.Commit();
                        oCommand.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return oReturn;
        }

        public override List<PositionDotedDetailCenter> GetPositionDotedDetail(string HeadPosition, string PosCode, string Language, DateTime StartDate, DateTime EndDate)
        {
            List<PositionDotedDetailCenter> oReturn = new List<PositionDotedDetailCenter>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlParameter oParam = new SqlParameter();
                    try
                    {

                        oCommand = new SqlCommand("sp_DHR_UI_GetOMDotedDetails", oConnection, oTransaction);
                        oCommand.CommandType = CommandType.StoredProcedure;

                        oParam = new SqlParameter("@lang_type", SqlDbType.VarChar, 2);
                        oParam.Value = Language;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@start_date", SqlDbType.DateTime);
                        oParam.Value = StartDate;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@end_date", SqlDbType.DateTime);
                        oParam.Value = EndDate;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@company_code", SqlDbType.VarChar);
                        oParam.Value = CompanyCode;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@head_position", SqlDbType.VarChar);
                        oParam.Value = HeadPosition;
                        oCommand.Parameters.Add(oParam);

                        oParam = new SqlParameter("@pos_code", SqlDbType.VarChar);
                        oParam.Value = PosCode;
                        oCommand.Parameters.Add(oParam);

                        DataTable oTable = new DataTable();
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);
                        foreach (DataRow dr in oTable.Rows)
                        {
                            PositionDotedDetailCenter item = new PositionDotedDetailCenter();
                            item.Id = dr["ID"].ToString();
                            if (!string.IsNullOrEmpty(dr["START_DATE"].ToString()))
                            {
                                item.StartDate = Convert.ToDateTime(dr["START_DATE"].ToString());
                            }
                            if (!string.IsNullOrEmpty(dr["END_DATE"].ToString()))
                            {
                                item.EndDate = Convert.ToDateTime(dr["END_DATE"].ToString());
                            }

                            item.PosCode = dr["POS_CODE"].ToString();
                            item.HeadPosCode = dr["HEAD_POSITION"].ToString();
                            item.PositionOfSubordinate = dr["POSITION_OF_SUBORDINATE"].ToString();
                            item.Unit = dr["UNIT"].ToString();
                            item.EmployeeID = dr["EMPLOYEE_ID"].ToString();
                            item.HeadOfPosition = dr["HEAD_OF_POSITION"].ToString();
                            item.HeadOfOrganizationName = dr["HEAD_OF_ORGANIZATION_NAME"].ToString();
                            item.ActionType = !string.IsNullOrEmpty(dr["ID"].ToString()) ? "" : "I";
                            oReturn.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oTransaction.Commit();
                        oCommand.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return oReturn;
        }
        #endregion

        #region Organization
        public override List<OrganizationCenter> GetOrganizationDataContent(string LevelCode, string UnitCode, DateTime StartDate, DateTime EndDate)
        {
            List<OrganizationCenter> oReturn = new List<OrganizationCenter>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommandPermiss = null;
                    SqlCommand oCommand = null;
                    try
                    {

                        oCommandPermiss = new SqlCommand("sp_CheckPermission", oConnection, oTransaction);
                        oCommandPermiss.CommandType = CommandType.StoredProcedure;
                        SqlParameter param = new SqlParameter("@user_name", SqlDbType.VarChar);
                        param.Value = this.User;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@service_name", SqlDbType.VarChar);
                        param.Value = "UI_UIGetOMUnit";
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        oCommandPermiss.ExecuteNonQuery();
                        var resultPermiss = oCommandPermiss.Parameters["@result"].Value;
                        var result_permiss = oCommandPermiss.Parameters["@result_message"].Value;

                        if (resultPermiss.ToString().ToUpper() == "S")
                        {
                            oCommand = new SqlCommand("sp_DHR_UI_GetOMUnit", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@start_date", SqlDbType.DateTime);
                            param.Value = StartDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@end_date", SqlDbType.DateTime);
                            param.Value = EndDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@company_code", SqlDbType.VarChar);
                            param.Value = CompanyCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@unit_level_value", SqlDbType.VarChar);
                            param.Value = LevelCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@unit_code", SqlDbType.VarChar);
                            param.Value = UnitCode;
                            oCommand.Parameters.Add(param);

                            DataTable oTable = new DataTable();
                            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                            oAdapter.Fill(oTable);

                            var result = oCommand.Parameters["@result"].Value;
                            var result_message = oCommand.Parameters["@result_message"].Value;

                            if (result.ToString().ToUpper() == "S")
                            {
                                foreach (DataRow dr in oTable.Rows)
                                {
                                    OrganizationCenter item = new OrganizationCenter();
                                    item.CompanyCode = dr["COMPANY_CODE"].ToString();
                                    item.OrganizationID = dr["UNIT_CODE"].ToString();
                                    item.ShortNameTH = dr["UNIT_SHORTNAME"].ToString();
                                    item.ShortNameEN = dr["UNIT_SHORTNAME_EN"].ToString();
                                    item.FullNameTH = dr["UNIT_NAME"].ToString();
                                    item.FullNameEN = dr["UNIT_NAME_EN"].ToString();
                                    item.OrganizationLevelID = dr["UNIT_LEVEL_VALUE"].ToString();
                                    item.OrganizationLevelName = dr["UNIT_LEVEL_TEXT_TH"].ToString();
                                    item.OrganizationLevelNameEN = dr["UNIT_LEVEL_TEXT_EN"].ToString();
                                    item.CostCenterID = dr["COST_CENTER_VALUE"].ToString();
                                    item.CostCenterName = dr["COST_CENTER_TEXT_TH"].ToString();
                                    item.CostCenterNameEN = dr["COST_CENTER_TEXT_EN"].ToString();
                                    item.HeadOfOrganizationID = dr["UPPER_UNIT_CODE"].ToString();
                                    item.HeadOfOrganizationName = dr["HEAD_NAME_TH"].ToString();
                                    item.HeadOfOrganizationNameEN = dr["HEAD_NAME_EN"].ToString();
                                    item.HeadOfPositionName = dr["HEAD_POSITION_TH"].ToString();
                                    item.HeadOfPositionNameEN = dr["HEAD_POSITION_EN"].ToString();
                                    item.BeginDate = Convert.ToDateTime(dr["START_DATE"].ToString());
                                    item.EndDate = Convert.ToDateTime(dr["END_DATE"].ToString());
                                    item.OrderNo = Convert.ToInt16(dr["PRIORITY"].ToString());
                                    oReturn.Add(item);
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oTransaction.Commit();
                        oCommandPermiss.Dispose();
                        oCommand.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return oReturn;
        }

        public override void SaveOrganizationData(OrganizationCenter Organization, string actionType, string serviceType)
        {
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommandPermiss = null;
                    SqlCommand oCommand = null;
                    try
                    {

                        oCommandPermiss = new SqlCommand("sp_CheckPermission", oConnection, oTransaction);
                        oCommandPermiss.CommandType = CommandType.StoredProcedure;
                        SqlParameter param = new SqlParameter("@user_name", SqlDbType.VarChar);
                        param.Value = this.User;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@service_name", SqlDbType.VarChar);
                        param.Value = "UI_WsReceiveOMUnit";
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        oCommandPermiss.ExecuteNonQuery();
                        var resultPermiss = oCommandPermiss.Parameters["@result"].Value;
                        var result_permiss = oCommandPermiss.Parameters["@result_message"].Value;

                        if (resultPermiss.ToString().ToUpper() == "S")
                        {
                            oCommand = new SqlCommand("sp_ReceiveOMUnit", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@COMPANY_CODE", SqlDbType.VarChar);
                            param.Value = CompanyCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_CODE", SqlDbType.VarChar);
                            param.Value = Organization.OrganizationID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@START_DATE", SqlDbType.DateTime);
                            param.Value = Organization.BeginDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@END_DATE", SqlDbType.DateTime);
                            try
                            {
                                param.Value = Organization.EndDate.Date.AddDays(1).AddSeconds(-1);
                            }
                            catch
                            {
                                param.Value = Organization.EndDate.Date;
                            }
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_SHORTNAME", SqlDbType.VarChar);
                            param.Value = Organization.ShortNameTH;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_NAME", SqlDbType.VarChar);
                            param.Value = Organization.FullNameTH;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_SHORTNAME_EN", SqlDbType.VarChar);
                            param.Value = Organization.ShortNameEN;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_NAME_EN", SqlDbType.VarChar);
                            param.Value = Organization.FullNameEN;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_LEVEL_CODE", SqlDbType.VarChar);
                            param.Value = "UNIT_LEVEL_CODE";
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_LEVEL_VALUE", SqlDbType.VarChar);
                            param.Value = Organization.OrganizationLevelID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@COST_CENTER_CODE", SqlDbType.VarChar);
                            param.Value = "COST_CENTER_CODE";
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@COST_CENTER_VALUE", SqlDbType.VarChar);
                            param.Value = Organization.CostCenterID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UPPER_UNIT_CODE", SqlDbType.VarChar);
                            param.Value = Organization.HeadOfOrganizationID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@PRIORITY", SqlDbType.Int);
                            param.Value = Organization.OrderNo;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@ACTION_TYPE", SqlDbType.VarChar);
                            param.Value = actionType;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@SERVICE_TYPE", SqlDbType.VarChar);
                            param.Value = serviceType;
                            oCommand.Parameters.Add(param);

                            oCommand.ExecuteNonQuery();
                            //DataTable oTable = new DataTable();
                            //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                            //oAdapter.Fill(oTable);

                            var result = oCommand.Parameters["@result"].Value;
                            var result_message = oCommand.Parameters["@result_message"].Value;

                            if (result.ToString().ToUpper() != "S")
                            {
                                throw new Exception(result.ToString().ToUpper());
                            }
                        }
                        oTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommandPermiss.Dispose();
                        oCommand.Dispose();
                        oConnection.Close();
                    }
                }
            }
        }

        public override List<string> getOrganizationChartByHeadOfOrg(string unitCode, string levelCode, string HeadCode, DateTime BeginDate, DateTime EndDate, string Language)
        {
            List<string> oReturn = new List<string>();

            oSqlManage["ProcedureName"] = "sp_DHR_UI_GetORGUpperlist";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@UNIT_CODE"] = unitCode;
            oParamRequest["@UNIT_LEVEL_VALUE"] = levelCode;
            oParamRequest["@UPPER_UNIT_CODE"] = HeadCode;
            oParamRequest["@COMPANY_CODE"] = CompanyCode;
            oParamRequest["@LANG_TYPE"] = Language;
            oParamRequest["@START_DATE"] = BeginDate;
            oParamRequest["@END_DATE"] = EndDate;
            try
            {
                DataTable oTB = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
                foreach (DataRow item in oTB.Rows)
                {
                    var yy = item.ItemArray[0].ToString();
                    oReturn.Add(item.ItemArray[0].ToString());
                }
                //oReturn.AddRange(DatabaseHelper.ExecuteQuery<string>(oSqlManage, oParamRequest));
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

            return oReturn;
        }

        public override DataTable getPriorityListByOrganizationID(string unitCode, DateTime BeginDate, DateTime EndDate, string Language)
        {
            DataTable oReturn = new DataTable();

            oSqlManage["ProcedureName"] = "sp_DHR_UI_GetORGPriorityList";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@UNIT_CODE"] = unitCode;
            oParamRequest["@COMPANY_CODE"] = CompanyCode;
            oParamRequest["@LANG_TYPE"] = Language;
            oParamRequest["@START_DATE"] = BeginDate;
            oParamRequest["@END_DATE"] = EndDate;
            try
            {
                oReturn = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

            return oReturn;
        }

        public override string ValidateOrganizationData(OrganizationCenter Organization, string oActionType, string oServiceType)
        {
            string oReturn = "SUCCESS";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommandPermiss = null;
                    SqlCommand oCommand = null;
                    try
                    {

                        oCommandPermiss = new SqlCommand("sp_CheckPermission", oConnection, oTransaction);
                        oCommandPermiss.CommandType = CommandType.StoredProcedure;
                        SqlParameter param = new SqlParameter("@user_name", SqlDbType.VarChar);
                        param.Value = this.User;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@service_name", SqlDbType.VarChar);
                        param.Value = "UI_WsReceiveOMUnit";
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        oCommandPermiss.ExecuteNonQuery();
                        var resultPermiss = oCommandPermiss.Parameters["@result"].Value;
                        var result_permiss = oCommandPermiss.Parameters["@result_message"].Value;

                        if (resultPermiss.ToString().ToUpper() == "S")
                        {
                            oCommand = new SqlCommand("sp_ReceiveOMUnit", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@COMPANY_CODE", SqlDbType.VarChar);
                            param.Value = CompanyCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_CODE", SqlDbType.VarChar);
                            param.Value = Organization.OrganizationID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@START_DATE", SqlDbType.DateTime);
                            param.Value = Organization.BeginDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@END_DATE", SqlDbType.DateTime);
                            try
                            {
                                param.Value = Organization.EndDate.Date.AddDays(1).AddSeconds(-1);
                            }
                            catch
                            {
                                param.Value = Organization.EndDate.Date;
                            }
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_SHORTNAME", SqlDbType.VarChar);
                            param.Value = Organization.ShortNameTH;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_NAME", SqlDbType.VarChar);
                            param.Value = Organization.FullNameTH;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_SHORTNAME_EN", SqlDbType.VarChar);
                            param.Value = Organization.ShortNameEN;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_NAME_EN", SqlDbType.VarChar);
                            param.Value = Organization.FullNameEN;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_LEVEL_CODE", SqlDbType.VarChar);
                            param.Value = "UNIT_LEVEL_CODE";
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UNIT_LEVEL_VALUE", SqlDbType.VarChar);
                            param.Value = Organization.OrganizationLevelID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@COST_CENTER_CODE", SqlDbType.VarChar);
                            param.Value = "COST_CENTER_CODE";
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@COST_CENTER_VALUE", SqlDbType.VarChar);
                            param.Value = Organization.CostCenterID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@UPPER_UNIT_CODE", SqlDbType.VarChar);
                            param.Value = Organization.HeadOfOrganizationID;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@PRIORITY", SqlDbType.Int);
                            param.Value = Organization.OrderNo;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@ACTION_TYPE", SqlDbType.VarChar);
                            param.Value = oActionType;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@SERVICE_TYPE", SqlDbType.VarChar);
                            param.Value = oServiceType;
                            oCommand.Parameters.Add(param);

                            oCommand.ExecuteNonQuery();

                            var result = oCommand.Parameters["@result"].Value;
                            var result_message = oCommand.Parameters["@result_message"].Value;

                            if (result.ToString().ToUpper() != "S")
                            {
                                oReturn = result.ToString().ToUpper();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        oCommandPermiss.Dispose();
                        oCommand.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return oReturn;
        }

        public override string getRootOrganizationUnit()
        {
            string oReturn = String.Empty;
            oSqlManage["ProcedureName"] = "sp_DHR_UI_GetMTConfig";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@CONDITION"] = "ROOT_ORG";
            oParamRequest["@COMPANY_CODE"] = CompanyCode;
            oParamRequest["@START_DATE"] = DateTime.Now.Date;
            oParamRequest["@END_DATE"] = DateTime.Now.Date;
            try
            {
                oReturn = DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest);
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            return oReturn;
        }

        public override OrganizationChief getLeaderByHeadOfOrganization(string oHeadOfOrg, DateTime oBeginDate, DateTime oEndDate)
        {
            OrganizationChief oReturn = new OrganizationChief();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommandPermiss = null;
                    SqlCommand oCommand = null;
                    try
                    {

                        oCommandPermiss = new SqlCommand("sp_CheckPermission", oConnection, oTransaction);
                        oCommandPermiss.CommandType = CommandType.StoredProcedure;
                        SqlParameter param = new SqlParameter("@user_name", SqlDbType.VarChar);
                        param.Value = this.User;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@service_name", SqlDbType.VarChar);
                        param.Value = "UI_UIGetOMUnit";
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                        param.Direction = ParameterDirection.Output;
                        oCommandPermiss.Parameters.Add(param);

                        oCommandPermiss.ExecuteNonQuery();
                        var resultPermiss = oCommandPermiss.Parameters["@result"].Value;
                        var result_permiss = oCommandPermiss.Parameters["@result_message"].Value;

                        if (resultPermiss.ToString().ToUpper() == "S")
                        {
                            oCommand = new SqlCommand("sp_DHR_UI_GetOMUnit", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            param = new SqlParameter("@result", SqlDbType.VarChar, 1);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@result_message", SqlDbType.VarChar, 200);
                            param.Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@start_date", SqlDbType.DateTime);
                            param.Value = oBeginDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@end_date", SqlDbType.DateTime);
                            param.Value = oEndDate.Date;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@company_code", SqlDbType.VarChar);
                            param.Value = CompanyCode;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@unit_code", SqlDbType.VarChar);
                            param.Value = oHeadOfOrg;
                            oCommand.Parameters.Add(param);

                            param = new SqlParameter("@unit_level_value", SqlDbType.VarChar);
                            param.Value = "";
                            oCommand.Parameters.Add(param);

                            DataTable oTable = new DataTable();
                            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                            oAdapter.Fill(oTable);

                            var result = oCommand.Parameters["@result"].Value;
                            var result_message = oCommand.Parameters["@result_message"].Value;

                            if (result.ToString().ToUpper() == "S")
                            {
                                foreach (DataRow dr in oTable.Rows)
                                {
                                    OrganizationChief item = new OrganizationChief();
                                    item.OrganizationID = dr["UNIT_CODE"].ToString();
                                    item.OrganizationLevel = "";
                                    item.HeadUnitNameTH = dr["HEAD_NAME_TH"].ToString();
                                    item.HeadUnitNameEN = dr["HEAD_NAME_EN"].ToString();
                                    item.HeadUnitPositionTH = dr["HEAD_POSITION_TH"].ToString();
                                    item.HeadUnitPositionEN = dr["HEAD_POSITION_EN"].ToString();
                                    oReturn = item;
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        oCommandPermiss.Dispose();
                        oCommand.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return oReturn;
        }
        #endregion

        #region Mark Data
        public override void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from MarkData where EmployeeID = @EmpID and DataCategory = @DataCategory ", oConnection, tx);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("MARKDATA");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count == 0 && isMarkIn)
                {
                    DataRow oRow = oTable.NewRow();
                    oRow["EmployeeID"] = EmployeeID;
                    oRow["DataCategory"] = DataCategory;
                    oRow["RequestNo"] = RequestNo;
                    oTable.Rows.Add(oRow);
                }
                else if (oTable.Rows.Count > 0 && !isMarkIn)
                {
                    foreach (DataRow dr in oTable.Select())
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        public override bool CheckMark(string DataCategory)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where DataCategory = " +
                                                 "@DataCategory", oConnection);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error " + ex.Message, ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }
        public override bool CheckMark(string DataCategory, string ReqNo)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where DataCategory = " +
                                                 "@DataCategory and RequestNo <> @ReqNo", oConnection);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
            oParam.Value = ReqNo;
            oCommand.Parameters.Add(oParam);

            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error" + ex.Message, ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }
        public override bool CheckMarkWithWhereCause(string DataCategory, string ReqNo, string oWhereCause)
        {
            /* oWhereCause is
                = for equal
                % for like */
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            string wCause = oWhereCause == "=" ? " DataCategory = @DataCategory" : " DataCategory like '%'+@DataCategory+'%'";
            string wRequestNo = String.IsNullOrEmpty(ReqNo) ? "" : " and RequestNo <> @ReqNo";
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where " + wCause + wRequestNo, oConnection);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);

            if (!String.IsNullOrEmpty(ReqNo))
            {
                oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
                oParam.Value = ReqNo;
                oCommand.Parameters.Add(oParam);
            }

            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error" + ex.Message, ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }
        public override List<MarkData> GetMarkDataAll()
        {
            List<MarkData> oReturn = new List<MarkData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("select * from MarkData ", oConnection);


            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("MARKDATA");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
                foreach (DataRow rows in oTable.Rows)
                {
                    MarkData item = new MarkData();
                    item.EmployeeID = rows["EmployeeID"].ToString();
                    item.DataCategory = rows["DataCategory"].ToString();
                    item.RequestNo = rows["RequestNo"].ToString();
                    oReturn.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Get Markdata data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
            return oReturn;
        }

        public override List<MarkData> GetMarkDataWithRequestType()
        {
            List<MarkData> oReturn = new List<MarkData>();

            oSqlManage["ProcedureName"] = "sp_getMarkDataWithRequestType";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            try
            {
                oReturn.AddRange(DatabaseHelper.ExecuteQuery<MarkData>(oSqlManage, oParamRequest));
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            return oReturn;
        }
        #endregion

        #region Document Data
        public override DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string Language)
        {
            DataTable returnValue = new DataTable();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetCreatedDoc", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_requestTypeID", RequestType);
            oCommand.Parameters.AddWithValue("@p_language", Language);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RequestDoc");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = oTable;

            oTable.Dispose();
            return returnValue;
        }

        public override DataTable GetCreatedDocAllByRequestTypeID(int RequestTypeID, string Language)
        {
            DataTable returnValue = new DataTable();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetCreatedDocAll", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_requestTypeID", RequestTypeID);
            oCommand.Parameters.AddWithValue("@p_language", Language);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RequestDoc");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = oTable;

            oTable.Dispose();
            return returnValue;
        }
        public override DataTable GetRequestDocumentData(string oRequestNo)
        {
            DataTable oReturn = new DataTable();
            oSqlManage["ProcedureName"] = "sp_RequestDocumentGetByRequestNo";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@RequestNo"] = oRequestNo;
            oReturn = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);

            return oReturn;
        }
        #endregion
    }
}