﻿using ESS.HR.OM.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace ESS.HR.OM.DB
{
    public class HROMConfigServiceImpl : AbstractHROMConfigService
    {
        #region Constructor
        public HROMConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();

        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");

        private static string ModuleID = "ESS.HR.OM.DB";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region Get Dropdownlist Level,Unit,Position
        /// <summary>
        /// Get Dropdownlist data
        /// </summary>
        /// <param name="DropdownType">ประเภท DDL ที่ต้องการดึงข้อมูล ORLV = Org Level,ORUN = Org Unit, OMPO = Position</param>
        /// <param name="LanguageCode">TH/EN</param>
        /// <param name="StartDate">วันที่เริ่มต้น</param>
        /// <param name="EndDate">วันที่สิ้นสุด</param>
        /// <param name="Param1">รหัส Level, รหัส Unit</param>
        /// <returns>List<ControlDataCenter> oResult</returns>
        public override List<ControlDropdownlistData> GetSearchContentDDL(string DropdownType, string LanguageCode,DateTime StartDate,DateTime EndDate,string Param1=null)
        {
            oSqlManage["ProcedureName"] = "sp_DHR_UI_DllController";
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@dll_type"] = DropdownType;
            oParamRequest["@lang_type"] = LanguageCode;
            oParamRequest["@start_date"] = StartDate;
            oParamRequest["@end_date"] = EndDate;
            oParamRequest["@parameter01"] = Param1;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<ControlDropdownlistData>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<ControlDropdownlistData> GetOrgLevelDDL(string LanguageCode,DateTime StartDate,DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("ORLV", LanguageCode, StartDate, EndDate);
            return oResult;
        }

        public override List<ControlDropdownlistData> GetOrgUnitByOrgLevelDDL(string LevelCode, string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("ORUN", LanguageCode, StartDate, EndDate,LevelCode);
            return oResult;
        }

        public override List<ControlDropdownlistData> GetPositionByOrgUnitDDL(string UnitCode, string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("OMPO", LanguageCode, StartDate, EndDate,UnitCode);
            return oResult;
        }
        public override List<ControlDropdownlistData> GetPositionUnderSupervisorDDL(string PosCode, string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("DLPO", LanguageCode, StartDate, EndDate, PosCode);
            return oResult;
        }
        public override List<ControlDropdownlistData> GetPositionUnderSupervisorNewDDL(string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("OMPO", LanguageCode, StartDate, EndDate,string.Empty);
            return oResult;
        }
        public override List<ControlDropdownlistData> GetCostCenterDDL(string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("COST", LanguageCode, StartDate, EndDate);
            return oResult;
        }
        public override List<ControlDropdownlistData> GetHeadOfOrgUnitDDL(string oOrgLevel, string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("UPLV", LanguageCode, StartDate, EndDate, oOrgLevel);
            return oResult;
        }

        public override List<ControlDropdownlistData> GetBandDDL(string LanguageCode, DateTime StartDate, DateTime EndDate)
        {
            List<ControlDropdownlistData> oResult = new List<ControlDropdownlistData>();
            oResult = GetSearchContentDDL("BAND", LanguageCode, StartDate, EndDate);
            return oResult;
        }
        #endregion

    }
}

