﻿using DHR.HR.API;
using DHR.HR.API.Model;
using DHR.HR.API.Shared;
using ESS.DATA;
using ESS.DATA.EXCEPTION;
using ESS.HR.OM.ABSTRACT;
using ESS.HR.OM.DATACLASS;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace ESS.HR.OM.DHR
{
    public class HROMDataServiceImpl : AbstractHROMDataService
    {
        private CultureInfo thCL = new CultureInfo("th-TH");
        private CultureInfo enCL = new CultureInfo("en-US");
        #region Constructor
        public HROMDataServiceImpl(string oComparyCode)
        {
            CompanyCode = oComparyCode;
        }
        #endregion Constructor

        #region Member
        private static string ModuleID = "ESS.HR.OM.DHR";
        private string CompanyCode { get; set; }
        CultureInfo oCL = new CultureInfo("en-US");

        private string DefaultBeginDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTBEGINDATEFORMAT");
            }
        }

        #region GET
        private string DefaultDateGet
        {
            get
            {
                //ddMMyyyy:00:00:00
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATGET");
            }
        }
        private string DefaultEndDateGet
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATGET");
            }
        }

        #endregion GET

        #region POST
        private string DefaultDatePost
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTDATEFORMATPOST");
            }
        }
        private string DefaultEndDatePost
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "DEFAULTENDDATEFORMATPOST");
            }
        }
        #endregion POST


        public string PeriodStartDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "DHR.HR", "PERIODSTARTDATE");

            }
        }

        #endregion Member

        public override List<OrganizationCenter> getOrganizationByOrgID(string oOrgID, string oLang)
        {
            List<OrganizationCenter> oReturn = new List<OrganizationCenter>();
            ResponseModel<Unit> responseModel = new ResponseModel<Unit>();
            try
            {
                responseModel = APIOrgManagement.CreateInstance(CompanyCode).GetDHRUnit(DefaultBeginDate, DateTime.MaxValue.ToString("ddMMyyyy"));
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (var item in responseModel.Data.Where(Data => Data.unitCode == oOrgID))
                    {
                        OrganizationCenter temp = new OrganizationCenter();

                        temp.BeginDate = DateTime.ParseExact(item.startDate, "ddMMyyyy", null);
                        temp.EndDate = DateTime.ParseExact(item.endDate, "ddMMyyyy", null);
                        temp.CostCenterID = item.costCenterValue;
                        temp.CostCenterName = oLang == "EN" ? item.costCenterTextEN : item.costCenterTextTH;
                        temp.ShortNameEN = item.unitShortnameEn;
                        temp.ShortNameTH = item.unitShortname;
                        temp.FullNameEN = item.unitNameEn;
                        temp.FullNameTH = item.unitName;
                        temp.OrderNo = 0;
                        temp.OrganizationID = item.unitCode;
                        temp.OrganizationLevelID = item.unitLevelValue;
                        temp.OrganizationLevelName = oLang == "EN" ? item.unitLevelTextEN : item.unitLevelTextTH;
                        temp.HeadOfOrganizationID = item.upperUnitCode;
                        temp.HeadOfOrganizationName = "";
                        oReturn.Add(temp);
                    }

                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HROM", "getOrganizationByOrgID Error", responseModel.message);
            }

            return oReturn;
        }

        public override OrganizationCenter getOrganizationDetail(string oOrgID, DateTime oBegDate, DateTime oEndDate)
        {
            OrganizationCenter oReturn = new OrganizationCenter();
            ResponseModel<Unit> responseModel = new ResponseModel<Unit>();
            try
            {
                responseModel = APIOrgManagement.CreateInstance(CompanyCode).GetDHRUnit(oBegDate.ToString("ddMMyyyy"), oEndDate.ToString("ddMMyyyy"));
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    var item = (Unit)responseModel.Data.Where(Data => Data.unitCode == oOrgID).FirstOrDefault();
                    OrganizationCenter temp = new OrganizationCenter();

                    temp.BeginDate = DateTime.ParseExact(item.startDate, "ddMMyyyy", null);
                    temp.EndDate = DateTime.ParseExact(item.endDate, "ddMMyyyy", null);
                    temp.CostCenterID = item.costCenterValue;
                    temp.ShortNameEN = item.unitShortnameEn;
                    temp.ShortNameTH = item.unitShortname;
                    temp.FullNameEN = item.unitNameEn;
                    temp.FullNameTH = item.unitName;
                    temp.OrderNo = 0;
                    temp.OrganizationID = item.unitCode;
                    temp.OrganizationLevelID = item.unitLevelValue;
                    temp.HeadOfOrganizationID = item.upperUnitCode;
                    temp.HeadOfOrganizationName = "";
                    oReturn = temp;
                }
            }
            catch (Exception ex)
            {
                throw new DataServiceException("HROM", "getOMUnitByPeriodDate Error", responseModel.message);
            }
            return oReturn;
        }


        #region Get Position data
        public override List<PositionCenter> GetPositionDataContent(DateTime StartDate, DateTime EndDate, string LevelCode = null, string UnitCode = null, string PosCode = null)
        {
            List<PositionCenter> oReturn = new List<PositionCenter>();
            ResponseModel<PositionModel> responseModel = new ResponseModel<PositionModel>();
            responseModel = APIOrgManagement.CreateInstance(CompanyCode).GetDHRPosition(StartDate.ToString("ddMMyyyy"), EndDate.ToString("ddMMyyyy"), UnitCode, LevelCode, PosCode);
            foreach (var item in responseModel.Data)
            {
                PositionCenter data = new PositionCenter();
                data.ID = item.id;
                data.CompanyCode = item.companyCode;
                data.PosCode = item.posCode;
                data.PosShortName = item.posShortname;
                data.PosName = item.posName;
                data.PosShortNameEN = item.posShortnameEn;
                data.PosNameEN = item.posNameEn;
                data.HeadOfUnitFlag = item.headOfUnitF;
                data.BandCode = item.bandCode;
                data.BandValue = item.bandValue;
                data.BandTextEN = item.bandTextEN;
                data.BandTextTH = item.bandTextTH;
                data.BandValueTextEN = item.bandTextEN;
                data.BandValueTextTH = item.bandTextTH;
                data.UnitCode = item.unitCode;
                data.UnitTextEN = item.unitTextEN;
                data.UnitTextTH = item.unitTextTH;
                data.EmpOfPosTextEN = item.empPositionEN;
                data.EmpOfPosTextTH = item.empPositionTH;
                data.PositionTextEN = item.positionTextEN;
                data.PositionTextTH = item.positionTextTH;
                data.StartDate = DateTime.ParseExact(item.startDate, "ddMMyyyy", null);
                data.EndDate = DateTime.ParseExact(item.endDate, "ddMMyyyy", null);
                data.UpdateBy = item.updateBy;
                data.UpdateDate = DateTime.ParseExact(item.updateDate, "ddMMyyyy HH:mm:ss", null);
                oReturn.Add(data);
            }
            return oReturn;

        }
        public override List<PositionCenter> GetPositionByPosCode(DateTime StartDate, DateTime EndDate, string PosCode)
        {
            var PosData = HROMManagement.CreateInstance(CompanyCode).GetPositionDataContent(StartDate, EndDate, string.Empty, string.Empty, PosCode);
            return PosData;
        }
        public override bool SaveWithValidateSolidLineData(PositionCenter oSolidData, string ServiceType, out string oExceptionError, out string oErrorTextCode, out string oErrorParam)
        {
            oErrorTextCode = string.Empty;
            oErrorParam = string.Empty;
            oExceptionError = string.Empty;
            bool result = true;
            SolidLineModel solidLinePostInf = new SolidLineModel();
            solidLinePostInf.id = oSolidData.ID;
            solidLinePostInf.companyCode = CompanyCode;
            solidLinePostInf.startDate = oSolidData.StartDate.ToString(DefaultDatePost);
            solidLinePostInf.endDate = oSolidData.EndDate.ToString(DefaultDatePost);
            solidLinePostInf.posCode = oSolidData.PosCode;
            solidLinePostInf.posShortname = oSolidData.PosShortName;
            solidLinePostInf.posName = oSolidData.PosName;
            solidLinePostInf.posShortnameEn = oSolidData.PosShortNameEN;
            solidLinePostInf.posNameEn = oSolidData.PosNameEN;
            solidLinePostInf.bandCode = !string.IsNullOrEmpty(oSolidData.BandValue) ? "BAND_CODE" : string.Empty;
            solidLinePostInf.bandValue = !string.IsNullOrEmpty(oSolidData.BandValue) ? oSolidData.BandValue : string.Empty;
            solidLinePostInf.headOfUnitF = oSolidData.HeadOfUnitFlag;
            solidLinePostInf.headCode = oSolidData.HeadCode;
            solidLinePostInf.unitCode = oSolidData.UnitCode;
            solidLinePostInf.actionType = oSolidData.ActionType;
            solidLinePostInf.serviceType = ServiceType;
            try
            {
                if (!string.IsNullOrEmpty(solidLinePostInf.actionType))
                {
                    PostResult oPost = APIOrgManagement.CreateInstance(CompanyCode).PostDHROMSolidLine_UI(solidLinePostInf);
                    if (oPost.returnCode != 101)
                    {
                        result = false;
                        oErrorTextCode = !string.IsNullOrEmpty(oPost.returnBusinessCode) ? oPost.returnBusinessCode : string.Empty;
                        oErrorParam = !string.IsNullOrEmpty(oPost.returnParam) ? oPost.returnParam : string.Empty;
                        oExceptionError = oPost.returnDesc;
                    }
                }
                else
                {
                    result = false;
                    oExceptionError ="ActionType is Null";
                }

            }
            catch (Exception ex)
            {
                result = false;
                oExceptionError = ex.Message;
            }

            return result;
        }
        public override bool SaveWithValidateDotedLineData(PositionDotedDetailCenter oDotedData, string ServiceType, out string oExceptionError, out string oErrorTextCode, out string oErrorParam)
        {
            oErrorTextCode = string.Empty;
            oErrorParam = string.Empty;
            oExceptionError = string.Empty;
            bool result = true;
            DotedLineModel dotedLinePostInf = new DotedLineModel();
            dotedLinePostInf.id = oDotedData.Id;
            dotedLinePostInf.companyCode = CompanyCode;
            dotedLinePostInf.startDate = oDotedData.StartDate.ToString(DefaultDatePost);
            dotedLinePostInf.endDate = oDotedData.EndDate.ToString(DefaultDatePost);
            dotedLinePostInf.headPosition = oDotedData.HeadPosCode;
            dotedLinePostInf.posCode = oDotedData.PosCode;
            dotedLinePostInf.actionType = oDotedData.ActionType;
            dotedLinePostInf.serviceType = ServiceType;
            try
            {
                if (!string.IsNullOrEmpty(dotedLinePostInf.actionType))
                {
                    PostResult oPost = APIOrgManagement.CreateInstance(CompanyCode).PostDHROMDotedLine_UI(dotedLinePostInf);
                    if (oPost.returnCode != 101)
                    {
                        result = false;
                        oErrorTextCode = !string.IsNullOrEmpty(oPost.returnBusinessCode) ? oPost.returnBusinessCode : string.Empty;
                        oErrorParam = !string.IsNullOrEmpty(oPost.returnParam) ? oPost.returnParam : string.Empty;
                    }
                }
                else
                {
                    result = false;
                    oExceptionError = "ActionType is Null";
                }

            }
            catch (Exception ex)
            {
                result = false;
                oExceptionError = ex.Message;
            }

            return result;
        }

        #endregion

        #region Organization 
        public override List<OrganizationCenter> GetOrganizationDataContent(string LevelCode, string UnitCode, DateTime StartDate, DateTime EndDate)
        {
            List<OrganizationCenter> oReturn = new List<OrganizationCenter>();
            ResponseModel<OrganizationModel> responseModel = new ResponseModel<OrganizationModel>();
            try
            {
                responseModel = APIOrgManagement.CreateInstance(CompanyCode).GetDHROMUnit_UI(StartDate.ToString("ddMMyyyy"), EndDate.ToString("ddMMyyyy"), UnitCode, LevelCode);
                if (responseModel.Data != null && responseModel.Data.Count > 0)
                {
                    foreach (OrganizationModel row in responseModel.Data)
                    {
                        OrganizationCenter item = new OrganizationCenter();
                        item.CompanyCode = row.companyCode;
                        item.ID = row.id;
                        item.OrganizationID = row.unitCode;
                        item.ShortNameTH = row.unitShortname;
                        item.ShortNameEN = row.unitShortnameEn;
                        item.FullNameTH = row.unitName;
                        item.FullNameEN = row.unitNameEn;
                        item.OrganizationLevelID = row.unitLevelValue;
                        item.OrganizationLevelName = row.unitLevelTextTH;
                        item.OrganizationLevelNameEN = row.unitLevelTextEN;
                        item.CostCenterID = row.costCenterValue;
                        item.CostCenterName = row.costCenterTextTH;
                        item.CostCenterNameEN = row.costCenterTextEN;
                        item.HeadOfOrganizationID = row.upperUnitCode;
                        item.BeginDate = DateTime.ParseExact(row.startDate, "ddMMyyyy", null);
                        item.EndDate = DateTime.ParseExact(row.endDate, "ddMMyyyy", null);

                        item.OrderNo = Convert.ToInt16(row.priority);
                        oReturn.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return oReturn;
        }

        public override string ValidateOrganizationData(OrganizationCenter Organization, string oActionType, string oServiceType)
        {
            string oMessage = "SUCCESS";
            try
            {
                OrganizationModel organizationPostInf = new OrganizationModel()
                {
                    id = Organization.ID,
                    companyCode = Organization.CompanyCode,
                    unitCode = Organization.OrganizationID,
                    unitShortname = Organization.ShortNameTH,
                    unitName = Organization.FullNameTH,
                    unitShortnameEn = Organization.ShortNameEN,
                    unitNameEn = Organization.FullNameEN,
                    unitLevelCode = "UNIT_LEVEL_CODE",
                    unitLevelValue = Organization.OrganizationLevelID,
                    unitLevelTextTH = Organization.OrganizationLevelName,
                    unitLevelTextEN = Organization.OrganizationLevelNameEN,
                    costCenterCode = "COST_CENTER_CODE",
                    costCenterValue = Organization.CostCenterID,
                    costCenterTextTH = Organization.CostCenterName,
                    costCenterTextEN = Organization.CostCenterNameEN,
                    upperUnitCode = Organization.HeadOfOrganizationID,
                    startDate = Organization.BeginDate.ToString("ddMMyyyy")
                };

                try
                {
                    organizationPostInf.endDate = Organization.EndDate.Date.AddDays(1).AddSeconds(-1).ToString("ddMMyyyy");
                }
                catch
                {
                    organizationPostInf.endDate = Organization.EndDate.Date.ToString("ddMMyyyy");
                }
                organizationPostInf.priority = Organization.OrderNo.ToString();
                organizationPostInf.actiontype = oActionType;
                organizationPostInf.servicetype = oServiceType;

                PostResult oPost = APIOrgManagement.CreateInstance(CompanyCode).PostDHROMUnit_UI(organizationPostInf);
                if (oPost.returnCode == 101)
                {
                    oMessage = "SUCCESS";
                    if (oActionType == "I")
                    {
                        Organization.SuggestOrganizationID = String.IsNullOrEmpty(oPost.returnParam) ? "" : oPost.returnParam;
                    }
                }
                else if (oPost.returnCode == 501 || oPost.returnCode == 601) // Warning | Comfirmation alert
                {
                    // Do Something                    
                    //oMessage = "ERROR" + " : " + oPost.returnDesc;
                }
                else
                {
                    var oArgs = String.IsNullOrEmpty(oPost.returnParam) ? new String[0] : oPost.returnParam.Split('|');
                    if (!String.IsNullOrEmpty(oPost.returnBusinessCode))
                        throw new DataServiceException(String.Empty, oPost.returnBusinessCode, oArgs);
                    else
                    {
                        throw new Exception(oPost.returnDesc);
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                oMessage = "PROCESS ERROR : " + ex.Message;
            }
            return oMessage;
        }

        public override void SaveOrganizationData(OrganizationCenter Organization, string actionType, string serviceType)
        {
            try
            {
                //assign value
                OrganizationModel organizationPostInf = new OrganizationModel()
                {
                    id = Organization.ID,
                    companyCode = Organization.CompanyCode,
                    unitCode = Organization.OrganizationID,
                    unitShortname = Organization.ShortNameTH,
                    unitName = Organization.FullNameTH,
                    unitShortnameEn = Organization.ShortNameEN,
                    unitNameEn = Organization.FullNameEN,
                    unitLevelCode = "UNIT_LEVEL_CODE",
                    unitLevelValue = Organization.OrganizationLevelID,
                    unitLevelTextTH = Organization.OrganizationLevelName,
                    unitLevelTextEN = Organization.OrganizationLevelNameEN,
                    costCenterCode = "COST_CENTER_CODE",
                    costCenterValue = Organization.CostCenterID,
                    costCenterTextTH = Organization.CostCenterName,
                    costCenterTextEN = Organization.CostCenterNameEN,
                    upperUnitCode = Organization.HeadOfOrganizationID,
                    startDate = Organization.BeginDate.ToString("ddMMyyyy"),
                    priority = Organization.OrderNo.ToString(),
                    actiontype = actionType,
                    servicetype = serviceType
                };

                try
                {
                    organizationPostInf.endDate = Organization.EndDate.Date.AddDays(1).AddSeconds(-1).ToString("ddMMyyyy");
                }
                catch
                {
                    organizationPostInf.endDate = Organization.EndDate.Date.ToString("ddMMyyyy");
                }
                

                PostResult oPost = APIOrgManagement.CreateInstance(CompanyCode).PostDHROMUnit_UI(organizationPostInf);
                if (oPost.returnCode == 101)
                {
                    //Do Nothing
                }
                else if (oPost.returnCode == 501 || oPost.returnCode == 601) // Warning | Comfirmation alert
                {
                    // Do Something                    
                    //oMessage = "ERROR" + " : " + oPost.returnDesc;
                }
                else
                {
                    var oArgs = String.IsNullOrEmpty(oPost.returnParam) ? new String[0] : oPost.returnParam.Split('|');
                    if (!String.IsNullOrEmpty(oPost.returnBusinessCode))
                        throw new DataServiceException(String.Empty, oPost.returnBusinessCode, oArgs);
                    else
                    {
                        throw new Exception(oPost.returnDesc);
                    }
                }
            }
            catch (DataServiceException dex)
            {
                throw new DataServiceException(dex.Category, dex.Code, dex.Args);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override string getExampleOMUnitID(OrganizationCenter Organization, string oActionType, string oServiceType)
        {
            string oMessage = String.Empty;
            try
            {
                OrganizationModel organizationPostInf = new OrganizationModel()
                {
                    id = Organization.ID,
                    companyCode = Organization.CompanyCode,
                    unitCode = Organization.OrganizationID,
                    unitShortname = Organization.ShortNameTH,
                    unitName = Organization.FullNameTH,
                    unitShortnameEn = Organization.ShortNameEN,
                    unitNameEn = Organization.FullNameEN,
                    unitLevelCode = "UNIT_LEVEL_CODE",
                    unitLevelValue = Organization.OrganizationLevelID,
                    unitLevelTextTH = Organization.OrganizationLevelName,
                    unitLevelTextEN = Organization.OrganizationLevelNameEN,
                    costCenterCode = "COST_CENTER_CODE",
                    costCenterValue = Organization.CostCenterID,
                    costCenterTextTH = Organization.CostCenterName,
                    costCenterTextEN = Organization.CostCenterNameEN,
                    upperUnitCode = Organization.HeadOfOrganizationID,
                    startDate = Organization.BeginDate.ToString("ddMMyyyy")
                };

                try
                {
                    organizationPostInf.endDate = Organization.EndDate.Date.AddDays(1).AddSeconds(-1).ToString("ddMMyyyy");
                }
                catch
                {
                    organizationPostInf.endDate = Organization.EndDate.Date.ToString("ddMMyyyy");
                }
                organizationPostInf.priority = Organization.OrderNo.ToString();
                organizationPostInf.actiontype = oActionType;
                organizationPostInf.servicetype = oServiceType;

                PostResult oPost = APIOrgManagement.CreateInstance(CompanyCode).PostDHROMUnit_UI(organizationPostInf);
                if (oPost.returnCode == 101)
                {   if(oActionType == "I")
                        oMessage = oPost.returnParam;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return oMessage;
        }

        #endregion
    }
}