﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.DATACLASS
{
    public class Company
    {
        public Company()
        {

        }
        public string CompanyCode { get; set; }
        public string Name { get; set; }
        public string DomainName { get; set; }
        public string FullNameTH { get; set; }
        public string FullNameEN { get; set; }
        public bool IsActive { get; set; }
        public string Background { get; set; }
        public string Logo { get; set; }
        public string Theme { get; set; }
        public string AuthenticationMode { get; set; }
    }
}
