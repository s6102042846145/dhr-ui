﻿using System.Collections.Generic;
using ESS.SHAREDATASERVICE.DATACLASS;
using System.Data;
using ESS.SHAREDATASERVICE.CONFIG;

namespace ESS.SHAREDATASERVICE.INTERFACE
{
    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    public interface IShareDataService
    {
        string GetUserConfigurationByCompany(string oCompanycode);
        List<Company> GetCompanyList();
        Dictionary<string, Dictionary<string, Dictionary<string, string>>> GetModuleSettings();
        Dictionary<string, string> GetModuleSettings(string oCompanyCode, string oModuleID);
        Company GetCompanyByCompanyCode(string sCompanyCode);
        void CopyUserRoleAndResponse(string sCompanyCode);
        Dictionary<string, string> GetDatabaseLocation();

        DataSet GetTextDescriptionByCompany(string CompanyCode, string LanguageCode);
        List<UserRoleClass> GetAllUserRole();
        DataTable GetAlluserRoleEditor();
        List<RoleClass> GetAllRole();
        List<UserRoleResponseType> GetAllUserRoleResponseType(string oLanguage);
        List<UserRoleResponse> GetUserRoleResponse(string EmployeeID, string CompanyCode);
        void SaveUserRoleResponse(List<UserRoleResponse> lstUserRoleResponse);
        string MaintainAdminBypass(string Username, string Password, string Email, string Firstlogin, string ResponseID, string CompanyCodes,string CompanyName,string CurrentCompany);
        string AdminBypassLogin(string Username, string Password,string flag, string Company);
        int AdminBypassCompany();
        string AdminBypassResetPassword(string Username, string Flag,string Company,string CompanyName);
        void AdminBypassSaveLog(string Username, string EmployeeCode, string CompanyCode, string Description, string IPAddress, string ClientName);
        List<BypassCompany> GetBypassCompanyList();
        object GetFlowIDByRequestTypeIDKeyCode(string RequestTypeID, string KeyCode);
    }
}