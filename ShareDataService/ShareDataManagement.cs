﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE.DATACLASS;
using System.Configuration;
using System.Data;
using ESS.SHAREDATASERVICE.CONFIG;

namespace ESS.SHAREDATASERVICE
{
    public class ShareDataManagement
    {
        #region Constructor
        private ShareDataManagement()
        {

        }
        #endregion

        #region MultiCompany  Framework
        private static Dictionary<string, Dictionary<string, Dictionary<string, string>>> __settingCache = null;
        private static Dictionary<string, Company> __companyCache = null;

        private static Dictionary<string, Dictionary<string, Dictionary<string, string>>> SettingCache
        {
            get
            {
                if (__settingCache == null)
                {
                    __settingCache = GetSettingsCache();
                }
                return __settingCache;
            }
            set
            {
                __settingCache = value;
            }
        }

        private static Dictionary<string, Company> CompanyCache
        {
            get
            {
                if (__companyCache == null)
                    __companyCache = GetCompanyCache();
                return __companyCache;
            }
            set
            {
                __companyCache = value;
            }
        }
        public static string LookupCache(string sCompanyCode, string sModuleID, string sKey, string sDefaultValue)
        {
            string sReturnValue = LookupCacheInternal(sCompanyCode, sModuleID, sKey);
            
            if (sReturnValue == null)
            {
                sReturnValue = sDefaultValue;
            }
            return sReturnValue;
        }

        public static string LookupCache(string sCompanyCode, string sModuleID, string sKey)
        {
            string sReturnValue = LookupCacheInternal(sCompanyCode, sModuleID, sKey);

            if (sReturnValue == null)
            {
                throw new Exception(string.Format("ModuleSetting not found for ({0}, {1}, {2}) in company db or share db", sCompanyCode, sModuleID.ToUpper(), sKey.ToUpper()));
            }
            return sReturnValue;
        }
        private static string LookupCacheInternal(string sCompanyCode, string sModuleID, string sKey)
        {
            string sReturnValue = null;

            if (SettingCache.ContainsKey(sCompanyCode))
            {
                if (SettingCache[sCompanyCode].ContainsKey(sModuleID.ToUpper()))
                {
                    if (SettingCache[sCompanyCode][sModuleID.ToUpper()].ContainsKey(sKey.ToUpper()))
                    {
                        sReturnValue = SettingCache[sCompanyCode][sModuleID.ToUpper()][sKey.ToUpper()];
                    }
                }
            }
            
            if (sReturnValue == null)
            {
                if (SettingCache.ContainsKey("DEFAULT"))
                {
                    if (SettingCache["DEFAULT"].ContainsKey(sModuleID.ToUpper()))
                    {
                        if (SettingCache["DEFAULT"][sModuleID.ToUpper()].ContainsKey(sKey.ToUpper()))
                        {
                            sReturnValue = SettingCache["DEFAULT"][sModuleID.ToUpper()][sKey.ToUpper()];
                        }
                    }
                }
            }
            return sReturnValue;
        }
       
        #endregion MultiCompany  Framework

        private static Configuration __config;

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        public static bool SendMail
        {
            get
            {
                bool bSendMail = true;
                if (config.AppSettings.Settings["SendMail"] != null)
                    bool.TryParse(config.AppSettings.Settings["SendMail"].Value, out bSendMail);
                return bSendMail;
            }
        }

        public static bool IsTest
        {
            get
            {
                bool bIsTest = false;
                if (config.AppSettings.Settings["IsTest"] != null)
                    bool.TryParse(config.AppSettings.Settings["IsTest"].Value, out bIsTest);
                return bIsTest;
            }
        }

        public static string MailSystem
        {
            get
            {
                string strMailSystem = "ratchatawan.w@pttdigital.com";
                if (config.AppSettings.Settings["MailSystem"] != null)
                    strMailSystem = config.AppSettings.Settings["MailSystem"].Value;
                return strMailSystem;
            }
        }

        public static string IsEvent
        {
            get
            {
                return LookupCache("DEFAULT", "ESS.SECURITY", "IS_EVENT");
            }
        }

        public static string AdminKey
        {
            get
            {
                return LookupCache("DEFAULT", "ESS.SECURITY", "ADMIN_KEY");
            }
        }
        public static void ClearConfigCache()
        {
            SettingCache = null;
            CompanyCache = null;
        }

        private static Dictionary<string, Company> GetCompanyCache()
        {
            Dictionary<string, Company> oCompanyCache = new Dictionary<string, Company>();
            List<Company> lstCompany = ServiceManager.DataService.GetCompanyList();
            foreach (Company oCompany in lstCompany)
            {
                oCompanyCache[oCompany.CompanyCode] = oCompany;
            }
            return oCompanyCache;
        }
        public static List<Company> GetCompanyList()
        {
            List<Company> oReturn = new List<Company>();
            
            // return only active company
            oReturn = CompanyCache.Values.ToList<Company>().FindAll(delegate(Company oCompany) { return (oCompany.IsActive); });
            
            return oReturn;
        }

        // get company by code (active or inactive)
        public static Company GetCompanyByCompanyCode(string sCompanyCode)
        {
            Company oReturn = null;
            
            oReturn = CompanyCache[sCompanyCode];

            if (oReturn == null)
                throw new Exception(string.Format("Company Code {0} not found.", sCompanyCode));
            return oReturn;
        }
        private static Dictionary<string, Dictionary<string, Dictionary<string, string>>> GetSettingsCache()
        {
            return ServiceManager.DataService.GetModuleSettings();
        }

        public static void CopyUserRoleAndResponse(string oCompanyCode)
        {
            ServiceManager.DataService.CopyUserRoleAndResponse(oCompanyCode);
        }

        public static Dictionary<string, string> GetDatabaseLocation()
        {
            return ServiceManager.DataService.GetDatabaseLocation();
        }

        public static DataSet GetTextDescriptionByCompany(string CompanyCode, string LanguageCode)
        {
            return ServiceManager.DataService.GetTextDescriptionByCompany(CompanyCode, LanguageCode);
        }

        public static List<UserRoleClass> GetAllUserRole()
        {
            return ServiceManager.DataService.GetAllUserRole();
        }

        public static DataTable GetAlluserRoleEditor()
        {
            return ServiceManager.DataService.GetAlluserRoleEditor();
        }

        public static List<RoleClass> GetAllRole()
        {
            return ServiceManager.DataService.GetAllRole();
        }

        public static List<UserRoleResponseType> GetAllUserRoleResponseType(string oLanguage)
        {
            return ServiceManager.DataService.GetAllUserRoleResponseType(oLanguage);
        }

        public static List<UserRoleResponse> GetUserRoleResponse(string EmployeeID,string CompanyCode)
        {
            return ServiceManager.DataService.GetUserRoleResponse(EmployeeID,CompanyCode);
        }

        public static void SaveUserRoleResponse(List<UserRoleResponse> lstUserRoleResponse)
        {
            ServiceManager.DataService.SaveUserRoleResponse(lstUserRoleResponse);
        }
        public static string MaintainAdminBypass (string Username, string Password, string Email, string Firstlogin, string ResponseID, string CompanyCodes,string CompanyName,string CurrentCompany)
        {
            return ServiceManager.DataService.MaintainAdminBypass( Username,  Password,  Email,  Firstlogin,  ResponseID, CompanyCodes, CompanyName, CurrentCompany);
        }
        public static string AdminBypassLogin(string Username, string Password ,string Flag,string Company)
        {
            return ServiceManager.DataService.AdminBypassLogin(Username, Password, Flag, Company);
        }
        public static int AdminBypassCompany()
        {
            return ServiceManager.DataService.AdminBypassCompany();
        }
        public static string AdminBypassResetPassword(string Username,  string Flag ,string Company,string CompanyName)
        {
            return ServiceManager.DataService.AdminBypassResetPassword(Username, Flag, Company, CompanyName);
        }
        public static void AdminBypassSaveLog(string Username, string EmployeeCode,string CompanyCode,string Description, string IPAddress, string ClientName)
        {
            ServiceManager.DataService.AdminBypassSaveLog(Username, EmployeeCode, CompanyCode, Description, IPAddress, ClientName);
        }
        public static List<BypassCompany> GetBypassCompanyList()
        {
            return ServiceManager.DataService.GetBypassCompanyList();
        }

        
    }
}
