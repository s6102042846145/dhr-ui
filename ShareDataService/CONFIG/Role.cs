﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.CONFIG
{
    public class RoleClass
    {
        public string Role { get; set; }

        public string Description { get; set; }
    }
}
