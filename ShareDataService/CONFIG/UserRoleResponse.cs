﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.CONFIG
{
    public class UserRoleResponse 
    {
        /*public UserRoleResponse()
        {
            Type oPropertyType = null;
            foreach (PropertyDescriptor oCurrentProperty in TypeDescriptor.GetProperties(this))
            {
                oPropertyType = oCurrentProperty.PropertyType;
                if (oPropertyType.Name.ToLower() == "string")
                {
                    oCurrentProperty.SetValue(this, "");
                }
                else if (oPropertyType.Name.ToLower() == "boolean")
                {
                    oCurrentProperty.SetValue(this, false);
                }
                else if (oPropertyType.Name.IndexOf("int", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    oCurrentProperty.SetValue(this, 0);
                }
                else if (oPropertyType.Name.ToLower() == "decimal")
                {
                    oCurrentProperty.SetValue(this, Convert.ToDecimal(0.00));
                }
                else if (oPropertyType.Name.ToLower() == "double")
                {
                    oCurrentProperty.SetValue(this, 0.0);
                }
                else if (oPropertyType.Name.ToLower() == "datetime")
                {
                    oCurrentProperty.SetValue(this, DateTime.MinValue);
                }
            }
        }*/
        public string EmployeeID { get; set; }
        public string CompanyCode { get; set; }
        public string UserRole { get; set; }
        public string ResponseType { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseCompanyCode { get; set; }
        public bool includeSub { get; set; }

    }
}
