﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.CONFIG
{
    public class UserRoleEdit
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public List<UserRoleResponseContent> Content;
    }
}
