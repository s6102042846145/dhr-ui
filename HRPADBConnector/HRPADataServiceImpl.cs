﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using ESS.HR.PA.ABSTRACT;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DB
{
    public class HRPADataServiceImpl : AbstractHRPADataService
    {
        #region Constructor
        public HRPADataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();

        private static string ModuleID = "ESS.HR.PA.DB";

        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region " private data "

        //private string ConnectionString
        //{
        //    get
        //    {
        //        return config.AppSettings.Settings["BaseConnStr"].Value;
        //    }
        //}

        //private string GetConnectionString(string profile)
        //{
        //    return config.AppSettings.Settings[profile].Value;
        //}

        #endregion " private data "


        #region " GetPersonalData "
        //public override INFOTYPE0002 GetINFOTYPE0002(string EmployeeID, DateTime CheckDate)
        public override PersonalInfoCenter GetPersonalInfo(string EmployeeID, DateTime CheckDate)
        {
            PersonalInfoCenter returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 Where EmployeeID = @EmpID and @CheckDate between BeginDate and EndDate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from PA_PersonalData Where EmployeeID = @EmpID and @CheckDate between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new PersonalInfoCenter();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetPersonalData "

        #region " SaveINFOTYPE0002 "
        //public override void SaveINFOTYPE0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        public override void SavePersonalInfo(string EmployeeID1, string EmployeeID2, List<PersonalInfoCenter> List, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0002 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            //SqlCommand oCommand1 = new SqlCommand("delete from PA_PersonalData where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            //SqlCommand oCommand = new SqlCommand("select * from PA_PersonalData where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0002");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (PersonalInfoCenter item in List)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveINFOTYPE0002 "

        #region " MarkUpdate "

        public override void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from MarkData where EmployeeID = @EmpID and DataCategory = @DataCategory ", oConnection, tx);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("MARKDATA");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count == 0 && isMarkIn)
                {
                    DataRow oRow = oTable.NewRow();
                    oRow["EmployeeID"] = EmployeeID;
                    oRow["DataCategory"] = DataCategory;
                    oRow["RequestNo"] = RequestNo;
                    oTable.Rows.Add(oRow);
                }
                else if (oTable.Rows.Count > 0 && !isMarkIn)
                {
                    foreach (DataRow dr in oTable.Select())
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " MarkUpdate "

        #region " CheckMark "

        public override bool CheckMark(string EmployeeID, string DataCategory)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion " CheckMark "

        #region Check MarkData


        





        public override bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory and RequestNo <> @ReqNo", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
            oParam.Value = ReqNo;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }
        public override bool CheckMarkDummy(string DataCategory)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where DataCategory = " +
                                                 "@DataCategory", oConnection);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error " + ex.Message, ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }
        public override bool CheckMarkAcademic(string DataCategory, string ReqNo)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where DataCategory = " +
                                                 "@DataCategory and RequestNo <> @ReqNo", oConnection);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
            oParam.Value = ReqNo;
            oCommand.Parameters.Add(oParam);

            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error" + ex.Message, ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }
        #endregion Check MarkData

        //public override void SavePersonalData(INFOTYPE0002 data)
        //{
        //    SqlConnection oConnection = new SqlConnection(.GetConnectionString("INFOTYPE0002"));
        //    SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 Where EmployeeID = @EmpID and @CheckDate between BeginDate and EndDate", oConnection);
        //    SqlParameter oParam;
        //    oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
        //    oParam.Value = EmployeeID;
        //    oCommand.Parameters.Add(oParam);

        //    oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
        //    oParam.Value = CheckDate;
        //    oCommand.Parameters.Add(oParam);

        //    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
        //    DataTable oTable = new DataTable("EMPLOYEE");
        //    oConnection.Open();
        //    oAdapter.Fill(oTable);
        //    oConnection.Close();
        //    oConnection.Dispose();
        //    oCommand.Dispose();
        //    returnValue = new INFOTYPE0002();
        //    returnValue.ParseToObject(oTable);
        //    oTable.Dispose();
        //}

        public override void SaveWorkPlace(List<WorkPlaceData> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            DataTable oTable;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from WorkPlaceData", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oTable = new DataTable("WorkPlaceData");
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (WorkPlaceData item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        public override List<ControlDropdownlistDataPA> GetSearchContentPADDL(string DropdownType, string LanguageCode, DateTime StartDate, DateTime EndDate, string Param1,string Param2,string Param3)
        {
            oSqlManage["ProcedureName"] = "sp_DHR_UI_DllController";
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@dll_type"] = DropdownType;
            oParamRequest["@lang_type"] = LanguageCode;
            oParamRequest["@start_date"] = StartDate;
            oParamRequest["@end_date"] = EndDate;
            oParamRequest["@parameter01"] = Param1;
            oParamRequest["@parameter02"] = Param2;
            oParamRequest["@parameter03"] = Param3;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<ControlDropdownlistDataPA>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<ControlDropdownlistDataPAInt> GetSearchContentPAIntDDL(string DropdownType, string LanguageCode, DateTime StartDate, DateTime EndDate, string Param1, string Param2, string Param3)
        {
            oSqlManage["ProcedureName"] = "sp_DHR_UI_DllController";
            List<ControlDropdownlistDataPAInt> oResult = new List<ControlDropdownlistDataPAInt>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@dll_type"] = DropdownType;
            oParamRequest["@lang_type"] = LanguageCode;
            oParamRequest["@start_date"] = StartDate;
            oParamRequest["@end_date"] = EndDate;
            oParamRequest["@parameter01"] = Param1;
            oParamRequest["@parameter02"] = Param2;
            oParamRequest["@parameter03"] = Param3;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<ControlDropdownlistDataPAInt>(oSqlManage, oParamRequest));
            return oResult;
        }
        

        //no param in getWorkPlace() --> select all, no condition
        public override List<WorkPlaceData> GetWorkPlace()
        {
            List<WorkPlaceData> returnValue = new List<WorkPlaceData>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            WorkPlaceData wData;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from WorkPlaceData", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("WorkPlaceData");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        wData = new WorkPlaceData();
                        wData.ParseToObject(dr);
                        returnValue.Add(wData);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string oCategoryCode)
        {

            List<INFOTYPE0105> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_INFOTYPE0105Get", oConnection);
            //SqlCommand oCommand = new SqlCommand("sp_PA_CommunicationGet", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_CategoryCode", oCategoryCode);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE0105>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0105 item = new INFOTYPE0105();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE0105Update";
            //string queryStr = "sp_PA_CommunicationUpdate";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
            oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE0105 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                //CallActionLog(null, oINF105, oAction, oStatus);
            }
        }

        public override void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_INFOTYPE0105Insert";
            //string queryStr = "sp_PA_CommunicationInsert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
            oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
            string oAction = "INSERT INFOTYPE0105 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                //CallActionLog(null, oINF105, oAction, oStatus);
            }
        }

        public override void SaveINFOTYPE0105(List<INFOTYPE0105> oListINF105Update, List<INFOTYPE0105> oListINF105Insert)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand, oCommand1;
            string oAction = "UPDATE INFOTYPE0105 ";
            bool oStatus = false;
            oCommand = new SqlCommand("sp_INFOTYPE0105Update", oConnection, tx);
            //oCommand = new SqlCommand("sp_PA_CommunicationUpdate", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand1 = new SqlCommand("sp_INFOTYPE0105Insert", oConnection, tx);
            //oCommand1 = new SqlCommand("sp_PA_CommunicationInsert", oConnection, tx);
            oCommand1.CommandType = CommandType.StoredProcedure;
            try
            {
                foreach (INFOTYPE0105 oINF105Update in oListINF105Update)
                {
                    oCommand.Parameters.Clear();
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105Update.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105Update.BeginDate);
                    oCommand.Parameters.AddWithValue("@p_EndDate", oINF105Update.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105Update.EndDate : oINF105Update.EndDate.TimeOfDay.Seconds == 59 ? oINF105Update.EndDate : oINF105Update.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand.Parameters.AddWithValue("@p_SubType", oINF105Update.CategoryCode);
                    oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105Update.DataText);
                    oCommand.ExecuteNonQuery();
                }
                foreach (INFOTYPE0105 oINF105Insert in oListINF105Insert)
                {
                    oCommand1.Parameters.Clear();
                    oCommand1.Parameters.AddWithValue("@p_EmployeeID", oINF105Insert.EmployeeID);
                    oCommand1.Parameters.AddWithValue("@p_BeginDate", oINF105Insert.BeginDate);
                    oCommand1.Parameters.AddWithValue("@p_EndDate", oINF105Insert.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105Insert.EndDate : oINF105Insert.EndDate.TimeOfDay.Seconds == 59 ? oINF105Insert.EndDate : oINF105Insert.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand1.Parameters.AddWithValue("@p_SubType", oINF105Insert.CategoryCode);
                    oCommand1.Parameters.AddWithValue("@p_DataText_Long", oINF105Insert.DataText);
                    oCommand1.ExecuteNonQuery();
                }

                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                oCommand1.Dispose();
            }
        }

        #region PA AttachmentFileSet
        public override List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType)
        {

            List<PersonalAttachmentFileSet> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AttachmentFileSetGet", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestorNo", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_RequestTypeID", RequestTypeID);
            oCommand.Parameters.AddWithValue("@p_RequestSubType", RequestSubType);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FILESET");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<PersonalAttachmentFileSet>();
            foreach (DataRow dr in oTable.Rows)
            {
                PersonalAttachmentFileSet item = new PersonalAttachmentFileSet();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType)
        {

            List<PersonalAttachmentFileSet> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AttachmentExportGet", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestorNo", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_RequestTypeID", RequestTypeID);
            oCommand.Parameters.AddWithValue("@p_RequestSubType", RequestSubType);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FILESET");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<PersonalAttachmentFileSet>();
            foreach (DataRow dr in oTable.Rows)
            {
                PersonalAttachmentFileSet item = new PersonalAttachmentFileSet();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override void FileAttachmentSubTypeSave(string RequestNo, string RequestSubType)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_FileAttachmentSubTypeSave";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestNo", RequestNo);
            oCommand.Parameters.AddWithValue("@p_RequestSubType", RequestSubType);
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }
        #endregion

        #region Common
        private IList<T> ExecuteQuery<T>(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            IList<T> oResult = new List<T>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oAdapter.Fill(oTable);
                        if (oTable != null && oTable.Rows.Count > 0)
                        {
                            oResult = Convert<T>.ConvertFrom(oTable);
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private bool ExecuteNoneQuery(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            bool oResult = false;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        if (oCommand.ExecuteNonQuery() > 0)
                        {
                            oResult = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private string ExecuteScalar(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            string oResult = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        oResult = Convert.ToString(oCommand.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        #endregion

        #region Get Created Document
        public override DataTable GetCreatedDocByEmployeeID(string EmployeeID, int RequestType, string language)
        {
            DataTable returnValue = new DataTable();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_CreatedDocGet", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_requestTypeID", RequestType);
            oCommand.Parameters.AddWithValue("@p_language", language);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RequestDoc");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = oTable;

            oTable.Dispose();
            return returnValue;
        }

        public override DataTable GetCreatedDocAll(int RequestType, string language)
        {
            DataTable returnValue = new DataTable();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_CreatedDocGetAll", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_requestTypeID", RequestType);
            oCommand.Parameters.AddWithValue("@p_language", language);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RequestDoc");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = oTable;

            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region Get lasted document that flowItemcode is COMPLETED
        public override DataTable GetLastedDocByFlowItemCode(string EmployeeID, int RequestType, string FlowItemCode)
        {
            DataTable returnValue = new DataTable();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RequestDocumentGetByRequestTypeByFlowItemCode", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@RequestType", RequestType);
            oCommand.Parameters.AddWithValue("@FlowItemCode", FlowItemCode);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("LastedDoc");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = oTable;

            oTable.Dispose();
            return returnValue;
        }
        #endregion

        public override DataTable GetConfigSelectYear()
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetModuleSelectYear", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;

        }







        public override List<ControlDropdownlistDataPA> GetEducationLevelDDL(string EducationType,string LanguageCode, DateTime StartDate, DateTime EndDate,string EducationCode)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, LanguageCode, StartDate, EndDate, EducationCode,null,null);
            return oResult;
        }

        public override List<ControlDropdownlistDataPA> GetEducationInstitutionDLL(string EducationType, string LanguageCode, DateTime StartDate, DateTime EndDate, string EducationCode)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, LanguageCode, StartDate, EndDate, EducationCode, null,null);
            return oResult;
        }



        


        public override List<ControlDropdownlistDataPA> GetEducationEmployeeDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string UnitCode, string UnitLevelValue, string PostCode)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, Langugae, BeginDate, EndDate, UnitLevelValue, UnitCode, PostCode);
            return oResult;
        }


        public override List<ControlDropdownlistDataPA> GetEducationDegreeDLL(string EducationType, string LanguageCode, DateTime StartDate, DateTime EndDate, string EducationCode)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, LanguageCode, StartDate, EndDate, EducationCode, null,null);
            return oResult;
        }

        public override List<ControlDropdownlistDataPA> SubtypeDDL(string Type, string LanguageCode, DateTime StartDate, DateTime EndDate, string Code)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(Type, LanguageCode, StartDate, EndDate, Code, null, null);
            return oResult;
        }

        public override List<ControlDropdownlistDataPA> GetUserroleDLL(string Type, string LanguageCode, DateTime StartDate, DateTime EndDate, string Code)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(Type, LanguageCode, StartDate, EndDate, Code, null, null);
            return oResult;
        }

        public override List<ControlDropdownlistDataPAInt> GetPareDLL(string Type, string LanguageCode, DateTime StartDate, DateTime EndDate, string Code)
        {
            List<ControlDropdownlistDataPAInt> oResult = new List<ControlDropdownlistDataPAInt>();
            oResult = GetSearchContentPAIntDDL(Type, LanguageCode, StartDate, EndDate, Code, null, null);
            return oResult;
        }



        public override List<ControlDropdownlistDataPA> GetEducationCountryDLL(string EducationType, string LanguageCode, DateTime StartDate, DateTime EndDate, string EducationCode)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, LanguageCode, StartDate, EndDate, EducationCode, null,null);
            return oResult;
        }

        public override List<ControlDropdownlistDataPA> GetIndustryDLL(string Type, string Langugae, DateTime BeginDate, DateTime EndDate, string Code)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(Type, Langugae, BeginDate, EndDate, Code, null, null);
            return oResult;
        }
        

        public override List<ControlDropdownlistDataPA> GetEducationMainMajorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, Langugae, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);
            return oResult;
        }

        public override List<ControlDropdownlistDataPA> GetEducationMinorDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, Langugae, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);
            return oResult;
        }


        public override List<HrEditByRequestNoCenter> GetHrEditDetailByRequestNo(string DataGetegory)
        {
           
            oSqlManage["ProcedureName"] = "sp_RequestDocumentCheckEditGetByRequestNo";
            List<HrEditByRequestNoCenter> oResult = new List<HrEditByRequestNoCenter>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@DataGetegory"] = DataGetegory;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<HrEditByRequestNoCenter>(oSqlManage, oParamRequest));
            return oResult;
           
        }

        public override List<HrEditByRequestNoCenter> GetMarkHrEditList(string RequestType)
        {

            oSqlManage["ProcedureName"] = "sp_RequestDocumentHrCheckEditList";
            List<HrEditByRequestNoCenter> oResult = new List<HrEditByRequestNoCenter>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@RequestType"] = RequestType;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<HrEditByRequestNoCenter>(oSqlManage, oParamRequest));
            return oResult;

        }

    








        public override List<ControlDropdownlistDataPA> GetEducationQualificationDLL(string EducationType, string Langugae, DateTime BeginDate, DateTime EndDate, string EducationCode,string EducationLevel, string EducationLevelSelected)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetSearchContentPADDL(EducationType, Langugae, BeginDate, EndDate, EducationCode, EducationLevel, EducationLevelSelected);
            return oResult;
        }

       







        public override List<HealthReport> GetHealth(int Year, string EmployeeID)
        {
            List<HealthReport> oHealthReport = new List<HealthReport>();
            HealthReport oHeal;

            if (Year == 2020)
            {
                oHeal = new HealthReport();
                oHeal.No = 1;
                oHeal.Topic = "ตรวจสุขภาพทั่วไปโดยแพทย์";
                oHeal.DetecStandard = "";
                oHeal.DetecUse = "";
                oHeal.Remark = "ความดันโลหิตสูง แนะนำให้ควบคุมอาหาร";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 2;
                oHeal.Topic = "ตรวจเอ็กซเรย์ปอดฟิล์มเล็ก";
                oHeal.DetecStandard = "";
                oHeal.DetecUse = "";
                oHeal.Remark = "กระดูกไหปลาร้าด้านขวาหักเก่าดามเหล็กอยู่";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 3;
                oHeal.Topic = "ตรวจค่าความเข้มข้นของเม็ดเลือดแดง (g/dl)";
                oHeal.DetecStandard = "(ช 13-18)(ญ 12 - 16)";
                oHeal.DetecUse = "13";
                oHeal.Remark = "ปรกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 4;
                oHeal.Topic = "ตรวจระดับน้ำตาลในเลือด (mg/dl)";
                oHeal.DetecStandard = "(70-110)";
                oHeal.DetecUse = "115";
                oHeal.Remark = "ระดับน้ำตาลสูงกว่าค่ามาตรฐาน";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 5;
                oHeal.Topic = "ตรวจระดับกรดยูริก (mg/dl)";
                oHeal.DetecStandard = "0-7.0";
                oHeal.DetecUse = "3.2";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 6;
                oHeal.Topic = "ตรวจระดับไขมันคลอเรสเตอรอล Cholesterol (mg/dl)";
                oHeal.DetecStandard = "(<200)";
                oHeal.DetecUse = "195";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 7;
                oHeal.Topic = "ตรวจระดับไขมันไตรกลีเซอไรด์ Triglyceride (mg/dl)";
                oHeal.DetecStandard = "(<150.0)";
                oHeal.DetecUse = "73";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 8;
                oHeal.Topic = "ตรวจระดับไขมัน HDL-C (mg/dl)";
                oHeal.DetecStandard = "(>40.0)";
                oHeal.DetecUse = "92";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 9;
                oHeal.Topic = "ตรวจระดับไขมัน LDL (mg/dl)";
                oHeal.DetecStandard = "(<130)";
                oHeal.DetecUse = "89";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 10;
                oHeal.Topic = "ตรวจหาเชื้อไวรัสตับอักเสบบี";
                oHeal.DetecStandard = "";
                oHeal.DetecUse = "";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 11;
                oHeal.Topic = "ตรวจระดับโปรตีน Albumin (gm/dl)";
                oHeal.DetecStandard = "(3.5-5.4)";
                oHeal.DetecUse = "4.7";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);

                oHeal = new HealthReport();
                oHeal.No = 12;
                oHeal.Topic = "ตรวจมะเร็งลำไส้ CEA";
                oHeal.DetecStandard = "";
                oHeal.DetecUse = "";
                oHeal.Remark = "ปกติ";
                oHealthReport.Add(oHeal);
            }

            return oHealthReport;
        }

        public override DataTable GetConfigTableControl()
        {
            DataTable returnValue = new DataTable();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_GetTableControl", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_VersionID", 0);
            oCommand.Parameters.AddWithValue("@p_ModuleName", "PA_PERSONAL");

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = oTable;

            oTable.Dispose();
            return returnValue;
        }

        public override DataTable GetAllOrgUnit(string oLang)
        {
            DataTable returnValue = new DataTable();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_GetOrgAndEmployee", oConnection);
            oCommand.Parameters.AddWithValue("@p_Lang", oLang);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = oTable;

            oTable.Dispose();
            return returnValue;
        }


        public override DataSet GetAttachmentFileAllByEmployee(string EmployeeID)
        {
            DataSet oReturn = new DataSet();
            oSqlManage["ProcedureName"] = "sp_PA_FileAttachment";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oReturn = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);

            return oReturn;
        }

        public override DataTable GetUserRoleSetting()
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_UserRoleSetting", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }

        public override string SaveUserRoleSetting(DataTable userRole, string LanaguageCode)
        {
            //string ErrMsg = "Success";
            string ErrMsg = GetCommonText("ROLE_SETTING", LanaguageCode, "SAVE_ROLE_COMPLETE");
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommand2 = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        foreach (DataRow row in userRole.Rows)
                        {
                            oCommandDel = new SqlCommand("sp_UserRoleDeleteByEmployeeID", oConnection, oTransaction);
                            oCommandDel.CommandType = CommandType.StoredProcedure;
                            oCommandDel.Parameters.AddWithValue("@p_EmployeeID", row["EmployeeID"].ToString());
                            oCommandDel.ExecuteNonQuery();
                            if (Convert.ToBoolean(row["FlagDelete"]) == false)
                            {
                                foreach (DataColumn item in userRole.Columns)
                                {

                                    oCommand = new SqlCommand("sp_UserRoleSave", oConnection, oTransaction);
                                    oCommand.CommandType = CommandType.StoredProcedure;
                                    oCommand.Parameters.AddWithValue("@p_EmployeeID", row["EmployeeID"].ToString());
                                    oCommand.Parameters.AddWithValue("@p_CompanyCode", CompanyCode);

                                    if (item.ColumnName.ToString() != "EmployeeID" && item.ColumnName.ToString() != "EmployeeName")
                                    {
                                        if (row[item.ColumnName].ToString().ToLower() == "true" || row[item.ColumnName].ToString() == "1")
                                        {
                                            oCommand.Parameters.AddWithValue("@p_UserRole", item.ColumnName.ToString());
                                            oCommand.ExecuteNonQuery();
                                        }

                                    }

                                }
                            }

                        }
                        oCommand2 = new SqlCommand("sp_UserRoleResponseSetting", oConnection, oTransaction);
                        oCommand2.CommandType = CommandType.StoredProcedure;
                        oCommand2.ExecuteNonQuery();

                        oTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommand2.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }

        public override string SetCheckViewEmployee(string adminCode, string EmpCode, string ActionType,string Poscode)
        {
            //string ErrMsg = "Success";
            string Result = "";
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommand2 = null;
                    SqlCommand oCommandDel = null;
                    try
                    {

                                    oCommand = new SqlCommand("sp_CheckViewEmployee", oConnection, oTransaction);
                                    oCommand.CommandType = CommandType.StoredProcedure;
                                    oCommand.Parameters.AddWithValue("@P_AdminID", adminCode);
                                    oCommand.Parameters.AddWithValue("@P_EmployeeID", EmpCode);
                                    oCommand.Parameters.AddWithValue("@P_ActionType", ActionType);
                                    oCommand.Parameters.AddWithValue("@P_PosCode", Poscode);
                                    oCommand.ExecuteNonQuery();
                                 
                       

                        oTransaction.Commit();
                        Result = "Success";
                    }
                    catch (Exception ex)
                    {
                        Result = "Failed";
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose(); 
                        oConnection.Close();
                    }
                }
            }
            return Result;
        }

        public override DataTable GetCheckViewEmployee(string adminCode, string EmpCode, string ActionType)
        {
            DataTable oReturn = new DataTable();
            oSqlManage["ProcedureName"] = "sp_CheckViewEmployee";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@P_AdminID"] = adminCode;
            oParamRequest["@P_EmployeeID"] = EmpCode;
            oParamRequest["@P_ActionType"] = ActionType;
            oParamRequest["@P_PosCode"] = EmpCode;
            oReturn = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);

            return oReturn;
        }



        public string GetCommonText(string Category, string Language, string Code)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetCommonText(@Category,@Language) where TextCode = @TextCode", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@Category", SqlDbType.VarChar);
            oParam.Value = Category;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TextCode", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TEXTTABLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = (string)dr["TextDescription"];
            }
            oTable.Dispose();
            return returnValue;
        }

        public override DataTable GetRoleSetting()
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetRoleSetting", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }
        public override string GetStatusForResult(string RequestNo)
        {
            string result = "";
            try
            {
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oSqlManage["ProcedureName"] = "sp_PA_GetStatusForResult";
                oParamRequest["@RequestNo"] = RequestNo;
                DataTable DataDT = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);
                result = DataDT.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public override DataTable GetAllResponseCodeByResponseType(string ResponseType, string CompanyCodeResponse, string LanguageCode)
        {
            DataTable oReturn = new DataTable();
            oSqlManage["ProcedureName"] = "sp_GetAllResponseCodeByResponseType";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_ResponseType"] = ResponseType;
            oParamRequest["@p_CompanyCode"] = CompanyCodeResponse;
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oReturn = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);

            return oReturn;
        }

        #region Evaluate Setting
        public override DataTable GetEvaluateSettingYear(string Language)
        {
            DataTable oReturn = new DataTable();
            oSqlManage["ProcedureName"] = "sp_EvaluateSettingYearGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_Language"] = Language;
            oReturn = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
            return oReturn;
        }

        public override List<EvaluateSetting> GetEvaSetting()
        {
            List<EvaluateSetting> oReturn = new List<EvaluateSetting>();
            oSqlManage["ProcedureName"] = "sp_EvaluateSettingGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<EvaluateSetting>(oSqlManage, oParamRequest));

            return oReturn;
        }

        public override List<EvaluateSetting> GetEvaluateSetting()
        {
            List<EvaluateSetting> oReturn = new List<EvaluateSetting>();
            oSqlManage["ProcedureName"] = "sp_EvaluateSettingGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<EvaluateSetting>(oSqlManage, oParamRequest));

            return oReturn;
        }

        public override List<EvaluateSetting> GetEvaluateSettingByYear(int year)
        {
            List<EvaluateSetting> oReturn = new List<EvaluateSetting>();
            oSqlManage["ProcedureName"] = "[sp_EvaluateSettingGetByYear]";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_year"] = year;
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<EvaluateSetting>(oSqlManage, oParamRequest));
            return oReturn;
        }

        public override List<EvaluateSetting> GetEvaluateSettingHistory(int year)
        {
            List<EvaluateSetting> oReturn = new List<EvaluateSetting>();
            oSqlManage["ProcedureName"] = "[sp_EvaluateSettingGetAll]";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_year"] = year;
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<EvaluateSetting>(oSqlManage, oParamRequest));
            return oReturn;
        }

        public override void SaveEvaSetting(List<EvaluateSetting> data)
        {
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;

                    try
                    {
                        oCommandDel = new SqlCommand("sp_EvaluateSettingDelete", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.Parameters.AddWithValue("@p_Year", data[0].Year);
                        oCommandDel.ExecuteNonQuery();

                        //Update/Insert
                        foreach (var item in data)
                        {
                            oCommand = new SqlCommand("sp_EvaluatePeriodSettingSave", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_PeriodID", item.PeriodID);
                            oCommand.Parameters.AddWithValue("@p_Year", item.Year);
                            oCommand.Parameters.AddWithValue("@p_BeginDate", item.BeginDate);
                            oCommand.Parameters.AddWithValue("@p_EndDate", item.EndDate);
                            oCommand.Parameters.AddWithValue("@p_UpdateByEmployeeID", item.UpdateByEmployeeID);
                            oCommand.Parameters.AddWithValue("@p_UpdateByPositionID", item.UpdateByPositionID);

                            oCommand.ExecuteNonQuery();
                        }

                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        oTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
        }
        #endregion

        public override List<PhoneDirectory> GetPhoneDirectoryByCriteria(string oSearchText, string oLanguageCode)
        {
            List<PhoneDirectory> oReturn = new List<PhoneDirectory>();
            oSqlManage["ProcedureName"] = "sp_GetPhoneDirectoryData";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_SearchText"] = oSearchText;
            oParamRequest["@p_LanguageCode"] = oLanguageCode;
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<PhoneDirectory>(oSqlManage, oParamRequest));

            return oReturn;
        }


        #region dam
        public override List<ControlDropdownlistDataPA> GetDropDownDLL(string DataType, string LanguageCode, DateTime StartDate, DateTime EndDate, string param1, string param2, string param3)
        {
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            oResult = GetDropdownPADDL(DataType, LanguageCode, StartDate, EndDate, param1, param2, param3);
            return oResult;
        }
        public override List<ControlDropdownlistDataPA> GetDropdownPADDL(string DropdownType, string LanguageCode, DateTime StartDate, DateTime EndDate, string Param1, string Param2, string Param3)
        {
            oSqlManage["ProcedureName"] = "sp_DHR_UI_DllController";
            List<ControlDropdownlistDataPA> oResult = new List<ControlDropdownlistDataPA>();
            try
            {
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@dll_type"] = DropdownType;
                oParamRequest["@lang_type"] = LanguageCode;
                if (DropdownType == "PAME")
                {
                    oParamRequest["@start_date"] = null;
                    oParamRequest["@end_date"] = null;
                }
                else
                {
                    oParamRequest["@start_date"] = StartDate;
                    oParamRequest["@end_date"] = EndDate;
                }
                oParamRequest["@parameter01"] = Param1;
                oParamRequest["@parameter02"] = Param2;
                oParamRequest["@parameter03"] = Param3;
                oResult.AddRange(DatabaseHelper.ExecuteQuery<ControlDropdownlistDataPA>(oSqlManage, oParamRequest));
            }
            catch (Exception e)
            {
                throw;
            }
            return oResult;
        }


        #endregion



    }
}