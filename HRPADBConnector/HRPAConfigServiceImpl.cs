using ESS.HR.PA.ABSTRACT;
using ESS.HR.PA.CONFIG;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace ESS.HR.PA.DB
{
    public class HRPAConfigServiceImpl : AbstractHRPAConfigService
    {
        #region Constructor
        public HRPAConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();

        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");

        private static string ModuleID = "ESS.HR.PA.DB";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region " SaveConfig "

        private delegate void GetData<T>(DataTable Table, List<T> Data);

        private delegate List<T> ParseObject<T>(DataTable dt);

        private void SaveConfig<T>(string ConfigName, List<T> Data, GetData<T> Method)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            DataTable oTable = new DataTable(ConfigName.ToUpper());
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from {0}", ConfigName), oConnection);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from {0}", ConfigName);
                oCommand.Transaction = tx;
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                Method(oTable, Data);
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save {0} Error", ConfigName), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }

        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method)
        {
            return LoadConfig<T>(ConfigName, "", Method, "", false);
        }

        private List<T> LoadConfig<T>(string ConfigName, string oByConfigName, ParseObject<T> Method, string oCode, bool CaseSensitive)
        {
            return LoadConfig<T>(ConfigName, oByConfigName, Method, CaseSensitive, oCode, "");
        }

        private List<T> LoadConfig<T>(string ConfigName, string oByConfigName, ParseObject<T> Method, bool CaseSensitive, string oCode, string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            List<T> oReturn = new List<T>();
            DataTable oTable;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;

            oConnection.Open();

            oCommand = new SqlCommand(string.Format("SELECT TOP 1 COLUMN_NAME AS COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'{0}'", ConfigName), oConnection);
     
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("COLUMN_NAME");
            oAdapter.Fill(oTable);
            string KeyColumnName = "";
            foreach (DataRow dr in oTable.Rows)
            {
                KeyColumnName = dr[0].ToString();
                if (KeyColumnName.ToUpper() == "KEY")
                {
                    KeyColumnName = "[Key]";
                }
                break;
            }

            oTable = new DataTable(ConfigName.ToUpper());
            oTable.CaseSensitive = CaseSensitive;
            if (MultiLanguage)
            {
                if (oByConfigName != string.Empty)
                {
                    oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{4} ,'{1}') as MultiLanguageName from {0} where {4} = '{3}'  order by Case when {4} = '{1}'  then 1 else 2 end,MultiLanguageName", ConfigName, LanguageCode, oByConfigName, oCode, KeyColumnName), oConnection);
                }
                else
                {
                    oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{2} ,'{1}') as MultiLanguageName from {0} order by Case when {2} = '{1}'  then 1 else 2 end,MultiLanguageName", ConfigName, LanguageCode, KeyColumnName), oConnection);

                    if (ConfigName == "Province" && KeyColumnName == "CountryCode")
                    {
                        oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{2} ,'{1}') as MultiLanguageName from {0} order by Case when {2} = '{1}'  then 1 else 2 end,MultiLanguageName", ConfigName, LanguageCode, "ProvinceCode"), oConnection);
                    }
                }
            }
            else
            {
                if (ConfigName == "Country")
                    oCommand = new SqlCommand(string.Format("select * from {0} where nationality <> '' order by Case when {2} = '{1}'  then 1 else 2 end", ConfigName, LanguageCode, KeyColumnName), oConnection);
                else
                    oCommand = new SqlCommand(string.Format("select * from {0} order by Case when {2} = '{1}'  then 1 else 2 end", ConfigName, LanguageCode, KeyColumnName), oConnection);
            }
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            oReturn.AddRange(Method(oTable));
            oTable.Dispose();
            return oReturn;
        }

        private T LoadConfigSingle<T>(string ConfigName, ParseObject<T> Method, string Code)
        {
            return LoadConfigSingle<T>(ConfigName, Method, Code, "");
        }

        private T LoadConfigSingle<T>(string ConfigName, ParseObject<T> Method, string Code, string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            T oReturn = default(T);
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable(ConfigName);
            oTable.CaseSensitive = false;
            if (MultiLanguage)
            {
                oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{0}Key,'{1}') as MultiLanguageName from {0} Where {0}Key = @Key", ConfigName, LanguageCode), oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select * from {0} Where {0}Key = @Key", ConfigName), oConnection);
            }
            SqlParameter oParam = new SqlParameter("@Key", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<T> list = Method(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " SaveConfig "

        //////////////////////////////////////////////////// START Object Parser ////////////////////////////////////////////////////
        #region " Start Object Parser "

        #region " TitleName "

        private List<TitleName> ParseToTitleName(DataTable dt)
        {
            List<TitleName> oReturn = new List<TitleName>();
            foreach (DataRow dr in dt.Rows)
            {
                TitleName Item = new TitleName();
                Item.Key = (string)dr["TitleNameKey"];
                Item.Description = (string)dr["TitleName"];
                Item.Abbreviation = (string)dr["TitleAbbreviation"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " TitleName "

        #region " PrefixName "

        private List<PrefixName> ParseToPrefixName(DataTable dt)
        {
            List<PrefixName> oReturn = new List<PrefixName>();
            foreach (DataRow dr in dt.Rows)
            {
                PrefixName Item = new PrefixName();
                Item.Key = (string)dr["PrefixNameKey"];
                Item.Description = (string)dr["PrefixName"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " PrefixName "

        #region " SecondTitle"
        private List<SecondTitle> ParseToSecondTitle(DataTable dt)
        {
            List<SecondTitle> oReturn = new List<SecondTitle>();
            foreach (DataRow dr in dt.Rows)
            {
                SecondTitle Item = new SecondTitle();
                Item.Key = (string)dr["SecondTitleKey"];
                Item.Description = (string)dr["SecondTitleName"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion

        #region " MilitaryTitle"
        private List<MilitaryTitle> ParseToMilitaryTitle(DataTable dt)
        {
            List<MilitaryTitle> oReturn = new List<MilitaryTitle>();
            foreach (DataRow dr in dt.Rows)
            {
                MilitaryTitle Item = new MilitaryTitle();
                Item.Key = (string)dr["MilitaryTitleKey"];
                Item.Description = (string)dr["MilitaryName"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " AcademicTitle"
        private List<AcademicTitle> ParseToAcademicTitle(DataTable dt)
        {
            List<AcademicTitle> oReturn = new List<AcademicTitle>();
            foreach (DataRow dr in dt.Rows)
            {
                AcademicTitle Item = new AcademicTitle();
                Item.Key = (string)dr["AcademicTitleKey"];
                Item.Description = (string)dr["AcademicTitleName"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " MedicalTitle"
        private List<MedicalTitle> ParseToMedicalTitle(DataTable dt)
        {
            List<MedicalTitle> oReturn = new List<MedicalTitle>();
            foreach (DataRow dr in dt.Rows)
            {
                MedicalTitle Item = new MedicalTitle();
                Item.Key = (string)dr["MedicalTitleKey"];
                Item.Description = (string)dr["MedicalTitleName"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " NameFormat "

        private List<NameFormat> ParseToNameFormat(DataTable dt)
        {
            List<NameFormat> oReturn = new List<NameFormat>();
            Dictionary<string, NameFormat> oDic = new Dictionary<string, NameFormat>();
            NameFormat Item;
            foreach (DataRow dr in dt.Rows)
            {
                string cKey = ((string)dr["NameFormatKey"]).Trim();
                if (!oDic.ContainsKey(cKey))
                {
                    Item = new NameFormat();
                    Item.Key = (string)dr["NameFormatKey"];
                    oDic.Add(Item.Key, Item);
                }
                else
                {
                    Item = oDic[cKey];
                }
                Item.Fields.Add((string)dr["NameFormatField"]);
            }
            oReturn.AddRange(oDic.Values);
            return oReturn;
        }

        #endregion " NameFormat "

        #region " Gender "

        private List<Gender> ParseToGender(DataTable dt)
        {
            List<Gender> oReturn = new List<Gender>();
            foreach (DataRow dr in dt.Rows)
            {
                Gender Item = new Gender();
                Item.Key = (string)dr["GenderKey"];
                //ModifyBy: Ratchatawan W. (2012-02-15)
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                    Item.Description = (string)dr["MultiLanguageName"];
                else
                    Item.Description = (string)dr["GenderName"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " Gender "

        #region " Country "

        private List<Country> ParseToCountry(DataTable dt)
        {
            List<Country> oReturn = new List<Country>();
            foreach (DataRow dr in dt.Rows)
            {
                Country Item = new Country();
                Item.CountryCode = (string)dr["CountryCode"];
                //ModifyBy: Ratchatawan W. (2012-02-15)
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                    Item.CountryName = (string)dr["MultiLanguageName"];
                else
                    Item.CountryName = (string)dr["CountryName"];
                //Item.Name = (string)dr["CountryName"];
                Item.Nationality = (string)dr["Nationality"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " Country "

        #region " Province "
        private List<Province> ParseToProvince(DataTable dt)
        {
            List<Province> oReturn = new List<Province>();
            foreach (DataRow dr in dt.Rows)
            {
                //ProvinceCode,ProvinceName
                Province Item = new Province();
                Item.CountryCode = (string)dr["CountryCode"];
                Item.ProvinceCode = (string)dr["ProvinceCode"];
                Item.ProvinceName = (string)dr["ProvinceName"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.ProvinceName = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " EducationLevel "
        private List<EducationLevel> ParseToEducationLevel(DataTable dt)
        {
            List<EducationLevel> oReturn = new List<EducationLevel>();
            foreach (DataRow dr in dt.Rows)
            {
                //ProvinceCode,ProvinceName
                EducationLevel Item = new EducationLevel();
                Item.EducationLevelCode = (string)dr["EducationLevelCode"];
                Item.EducationLevelText = (string)dr["EducationLevelText"];

                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.EducationLevelText = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " MaritalStatus "

        private List<MaritalStatus> ParseToMaritalStatus(DataTable dt)
        {
            List<MaritalStatus> oReturn = new List<MaritalStatus>();
            foreach (DataRow dr in dt.Rows)
            {
                MaritalStatus Item = new MaritalStatus();
                Item.Key = (string)dr["MaritalStatusKey"];
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                else
                {
                    Item.Description = (string)dr["MaritalStatusName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " MaritalStatus "

        #region " Religion "

        private List<Religion> ParseToReligion(DataTable dt)
        {
            List<Religion> oReturn = new List<Religion>();
            foreach (DataRow dr in dt.Rows)
            {
                Religion Item = new Religion();
                Item.Key = (string)dr["ReligionKey"];
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                else
                {
                    Item.Description = (string)dr["ReligionName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " Religion "

        #region " Language "

        private List<Language> ParseToLanguage(DataTable dt)
        {
            List<Language> oReturn = new List<Language>();
            foreach (DataRow dr in dt.Rows)
            {
                Language Item = new Language();
                Item.Key = (string)dr["LanguageKey"];
                Item.Code = (string)dr["LanguageCode"];
                Item.Description = (string)dr["LanguageName"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " Language "

        #region " AddressType "

        private List<AddressType> ParseToAddressType(DataTable dt)
        {
            List<AddressType> oReturn = new List<AddressType>();
            foreach (DataRow dr in dt.Rows)
            {
                AddressType Item = new AddressType();
                Item.Key = (string)dr["AddressTypeKey"];
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                else
                {
                    Item.Description = (string)dr["AddressTypeName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " AddressType "

        #region " Bank "

        private List<Bank> ParseToBank(DataTable dt)
        {
            List<Bank> oReturn = new List<Bank>();
            foreach (DataRow dr in dt.Rows)
            {
                Bank Item = new Bank();
                Item.Key = (string)dr["BankKey"];
                Item.Description = (string)dr["BankName"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " Bank "

        #region " FamilyMemeber "

        private List<FamilyMember> ParseToFamilyMember(DataTable dt)
        {
            //List<FamilyMember> oReturn = new List<FamilyMember>();
            //foreach (DataRow dr in dt.Rows)
            //{
            //    FamilyMember Item = new FamilyMember();
            //    Item.Key = (string)dr["Key"];
            //    Item.Description = (string)dr["Description"];
            //    oReturn.Add(Item);
            //}
            //return oReturn;

            List<FamilyMember> oReturn = new List<FamilyMember>();
            foreach (DataRow dr in dt.Rows)
            {
                FamilyMember Item = new FamilyMember();
                Item.Key = (string)dr["Key"];
                Item.Description = (string)dr["Description"];
                if (dt.Columns.IndexOf("MultiLanguageName") > -1)
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " Bank "

        #endregion " End Object Parser "
        //////////////////////////////////////////////////// END Object Parser ////////////////////////////////////////////////////

        //////////////////////////////////////////////////// START IHRPAConfig LoadConfig ////////////////////////////////////////////////////
        #region IHRPAConfig Members

        #region " PAConfiguration "
        public override List<PAConfiguration> GetPAConfigurationList()
        {
            List<PAConfiguration> oReturn = new List<PAConfiguration>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PAConfigurationGet", oConnection);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oAdapter.Fill(oTable);

            foreach (DataRow dr in oTable.Rows)
            {
                PAConfiguration oPAConfiguration = new PAConfiguration();
                oPAConfiguration.ParseToObject(dr);
                oReturn.Add(oPAConfiguration);
            }
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Close();
            oConnection.Dispose();

            return oReturn;
        }
        public override DataSet GetPAConfigurationListAll()
        {
            DataSet oReturn = new DataSet();
            oSqlManage["ProcedureName"] = "sp_PAConfigurationGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oReturn = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);

            return oReturn;
        }
        public override DataSet GetPAConfigurationListByCategoryName(string categoryName)
        {
            DataSet oReturn = new DataSet();
            oSqlManage["ProcedureName"] = "sp_PAConfigurationGetByCategory";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_categoryName"] = categoryName;
            oReturn = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);

            return oReturn;
        }

        #endregion

        #region " TitleList "

        public override List<TitleName> GetTitleList(string LanguageCode)
        {
            return this.LoadConfig<TitleName>("TitleName", "", new ParseObject<TitleName>(ParseToTitleName), false, "", LanguageCode);
        }

        public override List<TitleName> GetTitleEnList(string LanguageCode)
        {
            return this.LoadConfig<TitleName>("TitleNameEn", "", new ParseObject<TitleName>(ParseToTitleName), false, "", LanguageCode);
        }

        public override TitleName GetTitle(string Code)
        {
            return this.LoadConfigSingle<TitleName>("TitleName", new ParseObject<TitleName>(ParseToTitleName), Code);
        }

        public override TitleName GetTitle(string Code, string LanguageCode)
        {
            return this.LoadConfigSingle<TitleName>("TitleName", new ParseObject<TitleName>(ParseToTitleName), Code, LanguageCode);
        }

        public override void SaveTitleList(List<TitleName> data)
        {
            this.SaveConfig<TitleName>("TitleName", data, new GetData<TitleName>(ParseTitleToTable));
        }

        private void ParseTitleToTable(DataTable oTable, List<TitleName> Data)
        {
            foreach (TitleName item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["TitleNameKey"] = item.Key;
                oNewRow["TitleName"] = item.Description;
                oNewRow["TitleAbbreviation"] = item.Abbreviation;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " TitleList "

        #region " PrefixName "

        public override List<PrefixName> GetPrefixNameList()
        {
            return this.LoadConfig<PrefixName>("PrefixName", new ParseObject<PrefixName>(ParseToPrefixName));
        }
        public override List<PrefixName> GetPrefixNameList(string LanguageCode)
        {
            return this.LoadConfig<PrefixName>("PrefixName", "", new ParseObject<PrefixName>(ParseToPrefixName), false, "", LanguageCode);
        }
        public override PrefixName GetPrefixName(string Code)
        {
            return this.LoadConfigSingle<PrefixName>("PrefixName", new ParseObject<PrefixName>(ParseToPrefixName), Code);
        }

        public override void SavePrefixNameList(List<PrefixName> data)
        {
            this.SaveConfig<PrefixName>("PrefixName", data, new GetData<PrefixName>(ParsePrefixNameToTable));
        }

        private void ParsePrefixNameToTable(DataTable oTable, List<PrefixName> Data)
        {
            foreach (PrefixName item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["PrefixNameKey"] = item.Key;
                oNewRow["PrefixName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);

            }
        }

        #endregion " PrefixName "

        #region " SecondTitle "
        public override List<SecondTitle> GetSecondTitleList()
        {
            return this.LoadConfig<SecondTitle>("SecondTitle", new ParseObject<SecondTitle>(ParseToSecondTitle));
        }
        public override List<SecondTitle> GetSecondTitleList(string LanguageCode)
        {
            return this.LoadConfig<SecondTitle>("SecondTitle", "", new ParseObject<SecondTitle>(ParseToSecondTitle), false, "", LanguageCode);
        }
        public override SecondTitle GetSecondTitle(string Code)
        {
            return this.LoadConfigSingle<SecondTitle>("SecondTitle", new ParseObject<SecondTitle>(ParseToSecondTitle), Code);
        }

        public override void SaveSecondTitleList(List<SecondTitle> data)
        {
            this.SaveConfig<SecondTitle>("SecondTitle", data, new GetData<SecondTitle>(ParseSecondTitleToTable));
        }

        private void ParseSecondTitleToTable(DataTable oTable, List<SecondTitle> Data)
        {
            foreach (SecondTitle item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["SecondTitleKey"] = item.Key;
                oNewRow["SecondTitleName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);

            }
        }
        #endregion

        #region " MilitaryTitle "
        public override List<MilitaryTitle> GetMilitaryTitleList()
        {
            return this.LoadConfig<MilitaryTitle>("MilitaryTitle", new ParseObject<MilitaryTitle>(ParseToMilitaryTitle));
        }
        public override List<MilitaryTitle> GetMilitaryTitleList(string LanguageCode)
        {
            return this.LoadConfig<MilitaryTitle>("MilitaryTitle", "", new ParseObject<MilitaryTitle>(ParseToMilitaryTitle), false, "", LanguageCode);
        }

        public override MilitaryTitle GetMilitaryTitle(string Code)
        {
            return this.LoadConfigSingle<MilitaryTitle>("MilitaryTitle", new ParseObject<MilitaryTitle>(ParseToMilitaryTitle), Code);
        }

        public override void SaveMilitaryTitleList(List<MilitaryTitle> data)
        {
            this.SaveConfig<MilitaryTitle>("MilitaryTitle", data, new GetData<MilitaryTitle>(ParseMilitaryTitleToTable));
        }

        private void ParseMilitaryTitleToTable(DataTable oTable, List<MilitaryTitle> Data)
        {
            foreach (MilitaryTitle item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["MilitaryTitleKey"] = item.Key;
                oNewRow["MilitaryTitleName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);

            }
        }
        #endregion

        #region " AcademicTitle "
        public override List<AcademicTitle> GetAcademicTitleList()
        {
            return this.LoadConfig<AcademicTitle>("AcademicTitle", new ParseObject<AcademicTitle>(ParseToAcademicTitle));
        }
        public override List<AcademicTitle> GetAcademicTitleList(string LanguageCode)
        {
            return this.LoadConfig<AcademicTitle>("AcademicTitle", "", new ParseObject<AcademicTitle>(ParseToAcademicTitle), false, "", LanguageCode);
        }

        public override AcademicTitle GetAcademicTitle(string Code)
        {
            return this.LoadConfigSingle<AcademicTitle>("AcademicTitle", new ParseObject<AcademicTitle>(ParseToAcademicTitle), Code);
        }

        public override void SaveAcademicTitleList(List<AcademicTitle> data)
        {
            this.SaveConfig<AcademicTitle>("AcademicTitle", data, new GetData<AcademicTitle>(ParseAcademicTitleToTable));
        }

        private void ParseAcademicTitleToTable(DataTable oTable, List<AcademicTitle> Data)
        {
            foreach (AcademicTitle item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["AcademicTitleKey"] = item.Key;
                oNewRow["AcademicTitleName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);

            }
        }
        #endregion

        #region " MedicalTitle "
        public override List<MedicalTitle> GetMedicalTitleList()
        {
            return this.LoadConfig<MedicalTitle>("MedicalTitle", new ParseObject<MedicalTitle>(ParseToMedicalTitle));
        }
        public override List<MedicalTitle> GetMedicalTitleList(string LanguageCode)
        {
            return this.LoadConfig<MedicalTitle>("MedicalTitle", "", new ParseObject<MedicalTitle>(ParseToMedicalTitle), false, "", LanguageCode);
        }
        public override MedicalTitle GetMedicalTitle(string Code)
        {
            return this.LoadConfigSingle<MedicalTitle>("MedicalTitle", new ParseObject<MedicalTitle>(ParseToMedicalTitle), Code);
        }

        public override void SaveMedicalTitleList(List<MedicalTitle> data)
        {
            this.SaveConfig<MedicalTitle>("MedicalTitle", data, new GetData<MedicalTitle>(ParseMedicalTitleToTable));
        }

        private void ParseMedicalTitleToTable(DataTable oTable, List<MedicalTitle> Data)
        {
            foreach (MedicalTitle item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["MedicalTitleKey"] = item.Key;
                oNewRow["MedicalTitleName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);

            }
        }
        #endregion

        #region " NameFormat "

        public override List<NameFormat> GetNameFormatList()
        {
            return this.LoadConfig<NameFormat>("NameFormat", new ParseObject<NameFormat>(ParseToNameFormat));
        }

        public override NameFormat GetNameFormat(string Code)
        {
            return this.LoadConfigSingle<NameFormat>("NameFormat", new ParseObject<NameFormat>(ParseToNameFormat), Code);
        }

        public override void SaveNameFormatList(List<NameFormat> data)
        {
            this.SaveConfig<NameFormat>("NameFormat", data, new GetData<NameFormat>(ParseNameFormatToTable));
        }

        private void ParseNameFormatToTable(DataTable oTable, List<NameFormat> Data)
        {
            foreach (NameFormat item in Data)
            {
                for (int index = 0; index < item.Fields.Count; index++)
                {
                    DataRow oNewRow = oTable.NewRow();
                    oNewRow["NameFormatKey"] = item.Key;
                    oNewRow["NameFormatID"] = index;
                    oNewRow["NameFormatField"] = item.Fields[index];
                    oTable.LoadDataRow(oNewRow.ItemArray, false);
                }
            }
        }

        #endregion " NameFormat "

        #region " Gender "

        public override List<Gender> GetGenderList(string LanguageCode)
        {
            return this.LoadConfig<Gender>("Gender", "", new ParseObject<Gender>(ParseToGender), false, "", LanguageCode);
        }

        public override Gender GetGender(string Code)
        {
            return this.LoadConfigSingle<Gender>("Gender", new ParseObject<Gender>(ParseToGender), Code);
        }

        public override Gender GetGender(string Code, string LanguageCode)
        {
            return this.LoadConfigSingle<Gender>("Gender", new ParseObject<Gender>(ParseToGender), Code, LanguageCode);
        }

        public override void SaveGenderList(List<Gender> data)
        {
            this.SaveConfig<Gender>("Gender", data, new GetData<Gender>(ParseGenderToTable));
        }

        private void ParseGenderToTable(DataTable oTable, List<Gender> Data)
        {
            foreach (Gender item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["GenderKey"] = item.Key;
                oNewRow["GenderName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " Gender "

        #region " Nationality "

        public override List<Nationality> GetAllNationality(string LanguageCode)
        {
            return this.LoadConfig<Nationality>("Nationality", "", new ParseObject<Nationality>(ParseToNationality), false, "", LanguageCode);
        }

        public override Nationality GetNationality(string Code, string LanguageCode)
        {
            return this.LoadConfigSingle<Nationality>("Nationality", new ParseObject<Nationality>(ParseToNationality), Code, LanguageCode);
        }

        private List<Nationality> ParseToNationality(DataTable dt)
        {
            List<Nationality> oReturn = new List<Nationality>();
            foreach (DataRow dr in dt.Rows)
            {
                Nationality Item = new Nationality();
                Item.NationalityKey = (string)dr["NationalityKey"];
                //ModifyBy: Ratchatawan W. (2012-02-15)
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                    Item.NationalityName = (string)dr["MultiLanguageName"];
                else
                    Item.NationalityName = (string)dr["NationalityName"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        #endregion " Nationality "

        #region " Country "
        public override List<Country> GetCountryList()
        {
            return this.LoadConfig<Country>("Country", new ParseObject<Country>(ParseToCountry));
        }

        public override List<Country> GetCountryList(string LanguageCode)
        {
            //oSqlManage["ProcedureName"] = "sp_M_CountryGetAll";
            //Modify by Pariyaporn P 03/04/2020
            //oSqlManage["ProcedureName"] = "sp_CountryGetAll";
            //List<Country> oResult = new List<Country>();
            //Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            //oParamRequest["@p_LanguageCode"] = LanguageCode;
            //oResult.AddRange(DatabaseHelper.ExecuteQuery<Country>(oSqlManage, oParamRequest));
            //return oResult;
            return this.LoadConfig<Country>("Country", "", new ParseObject<Country>(ParseToCountry), false, "", LanguageCode);
        }

        public override Country GetCountry(string Code, string LanguageCode)
        {
            return this.LoadConfigSingle<Country>("Country", new ParseObject<Country>(ParseToCountry), Code, LanguageCode);
        }

        public override Country GetCountry(string Code)
        {
            return this.LoadConfigSingle<Country>("Country", new ParseObject<Country>(ParseToCountry), Code);
        }

        public override void SaveCountryList(List<Country> data)
        {
            this.SaveConfig<Country>("Country", data, new GetData<Country>(ParseCountryToTable));
        }

        private void ParseCountryToTable(DataTable oTable, List<Country> Data)
        {
            foreach (Country item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["CountryCode"] = item.CountryCode;
                oNewRow["CountryName"] = item.CountryName;
                oNewRow["Nationality"] = item.Nationality;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " Country "

        #region " Province "

        public override List<Province> GetProvinceAll(string LanguageCode)
        {
            return this.LoadConfig<Province>("Province", "", new ParseObject<Province>(ParseToProvince), false, "", LanguageCode);
        }
        #endregion

        #region " EducationLevel "

        public override List<EducationLevel> GetAllEducationLevel(string LanguageCode)
        {
            return this.LoadConfig<EducationLevel>("EducationLevel", "", new ParseObject<EducationLevel>(ParseToEducationLevel), false, "", LanguageCode);
        }
        #endregion

        #region " MaritalStatus "

        public override List<MaritalStatus> GetMaritalStatusList(string LanguageCode)
        {
            return this.LoadConfig<MaritalStatus>("MaritalStatus", "", new ParseObject<MaritalStatus>(ParseToMaritalStatus), false, "", LanguageCode);
        }

        public override MaritalStatus GetMaritalStatus(string Code)
        {
            return this.LoadConfigSingle<MaritalStatus>("MaritalStatus", new ParseObject<MaritalStatus>(ParseToMaritalStatus), Code);
        }

        public override MaritalStatus GetMaritalStatus(string Code, string LanguageCode)
        {
            return this.LoadConfigSingle<MaritalStatus>("MaritalStatus", new ParseObject<MaritalStatus>(ParseToMaritalStatus), Code, LanguageCode);
        }

        public override void SaveMaritalStatusList(List<MaritalStatus> Data)
        {
            this.SaveConfig<MaritalStatus>("MaritalStatus", Data, new GetData<MaritalStatus>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<MaritalStatus> Data)
        {
            foreach (MaritalStatus item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["MaritalStatusKey"] = item.Key;
                oNewRow["MaritalStatusName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " MaritalStatus "

        #region " Religion "

        public override List<Religion> GetReligionList(string LanguageCode)
        {
            return this.LoadConfig<Religion>("Religion", "", new ParseObject<Religion>(ParseToReligion), false, "", LanguageCode);
        }

        public override Religion GetReligion(string Code)
        {
            return this.LoadConfigSingle<Religion>("Religion", new ParseObject<Religion>(ParseToReligion), Code);
        }

        public override Religion GetReligion(string Code, string LanguageCode)
        {
            return this.LoadConfigSingle<Religion>("Religion", new ParseObject<Religion>(ParseToReligion), Code, LanguageCode);
        }

        public override void SaveReligionList(List<Religion> Data)
        {
            this.SaveConfig<Religion>("Religion", Data, new GetData<Religion>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<Religion> Data)
        {
            foreach (Religion item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["ReligionKey"] = item.Key;
                oNewRow["ReligionName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " Religion "

        #region " Language "

        public override List<Language> GetLanguageList()
        {
            return this.LoadConfig<Language>("Language", "", new ParseObject<Language>(ParseToLanguage), "", true);
        }

        public override Language GetLanguage(string Code)
        {
            return this.LoadConfigSingle<Language>("Language", new ParseObject<Language>(ParseToLanguage), Code);
        }

        public override void SaveLanguageList(List<Language> Data)
        {
            this.SaveConfig<Language>("Language", Data, new GetData<Language>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<Language> Data)
        {
            oTable.CaseSensitive = true;
            foreach (Language item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["LanguageKey"] = item.Key;
                oNewRow["LanguageCode"] = item.Code;
                oNewRow["LanguageName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " Language "

        #region " AddressType "

        public override List<AddressType> GetAddressTypeList(string LanguageCode)
        {
            return this.LoadConfig<AddressType>("AddressType", "", new ParseObject<AddressType>(ParseToAddressType), false, "", LanguageCode);
        }

        public override void SaveAddressTypeList(List<AddressType> Data)
        {
            this.SaveConfig<AddressType>("AddressType", Data, new GetData<AddressType>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<AddressType> Data)
        {
            foreach (AddressType item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["AddressTypeKey"] = item.Key;
                oNewRow["AddressTypeName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " AddressType "

        #region " Bank "

        public override List<Bank> GetBankList(string LanguageCode)
        {
        //    return this.LoadConfig<Bank>("Bank", new ParseObject<Bank>(ParseToBank));
            return this.LoadConfig<Bank>("Bank", "", new ParseObject<Bank>(ParseToBank), false, "", LanguageCode);
        }

        public override Bank GetBank(string Code)
        {
            return this.LoadConfigSingle<Bank>("Bank", new ParseObject<Bank>(ParseToBank), Code);
        }

        public override void SaveBankList(List<Bank> data)
        {
            this.SaveConfig<Bank>("Bank", data, new GetData<Bank>(ParseBankToTable));
        }

        private void ParseBankToTable(DataTable oTable, List<Bank> Data)
        {
            foreach (Bank item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["BankKey"] = item.Key;
                oNewRow["BankName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion " Bank "

        #region " FamilyMember "

        public override List<FamilyMember> GetFamilyMemberList(string LanguageCode)
        {
            return this.LoadConfig<FamilyMember>("FamilyMember", "", new ParseObject<FamilyMember>(ParseToFamilyMember), false, "", LanguageCode);
        }

        public override FamilyMember GetFamilyMember(string Code)
        {
            return this.LoadConfigSingle<FamilyMember>("FamilyMember", new ParseObject<FamilyMember>(ParseToFamilyMember), Code);
        }
        #endregion " FamilyMember "

        //////////////////////////////////////////////////// END IHRPAConfig LoadConfig ////////////////////////////////////////////////////

        #region " GetFamilyMemberList "

        public override List<FamilyMember> GetFamilyMemberList()
        {
            List<FamilyMember> returnValue = new List<FamilyMember>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("SELECT * FROM FamilyMember ", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("FamilyMember");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        FamilyMember item = new FamilyMember();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetFamilyMemberList "

        #region " SaveFamilyMemberList "

        //public override void SaveFamilyMemberList(List<FamilyMember> Data)
        //{
        //    SqlConnection oConnection;
        //    SqlCommand oCommand;
        //    SqlDataAdapter oAdapter;
        //    DataTable oTable;
        //    SqlCommandBuilder oBuilder;
        //    DataRow dRow;
        //    using (oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        using (oCommand = new SqlCommand("select * from FamilyMember", oConnection))
        //        {
        //            oAdapter = new SqlDataAdapter(oCommand);
        //            oTable = new DataTable("FamilyMember");
        //            oBuilder = new SqlCommandBuilder(oAdapter);
        //            oConnection.Open();
        //            oAdapter.FillSchema(oTable, SchemaType.Source);
        //            oAdapter.Fill(oTable);

        //            foreach (FamilyMember item in Data)
        //            {
        //                dRow = oTable.NewRow();
        //                item.LoadDataToTableRow(dRow);
        //                oTable.LoadDataRow(dRow.ItemArray, false);
        //            }
        //            oAdapter.Update(oTable);
        //            oConnection.Close();
        //        }
        //    }
        //}

        #endregion " SaveFamilyMemberList "

        #region " GetProvinceList "

        public override List<Province> GetProvinceAll()
        {
            List<Province> returnValue = new List<Province>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Province", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Province");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Province item = new Province();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<District> GetDistrictByProvinceCodeForTravel(string oCountryCode, string oProvinceCode, string oLanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_M_DistrictGet";
            List<District> oReturn = new List<District>();
            Dictionary<string, object> oParams = new Dictionary<string, object>();
            oParams["@p_CountryCode"] = oCountryCode;
            oParams["@p_ProvinceCode"] = oProvinceCode;
            oParams["@p_LanguageCode"] = oLanguageCode;
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<District>(oSqlManage, oParams));
            return oReturn;
        }

        #endregion " GetProvinceList "

        public override Branch GetBranchData(string BranchCode)
        {
            Branch returnValue = new Branch();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Branch where BranchCode = @BranchCode", oConnection))
                {
                    oParam = new SqlParameter("@BranchCode", SqlDbType.VarChar);
                    oParam.Value = BranchCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Branch");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #region " GetAllEducationLevel "

        public override List<EducationLevel> GetAllEducationLevel()
        {
            List<EducationLevel> returnValue = new List<EducationLevel>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EducationLevel where IsActive = 1", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationLevel");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        EducationLevel item = new EducationLevel();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllEducationLevel "

        #region " GetAllCertificate "

        public override List<Certificate> GetAllCertificate()
        {
            List<Certificate> returnValue = new List<Certificate>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Certificate", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Certificate");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Certificate item = new Certificate();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllCertificate "

        #endregion IHRPAConfig Members
        //////////////////////////////////////////////////// END IHRPAConfig LoadConfig ////////////////////////////////////////////////////

        #region " SaveAllEducationLevel "

        public override void SaveAllEducationLevel(List<EducationLevel> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EducationLevel", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationLevel");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (EducationLevel item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllEducationLevel "

        #region "SaveAllCertificate"

        public override void SaveAllCertificate(List<Certificate> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Certificate", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Certificate");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Certificate item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion "SaveAllCertificate"

        #region " GetAllVocation "

        public override List<Vocation> GetAllVocation()
        {
            List<Vocation> returnValue = new List<Vocation>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Vocation", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Vocation");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Vocation item = new Vocation();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllVocation "

        #region " SaveAllVocation "

        public override void SaveAllVocation(List<Vocation> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Vocation", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Vocation");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Vocation item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllVocation "

        public override Institute GetInstitute(string InstituteCode)
        {
            Institute returnValue = new Institute();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Institute where InstituteCode = @InstituteCode", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@InstituteCode", SqlDbType.VarChar);
                    oParam.Value = InstituteCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Institute");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #region " GetAllInstitute "

        public override List<Institute> GetAllInstitute()
        {
            List<Institute> returnValue = new List<Institute>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Institute", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Institute");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Institute item = new Institute();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllInstitute "

        #region " SaveAllInstitute "

        public override void SaveAllInstitute(List<Institute> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Institute", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Institute");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Institute item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllInstitute "

        #region " GetAllHonor "

        public override List<Honor> GetAllHonor()
        {
            List<Honor> returnValue = new List<Honor>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Honor", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Honor");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Honor item = new Honor();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllHonor "

        #region " SaveAllHonor "

        public override void SaveAllHonor(List<Honor> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Honor", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Honor");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Honor item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllHonor "

        #region " GetAllBranch "

        public override List<Branch> GetAllBranch()
        {
            List<Branch> returnValue = new List<Branch>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Branch", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Branch");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Branch item = new Branch();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllBranch "

        #region " SaveAllBranch "

        public override void SaveAllBranch(List<Branch> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Branch", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Branch");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Branch item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllBranch "

        #region " GetAllCompanyBusiness "

        public override List<CompanyBusiness> GetAllCompanyBusiness()
        {
            List<CompanyBusiness> returnValue = new List<CompanyBusiness>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from CompanyBusiness", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("CompanyBusiness");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        CompanyBusiness item = new CompanyBusiness();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllCompanyBusiness "

        #region " SaveAllCompanyBusiness "

        public override void SaveAllCompanyBusiness(List<CompanyBusiness> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from CompanyBusiness", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("CompanyBusiness");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (CompanyBusiness item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllCompanyBusiness "

        #region " GetAllJobType "

        public override List<JobType> GetAllJobType()
        {
            List<JobType> returnValue = new List<JobType>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from JobType", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("JobType");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        JobType item = new JobType();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllJobType "

        #region " SaveAllJobType "

        public override void SaveAllJobType(List<JobType> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from JobType", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("JobType");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (JobType item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllJobType "

        #region " GetEducationLevel "

        public override EducationLevel GetEducationLevel(string EducationLevelCode)
        {
            EducationLevel returnValue = new EducationLevel();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EducationLevel where EducationLevelCode = @EducationLevelCode", oConnection))
                {
                    oParam = new SqlParameter("@EducationLevelCode", SqlDbType.VarChar);
                    oParam.Value = EducationLevelCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationLevel");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetEducationLevel "

        #region " GetCertificateCode "

        public override Certificate GetCertificateCode(string CertificateCode)
        {
            Certificate returnValue = new Certificate();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Certificate where CertificateCode = @CertificateCode", oConnection))
                {
                    oParam = new SqlParameter("@CertificateCode", SqlDbType.VarChar);
                    oParam.Value = CertificateCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Certificate");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetCertificateCode "

        #region " GetVocation "

        public override Vocation GetVocation(string VocationCode)
        {
            Vocation returnValue = new Vocation();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Vocation where VocationCode = @VocationCode", oConnection))
                {
                    oParam = new SqlParameter("@VocationCode", SqlDbType.VarChar);
                    oParam.Value = VocationCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("VOCATIONCODE");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetVocation "

        #region " GetCompanyBusiness "

        public override CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode)
        {
            CompanyBusiness returnValue = new CompanyBusiness();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from CompanyBusiness where BusinessCode = @BusinessCode", oConnection))
                {
                    oParam = new SqlParameter("@BusinessCode", SqlDbType.VarChar);
                    oParam.Value = CompanyBusinessCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("COMPANYBUSINESS");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetCompanyBusiness "

        #region " GetAllRelationship "

        public override List<Relationship> GetAllRelationship()
        {
            List<Relationship> returnValue = new List<Relationship>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EdulevelCert", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EdulevelCert");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Relationship item = new Relationship();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #endregion " GetAllRelationship "

        public override JobType GetJobType(string JobCode)
        {
            JobType returnValue = new JobType();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from JobType where JobCode = @JobCode", oConnection))
                {
                    oParam = new SqlParameter("@JobCode", SqlDbType.VarChar);
                    oParam.Value = JobCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("JOBTYPE");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        private List<string> GetAvailableLanguage(int LetterTypeID)
        {
            List<string> returnValue = new List<string>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from LetterLanguage where LetterTypeID = @LetterTypeID", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@LetterTypeID", SqlDbType.Int);
                    oParam.Value = LetterTypeID;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("LetterType");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        string item = (string)dr["LanguageCode"];
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<EducationGroup> GetAllEducationGroup()
        {
            List<EducationGroup> returnValue = new List<EducationGroup>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EducationGroup", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationGroup");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        EducationGroup item = new EducationGroup();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override EducationGroup GetEducationGroupByCode(string EducationGroupCode)
        {
            EducationGroup returnValue = new EducationGroup();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EducationGroup where EducationGroupCode = @EducationGroupCode", oConnection))
                {
                    oParam = new SqlParameter("@EducationGroupCode", SqlDbType.VarChar);
                    oParam.Value = EducationGroupCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationGroup");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<CourseDurationUnit> GetAllUnit()
        {
            List<CourseDurationUnit> returnValue = new List<CourseDurationUnit>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select Code, Description from NumberUnit where ForEducation = 1", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Unit");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        CourseDurationUnit item = new CourseDurationUnit();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<string> GetCertCodeFromEduLevelCode(string educationLevelCode)
        {
            List<string> returnValue = new List<string>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EdulevelCert where EducationLevel = @EduLevel", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@EduLevel", SqlDbType.VarChar);
                    oParam.Value = educationLevelCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EdulevelCert");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        EduLevelCert item = new EduLevelCert();
                        item.ParseToObject(dr);
                        returnValue.Add(item.CertificateCode);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<string> GetBranchFromEduLevelCode(string educationLevelCode)
        {
            List<string> returnValue = new List<string>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from Branch where EducationLevelCode = @EduLevel", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@EduLevel", SqlDbType.VarChar);
                    oParam.Value = educationLevelCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EdulevelCert");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Branch item = new Branch();
                        item.ParseToObject(dr);
                        returnValue.Add(item.BranchCode);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #region " SaveAllRelationship "

        public override void SaveAllRelationship(List<Relationship> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from EdulevelCert", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EdulevelCert");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Relationship item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        #endregion " SaveAllRelationship "

        public override List<Province> GetProvinceByCountryCodeForTravel(string Code, string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_ProvinceAllByCountryCode";
            List<Province> oResult = new List<Province>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CountryCode"] = Code;
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Province>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<Region> GetRegionByCountryCode(string Code, string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_M_ProvinceGet";
            List<Region> oResult = new List<Region>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CountryCode"] = Code;
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Region>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<Document> GetDocumentTypeAllForDocumentInfo( string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetDocumentTypeAllForDocumentInfo";
            List<Document> oResult = new List<Document>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Document>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override List<ValidityType> GetValidityTypeAllForDocumentInfo(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetValidityTypeAllForDocumentInfo";
            List<ValidityType> oResult = new List<ValidityType>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<ValidityType>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override bool ShowTimeAwareLink(int LinkID)
        {
            bool oReturn = false;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetTimeAwareLink", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_LinkID", LinkID);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TIMEAWARELINK");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            if (oTable.Rows.Count > 0)
                oReturn = Convert.ToBoolean(oTable.Rows[0][0]);

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            return oReturn;
        }

        public override List<AcademicCategoryType> GetAcademicCategoryTypeList(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetAcademicCategoryTypeAll";
            List<AcademicCategoryType> oResult = new List<AcademicCategoryType>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<AcademicCategoryType>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<AcademicRole> GetAcademicRoleList(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetAcademicRoleAll";
            List<AcademicRole> oResult = new List<AcademicRole>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<AcademicRole>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<AcademicSubType> GetAcademicSubTypeList(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetAcademicSubTypeAll";
            List<AcademicSubType> oResult = new List<AcademicSubType>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<AcademicSubType>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override List<Institute> GetAllInstitute(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetInstituteAll";
            List<Institute> oResult = new List<Institute>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Institute>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override List<Honor> GetAllHonor(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetHonorAll";
            List<Honor> oResult = new List<Honor>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Honor>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override List<Certificate> GetAllCertificate(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetCertificateAll";
            List<Certificate> oResult = new List<Certificate>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Certificate>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override List<Branch> GetAllBranch(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetBranchAll";
            List<Branch> oResult = new List<Branch>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<Branch>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override List<EducationGroup> GetAllEducationGroup(string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_GetEducationGroupAll";
            List<EducationGroup> oResult = new List<EducationGroup>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<EducationGroup>(oSqlManage, oParamRequest));
            return oResult;
        }
        #region Common
        private IList<T> ExecuteQuery<T>(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            IList<T> oResult = new List<T>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oAdapter.Fill(oTable);
                        if (oTable != null && oTable.Rows.Count > 0)
                        {
                            oResult = Convert<T>.ConvertFrom(oTable);
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private bool ExecuteNoneQuery(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            bool oResult = false;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        if (oCommand.ExecuteNonQuery() > 0)
                        {
                            oResult = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private string ExecuteScalar(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            string oResult = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        oResult = Convert.ToString(oCommand.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        #endregion

        #region TRANSFER Master data from TEMP_DHR TO DB
        public override bool TransferDataDHRToDB(string oProcedureName, out string oMessage)
        {
            bool oReturn = false;
            oMessage = string.Empty;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@pMessage", SqlDbType.VarChar, 1000);
            oCommand.Parameters["@pMessage"].Direction = ParameterDirection.Output;
            oConnection.Open();
            if (oCommand.ExecuteNonQuery() > 0)
            {
                oReturn = true;
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            oMessage = oCommand.Parameters["@pMessage"].Value.ToString();
            return oReturn;
        }
        #endregion

    }
}

