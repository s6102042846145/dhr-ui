using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.DB
{
    public class OmServiceImpl : AbstractOMService
    {

        #region Constructor
        public OmServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = this.BaseConnStr;
        }
        #endregion Constructor

        #region Property
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        public string CompanyCode { get; set; }
        private static string ModuleID = "ESS.EMPLOYEE.DB";
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        #endregion Member

        #region OM Config
        public override List<ObjectTypeIDRange> GetObjectIDRange(ObjectType objectType)
        {
            List<ObjectTypeIDRange> oReturn = new List<ObjectTypeIDRange>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_ObjectTypeGetIDRange", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectTypeID", objectType.ToString());

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ObjectType");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                ObjectTypeIDRange item = new ObjectTypeIDRange();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();

            if (oReturn.Count == 0)
                throw new Exception(string.Format("Not found ObjID1, ObjID2 for '{0}' in table ObjectType", objectType));

            return oReturn;
        }
        public override List<ObjectType> GetAllObjectType()
        {
            List<ObjectType> oReturn = new List<ObjectType>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_ObjectTypeGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ObjectType");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            
            foreach (DataRow dr in oTable.Rows)
            {
                ObjectType item = (ObjectType)Enum.Parse(typeof(ObjectType), dr["ObjectTypeID"].ToString()); 
                oReturn.Add(item);
            }
            oTable.Dispose();

            return oReturn;
        }
        public override List<ObjectTypeIDRange> GetAllObjectTypeIDRange()
        {
            List<ObjectTypeIDRange> oReturn = new List<ObjectTypeIDRange>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_ObjectTypeIDRangeGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ObjectTypeIDRange");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                ObjectTypeIDRange item = new ObjectTypeIDRange();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();

            return oReturn;
        }
        public override List<string> GetAllRelationType()
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_RelationGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RelationType");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                string item = dr["RelationID"].ToString();
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region GetAllObject
        private List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Mode, DateTime dtCheckDate)
        {

            List<INFOTYPE1000> returnValue = new List<INFOTYPE1000>();
            
            foreach (ObjectType objectType in objectTypes)
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand();
                oCommand.Connection = oConnection;

                if (objectType == ObjectType.P)
                {
                    oCommand.CommandText = "select 'P' as ObjectType, e.EmployeeID as ObjectID, e.BeginDate, e.EndDate, e.Name as ShortText, e.Name as Text, n.AlternateName as ShortTextEn, n.AlternateName as TextEn, 0 as Level ";
                    oCommand.CommandText += " from INFOTYPE0001 e LEFT JOIN INFOTYPE0182 n ON e.EmployeeID = n.EmployeeID";
                    //oCommand.CommandText += " from PA_Assignment e LEFT JOIN PA_AlternativeName n ON e.EmployeeID = n.EmployeeID";
                    if (dtCheckDate != DateTime.MinValue)
                    {
                        oCommand.CommandText += " AND @CheckDate BETWEEN n.BeginDate and n.EndDate where @CheckDate between e.BeginDate and e.EndDate";
                        oCommand.Parameters.AddWithValue("@CheckDate", dtCheckDate);
                    }
                }
                else
                {
                    if (dtCheckDate == DateTime.MinValue)
                    {
                        oCommand.CommandText = "select * from INFOTYPE1000 Where ObjectType = @ObjectType and @CheckDate between BeginDate and EndDate";
                        //oCommand.CommandText = "select * from OM_Organization Where ObjectType = @ObjectType and @CheckDate between BeginDate and EndDate";
                        oCommand.Parameters.AddWithValue("@ObjectType", objectType.ToString());
                        //Addition by Nipon and Lucifer
                        oCommand.Parameters.AddWithValue("@CheckDate", DateTime.Now);
                    }
                    else
                    {
                        oCommand.CommandText = "select * from INFOTYPE1000 Where ObjectType = @ObjectType and @CheckDate between BeginDate and EndDate";
                        //oCommand.CommandText = "select * from OM_Organization Where ObjectType = @ObjectType and @CheckDate between BeginDate and EndDate";
                        oCommand.Parameters.AddWithValue("@ObjectType", objectType.ToString());
                        oCommand.Parameters.AddWithValue("@CheckDate", dtCheckDate);
                    }
                }
                
                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                DataTable oTable = new DataTable("OBJECT");
                oConnection.Open();
                oAdapter.Fill(oTable);
                oCommand.Dispose();
                foreach (DataRow dr in oTable.Rows)
                {
                    INFOTYPE1000 ret = new INFOTYPE1000();
                    ret.ParseToObject(dr);
                    returnValue.Add(ret);
                }
                oTable.Dispose();
                oConnection.Close();
                oConnection.Dispose();
            }
            return returnValue;
        }
        public override List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Mode)
        {
            return GetAllObject(objectTypes, Mode, DateTime.MinValue);
        }
        public override List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, DateTime dtCheckDate)
        {
            return GetAllObject(objectTypes, "DEFAULT", dtCheckDate);
        }
        public override List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Mode)
        {
            List<INFOTYPE1000> returnValue = new List<INFOTYPE1000>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string sub = "";
            SqlCommand oCommand = new SqlCommand();
            oCommand.Connection = oConnection;
            if (objectTypes.Count > 0)
            {
                SqlParameter oParam;
                for (int index = 0; index < objectTypes.Count; index++)
                {
                    sub += string.Format(",@ObjectType{0}", index);
                    oParam = new SqlParameter(string.Format("@ObjectType{0}", index), SqlDbType.VarChar);
                    oParam.Value = objectTypes[index];
                    oCommand.Parameters.Add(oParam);
                }
                sub = "(" + sub.TrimStart(',') + ")";
                oCommand.CommandText = "select * from INFOTYPE1000 Where ObjectType in " + sub + " AND";
                //oCommand.CommandText = "select * from OM_Organization Where ObjectType in " + sub + " AND";
            }
            else
            {
                oCommand.CommandText = "select * from INFOTYPE1000 Where";
                //oCommand.CommandText = "select * from OM_Organization Where";
            }

            oCommand.CommandText += string.Format(" ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 ret = new INFOTYPE1000();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region GetAllRelation
        public override List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            List<INFOTYPE1001> returnValue = new List<INFOTYPE1001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string sub = "";
            SqlCommand oCommand = new SqlCommand();
            oCommand.Connection = oConnection;
            if (objectTypes.Count > 0)
            {
                SqlParameter oParam;
                for (int index = 0; index < objectTypes.Count; index++)
                {
                    sub += string.Format(",@ObjectType{0}", index);
                    oParam = new SqlParameter(string.Format("@ObjectType{0}", index), SqlDbType.VarChar);
                    oParam.Value = objectTypes[index];
                    oCommand.Parameters.Add(oParam);
                }
                sub = "(" + sub.TrimStart(',') + ")";
                oCommand.CommandText = "select * from INFOTYPE1001 Where ObjectType in " + sub;
                //oCommand.CommandText = "select * from OM_Relationship Where ObjectType in " + sub;
            }
            else
            {
                oCommand.CommandText = "select * from INFOTYPE1001";
                //oCommand.CommandText = "select * from OM_Relationship";
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1001>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1001 ret = new INFOTYPE1001();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }
        public override List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            List<INFOTYPE1001> returnValue = new List<INFOTYPE1001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string sub = "";
            SqlCommand oCommand = new SqlCommand();
            oCommand.Connection = oConnection;
            if (objectTypes.Count > 0)
            {
                SqlParameter oParam;
                for (int index = 0; index < objectTypes.Count; index++)
                {
                    sub += string.Format(",@ObjectType{0}", index);
                    oParam = new SqlParameter(string.Format("@ObjectType{0}", index), SqlDbType.VarChar);
                    oParam.Value = objectTypes[index];
                    oCommand.Parameters.Add(oParam);
                }
                sub = "(" + sub.TrimStart(',') + ")";
                oCommand.CommandText = "select * from INFOTYPE1001 Where ObjectType in " + sub + " AND";
                //oCommand.CommandText = "select * from OM_Relationship Where ObjectType in " + sub + " AND";
            }
            else
            {
                oCommand.CommandText = "select * from INFOTYPE1001 Where";
                //oCommand.CommandText = "select * from OM_Relationship Where";
            }

            oCommand.CommandText += string.Format(" ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1001>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1001 ret = new INFOTYPE1001();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetObjectData "

        public override INFOTYPE1000 GetObjectData(ObjectType objectType, string objectID, DateTime CheckDate, string LanguageCode)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 Where ObjectType = @ObjectType and ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization Where ObjectType = @ObjectType and ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectType", SqlDbType.VarChar);
            oParam.Value = objectType.ToString();
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = objectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = new INFOTYPE1000();
                returnValue.ParseToObject(oTable);
                if (LanguageCode != "TH")
                {
                    returnValue.Text = returnValue.TextEn;
                    returnValue.ShortText = returnValue.ShortTextEn;
                }
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetObjectData "

        #region " SaveAllObject "

        public override void SaveAllObject(List<ObjectType> objectTypes, List<INFOTYPE1000> data, string Profile)
        {
            string cOtype = "";

            foreach (ObjectType item in objectTypes)
            {
                cOtype += string.Format(",'{0}'", item);
            }

            cOtype = cOtype.TrimStart(',');

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1000 where ObjectType in ({0})", cOtype), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1000 where ObjectType in ({0})", cOtype), oConnection, tx);
            //SqlCommand oCommand1 = new SqlCommand(string.Format("delete from OM_Organization where ObjectType in ({0})", cOtype), oConnection, tx);
            //SqlCommand oCommand = new SqlCommand(string.Format("select * from OM_Organization where ObjectType in ({0})", cOtype), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1000");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1000 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }
                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveAllObject "

        #region " SaveAllRelation "

        public override void SaveAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, List<INFOTYPE1001> data, string Profile)
        {
            string cOtype = "";

            foreach (ObjectType item in objectTypes)
            {
                cOtype += string.Format(",'{0}'", item);
            }

            string cRelation = "";

            foreach (string item in relationCodes)
            {
                cRelation += string.Format(",'{0}'", item);
            }

            cOtype = cOtype.TrimStart(',');
            cRelation = cRelation.TrimStart(',');

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1001 where ObjectType in ({0}) AND Relation in ({1})", cOtype, cRelation), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1001 where ObjectType in ({0}) AND Relation in ({1})", cOtype, cRelation), oConnection, tx);
            //SqlCommand oCommand1 = new SqlCommand(string.Format("delete from OM_Relationship where ObjectType in ({0}) AND Relation in ({1})", cOtype, cRelation), oConnection, tx);
            //SqlCommand oCommand = new SqlCommand(string.Format("select * from OM_Relationship where ObjectType in ({0}) AND Relation in ({1})", cOtype, cRelation), oConnection, tx);
            
            //SqlCommand oCommand2 = new SqlCommand("CreateSnapShot", oConnection, tx);
            //SqlCommand oCommand3 = new SqlCommand("CreateTempSnapshot", oConnection, tx);
            //oCommand2.CommandType = CommandType.StoredProcedure;
            //oCommand3.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1001");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1001 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                //oCommand2.ExecuteNonQuery();
                //oCommand3.ExecuteNonQuery();

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveAllRelation "

        #region " GetOrgRoot "

        public override INFOTYPE1000 GetOrgRoot(string OrgID, int Level, DateTime CheckDate, string LanguageCode)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select @temp = dbo.GetRootOrg(@ObjectID,@Level,@CheckDate);select * from INFOTYPE1000 where objectID = @temp and @CheckDate1 between begindate and enddate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select @temp = dbo.GetRootOrg(@ObjectID,@Level,@CheckDate);select * from OM_Organization where objectID = @temp and @CheckDate1 between begindate and enddate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Level", SqlDbType.Int);
            oParam.Value = Level;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@temp", SqlDbType.VarChar);
            oParam.Value = "";
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE1000();
            returnValue.ParseToObject(oTable);
            if (LanguageCode != "TH")
            {
                returnValue.Text = returnValue.TextEn;
                returnValue.ShortText = returnValue.ShortTextEn;
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetOrgRoot "

        #region " GetRelationObjects "

        public override List<INFOTYPE1000> GetRelationObjects(string ObjectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, string relation1, ObjectType nextObjectType1, bool isReverse1, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string cSQL = "select A.ObjectType,A.ObjectID,dbo.GetMaxDate(A.BeginDate,B.BeginDate) As BeginDate,dbo.GetMinDate(A.EndDate,B.EndDate) As EndDate,A.ShortText,A.[Text],A.ShortTextEn,A.TextEn,A.[Level],Count(C.ObjectID) CountChildren";
            cSQL += "\nfrom INFOTYPE1000 A inner join INFOTYPE1001 B";
            if (isReverse)
            {
                cSQL += "\non B.objecttype = A.ObjectType and B.ObjectID = A.ObjectID";
                if (isReverse1)
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.NextObjectType = B.ObjectType and C.NextObjectID = B.ObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                    //cSQL += "\nleft outer join OM_Relationship C on C.NextObjectType = B.ObjectType and C.NextObjectID = B.ObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                else
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.ObjectType = B.ObjectType and C.ObjectID = B.ObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                    //cSQL += "\nleft outer join OM_Relationship C on C.ObjectType = B.ObjectType and C.ObjectID = B.ObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                cSQL += "\nwhere B.relation = @relation and B.nextobjectid = @ObjectID and B.nextobjecttype = @nextObjectType and B.ObjectType = @ObjectType and @CheckDate between A.BeginDate and A.EndDate and @CheckDate between B.BeginDate and B.EndDate";
            }
            else
            {
                cSQL += "\non B.nextobjecttype = A.ObjectType and B.nextObjectID = A.ObjectID";
                if (isReverse1)
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.NextObjectType = B.NextObjectType and C.NextObjectID = B.NextObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                    //cSQL += "\nleft outer join OM_Relationship C on C.NextObjectType = B.NextObjectType and C.NextObjectID = B.NextObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                else
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.ObjectType = B.ObjectType and C.NextObjectID = B.NextObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                    //cSQL += "\nleft outer join OM_Relationship C on C.ObjectType = B.ObjectType and C.NextObjectID = B.NextObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                cSQL += "\nwhere B.relation = @relation and B.objectid = @ObjectID and B.objecttype = @nextObjectType and B.nextObjectType = @ObjectType and @CheckDate between A.BeginDate and A.EndDate and @CheckDate between B.BeginDate and B.EndDate";
            }
            cSQL += "\ngroup by A.ObjectType,A.ObjectID,A.BeginDate,B.BeginDate,A.EndDate,B.EndDate,A.ShortText,A.[Text],A.ShortTextEn,A.TextEn,A.[Level],B.OrderKey";
            cSQL += "\nOrder By B.OrderKey";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@relation", SqlDbType.VarChar);
            oParam.Value = relation;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = ObjectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@nextObjectType", SqlDbType.VarChar);
            oParam.Value = nextObjectType;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ObjectType", SqlDbType.VarChar);
            oParam.Value = type;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@relation1", SqlDbType.VarChar);
            oParam.Value = relation1;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            INFOTYPE1000 item;
            foreach (DataRow dr in oTable.Rows)
            {
                item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                item.HaveChild = (int)dr["CountChildren"] > 0;
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
            //return new List<INFOTYPE1000>();
        }

        #endregion " GetRelationObjects "

        #region " GetEmployeeUnder "

        public override List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate)
        {
            List<EmployeeData> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string cSQL = "select B.NextObjectID from INFOTYPE1001 A ";
            cSQL += "\ninner join INFOTYPE1001 B on B.ObjectType = A.ObjectType and B.ObjectID = A.ObjectID and B.Relation = 'A008' and B.NextObjectType = 'P'";
            cSQL += "\nwhere A.relation = 'A003' and A.NextObjectType = 'O' and A.NextObjectID = @ObjectID and @CheckDate between A.BeginDate and A.EndDate and @CheckDate between B.BeginDate and B.EndDate";
            cSQL = "select EmployeeID from INFOTYPE1001_P_O where OrgUnit = @ObjectID and @CheckDate between BeginDate and EndDate";
            //string cSQL = "select B.NextObjectID from OM_Relationship A ";
            //cSQL += "\ninner join OM_Relationship B on B.ObjectType = A.ObjectType and B.ObjectID = A.ObjectID and B.Relation = 'A008' and B.NextObjectType = 'P'";
            //cSQL += "\nwhere A.relation = 'A003' and A.NextObjectType = 'O' and A.NextObjectID = @ObjectID and @CheckDate between A.BeginDate and A.EndDate and @CheckDate between B.BeginDate and B.EndDate";

            //cSQL = "select EmployeeID from OM_Relationship_PO_BelongTo where OrgUnit = @ObjectID and @CheckDate between BeginDate and EndDate";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<EmployeeData>();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue.Add(new EmployeeData((string)dr[0]));
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetEmployeeUnder "

        #region " IsHaveSubOrdinate "

        public override bool IsHaveSubOrdinate(string PositionID, DateTime CheckDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select count(*) as SubOrdinateCount from GetApprovalDetail(@positionid , @CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@positionid", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            int nCount;
            oConnection.Open();
            nCount = (int)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return nCount > 0;
        }

        #endregion " IsHaveSubOrdinate "

        #region " SaveAllPositionBandLevel "

        public override void SaveAllPositionBandLevel(List<INFOTYPE1013> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1013"), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1013"), oConnection, tx);
            //SqlCommand oCommand1 = new SqlCommand(string.Format("delete from OM_EmployeeGroup"), oConnection, tx);
            //SqlCommand oCommand = new SqlCommand(string.Format("select * from OM_EmployeeGroup"), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1013");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1013 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override void SaveAllJobIndex(List<INFOTYPE1513> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1513"), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1513"), oConnection, tx);
            //SqlCommand oCommand1 = new SqlCommand(string.Format("delete from OM_Position_LineStaff"), oConnection, tx);
            //SqlCommand oCommand = new SqlCommand(string.Format("select * from OM_Position_LineStaff"), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1513");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1513 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion " SaveAllPositionBandLevel "

        #region " GetAllPositionBandLevel "

        public override List<INFOTYPE1013> GetAllPositionBandLevel(string Profile)
        {
            List<INFOTYPE1013> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1013", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_EmployeeGroup", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1013");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1013>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1013 ret = new INFOTYPE1013();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile)
        {
            List<INFOTYPE1013> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand(String.Format("select * from INFOTYPE1013 Where ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2), oConnection);
            //SqlCommand oCommand = new SqlCommand(String.Format("select * from OM_EmployeeGroup Where ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2), oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1013");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1013>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1013 ret = new INFOTYPE1013();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetAllPositionBandLevel "

        #region " GetPositionBandLevel "

        public override INFOTYPE1013 GetPositionBandLevel(string PositionID, DateTime CheckDate)
        {
            INFOTYPE1013 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1013 Where ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_EmployeeGroup Where ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1013");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE1013();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetPositionBandLevel "

        #region " GetEmployeeByPositionID "

        public override EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            EmployeeData returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string cSQL = "select A.NextObjectID As EmployeeID from INFOTYPE1001_A008 A";
            cSQL += "\nwhere A.ObjectID = @positionid and A.ObjectType = 'S' and A.Relation = 'A008' and A.NextObjectType = 'P' and @CheckDate between A.BeginDate and A.EndDate";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@positionid", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = new EmployeeData((string)dr[0], PositionID, CompanyCode, DateTime.Now);
                break;
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetEmployeeByPositionID "

        #region " SaveAllApprovalData "

        public override void SaveAllApprovalData(List<INFOTYPE1003> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1003"), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1003"), oConnection, tx);
            //SqlCommand oCommand1 = new SqlCommand(string.Format("delete from OM_OrganizationDepartment"), oConnection, tx);
            //SqlCommand oCommand = new SqlCommand(string.Format("select * from OM_OrganizationDepartment"), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1003");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1003 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveAllApprovalData "

        #region " GetAllApprovalData "

        public override List<INFOTYPE1003> GetAllApprovalData(string Profile)
        {
            List<INFOTYPE1003> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1003", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_OrganizationDepartment", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1003>();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1003 ret = new INFOTYPE1003();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }

            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile)
        {
            List<INFOTYPE1003> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand(String.Format("select * from INFOTYPE1003 Where ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2), oConnection);
            //SqlCommand oCommand = new SqlCommand(String.Format("select * from OM_OrganizationDepartment Where ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2), oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1003>();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1003 ret = new INFOTYPE1003();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }

            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetAllApprovalData "

        #region " GetApprovalData "

        public override INFOTYPE1003 GetApprovalData(string PositionID, DateTime CheckDate)
        {
            INFOTYPE1003 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1003 Where ObjectType = 'S' and ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_OrganizationDepartment Where ObjectType = 'S' and ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE1003();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetApprovalData "

        #region " GetOrgUnder "

        public override List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'O' and objectID in (select objectid from infotype1001_a002 where nextobjectid = @objectid and nextobjecttype = 'O' and @CheckDate1 between begindate and enddate) and @CheckDate1 between begindate and enddate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization where objecttype = 'O' and objectID in (select objectid from OM_Relationship_ReportsTo where nextobjectid = @objectid and nextobjecttype = 'O' and @CheckDate1 between begindate and enddate) and @CheckDate1 between begindate and enddate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetOrgUnder "

        #region GetOrgUnit
        public override List<INFOTYPE1000> GetOrgUnit(string OrgID, bool IncludeSub)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE1000GetChildOrgUnit", oConnection);
            //SqlCommand oCommand = new SqlCommand("sp_OM_OrganizationGetChildOrgUnit", oConnection);
            oCommand.CommandTimeout = 600;
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.AddWithValue("@p_OrgUnitID", OrgID);
            oCommand.Parameters.AddWithValue("@p_IncludeSub", IncludeSub);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetAllPosition "

        public override List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID in (select objectid from infotype1001_a008 where NextObjectID = @EmployeeID and @CheckDate1 between begindate and enddate)", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID in (select objectid from OM_Relationship_Holder where NextObjectID = @EmployeeID and @CheckDate1 between begindate and enddate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1000> GetAllPositionForHrMaster(DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID not in (select objectid from infotype1001 where Relation = 'A008' and @CheckDate1 between begindate and enddate)", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID not in (select objectid from OM_Relationship where Relation = 'A008' and @CheckDate1 between begindate and enddate)", oConnection);
            SqlParameter oParam;
            //oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            //oParam.Value = EmployeeID;
            //oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1000> GetAllPositionNoCheckDate(string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'S'", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization where objecttype = 'S'", oConnection);


            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1000> GetPositionByOrganize(string Organize, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID in (select objectid from infotype1001_a003 where NextObjectID = @Organize and @CheckDate1 between begindate and enddate)", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID in (select objectid from OM_Relationship_BelongTo where NextObjectID = @Organize and @CheckDate1 between begindate and enddate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Organize", SqlDbType.VarChar);
            oParam.Value = Organize;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1000> GetPositionByOrganizeForHRMaster(string Organize, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID in (select objectid from infotype1001 where NextObjectType = 'O' and NextObjectID = @Organize and @CheckDate1 between begindate and enddate and ObjectType = 'S' and ObjectID not in (select ObjectID from infotype1001 where Relation = 'A008' and ObjectType = 'S' and @CheckDate1 between BeginDate and EndDate))", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID in (select objectid from OM_Relationship where NextObjectType = 'O' and NextObjectID = @Organize and @CheckDate1 between begindate and enddate and ObjectType = 'S' and ObjectID not in (select ObjectID from OM_Relationship where Relation = 'A008' and ObjectType = 'S' and @CheckDate1 between BeginDate and EndDate))", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Organize", SqlDbType.VarChar);
            oParam.Value = Organize;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetAllPosition "

        public override INFOTYPE1000 GetOrgUnit(string PositionID, DateTime CheckDate, string Language)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 Where ObjectID in (select NextObjectID from INFOTYPE1001_A003 where @CheckDate between BeginDate and EndDate and ObjectID = @PositionID) and @CheckDate Between BeginDate and EndDate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Organization Where ObjectID in (select NextObjectID from OM_Relationship_BelongTo where @CheckDate between BeginDate and EndDate and ObjectID = @PositionID) and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@PositionID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = new INFOTYPE1000();
                returnValue.ParseToObject(dr);
                if (Language != "TH")
                {
                    returnValue.Text = returnValue.TextEn;
                    returnValue.ShortText = returnValue.ShortTextEn;
                }
                break;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A002' and NextObjectType = 'S' and @CheckDate between BeginDate and EndDate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select NextObjectID from OM_Relationship where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A002' and NextObjectType = 'S' and @CheckDate between BeginDate and EndDate", oConnection);
            SqlCommand oCommand1;
            SqlParameter oParam;
            SqlDataAdapter oAdapter1;
            DataTable oTable1;
            oParam = new SqlParameter("@CurrentID", SqlDbType.VarChar);
            oParam.Value = CurrentPositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RELATION");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            string reportToID = "";

            if (oTable.Rows.Count > 0)
            {
                reportToID = (string)oTable.Rows[0]["NextObjectID"];
            }

            if (reportToID == "")
            {
                oCommand.CommandText = "select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A003' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate";
                //oCommand.CommandText = "select NextObjectID from OM_Relationship where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A003' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate";
                oTable = new DataTable("RELATION");
                oConnection.Open();
                oAdapter.Fill(oTable);
                oConnection.Close();

                string currentOrgUnit = "";

                if (oTable.Rows.Count > 0)
                {
                    currentOrgUnit = (string)oTable.Rows[0]["NextObjectID"];
                }
                while (currentOrgUnit.Trim() != "")
                {
                    oCommand1 = new SqlCommand("select ObjectID from INFOTYPE1001 where NextObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A012' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate", oConnection);
                    //oCommand1 = new SqlCommand("select ObjectID from OM_Relationship where NextObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A012' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate", oConnection);
                    oParam = new SqlParameter("@CurrentID", SqlDbType.VarChar);
                    oParam.Value = currentOrgUnit;
                    oCommand1.Parameters.Add(oParam);

                    oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
                    oParam.Value = CheckDate.Date;
                    oCommand1.Parameters.Add(oParam);

                    oAdapter1 = new SqlDataAdapter(oCommand1);
                    oTable1 = new DataTable("RELATION1");
                    oConnection.Open();
                    oAdapter1.Fill(oTable1);
                    oConnection.Close();
                    string managerPosID = "";
                    if (oTable1.Rows.Count > 0)
                    {
                        managerPosID = (string)oTable1.Rows[0]["ObjectID"];
                    }
                    if (managerPosID != "" && managerPosID != CurrentPositionID)
                    {
                        oCommand1.CommandText = "select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A008' and NextObjectType = 'P' and @CheckDate between BeginDate and EndDate";
                        //oCommand1.CommandText = "select NextObjectID from OM_Relationship where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A008' and NextObjectType = 'P' and @CheckDate between BeginDate and EndDate";
                        oCommand1.Parameters["@CurrentID"].Value = managerPosID;

                        oTable1 = new DataTable("RELATION1");
                        oConnection.Open();
                        oAdapter1.Fill(oTable1);
                        oConnection.Close();
                        if (oTable1.Rows.Count > 0)
                        {
                            reportToID = managerPosID;
                            break;
                        }
                    }

                    oCommand1.CommandText = "select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'O' and Relation = 'A002' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate";
                    //oCommand1.CommandText = "select NextObjectID from OM_Relationship where ObjectID = @CurrentID and ObjectType = 'O' and Relation = 'A002' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate";
                    oCommand1.Parameters["@CurrentID"].Value = currentOrgUnit;

                    oTable1 = new DataTable("RELATION1");
                    oConnection.Open();
                    oAdapter1.Fill(oTable1);
                    oConnection.Close();
                    if (oTable1.Rows.Count > 0)
                    {
                        currentOrgUnit = (string)oTable1.Rows[0]["NextObjectID"];
                    }
                    else
                    {
                        currentOrgUnit = "";
                    }
                }
            }

            oConnection.Dispose();
            oCommand.Dispose();
            if (reportToID != "")
            {
                returnValue = OMManagement.CreateInstance(CompanyCode).GetObjectData(ObjectType.S, reportToID, CheckDate, LanguageCode);
            }
            return returnValue;
        }

        public override INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1001 Where ObjectID = @OrgID and ObjectType='O' and NextObjectType='O' and @CheckDate Between BeginDate and EndDate", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_Relationship Where ObjectID = @OrgID and ObjectType='O' and NextObjectType='O' and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@OrgID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = this.GetObjectData(ObjectType.O, (string)dr["NextObjectID"], CheckDate, "TH");
                break;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<EmployeeData> GetEmployeeByOrg(string OrgID, DateTime CheckDate)
        {
            List<EmployeeData> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            String cSQL = "select EmployeeID from INFOTYPE1001_P_O where OrgUnit = @ObjectID and @CheckDate between BeginDate and EndDate";
            //String cSQL = "select EmployeeID from OM_Relationship_PO_BelongTo where OrgUnit = @ObjectID and @CheckDate between BeginDate and EndDate";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<EmployeeData>();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue.Add(new EmployeeData((string)dr[0]));
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode)
        {
            List<OrganizationWithLevel> oReturn = new List<OrganizationWithLevel>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_UserRoleResponseAllOrganizationWithLevelGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_Orgunit", SqlDbType.VarChar);
            oParam.Value = Orgunit;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Orglevel", SqlDbType.Int);
            oParam.Value = 0;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Orgparent", SqlDbType.VarChar);
            oParam.Value = string.Empty;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Language", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            OrganizationWithLevel item;
            foreach (DataRow dr in oTable.Rows)
            {
                item = new OrganizationWithLevel();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-11-07)
        public override List<EmployeeData> GetEmployeeByOrg(string OrgID, DateTime BeginDate, DateTime EndDate)
        {
            List<EmployeeData> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetByOrganizationID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_OrganizationID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<EmployeeData>();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue.Add(new EmployeeData((string)dr[0]));
            }
            oTable.Dispose();
            return returnValue;
        }

        #region " SaveOrgUnitLevel "

        public override void SaveOrgUnitLevel(List<INFOTYPE1010> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1010"), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1010"), oConnection, tx);
            //SqlCommand oCommand1 = new SqlCommand(string.Format("delete from OM_OrganizationLevel"), oConnection, tx);
            //SqlCommand oCommand = new SqlCommand(string.Format("select * from OM_OrganizationLevel"), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("OM_OrganizatioINFOTYPE1010nLevel");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1010 item in data)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveOrgUnitLevel "

        public override INFOTYPE1010 GetLevelByOrg(string ObjectID, DateTime CheckDate)
        {
            INFOTYPE1010 oReturn = null;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1010 where ObjectID = @ObjectID", oConnection);
            //SqlCommand oCommand = new SqlCommand("select * from OM_OrganizationLevel where ObjectID = @ObjectID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = ObjectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1010");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                DataRow row = oTable.Rows[0];
                oReturn = new INFOTYPE1010();
                oReturn.ParseToObject(row);
            }
            return oReturn;
        }

        public override INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT job.*");
            sb.AppendLine("FROM dbo.INFOTYPE1001_a007 rel INNER JOIN dbo.INFOTYPE1000 job");
            sb.AppendLine("	ON rel.ObjectID = job.ObjectID");
            sb.AppendLine("		AND rel.ObjectType = job.ObjectType");
            sb.AppendLine("		AND @CheckDate BETWEEN rel.BeginDate AND rel.EndDate");
            sb.AppendLine("		AND @CheckDate BETWEEN job.BeginDate AND job.EndDate");
            sb.AppendLine("WHERE rel.NextObjectID=@PositionID AND rel.NextObjectType = 'S'");
            SqlCommand oCommand = new SqlCommand(sb.ToString(), oConnection);
            oCommand.Parameters.AddWithValue("@PositionID", PositionID);
            oCommand.Parameters.AddWithValue("@CheckDate", CheckDate);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = new INFOTYPE1000();
                returnValue.ParseToObject(oTable);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override INFOTYPE1000 GetOrganizationByPositionID(string PositionID, string LanguageCode)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetOrgByPositionID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_PositionID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            //Kiattiwat K. 13-09-2012
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            INFOTYPE1000 item = new INFOTYPE1000();
            foreach (DataRow dr in oTable.Rows)
            {
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
            }
            oTable.Dispose();
            return item;
        }

        public override List<UserRoleResponse> GetUserRoleResponse()
        {
            oSqlManage["ProcedureName"] = "sp_UserRoleResponseGetAll";
            List<UserRoleResponse> oResult = new List<UserRoleResponse>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<UserRoleResponse>(oSqlManage, oParamRequest));
            return oResult;
        }
        public override void SaveUserRoleResponseSnapshot(List<UserRoleResponseSnapShot> lstSnapshot)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            try
            {
                foreach (UserRoleResponseSnapShot oSnapshot in lstSnapshot)
                {
                    //Save TravelGroupRequest
                    oCommand = new SqlCommand("sp_UserRoleResponseSnapShotSave", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;

                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oSnapshot.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_CompanyCode", oSnapshot.CompanyCode);
                    oCommand.Parameters.AddWithValue("@p_UserRole", oSnapshot.UserRole);
                    oCommand.Parameters.AddWithValue("@p_ResponseType", oSnapshot.ResponseType);
                    oCommand.Parameters.AddWithValue("@p_ResponseCode", oSnapshot.ResponseCode);
                    oCommand.Parameters.AddWithValue("@p_ResponseCompanyCode", oSnapshot.ResponseCompanyCode);
                    oCommand.Parameters.AddWithValue("@p_BeginDate", oSnapshot.BeginDate);
                    oCommand.Parameters.AddWithValue("@p_EndDate", oSnapshot.EndDate);

                    oCommand.ExecuteNonQuery();
                    oCommand.Dispose();
                }
                oTransaction.Commit();
            }
            catch (Exception e)
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                throw e;
            }
            oConnection.Close();
        }

        public override void CreateSnapshot()
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            try
            {
                //Create SnapShot
                oCommand = new SqlCommand("CreateSnapShot", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.CommandTimeout = 60000;
                oCommand.ExecuteNonQuery();
                oCommand.Dispose();
             
                oTransaction.Commit();
            }
            catch (Exception e)
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                throw e;
            }
            oConnection.Close();
        }

        public override void CreateRequestFlowReceipientSnapshot()
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            try
            {
                //Create SnapShot
                oCommand = new SqlCommand("CreateRequestFlowReceipientSnapshot", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.CommandTimeout = 60000;
                oCommand.ExecuteNonQuery();
                oCommand.Dispose();
             
                oTransaction.Commit();
            }
            catch (Exception e)
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                throw e;
            }
            oConnection.Close();
        }
        #region Common
        private IList<T> ExecuteQuery<T>(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            IList<T> oResult = new List<T>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oAdapter.Fill(oTable);
                        if (oTable != null && oTable.Rows.Count > 0)
                        {
                            oResult = Convert<T>.ConvertFrom(oTable);
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private bool ExecuteNoneQuery(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            bool oResult = false;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        if (oCommand.ExecuteNonQuery() > 0)
                        {
                            oResult = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private string ExecuteScalar(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            string oResult = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        oResult = Convert.ToString(oCommand.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        #endregion

        public override void MapEmpSubGroup()
        {
            oSqlManage["ProcedureName"] = "sp_MapEmpSubGroup";
			Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }
        public override int GetBrandNoByEmpSubGroup(decimal EmployeeID)
        {
            int BrandNo = 0;
            try
            {
                oSqlManage["ProcedureName"] = "sp_Job_Brand_GetByEmpSubGroup";
                Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
                oParamRequest["@p_EmpSubGroup"] = EmployeeID;
                DataTable dt = DatabaseHelper.ExecuteQueryToDataTable(oSqlManage, oParamRequest);
                if (dt.Rows.Count > 0)
                {
                    BrandNo = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
            }
            catch(Exception ex)
            {

            }
            return BrandNo;
        }
    }
}