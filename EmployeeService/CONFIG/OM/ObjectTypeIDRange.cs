using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    public class ObjectTypeIDRange : AbstractObject
    {
        public ObjectTypeIDRange()
        {
        }
        public string ObjectTypeID { get; set; }
        public string ObjectID1 { get; set; }
        public string ObjectID2 { get; set; }
    }
}