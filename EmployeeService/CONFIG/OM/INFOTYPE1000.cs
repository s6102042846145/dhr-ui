using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.UTILITY.EXTENSION;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    
    public class INFOTYPE1000 : AbstractObject
    {
        public INFOTYPE1000()
        {
        }


        public INFOTYPE1000 FindDivisionData(string LanguageCode)
        {
            if (ObjectType != ObjectType.O)
                return null;
            return OMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).FindDivision(this, LanguageCode);
        }

        public INFOTYPE1000 FindDepartmentData(string LanguageCode)
        {
            if (ObjectType != ObjectType.O)
                return null;
            return OMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).FindDepartment(this, LanguageCode);
        }

        public INFOTYPE1000 FindVCData(string LanguageCode)
        {
            if (ObjectType != ObjectType.O)
                return null;
            return OMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).FindVC(this, LanguageCode);
        }

        public INFOTYPE1000 FindOrgUnit(string LanguageCode)
        {
            if (ObjectType != ObjectType.S)
                return null;
            return OMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).FinOrgUnit(this, LanguageCode);
        }

        //protected override string GetEnumString(PropertyInfo prop)
        //{
        //    if (prop.Name == "ObjectType")
        //    {
        //        return Convert.ToChar((int)prop.GetValue(this, null)).ToString();
        //    }
        //    else
        //    {
        //        return base.GetEnumString(prop);
        //    }
        //}

        public ObjectType ObjectType { get; set; }

        public string ObjectID { get; set; }

        public string SubBand { get; set; }
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string ShortText { get; set; }
        public string Text { get; set; }
        public string ShortTextEn { get; set; }

        public string TextEn { get; set; }

        public int Level
        {
            get;
            set;
            //get
            //{
            //    int oLevel = 0;
            //    INFOTYPE1010 orgLevel = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorOMService("DB").GetLevelByOrg(this.ObjectID, this.BeginDate);

            //    if (orgLevel == null)
            //        oLevel = 0;
            //    else
            //        oLevel = int.Parse(orgLevel.OrgLevel);

            //    return oLevel;
            //}
            //set
            //{

            //}
        }

        public string AlternativeName(string Language)
        {
            if (Language.ToUpper() == "TH")
            {
                return this.Text;
            }
            else
            {
                return this.TextEn;
            }
        }

        public string AlternativeShortName(string Language)
        {
            if (Language.ToUpper() == "TH")
            {
                return this.ShortText;
            }
            else
            {
                return this.ShortTextEn;
            }
        }

        public bool HaveChild { get; set; }
        public INFOTYPE1000 GetParent(string LangeuageCode)
        {
            if (ObjectType != ObjectType.O)
                return null;
            return OMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetOrgParent(this.ObjectID, this.BeginDate);
        }
        public string PositionName
        {
            get
            {
                return string.Format("{0} : {1}", ObjectID, Text);
            }
        }
        public string BudgetOrgName
        {
            get
            {
                return string.Format("{0} : {1}", ObjectID, Text);
            }
        }

    }
}