using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    [Serializable()]
    public class INFOTYPE1013 : AbstractObject
    {
        public INFOTYPE1013()
        {
        }

        public string ObjectID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
        public int BrandNo { get; set; }

    }
}