using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    [Serializable()]
    public class INFOTYPE1003 : AbstractObject
    {

        public INFOTYPE1003()
        {
        }

        public ObjectType ObjectType { get; set; }

        public string ObjectID { get; set; }
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsStaff { get; set; }
        //protected override string GetEnumString(PropertyInfo prop)
        //{
        //    if (prop.PropertyType == typeof(ObjectType))
        //    {
        //        return Convert.ToChar(prop.GetValue(this, null)).ToString();
        //    }
        //    return base.GetEnumString(prop);
        //}

        //public static List<INFOTYPE1003> GetAll()
        //{
        //    return ServiceManager.GetMirrorOMService("SAP").GetAllApprovalData();
        //}

        //public static List<INFOTYPE1003> GetAll(string Mode, string Profile)
        //{
        //    return ServiceManager.GetMirrorOMService(Mode).GetAllApprovalData(Profile);
        //}

   
    }
}