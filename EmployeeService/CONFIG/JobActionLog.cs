﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.CONFIG
{
   public class JobActionLog
    {
        public int JobID { get; set; }
        public int JobTypeID { get; set; }
        public string LogData { get; set; }
        public DateTime LogDate { get; set; }
        public string LogRemark { get; set; }
    }
}
