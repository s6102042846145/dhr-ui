using System.Collections.Generic;

//namespace ESS.HR
namespace ESS.EMPLOYEE.CONFIG.PA
{
    public class INFOTYPE0001_Comparer : IComparer<INFOTYPE0001>
    {
        public int Compare(INFOTYPE0001 obj1, INFOTYPE0001 obj2)
        {
            return obj1.BeginDate.CompareTo(obj2.BeginDate);
        }
    }
}