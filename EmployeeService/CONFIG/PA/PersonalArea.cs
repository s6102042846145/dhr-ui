﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    public class PersonalArea : AbstractObject
    {
        public string PersonalAreaCode { get; set; }
        public string Description {get; set;}
        public string CountryCode { get; set; }
        public string City { get; set; }
        public string CurrencyCode { get; set; }
    }
}
