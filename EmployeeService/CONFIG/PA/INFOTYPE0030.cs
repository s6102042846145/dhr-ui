using System;
using System.Collections.Generic;
using System.ComponentModel;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0030 : AbstractInfoType
    {
        public override string InfoType
        {
            get { return "0030"; }
        }
        [DefaultValue("00")]
        public string SecondmentType { get; set; }
        public string OraganizationUnit { get; set; }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).GetINFOTYPE0030List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0030> List = new List<INFOTYPE0030>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0030)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0030(EmployeeID1, EmployeeID2, List, Profile);
        }
    }
}