using System;
using System.Collections.Generic;
using System.ComponentModel;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0105 : AbstractInfoType
    {
        public INFOTYPE0105()
        {
            Type oPropertyType = null;
            foreach (PropertyDescriptor oCurrentProperty in TypeDescriptor.GetProperties(this))
            {
                oPropertyType = oCurrentProperty.PropertyType;
                if (oPropertyType.Name.ToLower() == "string")
                {
                    oCurrentProperty.SetValue(this, "");
                }
                else if (oPropertyType.Name.ToLower() == "boolean")
                {
                    oCurrentProperty.SetValue(this, false);
                }
                else if (oPropertyType.Name.IndexOf("int", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    oCurrentProperty.SetValue(this, 0);
                }
                else if (oPropertyType.Name.ToLower() == "decimal")
                {
                    oCurrentProperty.SetValue(this, Convert.ToDecimal(0.00));
                }
                else if (oPropertyType.Name.ToLower() == "double")
                {
                    oCurrentProperty.SetValue(this, 0.0);
                }
                else if (oPropertyType.Name.ToLower() == "datetime")
                {
                    oCurrentProperty.SetValue(this, DateTime.MinValue);
                }
            }
        }

        public string CategoryCode { get; set; }
        public string DataText { get; set; }
        //public string Email
        //{
        //    get;
        //    set;
        //    //get { return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeService.GetPA_Communication(EmployeeID, "9008").DataText; }
        //}

        //public string TelephoneNo
        //{
        //    get;
        //    set;
        //    //get { return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeService.GetPA_Communication(EmployeeID, "9004").DataText; }
        //}

        //public string MobileNo
        //{
        //    get;
        //    set;
        //    //get
        //    //{
        //    //    return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeService.GetPA_Communication(EmployeeID, "9003").DataText;
        //    //}
        //}

        public override string InfoType
        {
            get { return "0105"; }
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0105> List = new List<INFOTYPE0105>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0105)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0105(EmployeeID1, EmployeeID2, List, Profile);
        }
        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).GetINFOTYPE0105List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }
    }
}