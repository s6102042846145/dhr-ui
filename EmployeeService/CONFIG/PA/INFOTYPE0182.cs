using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0182 : AbstractInfoType
    {
        public INFOTYPE0182()
        {
        }

        public string NameType { get; set; }

        public string AlternateName { get; set; }

        public override string InfoType
        {
            get { return "0182"; }
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0182> List = new List<INFOTYPE0182>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0182)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SaveINFOTYPE0182(EmployeeID1, EmployeeID2, List, Profile);
        }

        //public static List<INFOTYPE0001> GetList(string EmployeeID)
        //{
        //    throw new Exception("ttt");
        //    //return ServiceManager.EmployeeService.GetPA_AlternativeNameList(EmployeeID);
        //}

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).GetINFOTYPE0182List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }
    }
}