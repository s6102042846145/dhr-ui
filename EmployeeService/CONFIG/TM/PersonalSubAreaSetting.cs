using System;
using System.Collections.Generic;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class PersonalSubAreaSetting
    {
        public PersonalSubAreaSetting()
        {
        }

        public string PersonalArea { get; set; }

        public string PersonalSubArea { get; set; }
        public string Description { get; set; }
        public string CountryGrouping { get; set; }
        public string AbsAttGrouping { get; set; }
        public string TimeQuotaGrouping { get; set; }
        public string HolidayCalendar { get; set; }
        public string WorkScheduleGrouping { get; set; }
        public string DailyWorkScheduleGrouping { get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("PERSONALSUBAREASETTING_{0}_{1}", PersonalArea, PersonalSubArea);
            return cCode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}