using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using ESS.WORKFLOW;
namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class MonthlyWS : AbstractObject
    {
        #region Constructor

        public MonthlyWS()
        {
        }

        #endregion Constructor

        private Dictionary<int, string> ValuationClassList = new Dictionary<int, string>();

       
        #region " Headers Properties "

        public string EmpSubGroupForWorkSchedule { get; set; }

        public string PublicHolidayCalendar { get; set; }

        public string EmpSubAreaForWorkSchedule { get; set; }

        public string WorkScheduleRule { get; set; }

        public int WS_Year { get; set; }

        public int WS_Month { get; set; }

        public void SetValuationClassExactly(int EndDay, string ValuationClass)
        {
            if (!ValuationClassList.ContainsKey(EndDay))
            {
                ValuationClassList.Add(EndDay, ValuationClass);
            }
            else
            {
                ValuationClassList[EndDay] = ValuationClass;
            }
        }

        public string GetValuationClassExactly(int EndDay)
        {
            foreach (int key in ValuationClassList.Keys)
            {
                if (key >= EndDay)
                {
                    return ValuationClassList[key];
                }
            }
            return ValuationClass;
        }

        public string ValuationClass { get; set; }
        public string GetWSCode(int dayNo, bool isCode)
        {
            PropertyInfo oProp;
            Type oType = this.GetType();
            string cDayNo = dayNo.ToString("00");
            string cReturn = "";
            if (isCode)
            {
                oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));
            }
            else
            {
                oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
            }
            if (oProp != null)
            {
                cReturn = (string)oProp.GetValue(this, null);
            }
            return cReturn;
        }

        #endregion " Headers Properties "

        #region " Properties "

        public string Day01 { get; set; }

        public string Day01_H { get; set; }

        public string Day02 { get; set; }

        public string Day02_H { get; set; }

        public string Day03 { get; set; }

        public string Day03_H { get; set; }

        public string Day04 { get; set; }

        public string Day04_H { get; set; }

        public string Day05 { get; set; }

        public string Day05_H { get; set; }

        public string Day06 { get; set; }

        public string Day06_H { get; set; }

        public string Day07 { get; set; }

        public string Day07_H { get; set; }

        public string Day08 { get; set; }

        public string Day08_H { get; set; }

        public string Day09 { get; set; }

        public string Day09_H { get; set; }

        public string Day10 { get; set; }

        public string Day10_H { get; set; }

        public string Day11 { get; set; }

        public string Day11_H { get; set; }

        public string Day12 { get; set; }

        public string Day12_H { get; set; }

        public string Day13 { get; set; }

        public string Day13_H { get; set; }

        public string Day14 { get; set; }

        public string Day14_H { get; set; }

        public string Day15 { get; set; }

        public string Day15_H { get; set; }

        public string Day16 { get; set; }

        public string Day16_H { get; set; }

        public string Day17 { get; set; }

        public string Day17_H { get; set; }

        public string Day18 { get; set; }

        public string Day18_H { get; set; }

        public string Day19 { get; set; }

        public string Day19_H { get; set; }

        public string Day20 { get; set; }

        public string Day20_H { get; set; }

        public string Day21 { get; set; }

        public string Day21_H { get; set; }

        public string Day22 { get; set; }

        public string Day22_H { get; set; }

        public string Day23 { get; set; }

        public string Day23_H { get; set; }

        public string Day24 { get; set; }

        public string Day24_H { get; set; }

        public string Day25 { get; set; }

        public string Day25_H { get; set; }

        public string Day26 { get; set; }

        public string Day26_H { get; set; }

        public string Day27 { get; set; }

        public string Day27_H { get; set; }

        public string Day28 { get; set; }

        public string Day28_H { get; set; }

        public string Day29 { get; set; }

        public string Day29_H { get; set; }

        public string Day30 { get; set; }

        public string Day30_H { get; set; }
        public string Day31 { get; set; }
        public string Day31_H { get; set; }

        #endregion " Properties "


        public void SaveTo(string CompanyCode, int Year, List<MonthlyWS> list, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveMonthlyWorkscheduleList(Year, list, Profile);
        }

    }
}