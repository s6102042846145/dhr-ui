using System;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class CARDSETTING : AbstractInfoType
    {
        //#region " Members "

        public string EmployeeID_Card { get; set; }
        public DateTime BeginDate_Card { get; set; }
        public DateTime EndDate_Card { get; set; }
        public string CardNo { get; set; }
        public string Location { get; set; }

        //#endregion " Members "

        public override string InfoType
        {
            get { return "0001"; }
        }

    }
}