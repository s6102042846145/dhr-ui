using System;
using System.Collections.Generic;
using System.ComponentModel;
using ESS.UTILITY.EXTENSION;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class Substitution : AbstractObject
    {
        public Substitution()
        {
        }
        public bool IsDraft { get; set; }

        public string SubDWSCodeOld { get; set; }

        public string SubDWSCodeNew { get; set; }

        public string EmpDWSCodeNew { get; set; }

        public string EmpDWSCodeOld { get; set; }

        public bool IsOverride { get; set; }

        public string EmployeeID { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime SubstituteBeginDate { get; set; }

        public DateTime SubstituteEndDate { get; set; }

        public TimeSpan SubstituteBeginTime { get; set; }

        public TimeSpan SubstituteEndTime { get; set; }

        public string DWSGroup { get; set; }

        public string DWSCode { get; set; }

        public string EmpSubGroupSetting { get; set; }

        public string HolidayCalendar { get; set; }
        public string EmpSubAreaSetting { get; set; }

        public string Status { get; set; }
        public string WSRule { get; set; }

        public string Substitute { get; set; }
        public string RequestNo { get; set; }

        public string DWSCodeView
        {
            get
            {
                if (DWSCode == "" && WSRule != "")
                {
                    MonthlyWS temp = EMPLOYEE.ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(EmpSubGroupSetting, EmpSubAreaSetting, HolidayCalendar, WSRule, BeginDate.Year, BeginDate.Month);
                    if (temp != null)
                    {
                        return temp.GetWSCode(BeginDate.Day, true);
                    }
                    else
                    {
                        return DWSCode;
                    }
                }
                else
                {
                    return DWSCode;
                }
            }
        }

    }
}