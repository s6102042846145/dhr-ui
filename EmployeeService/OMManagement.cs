using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.SHAREDATASERVICE;

namespace ESS.EMPLOYEE
{
    public class OMManagement
    {
        #region Constructor

        private OMManagement()
        {
        }

        #endregion Constructor

        #region MultiCompany  Framework

        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, OMManagement> Cache = new Dictionary<string, OMManagement>();

        private static string ModuleID = "ESS.EMPLOYEE";
        public string CompanyCode { get; set; }

        public static OMManagement CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new OMManagement()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            OMManagement oOMManagement = new OMManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oOMManagement;
        }

        #endregion MultiCompany  Framework

        public string ROOT_ORGANIZATION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ROOT_ORGANIZATION");
            }
        }

        private string SAP_VERSION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.EMPLOYEE.SAP", "SAP_VERSION");
            }
        }

        /// <summary>
        /// Get object infotype1000 from SAP
        /// </summary>
        /// <param name="lstObjectType"></param>
        /// <param name="dtCheckDate"></param>
        /// <param name="SAP_VERSION"></param>
        /// <returns></returns>
        public List<INFOTYPE1000> GetAllObject(List<ObjectType> lstObjectType, DateTime dtCheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllObject(lstObjectType, dtCheckDate);
        }

        public List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllOrganizationWithLevel(Orgunit, LanguageCode);
        }

        //Add By Kiattiwat K. (10-02-2012)
        public INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetOrgParent(OrgID, CheckDate);
        }

        public INFOTYPE1010 GetLevelByOrg(string OrgID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetLevelByOrg(OrgID, CheckDate);
        }
        public INFOTYPE1000 GetObjectData(ObjectType ObjectType, string ObjectID, DateTime CheckDate, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType, ObjectID, CheckDate, Language);
        }

        public INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetJobByPositionID(PositionID, CheckDate);
        }

        public INFOTYPE1000 GetOrganizationByPositionID(string PositionID, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetOrganizationByPositionID(PositionID, Language);
        }

        public List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetEmployeeUnder(OrgID, CheckDate);
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetEmployeeByPositionID(PositionID, CheckDate);
        }
        public List<INFOTYPE1003> GetAll(string objID1, string objID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(Mode).GetAllApprovalData(objID1, objID2, Profile);
        }

        public void SaveAll(List<INFOTYPE1003> data, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(Mode).SaveAllApprovalData(data, Profile);
        }        
        
        public List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllPosition(EmployeeID, CheckDate, LanguageCode);
        }

        public List<INFOTYPE1000> GetAllPositionForHrMaster(DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllPositionForHrMaster(CheckDate,LanguageCode);
        }

        public List<INFOTYPE1000> GetAllPositionNoCheckDate(string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllPositionNoCheckDate(LanguageCode);
        }

        public List<INFOTYPE1000> GetPositionByOrganize(string Organize, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionByOrganize(Organize, CheckDate, LanguageCode);
        }

        public List<INFOTYPE1000> GetPositionByOrganizeForHRMaster(string Organize, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionByOrganizeForHRMaster(Organize, CheckDate, LanguageCode);
        }

        public INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.FindNextPosition(CurrentPositionID, CheckDate, LanguageCode);
        }

        public List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetOrgUnder(OrgID, CheckDate, LanguageCode);
        }

        public INFOTYPE1000 GetOrgRoot(string OrgID, int Level, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetOrgRoot(OrgID, Level, CheckDate, LanguageCode);
        }

        public INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetOrgParent(OrgID, CheckDate);
        }

        public INFOTYPE1000 FindDivision(INFOTYPE1000 orgUnit, string LanguageCode)
        {
            if (orgUnit.Level <= int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DIVISIONLEVEL")))
            {
                return orgUnit;
            }
            return GetOrgRoot(orgUnit.ObjectID, int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DIVISIONLEVEL")), orgUnit.EndDate, LanguageCode);
        }

        public INFOTYPE1000 FindDepartment(INFOTYPE1000 orgUnit, string LanguageCode)
        {
            if (orgUnit.Level <= int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEPARTMENTLEVEL")))
            {
                return orgUnit;
            }
            return GetOrgRoot(orgUnit.ObjectID, int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEPARTMENTLEVEL")), orgUnit.EndDate, LanguageCode);
        }

        public INFOTYPE1000 FindVC(INFOTYPE1000 orgUnit, string LanguageCode)
        {
            if (orgUnit.Level <=int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VCLEVEL")) )
            {
                return orgUnit;
            }
            return GetOrgRoot(orgUnit.ObjectID,int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VCLEVEL")) , orgUnit.EndDate, LanguageCode);
        }

        public INFOTYPE1000 FinOrgUnit(INFOTYPE1000 item, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetOrgUnit(item.ObjectID, item.EndDate, LanguageCode);
        }


        public List<ObjectType> GetAllObjectType()
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllObjectType();
        }

        public List<ObjectTypeIDRange> GetAllObjectTypeIDRange()
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllObjectTypeIDRange();
        }

        public List<string> GetAllRelationType()
        {
           return ServiceManager.CreateInstance(CompanyCode).OMService.GetAllRelationType();
        }
        public List<INFOTYPE1000> GetAllObject(string SourceMode, string SourceProfile)
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();
            List<ObjectTypeIDRange> lstRange = this.GetAllObjectTypeIDRange();
            foreach (ObjectTypeIDRange oRange in lstRange)
            {
                List<ObjectType> lstObjectType = new List<ObjectType>();
                ObjectType ot = (ObjectType)Enum.Parse(typeof(ObjectType), oRange.ObjectTypeID);
                lstObjectType.Add(ot);
                if(SAP_VERSION == "6")
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(SourceMode).GetAllObject(oRange.ObjectID1, oRange.ObjectID2, lstObjectType, SourceProfile));
                else
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMServiceHana(SourceMode).GetAllObject(oRange.ObjectID1, oRange.ObjectID2, lstObjectType, SourceProfile));
            }
            return oReturn;
        }

        public List<INFOTYPE1513> GetAllJobIndex(string ObjectID1, string ObjectID2, string SourceMode, string SourceProfile)
        {
            List<INFOTYPE1513> oReturn = new List<INFOTYPE1513>();
            List<ObjectTypeIDRange> lstRange = ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService("DB").GetObjectIDRange(ObjectType.S);
            foreach (ObjectTypeIDRange item in lstRange)
            {
                if (SAP_VERSION == "6")
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(SourceMode).GetAllJobIndex(item.ObjectID1, item.ObjectID2, SourceProfile));
                else
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMServiceHana(SourceMode).GetAllJobIndex(item.ObjectID1, item.ObjectID2, SourceProfile));
            }
            return oReturn;
        }

        public void SaveAllObject(List<INFOTYPE1000> Data, string TargetMode, string TargetProfile)
        {
            List<ObjectType> AllObjectType = this.GetAllObjectType();
            ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(TargetMode).SaveAllObject(AllObjectType, Data, TargetProfile);
        }

        public List<INFOTYPE1001> GetAllRelation(string ObjectID1, string ObjectID2, string SourceMode, string SourceProfile)
        {
            List<ObjectType> AllObjectType = this.GetAllObjectType();
            List<string> AllRelationCode = this.GetAllRelationType();
            if(SAP_VERSION == "6")
                return ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(SourceMode).GetAllRelation(ObjectID1, ObjectID2, AllObjectType, AllRelationCode, SourceProfile);
            else
                return ServiceManager.CreateInstance(CompanyCode).GetMirrorOMServiceHana(SourceMode).GetAllRelation(ObjectID1, ObjectID2, AllObjectType, AllRelationCode, SourceProfile);
        }

        public void SaveAllRelation(List<INFOTYPE1001> Data, string TargetMode, string TargetProfile)
        {
            List<ObjectType> AllObjectType = this.GetAllObjectType();
            List<string> AllRelationCode = this.GetAllRelationType();
            ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(TargetMode).SaveAllRelation(AllObjectType, AllRelationCode, Data, TargetProfile);
        }

        public List<INFOTYPE1003> GetAllApprovalData(string ObjectID1, string ObjectID2, string SourceMode, string SourceProfile)
        {
            List<INFOTYPE1003> oReturn = new List<INFOTYPE1003>();
            List<ObjectTypeIDRange> lstRange = ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService("DB").GetObjectIDRange(ObjectType.S);
            foreach (ObjectTypeIDRange item in lstRange)
            {
                if (SAP_VERSION == "6")
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(SourceMode).GetAllApprovalData(item.ObjectID1, item.ObjectID2, SourceProfile));
                else
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMServiceHana(SourceMode).GetAllApprovalData(item.ObjectID1, item.ObjectID2, SourceProfile));
            }
            return oReturn;
        }

        public void SaveAllApprovalData(List<INFOTYPE1003> Data, string TargetMode, string TargetProfile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(TargetMode).SaveAllApprovalData(Data, TargetProfile);
        }

        public List<INFOTYPE1010> GetAllOrgUnitLevel(string ObjectID1, string ObjectID2, string Mode, string Profile)
        {
            List<INFOTYPE1010> oReturn = new List<INFOTYPE1010>();
            List<ObjectTypeIDRange> lstRange = ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService("DB").GetObjectIDRange(ObjectType.O);
            foreach (ObjectTypeIDRange item in lstRange)
            {
                if (SAP_VERSION == "6")
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(Mode).GetAllOrgUnitLevel(item.ObjectID1, item.ObjectID2, Profile));
                else
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMServiceHana(Mode).GetAllOrgUnitLevel(item.ObjectID1, item.ObjectID2, Profile));
            }
            return oReturn;
        }
        
        public void SaveAllOrgUnitLevel(List<INFOTYPE1010> Data, string TargetMode, string TargetProfile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(TargetMode).SaveOrgUnitLevel(Data, TargetProfile);
        }

        public List<INFOTYPE1013> GetAllPositionBandLevel(string ObjID1, string ObjID2, string Mode, string Profile)
        {
            List<INFOTYPE1013> oReturn = new List<INFOTYPE1013>();
            List<ObjectTypeIDRange> lstRange = ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService("DB").GetObjectIDRange(ObjectType.S);
            foreach (ObjectTypeIDRange item in lstRange)
            {
                if (SAP_VERSION == "6")
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(Mode).GetAllPositionBandLevel(item.ObjectID1, item.ObjectID2, Profile));
                else
                    oReturn.AddRange(ServiceManager.CreateInstance(CompanyCode).GetMirrorOMServiceHana(Mode).GetAllPositionBandLevel(item.ObjectID1, item.ObjectID2, Profile));
            }
            return oReturn;
        }
        public void SaveAllPositionBandLevel(List<INFOTYPE1013> Data, string TargetMode, string TargetProfile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(TargetMode).SaveAllPositionBandLevel(Data, TargetProfile);
        }
        public void SaveAllJobIndex(List<INFOTYPE1513> Data, string TargetMode, string TargetProfile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorOMService(TargetMode).SaveAllJobIndex(Data, TargetProfile);
        }

        public void GenUserRoleResponseSnapshot()
        {
            ServiceManager oServiceMan = ServiceManager.CreateInstance(CompanyCode);

            // Copy UserRole and UserRoleResponse to CompanyDB
            ShareDataManagement.CopyUserRoleAndResponse(CompanyCode);

            // Call Create Snapshot
            oServiceMan.OMService.CreateSnapshot();

            // Calculate UserRoleResponseSnapshot
           List<UserRoleResponse> lstResponse = oServiceMan.OMService.GetUserRoleResponse();
            foreach (UserRoleResponse oResponse in lstResponse)
            {
                List<UserRoleResponseSnapShot> lstSnapshot = new List<UserRoleResponseSnapShot>();
                if (oResponse.ResponseType == "O")
                {
                    // Find Sub Org and assign to oResponse.EmployeeID
                    List<INFOTYPE1000> lstOrg = ServiceManager.CreateInstance(oResponse.ResponseCompanyCode).OMService.GetOrgUnit(oResponse.ResponseCode, oResponse.includeSub);
                    foreach (INFOTYPE1000 oOrg in lstOrg)
                    {
                        UserRoleResponseSnapShot oSnapshot = new UserRoleResponseSnapShot(oResponse, oOrg);
                        lstSnapshot.Add(oSnapshot);
                    }
                }
                else
                {
                    // Copy UserRoleResponse to UserRoleResponseSnapShot
                    UserRoleResponseSnapShot oSnapshot = new UserRoleResponseSnapShot(oResponse);
                    lstSnapshot.Add(oSnapshot);
                }
                oServiceMan.OMService.SaveUserRoleResponseSnapshot(lstSnapshot);
            }

            // Call Create Snapshot
            oServiceMan.OMService.CreateRequestFlowReceipientSnapshot();
        }

        public void MapEmpSubGroup()
        {
           ServiceManager.CreateInstance(CompanyCode).OMService.MapEmpSubGroup();
        }

        #region Job OM

        public string JobALL_DHR_TO_INFOTYPE1000(string DataSource)
        {
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataOMService.JobALL_DHR_TO_INFOTYPE1000();
        }

        public string JobALL_DHR_TO_INFOTYPE1001(string DataSource)
        {
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataOMService.JobALL_DHR_TO_INFOTYPE1001();
        }

        public string JobALL_DHR_TO_INFOTYPE1010(string DataSource)
        {
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataOMService.JobALL_DHR_TO_INFOTYPE1010();
        }

        public string JobALL_DHR_TO_INFOTYPE1013(string DataSource)
        {
            return ServiceManager.CreateInstance(CompanyCode, DataSource).DataOMService.JobALL_DHR_TO_INFOTYPE1013();
        }
        #endregion Job OM
    }
}