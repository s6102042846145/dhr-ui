﻿using ESS.EMPLOYEE.CONFIG.WF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE
{
    public class GenerateFlowKeyService
    {
        public GenerateFlowKeyService()
        {
        }

        private string CompanyCode { get; set; }

        public static GenerateFlowKeyService SVC(string oCompanyCode)
        {
            GenerateFlowKeyService oGenerateFlowKeyService = new GenerateFlowKeyService()
            {
                CompanyCode = oCompanyCode
            };
            return oGenerateFlowKeyService;
        }
        public string GetFlowKey(EmployeeData Requestor, EmployeeData Creator, int RequestTypeID, int RequestTypeVersionID)
        {
            string ReturnKey = "EMPLOYEE_CREATE"; //Default
            bool IsActionInsteadOf = Creator.EmployeeID != Requestor.EmployeeID ? true : false;
            List<RequestTypeKeyCodeSetting> oFlowKey = EmployeeManagement.CreateInstance(CompanyCode).GetRequestFlowSettingByCriteria(RequestTypeID, RequestTypeVersionID);
            try
            {
                if (oFlowKey.Count == 0)
                {
                    return ReturnKey;
                }
                else
                {
                    foreach (RequestTypeKeyCodeSetting item in oFlowKey)
                    {

                        if (item.Type.ToUpper() == "USERROLE")
                        {
                            string[] DBroles = item.Value.Split(',');
                            string[] Creatorrole = Creator.UserRoles.Split(',');
                            var isHoldingRole = from role in Creatorrole
                                                where DBroles.Contains(role)
                                                select 1;

                            if (isHoldingRole.ToArray().Length > 0)
                            {
                                if (!item.IsActionInsteadOf.HasValue || item.IsActionInsteadOf == IsActionInsteadOf || item.IsActionInsteadOf == null)
                                {
                                    ReturnKey = item.KeyCode;
                                    break;
                                }
                            }
                        }
                        else if (item.Type.ToUpper() == "BAND")
                        {
                            string[] DBband = item.Value.Split(',');
                            int Creatorband = Creator.BrandNo;
                            bool isHoldingBand = DBband.Contains(Creatorband.ToString());

                            if (isHoldingBand)
                            {
                                if (!item.IsActionInsteadOf.HasValue || item.IsActionInsteadOf == IsActionInsteadOf || item.IsActionInsteadOf == null)
                                {
                                    ReturnKey = item.KeyCode;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {

                //  ReturnKey = string.Empty;
            }


            return ReturnKey;
        }
    }
}
