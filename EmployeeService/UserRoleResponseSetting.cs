using System;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class UserRoleResponseSetting : AbstractObject
    {
        private string __userrole = "";
        private bool __includeSub = false;

        public UserRoleResponseSetting()
        {
        }

        public UserRoleResponseSetting(string role)
            : this(role, false)
        {
        }

        public UserRoleResponseSetting(string role, bool includeSub)
        {
            __userrole = role;
            __includeSub = includeSub;
        }

        public string UserRole
        {
            get
            {
                return __userrole;
            }
            set
            {
                __userrole = value;
            }
        }

        public bool IncludeSub
        {
            get
            {
                return __includeSub;
            }
            set
            {
                __includeSub = value;
            }
        }
    }
}