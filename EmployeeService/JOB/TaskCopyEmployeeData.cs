using ESS.EMPLOYEE.CONFIG.PA;
using ESS.JOB.ABSTRACT;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;

namespace ESS.EMPLOYEE.JOB
{
    public class TaskCopyEmployeeData : AbstractTaskWorker
    {
        private string __infoType = "";
        private string __empID1 = "00000000";
        private string __empID2 = "99999999";
        private string __sourceMode = "";
        private string __sourceProfile = "";
        private string __targetMode = "";
        private string __targetProfile = "";

        public TaskCopyEmployeeData()
        {
        }

        public string InfoType { get; set; }
        public string EmployeeID1 { get; set; }
        public string EmployeeID2 { get; set; }
        public string SourceMode { get; set; }

        public string SourceProfile { get; set; }

        public string TargetMode { get; set; }

        public string TargetProfile { get; set; }

        public override void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            string oWorkAction = string.Format("Try to copy data {0} from {1} to {3}", InfoType, SourceMode, SourceProfile, TargetMode, TargetProfile);
            Console.WriteLine(oWorkAction);
            if (InfoType == "INFOTYPE0001")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                if (SourceMode.ToUpper() == "DHR")
                {
                    string result = oMan.JobALL_DHR_TO_INFOTYPE0001(EmployeeID1, EmployeeID2, SourceMode.ToUpper());
                    Console.WriteLine("Save {0} ", result);
                    if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                }
                else
                {
                    #region not in use
                    //try
                    //{
                    //    List<INFOTYPE0001> lstInf = oMan.GetINFOTYPE0001List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                    //    Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                    //    oMan.SaveINFOTYPE0001To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                    //    Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
                    //}
                    //catch (Exception ex)
                    //{
                    //    EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                    //}
                    #endregion
                }
            }
            else if (InfoType == "INFOTYPE0002")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                if (SourceMode.ToUpper() == "DHR")
                {
                    string result = oMan.JobALL_DHR_TO_INFOTYPE0002(EmployeeID1, EmployeeID2, SourceMode.ToUpper());
                    Console.WriteLine("Save {0} ", result);
                    if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                }
                else
                {
                    #region Not in use
                    //try
                    //{
                    //    List<INFOTYPE0002> lstInf = oMan.GetINFOTYPE0002List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                    //    Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                    //    oMan.SaveINFOTYPE0002To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                    //    Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
                    //    //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {lstInf.Count} record(s)"), InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);//Comment by Koissares 20201126
                    //}
                    //catch (Exception ex)
                    //{
                    //    EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                    //}
                    #endregion
                }

            }
            else if (InfoType == "INFOTYPE0007")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                if (SourceMode.ToUpper() == "DHR")
                {
                    string result = oMan.JobALL_DHR_TO_INFOTYPE0007(EmployeeID1, EmployeeID2, SourceMode.ToUpper());
                    Console.WriteLine("Save {0} ", result);
                    if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                }
                else
                {
                    #region not in use
                    //try
                    //{
                    //    List<INFOTYPE0007> lstInf = oMan.GetINFOTYPE0007List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                    //    Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                    //    oMan.SaveINFOTYPE0007To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                    //    Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
                    //    //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {lstInf.Count} record(s)"), InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);//Comment by Koissares 20201126
                    //}
                    //catch (Exception ex)
                    //{
                    //    EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                    //}
                    #endregion
                }

            }
            else if (InfoType == "INFOTYPE0105")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                if (SourceMode.ToUpper() == "DHR")
                {
                    string result = oMan.JobALL_DHR_TO_INFOTYPE0105(EmployeeID1, EmployeeID2, SourceMode.ToUpper());
                    Console.WriteLine("Save {0} ", result);
                    if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                }
                else
                {
                    #region Not in use
//try
//                    {
//                        List<INFOTYPE0105> lstInf = oMan.GetINFOTYPE0105List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
//                        Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
//                        oMan.SaveINFOTYPE0105To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
//                        Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
//                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {lstInf.Count} record(s)"), InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);//Comment by Koissares 20201126
//                    }
//                    catch (Exception ex)
//                    {
//                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
//                    }
                    #endregion
                }
            }
            else if (InfoType == "INFOTYPE0105_MAPPING")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                if (SourceMode.ToUpper() == "DHR")
                {
                    string result = oMan.JobALL_DHR_TO_INFOTYPE0105_Mapping(SourceMode.ToUpper(), out string oMessage);
                    Console.WriteLine("Save {0} ", result);
                    if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, InfoType);
                }
                else
                {
                    #region Not in use

                    #endregion
                }
            }
            else if (InfoType == "INFOTYPE0182")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                if (SourceMode.ToUpper() == "DHR")
                {
                    string result = oMan.JobALL_DHR_TO_INFOTYPE0182(EmployeeID1, EmployeeID2, SourceMode.ToUpper());
                    Console.WriteLine("Save {0} ", result);
                    if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
                }
                else
                {
                    #region Not in use
//try
//                    {
//                        List<INFOTYPE0182> lstInf = oMan.GetINFOTYPE0182List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
//                        Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
//                        oMan.SaveINFOTYPE0182To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
//                        Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
//                        //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {lstInf.Count} record(s)"), InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);//Comment by Koissares 20201126
//                    }
//                    catch (Exception ex)
//                    {
//                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, InfoType + "|EmployeeID1:" + EmployeeID1 + "|EmployeeID2:" + EmployeeID2);
//                    }
                    #endregion
                }
            }
            else if (InfoType == "DATESPECIFIC")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                if (SourceMode.ToUpper() == "DHR")
                {
                    string result = oMan.JobALL_DHR_TO_DateSpecificData(SourceMode.ToUpper());
                    Console.WriteLine("Save {0} ", result);
                    if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, InfoType);
                }
                else
                {
                    EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "No record To Save", InfoType);
                }
            }
        }
    }
}