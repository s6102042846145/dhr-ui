using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.JOB.ABSTRACT;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class TaskCopyEmployeeConfig : AbstractTaskWorker
    {
        public TaskCopyEmployeeConfig()
        {
        }

        public string ConfigName { get; set; }


        public string SourceMode { get; set; }


        public string SourceProfile { get; set; }

        public string TargetMode { get; set; }

        public string TargetProfile { get; set; }


        public string Param1 { get; set; }


        public string Param2 { get; set; }


        public override void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            string oWorkAction = string.Format("Try to copy employee config '{0}' from {1} to {3}", ConfigName, SourceMode, SourceProfile, TargetMode, TargetProfile);
            Console.WriteLine(oWorkAction);
            switch (ConfigName.ToUpper())
            {
                case "PERSONALSUBAREASETTING":
                    if (SourceMode.ToUpper() == "DHR")
                    {
                        string result = EmployeeManagement.CreateInstance(CompanyCode).JobAll_DHR_TO_PersonalSubAreaSetting(TargetMode.ToUpper());
                        Console.WriteLine("Save {0} ", result);
                        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    }
                    else
                    {
                        try
                        {
                            List<PersonalSubAreaSetting> personalSubAreaSettingList = EmployeeManagement.CreateInstance(CompanyCode).GetPersonalSubAreaSettingList(SourceMode, SourceProfile);
                            EmployeeManagement.CreateInstance(CompanyCode).SaveTo(personalSubAreaSettingList, TargetMode, TargetProfile);
                            Console.WriteLine("Copy {0} record(s) complete", personalSubAreaSettingList.Count);
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {personalSubAreaSettingList.Count} record(s)"), ConfigName);//Comment by Koissares 20201126
                        }
                        catch (Exception ex)
                        {
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                        }
                    }
                    break;

                case "PERSONALSUBGROUPSETTING":
                    if (SourceMode.ToUpper() == "DHR")
                    {
                        string result = EmployeeManagement.CreateInstance(CompanyCode).JobALL_DHR_TO_PersonalSubGroupSetting(TargetMode.ToUpper());
                        Console.WriteLine("Save {0} ", result);
                        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    }
                    else
                    {
                        try
                        {
                            List<PersonalSubGroupSetting> personalSubGroupSettingList = EmployeeManagement.CreateInstance(CompanyCode).GetPersonalSubGroupSettingList(SourceMode, SourceProfile);
                            EmployeeManagement.CreateInstance(CompanyCode).SaveTo(personalSubGroupSettingList, TargetMode, TargetProfile);
                            Console.WriteLine("Copy {0} record(s) complete", personalSubGroupSettingList.Count);
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {personalSubGroupSettingList.Count} record(s)"), ConfigName);//Comment by Koissares 20201126
                        }
                        catch (Exception ex)
                        {
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                        }
                    }
                   
                    break;

                case "MONTHLYWS":
                    if (SourceMode.ToUpper() == "DHR")
                    {
                        string result = EmployeeManagement.CreateInstance(CompanyCode).JobALL_DHR_TO_MonthlyWorkschedule(MonthlyWSYear, TargetMode.ToUpper());
                        Console.WriteLine("Save {0} ", result);
                        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    }
                    else
                    {
                        try
                        {
                            List<MonthlyWS> monthlyWSList = EmployeeManagement.CreateInstance(CompanyCode).GetAll(MonthlyWSYear, SourceMode, SourceProfile);
                            EmployeeManagement.CreateInstance(CompanyCode).SaveTo(MonthlyWSYear, monthlyWSList, TargetMode, TargetProfile);
                            Console.WriteLine("Copy {0} record(s) complete", monthlyWSList.Count);
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {monthlyWSList.Count} record(s)"), ConfigName);//Comment by Koissares 20201126
                        }
                        catch (Exception ex)
                        {
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                        }
                    }

                    break;

                case "DAILYWS":
                    if (SourceMode.ToUpper() == "DHR")
                    {
                        string result = EmployeeManagement.CreateInstance(CompanyCode).JobALL_DHR_TO_DailyWorkschedule(TargetMode.ToUpper());
                        Console.WriteLine("Save {0} ", result);
                        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    }
                    else
                    {
                        try
                        {
                            List<DailyWS> dailyWSList = EmployeeManagement.CreateInstance(CompanyCode).GetAll(SourceMode, SourceProfile);
                            EmployeeManagement.CreateInstance(CompanyCode).SaveTo(dailyWSList, TargetMode, TargetProfile);
                            Console.WriteLine("Copy {0} record(s) complete", dailyWSList.Count);
                            //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {dailyWSList.Count} record(s)"), ConfigName);//Comment by Koissares 20201126
                        }
                        catch (Exception ex)
                        {
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                        }
                    }

                    break;
            }
        }

        private int MonthlyWSYear
        {
            get
            {
                int nReturn;
                if (!int.TryParse(Param1, out nReturn))
                {
                    nReturn = DateTime.Now.Year;
                }
                return nReturn;
            }
        }
    }
}