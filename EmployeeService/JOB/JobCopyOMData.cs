using System.Collections.Generic;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class JobCopyOMData : AbstractJobWorker
    {
        public JobCopyOMData()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyOMData task;

            string SAP_VERSION = ShareDataManagement.LookupCache(CompanyCode, "ESS.EMPLOYEE.SAP", "SAP_VERSION");

            task = new TaskCopyOMData();
            task.ConfigName = "OBJECT";
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_OM_GetAllObject_INFOTYPE1000_ObjId1_ObjId2_ObjectTypes_Profile;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            returnValue.Add(task);

            task = new TaskCopyOMData();
            task.ConfigName = "RELATION";
            task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_OM_GetAllRelation_INFOTYPE1001_ObjId1_ObjId2_ObjectTypes_RelationCodes_Profile;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            returnValue.Add(task);

            #region other task not in use
            //task = new TaskCopyOMData();
            //task.ConfigName = "APPROVALDATA";
            //task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            //task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            //task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            //task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            //task.ObjectID1 = Params[0].Value;
            //task.ObjectID2 = Params[1].Value;
            //returnValue.Add(task);

            //task = new TaskCopyOMData();
            //task.ConfigName = "POSITIONBANDLEVEL";
            //task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_OM_GetAllPositionBandLevel_INFOTYPE1013_ObjId1_ObjId2_Profile;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            //task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            //task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            //task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            //task.ObjectID1 = Params[0].Value;
            //task.ObjectID2 = Params[1].Value;
            //returnValue.Add(task);

            //task = new TaskCopyOMData();
            //task.ConfigName = "ORGUNITLEVEL";
            //task.SourceMode = EmployeeServices.SVC(CompanyCode).EMPLOYEE_OM_GetAllOrgUnitLevel_INFOTYPE1010_ObjId1_ObjId2_Profile;//EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            //task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            //task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            //task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            //task.ObjectID1 = Params[0].Value;
            //task.ObjectID2 = Params[1].Value;
            //returnValue.Add(task);

            //task = new TaskCopyOMData();
            //task.ConfigName = "MAPEMPSUBGROUP";
            //task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            //task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            //task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            //task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            //task.ObjectID1 = Params[0].Value;
            //task.ObjectID2 = Params[1].Value;
            //returnValue.Add(task);
            #endregion
            return returnValue;
        }
    }
}