using System;
using ESS.JOB.ABSTRACT;
using ESS.WORKFLOW;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.SHAREDATASERVICE;

namespace ESS.EMPLOYEE.JOB
{
    public class TaskCopyOMData : AbstractTaskWorker
    {

        private string __objID1 = "00000000";
        private string __objID2 = "99999999";

        public TaskCopyOMData()
        {
        }

        public string ConfigName { get; set; }

        public string SourceMode { get; set; }


        public string SourceProfile { get; set; }

        public string TargetMode { get; set; }

        public string TargetProfile { get; set; }


        public string ObjectID1 { get; set; }


        public string ObjectID2 { get; set; }

        public string SAP_VERSION { get; set; }

        public override void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            string oWorkAction = string.Format("Try to copy OM data '{0}' from {1} to {3}", ConfigName, SourceMode, SourceProfile, TargetMode, TargetProfile);
            Console.WriteLine(oWorkAction);
            OMManagement oMan = OMManagement.CreateInstance(CompanyCode);
            switch (ConfigName.ToUpper())
            {
                case "OBJECT":
                    if (SourceMode.ToUpper() == "DHR")
                    {
                        string result = oMan.JobALL_DHR_TO_INFOTYPE1000(SourceMode.ToUpper());
                        Console.WriteLine("Save {0} ", result);
                        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    }
                    else
                    {
                        #region Not in use
                        //try
                        //{
                        //    List<INFOTYPE1000> lstInf = oMan.GetAllObject(SourceMode, SourceProfile);
                        //    Console.WriteLine("Load {0} record(s) completed ", lstInf.Count);
                        //    oMan.SaveAllObject(lstInf, TargetMode, TargetProfile);
                        //    Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
                        //    //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {lstInf.Count} record(s)"),ConfigName);//Comment by Koissares 20201126
                        //}
                        //catch (Exception ex)
                        //{
                        //    EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                        //}
                        #endregion
                    }

                    break;

                case "RELATION":
                    if (SourceMode.ToUpper() == "DHR")
                    {
                        string result = oMan.JobALL_DHR_TO_INFOTYPE1001(SourceMode.ToUpper());
                        Console.WriteLine("Save {0} ", result);
                        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    }
                    else
                    {
                        #region Not in use
                        //try
                        //{
                        //    List<INFOTYPE1001> lstInf1001 = oMan.GetAllRelation(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                        //    Console.WriteLine("Load {0} record(s) completed", lstInf1001.Count);
                        //    oMan.SaveAllRelation(lstInf1001, TargetMode, TargetProfile);
                        //    Console.WriteLine("Save {0} record(s) completed.", lstInf1001.Count);
                        //    //EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, string.Format($"Copy {lstInf1001.Count} record(s)"), ConfigName);//Comment by Koissares 20201126
                        //}
                        //catch (Exception ex)
                        //{
                        //    EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                        //}
                        #endregion
                    }

                    break;

                    #region other Case not in use
                    //case "USERROLERESPONSESNAPSHOT":
                    //    try
                    //    {
                    //        Console.WriteLine("Gen UserRoleResponseSnapshot Start");
                    //        oMan.GenUserRoleResponseSnapshot();
                    //        Console.WriteLine("Gen UserRoleResponseSnapshot Completed");
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                    //    }
                    //    break;                    

                    //case "ORGUNITLEVEL":
                    //    if (SourceMode.ToUpper() == "DHR")
                    //    {
                    //        string result = oMan.JobALL_DHR_TO_INFOTYPE1010(SourceMode.ToUpper());
                    //        Console.WriteLine("Save {0} ", result);
                    //        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                    //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    //    }
                    //    else
                    //    {
                    //        try
                    //        {
                    //            List<INFOTYPE1010> lstInf1010 = OMManagement.CreateInstance(CompanyCode).GetAllOrgUnitLevel(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                    //            Console.WriteLine("Load {0} record(s) completed", lstInf1010.Count);
                    //            oMan.SaveAllOrgUnitLevel(lstInf1010, TargetMode, TargetProfile);
                    //            Console.WriteLine("Save {0} record(s) completed.", lstInf1010.Count);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                    //        }
                    //    }

                    //    break;

                    //case "POSITIONBANDLEVEL":
                    //    if (SourceMode.ToUpper() == "DHR")
                    //    {
                    //        string result = oMan.JobALL_DHR_TO_INFOTYPE1013(SourceMode.ToUpper());
                    //        Console.WriteLine("Save {0} ", result);
                    //        if (result.ToUpper().StartsWith("ERROR"))//Edit by Koissares 20201126
                    //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, result, ConfigName);
                    //    }
                    //    else
                    //    {
                    //        try
                    //        {
                    //            List<INFOTYPE1013> lstInf1013 = oMan.GetAllPositionBandLevel(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                    //            Console.WriteLine("Load {0} record(s) completed", lstInf1013.Count);
                    //            oMan.SaveAllPositionBandLevel(lstInf1013, TargetMode, TargetProfile);
                    //            Console.WriteLine("Save {0} record(s) completed.", lstInf1013.Count);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                    //        }
                    //    }

                    //    break;
                    //case "MAPEMPSUBGROUP":
                    //    try
                    //    {
                    //        oMan.MapEmpSubGroup();
                    //        Console.WriteLine("Map EmpSubGroup completed.");
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        EmployeeManagement.CreateInstance(CompanyCode).InsertJobActionLog(TaskID, TaskID, "Error:" + ex.Message, ConfigName);
                    //    }
                    //    break;
                    #endregion
            }
        }
    }
}