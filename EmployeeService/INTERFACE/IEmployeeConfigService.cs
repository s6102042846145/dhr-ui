using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.WF;
using System.Data;
using ESS.EMPLOYEE.CONFIG;

namespace ESS.EMPLOYEE.INTERFACE
{
    public interface IEmployeeConfigService
    {
        PersonalArea GetPersonalAreaSetting(string PersonalArea);

        #region PersonalSubAreaSetting

        PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea);
        List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile);
        List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea);
        void SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> Data, string Profile);

        List<string> GetPersonalSubAreaWSRMapping(string strSubArea);

        #endregion PersonalSubAreaSetting

        #region PersonalSubGroupSetting

        PersonalSubGroupSetting GetPersonalSubGroupSetting(string EmpGroup, string EmpSubGroup);
        List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string SourceProfile);
        void SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> Data, string TargetProfile);

        #endregion PersonalSubGroupSetting


        #region DailyWS

        DailyWS GetDailyWorkschedule(string EmployeeID, DateTime CheckDate);
        DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate);
        List<DailyWS> GetDailyWorkscheduleList(string Profile);
        List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate);
        void SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile);

        #endregion DailyWS

        #region MonthlyWS

        MonthlyWS GetPublicCalendar(string CompanyCode, int Year, int Month);
        MonthlyWS GetCalendar(string EmployeeID, int Year, int Month);
        MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month);
        MonthlyWS GetSimulateCalendar(string EmployeeID, int Year, int Month, string WFRule);
        List<MonthlyWS> GetMonthlyWorkscheduleList(int Year, string SourceProfile);
        List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar);
        List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, bool IncludeMonth, bool IncludeYear);
        List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption);
        void SaveMonthlyWorkscheduleList(int Year, List<MonthlyWS> Data, string Profile);

        DataTable GetMonthlyWorkscheduleGetByPeriod(string EmployeeID, int iYear, int iMonth);

        #endregion MonthlyWS

        #region Substitution

        List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective);
        
        #endregion Substitution 


        #region INFOTYPE1000

        List<INFOTYPE1000> GetGenderCheckBoxData();
        List<INFOTYPE1000> GetPrefixDropdownData(); 
        List<INFOTYPE1000> GetMaritalStatusDropdownData();
        List<INFOTYPE1000> GetNationalityDropdownData();
        List<INFOTYPE1000> GetLanguageDropdownData();
        List<INFOTYPE1000> GetReligionDropdownData();
        List<INFOTYPE1000> GetEmpGroupDropdownData();
        List<INFOTYPE1000> GetEmpSubGroupDropdownData();
        List<INFOTYPE1000> GetSubTypeDropdownData();
        List<INFOTYPE1000> GetCostCenterDropdownData(DateTime CheckDate);
        List<INFOTYPE1000> GetCostCenterByOrganize(string Organize);
        List<INFOTYPE1000> GetAreaDropdownData();
        List<INFOTYPE1000> GetSubAreaDropdownData();
        List<INFOTYPE1000> GetRelationSelectData();
        List<INFOTYPE1000> GetRelationValidationSelectData();

        #endregion INFOTYPE1000

        #region Job Config from DHR

        string JobALL_DHR_TO_PersonalSubAreaSetting();
        string JobALL_DHR_TO_PersonalSubGroupSetting();
        string JobALL_DHR_TO_MonthlyWorkschedule(int Year);
        string JobALL_DHR_TO_DailyWorkschedule();

        #endregion Job Config from DHR

        List<string> GetRelationValidationByRelation(string relation);

        bool InsertActionLog(UTILITY.DATACLASS.ActionLog oActionLog);

        bool InsertJobActionLog(JobActionLog oActionLog);

        string GetBusinessPlace(string oBusinessArea);

        List<WorkScheduleRule> GetWorkScheduleRule(string Profile);

        BreakPattern GetBreakPattern(string DWSGroup, string BreakCode);

        WFRuleSetting GetWFRuleSetting(string WFRule);

        List<AttachedFileConfiguration> GetAttachedFileConfigurationList(string CategoryGroup, string CategoryType);

        DataTable GetOffCycleTypeList(string oLang);

        ESS.EMPLOYEE.CONFIG.OM.FiscalYear GetFiscalYear();
        #region New Workflow
        List<RequestTypeKeyCodeSetting> GetRequestFlowSettingByCriteria(int oRequestTypeID, int oVersion);
        #endregion
    }
}