using System;
using ESS.UTILITY.EXTENSION;
using System.Collections.Generic;

namespace ESS.EMPLOYEE.DATACLASS
{
    public class DelegateHeader : AbstractObject
    {
        public DelegateHeader()
        {
            DetailList = new List<DelegateDetail>();
        }

        public string RequestNo { get; set; }
        public int DelegateID { get; set; }
        public string DelegateFrom { get; set; }
        public string DelegateFromName { get; set; }
        public string DelegateFromPosition { get; set; }
        public string DelegateFromPositionName { get; set; }
        public string DelegateFromOrg { get; set; }
        public string DelegateFromOrgName { get; set; }
        public Dictionary<string,Object> AllEmployeePossibleToDelegated { get; set; }
        public string DelegateFromCompanyCode { get; set; }
        public string DelegateTo { get; set; }
        public string DelegateToName { get; set; }
        public string DelegateToPosition { get; set; }
        public string DelegateToPositionName { get; set; }
        public string DelegateToOrg { get; set; }
        public string DelegateToOrgName { get; set; }
        public string DelegateToCompanyCode { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }

        public string ImageUrl { get; set; }

        public string CreatedByName { get; set; }
        public string CreatedByPositionID { get; set; }
        public string CreatedByCompanyCode { get; set; }
        public DateTime CreatedByDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedByPositionID { get; set; }
        public string UpdatedByCompanyCode { get; set; }
        public DateTime UpdatedByDate { get; set; }
        public string Status { get; set; }

        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }

        public List<DelegateDetail> DetailList { get; set; }
    }

    public class DelegateDetail : AbstractObject
    {
        public int DelegateID { get; set; }
        public int RequestTypeID { get; set; }

        public bool IsCheck { get; set; }
        public string RequestTypeName { get; set; }
    }
}