#region Using

using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.INTERFACE;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.WF;
using System.Data;
using ESS.UTILITY.DATACLASS;
using ESS.EMPLOYEE.CONFIG;

#endregion Using

namespace ESS.EMPLOYEE.ABSTRACT
{
    public class AbstractEmployeeConfigService : IEmployeeConfigService
    {
        #region IEmployeeConfig Members
        
        public virtual PersonalArea GetPersonalAreaSetting(string PersonalArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual PersonalSubGroupSetting GetPersonalSubGroupSetting(string EmpGroup, string EmpSubGroup)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> GetMonthlyWorkscheduleList(int Year, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveMonthlyWorkscheduleList(int Year, List<MonthlyWS> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DailyWS> GetDailyWorkscheduleList(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual BreakPattern GetBreakPattern(string DWSGroup, string BreakCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual WFRuleSetting GetWFRuleSetting(string WFRule)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, bool IncludeMonth, bool IncludeYear)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetPersonalSubAreaWSRMapping(string strSubArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual List<INFOTYPE1000> GetGenderCheckBoxData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetPrefixDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetMaritalStatusDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetNationalityDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetLanguageDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetReligionDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetEmpGroupDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetEmpSubGroupDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetSubTypeDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetCostCenterDropdownData(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetCostCenterByOrganize(string Organize)
        {
            throw new Exception("The method or operation is not impliemented.");
        }

        public virtual List<INFOTYPE1000> GetAreaDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetSubAreaDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetRelationSelectData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetRelationValidationByRelation(string relation)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE1000> GetRelationValidationSelectData()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion IEmployeeConfig Members
        public virtual DataTable GetMonthlyWorkscheduleGetByPeriod(string EmployeeID, int iYear, int iMonth)
        {
            throw new NotImplementedException();
        }
        public virtual bool InsertActionLog(ActionLog oActionLog)
        {
            throw new NotImplementedException();
        }
        public virtual bool InsertJobActionLog(JobActionLog oActionLog)
        {
            throw new NotImplementedException();
        }
        public virtual string GetBusinessPlace(string oBusinessArea)
        {
            throw new NotImplementedException();
        }

        #region Job Config from DHR

        public virtual string JobALL_DHR_TO_PersonalSubAreaSetting()
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_PersonalSubGroupSetting()
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_MonthlyWorkschedule(int Year)
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_DailyWorkschedule()
        {
            throw new NotImplementedException();
        }

        public virtual DailyWS GetDailyWorkschedule(string EmployeeID, DateTime CheckDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<WorkScheduleRule> GetWorkScheduleRule(string Profile)
        {
            throw new NotImplementedException();
        }

        #endregion Job Config from DHR

        public virtual List<AttachedFileConfiguration> GetAttachedFileConfigurationList(string CategoryGroup, string CategoryType)
        {
            throw new NotImplementedException();
        }

        public virtual List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective)
        {
            throw new NotImplementedException();
        }

        public virtual MonthlyWS GetCalendar(string EmployeeID, int Year, int Month)
        {
            throw new NotImplementedException();
        }

        public virtual MonthlyWS GetPublicCalendar(string CompanyCode, int Year, int Month)
        {
            throw new NotImplementedException();
        }

        public virtual MonthlyWS GetSimulateCalendar(string EmployeeID, int Year, int Month, string WFRule)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetOffCycleTypeList(string oLang)
        {
            throw new NotImplementedException();
        }
        public virtual ESS.EMPLOYEE.CONFIG.OM.FiscalYear GetFiscalYear()
        {
            throw new NotImplementedException();
        }
        #region New Workflow
        public virtual List<RequestTypeKeyCodeSetting> GetRequestFlowSettingByCriteria(int oRequestTypeID, int oVersion)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}