#region Using

using System;
using System.Collections.Generic;
using System.Data;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.INTERFACE;
using ESS.EMPLOYEE.JOB;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.UTILITY.DATACLASS;
using ESS.EMPLOYEE.CONFIG;

#endregion Using

namespace ESS.EMPLOYEE.ABSTRACT
{
    public class AbstractEmployeeDataService : IEmployeeDataService
    {
        #region IEmployeeService Members

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime oCheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool ValidateActiveEmployeeForTravel(string EmployeeID, DateTime BeginTravelDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string GetEmployeeIDFromUserID(string UserID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool ValidateEmployeeID(string EmployeeID)
        {
            return this.ValidateEmployeeID(EmployeeID, DateTime.Now);
        }

        public virtual bool ValidateEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID)
        {
            return GetINFOTYPE0000List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID1, string EmployeeID2)
        {
            return GetINFOTYPE0000List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetINFOTYPE0000List(EmployeeID1, EmployeeID2, DateTime.MinValue, Profile);
        }

        public virtual List<INFOTYPE0000> GetINFOTYPE0000List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public INFOTYPE0001 GetINFOTYPE0001(string EmployeeID)
        {
            return this.GetINFOTYPE0001(EmployeeID, DateTime.Now);
        }

        public INFOTYPE0001 GetINFOTYPE0001(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0001 returnValue = null;
            List<INFOTYPE0001> list = GetINFOTYPE0001List(EmployeeID, EmployeeID, CheckDate, "DEFAULT");
            if (list.Count > 0)
            {
                returnValue = list[0];
            }
            return returnValue;
        }

        public CARDSETTING GetCardSetting(string EmployeeID)
        {
            return this.GetCardSetting(EmployeeID, DateTime.Now);
        }

        public CARDSETTING GetCardSetting(string EmployeeID, DateTime CheckDate)
        {
            CARDSETTING returnValue = null;
            List<CARDSETTING> list = GetCardSettingList(EmployeeID, CheckDate, "DEVTIMESHEET");
            if (list.Count > 0)
            {
                returnValue = list[0];
            }
            return returnValue;
        }

        public virtual List<CARDSETTING> GetCardSettingList(string EmployeeID, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID)
        {
            return GetINFOTYPE0001List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2)
        {
            return GetINFOTYPE0001List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID, DateTime CheckDate)
        {
            return GetINFOTYPE0001List(EmployeeID, EmployeeID, CheckDate, "DEFAULT");
        }

        public List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetINFOTYPE0001List(EmployeeID1, EmployeeID2, DateTime.Now, Profile);
        }
        public virtual List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public List<INFOTYPE0001> GetINFOTYPE0001AllList(string EmployeeID)
        {
            return GetINFOTYPE0001List(EmployeeID, EmployeeID, DateTime.MinValue, "DEFAULT");
        }

        public virtual List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0001(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0182 GetINFOTYPE0182(string EmployeeID, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE0182(string EmployeeID, string Language, INFOTYPE0182 item)
        {
            throw new Exception("The method 'UpdateINFOTYPE0182(string EmployeeID, string Language, INFOTYPE0182 item)' is not implemented.");
        }

        public virtual void SaveINFOTYPE0182(string EmployeeID1, string EmployeeID2, List<INFOTYPE0182> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
            //return GetINFOTYPE0182List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2)
        {
            return GetINFOTYPE0182List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public virtual List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method 'List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2, string Profile)' is not implemented.");
        }

        public virtual INFOTYPE0105 GetINFOTYPE0105(string EmployeeID, string oCategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0105> GetINFOTYPE0105ByCategoryCode(string EmployeeID, string oCategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID)
        {
            return GetINFOTYPE0105List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID1, string EmployeeID2)
        {
            return GetINFOTYPE0105List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public virtual List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0105(string EmployeeID1, string EmployeeID2, List<INFOTYPE0105> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0007> GetINFOTYPE0007(string EmployeeID, int Year, int Month)
        {
            return GetINFOTYPE0007List(EmployeeID, EmployeeID, Year, Month, "DEFAULT");
        }

        public List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2)
        {
            return GetINFOTYPE0007List(EmployeeID1, EmployeeID2, -1, -1, "DEFAULT");
        }

        public List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetINFOTYPE0007List(EmployeeID1, EmployeeID2, -1, -1, Profile);
        }

        public virtual List<INFOTYPE0007> GetINFOTYPE0007List(DateTime BeginDate, DateTime EndDate, string TimeEvaluateClass)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0007> GetINFOTYPE0007List(int Year, int Month, string TimeEvaluationClass)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0007(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE0027> GetInfotype0027List(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE0027> GetInfotype0027List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SaveInfotype0027(string EmployeeID1, string EmployeeID2, List<INFOTYPE0027> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual UserSetting GetUserSetting(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetUserRole(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual UserRoleSetting GetUserRoleSetting(string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserResponse(string Role, string AdminGroup)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID, DateTime checkDate)
        {
            throw new NotImplementedException();
        }
        public virtual List<ESS.EMPLOYEE.CONFIG.TM.Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ESS.EMPLOYEE.CONFIG.TM.Substitution> GetInfotype2003_Log(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EmployeeData FindManager(string EmployeeID, string PositionID, string ManagerCode, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveUserSetting(UserSetting userSetting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetDelegatePersons(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TaskCopyEmployeeConfig> GetTasks()
        {
            throw new Exception("Tge method 'List<TaskCopyEmployeeConfig> GetTasks()' is not implemented.");
        }

        public List<WorkPlaceCommunication> GetWorkplaceData()
        {
            return GetWorkplaceData("");
        }

        public virtual List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveWorkPlaceData(string EmployeeID, List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SaveWorkPlaceData(List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            SaveWorkPlaceData("", workplaceList, Profile);
        }


        public virtual List<INFOTYPE0001> GetMeEmpList(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DateSpecificData> GetDateSpecificList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetUserInRole(string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        //CHAT 2011-10-05 ����Ѻ�֧���������ʾ�ѡ�ҹ��������˹��§ҹ�������
        public virtual DataTable GetEmployeeIDByOrgUnit(string OrgUnit)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-05 ����Ѻ�֧������ ResponseType �ͧ��������͵�Ǩ�ͺ�Է�������ҹ��� Type
        public virtual List<string> GetUserResponseType(string EmpID, string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Performance �ͧ��ѡ�ҹ
        public virtual DataTable GetRequestorPerformance(string EmpID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Competency �ͧ��ѡ�ҹ
        public virtual DataTable GetRequestorCompetency(string EmpID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IEmployeeService Members

        #region IEmployeeService Members

        public INFOTYPE0007 GetINFOTYPE0007(string EmployeeID, DateTime CheckDate)
        {
            return GetINFOTYPE0007(EmployeeID, CheckDate, "DEFAULT");
        }

        public virtual INFOTYPE0007 GetINFOTYPE0007(string EmployeeID, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IEmployeeService Members

        public virtual string GetNameFromINFOTYPE0002(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Chamrat P. (2021-01-24)
        public virtual string GetNameWithTitleFromINFOTYPE0002(string EmployeeID)
        {
            throw new Exception("The mehtod or operation is not implemented.");
        }

        //AddBy: Ratchatawan W. (2012-04-23)
        public virtual bool ValidateManager(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Ratchatawan W. (2012-04-23)
        public virtual List<EmployeeData> GetDelegateEmployeeOMByPosition(string EmployeeID, string PositionID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID, string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0032 GetINFOTYPE0032ByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0032> GetINFOTYPE0032List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0032(string EmployeeID1, string EmployeeID2, List<INFOTYPE0032> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> GetAllINFOTYPE0001(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> GetAllEmployeeName(string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> GetAllEmployeeNameForUserRole(string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Nattawat S. 230520020
        public virtual DataSet GetUserInResponseForActionOfInsteadWithIncludeSub(string EmployeeID, int SubjectID, DateTime CheckDate, string FilterEmpgroup, string FilterText)
        {
            throw new Exception("The method or operation Named 'GetUserInResponseForActionOfInsteadWithIncludeSub' is not implemented.");
        }
        public virtual DataSet GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, string SearchText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0007(List<INFOTYPE0007> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0007(List<INFOTYPE0007> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region IEmployeeService Members

        public virtual List<EmployeeData> GetOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsHaveOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0001(List<INFOTYPE0001> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0030> GetINFOTYPE0030List(string EmployeeID1, string EmployeeID2)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0030> GetINFOTYPE0030List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveINFOTYPE0030(string EmployeeID1, string EmployeeID2, List<INFOTYPE0030> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0030 GetINFOTYPE0030(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IEmployeeService Members

        public virtual List<INFOTYPE0185> GetInfotype0185(string EmployeeID, DateTime date)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string InsertINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string InsertINFOTYPE0002And0182And0001And0105Data(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182,INFOTYPE0001 oINF1,INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0002And0182And0001And0105And1001Data(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0002And0182(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0001(string EmployeeID, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0105(string EmployeeID, DateTime BeginDate, string SubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void Resign(string EmployeeID, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> INFOTYPE0001GetAllEmployeeHistory(string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<string, object> INFOTYPE0002And0182GetAllHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> INFOTYPE0001GetAllHistory(string EmployeeID, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0105> INFOTYPE0105GetAllHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<string, object> INFOTYPE0002And0182Get(string EmployeeID, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0001 INFOTYPE0001Get(string EmployeeID, DateTime BeginDate, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0105 INFOTYPE0105Get(string EmployeeID, DateTime BeginDate,string CategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> INFOTYPE1000GetAllHistory()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE1000(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 INFOTYPE1000Get(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1001> INFOTYPE1001GetAllHistory()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1001> GetPositionForPerson(string NextObjectID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1001> GetBelongToRelation()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1001 INFOTYPE1001Get(string ObjectType, string ObjectID, string NextObjectType, string NextObjectID, string Relation, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE1001(INFOTYPE1001 oINF1001, INFOTYPE1001 oOldINF1001)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE0002And0182And0001And0105And1001List(List<INFOTYPE0001> INF0001Lst, List<INFOTYPE0002> INF0002Lst, List<INFOTYPE0182> INF0182Lst, List<INFOTYPE0105> INF0105Lst, List<INFOTYPE1001> INF1001Lst)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1013> INFOTYPE1013GetAllHistory()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1013 INFOTYPE1013Get(string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetUserInResponseForActionOfInsteadByResponseType(string ResponseType, string ResponseCode, string ResponseCompanyCode, bool IncludeSub, DateTime CheckDate,string EmployeeID)
        {
            throw new NotImplementedException();
        }


        public virtual DataTable GetOrganizationStructure(string EmployeeID, string PositionID)
        {
            throw new NotImplementedException();
        }
		
		public virtual DataTable ValidateData(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual bool InsertActionLog(ActionLog oActionLog)
        {
            throw new NotImplementedException();
        }
        public virtual bool InsertJobActionLog(JobActionLog oActionLog)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetMonthlyWorkSchedule(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE0001> GetAllINFOTYPE0001ForReport(DateTime oCheckDate, string oLanguage)
        {
            throw new NotImplementedException();
        }


        public virtual DataTable GetExternalUserbyID(string UserID, string oLanguage)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetPettyCustodianGetByCode(string PettyCode)
        {
            throw new NotImplementedException();
        }


        public virtual DataSet GetContractDetailByEmployeeID(string EmployeeID, string CostCenter,DateTime CheckDate)
        {
            throw new NotImplementedException();
        }

        public virtual bool IsContractUser(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual bool IsExternalUser(string EmployeeID)
        {
            throw new NotImplementedException();
        }


        public virtual DateSpecificData GetDateSpecific(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual DateSpecificData GetERPDateSpecific(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetExternalUserSnapshotbyRequestID(int RequestID, int ApproverID)
        {
            throw new NotImplementedException();
        }

        #region Job Data from DHR
        public virtual string JobALL_DHR_TO_INFOTYPE0001(string EmployeeID1, string EmployeeID2)
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE0002(string EmployeeID1,string EmployeeID2)
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE0007(string EmployeeID1, string EmployeeID2)
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE0027(string EmployeeID1, string EmployeeID2)
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE0030(string EmployeeID1, string EmployeeID2)
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE0105(string EmployeeID1, string EmployeeID2)
        {
            throw new NotImplementedException();
        }
        public virtual string JobALL_DHR_TO_INFOTYPE0105_Mapping(out string oMessage)
        {
            throw new NotImplementedException();
        }

        public virtual string JobALL_DHR_TO_INFOTYPE0182(string EmployeeID1, string EmployeeID2)
        {
            throw new NotImplementedException();
        }

        public virtual string JobAll_DHR_TO_DateSpecificData()
        {
            throw new NotImplementedException();
        }

        #endregion Job Data from DHR

        public virtual bool VerifyPinCode(string EmployeeID, string oPincode)
        {
            throw new NotImplementedException();
        }

        public virtual bool ExistPinCodeByEmployee(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual void ChangePinCode(string EmployeeID, string oPincode, string oNewPINcode)
        {
            throw new NotImplementedException();
        }

        public virtual void RequestNEWPin(EmployeeData oEmp)
        {
            throw new NotImplementedException();
        }

        public virtual bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            throw new NotImplementedException();
        }

        public virtual void CreateNEWPin(string EmployeeID, string TicketID, string oPincode)
        {
            throw new NotImplementedException();
        }

        public virtual List<Substitution> GetInfotype2003_LogList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<Substitution> GetInfotype2003_LogListInverse(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<Substitution> GetInfotype2003_POSTING(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual List<Substitution> GetInfotype2003_POSTINGInverse(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetEmployeesList(string oLang)
        {
            throw new NotImplementedException();
        }
        
        public virtual DataTable GetFlowIDByRequestTypeIDKeyCode(string RequestTypeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<EmployeeData> GetOMByEmployeePosition(string EmployeeID, string Position, DateTime CheckDate)
        {
            throw new NotImplementedException();
        }
        public virtual DataTable GetConfigSelectYear()
        {
            throw new NotImplementedException();
        }
    }
}