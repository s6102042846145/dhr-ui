#region Using

using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.INTERFACE;
using ESS.UTILITY.EXTENSION;

#endregion Using

namespace ESS.EMPLOYEE.ABSTRACT
{
    [Serializable()]
    public abstract class AbstractInfoType : AbstractObject, IInfoType
    {
        #region Properties

        public string EmployeeID { get; set; }

        public string SubType { get; set; }

        public string SeqNo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string RequestorPositionName { get; set; }

        public string ErrorMessage { get; set; }

        public abstract string InfoType { get; }

        #endregion Properties

        #region Function

        public virtual object LoadData(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("method LoadData is not implement");
        }

        public virtual List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile, params object[] args)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data, params object[] args)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion Function
    }
}