using System.Collections.Generic;
using System.Data;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.MAIL.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.AUTHORIZATION;
using System;
using ESS.WORKFLOW.INTERFACE;
using ESS.DELEGATE.DATACLASS;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ESS.WORKFLOW
{
    public class WorkflowManagement
    {
        #region Constructor
        private WorkflowManagement()
        {

        }
        #endregion 

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, WorkflowManagement> Cache = new Dictionary<string, WorkflowManagement>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static WorkflowManagement CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new WorkflowManagement()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            WorkflowManagement oWorkflowManagement = new WorkflowManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oWorkflowManagement;
        }

        public string FILE_ROOTPATH
        {
            get
            {
                string strRootPath = ShareDataManagement.LookupCache(CompanyCode, ModuleID, "FILE_ROOTPATH");
                strRootPath = System.Web.HttpContext.Current.Server.MapPath(strRootPath);
                if (strRootPath[strRootPath.Length - 1] != '\\')
                {
                    strRootPath += "\\";
                }
                return strRootPath;
            }
        }


        public string TICKET_URL
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TICKET_URL");
            }
        }

        public string TICKET_URL_FOREXTERNAL_USER
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TICKET_URL_FOREXTERNAL_USER");
            }
        }

        public string MASSAPPROVE_URL_FOREXTERNAL_USER
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MASSAPPROVE_URL_FOREXTERNAL_USER");
            }
        }

        public string SHOW_USERROLE_IN_ROLEORNAME
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SHOW_USERROLE_IN_ROLEORNAME","ROLE");
            }
        }

        public bool SHOW_USERROLE_IN_MAIL_SUBJECT
        {
            get
            {
                bool iSHOW_USERROLE_IN_MAIL_SUBJECT = false;
                bool.TryParse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SHOW_USERROLE_IN_MAIL_SUBJECT"), out iSHOW_USERROLE_IN_MAIL_SUBJECT);
                return iSHOW_USERROLE_IN_MAIL_SUBJECT;
            }
        }

        public string DEFAULT_URL
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "URL");
            }
        }

        #endregion MultiCompany  Framework

        public bool CanApproveCoWorkflow(string RequestNo, string ActionBy, string ActionByPosition)
        {
            return ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.CanApproveCoWorkflow(RequestNo, ActionBy, ActionByPosition);
        }

        public ReceipientList GetSpecialReceipient(string EmployeeID, string Position, string Code, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.GetSpecialReceipient(EmployeeID, Position, Code, CheckDate);
        }

        public EmployeeData GetProjectManager(string ProjectCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetProjectManager(ProjectCode);
        }

        public List<string> GetAllAdminGroup()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllAdminGroup();
        }
        public void RecallUpdateDocument(string RequestNo, string ActionBy, string ActionByPosition)
        {
            ServiceManager.CreateInstance(CompanyCode).WorkflowService.RecallUpdateDocument(RequestNo, ActionBy, ActionByPosition);
        }
        public void RequestTypeSummarySave(int RequestID, int ItemID, string SummaryTextTH, string SummaryTextEN)
        {
            ServiceManager.CreateInstance(CompanyCode).WorkflowService.RequestTypeSummarySave(RequestID, ItemID, SummaryTextTH, SummaryTextEN);
        }
        public string GetCommonText(string Category, string Language, string Code)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestText(Category, Language, Code);
        }
        //AddBy: Ratchatawan W. (2011-08-23)
        public bool CheckDelegateAuthorize(int RequestID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.CheckDelegateAuthorize(RequestID);
        }
        //AddBy: Ratchatawan W. (2011-08-23)
        public bool CheckDelegateAuthorize(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.CheckDelegateAuthorize(RequestNo);
        }
        //AddBy: Ratchatawan W. (2011-08-23)
        public DataTable GetRequestInDelegateBox()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestInDelegateBox();
        }
        public DataSet GetRequestDocumentListForMobile(int BoxID, EmployeeData Employee, string Filter)//RequestBoxFilter Filter)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestDocumentList(BoxID, Employee, Filter);
        }
        public DataSet GetPositionApprove(int BoxID,string EmployeeID,string PositionID,string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetPositionApprove(BoxID, EmployeeID, PositionID, Language);
        }
        public void GenerateMailTo(Dictionary<int, List<WorkflowMailTo>> list, Receipient rcp, bool IsCC, RequestDocument reqDoc,int TemplateID)
        {
            ServiceManager.CreateInstance(CompanyCode).WorkflowService.GenerateMailTo(list, rcp, IsCC, reqDoc,TemplateID);
        }

        #region AuthorizationModel
        public List<AuthorizationModelSet> CalculateAuthorize(EmployeeData ActionBy, RequestDocument Document,int oModelID, List<AuthorizationModelKey> oKeys)
        {
            List<AuthorizationModelSet> oReturn = new List<AuthorizationModelSet>();
            List<object> values = new List<object>();
            foreach (AuthorizationModelKey key in oKeys)
            {
                PropertyInfo oProp;
                object value = "";
                switch (key.AuthConditionMode)
                {
                    case AuthorizationModelKeyMode.DATA:
                        value = Document.Data.Rows[0][key.AuthConditionCode];
                        break;

                    case AuthorizationModelKeyMode.USER: 
                        oProp = typeof(EmployeeData).GetProperty(key.AuthConditionCode, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                        if (oProp != null)
                        {
                            value = oProp.GetValue(ActionBy, null);
                        }
                        break;

                    case AuthorizationModelKeyMode.REQUESTOR:
                        oProp = typeof(EmployeeData).GetProperty(key.AuthConditionCode, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                        if (oProp != null)
                        {
                            value = oProp.GetValue(Document.Requestor, null);
                        }
                        break;

                    default:
                        value = "";
                        break;
                }
                values.Add(value);
            }

            #region " Generate Condition "

            oReturn = ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.CalculateAuthorize(oModelID ,oKeys, values);

            #endregion " Generate Condition "

            return oReturn;
        }

        public IEnumerable<AuthorizationModel> GetAllModel()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllAuthorizationModel();
        }

        public AuthorizationModel GetModel(int oModelID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizationModel(oModelID);
        }

        public string GetMailTemplate(int MailID, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMailTemplateByID(MailID, LanguageCode);
        }

        public string GetMailSubject(int MailID, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetMailSubjectByID(MailID, LanguageCode);
        }
        public Receipient GetStepReceipient(int oAuthModelID, int oStepID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizationModelStep(oAuthModelID, oStepID);
        }
        public void CreateTicket(string TicketID, string RequestNo, string EmployeeID, string CompanyCode, bool IsExternalUser)
        {
            ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.CreateTicket(TicketID, RequestNo, EmployeeID, CompanyCode, IsExternalUser);
        }

        public List<AuthorizationModelSetCondition> GetValue(int oAuthModelID, int oSetID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllAuthorizationModelCondition(oAuthModelID, oSetID);
        }
        #endregion

        #region RequestApplication
        public List<RequestApplication> GetAuthorizeApplications()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizeApplications();
        }

        public List<ApplicationSubject> GetAuthorizeApplicationSubject(int oApplicaqtionID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizeApplicationSubject(oApplicaqtionID);
        }

        public RequestBox GetRequestBox(int iBoxID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestBox(iBoxID);
        }
        public int CountRequestBox(int iBoxID, string sEmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.CountRequestBox(iBoxID, sEmployeeID);
        }
        #endregion

        public void ResolveReceipientToOtherCompany(DataTable dtReceipient)
        { 
            foreach(DataRow dr in dtReceipient.Rows)
                ServiceManager.CreateInstance(dr["CompanyCode"].ToString()).WorkflowService.ResolveReceipientToOtherCompany(dr);
        }

        public DataTable GetUserRoleResponseForOtherCompany(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetUserRoleResponseForOtherCompany(EmployeeID, CompanyCode);
        }

        public DataTable GetFlowItemActionPreference()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetFlowItemActionPreference();
        }

        public EmployeeData GetLastedActionByActionCode(int RequestID,string ActionCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetLastedActionByActionCode(RequestID, ActionCode);
        }

        public EmployeeData GetLastedActionByActionCode(string RequestNo, string ActionCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetLastedActionByActionCode(RequestNo, ActionCode);
        }

        public List<RequestFlow> GetPassFlow(string strRequestNo)
        {
            RequestDocument oDoc = RequestDocument.LoadDocumentReadOnly(strRequestNo, CompanyCode);
            return oDoc.GetPassedFlow();
        }

        public RequestDocument LoadDocumentWithOutCheckAuthorize(string strRequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.LoadDocumentWithOutCheckAuthorize(strRequestNo);
        }


        public EmployeeData GetLastedManagerByRequestNo(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetLastedManagerByRequestNo(RequestNo);
        }

        //public List<PostFBCJResponse> GetFBCJResponseLogPost(string oRequestNo)
        //{
        //    return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetFBCJResponseLogPost(oRequestNo);
        //}
        
        //public void SaveResponseLog(ResponseLog oResponseLog)
        //{
        //    return ServiceManager.CreateInstance(CompanyCode).WorkflowService.SaveResponseLog(oResponseLog);
        //}
        
        public DataTable GetRequestTypeKeySetting(int RequestTypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeKeySetting(RequestTypeID);
        }

        public RequestTypeSetting GetRequestTypeSetting(RequestDocument doc)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeSetting(doc.RequestTypeID, doc.RequestType.VersionID, doc.FlowKey);
        }

        public RequestTypeSetting GetRequestTypeSettingWithoutCheckFlow(RequestDocument doc)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestTypeSettingByFlowID(doc.RequestTypeID, doc.RequestType.VersionID, doc.FlowID);
        }

        public List<Receipient> GetAllActionInPass(RequestDocument doc,string EmployeeID)
        {
            return ServiceManager.CreateInstance(doc.CompanyCode).WorkflowService.GetAllActionInPass(doc.RequestID, EmployeeID);
        }

        public bool IsThisUserCanEndThisFlow(int RequestTypeID,DataTable Info,EmployeeData ActionBy)
        {
            Dictionary<int, bool> Result = new Dictionary<int, bool>();
            DataTable oDataTable = GetRequestTypeKeySetting(RequestTypeID);
            if (oDataTable.Select("[Key]='AUTHORIZEMODEL'").Length > 0)
            {
                int AuthorizeStepID = 0;
                foreach (string strAuth in oDataTable.Select("[Key]='AUTHORIZEMODEL'")[0]["Value"].ToString().Split(','))
                {
                    int AuthorizeModelID = -1;
                    int.TryParse(strAuth, out AuthorizeModelID);
                    AuthorizationModel oModel = GetModel(AuthorizeModelID);
                    List<AuthorizationModelSet> set = CalculateAuthorize(ActionBy, Info, oModel.AuthModelID, oModel.Keys);
                    Result.Add(oModel.AuthModelID, set.Count > 0);
                }
            }

            return !Result.ContainsValue(false);
        }

        public List<AuthorizationModelSet> CalculateAuthorize(EmployeeData ActionBy, DataTable Info, int oModelID, List<AuthorizationModelKey> oKeys)
        {
            List<AuthorizationModelSet> oReturn = new List<AuthorizationModelSet>();
            List<object> values = new List<object>();
            foreach (AuthorizationModelKey key in oKeys)
            {
                object value = "";
                switch (key.AuthConditionMode)
                {
                    case AuthorizationModelKeyMode.DATA:
                        value = Info.Rows[0][key.AuthConditionCode];
                        break;

                    case AuthorizationModelKeyMode.USER:
                        PropertyInfo oProp = typeof(EmployeeData).GetProperty(key.AuthConditionCode, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                        if (oProp != null)
                        {
                            value = oProp.GetValue(ActionBy, null);
                        }
                        break;

                    default:
                        value = "";
                        break;
                }
                values.Add(value);
            }

            #region " Generate Condition "

            oReturn = ServiceManager.CreateInstance(CompanyCode).WorkflowService.CalculateAuthorize(oModelID, oKeys, values);

            #endregion " Generate Condition "

            return oReturn;
        }

        public DataTable ValidateTicket(string TicketID, string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.ValidateTicket(TicketID, EmployeeID);
        }

        public DataTable ValidateTicketForExternaluser(string TicketID, string ExternaluserID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.ValidateTicketForExternalUser(TicketID, ExternaluserID);
        }

        public List<EmployeeData> GetDelegateToByEmployeePosition(string EmployeeID, string PositionID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetDelegateToByEmployeePosition(EmployeeID, PositionID);
        }

        public List<DelegateHeader> GetDelegateDataByCriteria(string EmployeeID, int Month, int Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetDelegateDataByCriteria(EmployeeID, Month, Year);
        }

        public DataTable GetAllRequestType(string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllRequestType(LanguageCode);
        }

        public DataTable GetDelegationInfo(int RequestID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetDelegationInfo(RequestID);
        }

        public string SaveDelegateHeader(DelegateHeader oDelegateHeader)
        {
           return ServiceManager.CreateInstance(CompanyCode).WorkflowService.SaveDelegateHeader(oDelegateHeader);
        }

        public void DeleteDelegateHeader(DelegateHeader oDelegateHeader)
        {
            ServiceManager.CreateInstance(CompanyCode).WorkflowService.DeleteDelegateHeader(oDelegateHeader);
        }
        public void SaveRequestFlag(List<RequestFlag> lstFlag)
        {
            ServiceManager.CreateInstance(CompanyCode).WorkflowService.SaveRequestFlag(lstFlag);   
        }

        public string GetLastedCommentForActionCode(int RequestID, string ActionCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetLastedCommentByActionCode(RequestID, ActionCode);
        }

        public string GetEditReasonByRequestNo(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetEditReasonByRequestNo(RequestNo);
        }

        public string GetLastedActionByEmployeeID(int RequestID, string EmployeeID, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetLastedActionByEmployeeID(RequestID, EmployeeID, LanguageCode);
        }

        public DataTable GetActionInPassByRequestNo(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetActionInPassByRequestNo(RequestNo);
        }

        public string FillInText(RequestDocument Document,string input, FlowItemAction action, EmployeeData oEmp, string Language, WorkflowMailTo mailto)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Regex oRegex = new Regex(@"\[#(?<REQUESTINFO>\w+)\]");
            MatchCollection groups = oRegex.Matches(input);

            foreach (Match item in groups)
            {
                switch (item.Groups["REQUESTINFO"].Value.ToUpper())
                {
                    case "REQUESTOR":
                        input = input.Replace(item.Value, Document.Requestor.EmployeeID);
                        break;

                    case "REQUESTORNAME":
                        input = input.Replace(item.Value, Document.Requestor.AlternativeName(Language));
                        break;

                    case "REQUESTTYPETEXT":
                        input = input.Replace(item.Value, RequestText.CreateInstance(this.CompanyCode).LoadText("REQUESTTYPE", Language, Document.RequestType.Code).ToLower());
                        break;
                    case "MAILTO":
                        input = input.Replace(item.Value, mailto.MailToDisplay);
                        break;
                    case "RECEIPIENT":
                        input = input.Replace(item.Value, Document.TranslateList(Document.Receipients).ToString());
                        break;
                    case "RECEIPIENTBAK":
                        input = input.Replace(item.Value, Document.TranslateList(Document.BackupReceipients).ToString());
                        break;

                    case "REQUESTNO":
                        input = input.Replace(item.Value, Document.RequestNo);
                        break;

                    case "STATUSTEXT":
                        input = input.Replace(item.Value, RequestText.CreateInstance(this.CompanyCode).LoadText("STATUS", Language, Document.CurrentFlowItem.Code).ToLower());
                        break;

                    case "ACTIONDATE":
                        input = input.Replace(item.Value, DateTime.Now.ToString("dd/MM/yyyy", oCL));
                        break;

                    case "ACTIONBY":
                        input = input.Replace(item.Value, oEmp.EmployeeID);
                        break;

                    case "ACTIONBYNAME":
                        input = input.Replace(item.Value, oEmp.AlternativeName(Language));
                        break;

                    case "ACTIONTEXT":
                        input = input.Replace(item.Value, RequestText.CreateInstance(this.CompanyCode).LoadText("ACTION", Language, action.Code));
                        break;
                    case "LASTEDACTIONTEXT": //�֧���� Action ������Ѻ���존����ش
                        string ActionText = GetLastedActionByEmployeeID(Document.RequestID, oEmp.EmployeeID, Language);
                        input = input.Replace(item.Value, ActionText);
                        break;
                    //case "REQUESTSUMMARY":
                    //    input = input.Replace(item.Value, __dataService.GenEmailBody(Document.Requestor,Document.Data, Document.CurrentFlowItem.Code, Language));
                    //    break;

                    case "COMMENT":
                        if (Document.Comment != null && Document.Comment != "")
                        {
                            input = input.Replace(item.Value, string.Format("{0}", Document.Comment));
                        }
                        else
                        {
                            input = input.Replace(item.Value, "");
                        }
                        break;
                    case "REASON_RECALL":
                        if (string.IsNullOrEmpty(Document.Comment))
                        {
                            string LastedRecallComment = GetLastedCommentForActionCode(Document.RequestID, "RECALL");
                            if (!string.IsNullOrEmpty(LastedRecallComment))
                            {
                                input = input.Replace(item.Value, string.Format("{0}", LastedRecallComment));
                            }
                            else
                            {
                                input = input.Replace(item.Value, "");
                            }
                        }
                        else
                        {
                            input = input.Replace(item.Value, string.Format("{0}", Document.Comment));
                        }
                        break;
                    case "REASON_CANCEL":
                        if (string.IsNullOrEmpty(Document.Comment))
                        {
                            string LastedCancelComment = WorkflowManagement.CreateInstance(this.CompanyCode).GetLastedCommentForActionCode(Document.RequestID, "CANCEL");
                            if (!string.IsNullOrEmpty(LastedCancelComment))
                            {
                                input = input.Replace(item.Value, string.Format("{0}", LastedCancelComment));
                            }
                            else
                            {
                                input = input.Replace(item.Value, "");
                            }
                        }
                        else
                        {
                            input = input.Replace(item.Value, string.Format("{0}", Document.Comment));
                        }
                        break;
                }
            }
            return input;
        }

        public string GetRequestText(string Category, string Language, string Code)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestText(Category, Language, Code);
        }

        public DataTable GetRecipientByItemID(int RequestID, int ItemID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRecipientByItemID(RequestID, ItemID);
        }

        public List<RequestDocument> GetRequestDocumentByRequestTypeID(int RequestTypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestDocumentByRequestTypeID(RequestTypeID);
        }

        public void UpdateRequestTypeSummarySnapshot(List<RequestDocument> oRequestDocumentList, int RequestTypeID)
        {
            ServiceManager.CreateInstance(CompanyCode).WorkflowService.UpdateRequestTypeSummarySnapshot(oRequestDocumentList, RequestTypeID);
        }

        public string GetAuthorizeBoxByEmployeeID(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizeBoxByEmployeeID(EmployeeID);
        }


        public  List<RequestFlag> GetRequestFlag(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestFlag(RequestNo);
        }

        public void ResponseFlagUpdate(string RequestNo, string TransactionType, string ServiceURL, DateTime TransactionStartDate, DateTime ServiceFinishDate, string ServiceResult, string ServiceException, DateTime DocumentFinishDate, string DocumentException)
        {
            ServiceManager.CreateInstance(CompanyCode).WorkflowService.ResponseFlagSave(RequestNo, TransactionType, ServiceURL, TransactionStartDate, ServiceFinishDate, ServiceResult, ServiceException, DocumentFinishDate, DocumentException);  
        }

        public bool CheckResponseFlag(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.CheckResponseFlag(RequestNo);
        }

        public DataTable GetEmployeeByUserRole(int RequestID, string UserRole, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetEmployeeByUserRole(RequestID, UserRole,Language);
        }
    }
}