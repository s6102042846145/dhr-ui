using System;

namespace ESS.WORKFLOW
{
    public class WorkflowException : Exception
    {
        public WorkflowException(string Message, Exception innerException)
            : base(Message, innerException)
        {
        }

        public WorkflowException(string Message)
            : base(Message)
        {
        }
    }
}