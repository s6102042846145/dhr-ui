using System;

namespace ESS.WORKFLOW
{
    public class ProcessRequestException : Exception
    {
        public ProcessRequestException(string message)
            : base(message)
        {
        }

        public ProcessRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}