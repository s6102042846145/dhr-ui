using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.WORKFLOW.AUTHORIZATION;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOAUTHORIZE : IRequestAction
    {
        public SENDTOAUTHORIZE()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            int AuthorizeModelID = -1;
            if (Params.ContainsKey("AUTHORIZEMODEL"))
            {
                foreach (string strAuth in Params["AUTHORIZEMODEL"].ToString().Split(','))
                {
                    AuthorizeModelID = -1;
                    int.TryParse(strAuth, out AuthorizeModelID);

                    AuthorizationModel oModel = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetModel(AuthorizeModelID);
                    Receipient rcp = WorkflowManagement.CreateInstance(Document.RequestorCompanyCode).GetStepReceipient(oModel.AuthModelID, 0);
                    oReturn.Receipients.Add(rcp);
                    if (oReturn.Receipients.Count > 0 && oReturn.Receipients.IsCanAction(Document, Document.Requestor))
                    {
                        oReturn.Receipients.Clear();
                    }
                    break;
                }
            }
            oReturn.GoToNextItem = true;
            oReturn.AuthorizeStep = 0;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}