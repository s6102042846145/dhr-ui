﻿using PTTCHEM.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class CHECKAUTHORIZEMULTIPLE : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();

            oReturn.GoToNextItem = true;
            oReturn.IsCompleted = true;
            if (IsSimulation)
            {
              
                //foreach (RequestFlowMultipleReceipient rep in Document.RequestFlowMultipleReceipientList)
                //{
                //    rep.ActionBy = rep.ApproverID;
                //    rep.ActionDate = DateTime.Now;
                //    rep.ActionByPosition = ActionBy.PositionID;
                //    rep.ActionCode = "APPROVE";
                //}
            }
            else
            {
                if (Document.RequestFlowMultipleReceipientList.Exists(obj => obj.ApproverID == ActionBy.EmployeeID))
                {
                    List<RequestFlowMultipleReceipient> Current = Document.RequestFlowMultipleReceipientList.FindAll(obj => obj.ApproverID == ActionBy.EmployeeID);
                    foreach (RequestFlowMultipleReceipient cur in Current)
                    {
                        foreach (RequestFlowMultipleReceipient repInGroup in Document.RequestFlowMultipleReceipientList.FindAll(obj => obj.ApproverGroup == cur.ApproverGroup || obj.ApproverID == cur.ApproverID))
                        {
                            repInGroup.ActionBy = ActionBy.EmployeeID;
                            repInGroup.ActionDate = DateTime.Now;
                            repInGroup.ActionByPosition = ActionBy.PositionID;
                            repInGroup.ActionByCompanyCode = ActionBy.CompanyCode;
                            repInGroup.ActionCode = "APPROVE";
                        }
                    }
                }

                if (Document.RequestFlowMultipleReceipientList.Exists(obj => obj.ActionDate <= DateTime.MinValue))
                {
                    foreach (RequestFlowMultipleReceipient existsing in Document.RequestFlowMultipleReceipientList.FindAll(obj => obj.ActionDate <= DateTime.MinValue))
                    {
                        Receipient rcp = new Receipient(ReceipientType.EMPLOYEE, existsing.ApproverID, existsing.ApproverCompanyCode, Document.RequestID);
                        oReturn.Receipients.Add(rcp);
                    }
                    oReturn.GoToNextItem = false;
                    oReturn.IsCancelled = false;
                    oReturn.IsCompleted = false;
                    oReturn.IsWaitForEdit = false;
                }
            }

            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.RequestFlowMultipleReceipientList = Document.RequestFlowMultipleReceipientList;

            return oReturn;
        }
    }
}
