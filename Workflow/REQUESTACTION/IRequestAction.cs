using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW
{
    public interface IRequestAction
    {
        RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params,bool IsSimulation);
    }
}