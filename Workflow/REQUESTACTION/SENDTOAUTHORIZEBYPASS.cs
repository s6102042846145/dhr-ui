using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.WORKFLOW.AUTHORIZATION;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOAUTHORIZEBYPASS : IRequestAction
    {
        public SENDTOAUTHORIZEBYPASS()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            int AuthorizeModelID = -1;
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            if (Params.ContainsKey("AUTHORIZEMODEL"))
            {
                foreach (string strAuth in Params["AUTHORIZEMODEL"].ToString().Split(','))
                {
                    AuthorizeModelID = -1;
                    int.TryParse(strAuth, out AuthorizeModelID);

                    AuthorizationModel oModel = WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetModel(AuthorizeModelID);
                    Receipient rcp = WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetStepReceipient(oModel.AuthModelID, oReturn.AuthorizeStep);
                    oReturn.Receipients.Add(rcp);
                    if (oReturn.Receipients.Count > 0)
                        break;
                }
            }

            oReturn.GoToNextItem = true;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}