﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENTTOMULTIROLE : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            if (Params.ContainsKey("USERROLE"))
            {
                foreach (string adminrole in Params["USERROLE"].Split(','))
                {
                    Receipient rcp = new Receipient(ReceipientType.USERROLE, adminrole, Document.RequestorCompanyCode, Document.RequestID);
                    oReturn.Receipients.Add(rcp);
                }
            }

            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }
    }
}
