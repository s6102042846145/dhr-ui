using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOSPECIAL : IRequestAction
    {
        public SENDTOSPECIAL()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            string specialcode = "";
            if (Params.ContainsKey("SPECIALCODE"))
            {
                specialcode = Params["SPECIALCODE"];
            }
            RequestActionResult oReturn = new RequestActionResult();
            Receipient rcp = new Receipient(ReceipientType.SPECIAL, specialcode, Document.RequestorCompanyCode, Document.RequestID);
            oReturn.Receipients.Add(rcp);
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}