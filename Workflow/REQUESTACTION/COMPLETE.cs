using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class COMPLETE : IRequestAction
    {
        public COMPLETE()
        {
        }

        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = true;
            oReturn.IsWaitForEdit = false;
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}