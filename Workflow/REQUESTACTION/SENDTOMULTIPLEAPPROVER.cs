﻿using PTTCHEM.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOMULTIPLEAPPROVER : IRequestAction
    {
        public RequestActionResult DoAction(RequestDocument Document, EMPLOYEE.EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            List<RequestFlowMultipleReceipient> RequestFlowMultipleReceipientList = new List<RequestFlowMultipleReceipient>(); 
            RequestFlowMultipleReceipient oRequestFlowMultipleReceipient;
            if (Params.ContainsKey("MULTIPLEAPPROVER"))
            {
                foreach (string item in Params["MULTIPLEAPPROVER"].Split('|')) //Group1:EmployeeID1,EmployeeID2|Group2:EmployeeID3,EmployeeID4
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        foreach (string approver_id in item.Split(':')[1].Split(','))
                        {
                            Receipient rcp = new Receipient(ReceipientType.EMPLOYEE, approver_id, Document.RequestorCompanyCode, Document.RequestID);
                            oReturn.Receipients.Add(rcp);

                            if (!RequestFlowMultipleReceipientList.Exists(obj => obj.RequestID == Document.RequestID && obj.ApproverID == approver_id && obj.ApproverCompanyCode == Document.RequestorCompanyCode && obj.ApproverGroup == item.Split(':')[0]))
                            {
                                oRequestFlowMultipleReceipient = new RequestFlowMultipleReceipient();
                                oRequestFlowMultipleReceipient.RequestID = Document.RequestID;
                                oRequestFlowMultipleReceipient.ApproverID = approver_id;
                                oRequestFlowMultipleReceipient.ApproverCompanyCode = Document.RequestorCompanyCode;
                                oRequestFlowMultipleReceipient.ApproverGroup = item.Split(':')[0];
                                RequestFlowMultipleReceipientList.Add(oRequestFlowMultipleReceipient);
                            }
                        }
                    }
                }
            }

            Document.RequestFlowMultipleReceipientList = RequestFlowMultipleReceipientList;
            oReturn.RequestFlowMultipleReceipientList = Document.RequestFlowMultipleReceipientList;
            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }
    }
}
