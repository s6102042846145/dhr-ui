using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOCURRENTCUSTODIAN : IRequestAction
    {
        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();

            if (IsSimulation)
            {
                Receipient rcp = new Receipient(ReceipientType.CUSTODIAN, Params["PETTYCODE"], Document.RequestorCompanyCode, Document.RequestID);
                oReturn.Receipients.Add(rcp);
              
            }
            else
            {
                Receipient rcp = new Receipient(ReceipientType.EMPLOYEE, ActionBy.EmployeeID, ActionBy.CompanyCode, Document.RequestID);
                oReturn.Receipients.Add(rcp);
            }

            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}