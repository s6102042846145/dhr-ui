using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.REQUESTACTION
{
    public class SENDTOROLE : IRequestAction
    {
        #region IRequestAction Members

        public RequestActionResult DoAction(RequestDocument Document, EmployeeData ActionBy, Dictionary<string, string> Params, bool IsSimulation)
        {
            RequestActionResult oReturn = new RequestActionResult();
            if (Params.ContainsKey("USERROLE"))
            {
                foreach (string adminrole in Params["USERROLE"].Split(','))
                {
                    Receipient rcp = new Receipient(ReceipientType.USERROLE, adminrole, Document.RequestorCompanyCode, Document.RequestID);
                    oReturn.Receipients.Add(rcp);
                }
            }

            oReturn.AuthorizeStep = Document.AuthorizeStep;
            oReturn.GoToNextItem = true;
            oReturn.IsCancelled = false;
            oReturn.IsCompleted = false;
            oReturn.IsWaitForEdit = false;
            return oReturn;
        }

        #endregion IRequestAction Members
    }
}