using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.INTERFACE;

namespace ESS.WORKFLOW
{
    public class ServiceManager
    {

        #region Constructor
        private ServiceManager()
        {

        }
        #endregion 

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();
        public string CompanyCode { get; set; }

        public static string ModuleID =  "ESS.WORKFLOW";
        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new ServiceManager()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        #region " privatedata "
        private string MODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MODE");
            }
        }

        private string MAILMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAILMODE");
            }
        }

        internal string EXCHANGESERVER
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "EXCHANGESERVER");
            }
        }

        //private static Type GetService(string ServiceName,string Mode)
        //{
        //    Type oReturn = null;
        //    Assembly oAssembly;
        //    string assemblyName = string.Format("ESS.{0}.{1}", ServiceName.ToUpper(), Mode.ToUpper());
        //    string typeName = string.Format("ESS.{1}.{0}.{1}ServiceImpl", Mode.ToUpper(), CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ServiceName.ToLower()));
        //    oAssembly = Assembly.Load(assemblyName);
        //    oReturn = oAssembly.GetType(typeName,true);
        //    return oReturn;
        //}
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.WORKFLOW.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.WORKFLOW.{0}.{1}", Mode.ToUpper(), ClassName);//CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "

        internal IWorkflowService WorkflowService
        {
            get
            {
                Type oType = GetService(MODE, "WorkflowServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IWorkflowService)Activator.CreateInstance(oType,CompanyCode);
                }
            }
        }

        internal IWorkflowService MailService
        {
            get
            {
                Type oType = GetService(MAILMODE, "WorkflowServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IWorkflowService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
    }
}