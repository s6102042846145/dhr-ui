using System;
using System.Data;
using ESS.DATA.FILE;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW.INTERFACE
{
    public interface IRequestViewer
    {
        void SetData(RequestDocument request);

        void LoadData(EmployeeData Requestor, DataSet ds, DateTime documentDate);

        bool ProcessError(Exception e);

        void SetAttachment(FileAttachmentSet fileSet);

        bool HasAttachment
        {
            get;
        }

        bool NeedSave
        {
            get;
        }
    }
}