using System;
using System.Collections.Generic;
using System.Data;
using ESS.DATA.FILE;
using ESS.EMPLOYEE;
using ESS.MAIL.DATACLASS;
using ESS.WORKFLOW.AUTHORIZATION;
using PTTCHEM.WORKFLOW;
using ESS.DELEGATE.DATACLASS;

namespace ESS.WORKFLOW.INTERFACE
{
    public interface IWorkflowService
    {
        #region " Flow "

        bool CanApproveCoWorkflow(string RequestNo, string ActionBy, string ActionByPosition);
        List<Flow> GetAllFlows();

        Flow GetFlow(int FlowID);

        List<FlowItem> GetFlowItems(int FlowID);

        FlowItem GetFlowItem(int FlowID, int FlowItemID);

        List<State> GetAllStates();

        State GetState(int StateID);

        ReceipientList GetStateResponse(int StateID);

        List<FlowItemAction> GetAllPossibleAction(int FlowID, int FlowItemID);

        ActionType GetActionType(int ActionTypeID);

        List<ActionType> GetAllActionTypes();

        List<ActionTypeParam> GetActionTypeParameters(int ActionTypeID);

        ReceipientList GetActionTypeReceipients(int ActionTypeID);

        ReceipientList GetSpecialReceipient(string EmployeeID, string Position, string Code, DateTime CheckDate);

        List<FlowParameter> GetFlowParameter(int FlowID);

        FlowParameter GetMappingParameter(int FlowID, int FlowItemID, int FlowItemActionID, int ParamID,string NextItemCode);

        FlowParameter GetMappingAutomaticParameter(int FlowID, int FlowItemID, int FlowItemActionID, int ParamID, string NextItemCode);

        #endregion " Flow "

        #region " RequestType "
        List<RequestDocument> GetRequestDocumentByRequestTypeID(int RequestTypeID);

        RequestType GetRequestType(int RequestTypeID);

        RequestType GetRequestType(int RequestTypeID, DateTime CheckDate, string LanguageCode);

        List<UserRoleResponseSetting> GetRequestTypeResponse(int RequestTypeID);

        List<RequestType> GetAllRequestTypes();

        List<RequestTypeSchema> GetRequestTypeSchemas(int RequestTypeID, int VersionID);

        List<RequestTypeKey> GetRequestTypeKeys(int RequestTypeID, int VersionID);

        List<RequestTypeSetting> GetRequestTypeSettings(int RequestTypeID, int VersionID);

        RequestTypeSetting GetRequestTypeSetting(int RequestTypeID, int VersionID, string FlowKey);

        RequestTypeSetting GetRequestTypeSettingByFlowID(int RequestTypeID, int VersionID, int FlowID);

        List<RequestTypeParam> GetRequestTypeParameters(int RequestTypeID, int VersionID, int KeyID);

        int CalculateFlow(int RequestTypeID, int VersionID, string FlowKey);

        ReceipientList GetRequestTypeOwner(int RequestTypeID);

        #endregion " RequestType "

        #region " Request "

        string GetLastedCommentByActionCode(int RequestID,string ActionCode);
        RequestFlow GetLastProcess(string RequestNo);

        List<RequestFlow> GetPassedProcess(string RequestNo);

        List<RequestFlow> SimulateProcess(RequestDocument Document);
        List<RequestFlowForwardSnapshot> SimulateForwardProcess(RequestDocument Document);

        RequestActionResult ProcessRequest(RequestDocument requestDocument, FlowItemAction action);
        RequestActionResult ProcessRequest(RequestDocument requestDocument, FlowItemAction action, EmployeeData Requestor); //AddBy: Ratchatawan W. (2012-09-06)

        void UpdateDocument(RequestDocument requestDocument);

        void UpdateDocument(RequestDocument requestDocument, FlowItemAction action);

        void SaveData(RequestDocument requestDocument, DataTable Header, Object Additional);

        void SaveDataByPass(RequestDocument requestDocument, DataTable Header, Object Additional);

        RequestDocument LoadDocument(string RequestNo, string KeyMaster);

        RequestDocument LoadDocument(string RequestNo);


        RequestDocument LoadDocument(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView);

        RequestDocument LoadDocument(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView, EmployeeData Requestor);//AddBy: Ratchatawan W. (2012-09-06)


        Object LoadAdditionalData(string RequestNo, int RequestID, int RequestTypeID,int RequestTypeVersionID, int DocversionID);

        DataTable LoadInfoData(string RequestNo, int RequestID, int RequestTypeID, int RequestTypeVersionID, int DocversionID);

        string GetDisplayName(ReceipientType type, string code);

        #endregion " Request "

        #region " Application "

        List<RequestApplication> GetAuthorizeApplications();

        List<ApplicationSubject> GetAuthorizeApplicationSubject(int ApplicationID);

        List<UserRoleResponseSetting> GetApplicationSubjectResponse(int SubjectID);

        ApplicationSubject LoadApplicationSubject(int SubjectID);

        List<string> LoadApplicationSubjectAuthorize(int SubjectID);

        #endregion " Application "

        #region " Box "

        List<RequestBox> GetAuthorizeBoxes(string BoxCategoryCode);
        List<RequestBox> GetAuthorizeBoxes(string BoxCategoryCode, string EmployeeID);
        List<RequestDocument> GetRequestDocumentList(string BoxCode);
        List<RequestDocument> GetRequestDocumentList(string BoxCode, int ItemPerPage, int Page, RequestBoxFilter Filter);
        //AddBy: Ratchatawan W. (2011-09-08)
        List<RequestDocument> GetRequestDocumentList(string BoxCode, string EmployeePositionID, int ItemPerPage, int Page, RequestBoxFilter Filter);
        //AddBy: Ratchatawan W. (2011-09-09)

        int CountItemInbox(string BoxCode, RequestBoxFilter Filter);

        List<RequestType> GetAvailableRequestType(string BoxCode);

        RequestBox GetRequestBox(string BoxCode);

        #endregion " Box "

        string GetMailTemplateByID(int MailID, string LanguageCode);

        string GetMailSubjectByID(int MailID, string LanguageCode);

        string GetRequestText(string Category, string Language, string Code);

        List<AuthorizationModel> GetAllAuthorizationModel();

        List<AuthorizationModelKey> GetAuthorizationModelKey(int ModelID);

        List<AuthorizationModelSet> GetAuthorizationModelSet(int ModelID);

        List<AuthorizationModelSetCondition> GetAllAuthorizationModelCondition(int ModelID, int SetID);

        AuthorizationModel GetAuthorizationModel(int ModelID);

        Receipient GetAuthorizationModelStep(int ModelID, int StepID);

        bool CheckIsInRole(string EmployeeID, Receipient RCP, string Requestor, DateTime CheckDate);

        List<AuthorizationModelSet> CalculateAuthorize(int ModelID, List<AuthorizationModelKey> list, List<object> values);

        //ModifiedBy: Ratchatawan W. (2012-01-25)
        //List<EmployeeData> GetUserResponse(string Role, string AdminGroup);
        List<EmployeeData> GetUserResponse(string Role, EmployeeData emp);

        //AddBy: Ratchatawan W. (2012-12-27)
        RequestDocument LoadDocumentReadonly(string RequestNo, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView);

        RequestDocument LoadDocumentWithOutCheckAuthorize(string strRequestNo);

        #region " FileAttachment "

        void SaveFileAttachment(FileAttachmentSet fileSet);

        FileAttachmentSet LoadFileAttachment(int FileSetID);

        #endregion " FileAttachment "

        List<RequestFlag> GetRequestFlag(string RequestNo);

        void SaveRequestFlag(List<RequestFlag> lstFlag);

        EmployeeData GetProjectManager(string ProjectRole);

        void GenerateMailTo(Dictionary<int, List<WorkflowMailTo>> list, Receipient rcp, bool IsCC, RequestDocument Document, int TemplateMailID);
        List<string> GetAllAdminGroup();

        //CHAT 2011-10-14
        FileAttachmentSet GetRequestTypeFileSet(string employeeID, int requestTypeID, string requestSubType);

        FileAttachmentSet LoadMasterFileAttachment(EmployeeData employeeData, int requestTypeID, string subType);

        //CHAT 2011-10-14

        List<string> GetResponseCode(string Role, EmployeeData Emp);

        //AddBy: Ratchatawan W. (2012-02-27)
        List<string> GetResponseCodeByOrganizationOnly(string Role, EmployeeData Emp, string RootOrg);

        //AddBy: Ratchatawan W. (2012-04-24)

        #region " Delegate "

        List<EmployeeData> GetDelegateToByEmployeePosition(string EmployeeID,string PositionID);
        List<DelegateHeader> GetDelegateDataByCriteria(string EmployeeID, int Month, int Year);

        bool CheckDelegateAuthorize(int RequestID);

        bool CheckDelegateAuthorize(string RequestNo);

        DataTable GetRequestInDelegateBox(RequestBoxFilter Filter);

        string SaveDelegateHeader(DelegateHeader header);
        void DeleteDelegateHeader(DelegateHeader header);

        #endregion " Delegate "

        //AddBy: Ratchatawan W. (2012-04-30)
        bool GetAuthorizeMassApprove(string BoxCode, string EmployeeID, string EmployeePositionID);

        void CreateTicket(string TicketID, string RequestNo, string TicketFor, string TicketForCompanyCode, bool IsExternalUser);

        void UpdateRequestTypeFileSet(FileAttachmentSet NewFileSet);

        #region For Mobile
        int CountRequestBox(int BoxID, string EmployeeID);


        bool GetAuthorizeMassApproveForMobile(int BoxID, string EmployeeID, string EmployeePositionID);

        void RecallUpdateDocument(string RequestNo, string ActionBy, string ActionByPosition);

        string GetRequestTextOniService3(string Category, string Language, string Code);

        void UpdateRequestTypeSummarySnapshot(List<RequestDocument> oRequestDocumentList, int RequestTypeID);

        #endregion

        void RequestTypeSummarySave(int RequestID, int ItemID, string SummaryTextTH, string SummaryTextEN);

        DataTable GetRequestInDelegateBox();

        DataSet GetRequestDocumentList(int BoxID, EmployeeData Employee, string Filter);
        RequestDocument CreateRequest(EmployeeData Requestor, int RequestTypeID, Dictionary<string, string> Params);
        string GetRequestNo(DateTime CreatedDate, string RequestTypePrefix, string ReferRequestNo);
        string GetMailTemplate(int FlowID, int FlowItemID, int ID, string LanguageCode);
        string GetMailSubject(int FlowID, int FlowItemID, int ID, string LanguageCode);

        List<RequestFlowMultipleReceipient> GetRequestFlowMultipleReceipientList(int RequestID);

        List<RequestFlowMultipleReceipient_External> GetRequestFlowMultipleReceipient_ExternalList(int RequestID);

        List<FlowItemAction> GetAllPossibleAction(int FlowID, int FlowItemID, RequestDocument Doc, EmployeeData ActionBy);

        RequestBox GetRequestBox(int iBoxID);

        void ResolveReceipientToOtherCompany(DataRow dr);
        DataTable GetUserRoleResponseForOtherCompany(string EmployeeID, string CompanyCode);

        DataTable GetFlowItemActionPreference();

        EmployeeData GetLastedActionByActionCode(int RequestID,string ActionCode);

        EmployeeData GetLastedActionByActionCode(string RequestNo, string ActionCode);
        EmployeeData GetLastedManagerByRequestNo(string RequestNo);

        DataTable GetRequestTypeKeySetting(int RequestTypeID);

        List<Receipient> GetAllActionInPass(int RequestID, string EmployeeID);

        DataTable ValidateTicket(string TicketID, string EmployeeID);
        DataTable ValidateTicketForExternalUser(string TicketID, string ExternalUserID);

        DataTable GetAllRequestType(string LanguageCode);

        DataTable GetDelegationInfo(int RequestID);

        void DeleteRequestFlowForwardSnapshotByRequestID(int RequestID);
        void SaveRequestFlowForwardSnapshot(List<RequestFlowForwardSnapshot> ForwardFlowList);

        string GetLastedActionByEmployeeID(int RequestID,string EmployeeID,string LanguageCode);
        DataTable GetActionInPassByRequestNo(string RequestNo);

        DataTable GetRecipientByItemID(int RequestID, int ItemID);

        string GetEditReasonByRequestNo(string RequestNo);

        string GetAuthorizeBoxByEmployeeID(string EmployeeID);

        void ResponseFlagSave(string RequestNo, string TransactionType, string ServiceURL, DateTime TransactionStartDate, DateTime ServiceFinishDate, string ServiceResult, string ServiceException, DateTime DocumentFinishDate, string DocumentException);

        bool CheckResponseFlag(string RequestNo);
        DataSet GetPositionApprove(int BoxID, string EmployeeID, string PositionID, string Language);

        DataTable GetEmployeeByUserRole(int RequestID, string UserRole, string Language);
    }
}