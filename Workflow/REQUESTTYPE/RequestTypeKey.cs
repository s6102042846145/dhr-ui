using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    public class RequestTypeKey : BaseCodeObject
    {
        private RequestTypeKeyCategory __category = RequestTypeKeyCategory.DATA;
        private int __length;

        public RequestTypeKey()
        {
        }

        public RequestTypeKeyCategory Category
        {
            get
            {
                return __category;
            }
            set
            {
                __category = value;
            }
        }

        public int Length
        {
            get
            {
                return __length;
            }
            set
            {
                __length = value;
            }
        }
    }
}