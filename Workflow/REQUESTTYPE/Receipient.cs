using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.SHAREDATASERVICE;
using System.Data;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class Receipient
    {
        #region Constructor
        public Receipient()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, Receipient> Cache = new Dictionary<string, Receipient>();
        //public string CompanyCode { get; set; }
        //public static string ModuleID
        //{
        //    get
        //    {
        //        return "ESS.WORKFLOW";
        //    }
        //}
        //public static Receipient CreateInstance(string oCompanyCode)
        //{
        //    if (!Cache.ContainsKey(oCompanyCode))
        //    {
        //        Cache[oCompanyCode] = new Receipient()
        //        {
        //            CompanyCode = oCompanyCode
        //            ,
        //            Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
        //        };
        //    }
        //    return Cache[oCompanyCode];
        //}
        #endregion MultiCompany  Framework


        //public Receipient(ReceipientType type, string code,string companyCode)
        //{
        //    this.Type = type;
        //    this.Code = code;
        //    this.CompanyCode = companyCode;
        //}
        public Receipient(ReceipientType type, string code, string companyCode,int requestID)
        {
            this.Type = type;
            this.Code = code;
            this.CompanyCode = companyCode;
            this.RequestID = requestID;
        }
        public ReceipientType Type { get; set; }
        public int RequestID { get; set; }
        public string Code { get; set; }
        public string CompanyCode { get; set; }

        DateTime _CheckDate = DateTime.Now;
        public DateTime CheckDate { get { return _CheckDate; } set { _CheckDate = value; } }
        public string DisplayName
        {
            get
            {
                string oReturn = "";
                EmployeeData oEmp = new EmployeeData();
                switch (this.Type)
                {
                    case ReceipientType.EMPLOYEE:
                        oEmp = new EmployeeData(Code, string.Empty, CompanyCode, CheckDate);
                        //oReturn = string.Format("{0}::{1}", oEmp.ActionOfPosition.Text, oEmp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        oReturn = string.Format("{0} {1}", oEmp.EmployeeID, oEmp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        break;

                    case ReceipientType.POSITION:
                        oEmp.CompanyCode = CompanyCode;
                        oEmp = oEmp.GetEmployeeByPositionID(Code,CheckDate, WorkflowPrinciple.Current.UserSetting.Language);
                        //oReturn = string.Format("{0}::{1}", oEmp.ActionOfPosition.Text, oEmp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        oReturn = string.Format("{0} {1}", oEmp.EmployeeID, oEmp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        break;

                    case ReceipientType.USERROLE:
                        if (WorkflowManagement.CreateInstance(this.CompanyCode).SHOW_USERROLE_IN_ROLEORNAME == "ROLE")
                            oReturn = RequestText.CreateInstance(CompanyCode).LoadText("USERROLENAME", WorkflowPrinciple.Current.UserSetting.Language, this.Code);
                        else
                        {
                            DataTable EmpInRole = WorkflowManagement.CreateInstance(this.CompanyCode).GetEmployeeByUserRole(RequestID, this.Code, WorkflowPrinciple.Current.UserSetting.Language);
                            if (EmpInRole.Rows.Count > 0)
                            {
                                foreach (DataRow dr in EmpInRole.Rows)
                                    oReturn += string.Format("{0} , ",dr["Name"].ToString());
                                oReturn = oReturn.TrimEnd(',');
                            }
                        }
                        break;

                    case ReceipientType.SPECIAL:
                        oReturn = this.Code;
                        break;

                    case ReceipientType.EXTERNAL_USER:
                        DataTable oUser = EmployeeManagement.CreateInstance(CompanyCode).GetExternalUserSnapshotbyRequestID(RequestID, int.Parse(Code));
                        //oEmp = new EmployeeData(Code, string.Empty, CompanyCode,DateTime.Now);
                        //oReturn = string.Format("{0}::{1}", oEmp.ActionOfPosition.Text, oEmp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        oReturn = string.Format("{0}", oUser.Rows[0]["Name"]);
                        break;
                    case ReceipientType.CUSTODIAN:
                        oReturn = RequestText.CreateInstance(CompanyCode).LoadText("USERROLENAME", WorkflowPrinciple.Current.UserSetting.Language, "CUSTODIAN");
                        break;
                }
                return oReturn;
                //return ServiceManager.WorkflowService.GetDisplayName(this.Type, this.Code);
            }
        }
        public string DisplayPositionName
        {
            get
            {
                string oReturn = "";
                EmployeeData oEmp = new EmployeeData();
                switch (this.Type)
                {
                    case ReceipientType.EMPLOYEE:
                        oEmp = new EmployeeData(Code, string.Empty, CompanyCode, CheckDate);
                        //oReturn = string.Format("{0}::{1}", oEmp.ActionOfPosition.Text, oEmp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        oReturn = string.Format("{0}", oEmp.CurrentPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        break;

                    //case ReceipientType.EXTERNAL_USER:
                    //    oReturn = RequestText.CreateInstance(CompanyCode).LoadText("SYSTEM", WorkflowPrinciple.Current.UserSetting.Language, "EXTERNAL_USER");
                    //    break;

                    case ReceipientType.POSITION:
                        oEmp.CompanyCode = CompanyCode;
                        oEmp = oEmp.GetEmployeeByPositionID(Code, CheckDate,WorkflowPrinciple.Current.UserSetting.Language);
                        //oReturn = string.Format("{0}::{1}", oEmp.ActionOfPosition.Text, oEmp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        oReturn = string.Format("{0}", oEmp.CurrentPosition.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                        break;
                }
                return oReturn;
            }
        }
        public bool IsCanAction(EmployeeData ActionBy, RequestDocument Document)
        {
            EmployeeData oEmp = new EmployeeData() { CompanyCode = ActionBy.CompanyCode };
            if (ActionBy == null)
                return false;
            bool oReturn = false;
            if (this.Type == ReceipientType.EMPLOYEE && this.Code == ActionBy.EmployeeID)
            {
                oReturn = true;
            }
            else if (this.Type == ReceipientType.USERROLE)
            {
                if (Document.SubmittedDate == DateTime.MinValue)
                {
                    oReturn = WORKFLOW.ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).WorkflowService.CheckIsInRole(ActionBy.EmployeeID, this, Document.RequestorNo, Document.CreatedDate);
                }
                else
                {
                    oReturn = WORKFLOW.ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).WorkflowService.CheckIsInRole(ActionBy.EmployeeID, this, Document.RequestorNo, Document.SubmittedDate);
                }
            }
            else if (this.Type == ReceipientType.POSITION && oEmp.GetEmployeeByPositionID(this.Code).EmployeeID == ActionBy.EmployeeID)
            {
                oReturn = true;
            }
            return oReturn;
        }

        public List<string> Email(string AdminGroup)
        {
            List<string> oReturn = new List<string>();
            switch (this.Type)
            {
                case ReceipientType.EMPLOYEE:
                    EmployeeData oEmp = new EmployeeData(this.Code);
                    oReturn.Add(oEmp.EmailAddress);
                    break;

                case ReceipientType.USERROLE:
                    List<EmployeeData> list = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetUserResponse(this.Code, AdminGroup);
                    foreach (EmployeeData oEmp1 in list)
                    {
                        oReturn.Add(oEmp1.EmailAddress);
                    }
                    break;
            }
            return oReturn;
        }

        internal List<EmployeeData> GetMailListByAdminGroup(string AdminGroup)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            switch (this.Type)
            {
                case ReceipientType.EMPLOYEE:
                    EmployeeData oEmp = new EmployeeData(this.Code);
                    oReturn.Add(oEmp);
                    break;
                case ReceipientType.USERROLE:
                    List<EmployeeData> list = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetUserResponse(this.Code, AdminGroup);
                    foreach (EmployeeData oEmp1 in list)
                    {
                        oReturn.Add(oEmp1);
                    }
                    break;
            }
            return oReturn;
        }
    }
}