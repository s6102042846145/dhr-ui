using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    public class FlowParameter : BaseCodeObject
    {
        private int __flowID = -1;
        private ParameterType __type;

        public FlowParameter()
        {
            this.ID = -1;
            this.Code = "";
            this.FlowID = -1;
        }

        public ParameterType Type
        {
            get
            {
                return __type;
            }
            set
            {
                __type = value;
            }
        }

        public int FlowID
        {
            get
            {
                return __flowID;
            }
            set
            {
                __flowID = value;
            }
        }

        public override int GetHashCode()
        {
            string cCode = string.Format("FlowParameter_{0}_{1}", this.FlowID, this.ID);
            return cCode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            try
            {
                return obj.GetHashCode() == this.GetHashCode();
            }
            catch
            {
                return false;
            }
        }
    }
}