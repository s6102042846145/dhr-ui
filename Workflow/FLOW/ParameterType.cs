namespace ESS.WORKFLOW
{
    public enum ParameterType
    {
        STRING,
        INTEGER,
        DATETIME,
        DECIMAL,
        BOOLEAN
    }
}