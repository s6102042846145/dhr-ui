using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class State : BaseCodeObject
    {
        #region Constructor
        public State()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, State> Cache = new Dictionary<string, State>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static State CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new State()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            State oState = new State()
            {
                CompanyCode = oCompanyCode
            };
            return oState;
        }
        #endregion MultiCompany  Framework


        public bool IsEndPoint { get; set; }
        public bool IsCanAction { get; set; }

        public override int ID { get; set; }

        public ReceipientList StateResponse
        {
            get
            {
                //if (__stateResponse == null)
                //{
                return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetStateResponse(this.ID);
                //}
                //return __stateResponse;
            }
        }

        public override bool Equals(object obj)
        {
            try
            {
                return ((State)obj).ID == this.ID;
            }
            catch
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return ("State" + this.ID.ToString()).GetHashCode();
        }

        public List<State> GetAllStates()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAllStates();
        }

        public State GetState(int StateID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetState(StateID);
        }
    }
}