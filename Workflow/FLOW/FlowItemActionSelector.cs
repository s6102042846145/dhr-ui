﻿using ESS.WORKFLOW.ABSTRACT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class FlowItemActionSelector : BaseCodeObject
    {
        #region Constructor
        private FlowItemActionSelector()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, FlowItem> Cache = new Dictionary<string, FlowItem>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static FlowItemActionSelector CreateInstance(string oCompanyCode)
        {
            FlowItemActionSelector oFlowItemActionSelector = new FlowItemActionSelector()
            {
                CompanyCode = oCompanyCode
            };
            return oFlowItemActionSelector;
        }
        #endregion MultiCompany  Framework

        public int FlowID { get; set; }
        public int FlowItemID { get; set; }
        public int FlowItemActionID { get; set; }
        public string FlowItemActionCode { get; set; }
        public string ActionSelector{get;set;}
        public string VisibilityCondition { get; set; }
        public bool IsVisible { get; set; }
    }
}
