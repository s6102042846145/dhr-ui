using System;
using ESS.DATA.INTERFACE;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class EditAction : FlowItemAction
    {
        public EditAction()
        {
        }

        #region IActionData Members

        public string Code
        {
            get
            {
                return "EDIT";
            }
            set
            {
            }
        }

        #endregion IActionData Members
    }
}