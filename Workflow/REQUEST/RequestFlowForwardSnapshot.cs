using System;
using System.Collections.Generic;

namespace ESS.WORKFLOW
{
    public class RequestFlowForwardSnapshot
    {
        public RequestFlowForwardSnapshot()
        {
        }

        public int RowID { get; set; }
        public int RequestID { get; set; }
        public int ItemID { get; set; }
        public string LanguageCode { get; set; }
        public string ReceipientCode { get; set; }
        public string ReceipientName { get; set; }
        public string ReceipientPosition { get; set; }
        public string ReceipientCompanyCode { get; set; }
        public string ReceipientPositionName { get; set; }
        public string ActionCode { get; set; }
        public string ActionText { get; set; }
        public bool IsCurrentItem { get; set; }
        public bool IsEndPoint { get; set; }
        public string GroupName { get; set; }
        public bool IsSystem { get; set; }
        
    }
}