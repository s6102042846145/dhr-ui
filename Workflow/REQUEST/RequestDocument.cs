using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;
using ESS.DATA.EXCEPTION;
using ESS.DATA.FILE;
using ESS.DATA.INTERFACE;
using ESS.EMPLOYEE;
using ESS.FILE;
using ESS.MAIL.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using PTTCHEM.WORKFLOW;
using System.Xml.Serialization;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class RequestDocument : AbstractObject
    {
        #region " Members "

        #region " Header "

        private string __requestno;
        private int __requestTypeID;
        private string __requestorno;
        private string __requestorposition;

        [NonSerialized()]
        private EmployeeData __requestor;

        [NonSerialized()]
        private EmployeeData __creator;

        private string __creatorno;
        private string __creatorposition;
        private string __authorizeDelegate = "";
        private DateTime __createddate;
        private DateTime __submitdate = DateTime.MinValue;
        private DateTime __documentdate = DateTime.MinValue;
        private string __keyMaster;
        private string __flowKey = null;
        private bool __isCompleted = false;
        private bool __isCancelled = false;
        private bool __isWaitForEdit = false;
        private bool __isOwnerView = false;
        private bool __isDataOwnerView = false;
        private DateTime __lastActionDate = DateTime.MinValue;
        private bool __isReadOnly = false;
        private bool __hasError = false;
        private string __errorText = "";
        private List<RequestFlowMultipleReceipient> __RequestFlowMultipleReceipientList;
        private List<RequestFlowMultipleReceipient_External> __RequestFlowMultipleReceipient_ExternalList;
        private List<RequestFlowForwardSnapshot> __RequestFlowForwardSnapshotList;
        private DataTable __historyList;
        WorkflowManagement oWorkflowManagement;
        private bool _isRunSaveExternal = true;

        #endregion " Header "

        #region " Flows & Data "

        private string __newComment;
        private string __newComment2;
        private int __fileSetID = -1;
        private FileAttachmentSet __fileSet = null;
        private int __flowID = -1;
        private int __currentFlowItemID = -1;
        private int __currentItemID = -1;
        private int __authorizeStep = -1;
        public int DocversionID { get; set; }

        [NonSerialized()]
        private Flow __flow;

        [NonSerialized()]
        private FlowItem __currentFlowItem;

        [NonSerialized()]
        private RequestType __rt;

        private DataTable __data;
        private ReceipientList __receipients = new ReceipientList();
        private ReceipientList __receipients_bak = new ReceipientList();
        private Object __additional;
        private string __groupName = "";

        #endregion " Flows & Data "

        [NonSerialized()]
        private IDataService __dataService = null;

        #endregion " Members "

        public event RequestTextSolverHandler ResolveText;

        #region Constructor
        public RequestDocument()
        {
           
        }
        #endregion

        #region MultiCompany  Framework
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static RequestDocument CreateInstance(string oCompanyCode)
        {
            RequestDocument oRequestDocument = new RequestDocument()
            {
                CompanyCode = oCompanyCode
            };
            return oRequestDocument;
        }
        #endregion MultiCompany  Framework

        public RequestDocument(bool ReadOnly)
        {
            __isReadOnly = ReadOnly;
        }

        #region " Properties "

        #region " RequestNo "

        public string RequestNo
        {
            get
            {
                return __requestno;
            }
            set
            {
                __requestno = value;
            }
        }

        #endregion " RequestNo "

        #region " RequestorNo "

        public string RequestorNo
        {
            get
            {
                return __requestorno;
            }
            set
            {
                __requestorno = value;
            }
        }

        #endregion " RequestorNo "

        #region " RequestorPosition "

        public string RequestorPosition
        {
            get
            {
                return __requestorposition;
            }
            set
            {
                __requestorposition = value;
            }
        }

        #endregion " RequestorPosition "

        #region " Requestor "

        public EmployeeData Requestor
        {
            get
            {
                if (__requestor == null)
                {
                    __requestor = new EmployeeData(__requestorno,__requestorposition, this.DocumentDate);
                    __requestor.CompanyCode = CompanyCode;
                    __requestor.GetAllPositions(WorkflowPrinciple.Current.UserSetting.Language);
                }
                return __requestor;
            }
            set
            {
                __requestor = value;
            
            }
        }

        #endregion " Requestor "

        #region " CreatorNo "

        public string CreatorNo
        {
            get
            {
                return __creatorno;
            }
            set
            {
                __creatorno = value;
            }
        }

        #endregion " CreatorNo "

        #region " CreatorPosition "

        public string CreatorPosition
        {
            get
            {
                return __creatorposition;
            }
            set
            {
                __creatorposition = value;
            }
        }

        #endregion " CreatorPosition "

        #region " AuthorizeDelegate "

        public string AuthorizeDelegate
        {
            get
            {
                return __authorizeDelegate;
            }
            set
            {
                __authorizeDelegate = value;
            }
        }

        #endregion " AuthorizeDelegate "

        #region " Creator "

        public EmployeeData Creator
        {
            get
            {
                if (__creator == null)
                {
                    __creator = new EmployeeData(__creatorno,__creatorposition,this.DocumentDate);
                    __creator.CompanyCode = CompanyCode;
                }
                return __creator;
            }
            set
            {
                __creator = value;
            }
        }

        #endregion " Creator "

        #region " CreatedDate "

        public DateTime CreatedDate
        {
            get
            {
                return __createddate;
            }
            set
            {
                __createddate = value;
            }
        }

        #endregion " CreatedDate "

        #region " SubmitDate "

        public DateTime SubmittedDate
        {
            get
            {
                return __submitdate == DateTime.MinValue ? this.CreatedDate : __submitdate;
            }
            set
            {
                __submitdate = value;
            }
        }

        #endregion " SubmitDate "

        #region " DocumentDate "

        public DateTime DocumentDate
        {
            get
            {
                return __documentdate == DateTime.MinValue ? this.CreatedDate : __documentdate;
            }
            set
            {
                __documentdate = value;
                __requestor = null;
            }
        }

        #endregion " DocumentDate "

        #region " IsReadOnly "

        public bool IsReadOnly
        {
            get
            {
                return __isReadOnly;
            }
        }

        #endregion " IsReadOnly "

        #region " IsCompleted "

        public bool IsCompleted
        {
            get
            {
                return __isCompleted;
            }
            set
            {
                __isCompleted = value;
            }
        }

        #endregion " IsCompleted "

        #region " IsCancelled "

        public bool IsCancelled
        {
            get
            {
                return __isCancelled;
            }
            set
            {
                __isCancelled = value;
            }
        }

        #endregion " IsCancelled "

        #region " IsWaitForEdit "

        public bool IsWaitForEdit
        {
            get
            {
                return __isWaitForEdit;
            }
            set
            {
                __isWaitForEdit = value;
            }
        }

        #endregion " IsWaitForEdit "

        #region " IsOwnerView "

        public bool IsOwnerView
        {
            get
            {
                return __isOwnerView;
            }
            set
            {
                __isOwnerView = value;
            }
        }

        #endregion " IsOwnerView "

        #region " IsDataOwnerView "

        public bool IsDataOwnerView
        {
            get
            {
                return __isDataOwnerView;
            }
            set
            {
                __isDataOwnerView = value;
            }
        }

        #endregion " IsDataOwnerView "

        #region " HasFileAttached "

        public bool HasFileAttached
        {
            get
            {
                return (this.__fileSet != null && this.__fileSet.Count > 0) || this.FileSetID > -1;
            }
        }

        #endregion " HasFileAttached "

        #region " Data "

        public DataTable Data
        {
            get
            {
                if (__data == null)
                {
                    DataTable __myTable = new DataTable();
                    __myTable = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.LoadInfoData(this.RequestNo,this.RequestID,this.RequestTypeID,this.RequestType.VersionID,this.DocversionID);
                    if (__myTable.Rows.Count == 0)
                    {
                        __myTable.Rows.Add(__myTable.NewRow());
                    }
                    __data = this.RequestType.GenerateDataTable();
                    if (__data.Rows.Count == 0)
                    {
                        __data.Rows.Add(__data.NewRow());
                    }
                    foreach (DataColumn oDC in __data.Columns)
                    {
                        if (__myTable.Columns.Contains(oDC.ColumnName))
                        {
                            __data.Rows[0][oDC] = __myTable.Rows[0][oDC.ColumnName];
                        }
                    }
                }
                return __data;
            }
            set { __data = value; }
        }

        #endregion " Data "


        #region " Additional "

        public Object Additional
        {
            get
            {
                if (__additional == null)
                {
                    __additional = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.LoadAdditionalData(this.RequestNo, this.RequestID, this.RequestTypeID, this.RequestType.VersionID, this.DocversionID);
                    __dataService.PrepareData(this.Requestor, __additional);
                }
                return __additional;
            }
            set
            {
                __additional = value;
            }

        }

        #endregion " Additional "

        public DataTable HistoryList
        {
            get
            {
                if (__historyList == null)
                {
                    __historyList = __dataService.GenerateHistory(this.RequestNo, this.Requestor);
                }
                return __historyList;
            }
            set
            {
                __historyList = value;
            }
        }


        #region " KeyMaster "

        public string KeyMaster
        {
            get
            {
                return __keyMaster;
            }
            set
            {
                __keyMaster = value;
            }
        }

        #endregion " KeyMaster "

        #region " RequestTypeID "

        public int RequestTypeID
        {
            get
            {
                return __requestTypeID;
            }
            set
            {
                __requestTypeID = value;
            }
        }

        #endregion " RequestTypeID "

        #region " RequestType "
        public RequestType RequestType
        {
            get
            {
                if (__rt == null)
                {
                    if (this.SubmittedDate != DateTime.MinValue)
                    {
                        __rt = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GetRequestType(this.RequestTypeID, this.SubmittedDate,WorkflowPrinciple.Current.UserSetting.Language);
                    }
                    else
                    {
                        __rt = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GetRequestType(this.RequestTypeID, this.CreatedDate, WorkflowPrinciple.Current.UserSetting.Language);
                    }
                }
                return __rt;
            }
        }

        #endregion " RequestType "

        #region " FlowKey "

        //public string FlowKey
        //{
        //    get
        //    {
        //        //if (Data == null)
        //        //{
        //        //    throw new Exception("please set data before");
        //        //}
        //        //return this.getFlowKey(Data);
        //        return __dataService.GenerateFlowKey(this.Requestor, this.Data);
        //    }
        //}
        public string FlowKey
        {
            get
            {
                if (__flowKey == null && this.Data != null)
                {
                    __flowKey = __dataService.GenerateFlowKey(this.Requestor, this.Creator, this.Data);
                }
                return __flowKey;
            }
        }

        #endregion " FlowKey "

        #region " FlowID "

        public int FlowID
        {
            get
            {
                return __flowID;
            }
            set
            {
                if (__flow != null && __flow.ID != __flowID)
                {
                    __flow = null;
                }
                __flowID = value;
            }
        }

        #endregion " FlowID "

        #region " CurrentFlowItemID "
        [XmlElement("FlowItemID")]

        public int CurrentFlowItemID
        {
            get
            {
                return __currentFlowItemID;
            }
            set
            {
                __currentFlowItemID = value;
            }
        }

        public int CurrentFlowItemStateID
        {
            get
            {
                return this.CurrentFlowItem.StateID;
            }

        }

        #endregion " CurrentFlowItemID "

        #region " CurrentItemID "
        [XmlElement("ItemID")]
        public int CurrentItemID
        {
            get
            {
                return __currentItemID;
            }
            set
            {
                __currentItemID = value;
                __additional = null;
            }
        }

        #endregion " CurrentItemID "

        #region " FileSetID "

        public int FileSetID
        {
            get
            {
                return __fileSetID;
            }
            set
            {
                __fileSetID = value;
            }
        }

        #endregion " FileSetID "

        #region " AuthorizeStep "

        public int AuthorizeStep
        {
            get
            {
                return __authorizeStep;
            }
            set
            {
                __authorizeStep = value;
            }
        }

        #endregion " AuthorizeStep "

        #region " Flow "

        public Flow Flow
        {
            get
            {
                if (__flow == null)
                {
                    __flow = Flow.CreateInstance(CompanyCode).GetFlow(this.FlowID);
                }
                return __flow;
            }
        }

        #endregion " Flow "

        #region " CurrentFlowItem "

        public FlowItem CurrentFlowItem
        {
            get
            {
                //if (this.IsOwnerView && (WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == this.CreatorNo) || (WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == this.RequestorNo))
                //{
                //    return this.Flow.OwnerView;
                //}
                if ((__currentFlowItem == null || __currentFlowItem.FlowID != this.FlowID || __currentFlowItem.ID != this.CurrentFlowItemID) && this.FlowID != -1 && this.CurrentFlowItemID != -1)
                {
                    __currentFlowItem = FlowItem.CreateInstance(this.RequestorCompanyCode).GetFlowItem(this.FlowID, this.CurrentFlowItemID);
                }
                return __currentFlowItem;
            }
            set
            {
                __currentFlowItem = value;
            }
        }

        #endregion " CurrentFlowItem "

        #region " Receipients "

        public ReceipientList Receipients
        {
            get
            {
                return __receipients;
            }
            set
            {
                __receipients = value;
            }
        }

        #endregion " Receipients "

        #region " BackupReceipients "

        public ReceipientList BackupReceipients
        {
            get
            {
                return __receipients_bak;
            }
            set
            {
                __receipients_bak = value;
            }
        }

        #endregion " BackupReceipients "

        #region " CalculateFlowID "

        private int CalculateFlowID(DataTable Info)
        {
            return ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.CalculateFlow(this.RequestTypeID, this.RequestType.VersionID, this.getFlowKey(Info));
        }

        private int CalculateFlowID(string FlowKey)
        {
            return ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.CalculateFlow(this.RequestTypeID, this.RequestType.VersionID, FlowKey);
        }

        #endregion " CalculateFlowID "

        #region " SummaryText "

        public string SummaryText
        {
            get
            {
                Regex regex = new Regex(@"{(#)?(?<DataField>\w+)(:(?<DataFormat>(#)?\w+))?}");
                string cReturn = "";
                try
                {
                    cReturn = RequestType.SummaryText;
                }
                catch
                {
                }
                foreach (Match item in regex.Matches(RequestType.SummaryText))
                {
                    RequestTextSolverEventArgs args;
                    if (item.Value.StartsWith("{#"))
                    {
                        PropertyInfo prop = typeof(RequestDocument).GetProperty(item.Groups["DataField"].Value, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, prop.GetValue(this, null));
                    }
                    else
                    {
                        string cColName = item.Groups["DataField"].Value;
                        if (this.Data.Rows.Count > 0 && this.Data.Columns.Contains(cColName))
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, this.Data.Rows[0][cColName]);
                        }
                        else
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, "");
                        }
                    }
                    if (args.Value != null)
                    {
                        if (ResolveText != null)
                        {
                            ResolveText(args);
                        }
                        if (!args.IsHandler)
                        {
                            if (args.Value.GetType() == typeof(DateTime))
                            {
                                DateTime myDate = (DateTime)args.Value;
                                cReturn = cReturn.Replace(item.Value, myDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
                            }
                            else
                            {
                                cReturn = cReturn.Replace(item.Value, args.Value.ToString());
                            }
                        }
                        else
                        {
                            cReturn = cReturn.Replace(item.Value, args.ResolveText);
                        }
                    }
                }
                return cReturn;
            }
        }

        public string SummaryTextEN
        {
            get
            {
                Regex regex = new Regex(@"{(#)?(?<DataField>\w+)(:(?<DataFormat>(#)?\w+))?}");
                string cReturn = "";
                try
                {
                    cReturn = RequestType.SummaryTextEN;
                }
                catch
                {
                }
                foreach (Match item in regex.Matches(RequestType.SummaryTextEN))
                {
                    RequestTextSolverEventArgs args;
                    if (item.Value.StartsWith("{#"))
                    {
                        PropertyInfo prop = typeof(RequestDocument).GetProperty(item.Groups["DataField"].Value, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, prop.GetValue(this, null));
                    }
                    else
                    {
                        string cColName = item.Groups["DataField"].Value;
                        if (this.Data.Rows.Count > 0 && this.Data.Columns.Contains(cColName))
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, this.Data.Rows[0][cColName]);
                        }
                        else
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, "");
                        }
                    }
                    if (args.Value != null)
                    {
                        if (ResolveText != null)
                        {
                            ResolveText(args);
                        }
                        if (!args.IsHandler)
                        {
                            if (args.Value.GetType() == typeof(DateTime))
                            {
                                DateTime myDate = (DateTime)args.Value;
                                cReturn = cReturn.Replace(item.Value, myDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
                            }
                            else
                            {
                                cReturn = cReturn.Replace(item.Value, args.Value.ToString());
                            }
                        }
                        else
                        {
                            cReturn = cReturn.Replace(item.Value, args.ResolveText);
                        }
                    }
                }
                return cReturn;
            }
        }

        public string SummaryTextTH
        {
            get
            {
                Regex regex = new Regex(@"{(#)?(?<DataField>\w+)(:(?<DataFormat>(#)?\w+))?}");
                string cReturn = "";
                try
                {
                    cReturn = RequestType.SummaryTextTH;
                }
                catch
                {
                }
                foreach (Match item in regex.Matches(RequestType.SummaryTextTH))
                {
                    RequestTextSolverEventArgs args;
                    if (item.Value.StartsWith("{#"))
                    {
                        PropertyInfo prop = typeof(RequestDocument).GetProperty(item.Groups["DataField"].Value, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, prop.GetValue(this, null));
                    }
                    else
                    {
                        string cColName = item.Groups["DataField"].Value;
                        if (this.Data.Rows.Count > 0 && this.Data.Columns.Contains(cColName))
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, this.Data.Rows[0][cColName]);
                        }
                        else
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, "");
                        }
                    }
                    if (args.Value != null)
                    {
                        if (ResolveText != null)
                        {
                            ResolveText(args);
                        }
                        if (!args.IsHandler)
                        {
                            if (args.Value.GetType() == typeof(DateTime))
                            {
                                DateTime myDate = (DateTime)args.Value;
                                cReturn = cReturn.Replace(item.Value, myDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
                            }
                            else
                            {
                                cReturn = cReturn.Replace(item.Value, args.Value.ToString());
                            }
                        }
                        else
                        {
                            cReturn = cReturn.Replace(item.Value, args.ResolveText);
                        }
                    }
                }
                return cReturn;
            }
        }

        #endregion " SummaryText "

        #region " LastActionDate "

        public DateTime LastActionDate
        {
            get
            {
                return __lastActionDate;
            }
            set
            {
                __lastActionDate = value;
            }
        }

        #endregion " LastActionDate "

        #region " Comment "

        public string Comment
        {
            get
            {
                return __newComment;
            }
            set
            {
                __newComment = value;
            }
        }

        public string Comment2
        {
            get
            {
                return __newComment2;
            }
            set
            {
                __newComment2 = value;
            }
        }

        #endregion " Comment "

        #region " GroupName "

        public string GroupName
        {
            get
            {
                return __groupName;
            }
            set
            {
                __groupName = value;
            }
        }

        #endregion " GroupName "

        public bool HasError
        {
            get
            {
                return __hasError;
            }
            set
            {
                __hasError = value;
            }
        }

        public string ErrorText
        {
            get
            {
                return __errorText;
            }
            set
            {
                __errorText = value;
            }
        }

        public string ReferRequestNo
        {
            get;
            set;
        }
        #endregion " Properties "

        #region " CalculateFlow "

        public void CalculateFlow()
        {
            FlowID = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.CalculateFlow(RequestType.ID, RequestType.VersionID, FlowKey);
        }

        #endregion " CalculateFlow "

        #region " CreateRequest "
        public RequestDocument CreateRequest(EmployeeData Requestor, RequestType RT, IDataService service, Dictionary<string, string> Params)
        {
            RequestDocument Doc = ServiceManager.CreateInstance(Requestor.CompanyCode).WorkflowService.CreateRequest(Requestor, RT.ID, Params);
            //service.CheckRequestIsCanCreate(Doc.Requestor);
            Doc.CreatedDate = DateTime.Now;
            Doc.SetDataService(service);
            Doc.CurrentFlowItemID = 0;
            Doc.__data = Doc.RequestType.GenerateDataTable();
            string Other_Param = string.Empty;
            if (Params.ContainsKey("OtherParam"))
            {
                Other_Param = Params["OtherParam"];
            }
            Doc.__additional = service.GenerateAdditionalData(Doc.Requestor, Doc.ReferRequestNo, Other_Param);
            return Doc;
        }

        #endregion " CreateRequest "

        #region " LoadDocument "
        public static RequestDocument LoadDocument(string RequestNo, string CompanyCode, string KeyMaster, bool IsOwnerView, bool IsDataOwnerView)
        {
            RequestDocument oDoc = ServiceManager.CreateInstance(CompanyCode).WorkflowService.LoadDocument(RequestNo, KeyMaster, IsOwnerView, IsDataOwnerView);
            return oDoc;
        }
        public static RequestDocument LoadDocumentReadOnly(string RequestNo,string CompanyCode)
        {
            RequestDocument oDoc = ServiceManager.CreateInstance(CompanyCode).WorkflowService.LoadDocument(RequestNo);
            return oDoc;
        }

        public static RequestDocument LoadDocumentReadOnly(string RequestNo,string CompanyCode, EmployeeData Requestor, bool IsOwnerView, bool IsDataOwnerView)
        {
            RequestDocument oDoc = ServiceManager.CreateInstance(CompanyCode).WorkflowService.LoadDocumentReadonly(RequestNo, string.Empty, IsOwnerView, IsDataOwnerView);
            return oDoc;
        }
        #endregion " LoadDocument "

        #region " SimulateFlow "

        public List<RequestFlow> SimulateFlow()
        {
            return ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.SimulateProcess(this);
        }

        public List<RequestFlow> SimulateFlow(RequestDocument doc)
        {
            return ServiceManager.CreateInstance(doc.CompanyCode).WorkflowService.SimulateProcess(doc);
        }

        #endregion " SimulateFlow "

        #region " GetPassedFlow "

        public List<RequestFlow> GetPassedFlow()
        {
            return ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GetPassedProcess(this.RequestNo);
        }

        #endregion " GetPassedFlow "

        #region " GetLastestFlow "

        public RequestFlow GetLastestFlow()
        {
            return ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GetLastProcess(this.RequestNo);
        }

        #endregion " GetLastestFlow "

        #region " Process "
        #endregion " Process "

        #region " ApplyData "

        public void ApplyData(Object newData,int NoOfFileAttached,string ActionCode,DateTime SubmitDate)
        {
            __dataService.ValidateData(newData, this.Requestor, this.RequestNo, this.DocumentDate, NoOfFileAttached);
            
            DataTable newInfo = this.Data.Copy();
            __dataService.CalculateInfoData(this.Requestor, newData, newInfo, ((this.CurrentFlowItem == null) ? string.Empty : this.CurrentFlowItem.Code), ActionCode, SubmitDate);
            
            bool __reroute = false;
            int __newFlowID = this.FlowID;
            if (this.CurrentFlowItem == null)
            {
                __newFlowID = this.CalculateFlowID(__dataService.GenerateFlowKey(this.Requestor, this.Creator, newInfo));
            }
            else if (this.CurrentFlowItem.IsCheckFlowAfterOverwrite)
            {
                __newFlowID = this.CalculateFlowID(__dataService.GenerateFlowKey(this.Requestor, this.Creator, newInfo));
            }

            if (__newFlowID == -1)
            {
                throw new Exception("Flow setting error. Please check setting for this request type.");
            }
            if (this.CurrentFlowItem == null || this.CurrentFlowItem.State.Code == "CR") 
            {
                // is new;
                __reroute = false;
                
                this.FlowID = __newFlowID;
                this.CurrentFlowItemID = this.Flow.CreateItemID;
                this.CurrentFlowItem = FlowItem.CreateInstance(this.Requestor.CompanyCode).GetFlowItem(this.FlowID, this.CurrentFlowItemID);
            }
            else
            {
                // check flow after overwrite
                if (this.CurrentFlowItem.IsCanOverwrite)
                {
                    if (this.CurrentFlowItem.IsCheckFlowAfterOverwrite)
                    {
                        if (this.FlowID != __newFlowID)
                        {
                            __reroute = true;
                        }
                    }
                    if (this.CurrentFlowItem.IsCheckDataAfterOverwrite && !__reroute)
                    {
                        if (__dataService.CheckFlowRoute(this.Data, newInfo))
                        {
                            __reroute = true;
                        }
                    }
                }
            }

            string PreviousState = this.CurrentFlowItem.Code;
            if (__reroute)
            {
              

                if (this.CurrentFlowItem.Code == "WAIT_FOR_EDIT" || this.CurrentFlowItem.Code == "WAIT_FOR_EDIT_AFTER_APPROVE")
                {
                    this.FlowID = __newFlowID;
                    this.CurrentFlowItemID = this.Flow.EditItemID;
                }
                else
                {
                    this.FlowID = __newFlowID;
                    this.CurrentFlowItemID = 0;
                    FlowItemAction oDefaultAction = this.CurrentFlowItem.PossibleActions(this, WorkflowPrinciple.Current.UserSetting.Employee)[0];
                    this.CurrentFlowItemID = oDefaultAction.NextItemID;
                }
                this.AuthorizeStep = 0;
            }

            this.__data = newInfo;
            this.__additional = newData;

            this.DocumentDate = __dataService.CalcuateDocumentDate(this.CreatedDate, this.SubmittedDate, this.Data);
            this.__flowKey = null;

            // ���ҧ request no ���� ����Ѻ�͡�������
            if (this.RequestID == 0)
            {
                this.RequestNo = this.GetNewRequestNo();
            }
            this.SaveExternalData(PreviousState,"SAVE");
            ServiceManager.CreateInstance(this.Requestor.CompanyCode).WorkflowService.SaveData(this, newInfo, newData);
        }

        public void ApplyDataForView(Object newData,bool IsCalculateInfo,string ActionCode,DateTime SubmitDate)
        {
            DataTable newInfo = this.Data.Copy();
            if (IsCalculateInfo)
            {
                __dataService.CalculateInfoData(this.Requestor, newData, newInfo, ((this.CurrentFlowItem == null) ? string.Empty : this.CurrentFlowItem.Code), ActionCode, SubmitDate);
                __dataService.ValidateDataAfterSubmit(this.Requestor, newData, newInfo, this.SubmittedDate); //AddBy: Ratchatawan W. (2017-10-31)
            }
            this.__data = newInfo;
            this.__additional = newData;
            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.SaveDataByPass(this, newInfo, newData);
        }

        public void ApplyDataForJob(Object newData, string ActionCode, DateTime SubmitDate)
        {
            DataTable newInfo = this.RequestType.GenerateDataTable();
            __dataService.CalculateInfoData(this.Requestor, newData, newInfo, ((this.CurrentFlowItem == null) ? string.Empty : this.CurrentFlowItem.Code), ActionCode, SubmitDate);
            this.__data = newInfo;
            this.__additional = newData;

            this.DocumentDate = __dataService.CalcuateDocumentDate(this.CreatedDate, this.SubmittedDate, this.Data);
            this.__flowKey = null;

            ServiceManager.CreateInstance(this.Requestor.CompanyCode).WorkflowService.SaveData(this, newInfo, newData);
        }

        #endregion " ApplyData "

        #region " SaveExternalData "

        private void SaveExternalData(string PreviousState,string ActionCode)
        {
            this.__dataService.SaveExternalData(this.Requestor, this.Data, this.Additional, PreviousState, this.CurrentFlowItem.Code, this.RequestNo, this.Comment, this.Comment2, ActionCode);
        }

        private void PostProcess(string PreviousState, string ActionCode)
        {
                this.__dataService.PostProcess(this.Requestor, this.Data, this.Additional, PreviousState, this.CurrentFlowItem.Code, this.RequestNo, this.Comment, ActionCode);
        }

        #endregion " SaveExternalData "

        //CHAT 2011-10-14 �����Ṻ����ҹ���͹��ѵ�����

        #region "SaveExternalFile"

        private void SaveExternalFile()
        {
            if (this.CurrentFlowItem.Code == "COMPLETED" && this.FileSetID != -1)
            {
                string requestSubType = this.__dataService.GenerateRequestSubtype(this.Requestor, this.Additional);
                FileAttachmentSet txFileset = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.LoadFileAttachment(this.FileSetID);
                FileAttachmentSet masterFileset = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.LoadMasterFileAttachment(this.Requestor, this.RequestTypeID, requestSubType);

                foreach (FileAttachment file in txFileset)
                {
                    int lastIndex = masterFileset.Count;
                    file.FileID = lastIndex;
                    masterFileset.Add(file);
                }
                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.SaveFileAttachment(masterFileset);
            }
        }

        #endregion "SaveExternalFile"

        #region " getFlowKey "

        private string getFlowKey(DataTable Info)
        {
            string cFlowKey = "";
            foreach (RequestTypeKey key in this.RequestType.Keys)
            {
                switch (key.Category)
                {
                    case RequestTypeKeyCategory.DATA:
                        //cFlowKey += Info.Rows[0][key.Code].ToString();
                        break;
                    //case RequestTypeKeyCategory.REQUESTOR:
                    //    if (this.SubmitDate == DateTime.MinValue)
                    //    {
                    //        cFlowKey += this.Requestor.RequestorData(this.CreatedDate, key.Code);
                    //    }
                    //    else
                    //    {
                    //        cFlowKey += this.Requestor.RequestorData(this.SubmitDate, key.Code);
                    //    }
                    //    break;
                }
            }
            return cFlowKey;
        }

        #endregion " getFlowKey "

        #region " SetDataService "

        public void SetDataService(IDataService DataService)
        {
            this.__dataService = DataService;
        }

        #endregion " SetDataService "

        #region " GetPossibleActions "

        public List<IActionData> GetPossibleActions()
        {
            List<IActionData> list = new List<IActionData>();
            if (__isReadOnly || this.IsDataOwnerView)
            {
                return list;
            }
            if (this.IsOwnerView)
            {
                if (!this.CurrentFlowItem.State.IsCanAction)
                {
                    return list;
                }
                else
                {
                    if ((WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == this.CreatorNo) || (WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == this.RequestorNo))
                    {
                        list.AddRange(FlowItem.CreateInstance(this.RequestorCompanyCode).GetFlowItem(this.FlowID, this.Flow.OwnerViewItemID).PossibleActions(this, WorkflowPrinciple.Current.UserSetting.Employee).ToArray());
                        //list.AddRange(this.CurrentFlowItem.PossibleActions.ToArray());
                    }
                }
            }
            else
            {
                list.AddRange(this.CurrentFlowItem.PossibleActions(this, WorkflowPrinciple.Current.UserSetting.Employee).ToArray());
                if (this.CurrentFlowItem.IsCanOverwrite)
                {
                    EditAction action = new EditAction();
                    list.Add(action);
                }
            }
            return list;
        }

        #endregion " GetPossibleActions "

        #region " FillInText "

        public string FillInText(string input, FlowItemAction action, EmployeeData oActionBy, string MailToEmployeeID,string Language, WorkflowMailTo mailto)
        {
            WorkflowManagement oWorkflow = WorkflowManagement.CreateInstance(this.RequestorCompanyCode);
            CultureInfo oCL = new CultureInfo("en-US");
            Regex oRegex = new Regex(@"\[#(?<REQUESTINFO>\w+)\]");
            MatchCollection groups = oRegex.Matches(input);

            foreach (Match item in groups)
            {
                switch (item.Groups["REQUESTINFO"].Value.ToUpper())
                {
                    case "REQUESTOR":
                        input = input.Replace(item.Value, this.Requestor.EmployeeID);
                        break;

                    case "REQUESTORNAME":
                        input = input.Replace(item.Value, this.Requestor.AlternativeNameForMail(Language));
                        break;

                    case "REQUESTTYPETEXT":
                        input = input.Replace(item.Value, RequestText.CreateInstance(this.RequestorCompanyCode).LoadText("REQUESTTYPE", Language, this.RequestType.Code).ToLower());
                        break;
                    case "MAILTO":
                        input = input.Replace(item.Value, mailto.MailToDisplay);
                        break;
                    case "RECEIPIENT":
                        input = input.Replace(item.Value, this.TranslateList(this.Receipients).ToString());
                        break;
                    case "RECEIPIENTBAK":
                        input = input.Replace(item.Value, this.TranslateList(this.BackupReceipients).ToString());
                        break;
                    case "REQUESTNO":
                        input = input.Replace(item.Value, this.RequestNo);
                        break;
                    case "STATUSTEXT":
                        input = input.Replace(item.Value, RequestText.CreateInstance(this.RequestorCompanyCode).LoadText("STATUS", Language, this.CurrentFlowItem.Code).ToLower());
                        break;
                    case "ACTIONDATE":
                        input = input.Replace(item.Value, DateTime.Now.ToString("dd/MM/yyyy", oCL));
                        break;
                    case "ACTIONBY":
                        input = input.Replace(item.Value, oActionBy.EmployeeID);
                        break;
                    case "ACTIONBYNAME":
                        if(oActionBy.IsExternalUser)
                            input = input.Replace(item.Value, mailto.MailToDisplay);
                        else
                            input = input.Replace(item.Value, oActionBy.AlternativeNameForMail(Language));
                        break;
                    case "ACTIONTEXT":
                        input = input.Replace(item.Value, RequestText.CreateInstance(this.RequestorCompanyCode).LoadText("ACTION", Language, action.Code));
                        break;
                    case "LASTEDACTIONTEXT": //�֧���� Action ������Ѻ���존����ش
                        string ActionText = oWorkflow.GetLastedActionByEmployeeID(this.RequestID, MailToEmployeeID, Language);
                        input = input.Replace(item.Value, ActionText);
                        break;
                    case "HAVE_EDIT_REASON":

                        string ReasonTH = "<p>" + oWorkflow.GetCommonText("SYSTEM", "TH", "EDIT_REASON") + ": <span style='color:#0000ff'>{0}</span></p>";
                        string ReasonEN = "<p>" + oWorkflow.GetCommonText("SYSTEM", "EN", "EDIT_REASON") + ": <span style='color:#0000ff'>{0}</span></p>";
                        //if (string.IsNullOrEmpty(this.Comment))
                        //{
                            string LastedRecallComment1 = oWorkflow.GetLastedCommentForActionCode(this.RequestID, "RECALL");
                            if (!string.IsNullOrEmpty(LastedRecallComment1))
                            {
                                input = input.Replace(item.Value, string.Format(Language == "TH" ? ReasonTH : ReasonEN, LastedRecallComment1));
                            }
                            else if (!string.IsNullOrEmpty(oWorkflow.GetEditReasonByRequestNo(this.RequestNo)))
                            {
                                input = input.Replace(item.Value, string.Format(Language=="TH"?ReasonTH:ReasonEN,oWorkflow.GetEditReasonByRequestNo(this.RequestNo)));
                            }
                             else
                            {
                                input = input.Replace(item.Value, "");
                            }
                        //}
                        //else
                        //{
                        //    input = input.Replace(item.Value, string.Format(Language == "TH" ? ReasonTH : ReasonEN, this.Comment));
                        //}
                        break;
                    case "COMMENT":
                        if (this.Comment != null && this.Comment != "")
                        {
                            input = input.Replace(item.Value, string.Format("{0}", this.Comment));
                        }
                        else
                        {
                            input = input.Replace(item.Value, "");
                        }
                        break;
                    case "REASON_RECALL":
                        if (string.IsNullOrEmpty(this.Comment))
                        {
                            string LastedRecallComment = oWorkflow.GetLastedCommentForActionCode(this.RequestID, "RECALL");
                            if (!string.IsNullOrEmpty(LastedRecallComment))
                            {
                                input = input.Replace(item.Value, string.Format("{0}", LastedRecallComment));
                            }
                            else
                            {
                                input = input.Replace(item.Value, "");
                            }
                        }
                        else
                        {
                            input = input.Replace(item.Value, string.Format("{0}", this.Comment));
                        }
                        break;
                    case "REASON_CANCEL":
                        if (string.IsNullOrEmpty(this.Comment))
                        {
                            string LastedCancelComment = WorkflowManagement.CreateInstance(this.RequestorCompanyCode).GetLastedCommentForActionCode(this.RequestID, "CANCEL");
                            if (!string.IsNullOrEmpty(LastedCancelComment))
                            {
                                input = input.Replace(item.Value, string.Format("{0}", LastedCancelComment));
                            }
                            else
                            {
                                input = input.Replace(item.Value, "");
                            }
                        }
                        else
                        {
                            input = input.Replace(item.Value, string.Format("{0}", this.Comment));
                        }
                        break;
                    // add request summary//
                    case "REQUESTSUMMARY":
                        input = input.Replace(item.Value, __dataService.GenEmailBody(this.Requestor, Data, this.CurrentFlowItem.Code, Language));
                        break;
                    // add request summary//
                    case "DEFAULT_URL":
                        input = input.Replace(item.Value, oActionBy.AlternativeName(Language));
                        break;
                }
            }
            return input;
        }

        #endregion " FillInText "

        #region " CopyReceipient "

        public void CopyReceipient()
        {
            this.BackupReceipients.Clear();
            this.BackupReceipients.AddRange(this.Receipients);
        }

        #endregion " CopyReceipient "

        #region " GetSpecialReceipient "

        public ReceipientList GetSpecialReceipient(string Code)
        {
            ReceipientList list = new ReceipientList();
            // change to datetime.now
            if (Code.Contains(":"))
            {
                EmployeeData emp = EmployeeManagement.CreateInstance(this.CompanyCode).GetEmployeeDataByPosition(Code.Split(':')[1], this.SubmittedDate, WorkflowPrinciple.Current.UserSetting.Language);
                list.AddRange(ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.GetSpecialReceipient(emp.EmployeeID, emp.CurrentPosition.ObjectID, Code.Split(':')[0], this.SubmittedDate));
            }
            else
                list.AddRange(ServiceManager.CreateInstance(this.CompanyCode).WorkflowService.GetSpecialReceipient(this.RequestorNo, this.RequestorPosition, Code, this.SubmittedDate));
            return list;
        }

        #endregion " GetSpecialReceipient "

        #region " LoadFileAttached "

        public FileAttachmentSet LoadFileAttached()
        {
            if (this.HasFileAttached)
            {
                if (__fileSet == null)
                {
                   __fileSet =  FileAttachmentSystemManagement.CreateInstance(this.CompanyCode).LoadFileSet(this.FileSetID);
                }
            }
            else
            {
                __fileSet = FileAttachmentSet.CreateFileSet();
            }
            return __fileSet;
        }

        #endregion " LoadFileAttached "

        #region " TranslateList "

        public ReceipientList TranslateReceipient(Receipient item)
        {
            ReceipientList list = new ReceipientList();
            list.Add(item);
            return this.TranslateList(list);
        }

        public ReceipientList TranslateList(ReceipientList list)
        {
            ReceipientList oReturn = new ReceipientList();
            foreach (Receipient rcp in list)
            {
                if (rcp == null)
                    continue;
                if (rcp.Type == ReceipientType.SPECIAL)
                {
                    if (rcp.Code == "#CREATOR" || rcp.Code == "#REQUESTOR")
                    {
                        if (rcp.Code == "#CREATOR")
                        {
                            oReturn.Add(new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo,this.CreatorCompanyCode, this.RequestID));
                        }
                        else
                        {
                            oReturn.Add(new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo,this.RequestorCompanyCode, this.RequestID));
                        }
                    }
                    else if (rcp.Code == "#DATAOWNER")
                    {
                        oReturn.AddRange(this.RequestType.RequestTypeOwner);
                    }
                    else
                    {
                        oReturn.AddRange(this.GetSpecialReceipient(rcp.Code));
                    }
                }
                else if (rcp.Type == ReceipientType.SPECIAL_DELEGATE)
                {
                    //oReturn.AddRange(this.GetSpecialReceipient_Delegate(rcp.Code));
                }
                else if (rcp.Type == ReceipientType.PROJECT)
                {
                    EmployeeData prj = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GetProjectManager(rcp.Code);
                    if (prj != null && prj.EmployeeID != "")
                    {
                        oReturn.Add(new Receipient(ReceipientType.EMPLOYEE, prj.EmployeeID, prj.CompanyCode, this.RequestID));
                    }
                }
                else
                {
                    oReturn.Add(rcp);
                }
            }
            return oReturn;
        }

        #endregion " TranslateList "

        #region " AttachFile "

        public void AttachFile(FileAttachmentSet fileSet)
        {
            this.FileSetID = fileSet.FileSetID;
            __fileSet = fileSet;
        }

        #endregion " AttachFile "


        public string RequestorCompanyCode { get; set; }

        public string CreatorCompanyCode { get; set; }

        public int RequestID { get; set; }

        public string Viewer { get; set; }

        public string Editor { get; set; }

        public List<RequestFlowMultipleReceipient> RequestFlowMultipleReceipientList {
            get
            {
                if(__RequestFlowMultipleReceipientList == null)
                    __RequestFlowMultipleReceipientList = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GetRequestFlowMultipleReceipientList(this.RequestID);
                return __RequestFlowMultipleReceipientList;
            }
            set { 
                __RequestFlowMultipleReceipientList = value;
            }
        }

        public List<RequestFlowMultipleReceipient_External> RequestFlowMultipleReceipient_ExternalList
        {
            get
            {
                if (__RequestFlowMultipleReceipient_ExternalList == null)
                    __RequestFlowMultipleReceipient_ExternalList = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GetRequestFlowMultipleReceipient_ExternalList(this.RequestID);
                return __RequestFlowMultipleReceipient_ExternalList;
            }
            set
            {
                __RequestFlowMultipleReceipient_ExternalList = value;
            }
        }

        public List<RequestFlowForwardSnapshot> RequestFlowForwardSnapshotList {
            get{
                if (__RequestFlowForwardSnapshotList == null) { __RequestFlowForwardSnapshotList = new List<RequestFlowForwardSnapshot>(); }
                return __RequestFlowForwardSnapshotList;
            }
            set { __RequestFlowForwardSnapshotList = value; }
        }        

        public List<IActionData> GetPossibleActionsForMobile(EmployeeData CurrentEmployee)
        {
            List<IActionData> list = new List<IActionData>();
            //if (__isReadOnly || this.IsDataOwnerView)
            //{
            //    return list;
            //}
            if (this.IsOwnerView)
            {
                if (!this.CurrentFlowItem.State.IsCanAction)
                {
                    return list;
                }
                else
                {
                    if ((CurrentEmployee.EmployeeID == this.CreatorNo) || (CurrentEmployee.EmployeeID == this.RequestorNo))
                    {
                        list.AddRange(FlowItem.CreateInstance(this.RequestorCompanyCode).GetFlowItem(this.FlowID, this.Flow.OwnerViewItemID).PossibleActions(this, CurrentEmployee).ToArray());

                        //list.AddRange(this.CurrentFlowItem.PossibleActions.ToArray());
                    }
                }
            }
            else
            {
                list.AddRange(this.CurrentFlowItem.PossibleActions(this, CurrentEmployee).ToArray());
                if (this.CurrentFlowItem.IsCanOverwrite && string.IsNullOrEmpty(this.IsTimeout) && list.Count > 0)
                {
                    FlowItemAction oFlowItemAction = new FlowItemAction();
                    oFlowItemAction.Code = "EDIT";
                    oFlowItemAction.IsVisible = true;

                    //EditAction action = new EditAction();
                    //action.Code = "EDIT";

                    list.Add(oFlowItemAction);
                }
            }
            return list;
        }

        public string IsTimeout
        {
            get
            {
                string value = string.Empty;
                if (CurrentFlowItem.IsCheckTimout)
                {

                    if (this.Data != null && this.IsCompleted == false && this.IsCancelled == false)
                    {
                        value = __dataService.CheckRequestIsTimeout(this.CreatedDate, this.Data);
                        if (Flow.TimeoutItemID > -1 )
                            this.CurrentFlowItemID = Flow.TimeoutItemID;
                    }
                }
                return value;
            }
        }

        public List<IActionData> GetPossibleActions(EmployeeData Requestor)
        {
            List<IActionData> list = new List<IActionData>();
            if (__isReadOnly || this.IsDataOwnerView)
            {
                return list;
            }
            if (this.IsOwnerView)
            {
                if (!this.CurrentFlowItem.State.IsCanAction)
                {
                    return list;
                }
                else
                {
                    if ((Requestor.EmployeeID == this.CreatorNo) || (Requestor.EmployeeID == this.RequestorNo))
                    {
                        list.AddRange(FlowItem.CreateInstance(this.RequestorCompanyCode).GetFlowItem(this.FlowID, this.Flow.OwnerViewItemID).PossibleActions(this, Requestor).ToArray());

                        //list.AddRange(this.CurrentFlowItem.PossibleActions.ToArray());
                    }
                }
            }
            else
            {
                list.AddRange(this.CurrentFlowItem.PossibleActions(this, Requestor).ToArray());
                if (this.CurrentFlowItem.IsCanOverwrite && string.IsNullOrEmpty(this.IsTimeout) && list.Count > 0)
                {
                    EditAction action = new EditAction();
                    list.Add(action);
                }
            }
            return list;
        }
        public RequestActionResult Process(FlowItemAction action, EmployeeData TakeActionBy)
        {
            WorkflowManagement oManagement = WorkflowManagement.CreateInstance(this.RequestorCompanyCode);
            if (__isReadOnly)
            {
                throw new ESS.SECURITY.NoAuthorizeException("Process Request");
            }
            RequestActionResult oResult = ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.ProcessRequest(this, action, TakeActionBy);
            
            EmployeeData emp = new EmployeeData();
            emp.CompanyCode = CompanyCode;
            bool isUpdate = false;
            bool isSendToDataOwenr = false;
            Exception exceptionForThrow = null;
            string PreviousFlowItemCode = string.Empty;
            string ActionCode = string.Empty;
            try
            {
                FlowItem item = FlowItem.CreateInstance(this.RequestorCompanyCode).GetFlowItem(this.FlowID, this.CurrentFlowItemID);
               
                //Start: Calculate action for sendmail 30 Oct 2016
                if (oResult.IsRestartFlow)
                {
                    List<FlowItemAction> oPosibleAction = FlowItemAction.CreateInstance(this.RequestorCompanyCode).GetAllPosibleAction(this.FlowID, this.CurrentFlowItemID);
                    if (oPosibleAction.Count > 0)
                        action = oPosibleAction[0];
                }
                //End
                FlowItem PreviousFLowItem = FlowItem.CreateInstance(this.RequestorCompanyCode).GetFlowItem(action.FlowID, action.FlowItemID);
                PreviousFlowItemCode = PreviousFLowItem.Code;
                ActionCode = action.Code;
                this.SaveExternalData(PreviousFlowItemCode, ActionCode);
                this.SaveExternalFile();
                isUpdate = true;
            }
            catch (SaveExternalDataException ex)
            {
                isUpdate = true;
                isSendToDataOwenr = ex.SendToDataOwner;
                exceptionForThrow = ex;
            }

            action.IsGotoNextItem = oResult.GoToNextItem; //AddBy: Ratchatawan W. (8 Jan 2016)

            if (isUpdate)
            {
                //CommentBy: Ratchatawan W. (15/5/2018)
                //if (this.Data.GetChanges() != null || this.Additional.GetChanges() != null)
                //{
                //    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.SaveDataByPass(this, this.Data, this.Additional);
                //}
                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.UpdateDocument(this, action);

            }
            if (isSendToDataOwenr)
            {
                this.CurrentFlowItemID = this.Flow.DataOwnerItemID;

                //AddBy: Ratchatawan W. (2012-09-28)
                //this.Receipients.AddRange(this.RequestType.RequestTypeOwner);
                //this.IsCompleted = false;

                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.UpdateDocument(this, new FlowItemExceptionAction());
            }

            string PostActionCode = ActionCode;
            if (ActionCode == "REVERSEANDREJECT_NOTDEFAULT" || ActionCode == "REVERSEANDREJECT_DEFAULT" || ActionCode == "REVERSE_DEFAULT" || ActionCode == "REVERSE_NOTDEFAULT")
                PostActionCode = "REVERSE";
            this.PostProcess(PreviousFlowItemCode, PostActionCode);

            if (exceptionForThrow != null)
            {
                throw new ProcessRequestException("Process data error with handled", exceptionForThrow);
            }

            #region " Notify by Email "
            try
            {
                int RequestMailID = 0;
                Dictionary<int, List<WorkflowMailTo>> list = new Dictionary<int, List<WorkflowMailTo>>();
                string cMailBody;

                #region " Notify Mail "

                #region " NotifyReceipients "
                if (oResult.IsAutomatic)
                {
                    if (action.NotifyAutomaticAction > -1)
                    {
                        RequestMailID = action.NotifyAutomaticAction;
                        foreach (Receipient rcp in this.TranslateList(this.Receipients))
                        {
                            if (rcp.Type != ReceipientType.USERROLE)
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyAutomaticAction);
                            else
                            {
                                if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyAutomaticAction);
                            }
                        }

                        #region " CC "
                        if (action.NotifyAutomaticAction_CCRequestor)
                        {
                            Receipient rcp;
                            rcp = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);

                            if (this.CreatorNo != this.RequestorNo)
                            {
                                rcp = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyAutomaticAction_CCCurrent)
                        {
                            foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                            {
                                if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyAutomaticAction_CCAllActionInPass)
                        {
                            foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                            {
                                rcp.Type = ReceipientType.EMPLOYEE;
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyAutomaticAction_CCSpecial && !string.IsNullOrEmpty(action.NotifyAutomaticAction_CCSpecialRole))
                        {
                            Receipient rcp = new Receipient();
                            foreach (string item in action.NotifyAutomaticAction_CCSpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName
                            {

                                foreach (string cc in item.Split(':')[1].Split(','))
                                {
                                    switch (item.Split(':')[0])
                                    {
                                        case "ROLE":
                                            rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "EMPLOYEE":
                                            rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "POSITION":
                                            rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "SPECIAL":
                                            ReceipientList lst = GetSpecialReceipient(cc);
                                            if (lst.Count > 0)
                                                rcp = new Receipient(ReceipientType.POSITION, lst[0].Code, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                    }
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                }
                            }
                        }
                        #endregion
                    }

                    else
                    {
                        if (action.NotifyReceipients > -1)
                        {
                            RequestMailID = action.NotifyReceipients;
                            foreach (Receipient rcp in this.TranslateList(this.Receipients))
                            {
                                if (rcp.Type != ReceipientType.USERROLE)
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyReceipients);
                                else
                                {
                                    if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyReceipients);
                                }
                            }

                            #region " CC "
                            if (action.NotifyReceipients_CCRequestor)
                            {
                                Receipient rcp;
                                rcp = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);

                                if (this.CreatorNo != this.RequestorNo)
                                {
                                    rcp = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                }
                            }

                            if (action.NotifyReceipients_CCCurrent)
                            {
                                foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                                {
                                    if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                }
                            }

                            if (action.NotifyReceipients_CCAllActionInPass)
                            {
                                foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                                {
                                    rcp.Type = ReceipientType.EMPLOYEE;
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                }
                            }

                            if (action.NotifyReceipients_CCSpecial && !string.IsNullOrEmpty(action.NotifyReceipients_CCSpecialRole))
                            {
                                Receipient rcp = new Receipient();
                                foreach (string item in action.NotifyReceipients_CCSpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName
                                {

                                    foreach (string cc in item.Split(':')[1].Split(','))
                                    {
                                        switch (item.Split(':')[0])
                                        {
                                            case "ROLE":
                                                rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);
                                                break;
                                            case "EMPLOYEE":
                                                rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                                break;
                                            case "POSITION":
                                                rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                                break;
                                            case "SPECIAL":
                                                ReceipientList lst = GetSpecialReceipient(cc);
                                                if (lst.Count > 0)
                                                    rcp = new Receipient(ReceipientType.POSITION, lst[0].Code, this.RequestorCompanyCode, this.RequestID);
                                                break;
                                        }
                                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    if (action.NotifyReceipients > -1)
                    {
                        RequestMailID = action.NotifyReceipients;
                        foreach (Receipient rcp in this.TranslateList(this.Receipients))
                        {
                            if (rcp.Type != ReceipientType.USERROLE)
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyReceipients);
                            else
                            {
                                if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyReceipients);
                            }
                        }

                        #region " CC "

                        if (action.NotifyReceipients_CCRequestor)
                        {
                            Receipient rcp;
                            rcp = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);

                            if (this.CreatorNo != this.RequestorNo)
                            {
                                rcp = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyReceipients_CCCurrent)
                        {
                            foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                            {
                                if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyReceipients_CCAllActionInPass)
                        {
                            foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                            {
                                rcp.Type = ReceipientType.EMPLOYEE;
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyReceipients_CCSpecial && !string.IsNullOrEmpty(action.NotifyReceipients_CCSpecialRole))
                        {
                            Receipient rcp = new Receipient();
                            foreach (string item in action.NotifyReceipients_CCSpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName
                            {

                                foreach (string cc in item.Split(':')[1].Split(','))
                                {
                                    switch (item.Split(':')[0])
                                    {
                                        case "ROLE":
                                            rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);;
                                            break;
                                        case "EMPLOYEE":
                                            rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "POSITION":
                                            rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "SPECIAL":
                                            ReceipientList lst = GetSpecialReceipient(cc);
                                            if (lst.Count > 0)
                                                rcp = new Receipient(ReceipientType.POSITION, lst[0].Code, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                    }
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                }
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                #region " NotifyRequestor "
                if (action.NotifyRequestor > -1)
                {
                    RequestMailID = action.NotifyRequestor;
                    Receipient rcp2;

                    if (action.NotifyRequestorWithGotoNextItem > -1 && action.IsGotoNextItem)
                    {
                        RequestMailID = action.NotifyRequestorWithGotoNextItem;

                        rcp2 = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp2, false, this, RequestMailID);

                        if (this.CreatorNo != this.RequestorNo)
                        {
                            rcp2 = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp2, false, this, RequestMailID);
                        }

                        #region " CC "
                        if (action.NotifyRequestorWithGotoNextItem_CCCurrent)
                        {
                            foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                            {
                                if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyRequestorWithGotoNextItem_CCAllActionInPass)
                        {
                            foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                            {
                                rcp.Type = ReceipientType.EMPLOYEE;
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyRequestorWithGotoNextItem_CCSpecial && !string.IsNullOrEmpty(action.NotifyRequestorWithGotoNextItem_CCSpecialRole))
                        {
                            Receipient rcp = new Receipient();
                            foreach (string item in action.NotifyRequestorWithGotoNextItem_CCSpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName
                            {

                                foreach (string cc in item.Split(':')[1].Split(','))
                                {
                                    switch (item.Split(':')[0])
                                    {
                                        case "ROLE":
                                            rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "EMPLOYEE":
                                            rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "POSITION":
                                            rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "SPECIAL":
                                            ReceipientList lst = GetSpecialReceipient(cc);
                                            if (lst.Count > 0)
                                                rcp = new Receipient(ReceipientType.POSITION, lst[0].Code, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                    }
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        rcp2 = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp2, false, this, RequestMailID);

                        if (this.CreatorNo != this.RequestorNo)
                        {
                            rcp2 = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp2, false, this, RequestMailID);
                        }

                        #region " CC "
                        if (action.NotifyRequestor_CCCurrent)
                        {
                            foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                            {
                                if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyRequestor_CCAllActionInPass)
                        {
                            foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                            {
                                rcp.Type = ReceipientType.EMPLOYEE;
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }

                        if (action.NotifyRequestor_CCSpecial && !string.IsNullOrEmpty(action.NotifyRequestor_CCSpecialRole))
                        {
                            Receipient rcp = new Receipient();
                            foreach (string item in action.NotifyRequestor_CCSpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName
                            {

                                foreach (string cc in item.Split(':')[1].Split(','))
                                {
                                    switch (item.Split(':')[0])
                                    {
                                        case "ROLE":
                                            rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "EMPLOYEE":
                                            rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "POSITION":
                                            rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                        case "SPECIAL":
                                            ReceipientList lst = GetSpecialReceipient(cc);
                                            if (lst.Count > 0)
                                                rcp = new Receipient(ReceipientType.POSITION, lst[0].Code, this.RequestorCompanyCode, this.RequestID);
                                            break;
                                    }
                                    ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                                }
                            }
                        }
                        #endregion
                    }
                    
                }
                #endregion

                #region " NotifyCurrent "
                if (action.NotifyCurrent > -1)
                {
                    RequestMailID = action.NotifyCurrent;
                    foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                    {
                        if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyCurrent);
                    }

                    #region " CC "
                    if (action.NotifyCurrent_CCRequestor)
                    {
                        Receipient rcp;
                        rcp = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);

                        if (this.CreatorNo != this.RequestorNo)
                        {
                            rcp = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                        }
                    }

                    if (action.NotifyCurrent_CCAllActionInPass)
                    {
                        foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                        {
                            rcp.Type = ReceipientType.EMPLOYEE;
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                        }
                    }

                    if (action.NotifyCurrent_CCSpecial && !string.IsNullOrEmpty(action.NotifyCurrent_CCSpecialRole))
                    {
                        Receipient rcp = new Receipient();
                        foreach (string item in action.NotifyCurrent_CCSpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName
                        {

                            foreach (string cc in item.Split(':')[1].Split(','))
                            {
                                switch (item.Split(':')[0])
                                {
                                    case "ROLE":
                                        rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);
                                        break;
                                    case "EMPLOYEE":
                                        rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                        break;
                                    case "POSITION":
                                        rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                        break;
                                }
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }
                    }

                    #endregion
                }
                #endregion

                #region " NotifyAllActionInPass "
                if (action.NotifyAllActionInPass > -1)
                {
                    RequestMailID = action.NotifyAllActionInPass;
                    foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                    {
                        rcp.Type = ReceipientType.EMPLOYEE;
                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifyAllActionInPass);
                    }

                    #region " CC "
                    if (action.NotifyAllActionInPass_CCRequestor)
                    {
                        Receipient rcp;
                        rcp = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                        ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);

                        if (this.CreatorNo != this.RequestorNo)
                        {
                            rcp = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                        }
                    }

                    if (action.NotifyAllActionInPass_CCCurrent)
                    {
                        foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                        {
                            if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                        }
                    }

                    if (action.NotifyAllActionInPass_CCSpecial && !string.IsNullOrEmpty(action.NotifyAllActionInPass_CCSpecialRole))
                    {
                        Receipient rcp = new Receipient();
                        foreach (string item in action.NotifyAllActionInPass_CCSpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName
                        {

                            foreach (string cc in item.Split(':')[1].Split(','))
                            {
                                switch (item.Split(':')[0])
                                {
                                    case "ROLE":
                                        rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);
                                        break;
                                    case "EMPLOYEE":
                                        rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                        break;
                                    case "POSITION":
                                        rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                        break;
                                }
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                            }
                        }
                    }
                    #endregion
                }
                #endregion

                #region " NotifySpecial "
                if (action.NotifySpecial > -1 && !string.IsNullOrEmpty(action.SpecialRole))
                {
                    
                     RequestMailID = action.NotifySpecial;
                     foreach (string item in action.SpecialRole.Split('|')) //EMPLOYEE:EmployeeID1,EmployeeID2|ROLE:RoleName|SPECIAL:#MANAGER_R_01
                     {
                         Receipient rcp = new Receipient();
                         foreach (string cc in item.Split(':')[1].Split(','))
                         {
                             switch (item.Split(':')[0])
                             {
                                 case "ROLE":
                                     rcp = new Receipient(ReceipientType.USERROLE, cc, this.RequestorCompanyCode, this.RequestID);
                                     break;
                                 case "EMPLOYEE":
                                     rcp = new Receipient(ReceipientType.EMPLOYEE, cc, this.RequestorCompanyCode, this.RequestID);
                                     break;
                                 case "POSITION":
                                     rcp = new Receipient(ReceipientType.POSITION, cc, this.RequestorCompanyCode, this.RequestID);
                                     break;
                                 case "SPECIAL":
                                     ReceipientList lst = GetSpecialReceipient(cc);
                                     if (lst.Count > 0)
                                         rcp = new Receipient(ReceipientType.POSITION, lst[0].Code, this.RequestorCompanyCode, this.RequestID);
                                     break;
                             }
                             ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, false, this, action.NotifySpecial);
                         }
                     }

                     #region " CC "

                     if (action.NotifySpecial_CCRequestor)
                     {
                         Receipient rcp;
                         rcp = new Receipient(ReceipientType.EMPLOYEE, this.RequestorNo, this.RequestorCompanyCode, this.RequestID);
                         ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);

                         if (this.CreatorNo != this.RequestorNo)
                         {
                             rcp = new Receipient(ReceipientType.EMPLOYEE, this.CreatorNo, this.CreatorCompanyCode, this.RequestID);
                             ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                         }
                     }

                     if (action.NotifySpecial_CCCurrent)
                     {
                         foreach (Receipient rcp in this.TranslateList(this.BackupReceipients))
                         {
                             if (!(rcp.Code == "JUNIOR_ACCOUNT" || rcp.Code == "SENIOR_ACCOUNT"))
                                 ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                         }
                     }

                     if (action.NotifySpecial_CCAllActionInPass)
                     {
                         foreach (Receipient rcp in oManagement.GetAllActionInPass(this, WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID))
                         {
                             rcp.Type = ReceipientType.EMPLOYEE;
                             ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.GenerateMailTo(list, rcp, true, this, RequestMailID);
                         }
                     }

                     #endregion
                }
                #endregion

                #region " send mail "
                foreach (int TemplateID in list.Keys)
                {
                    //Generate CC Mail
                    List<WorkflowMailTo> MailCCList =  new List<WorkflowMailTo>();
                    foreach (WorkflowMailTo mailto in list[TemplateID].FindAll(obj => obj.IsCC))
                    {
                        if (emp.GetUserSetting(mailto.EmployeeID).ReceiveMail)
                        {
                            MailCCList.Add(mailto);
                        }
                    }

                    foreach (WorkflowMailTo mailto in list[TemplateID].FindAll(obj => !obj.IsCC))
                    {
                        cMailBody = ResolveTextInEmail(action.GetMailTemplate(TemplateID, mailto.LanguageCode));
                        cMailBody = this.FillInText(cMailBody, action, WorkflowPrinciple.Current.UserSetting.Employee, mailto.EmployeeID, mailto.LanguageCode, mailto);

                        WorkflowMail oMailMessage = new WorkflowMail(this.RequestorCompanyCode);
                        oMailMessage.MailFrom = WorkflowPrinciple.Current.UserSetting.Employee.EmailAddress;
                        oMailMessage.MailFromDisplay = string.IsNullOrEmpty(WorkflowPrinciple.Current.UserSetting.Employee.AlternativeName(mailto.LanguageCode)) ? "" : WorkflowPrinciple.Current.UserSetting.Employee.AlternativeName(mailto.LanguageCode);
                        oMailMessage.CreatedBy = WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
                        oMailMessage.CreatedDate = DateTime.Now;

                        if (emp.GetUserSetting(mailto.EmployeeID).ReceiveMail)
                        {
                            oMailMessage.MailTo = new List<WorkflowMailTo>();
                            oMailMessage.MailTo.Add(mailto);
                            oMailMessage.MailCC.AddRange(MailCCList);

                            //AddBy: Ratchatawan W. (2011-08-10)
                            #region Auto generate ticket for bypass login
                            //Generate TicketID
                            string TicketID = Guid.NewGuid().ToString();
                            //Create Ticket in Database
                            ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.CreateTicket(TicketID, this.RequestNo, mailto.EmployeeID, mailto.CompanyCode, mailto.IsExternalUser);

                            //Start: ͹حҵ����������Ѻ Mail CC ����ö�Դ����Ҵ� Request �� (3 Jan 2016)
                            foreach (WorkflowMailTo MailCC in MailCCList)
                            {
                                ServiceManager.CreateInstance(this.RequestorCompanyCode).WorkflowService.CreateTicket(TicketID, this.RequestNo, MailCC.EmployeeID, MailCC.CompanyCode, mailto.IsExternalUser);
                            }
                            //End: ͹حҵ����������Ѻ Mail CC ����ö�Դ����Ҵ� Request ��


                            if (mailto.IsExternalUser)
                            {
                                cMailBody = cMailBody.Replace("[#LINK]", oManagement.TICKET_URL_FOREXTERNAL_USER + this.RequestorCompanyCode + "/" + TicketID + "/" + mailto.EmployeeID);

                                cMailBody += "<p><a href='[#LINK_FOREXTERNAL_USER]'>Link</a> for external user</p>";
                                cMailBody = cMailBody.Replace("[#LINK_FOREXTERNAL_USER]", string.Format(oManagement.MASSAPPROVE_URL_FOREXTERNAL_USER, mailto.EmployeeID, mailto.CompanyCode));
                            }
                            else
                            {
                                cMailBody = cMailBody.Replace("[#LINK]", oManagement.TICKET_URL + this.RequestorCompanyCode + "/" + TicketID);
                            }

                            #endregion

                            oMailMessage.RequestNo = this.RequestNo;
                            oMailMessage.MailSubject =  this.FillInText(ResolveTextInEmail(action.GetMailSubject(TemplateID, mailto.LanguageCode)), action, WorkflowPrinciple.Current.UserSetting.Employee, mailto.EmployeeID, mailto.LanguageCode, mailto);
                            if (ShareDataManagement.IsTest)
                                oMailMessage.MailSubject = "[ID:" + TemplateID.ToString() + "] "+oMailMessage.MailSubject;
                            oMailMessage.MailBody = cMailBody;
                            if (ShareDataManagement.SendMail)
                                oMailMessage.Send();
                        }
                    }
                }
                #endregion

                #endregion
            }
            catch
            {

            }
            #endregion

            return oResult;
        }

        public string ResolveTextInEmail(string cReturn)
        {
            Regex regex = new Regex(@"{(#)?(?<DataField>\w+)(:(?<DataFormat>(#)?\w+))?}");
            string cReturnBak = cReturn;
            foreach (Match item in regex.Matches(cReturnBak))
                {
                    RequestTextSolverEventArgs args;
                    if (item.Value.StartsWith("{#"))
                    {
                        PropertyInfo prop = typeof(RequestDocument).GetProperty(item.Groups["DataField"].Value, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, prop.GetValue(this, null));
                    }
                    else
                    {
                        string cColName = item.Groups["DataField"].Value;
                        if (this.Data.Rows.Count > 0 && this.Data.Columns.Contains(cColName))
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, this.Data.Rows[0][cColName]);
                        }
                        else
                        {
                            args = new RequestTextSolverEventArgs(item.Groups["DataFormat"].Value, "");
                        }
                    }
                    if (args.Value != null)
                    {
                        if (ResolveText != null)
                        {
                            ResolveText(args);
                        }
                        if (!args.IsHandler)
                        {
                            if (args.Value.GetType() == typeof(DateTime))
                            {
                                DateTime myDate = (DateTime)args.Value;
                                cReturn = cReturn.Replace(item.Value, myDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
                            }
                            else
                            {
                                cReturn = cReturn.Replace(item.Value, args.Value.ToString());
                            }
                        }
                        else
                        {
                            cReturn = cReturn.Replace(item.Value, args.ResolveText);
                        }
                    }
                }
                return cReturn;
        }

        public List<FlowItemAction> GetPossibleActionsForMobile()
        {
            List<FlowItemAction> list = new List<FlowItemAction>();
           // List<IActionData> list = new List<IActionData>();
            if (__isReadOnly || this.IsDataOwnerView)
            {
                return list;
            }
            if (this.IsOwnerView)
            {
                if (!this.CurrentFlowItem.State.IsCanAction)
                {
                    return list;
                }
                else
                {
                    if ((WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == this.CreatorNo) || (WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID == this.RequestorNo) && !this.CurrentFlowItem.State.IsEndPoint)
                    {
                        list.AddRange(FlowItem.CreateInstance(this.RequestorCompanyCode).GetFlowItem(this.FlowID, this.Flow.OwnerViewItemID).PossibleActions(this, WorkflowPrinciple.Current.UserSetting.Employee));//.ToArray());
                    }
                }
            }
            else
            {
                list.AddRange(this.CurrentFlowItem.PossibleActions(this, WorkflowPrinciple.Current.UserSetting.Employee));//.ToArray());
                if (this.CurrentFlowItem.IsCanOverwrite && string.IsNullOrEmpty(this.IsTimeout) && list.FindAll(obj => obj.IsVisible).Count > 0)
                {
                    FlowItemAction oFlowItemAction = new FlowItemAction();
                    oFlowItemAction.Code = "EDIT";
                    oFlowItemAction.IsVisible = true;

                    //EditAction action = new EditAction();
                    //action.Code = "EDIT";

                    list.Add(oFlowItemAction);
                }
            }
            return list;
        }

        private string GetNewRequestNo()
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestNo(this.CreatedDate,this.RequestType.Prefix, this.ReferRequestNo);
        }
    }
}