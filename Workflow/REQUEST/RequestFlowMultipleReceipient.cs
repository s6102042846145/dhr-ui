using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Text;

namespace PTTCHEM.WORKFLOW
{
    public class RequestFlowMultipleReceipient : AbstractObject
    {
        public RequestFlowMultipleReceipient()
        {
        }

        public int RequestID { get; set; }
        public string ApproverID { get; set; }
        public string ApproverCompanyCode { get; set; }
        public string ActionCode { get; set; }
        public DateTime ActionDate { get; set; }
        public string ActionBy { get; set; }
        public string ActionByPosition { get; set; }
        public string ActionByCompanyCode { get; set; }
        public string Comment { get; set; }

        public string ApproverGroup { get; set; }

    }
}
