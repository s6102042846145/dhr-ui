using System;
using System.Collections.Generic;
using ESS.EMPLOYEE;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class ReceipientList : List<Receipient>
    {
        public ReceipientList()
            : base()
        {
        }

        public override string ToString()
        {
            string cReturn = "";
            List<string> key = new List<string>();
            foreach (Receipient rcp in this)
            {
                string item = rcp.DisplayName;
                if (!key.Contains(rcp.Code))
                {
                    cReturn += "," + item;
                    key.Add(rcp.Code);
                }
            }
            return cReturn.TrimStart(',');
        }

        public string ToPositionString()
        {
            string cReturn = "";
            List<string> key = new List<string>();
            string item = string.Empty;
            foreach (Receipient rcp in this)
            {
                try
                {
                    item = string.Empty;
                    item = rcp.DisplayPositionName;
                }
                catch (Exception ex)
                {
                    item = WorkflowManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCommonText("SYSTEM", WorkflowPrinciple.Current.UserSetting.Language, "ERROR_DISPLAYPOSITIONNAME");
                } 
                if (!key.Contains(rcp.Code))
                {
                    cReturn += "," + item;
                    key.Add(rcp.Code);
                }
            }
            return cReturn.TrimStart(',');
        }


        public bool IsCanAction(RequestDocument Document, EmployeeData ActionBy)
        {
            if (ActionBy == null)
                return false;
            foreach (Receipient item in Document.TranslateList(this))
            {
                if (item.IsCanAction(ActionBy, Document))
                {
                    return true;
                }
            }
            return false;
        }
    }
}