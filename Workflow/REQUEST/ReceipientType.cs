namespace ESS.WORKFLOW
{
    public enum ReceipientType
    {
        USERROLE = 'R',
        EMPLOYEE = 'P',
        POSITION = 'S',
        SPECIAL = 'X',
        SPECIAL_DELEGATE = 'Y',
        PROJECT = 'J',
        EXTERNAL_USER = 'E',
        CUSTODIAN = 'T'    
    }
}