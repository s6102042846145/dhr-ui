using ESS.UTILITY.EXTENSION;

namespace ESS.WORKFLOW.AUTHORIZATION
{
    public class AuthorizationModelSetCondition : AbstractObject
    {
        public AuthorizationModelSetCondition()
        {
        }

        public int AuthModelID { get; set; }
        public int AuthModelSetID { get; set; }
        public int AuthConditionID { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
    }
}