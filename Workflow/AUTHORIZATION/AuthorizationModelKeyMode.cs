namespace ESS.WORKFLOW.AUTHORIZATION
{
    public enum AuthorizationModelKeyMode
    {
        DATA,
        USER,
        REQUESTOR
    }
}