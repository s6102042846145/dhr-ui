using System.Collections.Generic;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.SHAREDATASERVICE;

namespace ESS.WORKFLOW
{
    public class UserRoleManager
    {
        #region Constructor
        private UserRoleManager()
        {

        }
        #endregion 
        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, UserRoleManager> Cache = new Dictionary<string, UserRoleManager>();
        public string CompanyCode { get; set; }

        public static string ModuleID =  "ESS.WORKFLOW";
        public static UserRoleManager CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new UserRoleManager()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            UserRoleManager oUserRoleManager = new UserRoleManager()
            {
                CompanyCode = oCompanyCode
            };
            return oUserRoleManager;
        }
        #endregion MultiCompany  Framework

        public List<EmployeeData> GetUserResponse(string Role, EmployeeData emp)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetUserResponse(Role, emp);
        }

        public List<string> GetResponseCode(string Role, EmployeeData Emp)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetResponseCode(Role, Emp);
        }

        public List<string> GetResponseCodeByOrganizationOnly(string Role, EmployeeData Emp)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetResponseCodeByOrganizationOnly(Role, Emp, EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ROOT_ORGANIZATION);
        }
    }
}