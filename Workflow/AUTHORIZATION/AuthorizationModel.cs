using System.Collections.Generic;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;

namespace ESS.WORKFLOW.AUTHORIZATION
{
    public class AuthorizationModel : AbstractObject
    {
        #region Constructor
        public AuthorizationModel()
        {

        }
        #endregion

        #region MultiCompany  Framework
        private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, AuthorizationModel> Cache = new Dictionary<string, AuthorizationModel>();
        public string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "ESS.WORKFLOW";
            }
        }
        public static AuthorizationModel CreateInstance(string oCompanyCode)
        {
            AuthorizationModel oAuthorizationModel = new AuthorizationModel()
            {
                CompanyCode = oCompanyCode
            };
            return oAuthorizationModel;
        }
        #endregion MultiCompany  Framework

        public int AuthModelID { get; set; }
        public string AuthModelCode { get; set; }
        public List<AuthorizationModelKey> Keys
        {
            get
            {
                //if (__keys == null)
                //{
                return ServiceManager.CreateInstance( CompanyCode).WorkflowService.GetAuthorizationModelKey(this.AuthModelID);
                //}
                //return __keys;
            }
            set
            {

            }
        }

        public List<AuthorizationModelSet> Sets
        {
            get
            {
                //if (__sets == null)
                //{
                return ServiceManager.CreateInstance( CompanyCode).WorkflowService.GetAuthorizationModelSet(this.AuthModelID);
                //}
                //return __sets;
            }
            set
            {

            }
        }

        
    }
}