namespace ESS.WORKFLOW.ABSTRACT
{
    public class BaseCodeNameObject : BaseCodeObject
    {
        public BaseCodeNameObject()
        {
        }

        public string Name { get; set; }
    }
}