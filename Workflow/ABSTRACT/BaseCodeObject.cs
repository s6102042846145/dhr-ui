using System;

namespace ESS.WORKFLOW.ABSTRACT
{
    [Serializable()]
    public class BaseCodeObject : BaseObject
    {
        public BaseCodeObject()
        {
        }

        public string Code { get; set; }
    }
}