using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW.ABSTRACT;

namespace ESS.WORKFLOW
{
    [Serializable()]
    public class RequestBox : BaseCodeObject
    {
        #region Constructor
        public RequestBox()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, RequestBox> Cache = new Dictionary<string, RequestBox>();
        public string CompanyCode { get; set; }

        public static string ModuleID = "ESS.WORKFLOW";
        public static RequestBox CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new RequestBox()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            RequestBox oRequestBox = new RequestBox()
            {
                CompanyCode = oCompanyCode
            };
            return oRequestBox;
        }
        #endregion MultiCompany  Framework

        public bool IsCanDelegate { get; set; }

        public bool IsOwnerView { get; set; }

        public bool IsDataOwnerView { get; set; }

        public List<RequestBox> GetAuthorizeRequestBoxes(string BoxCategoryCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizeBoxes(BoxCategoryCode);
        }

        public List<RequestBox> GetAuthorizeRequestBoxes(string BoxCategoryCode, string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizeBoxes(BoxCategoryCode, EmployeeID);
        }

        public RequestBox GetRequestBox(string BoxCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestBox(BoxCode);
        }

        public bool GetAuthorizeMassApprove(string BoxCode, string EmployeeID, string EmployeePositionID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizeMassApprove(BoxCode, EmployeeID, EmployeePositionID);
        }

        public bool GetAuthorizeMassApproveForMobile(int BoxID, string EmployeeID, string EmployeePositionID)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAuthorizeMassApproveForMobile(BoxID, EmployeeID, EmployeePositionID);
        }

        public List<RequestDocument> GetDocumentList()
        {
            return GetDocumentList(this.Code);
        }

        public List<RequestDocument> GetDocumentList(string BoxCode)
        {
            return GetDocumentList(BoxCode, -1, -1);
        }

        public List<RequestDocument> GetDocumentList(string BoxCode, int ItemPerPage, int Page)
        {
            return GetDocumentList(BoxCode, ItemPerPage, Page, null);
        }
        public List<RequestDocument> GetDocumentList(string BoxCode, int ItemPerPage, int Page, RequestBoxFilter Filter)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestDocumentList(BoxCode, ItemPerPage, Page, Filter);
        }

        public int CountItemInBox(string BoxCode)
        {
            return CountItemInBox(BoxCode, null);
        }

        public int CountItemInBox(string BoxCode, RequestBoxFilter Filter)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetRequestDocumentList(BoxCode, 1001, 1, Filter).Count;
            //return ServiceManager.CreateInstance(CompanyCode).WorkflowService.CountItemInbox(BoxCode, Filter);
        }

        public List<RequestType> GetAvailableRequestType(string BoxCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).WorkflowService.GetAvailableRequestType(BoxCode);
        }

        public int CountItem
        {
            get
            {
                return CountItemInBox(this.Code);
            }
        }
    }
}