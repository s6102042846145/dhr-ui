﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace ESS.WORKFLOW
{
    public class ResponseLog
    {
        public ResponseLog()
        { 
        }
        public string RequestNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ResponseType { get; set; }
        public List<ResponseLogDetail> ResponseLogDetail { get; set; }
    }
}