﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHR.HR.API;
using DHR.HR.API.Model;
using ESS.SHAREDATASERVICE;

namespace DESS.API.DB
{
    public class PayrollDataServiceImpl : IPayrollDataService
    {
        public PayrollDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;

        }

        #region Property
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private string CompanyCode { get; set; }
        private static string ModuleID = "DHR.JOB.DB";
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion

        public string SaveAllBankMaster(List<PYBankMaster> data)
        {
            string ErrMsg = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                using (SqlTransaction oTransaction = oConnection.BeginTransaction())
                {
                    SqlCommand oCommand = null;
                    SqlCommand oCommandDel = null;
                    try
                    {
                        oCommandDel = new SqlCommand("sp_DHR_PY_Delete_BankMaster", oConnection, oTransaction);
                        oCommandDel.CommandType = CommandType.StoredProcedure;
                        oCommandDel.ExecuteNonQuery();
                        foreach (PYBankMaster item in data)
                        {
                            oCommand = new SqlCommand("sp_DHR_PY_Save_BankMaster", oConnection, oTransaction);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
                            oCommand.Parameters.AddWithValue("@p_bankKey", item.bankKey);
                            oCommand.Parameters.AddWithValue("@p_bankCode", item.bankCode);
                            oCommand.Parameters.AddWithValue("@p_bankValue", item.bankValue);
                            oCommand.Parameters.AddWithValue("@p_bankBranch", item.bankBranch);
                            oCommand.Parameters.AddWithValue("@p_street", item.street);
                            oCommand.Parameters.AddWithValue("@p_subdistrictCode", item.subdistrictCode);
                            oCommand.Parameters.AddWithValue("@p_districtCode", item.districtCode);
                            oCommand.Parameters.AddWithValue("@p_provinceCode", item.provinceCode);
                            oCommand.Parameters.AddWithValue("@p_postcode", item.postcode);
                            oCommand.Parameters.AddWithValue("@p_currencyCode", item.currencyCode);
                            oCommand.Parameters.AddWithValue("@p_currencyValue", item.currencyValue);
                            oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
                            oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

                            oCommand.ExecuteNonQuery();
                        }
                        oTransaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        ErrMsg = ex.Message;
                        oTransaction.Rollback();
                    }
                    finally
                    {
                        oCommand.Dispose();
                        oCommandDel.Dispose();
                        oConnection.Close();
                    }
                }
            }
            return ErrMsg;
        }
    }
}
