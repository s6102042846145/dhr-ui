﻿using DHR.HR.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.API.DB
{
    public interface ITimeDataService
    {
        #region JOB

        string SaveAllTMCalenHoliday(List<TMCalenHoliday> data);
        string SaveAllTMSetupHoliday(List<TMSetupHoliday> data);
        string SaveAllTMPlanWorkTime(List<TMPlanWorkTime> data);
        string SaveAllTMTimeRecType(List<TMTimeRecType> data);
        string SaveAllTMLeaveType(List<TMLeaveType> data);
        string SaveAllTMWorkingType(List<TMWorkingType> data);
        string SaveAllTMLeaveQuotaType(List<TMLeaveQuotaType> data);
        string SaveAllTMEmpGroup(List<TMEmpGroup> data);
        string SaveAllTMPersonalArea(List<TMPersonalArea> data);
        string SaveAllTMBreakSchedule(List<TMBreakSchedule> data);
        string SaveAllTMDailyWorkSchedule(List<TMDailyWorkSchedule> data);
        string SaveAllTMPeriodWorkSchedule(List<TMPeriodWorkSchedule> data);
        string SaveAllTMWorkSchedule(List<TMWorkSchedule> data);
        string SaveAllTMWorkScheduleRule(List<TMWorkScheduleRule> data);
        string SaveAllTMSubsititutionType(List<TMSubsititutionType> data);
        string SaveAllTMLeaveInf(List<TMLeaveInf> data);
        string SaveAllTMTimeAttend(List<TMTimeAttend> data);
        #endregion JOB
    }
}
