﻿using DESS.API.DB.Interfaces;
using DHR.HR.API;
using DHR.HR.API.Model;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DESS.API.DB
{
    public class OrganizationServiceImpl : IOrganizationService
    {
        public OrganizationServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }
      
        #region Property

        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        public string CompanyCode { get; set; }
        private static string ModuleID = "DHR.JOB.DB";
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }


        #endregion Property
        #region API Unuseed
        //public string SaveAllBelongTo(List<BelongTo> data ,string profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {

        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataBelongTo", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();

        //                foreach(BelongTo item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataBelongTo", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_posCode", item.posCode);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
        //                    oCommand.Parameters.AddWithValue("@p_unitCode", item.unitCode);
        //                    oCommand.ExecuteNonQuery();

        //                }
        //                oTransaction.Commit();
        //            }
        //            catch(Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();    
        //            } 
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}


        //public string SaveAllComSecondment(List<ComSecondment> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataComSecondment", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();

        //                foreach(ComSecondment item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataComSecondment", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_secondmentComCode", item.secondmentComCode);
        //                    oCommand.Parameters.AddWithValue("@p_secondmentShortname", item.secondmentShortname);
        //                    oCommand.Parameters.AddWithValue("@p_secondmentName", item.secondmentName);
        //                    oCommand.Parameters.AddWithValue("@p_secondmentShortnameEn", item.secondmentShortnameEn);
        //                    oCommand.Parameters.AddWithValue("@p_secondmentNameEn", item.secondmentNameEn);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }
        //            catch(Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}

        //public string SaveAllCompany(List<Company> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataCompany", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();

        //                foreach(Company item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataCompany",oConnection,oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_companyName", item.companyName);
        //                    oCommand.Parameters.AddWithValue("@p_addressCode", item.addressCode);
        //                    oCommand.Parameters.AddWithValue("@p_building", item.building);
        //                    oCommand.Parameters.AddWithValue("@p_floor", item.floor);
        //                    oCommand.Parameters.AddWithValue("@p_addressNo", item.addressNo);
        //                    oCommand.Parameters.AddWithValue("@p_street", item.street);
        //                    oCommand.Parameters.AddWithValue("@p_subdistrictCode", item.subdistrictCode);
        //                    oCommand.Parameters.AddWithValue("@p_district", item.district);
        //                    oCommand.Parameters.AddWithValue("@p_province", item.province);
        //                    oCommand.Parameters.AddWithValue("@p_postcode", item.postcode);
        //                    oCommand.Parameters.AddWithValue("@p_companyTaxid", item.companyTaxid);
        //                    oCommand.Parameters.AddWithValue("@p_companyAccount", item.companyAccount);
        //                    oCommand.Parameters.AddWithValue("@p_country", item.country);
        //                    oCommand.Parameters.AddWithValue("@p_companyNameEn", item.companyNameEn);
        //                    oCommand.Parameters.AddWithValue("@p_buildingEn", item.buildingEn);
        //                    oCommand.Parameters.AddWithValue("@p_floorEn", item.floorEn);
        //                    oCommand.Parameters.AddWithValue("@p_addressNoEn", item.addressNoEn);
        //                    oCommand.Parameters.AddWithValue("@p_streetEn", item.streetEn);
        //                    oCommand.Parameters.AddWithValue("@p_primaryLang", item.primaryLang);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }
        //            catch(Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();               
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }

        //        }

        //    }
        //    return ErrMsg;
        //}

        //public string SaveAllFiscalYear(List<FiscalYear> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataFiscalYear", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();
        //                foreach (FiscalYear item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataFiscalYear", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_startMonth", item.startMonth);
        //                    oCommand.Parameters.AddWithValue("@p_endMonth", item.endMonth);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}

        //public string SaveAllPosition(List<Position> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataPosition", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();

        //                foreach (Position item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataPosition", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_posCode", item.posCode);
        //                    oCommand.Parameters.AddWithValue("@p_posShortname", item.posShortname);
        //                    oCommand.Parameters.AddWithValue("@p_posName", item.posName);
        //                    oCommand.Parameters.AddWithValue("@p_posShortnameEn", item.posShortnameEn);
        //                    oCommand.Parameters.AddWithValue("@p_headOfUnitF", item.headOfUnitF);
        //                    oCommand.Parameters.AddWithValue("@p_bandCode", item.bandCode);
        //                    oCommand.Parameters.AddWithValue("@p_posNameEn", item.posNameEn);
        //                    //oCommand.Parameters.AddWithValue("@p_bandValue", item.bandValue);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }
        //            catch(Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}

        //public string SaveAllReportTo(List<ReportTo> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataReportTo", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();
        //                foreach (ReportTo item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataReportTo", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_unitCode", item.unitCode);
        //                    oCommand.Parameters.AddWithValue("@p_headPosition", item.headPosition);
        //                    oCommand.Parameters.AddWithValue("@p_headCode", item.headCode);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }

        //            catch (Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}

        //public string SaveAllUnit(List<Unit> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataUnit", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();

        //                foreach (Unit item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataUnit", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_unitCode", item.unitCode);
        //                    oCommand.Parameters.AddWithValue("@p_unitShortname", item.unitShortname);
        //                    oCommand.Parameters.AddWithValue("@p_unitName", item.unitName);
        //                    oCommand.Parameters.AddWithValue("@p_unitShortnameEn", item.unitShortnameEn);
        //                    oCommand.Parameters.AddWithValue("@p_unitNameEn", item.unitNameEn);
        //                    oCommand.Parameters.AddWithValue("@p_unitLevelCode", item.unitLevelCode);
        //                    oCommand.Parameters.AddWithValue("@p_unitLevelValue", item.unitLevelValue);
        //                    oCommand.Parameters.AddWithValue("@p_costCenterCode", item.costCenterCode);
        //                    oCommand.Parameters.AddWithValue("@p_costCenterValue", item.costCenterValue);
        //                    oCommand.Parameters.AddWithValue("@p_upperUnitCode", item.upperUnitCode);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));

        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }
        //            catch(Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}

        //public string SaveAllSolidLine(List<SolidLine> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataSolidLine", oConnection, oTransaction); 
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();
        //                foreach (SolidLine item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataSolidLine", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_unitCode", item.unitCode);
        //                    oCommand.Parameters.AddWithValue("@p_headPosition", item.headPosition);
        //                    oCommand.Parameters.AddWithValue("@p_headCode", item.headCode);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }

        //            catch (Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}

        //public string SaveAllDotedLine(List<DotedLine> data, string Profile)
        //{
        //    string ErrMsg = string.Empty;
        //    using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
        //    {
        //        oConnection.Open();
        //        using (SqlTransaction oTransaction = oConnection.BeginTransaction())
        //        {
        //            SqlCommand oCommand = null;
        //            SqlCommand oCommandDel = null;
        //            try
        //            {
        //                oCommandDel = new SqlCommand("sp_DHR_OM_Delete_DataDotedLine", oConnection, oTransaction);
        //                oCommandDel.CommandType = CommandType.StoredProcedure;
        //                oCommandDel.ExecuteNonQuery();
        //                foreach (DotedLine item in data)
        //                {
        //                    oCommand = new SqlCommand("sp_DHR_OM_Save_DataDotedLine", oConnection, oTransaction);
        //                    oCommand.CommandType = CommandType.StoredProcedure;
        //                    oCommand.Parameters.AddWithValue("@p_companyCode", item.companyCode);
        //                    oCommand.Parameters.AddWithValue("@p_headPosition", item.headPosition);
        //                    oCommand.Parameters.AddWithValue("@p_posCode", item.position);
        //                    oCommand.Parameters.AddWithValue("@p_startDate", DateTimeService.ConvertDateTime(CompanyCode, item.startDate));
        //                    oCommand.Parameters.AddWithValue("@p_endDate", DateTimeService.ConvertDateTime(CompanyCode, item.endDate));
        //                    oCommand.ExecuteNonQuery();
        //                }
        //                oTransaction.Commit();
        //            }

        //            catch (Exception ex)
        //            {
        //                ErrMsg = ex.Message;
        //                oTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                oCommand.Dispose();
        //                oCommandDel.Dispose();
        //                oConnection.Close();
        //            }
        //        }
        //    }
        //    return ErrMsg;
        //}
        #endregion
    }
}
