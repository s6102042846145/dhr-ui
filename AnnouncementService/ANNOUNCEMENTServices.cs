﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;

namespace ESS.ANNOUNCEMENT
{
    class ANNOUNCEMENTServices
    {
        public ANNOUNCEMENTServices()
        {

        }

        public static ANNOUNCEMENTServices SVC(string oCompanyCode)
        {
            ANNOUNCEMENTServices oANNOUNCEMENTServices = new ANNOUNCEMENTServices()
            {
                CompanyCode = oCompanyCode
            };
            return oANNOUNCEMENTServices;
        }

        private static string ModuleID = "ESS.ANNOUNCEMENT.SVC";
        private string CompanyCode { get; set; }

        public string GET_ANNOUNCEMENTLIST
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GET_ANNOUNCEMENTLIST");
            }
        }
        public string SET_ANNOUNCEMENT_UPDATE_STATE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SET_ANNOUNCEMENT_UPDATE_STATE");
            }
        }
        public string SET_DELETE_ANNOUNCEMENT_ID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SET_DELETE_ANNOUNCEMENT_ID");
            }
        }
        public string SAVE_ANNOUNCEMENT
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_ANNOUNCEMENT");
            }
        }
        public string SAVE_EMPLOYEE_ACCEPTANNOUNCEMENT
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAVE_EMPLOYEE_ACCEPTANNOUNCEMENT");
            }
        }
        

    }
}
