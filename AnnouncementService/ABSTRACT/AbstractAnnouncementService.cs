#region Using

using System;
using System.Collections.Generic;
using System.Data;
using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.ANNOUNCEMENT.INTERFACE;

#endregion Using

namespace ESS.ANNOUNCEMENT.ABSTRACT
{
    public class AbstractAnnouncementService : IAnnouncementService
    {
        #region IAnnouncementInterface Members

        public virtual List<AnnouncementData> GetAnnoncementList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void acceptAnnouncement(string employeeID, string announcementID, string statusType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void announcementSave(AnnouncementData announcement)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void announcementDelete(int announcementID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void announcementUpdateState(int oID, string oState)
        {
            throw new Exception("The methos annoncementUpdateState is not implemented");
        }

        public virtual void announcementDeleteByRequestNo(string RequestNo)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}