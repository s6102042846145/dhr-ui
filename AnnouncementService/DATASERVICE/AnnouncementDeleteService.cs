﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.DATA.ABSTRACT;
using ESS.DATA;
using ESS.EMPLOYEE;
using System.Globalization;
using ESS.ANNOUNCEMENT.DATACLASS;
using Newtonsoft.Json;

namespace ESS.ANNOUNCEMENT.DATASERVICE
{
    public class AnnouncementDeleteService : AbstractDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");

        //public string ImagePath
        //{
        //    get
        //    {
        //        return ServiceManager.ANNOUNCEMENT_PATH;
        //    }
        //}

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, ref object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            AnnouncementData oAnnouncementData = JsonConvert.DeserializeObject<AnnouncementData>(Data.ToString());
            AnnouncementManagement oAnnouncementManagement = AnnouncementManagement.CreateInstance(Requestor.CompanyCode);

            if (State == "COMPLETED")
            {
                oAnnouncementManagement.announcementDelete(oAnnouncementData.ID);
            }
            
        }

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            AnnouncementData announces = new AnnouncementData();

            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] param = CreateParam.Split('|');
                List<AnnouncementData> tmp = new List<AnnouncementData>();
                tmp = AnnouncementManagement.CreateInstance(Requestor.CompanyCode).GetAnnoncementList().Where(x => x.ID == Convert.ToInt16(param[1])).ToList();
                announces = tmp[0];
            }
            else
            {
                announces.ID = -1;
                announces.Name = string.Empty;
                announces.Content = string.Empty;
                announces.Image = string.Empty;
                announces.Priority = 0;
                announces.NewFlagDate = DateTime.Now;
                announces.BeginDate = DateTime.Now;
                announces.EndDate = DateTime.Now;
                announces.RequestNo = "";
                announces.Type = "NEWS";
                announces.Acceptable = false;
            }

            return announces;
        }

        public override void PrepareData(EmployeeData Requestor, ref object Data)
        {
            //throw new NotImplementedException();
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //throw new NotImplementedException();
        }
    }
}
