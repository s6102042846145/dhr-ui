#region Using

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.ANNOUNCEMENT.INTERFACE;
using ESS.SHAREDATASERVICE;

#endregion Using

namespace ESS.ANNOUNCEMENT
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion 

        #region MultiCompany  Framework
        private static string ModuleID = "ESS.ANNOUNCEMENT";

        public string CompanyCode { get; set; }
        private string DataSource { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }

        public static ServiceManager CreateInstance(string oCompanyCode, string oDataSource)
        {

            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode,
                DataSource = oDataSource
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        #region Member
        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        #endregion Member

        #region Properties

        #endregion Properties

        #region Function

        private Type GetService(string Mode)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.AnnouncementServiceImpl", ModuleID, Mode.ToUpper());
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            return oReturn;
        }

        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName); //CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        public IAnnouncementService ESSData
        {
            get
            {
                IAnnouncementService service;
                Type oType = GetService(ESSCONNECTOR);
                service = (IAnnouncementService)Activator.CreateInstance(oType,CompanyCode);
                //service.CompanyCode = CompanyCode;
                return service;
            }
        }

        public IAnnouncementService ESSConfig
        {
            get
            {
                IAnnouncementService service;
                Type oType = GetService(ESSCONNECTOR);
                service = (IAnnouncementService)Activator.CreateInstance(oType, CompanyCode);
                //service.CompanyCode = CompanyCode;
                return service;
            }
        }

        #endregion Function

        #region Multi Source Data Service
        public IAnnouncementService AnnouncementService
        {
            get
            {
                Type oType = GetService(DataSource, "AnnouncementServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IAnnouncementService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
        
        #endregion Multi Source Data Service
    }
}