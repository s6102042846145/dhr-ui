#region Using

using System.Collections.Generic;
using System.Data;
using ESS.ANNOUNCEMENT.DATACLASS;

#endregion Using

namespace ESS.ANNOUNCEMENT.INTERFACE
{
    public interface IAnnouncementService
    {
        #region Member

        List<AnnouncementData> GetAnnoncementList();
        void acceptAnnouncement(string employeeID, string announcementID, string statusType);
        void announcementSave(AnnouncementData announcement);
        void announcementDelete(int announcementID);
        void announcementUpdateState(int oID, string oState);

        void announcementDeleteByRequestNo(string RequestNo);
        #endregion Member
    }
}