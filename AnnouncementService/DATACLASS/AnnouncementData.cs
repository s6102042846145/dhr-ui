#region Using

using System;
using System.ComponentModel;
using ESS.DATA.FILE;
using ESS.UTILITY.EXTENSION;

#endregion Using

/*
AddBy: Ratchatawan W. (2013-02-26)
*/

namespace ESS.ANNOUNCEMENT.DATACLASS
{
    public class AnnouncementData : AbstractObject
    {
        #region Constructor

        public AnnouncementData()
        {
            Type oPropertyType = null;
            foreach (PropertyDescriptor oCurrentProperty in TypeDescriptor.GetProperties(this))
            {
                oPropertyType = oCurrentProperty.PropertyType;
                if (oPropertyType.Name.ToLower() == "string")
                {
                    oCurrentProperty.SetValue(this, "");
                }
                else if (oPropertyType.Name.ToLower() == "boolean")
                {
                    oCurrentProperty.SetValue(this, false);
                }
                else if (oPropertyType.Name.IndexOf("int", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    oCurrentProperty.SetValue(this, 0);
                }
                else if (oPropertyType.Name.ToLower() == "decimal")
                {
                    oCurrentProperty.SetValue(this, Convert.ToDecimal(0.00));
                }
                else if (oPropertyType.Name.ToLower() == "double")
                {
                    oCurrentProperty.SetValue(this, 0.0);
                }
                else if (oPropertyType.Name.ToLower() == "datetime")
                {
                    oCurrentProperty.SetValue(this, DateTime.MinValue);
                }
            }
        }

        #endregion Constructor

        #region Properties

        public int ID { get; set; }

        public string Name { get; set; }
        public string NameEN { get; set; }
        public string Content { get; set; }
        public string ContentEN { get; set; }
        public string Image { get; set; }

        public string NewImage { get; set; }

        public int Priority { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime NewFlagDate { get; set; }

        public string Type { get; set; }

        public string RequestNo { get; set; }

        public bool Acceptable { get; set; }

        public string AcceptedList { get; set; } //EmployeeID was accept

        public string SeenList { get; set; } //EmployeeID was see

        public int EmpQuatityAccess { get; set; } //EmployeeID can see each announces

        public FileAttachment FileAttach { get; set; }

        #endregion Properties
    }
}